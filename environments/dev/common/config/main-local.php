<?php

return [
    'components' => [
        'cache' => [
          'class' => 'yii\caching\FileCache',
        ], 
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=103.21.217.196:3306;dbname=crowe_tr',
            'username' => 'root',
            'password' => 'T1m3R3p0rt@semut2019!',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ], 
        /*'mailer' => [
		    'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.knn.co.id',
                'username' => 'noreply-timereport@knn.co.id',
                'password' => 'semutsemuthitam',
                'port' => '465',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],*/
        'mailer' => [
		    'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'noreply.crowe@gmail.com',
                'password' => 'semutsemuthitam',
                'port' => '587',
                'encryption' => 'tls',
                /*
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
                */
            ],
        ],

    ],
];
