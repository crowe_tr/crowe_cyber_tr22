<?php
  $config = [
      'components' => [
          'request' => [
              // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
              'cookieValidationKey' => '111000101',
          ],
          'assetManager' => [
            'appendTimestamp' => true,
            'hashCallback' => function ($path) {
                return hash('md4', $path);
            },
          ],
      ],
  ];

  if (!YII_ENV_TEST) {
      /*$config['bootstrap'][] = 'debug';
      $config['modules']['debug'] = [
          'class' => 'yii\debug\Module',
      ];*/
      $config['bootstrap'][] = 'gii';
      $config['modules']['gii'] = [
          'class' => 'yii\gii\Module',
      ];
  }

  return $config;
