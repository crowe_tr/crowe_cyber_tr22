<?php
  $config = [
      'components' => [
          'request' => [
              // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
              'cookieValidationKey' => '111000101',
          ],
          'assetManager' => [
            'appendTimestamp' => true,
            'hashCallback' => function ($path) {
                return hash('md4', $path);
            },
          ],
      ],
  ];
  return $config;
