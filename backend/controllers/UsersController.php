<?php

namespace backend\controllers;

use Yii;
use common\models\USERS;
use common\models\EMPLOYEE;
use common\models\RIGHTS_AUTH_ASSIGMENT;
use common\models\RIGHT_AUTH_ITEM;
use yii\helpers\Url;


use backend\models\USERSSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for USERS model.
 */
class UsersController extends Controller
{
	public $layout = 'main';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	public function actionAjaxfillfromemployee($update = "") {
		$_return = array();
		$_return['EMAIL'] = '';

        if (Yii::$app->request->isAjax) {
			$model = new USERS;
			if ($model->load(Yii::$app->request->post())) {
				$model_employee = $this->findModel_employee($model->PERSON_ID);
				$_return['EMAIL'] = $model_employee->EMAIL;
			}
            
            echo JSON_ENCODE($_return);
        }else{
           throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function InitValues($model){
        if(!$model->isNewRecord){ //Khusus ketika update data kk
            
        }
        return $model;
    }
    public function FormatValues($model, $old_model = ""){
        if($model->IsNewRecord){
            $model->CREATED_BY = !empty(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '-1';
            $model->DATE_INPUT = date('Y-m-d H:i:s');
            $model->FLAG = 1;

			$password_match = false;
			if($model->PASSWORD_HASH == $model->PASSWORD_REPEAT){
				$password_match = true;
			}
			
            $model->setPassword($model->PASSWORD_HASH);
            $model->generateAuthKey();
			
			if($password_match){
				$model->PASSWORD_REPEAT = $model->PASSWORD_HASH;
			}
			
        }else{
			if($model->PASSWORD_HASH != $old_model->PASSWORD_HASH){
				$model->setPassword($model->PASSWORD_HASH);
				$model->generateAuthKey();
				
				$model->PASSWORD_REPEAT = $model->PASSWORD_HASH;
			}
			$model->UPDATED_BY = !empty(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '-1';
			$model->LAST_UPDATE = date('Y-m-d H:i:s');
        }
                
        return $model;
    }
    public function actionIndex($action = "", $id ="")
    {
		$this->layout = 'master';
        $post = Yii::$app->request->post();
		$show_modal = 0;
		
        $model = new USERS();
		$model->scenario = 'setting';

		if(!empty($action)){
			if(!empty($id)) {
				if ($action == 'update') {
					$model = $this->findModel($id);
					$model->scenario = 'setting';
					
					$roles = Yii::$app->authManager->getRolesByUser($model->USERNAME);
					$model->PERMISSIONS = array();
					foreach($roles as $role){
						$model->PERMISSIONS[$role->name] = $role->name;
					}
				} else if ($action == 'duplicate') {
					$copy_model = $this->findModel($id);

					$model = $copy_model;
					$model->USERNAME = "";
					$model->PASSWORD_HASH = "";
					$model->IsNewRecord = true;
				}
				$model->PASSWORD_REPEAT = $model->PASSWORD_HASH;
			}
			$show_modal = 1;
		}
		if ($model->load($post)) {
			$old_model = $model;
			if(!$model->isNewrecord){
				$old_model = $this->findModel($id);
			}
            $model = $this->FormatValues($model, $old_model);
            if($model->save()){
				$auth = Yii::$app->authManager;
				$auth->revokeAll($old_model->USERNAME);
				
				if(!empty($model->PERMISSIONS)){
					foreach($model->PERMISSIONS as $PERMISSION){
						$role = $auth->getRole($PERMISSION);
						$auth->assign($role, $model->USERNAME);
					}
				}

               return $this->redirect(Url::current(['action' => null, 'id'=> null]));
            }else{
				$model->PASSWORD_REPEAT = "";
				$model->PASSWORD_HASH = "";
			}
        }
		
        $searchModel = new USERSSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'show_modal' => $show_modal,
        ]);
    }
    public function actionDelete($id)
    {
		$auth = Yii::$app->authManager;
        $model = $this->findModel($id);
		$username = $model->USERNAME;
		if($model->delete()){
			$auth->revokeAll($username);
		}
        return $this->redirect(['index']);
    }

    /**
     * Finds the USERS model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return USERS the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel_employee($id)
    {
        if (($model = EMPLOYEE::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModel($id)
    {
        if (($model = USERS::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
