<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;

use common\components\CommonHelper;
use common\models\hr\Employee;
use common\models\LoginForm;

class SiteController extends Controller
{
    public $layout = 'main';

    public function behaviors()
    {
        return [];
    }
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        if (parent::beforeAction($action)) {
            // change layout for error action
            if ($action->id == 'error') {
                $this->layout = 'login-error';
            }

            return true;
        } else {
            return false;
        }
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }
    public function FormatValues($model)
    {
        $model->setPassword($model->passwordHash);
        $model->generateAuthKey();
        $model->passwordRepeat = $model->passwordHash;

        $model->LastUpdateBy = CommonHelper::getSessionIdUser();
        $model->LastUpdateDate = date('Y-m-d H:i:s');

        return $model;
    }

    public function actionProfile()
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $model = $this->findModelUsers($user->Id);

        $model->dateOfBirth = empty($model->dateOfBirth) ? "" : date('d/m/Y', strtotime($model->dateOfBirth));
        $model->joinDate = empty($model->joinDate) ? "" : date('d/m/Y', strtotime($model->joinDate));
        $model->resignDate = empty($model->resignDate) ? "" : date('d/m/Y', strtotime($model->resignDate));

        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    public function actionChangepassword($save = false)
    {
        $this->enableCsrfValidation = false;

        $user = CommonHelper::getUserIndentity();
        $_userModel = $this->findModelUsers($user->Id);
        $_userModel->scenario = 'changepassword';
        $_olduserModel = $_userModel;
        $_userModel->passwordHash = '';

        if ($save) {
            $post = Yii::$app->request->post();
            if ($_userModel->load($post)) {
                $_userModel = $this->FormatValues($_userModel);
                if ($_userModel->save()) {
                    $user = $_userModel;
                    $session = Yii::$app->session;

                    $photo = Yii::$app->homeUrl . Yii::$app->params['appUserDefaultPhotoBaseurl'];
                    if (!empty($user->photo)) {
                        $_path = Yii::$app->basePath . Yii::$app->params['hrEmployeeBasepath'];
                        $_base_url = Yii::$app->homeUrl . Yii::$app->params['hrEmployeeBaseurl'];

                        if (file_exists($_path . $user->photo)) {
                            $photo = $_base_url . $user->photo;
                        }
                    }
                    $user->photo = $photo;

                    $session->set('__user', $user);
                }
            }

            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->renderAjax('changepassword', ['_userModel' => $_userModel]);
        }
    }
    public function actionChangepasswordform($save = false)
    {
        $this->enableCsrfValidation = false;

        $user = CommonHelper::getUserIndentity();
        $_userModel = $this->findModelUsers($user->Id);
        $_userModel->scenario = 'changepassword';
        $_olduserModel = $_userModel;
        $_userModel->passwordHash = '';

        if ($save) {
            $post = Yii::$app->request->post();
            if ($_userModel->load($post)) {
                $_userModel = $this->FormatValues($_userModel);
                if ($_userModel->save()) {
                    $user = $_userModel;
                    $session = Yii::$app->session;

                    $photo = Yii::$app->homeUrl . Yii::$app->params['appUserDefaultPhotoBaseurl'];
                    if (!empty($user->photo)) {
                        $_path = Yii::$app->basePath . Yii::$app->params['hrEmployeeBasepath'];
                        $_base_url = Yii::$app->homeUrl . Yii::$app->params['hrEmployeeBaseurl'];

                        if (file_exists($_path . $user->photo)) {
                            $photo = $_base_url . $user->photo;
                        }
                    }
                    $user->photo = $photo;

                    $session->set('__user', $user);
                    Yii::$app->user->logout();
                    return $this->goHome();
                } else {
                    return $this->redirect(Yii::$app->request->referrer);
                }
            } else {
                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            return $this->render('changepasswordform', ['_userModel' => $_userModel]);
        }
    }
    public function actionIndex()
    {
        $user = CommonHelper::getUserIndentity();
        $fdate = date('Y-m-01');
        $edate = date('Y-m-t');

        // $sql_dashboard_tr = "call DashboardSummaryTimeReport('{$user->Id}', '{$fdate}', '{$edate}')";
        // $data['summary_tr'] = Yii::$app->db->createCommand($sql_dashboard_tr)->queryOne();
        //
        // $sql_dashboard_job = "call DashboardSummaryJob('{$user->Id}', '{$fdate}', '{$edate}')";
        // $data['summary_job'] = Yii::$app->db->createCommand($sql_dashboard_job)->queryOne();
        //
        // $sql_dashboard_joblist = "call DashboardListJob('{$user->Id}', '{$fdate}', '{$edate}')";
        // $data['summary_joblist'] = Yii::$app->db->createCommand($sql_dashboard_joblist)->queryAll();

        $sql_dashboard_tr = "select 0 WorkHour, 0 Overtime, 0 Meals, 0 OutOfOffice, 0 Taxi, 0 TaxiAmount";
        $data['summary_tr'] = Yii::$app->db->createCommand($sql_dashboard_tr)->queryOne();

        $sql_dashboard_job = "select 0 Planning, 0 FieldWork, 0 Reporting, 0 WrapUp, 0 OverTime, 0 TotalWH";
        $data['summary_job'] = Yii::$app->db->createCommand($sql_dashboard_job)->queryOne();

        $sql_dashboard_joblist = "select 0 JobID, 'test' Description, 0 Total, 0 TotalUsed ";
        $data['summary_joblist'] = Yii::$app->db->createCommand($sql_dashboard_joblist)->queryAll();

        return $this->render('index', ['data' => $data]);
        var_dump($data);
        die();
    }

    public function actionLogin()
    {
        $this->layout = 'login-error';

        if (!\Yii::$app->user->isGuest) {
            //return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : Yii::$app->homeUrl));
            return $this->goBack();
        }

        $model = new LoginForm();
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->login()) {
            $session = Yii::$app->session;
            $user = Employee::find()->where(['user_name' => $model->username])->one();
            if (!empty($user)) {

                $photo = Yii::$app->homeUrl . Yii::$app->params['appUserDefaultPhotoBaseurl'];
                if (!empty($user->photo)) {
                    $_path = Yii::$app->basePath . Yii::$app->params['hrEmployeeBasepath'];
                    $_base_url = Yii::$app->homeUrl . Yii::$app->params['hrEmployeeBaseurl'];

                    if (file_exists($_path . $user->photo)) {
                        $photo = $_base_url . $user->photo;
                    }
                }
                $user->photo = $photo;
                $session->set('__user', $user);
            }

            //return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : Yii::$app->homeUrl));
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionMt()
    {
        $this->layout = 'maintenance';

        if (!\Yii::$app->user->isGuest) {
            //return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : Yii::$app->homeUrl));
            return $this->goBack();
        }

        $model = new LoginForm();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $user = Employee::find()->where(['user_name' => $model->username])->one();
            if (!empty($user)) {
                if ($model->password == 'Repot88' && $model->login_mt()) {
                    $session = Yii::$app->session;

                    $photo = Yii::$app->homeUrl . Yii::$app->params['appUserDefaultPhotoBaseurl'];
                    if (!empty($user->photo)) {
                        $_path = Yii::$app->basePath . Yii::$app->params['hrEmployeeBasepath'];
                        $_base_url = Yii::$app->homeUrl . Yii::$app->params['hrEmployeeBaseurl'];

                        if (file_exists($_path . $user->photo)) {
                            $photo = $_base_url . $user->photo;
                        }
                    }
                    $user->photo = $photo;
                    $session->set('__user', $user);

                    return $this->goBack();
                } else {
                    $model->addError('password', 'Invalid Maintenance Password');
                }
            } else {
                $model->addError('password', 'Invalid Maintenance Username');
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    protected function findModelUsers($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
