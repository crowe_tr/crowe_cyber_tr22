<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;


/*
    Fm for File Manager 
    Hiphip ayee

*/

class FmController extends Controller
{
    public function beforeAction($action)
    { //temporary berbahaya kalau dibuat seperti ini https://stackoverflow.com/questions/38299474/yii2-bad-request-400-unable-to-verify-your-data-submission
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionUpload()
    {
        $folder = "public/tmp/";

        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $filename = date('ymdhis') . rand() . '.' . $ext;
        move_uploaded_file($_FILES["file"]['tmp_name'], $folder . $filename);

        return $filename;
    }
}
