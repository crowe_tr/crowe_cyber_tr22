<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use common\models\cm\CmDept;
use common\models\cm\CmSupplier;
use common\models\hr\hrPosition;
use common\models\hr\HrEmployee;
use common\models\fn\FnGoodsUnit;
use common\models\fn\FnPcrMsGoods;
use common\models\CmMsGnProvince;
use common\models\CmMsGnRegency;
use common\models\CmMsGnSubdistrict;
use common\models\CmMsGnVillage;

/**
 * Helper controller.
 */
class HelperController extends Controller
{
    public $layout;
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSpoof()
    {
        $mailqueue = \Yii::$app->mailqueue->compose(
            [
                'html' => 'mail',
            ], ['model' => null]
            );
        $mailqueue->getSwiftMessage()->getHeaders()->addTextHeader('From', 'value');
        $mailqueue->setFrom(['sysadmin@dutacendana.co.id' => 'PROKU Notifications'])
            ->setReplyto(['sysadmin@dutacendana.co.id' => 'PROKU Notifications'])
            ->setTo('sysadmin@dutacendana.co.id')
            ->setSubject('Spoof')
            ->queue();

        return $mailqueue;
    }

    public function actionSearchemployeedonothaveuseropt($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        $model = HrEmployee::find()
                    ->select('Id as id, Name as text')
                    ->where("Name LIKE '%{$q}%' OR Id = '{$q}'")
                    ->limit(10)
                    ->asArray()
                    ->all();
        $out['results'] = $model;

        return $out;
    }
    public function actionSearchemployeeopt($q = '', $id = '')
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        $model = HrEmployee::find()
                ->select('Id as id, Name as text')
                ->where(['LIKE', 'Name', $q])
                ->limit(10)
                ->asArray()
                ->all();
        $out['results'] = $model;
        if (!empty($id)) {
            $out['results'] = ['id' => $id, 'text' => HrEmployee::findOne($id)->Name];
        }

        return $out;
    }
    public function actionSearchitemopt($q = null, $id = null, $office = null, $departement = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            // and ((CmMsCpOffice_Id = '{$office}' or CmDept_Id = '{$departement}') or (CmMsCpOffice_Id = '' or CmDept_Id = ''))
            $model = FnPcrMsGoods::find()
                    ->select('
						FnPcrMsGoods.Id as id,
						FnPcrMsGoods.Name as text,
						CmSupplier_Id,
						CmSupplier.Name as Vendor,
						Cost,
						CurrencyCode,
						FnGoodsUnit_Id,
						FnGoodsUnit.Unit as Unit,
					')
                    ->JoinWith('unit')
                    ->JoinWith('vendor')
                    ->where("FnPcrMsGoods.Name LIKE '%{$q}%'")
                    ->asArray()
                    ->limit(10)
                    ->all();

            $out['results'] = $model;
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => FnPcrMsGoods::findOne($id)->Name];
        }

        return $out;
    }

    public function actionSearchitemtypehead($q = null, $office = null, $departement = null)
    {
        $out = array();
        $model = FnPcrMsGoods::find()
                ->where("Name LIKE '{$q}%' or (CmMsCpOffice_Id = null or CmDept_Id = null)")
                ->limit(10)
                ->all();
        foreach ($model as $data) {
            $out[] = [
                'Name' => $data->Name,
                'Cost' => $data->Cost,
                'Cost' => $data->Cost,
                'CurrencyCode' => !empty($data->CurrencyCode) ? $data->CurrencyCode : '',
                'Vendor' => !empty($data->vendor->Name) ? $data->vendor->Name : '',
                'Unit' => !empty($data->unit->Unit) ? $data->unit->Unit : '',
            ];
        }

        return json_encode($out);
    }
    public function actionSrcsupplieropt($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = CmSupplier::find()
                ->select('Id as id, Name as text, ContactPerson, Phone, Fax, Address, Logo')
                ->where(
                  "ContactPerson LIKE '%{$q}%' OR Name LIKE '%{$q}%' OR Id = '{$q}' "
                )
                ->limit(20)
                ->asArray()
                ->all();
            $out['results'] = array();
            foreach ($model as $data) {
                $name = $data['text'];
                $cp = $data['ContactPerson'];
                $phone = $data['Phone'];
                $fax = $data['Fax'];
                $address = $data['Address'];
                $logo = empty($data['Logo']) ? '' : "<img src='{$data['Logo']}' class='img img-responsive pull-right no-padding col-xs-2' height='20px' width='20px'/>";

                $array = array(
                          'id' => $data['id'],
                          'text' => "<div class='no-margin no-padding' >{$logo}"
                                        ."<p class='bold fs-13 no-margin'>{$name}</p>"
                                        ."<p class='no-margin'>CP : {$cp}</p>"
                                        ."<p class='no-margin'>P : {$phone}, Fax : {$fax}</p>"
                                        ."<p class='fs-12 no-margin'>{$address}</p>"
                                    .'</div>',
                    );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CmSupplier::findOne($id)->Name];
        }

        return $out;
    }

    public function actionSearchbilltoopt($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = FnMsPcrBillTo::find()
                            ->select('Id as id, Name as text, ContactPerson, Phone, Address')
                            ->where(['LIKE', 'Name', $q])
                            ->limit(20)
                            ->asArray()
                            ->all();
            $out['results'] = array();
            foreach ($model as $data) {
                $name = $data['text'];
                $cp = $data['ContactPerson'];
                $phone = $data['Phone'];
                $address = $data['Address'];

                $array = array(
                                      'id' => $data['id'],
                                      'text' => "<div class='row no-margin' >"
                                              ."<div class='col-xs-12 no-padding pull-left'>"
                                                      ."<p class='bold fs-13 no-margin'>$name</p>"
                                                      ."<p class='no-margin'>CP : ".$cp.'</p>'
                                                      ."<p class='fs-12 no-margin'>"
                                                              .$phone
                                                              .'<br/>'
                                                              .$address
                                                          .'</p>'
                                              .'</div>'
                                          .'</div>',
                                );
                array_push($out['results'], $array);
            }

                    //$out['results'] = $model;
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CmSupplier::findOne($id)->Name];
        }

        return $out;
    }
    public function actionSearchshiptoopt($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = FnMsPcrShipTo::find()
                            ->select('Id as id, Name as text, ContactPerson, Phone, Address')
                            ->where(['LIKE', 'Name', $q])
                            ->limit(20)
                            ->asArray()
                            ->all();
            $out['results'] = array();
            foreach ($model as $data) {
                $name = $data['text'];
                $cp = $data['ContactPerson'];
                $phone = $data['Phone'];
                $address = $data['Address'];

                $array = array(
                                      'id' => $data['id'],
                                      'text' => "<div class='row no-margin' >"
                                              ."<div class='col-xs-12 no-padding pull-left'>"
                                                      ."<p class='bold fs-13 no-margin'>$name</p>"
                                                      ."<p class='no-margin'>CP : ".$cp.'</p>'
                                                      ."<p class='fs-12 no-margin'>"
                                                              .$phone
                                                              .'<br/>'
                                                              .$address
                                                          .'</p>'
                                              .'</div>'
                                          .'</div>',
                                );
                array_push($out['results'], $array);
            }

                    //$out['results'] = $model;
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CmSupplier::findOne($id)->Name];
        }

        return $out;
    }
    public function actionSearchunitopt($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = FnGoodsUnit::find()
                    ->select('Id as id, Unit as text')
                    ->where(['LIKE', 'Unit', $q])
                    ->limit(20)
                    ->asArray()
                    ->all();
            $out['results'] = $model;
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => FnGoodsUnit::findOne($id)->Unit];
        }

        return $out;
    }

    public function actionLoaddepartement()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = CmDept::find()->andWhere(['CmMsCpOffice_Id' => $id])->asArray()->all();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['Id'], 'name' => $ls['Departement']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);

                return;
            }
        }
        echo json_encode(['output' => array(), 'selected' => '']);
    }
    public function actionLoadprovince()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = CmMsGnProvince::find()->andWhere(['CmMsGnCountry_Id' => $id])->asArray()->all();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['Id'], 'name' => $ls['Province']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);

                return;
            }
        }
        echo json_encode(['output' => array(), 'selected' => '']);
    }

    public function actionLoadregency()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = CmMsGnRegency::find()->andWhere(['CmMsGnProvince_Id' => $id])->asArray()->all();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['Id'], 'name' => $ls['Regency']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);

                return;
            }
        }
        echo json_encode(['output' => array(), 'selected' => '']);
    }

    public function actionLoadsubdistrict()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = CmMsGnSubdistrict::find()->andWhere(['CmMsGnRegency_Id' => $id])->asArray()->all();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['Id'], 'name' => $ls['Subdistrict']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);

                return;
            }
        }
        echo json_encode(['output' => array(), 'selected' => '']);
    }

    public function actionLoadvillage()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = CmMsGnVillage::find()->andWhere(['CmMsGnSubdistrict_Id' => $id])->asArray()->all();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['Id'], 'name' => $ls['Village']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);

                return;
            }
        }
        echo json_encode(['output' => array(), 'selected' => '']);
    }
    public function actionLoadjobtitle()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = hrPosition::find()->andWhere(['CmDept_Id' => $id])->asArray()->all();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['Id'], 'name' => $ls['Position']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);

                return;
            }
        }
        echo json_encode(['output' => array(), 'selected' => '']);
    }
}
