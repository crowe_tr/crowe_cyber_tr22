<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use yii\widgets\MaskedInput;

use kartik\widgets\ActiveForm;

use common\models\EMPLOYEE;
use common\models\USERS;
use common\models\RIGHT_AUTH_ITEM;


$this->title = Yii::t('backend', 'Account Setting');

?>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-default m-t-20">
				<div class="panel-body">
					<h3 class="no-margin">Setting akun</h3>
					<p>
						Berikut informasi untuk akun anda yang digunakan untuk login.
					</p>
					<?php $form = ActiveForm::begin(['fieldConfig' => ['options'=> ['class' => 'form-group form-group-default']]]); 
					echo $form->errorSummary($model);
					?>
						<div class="form-group-attached">
							<div class="row">
								<div class="col-md-6">
									<?= $form->field($model, 'Id')->textInput(['options'=>['disabled'=>true]]) ?>
								</div>
								<div class="col-md-6">
									<?=                        
									$form->field($model, 'Email')->widget(MaskedInput::className(), [
										'clientOptions' => [
											'alias' =>  'email'
										],
									]) 
									?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<?= $form->field($model, 'PasswordHash')->passwordInput() ?>
								</div>
								<div class="col-md-6">
									<?php $model->PasswordRepeat = $model->PasswordHash; ?>
										<?= $form->field($model, 'PasswordRepeat')->passwordInput() ?>
								</div>
							</div>
						</div>

						<br>
						<?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Tambah Log') : Yii::t('backend', 'Perbaharui akun'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
							<?= Html::a(
		Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-default']) ?>

								<?php ActiveForm::end(); ?>
				</div>
			</div>

		</div>
	</div>
