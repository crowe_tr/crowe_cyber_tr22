<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
function getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}
$this->title = $name;
?>
    <h1>
        <b class="font-red"><?= Html::encode($this->title) ?></b>
        <br>
        <small><?= nl2br(Html::encode($message)) ?></small>
    </h1>
    <p>
        Click
        <?= Html::a(Yii::t('app', 'here'), ['site/index'], ['class' => '']) ?>
        to return to home page or click
        <?= Html::a(Yii::t('app', 'here'), ['site/logout'], ['class' => '']) ?>
        to relogin
        <br> -
    </p>
    <h4>
        <?php echo getUserIP(); ?> 
        <br>
        <small><?php echo $_SERVER['HTTP_USER_AGENT'] ; ?></small>
    </h4>
