<?php
use yii\helpers\Html;
?>
<a href="" style="display: inline-block;
width: 100%;
border-bottom: 1px solid #E9ECF2;
text-decoration: none;
padding: 0px;
background: #FFF none repeat scroll 0% 0%;">
	<div class="message_list_block" style="padding: 15px;">
		<div class="right" style="margin-left: 0 !important;">
			<span class="name">
				<?= !empty($model->creator->NAME) ? $model->creator->NAME : '<i>not set</i>'; ?>
			</span>
			<div class="pull-right right_details">
				<ul class="list-unstyled list-inline">
					<li><?= date('H:i, d M Y ', strtotime($model->DATE_INPUT)); ?></li>
				</ul>
			</div>
			<h4><?= Html::encode($model->DESCRIPTION); ?></h4>
		</div>
	</div>
</a>