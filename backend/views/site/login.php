<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
	<p class="p-t-35">Sign with your account</p>

		<?php
			$form = ActiveForm::begin([
					'enableClientValidation' => true,
					'enableAjaxValidation' => false,
					'validateOnSubmit' => true,
					'validateOnChange'  => true,
					'validateOnType' => true,
					'options'=>['enctype'=>'multipart/form-data'],
					'fieldConfig' => [
						'template' => '{label}{input}',
						'options'=> [
							'class' => 'form-group form-group-default'
						]
					]
				]);
				echo $form->errorSummary($model);

		?>
		<div class="form-group-attached">
			<div class="row">
				<div class="col-md-12">
					<?= $form->field($model, 'username')->textInput(['placeholder'=>'Login']) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Credentials']) ?>
				</div>
			</div>
		</div>
			<div class="row">
				<div class="col-6">
					<?php $model->rememberMe = false ?>
						<?php $form->field($model, 'rememberMe', [
								'template' => '
										{input}{label}
								',
							'options' => ['class' => '']
							]
					)->checkbox() ?>
					<?= Html::submitButton('Sign in', ['class' => 'btn btn-success btn-cons m-t-10', 'name' => 'login-button']) ?>

				</div>
				<div class="col-6 text-right">
					<a href="https://antariksa.org" class="text-info small">Help? Contact Support</a>
				</div>
			</div>




		<?php ActiveForm::end(); ?>
