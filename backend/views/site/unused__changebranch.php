<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use common\models\cm\CmCompany;
use common\models\cm\CmCompanyBranch;
use common\models\cm\CmDept;

$rand = rand();
$form = ActiveForm::begin([
    'id' => $rand.'-form',
    'action' => ['site/changebranch', 'save' => true],
    'enableClientValidation' => true,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
    ],
]);
echo $form->errorSummary($_userModel);
?>
<div class="form-group-attached">
  <?php if ($_userModel->canChangeCompany): ?>
  <div class="row">
    <div class="col-md-12">
      <?php
          echo $form->field($_userModel, 'CompanyId', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
              Select2::classname(),
              [
                  'data' => ArrayHelper::map(CmCompany::find()->All(), 'Id', 'Name'),
                  'options' => ['id' => $rand.'CompanyId', 'placeholder' => 'Select ...'],
              ]
          );
      ?>
    </div>
  </div>
  <?php endif; ?>
<?php if ($_userModel->canChangeBranch): ?>
  <div class="row">
    <div class="col-md-12">
      <?php
          echo $form->field($_userModel, 'BranchId', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
              'data' => ArrayHelper::map(CmCompanyBranch::find()->where(['CompanyId' => $_userModel->CompanyId])->All(), 'Id', 'Name'),
              'options' => ['id' => $rand.'BranchId', 'placeholder' => 'Select ...'],
              'type' => DepDrop::TYPE_SELECT2,
              'select2Options' => ['pluginOptions' => ['allowClear' => true]],
              'pluginOptions' => [
                  'allowClear' => false,
                  'depends' => [$rand.'CompanyId'],
                  'url' => Url::to(['/common/helper/loadbranch']),
                  'loadingText' => 'Loading ...',
              ],
          ]);
      ?>
    </div>
  </div>
<?php endif; ?>
<?php if ($_userModel->canChangeDept): ?>
  <div class="row">
      <div class="col-md-12">
      <?php
        echo $form->field($_userModel, 'DeptId', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
                'data' => ArrayHelper::map(CmDept::find()->asArray()->all(), 'Id', 'Departement'),
                'options' => ['id' => $rand.'DeptId', 'placeholder' => 'Select ...'],
            ]
        );
      ?>
    </div>
  </div>
<?php endif; ?>
</div>

<div class="row">
  <div class="col-md-12 m-t-10 text-right">
    <?= Html::submitButton('SAVE CHANGE', ['class' => 'btn btn-success  btn-lg']); ?>
    <button type="button" class="btn btn-info btn-cons p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end(); ?>
