<?php
	use yii\helpers\Html;
	use common\components\CommonHelper;
	use miloschuman\highcharts\Highcharts;
	use yii\web\JsExpression;

	$WorkHour = empty($data['summary_tr']['WorkHour']) ? 0 : $data['summary_tr']['WorkHour'];
	$Overtime = empty($data['summary_tr']['Overtime']) ? 0 : $data['summary_tr']['Overtime'];
	$Meals = empty($data['summary_tr']['Meals']) ? 0 : $data['summary_tr']['Meals'];
	$OutOfOffice = empty($data['summary_tr']['OutOfOffice']) ? 0 : $data['summary_tr']['OutOfOffice'];
	$Taxi = empty($data['summary_tr']['Taxi']) ? 0 : $data['summary_tr']['Taxi'];
	$TaxiAmount = empty($data['summary_tr']['TaxiAmount']) ? 0 : $data['summary_tr']['TaxiAmount'];

	$Planning = empty($data['summary_job']['Planning']) ? 0 : $data['summary_job']['Planning'];
	$FieldWork = empty($data['summary_job']['FieldWork']) ? 0 : $data['summary_job']['FieldWork'];
	$Reporting = empty($data['summary_job']['Reporting']) ? 0 : $data['summary_job']['Reporting'];
	$WarpUp = empty($data['summary_job']['WarpUp']) ? 0 : $data['summary_job']['WarpUp'];
	$Overtime = empty($data['summary_job']['Overtime']) ? 0 : $data['summary_job']['Overtime'];
	$TotalWH = empty($data['summary_job']['TotalWH']) ? 0 : $data['summary_job']['TotalWH'];


?>
<div class="row">
	<div class="col-md-6 text-md-left">
		<h3>DASHBOARD</h3>
	</div>
	<div class="col-md-6 text-md-right">
		<h3><?=date('l, d F Y')?></h3>
	</div>
</div>
<br/>
<!---
<div class="row">
	<div class="col-md-5">
		<div class="card card-default">
			<div class="card-body text-center no-padding">
				<div class="row">
					<div class="col-md-12 padding-10 fs-12">
						OUTSTANDING
					</div>
				</div>
				<div class="row b-t b-grey">
					<div class="col-md-6 padding-20 b-r b-grey">
						<a href='<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/all']); ?>'>
							TIME REPORT
							<h1 class="bold">50</h1>
						</a>
					</div>
					<div class="col-md-6 padding-20">
						<a href='<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/jobapproval/all']); ?>'>
							JOB APPROVAL
							<h1 class="bold">50</h1>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7">
	</div>
</div>
-->

<div class="row">
	<div class="col-md-12">
		<div class="card card-default">
			<div class="card-body">
				<?php
				echo Highcharts::widget([
					'options' => [
						'title' => ['text' => 'JOB PROJECT'],
						'xAxis' => [
							'categories' => ['JAN', 'FEB', 'MAR', 'APR' ,'MEI', 'JUN', 'JUL','AUG', 'SEPT', 'OCT' , 'NOV', 'DES']
						],
						'yAxis' => [
							'title' => ['text' => 'TOTAL JOB']
						],
						'series' => [
							['name' => 'DRAFT', 'data' => [1, 0, 4,1, 0, 4,1, 0, 4,1, 0, 4]],
							['name' => 'SUBMIT', 'data' => [2, 2, 3,2, 2, 3,2, 2, 3,2, 2, 3]],
							['name' => 'APPROVED', 'data' => [4, 3, 6,4, 3, 6,4, 3, 6,4, 3, 6]],
							['name' => 'REJECTED', 'data' => [6, 4, 5,6, 4, 5,6, 4, 5,6, 4, 5]],
							['name' => 'ON GOING', 'data' => [10, 5, 4,10, 5, 4,10, 5, 4,10, 5, 4]],
							['name' => 'FINISHED', 'data' => [11, 2, 3,11, 2, 3,11, 2, 3,11, 2, 3]],
							['name' => 'CLOSED', 'data' => [10, 3, 1,10, 3, 1,10, 3, 1,10, 3, 1]],
						]
					]
				]);
				?>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="card card-default">
			<div class="card-body">
				<?php
				echo Highcharts::widget([
	    'options' => [
	        'title' => ['text' => 'TOTAL CLAIM'],
	        'plotOptions' => [
	            'pie' => [
	                'cursor' => 'pointer',
	            ],
	        ],
	        'series' => [
	            [ // new opening bracket
	                'type' => 'pie',
	                'name' => 'TOTAL CLAIM',
	                'data' => [
	                    ['MEALS', 5],
	                    ['OUT OF OFFICE', 12],
	                    ['TAXI', 3],
	                ],
	            ] // new closing bracket
	        ],
	    ],
	]);
				?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card card-default">
			<div class="card-body">
				<?php
				echo Highcharts::widget([
			'options' => [
					'title' => ['text' => 'TOTAL WORK HOURS'],
					'xAxis' => [

						'categories' => ['PLANNING', 'FIELD WORK', 'REPORTING', 'WARP UP', 'OVERTIME']
					],
					'plotOptions' => [
							'pie' => [
									'cursor' => 'pointer',
							],
					],
					'series' => [
							[ // new opening bracket
									'type' => 'column',
									'name' => 'REMAINING',
									'data' => [
											['PLANNING', 6],
											['FIELD WORK', 20],
											['REPORTING', 3],
											['WARP UP', 4],
											['OVERTIME', 8]
									],
							], // new closing bracket
							[ // new opening bracket
									'type' => 'column',
									'name' => 'ACTUAL',
									'data' => [
											['PLANNING', 4],
											['FIELD WORK', 45],
											['REPORTING', 7],
											['WARP UP', 6],
											['OVERTIME', 22]
									],
							], // new closing bracket

							[ // new opening bracket
									'type' => 'column',
									'name' => 'BUDGETING',
									'data' => [
										['PLANNING', 10],
										['FIELD WORK', 70],
										['REPORTING', 10],
										['WARP UP', 10],
										['OVERTIME', 30]
									],
							] // new closing bracket
					],
			],
				]);
				?>
			</div>
		</div>
	</div>
</div>

<!-- <div class="row">
	<div class="col-md-6">
		<div class="card card-default">
			<div class="card-body">
				<span class="bold">SUMMARY</span>
				<h3>TIME REPORT</h3>
				<table class="table table-striped">
					<tr>
						<th class="col-md-7">DESCRIPTIONS</th>
						<th class="col-md-5 text-center">TOTAL</th>
					</tr>
					<tr>
						<td>WORKHOUR</td>
						<td class="text-right"><?=$WorkHour?> Hours</td>
					</tr>
					<tr>
						<td>OVERTIME</td>
						<td class="text-right"><?=$Overtime?> Hours</td>
					</tr>
					<tr>
						<td>MEALS</td>
						<td class="text-right"><?=$Meals?></td>
					</tr>
					<tr>
						<td>OUT OF OFFICE</td>
						<td class="text-right"><?=$OutOfOffice?></td>
					</tr>
					<tr>
						<td>TAXI</td>
						<td class="text-right"><?=$Taxi;?> VOUCHER</td>
					</tr>
					<tr>
						<td>TAXI AMOUNT</td>
						<td class="text-right">Rp. <?=number_format($TaxiAmount, 0);?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card card-default">
			<div class="card-body">
				<span class="bold">SUMMARY</span>
				<h3>JOB BUDGETING</h3>
				<table class="table table-striped">
					<tr>
						<th class="col-md-7">DESCRIPTIONS</th>
						<th class="col-md-5 text-right">TOTAL AVAILABLE</th>
					</tr>
					<tr>
						<td>PLANNING</td>
						<td class="text-right"><?=$Planning?> Hours</td>
					</tr>
					<tr>
						<td>FIELD WORK</td>
						<td class="text-right"><?=$FieldWork?> Hours</td>
					</tr>
					<tr>
						<td>REPORTING</td>
						<td class="text-right"><?=$Reporting?> Hours</td>
					</tr>
					<tr>
						<td>WARP UP</td>
						<td class="text-right"><?=$WarpUp?> Hours</td>
					</tr>
					<tr>
						<td>OVERTIME</td>
						<td class="text-right"><?=$Overtime?> Hours</td>
					</tr>
					<tr>
						<td class="bold">TOTAL WH</td>
						<td class="bold text-right"><?=$TotalWH?> Hours</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div> -->
<div class="row">
	<div class="col-md-12">
		<div class="card card-default">
			<div class="card-body">
				<span class="bold">ON GOING</span>
				<h3>JOB </h3>
				<table class="table table-striped">
					<tr>
						<th>NO.</th>
						<th class="col-md-2">JOB-CODE</th>
						<th class="col-md-3">CLIENT</th>
						<th class="col-md-3">DESCRIPTIONS</th>
						<th class="col-md-1 text-center">TOTAL WH</th>
						<th class="col-md-1 text-center">REMAINING WH</th>
						<th class="col-md-1 text-center">UNREAD COMMENTS</th>
					</tr>
					<tr>
						<th>1</th>
						<th class="col-md-2">KKN-001</th>
						<th class="col-md-3">KANTOR KEJAKSAAN NEGARA</th>
						<th class="col-md-3">GENERAL AUDIT</th>
						<th class="col-md-1 text-center">100</th>
						<th class="col-md-1 text-center">30</th>
						<th class="col-md-1 text-center">5</th>
					</tr>
					<tr>
						<th>1</th>
						<th class="col-md-2">CNA-001</th>
						<th class="col-md-3">CENTRALNASIONAL AMARIS </th>
						<th class="col-md-3">GENERAL AUDIT 2019</th>
						<th class="col-md-1 text-center">300</th>
						<th class="col-md-1 text-center">80</th>
						<th class="col-md-1 text-center">10</th>
					</tr>
					<tr>
						<th>1</th>
						<th class="col-md-2">KKN-002</th>
						<th class="col-md-3">KANTOR KEJAKSAAN NEGARA</th>
						<th class="col-md-3">GENERAL AUDIT</th>
						<th class="col-md-1 text-center">200</th>
						<th class="col-md-1 text-center">30</th>
						<th class="col-md-1 text-center">7</th>
					</tr>
					<tr>
						<th>1</th>
						<th class="col-md-2">SI-001</th>
						<th class="col-md-3">SURVEYOR INDONESIA</th>
						<th class="col-md-3">GENERAL AUDIT 2019</th>
						<th class="col-md-1 text-center">150</th>
						<th class="col-md-1 text-center">50</th>
						<th class="col-md-1 text-center">15</th>
					</tr>
					<tr>
						<th>1</th>
						<th class="col-md-2">BKPM-001</th>
						<th class="col-md-3">BADAN KOORDINASI PENANAMAN MODAL</th>
						<th class="col-md-3">GENERAL AUDIT 2019</th>
						<th class="col-md-1 text-center">600</th>
						<th class="col-md-1 text-center">400</th>
						<th class="col-md-1 text-center">30</th>
					</tr>
					<tr>
						<th>1</th>
						<th class="col-md-2">KKN-001</th>
						<th class="col-md-3">KANTOR KEJAKSAAN NEGARA</th>
						<th class="col-md-3">GENERAL AUDIT</th>
						<th class="col-md-1 text-center">100</th>
						<th class="col-md-1 text-center">30</th>
						<th class="col-md-1 text-center">5</th>
					</tr>
					<?php
						if(!empty($data['summary_joblist'])){
							$i = 0;
							foreach($data['summary_joblist'] as $jl){
								$i++;
								echo "
									<tr>
										<td>".$i.".</td>
										<td>".$jl['Description']."</td>
										<td class='text-right'>".$jl['Total']." Hours</td>
										<td class=' text-center'>
											<a href='".Yii::$app->urlManager->createAbsoluteUrl(['tr/job/request', 'id'=>$jl['JobID']])."' class='badge badge-success'>0 Comments</a>
										</td>
									</tr>
								";
							}
						}else{
							echo "<tr><td colspan='4'>There are no jobs assigned to you</td></tr>";
						}

					?>
				</table>
			</div>
		</div>
	</div>
</div>
