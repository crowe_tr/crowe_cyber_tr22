<?php
	use yii\helpers\Html;
	use common\components\CommonHelper;
	use miloschuman\highcharts\Highcharts;
	use yii\web\JsExpression;

	$WorkHour = empty($data['summary_tr']['WorkHour']) ? 0 : $data['summary_tr']['WorkHour'];
	$Overtime = empty($data['summary_tr']['Overtime']) ? 0 : $data['summary_tr']['Overtime'];
	$Meals = empty($data['summary_tr']['Meals']) ? 0 : $data['summary_tr']['Meals'];
	$OutOfOffice = empty($data['summary_tr']['OutOfOffice']) ? 0 : $data['summary_tr']['OutOfOffice'];
	$Taxi = empty($data['summary_tr']['Taxi']) ? 0 : $data['summary_tr']['Taxi'];
	$TaxiAmount = empty($data['summary_tr']['TaxiAmount']) ? 0 : $data['summary_tr']['TaxiAmount'];

	$Planning = empty($data['summary_job']['Planning']) ? 0 : $data['summary_job']['Planning'];
	$FieldWork = empty($data['summary_job']['FieldWork']) ? 0 : $data['summary_job']['FieldWork'];
	$Reporting = empty($data['summary_job']['Reporting']) ? 0 : $data['summary_job']['Reporting'];
	$WarpUp = empty($data['summary_job']['WarpUp']) ? 0 : $data['summary_job']['WarpUp'];
	$Overtime = empty($data['summary_job']['Overtime']) ? 0 : $data['summary_job']['Overtime'];
	$TotalWH = empty($data['summary_job']['TotalWH']) ? 0 : $data['summary_job']['TotalWH'];


?>
<div class="row">
	<div class="col-md-6 text-md-left">
		<h3>DASHBOARD</h3>
	</div>
	<div class="col-md-6 text-md-right">
		<h3><?=date('l, d F Y')?></h3>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
			<img src="<?php echo $this->theme->baseUrl; ?>/assets/img/crowe_indonesia.png" class="bg-img-cs-al" >
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card card-default">
			<div class="card-body">
				<span class="bold">ON GOING</span>
				<h3>JOB </h3>
				<table class="table table-striped">
					<?php
						if(!empty($data['summary_joblist'])){
							$i = 0;
							foreach($data['summary_joblist'] as $jl){
								$i++;
								echo "
									<tr>
										<td>".$i.".</td>
										<td>".$jl['Description']."</td>
										<td class='text-right'>".$jl['Total']." Hours</td>
										<td class=' text-center'>
											<a href='".Yii::$app->urlManager->createAbsoluteUrl(['tr/job/request', 'id'=>$jl['JobID']])."' class='badge badge-success'>0 Comments</a>
										</td>
									</tr>
								";
							}
						}else{
							echo "<tr><td colspan='4'>There are no jobs assigned to you</td></tr>";
						}

					?>
				</table>
			</div>
		</div>
	</div>
</div>
<br/>
