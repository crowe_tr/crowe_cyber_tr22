<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$rand = rand();
$form = ActiveForm::begin([
    'id' => $rand.'-form',
    'action' => ['site/changepasswordform', 'save' => true],
    'enableClientValidation' => true,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ], 
    ],
]);

?>

<div class="modal-dialog">
  <div class="modal-content no-border" style="max-width: 640px;">
    <div class="modal-header clearfix text-left bg-success p-t-20 p-b-10 p-l-20 p-r-20">
      CHANGE PASSWORD
    </div>

    <div class="modal-body">
     <?= $form->errorSummary($_userModel); ?>
      <div class="row">
        <div class="col-md-12 m-t-10">
          <div class="form-group-attached">
            <div class="row">
              <div class="col-md-6">
                <?= $form->field($_userModel, 'passwordHash')->passwordInput(['maxlength' => true]) ?>
              </div>
              <div class="col-md-6">
                <?= $form->field($_userModel, 'passwordRepeat')->passwordInput(['maxlength' => true]) ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 m-t-10">
          <span class='alert-warning no-margin'>
              Note : After you have successfully changed your password you will be directed to the login page
          </span>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 m-t-10 text-right">
          <?= Html::submitButton('SAVE CHANGE', ['class' => 'btn btn-success pull-right']); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>
