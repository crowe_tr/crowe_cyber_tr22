<?php 
	use yii\helpers\Html; 
	$_base_url = Yii::$app->homeUrl . Yii::$app->params['hrEmployeeBaseurl'];
?>
	 
<div class="card share full-width no-margin no-border" style="border-radius: 0 !important">
	<hr/>
	<div class="card-header clearfix no-border p-t-0 p-b-0">
		<div class="user-pic">
			<img src="<?= $_base_url . $model->creator->PHOTO ?>" width="40" height="40">				
		</div>
		<h5><?= !empty($model->creator->NAME) ? $model->creator->NAME : '<i>not set</i>'; ?></h5>
		<h6><small class="fs-12 hint-text">On <?= date('H:i:s - D, d M Y', strtotime($model->DATE_INPUT)); ?></small></h6>
	</div>
	<div class="card-description p-l-55">
		<?= $model->DESCRIPTION; ?>
	</div>
</div>
