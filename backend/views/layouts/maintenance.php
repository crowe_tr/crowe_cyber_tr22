<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<?= Html::csrfMetaTags() ?>
	<title>
		Crowe | <?= Html::encode($this->title) ?>
	</title>
	<?php $this->head() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
	<link rel="apple-touch-icon" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/ico/60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/ico/76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/ico/120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/ico/152.png">
	<link rel="icon" type="image/x-icon" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/favicon.ico" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php echo $this->theme->baseurl; ?>/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
	<link class="main-stylesheet" href="<?php echo $this->theme->baseurl; ?>/pages/css/themes/light.css" rel="stylesheet" type="text/css" />
	<link class="main-stylesheet" href="<?php echo $this->theme->baseurl; ?>/assets/css/style.css" rel="stylesheet" type="text/css" />

	<!--[if lte IE 9]>
		<link href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/css/ie9.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<script type="text/javascript">
		window.onload = function() {
			// fix for windows 8
			if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
				document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/css/windows.chrome.fix.css" />'
		}
	</script>
</head>

<body>
	<div class="row">
		<div class="col-md-4 offset-md-4">
			<img src="<?php echo $this->theme->baseUrl; ?>/assets/img/crowe-logo.svg" width="128" height="62">
			<br />
			<h1 class="text-center">MAINTENANCE</h1>
			<?= $content ?>
			<div class="row">
				<div class="col-md-12 padding-20 text-center">
					<p>
						<span class="hint-text">Copyright © 2019</span>
						<span class="font-montserrat"><b>Crowe</b></span>.
						<span class="hint-text">All rights reserved. </span> <br />
					</p>
				</div>
			</div>
		</div>
	</div>


	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->theme->baseurl; ?>/assets/plugins/bootstrap-select2/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->theme->baseurl; ?>/assets/plugins/classie/classie.js"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/pages/js/pages.min.js"></script>

</html>
<?php $this->endPage() ?>