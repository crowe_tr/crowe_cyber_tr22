<?php
	use yii\helpers\Html;
	use yii\widgets\Breadcrumbs;
	use common\widgets\Alert;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<?= Html::csrfMetaTags() ?>
	<title>
		Crowe | <?= Html::encode($this->title) ?>
	</title>
	<?php $this->head() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
	<link rel="apple-touch-icon" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/ico/60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/ico/76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/ico/120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/ico/152.png">
	<link rel="icon" type="image/x-icon" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/favicon.ico" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php echo $this->theme->baseurl; ?>/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php echo $this->theme->baseurl; ?>/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
	<link class="main-stylesheet" href="<?php echo $this->theme->baseurl; ?>/pages/css/themes/light.css" rel="stylesheet" type="text/css" />
	<link class="main-stylesheet" href="<?php echo $this->theme->baseurl; ?>/assets/css/style.css" rel="stylesheet" type="text/css" />

	<!--[if lte IE 9]>
		<link href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/css/ie9.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<script type="text/javascript">
		window.onload = function() {
			// fix for windows 8
			if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
				document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?php echo $this->theme->baseurl; ?>/<?php echo $this->theme->baseurl; ?>/pages/css/windows.chrome.fix.css" />'
		}
	</script>
</head>

<body class="">
	<div class="login-wrapper  ">
		<div class="bg-pic" style="background: transparent url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPwAAADmCAYAAADx5uiaAAAIeElEQVR4nO3cQW4DRwwEwFEk2c7/P5xD4A94ugESKgJ9XcySrJFOe845z3POu5B/wnmecx7hvAr50s94PxvPTb/7szCjxh4BHw7wwAMfyKYF1U/ggb/MpgXVT+CBv8ymBdVP4IG/zKYF1U/ggb/MpgXVT+CBv8ymBdVP4IG/zKYF1U/ggb/MpgXVT+CBv8ymBdVP4IG/zKYF1U/ggb/MpgXVT+CBv8ymBdVP4IG/zKYF1U/ggb/MpgXVT+CBv8ymBdVP4IG/zKYF1U/ggb/MpgXVT+DHgv8dfjKv0jPTeX/wM1+nc4l86ty3XKCVagy/cdu1ln7D4Bvv/jj5ehTO2ejls/Tu6VQKeOBTBTzwwIcCPPCRAh74VAEPPPChAA98pIAHPlXAAw98KMADHynggU8V8MADHwrwwEcKeOBTBTzwwIcCPPCRAh74VAEPPPChAA98pIAHPlXAAw98KMADHynggU8V8MADHwrwwEcKeOBTBTzwwIcCPPCRAh74VAEfBt94aKOh79Iz0xdTA9KWfv4ufrqfjWc2Ls90NWa/5gZtQUovU+MX/rlkRq1PdG+YUePfTeNyAr4Q4IFPFPCFc25YJuDnzwj4QoCfPSPgswV84Zwblgn4+TMCvhDgZ88I+GwBXzjnhmUCfv6MgC8E+NkzAj5bwBfOuWGZgJ8/I+ALAX72jIDPFvCFc25YJuDnzwj4QoCfPSPgswV84Zwblgn4+TMCvhDgZ88I+GwBXzjnhmUCfv6MgC8E+NkzAj5bwBfOuWGZgJ8/I+ALAX72jIDPFvCFc25YJuDnzwj4QoCfPSPgs1UB33joV+GZLfAb3v2rkO+Th/ldOueGGb1O56JPn/E8C4N/nzzOxq3cAp/u5c/5f/HT2XAxtS7QT9154MMBHvjJOw98OMADP3nngQ8HeOAn7zzw4QAP/OSdBz4c4IGfvPPAhwM88JN3HvhwgAd+8s4DHw7wwE/eeeDDAR74yTsPfDjAAz9554EPB3jgJ+888OEAD/zknQc+HOCBn7zzwIcDPPCTdx74cIAHfvLOAx8O8MBP3nngwwEe+Mk7D3w4wAM/eeeBDwd44CfvfO0jlul8FwbUeGYD57+FZ/6UZtR4Zno/30ue+Tr5S+Q8Cg/9HdSGBW1cIhuWvvEr13hmY+m3/FsEPhzggQc+EOCBBx544C/OCTzwwF8GeOCBDwR44IEHHviLcwIPPPCXAR544AMBHnjggQf+4pzAAw/8ZYAHHvhAgAceeOCBvzgn8MADfxnggQc+EOCBBx544C/OCTzwwF8GeOCBDwR44IEHHviLcwIPPPCXAR74jwP/LDz0d/jJND4O+XN2LP3X6YBv5BnOq/TMDXkXnnleJ3/btS6RDZA+uZ/Pk6/GP9AW0A3/Gj56QYEHfvLcgQ8HeOAnzx34cIAHfvLcgQ8HeOAnzx34cIAHfvLcgQ8HeOAnzx34cIAHfvLcgQ8HeOAnzx34cIAHfvLcgQ8HeOAnzx34cIAHfvLcgQ8HeOAnzx34cIAHfvLcgQ8HeOAnzx34cIAHfvLcgQ8HeOAnzx34cIAHfvLcgQ8HeOAnzx34cIAHfvLcK+DfhYdu+SpoC/yGBW29e/qDk43LrvHeDUfpj7Z+n+LQ05W+QH5/ORtfRN2woJvO+an/mBpfagY+nE2QtpwTeOCBD2TLOYEHHvhAtpwTeOCBD2TLOYEHHvhAtpwTeOCBD2TLOYEHHvhAtpwTeOCBD2TLOYEHHvhAtpwTeOCBD2TLOYEHHvhAtpwTeOCBD2TLOYEHHvhAtpwTeOCBD2TLOYEHHvhAtpwTeOCBD2TLOYEHHvhAtpwTeOCBD2TLOYEHHvhAtpwT+CD4xtK3kh5S65npNJbpXXj3xhm3fGzzXTjjTyHncfL1+0s3/Rep8e6Pwjm3/HK+zg7w79O5mNP9/H3/ZIAPF/DAAx8K8MADDzzwfwzwwEcKeOCBBx74PwZ44IEPBHjggQ8U8MADDzzwfwzwwAMfCPDAAx8o4IEHHnjg/xjggQc+EOCBBz5QwAMPPPDA/zHAAw98IMADD3yggAceeOCB/2OABx74QIAHHvhAAQ888EPBNwbfAP8uPPNVGvwG8I1+fp0d4Btzb/UzDv598gvauO0aQ28tU+MCTc8oDbOJUz9zAb4QC6qfU/sJfCEWVD+n9hP4Qiyofk7tJ/CFWFD9nNpP4AuxoPo5tZ/AF2JB9XNqP4EvxILq59R+Al+IBdXPqf0EvhALqp9T+wl8IRZUP6f2E/hCLKh+Tu0n8IVYUP2c2k/gC7Gg+jm1n8AXYkH1c2o/gS/Egurn1H4CX4gF1c+p/QS+EAuqn1P7CXwhFlQ/p/YT+EIsqH5O7ed5l4bUuETSabx3o5+ffoGmd2nLu1fAN16+MaTHydejcM5GL5+ld2/8cjYu5Q0zapyz8U9kzcsDn3934GfvPPDhAh74yTsPfLiAB37yzgMfLuCBn7zzwIcLeOAn7zzw4QIe+Mk7D3y4gAd+8s4DHy7ggZ+888CHC3jgJ+888OECHvjJOw98uIAHfvLOAx8u4IGfvPPAhwt44CfvPPDhAh74yTsPfLiAB37yzgMfLuCBn7zzwIcLeOAn7zzw4QIe+Mk7D3y4gAd+8s6vAf91Ol/xbCxT45mNRUpX83JKJ30xNd57y8dgK7fIVyHfZ8cl0ljQxr+b5uWUzCf/s3udwh4BDzzwwAN/EeCBB/4ywGcLeOCBDwV44IG/DPDZAh544EMBHnjgLwN8toAHHvhQgAce+MsAny3ggQc+FOCBB/4ywGcLeOCBDwV44IG/DPDZAh544EMBHnjgLwN8toAHHvhQgAce+MsAny3ggQc+FOCBB/4ywGcL+A8F38D5s+SZ34UBfS0ZfAP8u/TujUt5A/h4P/8DtNSYO4YFB0IAAAAASUVORK5CYII=') repeat scroll 0% 0%">
			<div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
			<h3 class="semi-bold text-white">Crowe - Time Report</h3>
			<p class="small">
				All work copyright of respective owner, otherwise © 2019 Crowe Indonesia.
			</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-8 col-sm-5 col-xs-0 left-img-pd bg-new-login">
					<img src="<?php echo $this->theme->baseUrl; ?>/assets/img/crowe_indonesia.png" class="bg-img-cs" >
			</div>
			<div class="col-md-4 col-sm-7 col-xs-12 bg-white padding-40">
				<img src="<?php echo $this->theme->baseUrl; ?>/assets/img/crowe-logo.svg" width="128" height="62">
				<br/>
				<div class="row">
					<div class="col-md-12 padding-20">
						<?= $content ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 padding-20 text-center">
						<p>
							<span class="hint-text">Copyright © 2019</span>
							<span class="font-montserrat"><b>Crowe</b></span>.
							<span class="hint-text">All rights reserved. </span> <br/>
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-0 col-xs-0 bg-white">

			</div>
		</div>
	</div>


	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->theme->baseurl; ?>/assets/plugins/bootstrap-select2/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->theme->baseurl; ?>/assets/plugins/classie/classie.js"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseurl; ?>/pages/js/pages.min.js"></script>
</html>
<?php $this->endPage() ?>
