<?php

use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
	<meta charset="<?= Yii::$app->charset ?>" />
	<?= Html::csrfMetaTags() ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	<title>
		Crowe
		<?= !empty($this->title) ? ' | ' . Html::encode(ucfirst($this->title)) : ''; ?>
	</title>
	<?php $this->head() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
	<link rel="apple-touch-icon" href="<?php echo $this->theme->baseUrl; ?>/pages/ico/60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->theme->baseUrl; ?>/pages/ico/76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->theme->baseUrl; ?>/pages/ico/120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->theme->baseUrl; ?>/pages/ico/152.png">
	<link rel="icon" type="image/x-icon" href="<?php echo $this->theme->baseUrl; ?>/favicon.ico" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->theme->baseUrl; ?>/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
	<link class="main-stylesheet" href="<?php echo $this->theme->baseUrl; ?>/pages/css/themes/light.css" rel="stylesheet" type="text/css" />
	<link class="main-stylesheet" href="<?php echo $this->theme->baseUrl; ?>/assets/css/style.css" rel="stylesheet" type="text/css" />

	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
</head>

<body>
	<?php $this->beginBody(); ?>

	<?php echo $this->render('navbar'); ?>
	<?php echo $this->render('headersub'); ?>
	<div class="modal fade" id="AppProfile" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content no-border" style="max-width: 920px">
				<div class="modal-header clearfix text-left bg-success p-t-20 p-b-10 p-l-20 p-r-20">
					<p class="text-white all-caps">
						<b>PROFILE SETTING</b>
					</p>
				</div>
				<div id="AppProfile-detail" class="modal-body padding-20">

				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="AppChangePassword" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content no-border" style="max-width: 640px">
				<div class="modal-header clearfix text-left bg-success p-t-20 p-b-10 p-l-20 p-r-20">
					<p class="text-white all-caps">
						<b>CHANGE PASSWORD</b>
					</p>
				</div>
				<div id="AppChangePassword-detail" class="modal-body padding-20">

				</div>
			</div>
		</div>
	</div>

	<div class="page-container ">

		<div id="full-loading" style="
					position: fixed;
					top: 0;
					left: 0;
					width: 100%;
					height: 100%;
					z-index: 9999;
					background: rgba(244,244,244,.8);
					text-align: center;
					padding-top: 15%;
					display: none;
				">
			<img src="<?php echo $this->theme->baseUrl; ?>/assets/img/loading.svg" />
		</div>
		<div id="notif"></div>

		<div class="page-content-wrapper ">
			<div class="content p-t-10">
				<div class="container-fluid container-fixed-lg">
					<div class="row">
						<div class="col-md-7">
							<?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 'homeLink' => false]); ?>
						</div>
						<div class="col-md-5 text-right p-t-15">
							<?= isset($this->params['breadcrumbs_btn']) ? $this->params['breadcrumbs_btn'] : ""; ?>
						</div>
					</div>

					<?= $content ?>
				</div>
			</div>
			<?php echo $this->render('footersub'); ?>
		</div>
		<?php if (!empty($this->params['alerts'])) : ?>
			<div class="alt-alert pgn-wrapper" data-position="top-right">
				<div class="pgn pgn-flip">
					<div class="alert <?= $this->params['alerts']['class'] ?>" role="alert">
						<p class="pull-left alt-alert-header"><?= $this->params['alerts']['header'] ?></p>
						<button class="close" data-dismiss="alert"></button>
						<div class="clearfix"></div>
						<div class="alt-alert-body">
							<?= $this->params['alerts']['body'] ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/feather-icons/feather.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/popper/umd/popper.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/select2/js/select2.full.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/classie/classie.js"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/pages/js/pages.js"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/js/scripts.js" type="text/javascript"></script>
	<script src="<?php echo $this->theme->baseUrl; ?>/assets/js/custom.js" type="text/javascript"></script>

	<?php
	if (isset($this->params['crud_ajax'])) :
		$js = "var paramJs = (paramJs || {});";
		$js .= "paramJs.urlFormShowModal = '" . Yii::$app->urlManager->createAbsoluteUrl([Url::current(['form'])]) . "';";
		$js .= "paramJs.urlFormSave = '" . Yii::$app->urlManager->createAbsoluteUrl([Url::current(['save'])]) . "';";
		$this->registerjs($js, View::POS_END);

		$fileCrudAjax = $this->theme->baseUrl . '/assets/js/crud-pjax.js';
		$this->registerJsFile($fileCrudAjax, ['position' => View::POS_END, 'depends' => [yii\web\JqueryAsset::className()]], $fileCrudAjax);
	endif;
	?>
	<script type="text/javascript">
		var paramJs = (paramJs || {});
		paramJs.urlAppProfile = '<?= Yii::$app->urlManager->createAbsoluteUrl(['site/profile']); ?>';
		paramJs.urlAppChangeBranch = '<?= Yii::$app->urlManager->createAbsoluteUrl(['site/changebranch']); ?>';
		paramJs.urlAppChangePassword = '<?= Yii::$app->urlManager->createAbsoluteUrl(['site/changepassword']); ?>';

		function showFullLoading() {
			$('#full-loading').show();
		};

		function hideFullLoading() {
			$('#full-loading').hide();
		};

		function notif(text = "") {
			$('#notif').html("" +
				"<div class='pgn-wrapper' data-position='top-right' style='top: 59px;'> \n" +
				"<div class='alert alert-info'> \n" +
				"<button type='button' class='close' data-dismiss='alert'> \n" +
				"<span aria-hidden='true'></span><span class='sr-only'>Close</span> \n" +
				"</button> \n" +
				text + "\n" +
				"</div> \n" +
				"</div> \n" +
				"</div> \n"
			);

		}

		function AppProfileModal(link = "") {
			var link = (link || paramJs.urlAppProfile);
			$.ajax({
				url: link,
				method: "POST",
				dataType: 'html',
				success: function(data) {
					$('#AppProfile-detail').html(data);
				},
			});
		}

		function AppProfile() {
			AppProfileModal();
			$('#AppProfile').modal('show');
		}

		function AppChangePasswordModal(link = "") {
			var link = (link || paramJs.urlAppChangePassword);
			$.ajax({
				url: link,
				method: "POST",
				dataType: 'html',
				success: function(data) {
					$('#AppChangePassword-detail').html(data);
				},
			});
		}

		function AppChangePassword() {
			AppChangePasswordModal();
			$('#AppChangePassword').modal('show');
		}
		$('input[type=text]').val(function() {
			return this.value.toUpperCase();
		})
	</script>
	<?php $this->endBody() ?>

</body>

</html>
<?php $this->endPage() ?>