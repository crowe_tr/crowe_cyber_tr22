<?php
	use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK
  use yii\helpers\Html;
  $user = CommonHelper::getUserIndentity();
?>
<div class="header" style="border-bottom: thin solid #f2f2f2">
  <!-- START MOBILE SIDEBAR TOGGLE -->
  <a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-menu" data-toggle="sidebar">
  </a>
  <!-- END MOBILE SIDEBAR TOGGLE -->
  <div class="">
    <div class="brand inline">
      <img src="<?php echo $this->theme->baseUrl; ?>/assets/img/crowe-logo.png" alt="logo" width="98" height="28">
    </div>
  </div>
  <div class="d-flex align-items-center">
    <!-- START User Info-->
    <div class="pull-left p-r-10 fs-14 font-heading d-lg-inline-block d-none m-l-20">
      <span class="b-r b-grey p-r-10 m-r-10">
        <?= empty($user->entity->entityName) ? "" : $user->entity->entityName; ?>
        <?= empty($user->division->divName) ? "" : " - ".$user->division->divName; ?>
        <?= empty($user->dept->deptName) ? "" : " - ".$user->dept->deptName; ?>
		  </span>
        Login as <span class="semi-bold"><?=
				$user->full_name
				// var_dump($user);
				// die();
				?></span>
    </div>
    <div class="dropdown pull-right d-lg-inline-block d-none">
      <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="thumbnail-wrapper d32 inline">
          <?=
            Html::img(
                $user->photo,
                //$this->theme->baseUrl.'/../sources/user.png',
                [
                    'class' => 'img img-rounded',
                    'alt' => 'logo',
                    'height' => '32',
                    'width' => '32',
                    'style'=>'border-radius: 8px !important'

                ]
            )
          ?>
        </span>
      </button>
      <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
        <?php
          //echo Html::a('Profile Setting', '#', ['class' => 'dropdown-item', 'onclick' => 'AppProfile()']);
          echo Html::a('Change Password', '#', ['class' => 'dropdown-item', 'onclick' => 'AppChangePassword()']);
          echo Html::a('
            <span class="pull-left">Logout</span>
            <span class="pull-right"><i class="pg-power text-danger"></i></span>',
            ['/site/logout'],
            ['class' => 'clearfix bg-master-lighter dropdown-item']
          );
       ?>

      </div>
    </div>
    <!-- END User Info-->
  </div>
</div>
