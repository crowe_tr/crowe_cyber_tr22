<div class=" container-fluid  container-fixed-lg footer">
    <div class="copyright sm-text-center">
        <div class="row">
            <div class="col">

                <p class="small no-margin pull-left sm-pull-reset">
                    <span class="hint-text">Copyright &copy; 2019 </span>
                    <span class="font-montserrat"> Crowe </span>.
                    <span class="hint-text">All rights reserved. </span>
                </p>
                <div class="clearfix"></div>
            </div>
            <?php
            if (Yii::$app->controller->id == "site") {
            ?>
                <div class="col-auto">
                    <div id="histats_counter"></div>

                    <script type="text/javascript">
                        var _Hasync = _Hasync || [];
                        _Hasync.push(['Histats.start', '1,4544463,4,225,112,50,00010001']);
                        _Hasync.push(['Histats.fasi', '1']);
                        _Hasync.push(['Histats.track_hits', '']);
                        (function() {
                            var hs = document.createElement('script');
                            hs.type = 'text/javascript';
                            hs.async = true;
                            hs.src = ('//s10.histats.com/js15_as.js');
                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
                        })();
                    </script>
                    <noscript><a href="/" target="_blank"><img src="//sstatic1.histats.com/0.gif?4544463&101" alt="free hit counter" border="0"></a></noscript>
                </div>
            <?php
            }
            ?>
        </div>

    </div>
</div>