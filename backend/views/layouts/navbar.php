<?php
  use mdm\admin\components\MenuHelper;
  use yii\bootstrap\Nav;
  use common\components\CommonHelper;
?> 
<!-- BEGIN SIDEBPANEL-->
<nav class="page-sidebar" data-pages="sidebar">
  <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
  <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
  <!-- BEGIN SIDEBAR MENU HEADER-->
  <div class="sidebar-header">
    <img src="<?php echo $this->theme->baseUrl; ?>/assets/img/crowe-logo.svg" alt="logo" class="brand" width="98" height="42">
  </div>
  <!-- END SIDEBAR MENU HEADER-->
  <!-- START SIDEBAR MENU -->
  <div class="sidebar-menu">
    <?php
      $session = Yii::$app->session;
      if (!isset($session['_ant_crowe_leftmenu'])) {
          $callback = function ($menu) {
              return [
                'label' => $menu['name'],
                'url' => $menu['route'],
                'data' => json_decode($menu['data'], true),
                'items' => $menu['children'],
              ];
          };
          $menus = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback, true);
          $html_menu = '';
          $left_menu = CommonHelper::CreateLeftMenuBasedOnMDM($menus, $html_menu, 1, 1, '', 'tr');

          $session->set('_ant_crowe_leftmenu', $left_menu);
          echo $session['_ant_crowe_leftmenu'];
          
      } else {
          echo $session['_ant_crowe_leftmenu'];
      }
  ?>

    <div class="clearfix"></div>
  </div>
</nav>
<!-- END SIDEBAR -->