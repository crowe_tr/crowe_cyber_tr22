<?php

namespace backend\modules\hr\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;

use common\models\hr\Employee;

class HelperController extends Controller
{
    public $layout;
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionLoadattleavetype()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = HrAttLeaveType::find()->andWhere(['GroupCode' => $id])->asArray()->all();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['Code'], 'name' => $ls['Type']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);

                return;
            }
        }
        echo json_encode(['output' => array(), 'selected' => '']);
    }
    public function actionLoadbranch($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        $model = CmCompany::find()
                ->select('Id as id, Name as text')
                ->where(
                  "Name LIKE '%{$q}%' OR Id = '%{$q}%'"
                )
                ->limit(10)
                ->asArray()
                ->all();
        $out['results'] = array();
        foreach ($model as $data) {
            $name = $data['text'];

            $array = array('id' => $data['id'], 'text' => $name);
            array_push($out['results'], $array);
        }

        return $out;
    }

    public function actionSrcleader($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        $model = Employee::find()
            ->select('user_id as id, full_name as text, guid')
            ->where(
                "(full_name LIKE '%{$q}%' OR user_id = '%{$q}%') AND user_id != '{$id}' AND user_id != '0'"
            )
            ->limit(10)
            ->asArray()
            ->all();
        $out['results'] = array();
        foreach ($model as $data) {
            $name = $data['text'];

            $array = array('id' => $data['guid'], 'text' => $data['id'] . " - " . $name);
            array_push($out['results'], $array);
        }

        return $out;
    }
    public function actionSrcemployee($q = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        $model = Employee::find()
            ->select('Id as id, fullName as text, guid')
            ->where(
                "(fullName LIKE '%{$q}%' OR Id = '%{$q}%') AND Id != '0'"
            )
            ->limit(6)
            ->asArray()
            ->all();
        $out['results'] = array();
        foreach ($model as $data) {
            $name = $data['text'];

            $array = array('id' => $data['id'], 'text' => $data['id'] . " - " . $name);
            array_push($out['results'], $array);
        }

        return $out;
    }

    public function actionSrcinitial($q = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        $model = Employee::find()
            ->select('initial as id, fullName as text')
            ->where(
                "(fullName LIKE '%{$q}%' OR initial LIKE '%{$q}%') AND id != '0' AND initial != ''"
            )
            ->limit(40)
            ->asArray()
            ->orderby('initial ASC')
            ->all();
        $out['results'] = array();
        foreach ($model as $data) {
            $name = $data['text'];

            $array = array('id' => $data['id'], 'text' => $data['id'] . " - " . $name);
            array_push($out['results'], $array);
        }

        return $out;
    }



}
