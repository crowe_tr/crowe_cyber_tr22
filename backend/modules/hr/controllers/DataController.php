<?php

namespace backend\modules\hr\controllers;

use Yii;
use yii\web\Controller;

class DataController extends Controller
{
  public static function data_religion()
  {
    return [
            'MOSLEM'    => 'MOSLEM',
            'CATHOLIC'  => 'CATHOLIC',
            'CHRISTIAN' => 'CHRISTIAN',
            'HINDU'     => 'HINDU',
            'BUDHA'     => 'BUDHA',
            'KHONGHUCU' => 'KHONGHUCU',
          ];
  }

  public static function data_marital()
  {
    return  [
              'SINGLE' => 'SINGLE',
              'MARRIED' => 'MARRIED',
              'DIVORCE' => 'DIVORCE'
            ];
  }

  public static function data_national()
  {
    return  [
              'INDONESIA'  =>  'INDONESIA',
              'BRUNEI DARUSSALAM'  =>  'BRUNEI DARUSSALAM',
              'PHILIPPINES'  =>  'PHILIPPINES',
              'CAMBODIA'  =>  'CAMBODIA',
              'LAOS'  =>  'LAOS',
              'MALAYSIA'  =>  'MALAYSIA',
              'MYANMAR'  =>  'MYANMAR',
              'SINGAPORE'  =>  'SINGAPORE',
              'THAILAND'  =>  'THAILAND',
              'TIMOR LESTE'  =>  'TIMOR LESTE',
              'VIETNAM'  =>  'VIETNAM',
              'OTHER' =>  'OTHER',
            ];
  }

  public static function data_edu()
  {
    return  [
              'SD' => 'SD',
              'SMP' => 'SMP',
              'SMA' => 'SMA',
              'SMK' => 'SMK',
              'D3' => 'D3',
              'S1' => 'S1',
              'S2' => 'S2',
              'S3' => 'S3',
            ];
  }

}
