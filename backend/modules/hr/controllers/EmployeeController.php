<?php

namespace backend\modules\hr\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\Url;

use common\components\CommonHelper;
use common\models\hr\Employee;
use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;
use common\models\hr\HrAnnualLeave;
use common\models\hr\search\Employee AS EmployeeSearch;
use common\models\rg\RgAuthItem;

class EmployeeController extends Controller
{

    public function actionSave___($id=null)
    {
        $return = false;
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        if(!empty($id)){
            $model = Employee::findOne($id);
            $model->LastUpdateBy = CommonHelper::getSessionIdUser();
            $model->LastUpdateDate = date('Y-m-d H:i:s');
        }else{
            $model = new Employee();
            $model->EntityId = $user->EntityId;
            $model->Status = 1;
            $model->Flag = 1;
            $model->CreatedBy = CommonHelper::getSessionIdUser();
        }

        if ($model->load($post)) {
            $SignPhoto = UploadedFile::getInstance($model, 'SignPhoto');
            if (!empty($SignPhoto)) {
                $ext = $SignPhoto->name;
                $model->SignPhoto = Yii::$app->security->generateRandomString()."{$ext}";
                $path = Yii::$app->basePath.Yii::$app->params['hrEmployeeBasepath'].$model->SignPhoto;
            } else {
                unset($model->SignPhoto);
            }

            $Photo = UploadedFile::getInstance($model, 'Photo');
            if (!empty($Photo)) {
                $ext = $Photo->name;
                $model->Photo = Yii::$app->security->generateRandomString()."{$ext}";
                $path = Yii::$app->basePath.Yii::$app->params['hrEmployeeBasepath'].$model->Photo;
            } else {
                unset($model->Photo);
            }

            $model = $this->FormatValues($model);
            if ($model->save()) {
                if (!empty($SignPhoto)) {
                    $model->SignPhoto = $model->Id.'_'.$model->SignPhoto;
                    $SignPhoto->saveAs($path);
                }
                if (!empty($Photo)) {
                    $model->Photo = $model->Id.'_'.$model->Photo;
                    $Photo->saveAs($path);
                }
            }

            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    }
                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $return = 'error : validation not valid : '.Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }




    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionForm($id = null, $action=null)
    {
        //if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new Employee();
            $model->load($post);
            // var_dump($id);
            // die();
            if (!empty($id)) {
                $model = $this->findModel($id);
                // var_dump($model->user_id, $model->guid, $id, $action);
                // die();
                $roles = Yii::$app->authManager->getRolesByUser($model->user_id);
                $model->Permissions = array();

                foreach ($roles as $role) {
                    $model->Permissions[$role->name] = $role->name;
                }

                if($action=="duplicate"){
                    $model->user_id = null;
                    $model->isNewRecord = true;
                }
            } else {
                $model = new Employee();
                $model->flag = 1;
            }

            $roles_dms = RgAuthItem::find()->orderBy('created_at')->where(['type' => 1])->All();
            $data['roles']['crowe'] = array();
            $data['roles']['crowe_desc'] = array();
            foreach ($roles_dms as $role) {
                $data['roles']['crowe'][$role->name] = $role->description;
                $data['roles']['crowe_desc'][$role->description] = $role->description;
            }

            // var_dump($id, $model->user_id);
            // die();
            $model->date_of_birth = (is_null($model->date_of_birth)) ? null : date('d/m/Y', strtotime($model->date_of_birth));
            $model->join_date = empty($model->join_date) ? "" : date('d/m/Y', strtotime($model->join_date));
            $model->resign_date = empty($model->resign_date) ? "" : date('d/m/Y', strtotime($model->resign_date));

            return $this->render('_form', [
                'data'=>$data,
                'model' => $model,
                'id'=> ($action=="duplicate") ? "" : $id,
                // 'id'=> ($action=="duplicate") ? "" : $model->guid,
            ]);
        //} else {
          //  throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        //}
    }

    public function actionSave()
    {
        $return = false;
        $model = new Employee();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $oldModel = $model;

            // $modelFind = Employee::findOne($model->user_id);
            $modelFind = Employee::find()->where(['guid' => $model->guid])->one();
            // $modelFind = Employee::findOne('guid', $model->guid);
            if ($modelFind !== null) {
                $model = $modelFind;
                $oldModel = $model;
            }
            $model->load($post);

            $model->date_of_birth = !empty($model->date_of_birth) ? date('Y-m-d', strtotime(str_replace('/', '-', $model->date_of_birth))) : null;
            $model->join_date = !empty($model->join_date) ? date('Y-m-d', strtotime(str_replace('/', '-', $model->join_date))) : null;
            $model->resign_date = !empty($model->resign_date) ? date('Y-m-d', strtotime(str_replace('/', '-', $model->resign_date))) : null;
            $model->flag = !empty($model->flag) ? $model->flag : null;
            $model->gender = !empty($model->gender) ? $model->gender : null;
            $model->parent_id = !empty($model->parent_id) ? $model->parent_id : null;
            $model->manager_id = !empty($model->manager_id) ? $model->manager_id : null;
            $model->supervisor_id = !empty($model->supervisor_id) ? $model->supervisor_id : null;
            // $model->gender = !empty($model->gender) ? $model->gender : null;
            $valid = $model->validate();
            // var_dump('tes', $valid);
            // die();
            if ($valid) {

                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if($model->isNewRecord){
                        $model->password_hash = '6789';
                        $model->setPassword($model->password_hash);
                        $model->generateAuthKey();
                        $model->passwordRepeat = $model->password_hash;
                    }

                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    }else{
                        $auth = Yii::$app->authManager;
                        $auth->revokeAll($oldModel->user_id);
                        $auth->revokeAll($model->user_id);

                        if (!empty($model->Permissions)) {
                            foreach ($model->Permissions as $permission) {
                                $role = $auth->getRole($permission);
                                $auth->assign($role, $model->user_id);
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        $return = true;

                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {

                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }
    public function actionAnnualleave(){
        $post = Yii::$app->request->post();
        $id = $post['id'];

        $model = $this->findModelAnnualLeave($id);
        return $this->renderAjax('annualleave', [
          'model'=>$model,
        ]);
    }
    public function actionAnnualleave_save(){
        $post = Yii::$app->request->post();
        $model = new HrAnnualLeave();

        if($model->load($post)){
            $id = $model->id;
            $model_save = $this->findModelAnnualLeave($id);
            $model_save->annual_days = CommonHelper::ResetDecimal($model->annual_days);
            $model_save->annual_used_hours_ob = CommonHelper::ResetDecimal($model->annual_used_hours_ob);
            if($model_save->save()){
                $return = true;
            }else{
                $return = $model->getErrors();
            }
        }else{
            $return = 'error : model not loaded';
        }
        return $return;
    }
    public function actionResetannualleave(){
        $model = new Employee();
        $model->scenario = 'resetannualleave';
        return $this->renderAjax('annualleave_reset', [
            'model'=>$model,
        ]);
    }
    public function actionResetannualvacation(){
        $post = Yii::$app->request->post();
        $model = new Employee();
        $model->scenario = 'resetannualleave';
        if($model->load($post)){
            $annualLeave = empty($model->annualVacation) ? 0 : $model->annualVacation;
            $year = empty($model->year) ? intval(date('Y')) : intval($model->year);

            $sql = "call update_employee_annual_leave({$year}, {$annualLeave});";
            $return = Yii::$app->db->createCommand($sql)->execute();
        }else{
            $return = 'error : model not loaded';
        }
        return $return;
    }
    public function actionResetmaxviewtimereport(){
      $sql = "call view_tr(3);";
        // $sql = "call hrEmployeeResetMaxViewTimereport();";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
    public function actionOpen1month(){
      $sql = "call view_tr(31);";
        // $sql = "call hrEmployeeMaxViewOpen30days();";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
    public function actionMaxtr($iday)
    {
      // var_dump($iday);
      // die();
      $sql = "call view_tr(".$iday.");";
      $data = Yii::$app->db->createCommand($sql)->execute();
      return $data;
    }
    public function actionReset($id)
    {
        $auth = Yii::$app->authManager;
        $model = $this->findModel($id);

        $flag = false;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model->password_hash = '6789';
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->passwordRepeat = $model->password_hash;

            if (!($flag = $model->save())) {
                $transaction->rollback();
            }

            if ($flag) {
                $transaction->commit();
            }
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findModelAnnualLeave($id)
    {
        if (($model = HrAnnualLeave::find()->where(['id'=>$id])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionDetail(){
        $post = Yii::$app->request->post();
        $id = $post['expandRowKey'];
        $results = "";
        if(!empty($id)){
            $model = $this->findModel($id);
            // var_dump($post, $id, $model->user_id);
            // die();
            $results = $this->renderAjax('detail', ['model'=>$model]);
        }

        return $results;
    }
}
