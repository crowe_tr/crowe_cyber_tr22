<?php

namespace backend\modules\hr\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\widgets\Select2;
use common\components\HrHelper;
use yii\web\JsExpression;

// use common\models\hr\Employee;

class HrController extends Controller
{
  public static function set_col($t, $icol, $ival)
  {
    return  '<'.$t.' class="col-md-'.$icol.'">
                  '.$ival.'
              </'.$t.'>
            ';
  }

  public static function set_tr48($ival1, $ival2)
  {
    return  '<tr>
              '.self::set_col('th', '4', $ival1).'
              '.self::set_col('td', '8', $ival2).'
            </tr>';
  }

  public static function set_column($iattribute, $icol_sm){
    return  [
              'attribute' => $iattribute,
              'headerOptions' => ['class' => 'col-sm-'.$icol_sm.' bg-success'],
              'filterOptions' => ['class' => 'b-b b-grey'],
              'contentOptions' => ['class' => 'kv-align-middle'],
            ];
  }

  public static function set_column_filter($iattribute, $icol_sm, $irow, $ifield, $isource){
    return  [
              'attribute' => $iattribute,
              'headerOptions' => ['class' => 'col-sm-'.$icol_sm.' bg-success'],
              'filterOptions' => ['class' => 'b-b b-grey'],
              'contentOptions' => ['class' => 'kv-align-middle'],
              'filterType' => GridView::FILTER_SELECT2,
              'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
              ],
              'filterInputOptions' => ['placeholder' => ''],
              'format' => 'raw',
              'filter' => ArrayHelper::map($irow, 'id', $ifield),
              'value' => function ($data) use($isource,$ifield){
                return empty($data->$isource->$ifield) ? '-' : $data->$isource->$ifield;
              },
            ];
  }

  public static function set_column_yn($iattribute, $icol_sm, $iinit){
    $vno = '';
    $vyes = '';
    if ($iinit == 1) {
      $vyes = 'Yes';
      $vno = 'No';
    } elseif ($iinit == 2) {
      $vyes = 'Active';
      $vno = 'Non Active';
    } elseif ($iinit == 3) {
      $vyes = 'Active';
      $vno = 'Resign';
    }

    return  [
              'attribute' => $iattribute,
              'headerOptions' => ['class' => 'col-sm-'.$icol_sm.' bg-success'],
              'filterOptions' => ['class' => 'b-b b-grey'],
              'contentOptions' => ['class' => 'kv-align-middle'],
              'filterType' => GridView::FILTER_SELECT2,
              'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
              ],
              'filterInputOptions' => ['placeholder' => ''],
              'format' => 'raw',
              'filter' => [$vno, $vyes],
              'value' => function ($data) use($iattribute, $vno, $vyes){
                return ($data->$iattribute == 1) ? $vyes : $vno;
              },
            ];
  }

  public static function get_link($ilink1, $ilink2 = null, $ilink3 = null)
  {
    $vlink = '/'.$ilink1;
    if (!is_null($ilink2)) {
      $vlink = $vlink . '/' . $ilink2;
    }
    if (!is_null($ilink3)) {
      $vlink = $vlink . '/' . $ilink3;
    }
    return $vlink;
  }

  public static function set_colepse($ilink1, $ilink2 = null, $ilink3 = null){
    $vlink = self::get_link($ilink1, $ilink2, $ilink3);

    return  [
              'class' => 'kartik\grid\ExpandRowColumn',
              'enableRowClick' => true,
              'mergeHeader' => false,
              'headerOptions' => ['class' => 'kartik-sheet-style b-l b-r  b-success bg-success'],
              'filterOptions' => ['class' => ''],
              'contentOptions' => ['class' => 'kv-align-middle bg-warning-lighter b-l b-r b-grey', 'style' => 'height: 50px'],
              'expandOneOnly' => true,
              'width' => '50px',
              'expandIcon' => '<i class="fa fa-angle-right"></i>',
              'collapseIcon' => '<i class="fa fa-angle-up"></i>',
              'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
              },
              'detailUrl' => Url::to([$vlink]),
            ];
  }

  public static function btn_act($icap, $ifa, $iclick)
  {
    return Html::a(
                    '<i class="fa '.$ifa.'"></i> <span class="hidden-xs">'.$icap.'</span>',
                    'javascript:;',
                    [
                      'onclick' => $iclick,
                      'class' => 'dropdown-item',
                    ]
                  );
  }

  public static function btn_act2($ifa, $iact, $idata_id, $ioptions)
  {
    return Html::a( '<i class="fa '.$ifa.'"></i>',
                    [$iact, 'id' => $idata_id],
                    $ioptions
                    );
  }

  public static function btn_elip($value='')
  {
    return '<div class="trigger">
              ' . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']) . '
            </div>';
  }

  public static function btn_elip_group($value='')
  {
    return  [
              'header' => '<i class="fa fa-cogs"></i>',
              'headerOptions' => ['class' => 'bg-success text-center'],
              'filterOptions' => ['class' => 'b-b b-grey'],
              'contentOptions' => ['class' => 'no-padding'],
              'format' => 'raw',
            ];
  }

  public static function btn_new()
  {
    return Html::a(
                    '<i class="pg-plus"></i> <span class="hidden-xs">CREATE NEW</span>',
                    ['form'],
                    [
                      //'onclick' => 'FormCreate()',
                      'class' => 'btn btn-warning text-white ',
                    ]
                  );
  }

  public static function reset_view_tr($value='')
  {
    return  "function resetViewTr(txt, link) {
              showFullLoading();

              if (confirm(txt)) {
                $.ajax({
                  url: link,
                  data: {},
                  method: 'POST',
                  dataType: 'json',
                  success: function() {
                    notif('Success Updating data !');
                    hideFullLoading();
                    reload();
                  },
                  error: function(XMLHttpRequest, textStatus, errorThrown, data) {
                    $('#error').show();
                    var err = unescape(XMLHttpRequest.responseText);
                    err = err.split('&#039;');
                    err = err[3];
                    err = escape(err);

                    err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                    err = unescape(err[1]);
                    $('#error').html(err);
                    $('html, body').animate({
                      scrollTop: 0
                    }, 0);
                    hideFullLoading();
                  }
                });
              } else {
                $('html, body').animate({
                  scrollTop: 0
                }, 0);
                hideFullLoading();
              }
            }";
  }


  public static function input_text_box($icol, $iform, $imodel, $ifield, $idata)
  {
    return
      '<div class="col-md-'.$icol.'">' .
        $iform->field($imodel, $ifield, ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
          Select2::classname(),
          [
              'data'  =>  $idata,
              'options' => ['placeholder' => 'Select ...'],
          ]
        ) .
      '</div>';
  }

    public static function input_lookup($icol, $iform, $imodel, $itable, $idata)
    {
      return
        '<div class="col-md-'.$icol.'">' .
            $iform->field($imodel, $itable . '_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                Select2::classname(),
                [
                    'options' => ['id' => $itable . '_id', 'placeholder' => 'Select...'],
                    'data' => ArrayHelper::map($idata, 'id', $itable . '_name'),
                    'pluginOptions' => [
                        'allowClear' => true,
                        'placeholder' => Yii::t('backend', 'Select..'),
                    ],

                ]
            ) .
        '</div>';
    }

    public static function input_lookup2($icol, $iform, $imodel, $ifield)
    {
      $data = HrHelper::getParent($imodel->$ifield);
      return  '<div class="col-md-'.$icol.'">'.
                $iform->field($imodel, $ifield, ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
                    ->widget(Select2::classname(), [
                        'initValueText' => !empty($data->full_name) ? $data->user_id . ' - ' . $data->full_name : "",
                        'options' => ['placeholder' => 'Search Leader ...'],
                        'pluginOptions' => [
                            'tags' => false,
                            'allowClear' => true,
                            'ajax' => [
                                'url' => Url::to(['/hr/helper/srcleader', 'id' => $imodel->user_id]),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                            ],
                        ],
                    ])
              .'</div>';
    }

}
