<?php

namespace app\modules\hr;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\hr\controllers';

    public function init()
    {
        parent::init();
        $this->layout = '@backend/views/layouts/main';
    }
}
