<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;

use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;
use backend\modules\hr\controllers\HrController;
use common\components\CmHelper;

/* HEADER DARI EMPLOYEE - kamal 08 Apr 2022 */
CmHelper::title_only($this, 'Employees Setup', 'SETUP');
$this->params['breadcrumbs_btn'] =
  ' <div class="btn-group dropdown dropdown-default">
      <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Utility
      </button>
      <div class="dropdown-menu" style="width: 240px">'
        . HrController::btn_act('OPEN TIMEREPORT 1 MONTH', 'fa-calendar',
            'resetViewTr("Are you sure you want to open 1 Month TimeReport MaxView for all employees ?","' . Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/maxtr','iday' => '31']) . '")')
        . HrController::btn_act('RESET TIMEREPORT', 'fa-calendar',
            'resetViewTr("Are you sure you want to reset TimeReport MaxView for all employees ?", "' . Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/maxtr','iday' => '3']) . '")')
        . HrController::btn_act('RESET ANNUAL LEAVE NEW', 'fa-refresh',
            'reload();')
      . '
      </div>
    </div>'
    .'&nbsp;'.HrController::btn_new();
$this->params['crud_ajax'] = false;

/* DATA DARI EMPLOYEE - kamal 08 Apr 2022 */
$column = [
  HrController::set_colepse('hr','employee','detail'),
  HrController::set_column('user_id', '1'),
  HrController::set_column('user_name', '2'),
  HrController::set_column_filter('entity_id', '2', Entity::find()->all(), 'entity_name', 'entity'),
  HrController::set_column_filter('div_id', '2', Division::find()->all(), 'div_name', 'division'),
  HrController::set_column_filter('dept_id', '2', Dept::find()->all(), 'dept_name', 'dept'),
  HrController::set_column_filter('level_id', '2', Level::find()->all(), 'level_name', 'level'),
  HrController::set_column_yn('flag', '1', 3),

/* BUTTON UNTUK EDIT DAN RESET PASSWORD - kamal 08 Apr 2022 */
  array_merge(
    HrController::btn_elip_group(),
    [ 'value' => function ($data) {
        $val =  '<div class="tooltip-action">'
                    . HrController::btn_elip() .
                    '<div class="action-mask">
                        <div class="action"> '
                          . HrController::btn_act2('fa-edit', 'form', $data->Id,
                            [
                              'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                              'style' => 'bjob-radius: 5px !important',
                            ])
                          . HrController::btn_act2('fa-refresh', 'reset', $data->Id,
                            [
                              'data-method' => 'post',
                              'data-confirm' => 'Are you sure want to reset password this user login ?',
                              'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                            ])
                      /* Buat Delete cara reload balik lagi nya belum ketemu */
                          . HrController::btn_act2('pg-trash', 'delete', $data->Id,
                            [
                              'data-method' => 'post',
                              'data-confirm' => 'Are you sure want to delete this user ?',
                              'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                              // 'onclick' => "FormDelete('".Yii::$app->urlManager->createUrl([Yii::$app->request->getPathInfo()."/delete", 'id' => $data->Id])."')",
                            ]) .
                        '</div>
                   </div>
               </div>';
      return $val;
    }]
  ),
];

  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view2($dataProvider, $searchModel, $column) );
?>

<script type="text/javascript">
  var paramJs = (paramJs || {});
  paramJs.urlFormShowModalResetAnnual = '<?= Yii::$app->urlManager->createAbsoluteUrl(['/hr/employee/resetannualleave']); ?>';
  paramJs.urlFormShowModalAnnual = '<?= Yii::$app->urlManager->createAbsoluteUrl(['/hr/employee/annualleave']); ?>';
  paramJs.urlRefreshAnnual = '<?= Yii::$app->urlManager->createAbsoluteUrl(['/hr/employee/detail']); ?>';

  function FormShowModalResetAnnual(link = "") {
    $('#crud-modal').modal('show');
    $('#detail').html("loading ...");

    var link = (link || paramJs.urlFormShowModalResetAnnual);
    $.ajax({
      url: link,
      data: $('#crud-form').serialize(),
      method: "GET",
      dataType: 'html',
      success: function(data) {
        $('#detail').html(data);
      },
    });
  }

  function FormShowModalAnnual(id, link = "") {
    $('#crud-modal').modal('show');
    $('#detail').html("loading ...");

    var link = (link || paramJs.urlFormShowModalAnnual);
    $.ajax({
      url: link,
      data: {id:id},
      method: "POST",
      dataType: 'html',
      success: function(data) {
        $('#detail').html(data);
      },
    });
  }
  function RefreshAnnual(id, link = "") {
    showFullLoading();
    var link = (link || paramJs.urlRefreshAnnual);
    $.ajax({
      url: link,
      data: {expandRowKey:id},
      method: "POST",
      dataType: 'html',
      success: function(data) {
        $('#wrapper-'+id).html(data);
        hideFullLoading();
      },
    });
  }

</script>

<?php
  $this->registerjs(HrController::reset_view_tr(), View::POS_END);
  $this->registerjs(CmHelper::scr_close(), View::POS_END);
  $this->registerjs(CmHelper::scr_reload(), View::POS_END);
?>
