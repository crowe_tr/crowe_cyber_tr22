<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\web\JsExpression;

use common\models\cm\CmMaster;
use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;
use common\models\hr\Employee;


$this->title = 'Employees';
$this->params['breadcrumbs'][] = "Setup";
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs_btn'] = Html::a(
    'BACK',
    ['index'],
    [
        //'onclick' => 'FormCreate()',
        'class' => 'btn btn-info text-white ',
    ]
);


?>
<?php
$form = ActiveForm::begin([
    'id' => 'crud-form',
    //'action'=>['save'],
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
    ],
    'errorSummaryCssClass' => 'alert alert-danger'
]);
echo $form->errorSummary($model);;

?>
<div id="error" class="alert alert-danger" style="display:none"></div>
<div class="row m-b-30">
    <div class="col-md-12">
        <b>Annual Leave</b>
        <div class="form-group-attached">
            <div class="row">
                <?= $form->field($model, 'id', ['options'=>[ 'style'=>'display:none;']])->textInput(); ?>

                <div class="col-md-4">
                    <?= $form->field($model, 'annual_year')->textInput(['disabled'=>true]); ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'annual_days')->widget(MaskedInput::className(), ['clientOptions' => [
                        'alias' =>  'decimal',
                        //'mask' => '99',
                        'groupSeparator' => ',',
                        'autoGroup' => true
                    ],]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'annual_used_hours_ob')->widget(MaskedInput::className(), ['clientOptions' => [
                        'alias' =>  'decimal',
                        //'mask' => '99',
                        'groupSeparator' => ',',
                        'autoGroup' => true
                    ],]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 m-t-5">
            <button id="button" type="submit" value="SAVE" class="btn btn-warning btn-lg">SAVE</button>
            <?=
                Html::a(
                    'BACK',
                    ['index'],
                    [
                        'class' => 'btn btn-info btn-lg text-white ',
                        'data-dismiss' => "modal"
                    ]
                );
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">
    paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/annualleave_save']); ?>';
    $('#crud-form').on('beforeSubmit', function() {
        var $form = new FormData($('#crud-form')[0]);
        $.ajax({
            url: paramJs.urlFormSave,
            type: 'POST',
            data: $form,
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function(data) {
                if (data != 1) {
                    alert(data);
                } else {
                    $('#crud-modal').modal('hide');
                    RefreshAnnual('<?=$model->employee_id;?>');
                }
            },
            error: function(jqXHR, errMsg) {
                alert(errMsg);
            }
        });
        return false;
    });
</script>