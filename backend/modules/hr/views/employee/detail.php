<?php
  use common\models\hr\HrAnnualLeave;
  use backend\modules\hr\controllers\HrController;

  // var_dump($model->leader);
  // die();
  $leader_id = !is_null($model->leader) ? $model->leader->user_id : "";
  $leader_fullName = !is_null($model->leader) ? $model->leader->full_name : "";
  $manager_id = !is_null($model->manager) ? $model->manager->user_id : "";
  $manager_name = !is_null($model->manager) ? $model->manager->full_name : "";
  $project_manager = !empty($model->projectmanager[0]['project_manager']) ? $model->projectmanager[0]['project_manager'] : "";
  $project_supervisor = !empty($model->projectsupervisor[0]['project_supervisor']) ? $model->projectsupervisor[0]['project_supervisor'] : "";
  $supervisor_id = !is_null($model->supervisor) ? $model->supervisor->user_id : "";
  $supervisor_name = !is_null($model->supervisor) ? $model->supervisor->full_name : "";
  $formatter = \Yii::$app->formatter;
?>

<div id="wrapper-<?= $model->user_id; ?>">
  <div class="row">
    <div class="col-md-6">
      <p class="m-t-15"><b>BIODATA</b></p>
      <table class="table table-bordered table-striped ">
        <?php
          echo HrController::set_tr48('Full Name', $model->full_name);
          echo HrController::set_tr48('Gender', ($model->gender == 'm') ? 'Male' : 'Female');
          echo HrController::set_tr48('Place Of Birth', $model->place_of_birth);
          echo HrController::set_tr48('Date Of Birth', $formatter->asDate($model->date_of_birth,'d MMM Y'));
          echo HrController::set_tr48('Religion', $model->religion);
          echo HrController::set_tr48('Marital Status', $model->marital_status);
          echo HrController::set_tr48('Nationality', $model->nationality);
          echo HrController::set_tr48('Education', $model->education);
        ?>
      </table>
      <p class="m-t-15"><b>CONTACT</b></p>
      <table class="table table-bordered table-striped ">
        <?php
          echo HrController::set_tr48('Mobile Phone', $model->mobile_phone);
          echo HrController::set_tr48('Email', $model->user_email);
          echo HrController::set_tr48('Address', $model->address);
        ?>
      </table>
    </div>
    <div class="col-md-6">
      <p class="m-t-15"><b>EMPLOYMENT STATUS</b></p>
      <table class="table table-bordered table-striped ">
        <?php
          echo HrController::set_tr48('Group', $leader_id .' - '. $leader_fullName);
          echo HrController::set_tr48('Manager', $manager_id .' - '. $manager_name);
          echo HrController::set_tr48('Project Manager', $project_manager);
          echo HrController::set_tr48('Supervisor', $supervisor_id .' - '. $supervisor_name);
          echo HrController::set_tr48('Project Supervisor', $project_supervisor);
          echo HrController::set_tr48('Join Date', $formatter->asDate($model->join_date,'dd MMM Y'));
          echo HrController::set_tr48('Resign Date', $formatter->asDate($model->resign_date,'dd MMM Y'));
          echo HrController::set_tr48('Resign Description', $model->resign_description);
        ?>
      </table>
      <p class="m-t-15"><b>ANNUAL VACATION</b></p>
      <table class="table-bordered table-striped ">
        <?php
          echo HrController::set_tr48('Keep Vacation Balance', $model->keep_vacation_balance);
          echo HrController::set_tr48('Annual Vacation', $model->annual_vacation);
        ?>
      </table>
    </div>
  </div>
</div>
