<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\web\JsExpression;

use common\models\cm\CmMaster;
use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;
use common\models\hr\Employee;

use backend\modules\hr\controllers\DataController;
use backend\modules\hr\controllers\HrController;
use common\components\CmHelper;
use common\components\HrHelper;



$this->title = 'Employees';
$this->params['breadcrumbs'][] = "Setup";
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs_btn'] = Html::a(
    'BACK',
    ['index'],
    [
        //'onclick' => 'FormCreate()',
        'class' => 'btn btn-info text-white ',
    ]
);


?>
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-body">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'crud-form',
                    //'action'=>['save'],
                    'enableClientValidation' => true,
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                    'fieldConfig' => [
                        'template' => '{label}{input}',
                        'options' => [
                            'class' => 'form-group form-group-default',
                        ],
                    ],
                    'errorSummaryCssClass' => 'alert alert-danger'
                ]);
                echo $form->errorSummary($model);
                // echo $form->field($model, 'guid', ['options' => ['class' => '']])->hiddenInput()->label(false);
                ?>
                <div id="error" class="alert alert-danger" style="display:none"></div>
                <div class="row m-b-30">
                    <div class="col-md-12">
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-md-2">
                                    <?php

                                      // echo $form->field($model, 'user_id')->textInput(['maxlength' => 32]);
                                    if ($model->isNewRecord) {
                                        echo $form->field($model, 'user_id')->textInput(['maxlength' => 32]);
                                    } else {
                                        // $model->id_label = $model->Id;
                                        // echo $form->field($model, 'user_id', ['options' => ['class' => '']])->hiddenInput()->label(false);
                                        // echo $form->field($model, 'id_label')->textInput(['disabled' => true, 'maxlength' => 32]);

                                        // $model->id_label = $model->guid;

                                        echo $form->field($model, 'user_id')->textInput(['maxlength' => 32]);
                                        echo $form->field($model, 'guid', ['options' => ['class' => '']])->hiddenInput()->label(false);
                                    }
                                    ?>
                                </div>
                                <?php
                                  echo CmHelper::set_text_input('7', $form, $model, 'full_name');
                                  echo CmHelper::set_text_input('3', $form, $model, 'initial');
                                 ?>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field(
                                        $model,
                                        'gender',
                                        [
                                            'options' => [
                                                'class' => ' form-group form-group-default form-group-default-select2',
                                            ],
                                        ]
                                    )->widget(
                                        Select2::classname(),
                                        [
                                            'data' => [
                                                'm' => Yii::t('backend', 'Male'),
                                                'f' => Yii::t('backend', 'Female'),
                                            ],
                                            'options' => ['placeholder' => 'Select ...'],
                                        ]
                                    ) ?>

                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'date_of_birth')->widget(MaskedInput::className(), ['clientOptions' => ['alias' => 'dd/mm/yyyy']]) ?>
                                </div>
                                <?php
                                  echo CmHelper::set_text_input('6', $form, $model, 'place_of_birth');
                                ?>

                            </div>
                            <div class="row">
                                <?php
                                  echo HrController::input_text_box('3', $form, $model, 'religion', DataController::data_religion());
                                  echo HrController::input_text_box('3', $form, $model, 'marital_status', DataController::data_marital());
                                  echo HrController::input_text_box('4', $form, $model, 'nationality', DataController::data_national());
                                  echo HrController::input_text_box('2', $form, $model, 'education', DataController::data_edu());
                                ?>
                            </div>
                        </div>

                        <p class="m-t-15"><b>CONTACT</b></p>
                        <div class="form-group-attached">
                            <div class="row">
                                <?php
                                  echo CmHelper::set_phone_input('6', $form, $model, 'mobile_phone');
                                  echo CmHelper::set_text_input('6', $form, $model, 'user_email');
                                ?>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'address')->textarea(['rows' => 2, 'style' => 'height: 72px']) ?>
                                </div>
                            </div>
                        </div>

                        <p class="m-t-15"><b>EMPLOYMENT STATUS</b></p>
                        <div class="form-group-attached">
                            <div class="row">
                                <?php
                                  echo HrController::input_lookup('2', $form, $model, 'entity', Entity::find()->asArray()->all());
                                  echo HrController::input_lookup('3', $form, $model, 'div', Division::find()->asArray()->all());
                                ?>
                                <div class="col-md-3">
                                    <?php
                                    echo $form->field($model, 'dept_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
                                        'data' => ArrayHelper::map(Dept::find()->asArray()->all(), 'id', 'dept_name'),
                                        'options' => ['id' => 'dept_id', 'placeholder' => 'Select ...'],
                                        'type' => DepDrop::TYPE_SELECT2,
                                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                        'pluginOptions' => [
                                            'depends' => ['div_id'],
                                            'url' => Url::to(['/cm/helper/loaddept']),
                                            'loadingText' => 'Loading ...',
                                            'allowClear' => true,
                                            'placeholder' => Yii::t('backend', 'Select..'),
                                        ],
                                    ]);
                                    ?>
                                </div>

                                <div class="col-md-4">
                                    <?php
                                    echo $form->field($model, 'level_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['id' => 'level_id', 'placeholder' => 'Select...'],
                                            'data' => ArrayHelper::map(Level::find()->asArray()->all(), 'id', 'level_name'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],
                                        ]
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <?= $form->field($model, 'join_date')->widget(DatePicker::classname(), [
                                        'type' => DatePicker::TYPE_INPUT,
                                        'options' => ['placeholder' => 'Enter Join date ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                        ],
                                    ])
                                    ?>
                                </div>

                                <div class="col-md-2">
                                    <?= $form->field($model, 'resign_date')->widget(DatePicker::classname(), [
                                        'type' => DatePicker::TYPE_INPUT,
                                        'options' => ['placeholder' => 'Enter Resign date ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                        ],
                                    ])
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'resign_description')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-2">
                                    <?php
                                    echo $form->field($model, 'flag', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['id' => 'flag', 'placeholder' => 'Select...'],
                                            'data' => [1 => 'Active', 0 => 'Resign'],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],

                                        ]
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <?php
                                  echo HrController::input_lookup2('4', $form, $model, 'parent_id');
                                  echo HrController::input_lookup2('4', $form, $model, 'manager_id');
                                  echo HrController::input_lookup2('4', $form, $model, 'supervisor_id');
                                ?>


                            </div>
                        </div>

                        <p class="m-t-15"><b>ANNUAL VACATION</b></p>
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-md-4" style="display:none;">
                                    <?php
                                    echo $form->field($model, 'keep_vacation_balance', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['placeholder' => 'Select...'],
                                            'data' => [
                                                1 => 'Yes',
                                                0 => 'No',
                                            ],
                                            'pluginOptions' => [
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],

                                        ]
                                    );
                                    ?>
                                </div>
                                <div class="col-md-3" style="display:none;">
                                    <?= $form->field($model, 'annual_vacation')->textInput(['maxlength' => true, 'type' => 'number']) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'max_view_time_report')->textInput(['maxlength' => true, 'type' => 'number']) ?>
                                </div>
                            </div>
                        </div>

                        <p class="m-t-15"><b>USER'S RIGHT</b></p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group-attached">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'user_name')->textInput(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-5">
                            <div class="col-md-12 well padding-10">
                                <?php
                                echo $form->field($model, 'Permissions', [
                                    'options' => ['class' => '']
                                ])->label(false)->checkboxButtonGroup($data['roles']['crowe'], [
                                    'class' => 'btn-group',
                                    'disabledItems' => [''],
                                    'itemOptions' => ['labelOptions' => ['class' => 'btn btn-warning', 'style' => 'box-shadow: unset !important']],
                                ]);
                                ?>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 m-t-5">
                            <hr class="m-b-5" />
                            <button id="button" type="button" value="SAVE" class="btn btn-warning btn-lg">SAVE</button>
                            <?=
                                Html::a(
                                    'BACK',
                                    ['index'],
                                    [
                                        'class' => 'btn btn-info btn-lg text-white ',
                                        'data-dismiss' => "modal"
                                    ]
                                );
                            ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var paramJs = (paramJs || {});
    paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/save', 'id' => $id]); ?>';

    //$('#crud-form').on('beforeSubmit', function() {
    $('#button').click(function(event) {
        event.preventDefault(); //this will prevent the default submit
        showFullLoading();
        var $form = new FormData($('#crud-form')[0]);
        $.ajax({
            url: paramJs.urlFormSave,
            type: 'POST',
            data: $form,
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function(data) {
                hideFullLoading();

                if (data != 1) {
                    $('#error').show();
                    $('#error').html(data);
                } else {
                    notif("Success Updating data !");
                    window.location.href = '/hr/employee/index';
                    <?php
                    // if (!is_null($id)) {
                        // echo 'window.location.href = "' . Yii::$app->urlManager->createAbsoluteUrl(['/hr/employee/index']) . '";';
                    // }
                    ?>
                    //$('#crud-modal').modal('hide');
                    //reload();
                }
            },
            error: function(jqXHR, errMsg) {
                hideFullLoading();
                $('#error').show();
                $('#error').html(data);
            }
        });
        return false;
        event.preventDefault(); //this will prevent the default submit

    });
</script>
