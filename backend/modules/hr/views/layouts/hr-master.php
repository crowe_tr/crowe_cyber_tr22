<?php

//helper dipanggil disni :O ASIIIKK

?>

<?php $this->beginContent('@backend/modules/hr/views/layouts/hr-layout.php'); ?>
	<?php //echo $this->render('main-jumbotron');?>
	<?php echo $this->render('hr-leftbar'); ?>
	<div class="page-container ">
		<?php echo $this->render('hr-header'); ?>
		<div class="page-content-wrapper">
			<div class="content padding-20">
				<?= $content ?>
			</div>
		</div>
		<div class="container-fluid container-fixed-lg footer p-l-20 p-r-20">
			<div class="copyright sm-text-center">
				<p class="small no-margin pull-left sm-pull-reset">
					<span class="hint-text">Copyright © 2016 </span>
					<span class="font-montserrat">PROKU</span>.
					<span class="hint-text">All rights reserved. </span>
				</p>
				<p class="small no-margin pull-right sm-pull-reset">
					<span class="hint-text">Hand-crafted & Made with Love ®</span>
				</p>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>



<?php $this->endContent(); ?>
