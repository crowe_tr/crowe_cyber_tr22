<?php
use yii\widgets\Breadcrumbs;

?>

<?php $this->beginContent('@backend/modules/hr/views/layouts/hr-layout.php'); ?>
	<?php //echo $this->render('main-jumbotron');?>
	<?php echo $this->render('hr-leftbar'); ?>
	<div class="page-container ">
		<?php echo $this->render('hr-header'); ?>
		<div class="page-content-wrapper">
			<div class="content padding-20">
				<div class="row m-b-10">
					<div class="col-md-7">
						<?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 'homeLink' => false]); ?>
					</div>
					<div class="col-md-5 p-t-5 text-right">
						<?= isset($this->params['breadcrumbs-btn']) ? $this->params['breadcrumbs-btn'] : ''; ?>
					</div>
				</div>

				<?= $content ?>
			</div>
		</div>
		<div class="container-fluid container-fixed-lg footer p-l-20 p-r-20">
			<div class="copyright sm-text-center">
				<p class="small no-margin pull-left sm-pull-reset">
					<span class="hint-text">Copyright © 2016 </span>
					<span class="font-montserrat">PROKU</span>.
					<span class="hint-text">All rights reserved. </span>
				</p>
				<p class="small no-margin pull-right sm-pull-reset">
					<span class="hint-text">Hand-crafted & Made with Love ®</span>
				</p>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>



<?php $this->endContent(); ?>
