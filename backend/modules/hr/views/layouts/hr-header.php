<?php
use yii\helpers\Html;
use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK

$user = CommonHelper::getUserIndentity();

?>

	<div class="header" style="border-bottom: thin solid #ddd;">
		<div class="container-fluid relative">
			<div class="pull-left full-height visible-sm visible-xs">
				<div class="header-inner">
					<a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
						<span class="icon-set menu-hambuger"></span>
					</a>
				</div>
			</div>
			<div class="pull-center hidden-md hidden-lg">
				<div class="header-inner">
					<div class="brand inline">
						<img src="<?php echo $this->theme->baseUrl; ?>/assets/img/logo.png" alt="logo" height="22" width="78">
					</div>
				</div>
			</div>
			<div class="pull-right full-height visible-sm visible-xs">
				<div class="header-inner">
					<a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
						<span class="icon-set menu-hambuger-plus"></span>
					</a>
				</div>
			</div>
		</div>
		<div class=" pull-left sm-table hidden-xs hidden-sm">
			<div class="header-inner">
				<div class="brand inline text-left p-l-20">
					<img src="<?php echo $this->theme->baseUrl; ?>/assets/img/logo.png" alt="logo" data-src="<?php echo $this->theme->baseUrl; ?>/assets/img/logo.png" data-src-retina="assets/img/logo_2x.png" height="22" width="78">
				</div>
			</div>
		</div>
		<div class=" pull-right">
			<div class="header-inner">
			</div>
		</div>
		<div class=" pull-right">
			<div class="visible-lg visible-md m-t-10">
				<div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
					<span class="semi-bold"><?= $user->employee->Name ?></span>
				</div>
				<div class="dropdown pull-right">
					<button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 inline m-t-5">
						<?=
                        Html::img(
                            $user->employee->Photo,
                            [
                                'class' => 'img img-rounded',
                                'alt' => 'logo',
                                'data-src' => $user->employee->Photo,
                                'data-src-retina' => $user->employee->Photo,
                                'height' => '32',
                                'width' => '32',

                            ]
                        )
                        ?>
						</span>
					</button>
					<ul class="dropdown-menu profile-dropdown" role="menu">
						<li class="bg-master-lighter">
							<?= Html::a('<span class="pull-left">Logout</span><span class="pull-right"><i class="pg-power text-danger"></i></span>', ['/site/logout'], ['class' => 'clearfix']) ?>
						</li>
					</ul>
				</div>
			</div>
			<!-- END User Info-->
		</div>
	</div>
