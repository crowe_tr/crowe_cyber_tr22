<?php
use yii\helpers\Html;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
		<head>
			<meta charset="<?= Yii::$app->charset ?>" />
			<?= Html::csrfMetaTags() ?>
				<meta http-equiv="X-UA-Compatible" content="IE=edge" />
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
				<title>
					proku
					<?= !empty($this->title) ? ' | '.Html::encode(ucfirst($this->title)) : ''; ?>
				</title>
				<?php $this->head() ?>
					<meta name="apple-mobile-web-app-capable" content="no">
					<meta name="apple-touch-fullscreen" content="no">
					<meta name="apple-mobile-web-app-status-bar-style" content="default">
					<meta content="PROKU - E-Procurement KU" name="description" />
					<meta content="Andre Antariksa" name="author" />

					<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
					<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
					<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
					<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
					<link href="<?php echo $this->theme->baseUrl; ?>/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
					<link class="main-stylesheet" href="<?php echo $this->theme->baseUrl; ?>/pages/css/pages.css" rel="stylesheet" type="text/css" />
					<link href="<?php echo $this->theme->baseUrl; ?>/pages/css/custom.css" rel="stylesheet" type="text/css" />

					<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script type="text/javascript">
		 $(document).ready(function(){
				 $('[data-toggle="tooltip"]').tooltip();
		 });
		</script>

	</head>
	<body >
		<?php $this->beginBody(); ?>
		<div id="notification-wrapper"></div>
			<?= $content ?>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/pages/js/pages.min.js"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/js/scripts.js" type="text/javascript"></script>
		<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>
