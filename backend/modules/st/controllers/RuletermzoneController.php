<?php

namespace backend\modules\st\controllers;

use Yii;
use common\models\st\RuleTermZone;
use common\models\st\search\RuleTermZone as RuleTermZoneSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\components\CmHelper;
use backend\modules\st\controllers\StController;

class RuletermzoneController extends Controller
{
    public function behaviors()
    {
      return StController::set_behaviors();
    }

    public function actionIndex()
    {
      $searchModel = new RuleTermZoneSearch();
      return StController::set_index($searchModel, $this);
    }

    public function actionForm($id = null, $action=null)
    {
      if (Yii::$app->request->isAjax) {
          $post = Yii::$app->request->post();

          $model = new RuleTermZone();
          $model->load($post);
          if (!empty($id)) {
              $model = $this->findModel($id);
          } else {
              $model = new RuleTermZone();
          }
          return StController::render_form($this, $model, $id, $action);
      } else {
          throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }

    public function actionSave()
    {
      $return = false;
      $post = Yii::$app->request->post();

      if(!empty($id)){
          $model = RuleTermZone::findOne($id); //1. kalau ada id maka mode update
      }else{
          $model = new RuleTermZone(); //2. kalau tidak ada idnya maka save jadi record baru
      }
      if ($model->load($post)) {
          $modelFind = RuleTermZone::findOne($model->id);
          if ($modelFind !== null) {
              $model = $modelFind;
          }

          $model->load($post);
          $return = StController::valid_save($model);
      } else {
          $return = 'error : model not loaded';
      }
      return $return;
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }
    protected function findModel($id)
    {
        if (($model = RuleTermZone::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
