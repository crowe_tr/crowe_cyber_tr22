<?php

namespace backend\modules\st\controllers;

use Yii;
use common\models\st\Task;
use common\models\st\search\Task as TaskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\CmHelper;
use backend\modules\st\controllers\StController;
// use backend\modules\cm\controllers\CmController;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
      return StController::set_behaviors();
    }
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        return StController::set_index($searchModel, $this);
    }

    public function actionForm($id = null, $action=null)
    {
      if (Yii::$app->request->isAjax) {
          $post = Yii::$app->request->post();

          $model = new Task();
          $model->load($post);
          if (!empty($id)) {
              $model = $this->findModel($id);
          } else {
              $model = new Task();
          }
          return StController::render_form($this, $model, $id, $action);
      } else {
          throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }

    public function actionSave()
    {
      $return = false;
      $post = Yii::$app->request->post();

      if(!empty($id)){
          $model = Task::findOne($id); //1. kalau ada id maka mode update
      }else{
          $model = new Task(); //2. kalau tidak ada idnya maka save jadi record baru
      }
      if ($model->load($post)) {
          $modelFind = Task::findOne($model->id);
          if ($modelFind !== null) {
              $model = $modelFind;
          }

          $model->load($post);
          $return = StController::valid_save($model);
      } else {
          $return = 'error : model not loaded';
      }
      return $return;
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
