<?php

namespace backend\modules\st\controllers;

use Yii;
use yii\filters\VerbFilter;
use common\components\CmHelper;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;


// use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\MaskedInput;
// use yii\helpers\ArrayHelper;

use kartik\widgets\ActiveForm;
use kartik\widgets\DepDrop;
// use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\grid\GridView;
use kolyunya\yii2\widgets\MapInputWidget;
// use backend\modules\fn\components\FnHelper;
use dosamigos\switchery\Switchery;


class StController
{
  public static function set_behaviors()
  {
    return [
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['POST'],
            ],
        ],
    ];
  }

  public static function set_index($isearchModel,$imain_form)
  {
    $dataProvider = $isearchModel->search(Yii::$app->request->queryParams);
    return $imain_form->render('index', [
        'searchModel' => $isearchModel,
        'dataProvider' => $dataProvider,
    ]);
  }

  public static function render_form($imain_form, $imodel, $iid, $iaction)
  {
    return $imain_form->renderAjax('_form', [
        'model' => $imodel,
        'id'=> ($iaction=="duplicate") ? "" : $iid,
    ]);
  }

  public static function render_form2($imain_form, $idata, $imodel, $iid, $iaction)
  {
    return $imain_form->renderAjax('_form', [
        'model' => $imodel,
        'id'=> ($iaction=="duplicate") ? "" : $iid,
    ]);
  }

  public static function set_save($imodel, $ipost)
  {
    $return = false;
    if ($imodel->load($ipost)) {
      $return = $this->valid_save($imodel);
    } else {
        $return = 'error : model not loaded';
    }

    return $return;
  }

  public static function valid_save($imodel)
  {
    $return = false;
    $valid = $imodel->validate();
    if ($valid) {
      $flag = $imodel->save(false);
      if ($flag){
        $return = true;
      } elseif(!($flag)) {
        $return = CmHelper::set_error($imodel);
      }
    } else {
      $return = CmHelper::set_error($imodel);
    }

    return $return;
  }

  public static function btn_back($imodul, $isubmodul)
  {
    $rsubmodul = '/'.$imodul.'/'.$isubmodul;
    return Html::a(
      'BACK',
      [$rsubmodul],
      [
        'class' => 'btn btn-info text-white ',
      ]);
  }

  public static function btn_new()
  {
    return Html::a(
        '<i class="pg-plus"></i> <span class="hidden-xs">CREATE NEW</span>',
        false,
        [
          'onclick' => 'FormCreate()',
          'class' => 'btn btn-warning text-white ',
        ]
      );
  }

  public static function btn_to($imodul, $isubmodul, $icap)
  {
    $rsubmodul = '/'.$imodul.'/'.$isubmodul;
    return Html::a(
      $icap,
      [$rsubmodul],
      [
        'class' => 'btn btn-info text-white ',
      ]);
  }

  public static function set_column_bool($iattribute, $icol, $iinit)
  {
    if ($iinit == 1) {
      $vyes = 'Yes';
      $vno = 'No';
    } elseif ($iinit == 2) {
      $vyes = 'Active';
      $vno = 'Non Active';
    }

    return CmHelper::set_column_bool($iattribute,$icol,$vyes,$vno,
                                      function($model) use ($iattribute, $vyes, $vno){
                                        if ($model->$iattribute == 1) {
                                          return $vyes;
                                        }
                                        else{
                                          return $vno;
                                        }
                                      }
                                    );
  }

  public static function set_icon($imodul, $isubmodul)
  {
    return [
        'header' => '<i class="fa fa-cogs"></i>',
        'headerOptions' => ['class' => 'bg-success text-center'],
        'filterOptions' => ['class' => 'b-b b-grey'],
        'contentOptions' => ['class' => 'no-padding'],
        'format' => 'raw',
        'value' => function($data) use ($imodul, $isubmodul){
          return CmHelper::set_icon2($data->id, $imodul,$isubmodul);
        },
    ];
  }

  public static function set_icon3($imodul, $isubmodul)
  {
    return [
        'header' => '<i class="fa fa-cogs"></i>',
        'headerOptions' => ['class' => 'bg-success text-center'],
        'filterOptions' => ['class' => 'b-b b-grey'],
        'contentOptions' => ['class' => 'no-padding'],
        'format' => 'raw',
        'value' => function($data) use ($imodul, $isubmodul){
          return CmHelper::set_icon3($data->id, $imodul,$isubmodul);
        },
    ];
  }

  public static function lookup_input($icol, $imain_form, $imodel, $iattribute1, $ival, $iattribute2, $iid = null)
  {
    $vid = 'id';
    if (!is_null($iid)) {
      $vid = $iid;
    }
    $data = '<div class="col-md-'.$icol.'">'.
              $imain_form->field($imodel, $iattribute1,
                ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
                ->widget(
                  Select2::classname(),
                    [
                      'data' => ArrayHelper::map($ival, $vid, $iattribute2),
                      'options' => [$vid => 'type', 'placeholder' => 'Select ...'],
                    ]
                )
            .'</div>';
    return $data;
  }

  public static function lookup_input2($icol, $imain_form, $imodel, $iattribute1, $ival, $iattribute2, $iid = null)
  {
    $vid = 'id';
    if (!is_null($iid)) {
      $vid = $iid;
    }
    // $data = '<div class="col-md-'.$icol.'">'.
    //           $imain_form->field($imodel, $iattribute1,
    //             ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
    //             ->widget(
    //               Select2::classname(),
    //                 [
    //
    //                   'data' => ArrayHelper::map($ival, $vid, $iattribute2),
    //                   'options' => [$vid => 'type', 'placeholder' => 'Select ...'],
    //                   'pluginOptions' => [
    // 									  'minimumInputLength' => 3,
    //                   ]
    //                 ]
    //             )
    //         .'</div>';
    // return $data;
    return self::lookup_input3($icol, $imain_form, $imodel, $iattribute1,
        ['options' => ['class' => ' form-group form-group-default form-group-default-select2']],
        [
          'data' => ArrayHelper::map($ival, $vid, $iattribute2),
          'options' => [$vid => 'type', 'placeholder' => 'Select ...'],
          'pluginOptions' => [
					  'minimumInputLength' => 3,
          ]
        ],
        $ival, $iattribute2, $iid);
  }

  public static function lookup_input3($icol, $imain_form, $imodel, $iattribute1, $ifield_arr, $iwidget_array)
  {
    // $vid = 'id';
    // if (!is_null($iid)) {
    //   $vid = $iid;
    // }
    $data = '<div class="col-md-'.$icol.'">'.
              $imain_form->field($imodel, $iattribute1,$ifield_arr)
                          ->widget(Select2::classname(),$iwidget_array)
            .'</div>';
    return $data;
  }

  public static function lookup_multi($icol, $imain_form, $imodel, $iattribute1, $ival)
  {
    $data = '<div class="col-md-'.$icol.'">'.
              $imain_form->field($imodel, $iattribute1,
                ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
                ->widget(
                  Select2::classname(),
                    [
                      'data' => ArrayHelper::map($ival, $iattribute1, $iattribute1),
                      'options' => ['placeholder' => 'Select Zone ...', 'multiple' => true],
                      'pluginOptions' => [
                          'tokenSeparators' => [','],
                      ]
                    ]
                )
            .'</div>';
    return $data;
  }

  public static function column_attribut($iclass = null, $idisable = null, $istyle = null, $iwidth = null)
  {
    $data = [];
    if (!is_null($iclass)) {
      $data['class'] = $iclass;
    }
    if (!is_null($idisable)) {
      $data['disabled'] = $idisable;
    }
    if (!is_null($istyle)) {
      $data['style'] = $istyle;
    }
    if (!is_null($iwidth)) {
      $data['width'] = $iwidth;
    }

    return $data;
  }

  public static function column_data($ifield, $ititle = '', $itype = null, $imulti_value = false)
  {

    $data = [
      'name'  => $ifield,
      'title' => $ititle,
    ];
    if (!is_null($itype)) {
      $data['type'] = $itype;
    }
    if (!($imulti_value)) {
      $data['value'] = function($data) use($ifield){
        return !empty($data[$ifield]) ? $data[$ifield] : "";
      };
    } else {
      $data['value'] = function($data) use($ifield){
        return !empty($data[$ifield]) ? explode(',', $data[$ifield]) : "";
      };
    }
    return $data;
  }

  public static function option_number()
  {
    return [
      'clientOptions' => [
        'alias' => 'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true,
      ],
      'options' => [
        'class' => 'form-control',
      ]
    ];
  }
}
