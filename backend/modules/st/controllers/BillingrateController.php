<?php

namespace backend\modules\st\controllers;

use Yii;
use common\models\st\BillingRate;
use common\models\st\BillingRateDetail;
use common\models\st\search\BillingRate as BillingRateSearch;
use common\components\CommonHelper;

use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\cm\Level;
use common\components\CmHelper;
use backend\modules\st\controllers\StController;

class BillingrateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
      return StController::set_behaviors();
    }
    public function actionIndex()
    {
        $searchModel = new BillingRateSearch();
        return StController::set_index($searchModel, $this);
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new BillingRate();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);

                if($action=="duplicate"){
                    $model->id = null;
                    $model->isNewRecord;
                }

                $modelDetail = new BillingRateDetail();
                $sql = "call common_list('get-billing-rate',".$model->id.");";
                $rates = Yii::$app->db->createCommand($sql)->queryAll();

                $tmpRates = [];
                $i = 0;
                foreach($rates as $rate){
                    $tmpRates[$i]['level_id'] = $rate['level_id'];
                    $tmpRates[$i]['level_name'] = $rate['level_name'];
                    $tmpRates[$i]['billing_rate_value'] = $rate['billing_rate_value'];
                    $i++;
                }
                $modelDetail->TabularInput = $tmpRates;
            } else {
                $model = new BillingRate();
                $modelDetail = new BillingRateDetail();

                $sql = "call common_list('get-billing-rate',null);";
                $levels = Yii::$app->db->createCommand($sql)->queryAll();
                $tmpRates = [];
                $i = 0;
                foreach($levels as $level){
                    $tmpRates[$i]['level_id'] = $level['level_id'];
                    $tmpRates[$i]['level_name'] = $level['level_name'];
                    $tmpRates[$i]['billing_rate_value'] = $level['billing_rate_value'];
                    $i++;
                }
                $modelDetail->TabularInput = $tmpRates;
            }
            return $this->renderAjax('_form', [
                'model' => $model,
                'modelDetail' => $modelDetail,
                'id'=> ($action=="duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave()
    {
        $return = false;
        $model = new BillingRate();
        $modelDetail = new BillingRateDetail();

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $modelFind = BillingRate::findOne($model->id);
            if ($modelFind !== null) {
                $model = $modelFind;
            }

            $model->load($post);
            $modelDetail->load($post);
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    } else {
                        $rates = $modelDetail->TabularInput;
                        foreach($rates as $t){
                            $t['billing_rate_value'] = !empty($t['billing_rate_value']) ? $t['billing_rate_value'] : 0;
                            $modelDetail = BillingRateDetail::find()->where([
                                'billing_rate_id' => $model->id,
                                'level_id' => $t['level_id']
                            ])->one();

                            if(empty($modelDetail->level_id)){
                                $modelDetail = new BillingRateDetail();
                            }
                            $modelDetail->billing_rate_id = $model->id;
                            $modelDetail->level_id = $t['level_id'];
                            $modelDetail->billing_rate_value = CommonHelper::ResetDecimal($t['billing_rate_value']);

                            if (!($flag = $modelDetail->save(false))) {
                                $transaction->rollBack();
                                $return = 'error : when saving target data';
                            }

                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }

    public function actionDelete($id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            BillingRateDetail::deleteAll([
                'billing_rate_id' => $id,
            ]);

            if (!($flag = $this->findModel($id)->delete() )) {
                $transaction->rollBack();
                $return = 'error : rollback';
            }
            if ($flag) {
                $transaction->commit();
                $return = true;
            }

        } catch (Exception $e) {
            $transaction->rollBack();
        }


    }
    protected function findModel($id)
    {
        if (($model = BillingRate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
