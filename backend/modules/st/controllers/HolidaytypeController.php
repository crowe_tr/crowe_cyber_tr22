<?php

namespace backend\modules\st\controllers;

use Yii;
use common\models\st\HolidayType;
use common\models\st\search\HolidayType as HolidayTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\components\CmHelper;
use backend\modules\st\controllers\StController;

class HolidaytypeController extends Controller
{
    public function behaviors()
    {
      return StController::set_behaviors();
    }

    public function actionIndex()
    {
        $searchModel = new HolidayTypeSearch();
        return StController::set_index($searchModel, $this);
    }

    public function actionForm($id = null, $action=null)
    {
      if (Yii::$app->request->isAjax) {
          $post = Yii::$app->request->post();
          $model = new HolidayType();
          $model->load($post);
          if (!empty($id)) {
              $model = $this->findModel($id);
          } else {
              $model = new HolidayType();
          }
          return StController::render_form($this, $model, $id, $action);
      } else {
          throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }

    public function actionSave()
    {
      $return = false;
      $model = new HolidayType();
      $post = Yii::$app->request->post();

      if(!empty($id)){
          $model = HolidayType::findOne($id); //1. kalau ada id maka mode update
      }else{
          $model = new HolidayType(); //2. kalau tidak ada idnya maka save jadi record baru
      }
      if ($model->load($post)) {
          $modelFind = HolidayType::findOne($model->id);
          if ($modelFind !== null) {
              $model = $modelFind;
          }

          $model->load($post);
          $return = StController::valid_save($model);
      } else {
          $return = 'error : model not loaded';
      }

      return $return;
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }
    protected function findModel($id)
    {
        if (($model = HolidayType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
