<?php

namespace backend\modules\st\controllers;

use Yii;
use common\models\st\RuleItem;
use common\models\st\search\RuleItem as RuleItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\components\CmHelper;
use backend\modules\st\controllers\StController;

class RuleitemController extends Controller
{
    public function behaviors()
    {
      return StController::set_behaviors();
    }

    public function actionIndex()
    {
      $searchModel = new RuleItemSearch();
      return StController::set_index($searchModel, $this);
    }

    public function actionForm($id = null, $action=null)
    {
      if (Yii::$app->request->isAjax) {
          $post = Yii::$app->request->post();
          $data = array();

          $model = new RuleItem();
          $model->load($post);
          if (!empty($id)) {
              $model = $this->findModel($id);
              $model->term_zone_name = explode(',', $model->term_zone_name);
          } else {
              $model = new RuleItem();
          }
          return StController::render_form2($this, $data, $model, $id, $action);
      } else {
          throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }

    public function actionSave()
    {
      $return = false;
      $post = Yii::$app->request->post();

      if(!empty($id)){
          $model = RuleItem::findOne($id); //1. kalau ada id maka mode update
      }else{
          $model = new RuleItem(); //2. kalau tidak ada idnya maka save jadi record baru
      }
      if ($model->load($post)) {
          $modelFind = RuleItem::findOne($model->id);
          if ($modelFind !== null) {
              $model = $modelFind;
          }

          $model->load($post);
          $model->term_zone_name = implode(',', $model->term_zone_name);
          $return = StController::valid_save($model);
      } else {
          $return = 'error : model not loaded';
      }
      return $return;
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }
    protected function findModel($id)
    {
        if (($model = RuleItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
