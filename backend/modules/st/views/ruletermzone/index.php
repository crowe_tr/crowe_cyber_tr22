<?php
  use yii\web\View;
  use kartik\grid\GridView;
  use common\components\CmHelper;
  use backend\modules\st\controllers\StController;

  CmHelper::title_only($this, 'Setup Term Zone', 'SETUP');
  $this->params['breadcrumbs_btn'] = StController::btn_back('st','rules').'&nbsp;'.StController::btn_new();
  $this->params['crud_ajax'] = true;

  $column = [
              CmHelper::set_column_no(),
              CmHelper::set_column('term_zone_name', 'Zone', '2'),
              CmHelper::set_column('term_zone_description', 'Description', '6'),
              StController::set_column_bool('is_stayed','2',1),
              StController::set_column_bool('status','2',1),
              StController::set_icon('st', 'ruletermzone'),
            ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();
?>
