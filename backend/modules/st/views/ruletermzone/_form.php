<?php
  use yii\widgets\ActiveForm;
  use yii\web\View;
  use common\models\cm\Level;
  use common\components\CmHelper;

  echo CmHelper::title_only($this, 'Rule Term Zone');
  $form = CmHelper::set_active_form();
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

  echo CmHelper::row_empty12();
  echo CmHelper::row_group_header();
  echo CmHelper::set_text_input('2', $form, $model, 'term_zone_name');
  echo CmHelper::set_text_input('6', $form, $model, 'term_zone_description');
  echo CmHelper::set_checkbox_input('2', $form, $model, 'is_stayed', 1);
  echo CmHelper::set_checkbox_input('2', $form, $model, 'status', 1);
  echo CmHelper::row_group_footer();
  echo CmHelper::set_button_input();

  ActiveForm::end();

  $js = CmHelper::script_input2('$form');
  $this->registerjs($js, View::POS_END);
?>
