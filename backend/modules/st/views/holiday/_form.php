<?php
  use yii\widgets\ActiveForm;
  use common\models\st\HolidayType;
  use common\components\CmHelper;
  use yii\web\View;
  use backend\modules\st\controllers\StController;

  echo CmHelper::title_only($this, 'Holiday');
  $form = CmHelper::set_active_form();
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

  echo CmHelper::row_group_header();
  echo CmHelper::set_field_date('3', $form, $model, 'holiday_date');
  echo StController::lookup_input('3', $form, $model, 'type_id', HolidayType::find()->all(), 'holiday_type_name');
  echo CmHelper::set_text_input('6', $form, $model, 'description');
  echo CmHelper::row_group_footer();
  echo CmHelper::set_button_input();

  ActiveForm::end();
  $js = CmHelper::script_input2('$form');
  $this->registerjs($js, View::POS_END);
?>
