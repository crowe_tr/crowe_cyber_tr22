<?php
  use yii\helpers\ArrayHelper;
  use kartik\grid\GridView;
  use common\models\st\HolidayType;
  use common\components\CmHelper;
  use backend\modules\st\controllers\StController;

  CmHelper::title_only($this, 'Holiday', 'SETUP');
  $this->params['breadcrumbs_btn'] = StController::btn_to('st','holidaytype','TYPE').'&nbsp;'.StController::btn_new();
  $this->params['crud_ajax'] = true;

  $column = [
              CmHelper::set_column_no(),
              CmHelper::set_column('holiday_date', 'Holiday Date', '2'),
              CmHelper::set_column_bool2('type_id','3',
                  ArrayHelper::map(HolidayType::find()->distinct()->all(), 'id', 'holiday_type_name'),
                  'type.holiday_type_name'),
              CmHelper::set_column('description', 'Description', '7'),
              StController::set_icon('st', 'holiday'),
            ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();
?>
