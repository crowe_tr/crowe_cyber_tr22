<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\st\TaskType;
use yii\web\View;
use common\components\CmHelper;



$this->title = 'Create Task';
$this->params['breadcrumbs'][] = ['label' => 'Task', 'url' => ['index']];

$form = CmHelper::set_active_form();
echo $form->errorSummary($model);
echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

echo CmHelper::row_empty12();
echo CmHelper::row_group_header();
echo CmHelper::set_text_input('2', $form, $model, 'seq');
echo CmHelper::set_lookup_input('3',
  $form->field($model, 'task_type_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
          Select2::classname(),
          [
                  'data' => ArrayHelper::map(TaskType::find()->all(), 'id', 'task_type_name'),
                  'options' => ['id' => 'type', 'placeholder' => 'Select ...'],
          ]
  )
);
echo CmHelper::set_text_input('7', $form, $model, 'task_name');
echo CmHelper::row_group_footer();

echo CmHelper::set_button_input();

ActiveForm::end();

$js = CmHelper::script_input2('$form');
$this->registerjs($js, View::POS_END);
?>
