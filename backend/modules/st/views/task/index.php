<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use common\models\st\TaskType;
use common\components\CmHelper;

echo CmHelper::title_only($this, 'Task List', 'SETUP');
$this->params['breadcrumbs_btn'] = Html::a(
    'TYPE',
    ['/st/tasktype'],
    [
      'class' => 'btn btn-info text-white ',
    ]
  );
$this->params['crud_ajax'] = true;

$column = [
            CmHelper::set_column_no(),
            CmHelper::set_column_bool2('task_type_id','3',
                ArrayHelper::map(TaskType::find()->distinct()->all(), 'id', 'task_type_name'),
                'type.task_type_name'),
            CmHelper::set_column('task_name', 'Task Name', '9'),

        [
          'header' => '',
          'headerOptions' => ['class' => 'bg-success text-center'],
          'filterOptions' => ['class' => 'b-b b-grey'],
          'contentOptions' => ['class' => 'no-padding'],
          'format' => 'raw',
          'value' => function ($data) {
            return CmHelper::set_icon3($data->id, 'st','task');
          },
        ],
    ];

    \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
    echo CmHelper::widget_pjax();
    echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
    \yii\widgets\Pjax::end();
?>
