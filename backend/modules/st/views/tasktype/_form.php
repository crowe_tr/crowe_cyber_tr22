<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\st\TaskType;
use dosamigos\switchery\Switchery;
use yii\web\View;
use common\components\CmHelper;

$this->title = 'Create Task Type';
$this->params['breadcrumbs'][] = ['label' => 'Task Type', 'url' => ['index']];

$form = CmHelper::set_active_form();
echo $form->errorSummary($model);
echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

echo CmHelper::row_empty12();
echo CmHelper::row_group_header();
echo CmHelper::set_text_input('9', $form, $model, 'task_type_name');
echo CmHelper::set_checkbox_input('3', $form, $model, 'flag');
?>
</div>
</div>
<div class="form-group-attached">
    <div class="row p-b-5">
<?php
      echo CmHelper::set_switch('3', $form, $model, 'is_meal');
      echo CmHelper::set_switch('3', $form, $model, 'is_ope');
      echo CmHelper::set_switch('3', $form, $model, 'is_taxi');
      echo CmHelper::set_switch('3', $form, $model, 'used_project');
?>
    </div>
</div>

<?php
echo CmHelper::set_button_input();

ActiveForm::end();
$js = CmHelper::script_input2('$form');
$this->registerjs($js, View::POS_END);

?>
