<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use common\models\st\TaskType;
use common\components\CmHelper;
use backend\modules\st\controllers\StController;

CmHelper::title_only($this, 'Task Type', 'SETUP');
// $this->params['breadcrumbs_btn'] = Html::a(
//     'BACK',
//     ['/st/task'],
//     [
//       'class' => 'btn btn-info text-white ',
//     ]
//   ).'&nbsp;'.Html::a(
//     '<i class="pg-plus"></i> CREATE NEW',
//     false,
//     [
//       'onclick' => 'FormCreate()',
//       'class' => 'btn btn-warning text-white ',
//     ]
//   );
$this->params['breadcrumbs_btn'] = StController::btn_back('st','task').'&nbsp;'.StController::btn_new();
$this->params['crud_ajax'] = true;

$column = [
            CmHelper::set_column_no(),
            CmHelper::set_column('task_type_name', 'Type Name', '10'),
            CmHelper::set_column_bool('flag','2','Active','Non Active',
                                        function($model){
                                              if ($model->flag == 1) {
                                                return 'Active';
                                              }
                                              else{
                                                return 'Non Active';
                                              }
                                            }
                                      ),
            [
                'header' => '<i class="fa fa-cogs"></i>',
                'headerOptions' => ['class' => 'bg-success text-center'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'no-padding'],
                'format' => 'raw',
                'value' => function($data){
                  return CmHelper::set_icon2($data->id, 'st','tasktype');
                },
            ],
          ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();
?>
