<?php
  use kartik\grid\GridView;
  use yii\web\View;
  use common\models\st\HolidayType;
  use common\components\CmHelper;
  use backend\modules\st\controllers\StController;

  CmHelper::title_only($this, 'Holiday Type', 'SETUP');
  $this->params['breadcrumbs_btn'] = StController::btn_back('st','holiday').'&nbsp;'.StController::btn_new();
  $this->params['crud_ajax'] = true;

  $column = [
              CmHelper::set_column_no(),
              CmHelper::set_column('holiday_type_name', 'Holiday Type Name', '8'),
              StController::set_column_bool('is_holiday','2',1),
              StController::set_column_bool('flag','2',2),
              StController::set_icon('st', 'holidaytype'),
            ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();
?>
