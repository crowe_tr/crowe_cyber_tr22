<?php
  use yii\widgets\ActiveForm;
  use common\models\st\HolidayType;
  use common\components\CmHelper;
  use yii\web\View;

  echo CmHelper::title_only($this, 'Holiday Type');
  $form = CmHelper::set_active_form();
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

  echo CmHelper::row_group_header();
  echo CmHelper::set_text_input('6', $form, $model, 'holiday_type_name');
  echo CmHelper::set_switch('3', $form, $model, 'is_holiday');
  echo CmHelper::set_field_bool('3', $form, $model, 'flag', 2);
  echo CmHelper::row_group_footer();
  echo CmHelper::set_button_input();

  ActiveForm::end();
  $js = CmHelper::script_input2('$form');
  $this->registerjs($js, View::POS_END);
?>
