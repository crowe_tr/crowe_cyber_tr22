<?php
  use yii\widgets\ActiveForm;
  use common\models\cm\Level;
  use common\models\st\RuleTermZone;
  use common\components\CmHelper;
  use backend\modules\st\controllers\StController;
  use yii\web\View;

  echo CmHelper::title_only($this, 'Rule Item');
  $form = CmHelper::set_active_form();
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

  echo CmHelper::row_empty12();
  echo CmHelper::row_group_header();
  echo CmHelper::set_text_input('8', $form, $model, 'term_item_name');
  echo CmHelper::set_switch('4', $form, $model, 'is_client_zone');
  echo CmHelper::row_foot_head();
  echo CmHelper::set_text_input('12', $form, $model, 'term_item_desc');
  echo CmHelper::row_foot_head();
  echo StController::lookup_multi('12', $form, $model, 'term_zone_name', RuleTermZone::find()->All());
  echo CmHelper::row_group_footer();
  echo CmHelper::set_button_input();

  ActiveForm::end();
  $js = CmHelper::script_input2('$form');
  $this->registerjs($js, View::POS_END);
?>
