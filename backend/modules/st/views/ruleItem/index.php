<?php
  use yii\web\View;
  use kartik\grid\GridView;
  use common\components\CmHelper;
  use backend\modules\st\controllers\StController;

  CmHelper::title_only($this, 'Setup Rules Item', 'SETUP');
  $this->params['breadcrumbs_btn'] = StController::btn_back('st','rules');
  $this->params['crud_ajax'] = true;

  $column = [
              CmHelper::set_column_no(),
              CmHelper::set_column('term_item_name', 'Name', '3'),
              CmHelper::set_column('term_zone_name', 'Zone Name', '3'),
              CmHelper::set_column('term_item_desc', 'Description', '4'),
              StController::set_column_bool('is_client_zone','2',1),
              StController::set_icon3('st', 'ruleitem'),
            ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();
?>
