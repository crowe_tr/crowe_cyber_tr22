<?php
  use kartik\grid\GridView;
  use common\components\CmHelper;
  use backend\modules\st\controllers\StController;

  CmHelper::title_only($this, 'Setup Rules', 'SETUP');
  $this->params['breadcrumbs_btn'] = StController::btn_to('st','ruletermzone','TERM ZONE').
    '&nbsp;'.StController::btn_to('st','ruleitem','ITEM').
    '&nbsp;'.StController::btn_new();
  $this->params['crud_ajax'] = true;

  $column = [
              CmHelper::set_column_no(),
              CmHelper::set_column('description', 'Description', '9'),
              CmHelper::set_column('effective_date', 'Effective Date', '3'),
              StController::set_icon('st', 'rules'),
            ];

      \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
      echo CmHelper::widget_pjax2();
      echo GridView::widget( CmHelper::grid_view2($dataProvider, $searchModel, $column) );
      \yii\widgets\Pjax::end();
?>
