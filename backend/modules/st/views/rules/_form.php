<?php
  use yii\helpers\Html;
  use yii\widgets\ActiveForm;
  use kartik\widgets\DatePicker;
  use unclead\multipleinput\MultipleInput;
  use kartik\widgets\Select2;
  use yii\widgets\MaskedInput;
  use yii\helpers\ArrayHelper;
  use kartik\checkbox\CheckboxX;
  use kartik\widgets\TimePicker;

  use common\models\cm\Level;
  use common\models\st\RuleItem;
  use common\models\st\RuleTermZone;
  use common\components\CmHelper;
  use backend\modules\st\controllers\StController;
  use yii\web\View;

  echo CmHelper::title_only($this, 'Form Setup Rules');
  $form = CmHelper::set_active_form();
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

  echo CmHelper::row_empty12();
  echo CmHelper::row_group_header();
  echo CmHelper::set_field_date('2', $form, $model, 'effective_date');
  echo CmHelper::set_text_input('10', $form, $model, 'description');

  echo CmHelper::row_foot_head();
  echo $form->field($modelLevel, 'TabularInput', ['options'=>['class'=>'table table-bordered']])->widget(
    MultipleInput::className(), [
      'iconSource' => 'fa',
      'theme'=>'default',
      'sortable'=>false,
      'addButtonPosition' => MultipleInput::POS_HEADER,
      'removeButtonOptions'=>[
        'class'=>'btn btn-danger btn-sm',
      ],
      'addButtonOptions'=>[
        'class'=>'btn btn-warning btn-sm',
      ],

      'columns' => [
        array_merge(StController::column_data('item_id'),
          array('options' => StController::column_attribut('hidden', null, 'color: black;', null)),
          array('columnOptions' => StController::column_attribut('', null, 'padding:0 !Important; border:0;', null)),
          array('headerOptions' => StController::column_attribut('', null, 'padding:0 !Important; border:0;', '10px')),
        ),
        array_merge(StController::column_data('term_item_name', 'RULE'),
          array('options' => StController::column_attribut('form-control', true, 'color: black;', null)),
          array('columnOptions' => StController::column_attribut('col-md-3', null, '', null)),
          array('headerOptions' => StController::column_attribut('bg-info text-white text-center', null, '', null)),
        ),
        array_merge(StController::column_data('qty_claim', 'Claim Qty', MaskedInput::className()),
          array('headerOptions' => StController::column_attribut('bg-success text-center', null, null, null)),
          array('columnOptions' => StController::column_attribut('col-md-1', null, null, null)),
          array('options' => StController::option_number()),
        ),
        array_merge(StController::column_data('qty_claim_stayed', 'Claim Stayed', MaskedInput::className()),
          array('headerOptions' => StController::column_attribut('bg-success text-center', null, null, null)),
          array('columnOptions' => StController::column_attribut('col-md-1', null, null, null)),
          array('options' => StController::option_number()),
        ),
        array_merge(StController::column_data('stayed_allowance', 'Stayed Allowance', MaskedInput::className()),
          array('headerOptions' => StController::column_attribut('bg-success text-center', null, null, null)),
          array('columnOptions' => StController::column_attribut('col-md-1', null, null, null)),
          array('options' => StController::option_number()),
        ),
        array_merge(StController::column_data('level_description', 'Applied To', Select2::classname(), true),
          array('headerOptions' => StController::column_attribut('bg-success text-white text-center', null, null, null)),
          array('columnOptions' => StController::column_attribut('col-md-9', null, null, null)),
          array('options' => [
            'data' => ArrayHelper::map(Level::find()->asArray()->all(), 'level_name', 'level_name'),
            'options' => ['placeholder' => '', 'multiple' => true],
            'pluginOptions' => [
              'tokenSeparators' => [','],
            ]
          ]),
        ),
    ]
  ])->label(false);

  echo CmHelper::row_foot_head();
  echo $form->field($modelDetail, 'TabularInput', ['options'=>['class'=>'table table-bordered']])->widget(
    MultipleInput::className(), [
      'iconSource' => 'fa',
      'theme'=>'default',
      'sortable'=>false,
      'addButtonPosition' => MultipleInput::POS_HEADER,
      'removeButtonOptions'=>[
        'class'=>'btn btn-danger btn-sm',
      ],
      'addButtonOptions'=>[
        'class'=>'btn btn-warning btn-sm',
      ],

      'columns' => [
        array_merge(StController::column_data('item_id'),
          array('options' => StController::column_attribut('hidden', null, 'color: black;', null)),
          array('columnOptions' => StController::column_attribut('', null, 'padding:0 !Important; border:0;', null)),
          array('headerOptions' => StController::column_attribut('', null, 'padding:0 !Important; border:0;', '10px')),
        ),
        array_merge(StController::column_data('term_item_name', 'RULE'),
          array('options' => StController::column_attribut('form-control', true, 'color: black;', null)),
          array('columnOptions' => StController::column_attribut('col-md-5', null, '', null)),
          array('headerOptions' => StController::column_attribut('bg-info text-white text-center', null, '', null)),
        ),
        array_merge(StController::column_data('term_id'),
          array('options' => StController::column_attribut('hidden', null, 'color: black;', null)),
          array('columnOptions' => StController::column_attribut('hidden', null, null, null)),
          array('headerOptions' => StController::column_attribut('hidden', null, null, null)),
        ),
        array_merge(StController::column_data('term_zone_name', 'ZONE'),
          array('options' => StController::column_attribut('form-control', true, 'color: black;', null)),
          array('columnOptions' => StController::column_attribut('col-md-2', null, '', null)),
          array('headerOptions' => StController::column_attribut('bg-info text-white text-center', null, '', null)),
        ),
        array_merge(StController::column_data('min_overtime', 'Min Overtime', MaskedInput::className()),
          array('headerOptions' => StController::column_attribut('bg-success text-center', null, null, null)),
          array('columnOptions' => StController::column_attribut('col-md-1', null, null, null)),
          array('options' => StController::option_number()),
        ),
        array_merge(StController::column_data('use_taxi', 'Use Taxi?', Select2::classname()),
          array('headerOptions' => StController::column_attribut('bg-success text-center', null, null, null)),
          array('columnOptions' => StController::column_attribut('col-md-1', null, null, null)),
          array('options' => [
            'class' => 'form-control',
            'data' => ['No','Yes'],
          ],),
        ),
        array_merge(StController::column_data('min_clock', 'Min Clock', TimePicker::className()),
          array('headerOptions' => StController::column_attribut('bg-success text-center', null, null, null)),
          array('columnOptions' => StController::column_attribut('col-md-1', null, null, null)),
          array('options' => [
            'class' => '',
            'pluginOptions' => [
              'showSeconds' => false,
              'showMeridian' => false,
              'minuteStep' => 1,
              'secondStep' => 5,
              'template' => false
            ],]),
        ),
        array_merge(StController::column_data('allowance_amount', 'Allowance Amount', MaskedInput::className()),
          array('headerOptions' => StController::column_attribut('bg-success text-center', null, null, null)),
          array('columnOptions' => StController::column_attribut('col-md-2', null, null, null)),
          array('options' => StController::option_number()),
        ),
    ]
  ])->label(false);
  echo CmHelper::row_group_footer();
  echo CmHelper::set_button_input();

  ActiveForm::end();
  $js = CmHelper::script_input2('$form');
  $this->registerjs($js, View::POS_END);
?>
