<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use unclead\multipleinput\MultipleInput;
use kartik\widgets\Select2;
use yii\widgets\MaskedInput;
use common\models\cm\Level;
use yii\helpers\ArrayHelper;
use common\components\CmHelper;
use yii\web\View;
use backend\modules\st\controllers\StController;


  echo CmHelper::title_only($this, 'Billing Rates');
  $form = CmHelper::set_active_form();
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

  echo CmHelper::row_empty12();
  echo CmHelper::row_group_header();
  echo CmHelper::set_field_date('3', $form, $model, 'effective_date');
  echo CmHelper::set_text_input('9', $form, $model, 'description');
  echo CmHelper::row_group_footer();


?>
    <div class="row">
      <div class="col-md-12">
      <?php
      echo $form->field($modelDetail, 'TabularInput', ['options'=>['class'=>'table']])->widget(
        MultipleInput::className(), [
          'iconSource' => 'fa',
          'theme'=>'default',
          'sortable'=>false,
          'addButtonPosition' => MultipleInput::POS_HEADER,
          'removeButtonOptions'=>[
            'class'=>'btn btn-danger btn-sm',
          ],
          'addButtonOptions'=>[
            'class'=>'btn btn-warning btn-sm',
          ],
          'columns' => [
            [
              'name'  => 'level_id',
              'title' => '',
              'value' => function($data) {
                return !empty($data['level_id']) ? $data['level_id'] : "";
              },
              'options' => [
                'class' => 'hidden',
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => '',
                'style'=>'padding:0 !Important; border:0',

              ],
              'headerOptions' => [
                'class'=>'',
                'style'=>'padding:0 !Important; border:0',
                'width'=>'10px'
              ]
            ],
            [
              'name'  => 'level_name',
              'title' => 'Level Name',
              'value' => function($data) {
                return !empty($data['level_name']) ? $data['level_name'] : "";
              },
              'options' => [
                'class' => 'form-control',
                'disabled'=>true,
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => 'col-md-7',

              ],
              'headerOptions' => [
                'class'=>'bg-info text-white text-center'
              ]

          ],
          [
            'name'  => 'billing_rate_value',
            'title' => 'Rate',
            'type'=>MaskedInput::className(),
            'value' => function($data) {
              return $data['billing_rate_value'];
            },
            'options' => [
              'clientOptions' => [
                'alias' => 'decimal',
                'groupSeparator' => ',',
                'autoGroup' => true,
              ],
              'options' => [
                'class' => 'form-control',
              ]
            ],
              'columnOptions' => [
                'class' => 'col-md-5',

              ],
              'headerOptions' => [
                'class'=>'bg-info text-white text-center'
              ]

          ],

        ]
      ])->label(false);
      ?>
      </div>
    </div>
  <div class="row">
    <div class="col-md-12 text-right m-t-5">
      <hr class="m-b-5"/>
      <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
      <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
    </div>
  </div>

<?php ActiveForm::end();  ?>
<script type="text/javascript">
$('#crud-form').on('beforeSubmit', function() {
  var $form = new FormData($('#crud-form')[0]);
  $.ajax({
    url: paramJs.urlFormSave,
    type: 'POST',
    data: $form,
    async: false,
    cache: false,
    contentType: false,
    processData: false,

    success: function (data) {
      if(data != 1){
        $('#error-summary').html('<div class="alert alert-danger">' + data + '</div>');
      }else{
        $('#crud-modal').modal('hide');
        reload();
      }
    },
    error: function(jqXHR, errMsg) {
      alert(errMsg);
    }
  });
  return false;
});
</script>
