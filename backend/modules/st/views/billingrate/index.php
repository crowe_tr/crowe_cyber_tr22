<?php
  use yii\web\View;
  use kartik\grid\GridView;
  use common\components\CmHelper;
  use backend\modules\st\controllers\StController;

  CmHelper::title_only($this, 'Billing Rates', 'SETUP');
  $this->params['breadcrumbs_btn'] = StController::btn_new();
  $this->params['crud_ajax'] = true;

  $column = [
              CmHelper::set_column_no(),
              CmHelper::set_column('description', 'Description', '10'),
              CmHelper::set_column('effective_date', 'Effective Date', '2'),
              StController::set_icon('st', 'billingrate'),
            ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();
?>
