<?php

namespace backend\modules\cl\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\components\CommonHelper;
use common\models\cl\ClientIndustries;
use common\models\cl\search\ClientIndustries as ClientIndustriesSearch;

class ClientindustriesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CmClientIndustriesSearch();
        $searchModel->Status = 1;
        $searchModel->Flag = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new CmClientIndustries();
        
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);
                if($action=="duplicate"){
                    $model->Id = null;
                    $model->isNewRecord;
                }
            } else {
                $model = new CmClientIndustries();
            }

            return $this->renderAjax('_form', [
                'model' => $model,
                'id' => ($action=="duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave($id=null) 
    {
        $return = false;
        $post = Yii::$app->request->post();

        if(!empty($id)){ 
            $model = CmClientIndustries::findOne($id); // Update
            $model->LastUpdateBy = CommonHelper::getSessionIdUser();
            $model->LastUpdateDate = date('Y-m-d H:i:s');
        }else{
            $model = new CmClientIndustries(); // New Record
            $model->Status = 1;
            $model->Flag = 1;
            $model->CreatedBy = CommonHelper::getSessionIdUser();
        }
        if ($model->load($post)) {
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    }
                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $return = 'error : validation not valid : '.Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->DeletedBy = CommonHelper::getSessionIdUser();
        $model->DeletedDate = date('Y-m-d H:i:s');
        $model->Status = 0;
        $model->save();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = CmClientIndustries::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}