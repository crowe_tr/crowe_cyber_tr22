<?php

namespace backend\modules\cl\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Html;

use common\components\CommonHelper;
use common\models\cl\Client;
use common\models\cl\search\Client as ClientSearch;
use common\models\cl\ClientIndustries;
use common\models\cm\Entity;
use common\models\cm\Division;

class ClientController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    private function getDataForForm($model)
    {
        $data = array();

        $ClientIndustries_opt = ArrayHelper::map(ClientIndustries::find()->asArray()->all(), 'id', 'industries');
        $data['ClientIndustries_opt'] = $ClientIndustries_opt;

        $Entity_opt = ArrayHelper::map(Entity::find()->asArray()->all(), 'id', 'entity_name');
        $data['Entity_opt'] = $Entity_opt;

        $Division_opt = ArrayHelper::map(Division::find()->asArray()->all(), 'id', 'div_name');
        $data['Division_opt'] = $Division_opt;

        return $data;
    }

    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $searchModel->flag = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new Client();
            $data = $this->getDataForForm($model);

            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);
                if($action=="duplicate"){
                    $model->Id = null;
                    $model->isNewRecord;
                }
                $model->client_areas = explode(',', $model->client_areas);

            } else {
                $model = new Client();
            }

            return $this->renderAjax('_form', [
                'data' => $data,
                'model' => $model,
                'id' => ($action=="duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave()
    {
        $return = false;
        $model = new Client();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $modelFind = Client::findOne($model->id);
            if ($modelFind !== null) {
                $model = $modelFind;
                // $model->LastUpdateBy = CommonHelper::getSessionIdUser();
                $model->last_update_date = date('Y-m-d H:i:s');
            }else{
                $model->flag = 1;
                // $model->CreatedBy = CommonHelper::getSessionIdUser();
            }

            $model->load($post);
            $model->client_areas = implode(',', $model->client_areas);
            // $model->Code = $model->Alias.'-'.$model->Seq;

            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    }
                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $return = 'error : validation not valid : '.Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }

    public function actionDelete($id)
    {
        // $model = $this->findModel($id);
        // // $model->DeletedBy = CommonHelper::getSessionIdUser();
        // $model->DeletedDate = date('Y-m-d H:i:s');
        // $model->Status = 0;
        // $model->save();
        //
        // return $this->redirect(['index']);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
