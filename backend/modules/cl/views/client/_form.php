<?php



use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;

use common\models\st\RuleTermZone;
use common\models\cl\viewClientZone;


$this->title = 'Create Client';
$this->params['breadcrumbs'][] = ['label' => 'Client', 'url' => ['index']];

?>

<?php
  $form = ActiveForm::begin([
    'id' => 'crud-form',
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
  ],
  'errorSummaryCssClass'=> 'alert alert-danger'
  ]);
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);


?>

<div class="row m-b-30">
    <div class="col-md-12">
        Fields with * are required
        <p class="m-t-15"><b>IDENTITY</b></p>
        <div id="error-summary">

        </div>
        <div class="form-group-attached">
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-md-3">
                  <div class="row">
                      <div class="col-md-12">
                        <?php
                        if($model->seq == null){
                          $model->seq = 0;
                        }
                         ?>
                          <?= $form->field($model, 'seq')->textInput() ?>
                      </div>
                  </div>
                </div>

                <div class="col-md-3">
                    <?php
                      if($model->latest_job == null){
                        $model->latest_job = 0;
                      }
                    ?>
                    <?= $form->field($model, 'latest_job')->textInput() ?>
                </div>

                <div class="col-md-3">
                  <?php $model->flag = 1 ?>
                  <?=  $form->field($model , 'flag')->checkbox()->label('status'); ?>
                </div>


            </div>
            <div class="row">
              <div class="col-md-6">
                  <?php
                      echo $form->field($model, 'entity_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                              'options' => ['id' => 'entity_name', 'placeholder' => 'Select...'],
                              'data' => $data['Entity_opt'],
                                  'pluginOptions' => [

                                  ],
                          ]
                      );
                  ?>
              </div>
              <div class="col-md-6">
                  <?php
                      echo $form->field($model, 'div_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                              'options' => ['id' => 'div_name', 'placeholder' => 'Select...'],
                              'data' => $data['Division_opt'],
                                  'pluginOptions' => [

                                  ],
                          ]
                      );
                  ?>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'client_name')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'npwp')->textInput() ?>
                </div>
                <div class="col-md-8">
                    <?php
                        echo $form->field($model, 'industry_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                            Select2::classname(),
                            [
                                'options' => ['id' => 'ClientIndustries', 'placeholder' => 'Select...'],
                                'data' => $data['ClientIndustries_opt'],
                                    'pluginOptions' => [

                                    ],
                            ]
                        );
                    ?>
                </div>
            </div>
        </div>



        <p class="m-t-15"><b>CLIENT AREAS</b></p>
        <div class="form-group-attached">
            <div class="row">
                <div class="col-md-12">

                <?php
                    echo $form->field($model, 'client_areas')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(viewClientZone::find()->All(), 'term_zone_name', 'term_zone_name'),
                    'options' => ['placeholder' => 'Select Zone ...', 'multiple' => true],
                    'pluginOptions' => [
                        'tokenSeparators' => [','],
                      ],
                    ]);
                ?>
                </div>
            </div>
        </div>



    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right m-t-5">
        <hr class="m-b-5"/>
        <?= Html::submitButton('SAVE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
        <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
$('#crud-form').on('beforeSubmit', function() {
  var $form = new FormData($('#crud-form')[0]);
  $.ajax({
    url: paramJs.urlFormSave,
    type: 'POST',
    data: $form,
    async: false,
    cache: false,
    contentType: false,
    processData: false,

    success: function (data) {
      console.log(data);
      if(data != 1){
        $('#error-summary').html('<div class="alert alert-danger">' + data + '</div>');
      }else{
        $('#crud-modal').modal('hide');
        reload();
      }
    },
    error: function(jqXHR, errMsg) {
      alert(errMsg);
    }
  });
  return false;
});
</script>
