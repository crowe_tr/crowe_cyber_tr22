<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Create Client Industries';
$this->params['breadcrumbs'][] = ['label' => 'Client Industries', 'url' => ['index']];

?>

<?php
  $form = ActiveForm::begin([
    'id' => 'crud-form',
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
  ],
  'errorSummaryCssClass'=> 'alert alert-danger'
  ]);
  echo $form->errorSummary($model);;
?>

  <div class="row m-b-30">
      <div class="col-md-12">
          Fields with * are required
          <p class="m-t-15"><b>IDENTITY</b></p>
          <div class="form-group-attached">
              <div class="row">
                  <div class="col-md-3">
                      <?= $form->field($model, 'Id')->textInput() ?>
                  </div>
                  <div class="col-md-9">
                      <?= $form->field($model, 'Industries')->textInput() ?>
                  </div>
              </div>
          </div>

      </div>
  </div>

  <div class="row">
    <div class="col-md-12 text-right m-t-5">
      <hr class="m-b-5"/>
      <?= Html::submitButton('SAVE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
      <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
    </div>
  </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
  paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['common/clientindustries/save', 'id'=>$id]); ?>';
  $('#crud-form').on('beforeSubmit', function() {
    
    var $form = new FormData($('#crud-form')[0]);
    $.ajax({
      url: paramJs.urlFormSave,
      type: 'POST',
      data: $form,
      async: false,
      cache: false,
      contentType: false,
      processData: false,

      success: function (data) {
        if(data != 1){
          alert(data);
        }else{
          $('#crud-modal').modal('hide');
          reload();
        }
      },
      error: function(jqXHR, errMsg) {
        alert(errMsg);
      }
    });
    return false;
  });

</script>