<?php

namespace backend\modules\cm\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use common\models\hr\Employee;
use common\models\cl\Client;
use common\models\cm\Dept;
use common\models\tr\Job;
use common\models\st\Task;
use common\components\CommonHelper;

class HelperController extends Controller
{
    public $layout;
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionSrcemployee($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = Employee::find()->where("
                fullName like '%".$q."%'
                or Id = '".$q."'
            ")->asArray()->all();

            $out['results'] = array();
            foreach ($model as $data) {
                $array = array(
                          'id' => $data['Id'],
                          'text' => $data['Id'].' - '.$data['fullName'],
                    );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $model = Employee::find()->where("
                Id = ".$id."
            ")->asArray()->one();

            $out['results'] = ['id' => $id, 'text' => $data['Id'].' - '.$data['fullName']];
        }

        return $out;
    }
    public function actionSrcclient($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = Client::find()->where("
                Name like '%".$q."%'
                or Id = '".$q."'
            ")->asArray()->all();

            $out['results'] = array();
            foreach ($model as $data) {
                $array = array(
                          'id' => $data['Id'],
                          'text' => $data['Id'].' - '.$data['Name'],
                    );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $model = Client::find()->where("
                Id = ".$id."
            ")->asArray()->one();

            $out['results'] = ['id' => $id, 'text' => $data['Id'].' - '.$data['Name']];
        }

        return $out;
    }
    public function actionSrcclient_code($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = Client::find()->where("
                Name like '%".$q."%'
                or code = '".$q."'
            ")->asArray()->all();

            $out['results'] = array();
            foreach ($model as $data) {
                $array = array(
                          'id' => $data['Code'],
                          'text' => $data['Code'].' - '.$data['Name'],
                    );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $model = Client::find()->where("
            Code = ".$id."
            ")->asArray()->one();

            $out['results'] = ['id' => $id, 'text' => $data['Code'].' - '.$data['Name']];
        }

        return $out;
    }

    public function find_client($clientID)
    {
      $client = Client::findOne($clientID);
      // var_dump($client['Name']);

      return $client['Name'];

    }
    public function actionSrcjob($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = Job::find()
                    ->innerJoin('clClient', 'clClient.id = trJob.ClientID')
                    ->where(
                      "
                        trJob.Description like '%".$q."%'
                        or trJob.JobCode like '%".$q."%'
                        or clClient.Name like '%".$q."%'
                    ")->asArray()->all();

            $out['results'] = array();
            foreach ($model as $data) {
                $array = array(
                          'id' => $data['JobCode'],
                          'text' => $data['JobCode'].' - '.$this->find_client($data['ClientID']).' - '.$data['Description'],
                    );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $model = Job::find()->where("
                JobID = ".$id."
            ")->asArray()->one();

            $out['results'] = ['id' => $id, 'text' => $model['JobCode'].' - '.$model['Name']];
        }

        return $out;
    }

    public function actionLoaddept()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = str_replace('+', ' ', end($_POST['depdrop_parents']));;
            $list = Dept::find()->andWhere(['div_id' => $id])->asArray()->all();

            $results = [];
            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['id'], 'name' => $ls['dept_name']];
                }
                $results = json_encode(['output' => $out, 'selected' => $selected]);
            } else {
                $results = json_encode(['output' => [], 'selected' => '']);
            }
        } else {
            $results = json_encode(['output' => [], 'selected' => '']);
        }
        return $results;
    }

    public function actionLoaddept_ver2()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = str_replace('+', ' ', end($_POST['depdrop_parents']));;
            $list = Dept::find()->joinwith('div')->andWhere(['cmDivision.divName' => $id])->asArray()->all();

            $results = [];
            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['deptName'], 'name' => $ls['deptName']];
                }
                $results = json_encode(['output' => $out, 'selected' => $selected]);
            } else {
                $results = json_encode(['output' => [], 'selected' => '']);
            }
        } else {
            $results = json_encode(['output' => [], 'selected' => '']);
        }
        return $results;
    }


    public function actionLoadtimereportjobs($id, $job=0)
    {
        $user = CommonHelper::getUserIndentity();
        $return = json_encode(['output' => array(), 'selected' => '']);

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $type = end($_POST['depdrop_parents']);
            $sql_list = "call TrJobList('".$id."', ".$job.")";
            $list = Yii::$app->db->createCommand($sql_list)->queryAll();


            $selected = null;
            if ($id != null && count($list) > 0) {
                if($type == 1){ //if PROJECTS
                    $selected = '';
                    foreach ($list as $i => $ls) {
                        $out[] = ['id' => $ls['JobID'], 'name' => $ls['Description']];
                    }
                    $return = json_encode(['output' => $out, 'selected' => $selected]);
                }else{
                    $return = json_encode(['output' => array(), 'selected' => '']);
                }
            }
        }else{
            $return = json_encode(['output' => array(), 'selected' => '']);
        }
        return $return;
    }
    public function actionLoadtimereportzone($TimeReportID)
    {
        $user = CommonHelper::getUserIndentity();

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = Yii::$app->db->createCommand("call TrJobOutOffList('{$TimeReportID}', '{$id}')")->queryAll();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    if($id == $ls['TrDetID']) {
                        if(!empty($ls['JobArea'])){
                            $JobArea = explode(',', $ls['JobArea']);
                            foreach($JobArea as $Area) {
                                $out[] = ['id' => $Area, 'name' => $Area];
                            }
                        }
                    }
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);
            }
        }else{
            echo json_encode(['output' => array(), 'selected' => '']);
        }
    }
    public function actionLoadtimereporttask()
    {
        $user = CommonHelper::getUserIndentity();

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = Task::find()->where(['taskTypeID'=>$id])->orderby(['Seq'=>SORT_ASC])->asArray()->all();
            $list = Yii::$app->db->createCommand("call getTask('".$user->Id."', $id)")->queryAll();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['id'], 'name' => $ls['taskName']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);
            }
        }else{
            echo json_encode(['output' => array(), 'selected' => '']);
        }
    }
    public function actionLoadtimereporttasktaxi()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $sql_list = "call getTrTaxiClassList('".$id."')";
            $list = Yii::$app->db->createCommand($sql_list)->queryAll();


            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['ID'], 'name' => $ls['taskTypeName']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);
            }
        }else{
            echo json_encode(['output' => array(), 'selected' => '']);
        }
    }

    public function actionLoadtimereportlookuptaxi($TimeReportID, $TrDetID)
    {

        $results = json_encode(['output' => array(), 'selected' => '']);
        $out = [];
        if (isset($_POST['depdrop_all_params'])) {
            $param = $_POST['depdrop_all_params'];
            $sql = "call TrJobTaxiList(".$TimeReportID.", ".$param['cmTaxiID'].", '".$TrDetID."')";
            $list = Yii::$app->db->createCommand($sql)->queryAll();

            $selected = null;
            if (count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $lookup) {
                    $out[] = ['id' => $lookup['TrDetID'], 'name' => $lookup['Description']];
                }
                $results = json_encode(['output' => $out, 'selected' => $selected]);
            }
        }
        return $results;
    }



}
