<?php

namespace backend\modules\cm\controllers;

use Yii;
use common\models\cm\Level;
use common\models\st\RuleTaskAE;
use common\models\st\TaskLevel;
use common\models\cm\search\Level as levelSearch;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\CommonHelper;
use common\components\HelperDB;
use common\components\CmHelper;
use backend\modules\cm\controllers\CmController;

/**
 * LevelController implements the CRUD actions for level model.
 */
class LevelController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
      return CmController::set_behaviors();
    }

    public function actionIndex()
    {
      $searchModel = new levelSearch();
      return CmController::set_index($searchModel, $this);
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new Level();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);
                $oldid = $model->id;
                $modelDetail = new RuleTaskAE();
                $tasks = TaskLevel::find()->where(["level_id" => $id])->orderBy('task_id')->all();
            } else {
                $model = new Level();
                $modelDetail = new RuleTaskAE();
                $tasks = TaskLevel::find()->where(["level_id" => null])->orderBy('task_id')->all();
            }

            $tmpTasks = [];
            $i = 0;
            foreach($tasks as $task){
                $tmpTasks[$i]['task_id'] = $task['task_id'];
                $tmpTasks[$i]['task_name'] = $task['task_name'];
                $tmpTasks[$i]['percentage'] = $task['percentage'];
                $i++;
            }
            $modelDetail->TabularInput = $tmpTasks;

            return $this->renderAjax('_form', [
                'model' => $model,
                'modelDetail' => $modelDetail,
                'id'=> ($action=="duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave()
    {
        $return = false;
        $model = new Level();
        $modelDetail = new RuleTaskAE();

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $modelFind = Level::findOne($model->id);
            if ($modelFind !== null) {
                $model = $modelFind;
            }
            $model->load($post);
            $modelDetail->load($post);
            $valid = $model->validate();
            if ($valid) {
                $tasks = $modelDetail->TabularInput;
                $total_percentage = 0;
                foreach($tasks as $task){
                    $total_percentage += !empty($task['percentage']) ? $task['percentage'] : 0;
                }

                if(($total_percentage == 100) || ($total_percentage == 0)){
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if (!($flag = $model->save(false))) {

                            $transaction->rollBack();
                            $return = 'error : rollback';
                        } else {

                            $tasks = $modelDetail->TabularInput;
                            foreach($tasks as $task){
                                $task['percentage'] = !empty($task['percentage']) ? $task['percentage'] : 0;
                                $modelDetail = RuleTaskAE::find()->where([
                                    'level_id' => $model->id,
                                    'task_id' => $task['task_id']
                                ])->one();

                                if(empty($modelDetail->task_id)){
                                    $modelDetail = new RuleTaskAE();
                                }
                                $modelDetail->level_id = $model->id;
                                $modelDetail->task_id = $task['task_id'];
                                $modelDetail->percentage = CommonHelper::ResetDecimal($task['percentage']);

                                if (!($flag = $modelDetail->save(false))) {
                                    $transaction->rollBack();
                                    $return = 'error : when saving target data';
                                }

                            }
                        }

                        if ($flag) {
                            $transaction->commit();
                            $return = true;
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }else{
                    $return = 'Total Current Percentage is '.$total_percentage.'%, Total Percentage must be 100%';
                }

            } else {
                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    protected function findModel($id)
    {
        if (($model = Level::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
