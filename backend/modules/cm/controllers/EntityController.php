<?php

namespace backend\modules\cm\controllers;

use Yii;
use yii\helpers\Html;

use common\models\cm\Entity;
use common\models\cm\search\Entity as EntitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use common\components\CmHelper;
use backend\modules\cm\controllers\CmController;

class EntityController extends Controller
{

    public function behaviors()
    {
        return CmController::set_behaviors();
    }

    public function actionIndex()
    {
      $searchModel = new EntitySearch();
      return CmController::set_index($searchModel, $this);
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = new Entity();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);
            } else {
                $model = new Entity();
            }
            return CmController::render_form($this, $model, $id, $action);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave($id=null)
    {
        $return = false;
        $post = Yii::$app->request->post();
        if(!empty($id)){
            $model = Entity::findOne($id); //1. kalau ada id maka mode update
        }else{
            $model = new Entity(); //2. kalau tidak ada idnya maka save jadi record baru
        }
        return CmController::set_save($model, $post);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Entity::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
