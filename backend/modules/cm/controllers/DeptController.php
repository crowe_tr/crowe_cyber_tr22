<?php

namespace backend\modules\cm\controllers;

use Yii;
use common\models\cm\Dept;
use common\models\cm\search\Dept as DeptSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use backend\modules\cm\controllers\CmController;

/**
 * LevelController implements the CRUD actions for Dept model.
 */
class DeptController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
      return CmController::set_behaviors();
    }

    public function actionIndex()
    {
        $searchModel = new DeptSearch();
        return CmController::set_index($searchModel, $this);
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new Dept();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);
            } else {
                $model = new Dept();
            }
            return CmController::render_form($this, $model, $id, $action);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave($id=null)
    {
      $return = false;
      $post = Yii::$app->request->post();

      if(!empty($id)){
          $model = Dept::findOne($id); //1. kalau ada id maka mode update
      }else{
          $model = new Dept(); //2. kalau tidak ada idnya maka save jadi record baru
      }
      return CmController::set_save($model, $post);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }
    protected function findModel($id)
    {
        if (($model = Dept::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
