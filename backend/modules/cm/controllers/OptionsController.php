<?php

namespace backend\modules\cm\controllers;

use Yii;
use yii\helpers\Html;

use common\models\cm\Options;
use common\models\cm\search\OptionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use common\components\CmHelper;
use backend\modules\cm\controllers\CmController;

/**
 * OptionsController implements the CRUD actions for Options model.
 */
class OptionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
      return CmController::set_behaviors();
    }

    /**
     * Lists all Options models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OptionsSearch();
        return CmController::set_index($searchModel, $this);
    }

    /**
     * Displays a single Options model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Options model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

     public function actionForm($id = null, $action=null)
     {
       if (Yii::$app->request->isAjax) {
           $post = Yii::$app->request->post();
           $model = new Options();
           $model->load($post);
           if (!empty($id)) {
               $model = $this->findModel($id);
           } else {
               $model = new Options();
           }
           return CmController::render_form($this, $model, $id, $action);
       } else {
           throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
       }
     }

     public function actionSave($id=null)
     {
       $return = false;
       $post = Yii::$app->request->post();
       if(!empty($id)){
           $model = Options::findOne($id); //1. kalau ada id maka mode update
       }else{
           $model = new Options(); //2. kalau tidak ada idnya maka save jadi record baru
       }
       return CmController::set_save($model, $post);
     }

    public function actionCreate()
    {
        $model = new Options();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->options_name]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Options model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->options_name]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Options model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Options model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Options the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Options::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
