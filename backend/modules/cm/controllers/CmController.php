<?php

namespace backend\modules\cm\controllers;

use Yii;
use yii\filters\VerbFilter;
use common\components\CmHelper;

class CmController
{
  public static function set_behaviors()
  {
    return [
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['POST'],
            ],
        ],
    ];
  }

  public static function set_index($isearchModel,$imain_form)
  {
    $dataProvider = $isearchModel->search(Yii::$app->request->queryParams);
    return $imain_form->render('index', [
        'searchModel' => $isearchModel,
        'dataProvider' => $dataProvider,
    ]);
  }

  public static function render_form($imain_form, $imodel, $iid, $iaction)
  {
    return $imain_form->renderAjax('_form', [
        'model' => $imodel,
        'id'=> ($iaction=="duplicate") ? "" : $iid,
    ]);
  }


  public static function set_save($imodel, $ipost)
  {
    $return = false;
    if ($imodel->load($ipost)) {
      $valid = $imodel->validate();
      if ($valid) {
        $flag = $imodel->save(false);
        if ($flag){
          $return = true;
        } elseif(!($flag)) {
          $return = CmHelper::set_error($imodel);
        }
      } else {
        $return = CmHelper::set_error($imodel);
      }
    } else {
        $return = 'error : model not loaded';
    }

    return $return;
  }

  public static function set_column_bool($iattribute)
  {
    return CmHelper::set_column_bool($iattribute,'1','Yes','No',
                                      function($model) use ($iattribute){
                                        if ($model->$iattribute == 1) {
                                          return 'Yes';
                                        }
                                        else{
                                          return 'No';
                                        }
                                      }
                                    );
  }


}
