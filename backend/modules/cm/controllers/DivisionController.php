<?php

namespace backend\modules\cm\controllers;

use Yii;
use common\models\cm\Division;
use common\models\cm\search\Division as DivisionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use backend\modules\cm\controllers\CmController;

/**
 * DivisionController implements the CRUD actions for Division model.
 */
class DivisionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
      return CmController::set_behaviors();
    }

    /**
     * Lists all Division models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DivisionSearch();
        return CmController::set_index($searchModel, $this);
    }

    /**
     * Displays a single Division model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Division model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionForm($id = null, $action=null)
     {
         if (Yii::$app->request->isAjax) {
             $post = Yii::$app->request->post();
             $model = new Division();
             $model->load($post);
             if (!empty($id)) {
                 $model = $this->findModel($id);
             } else {
                 $model = new Division();
             }
             return CmController::render_form($this, $model, $id, $action);
         } else {
             throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
         }
     }

     public function actionSave($id=null)
     {
         $return = false;
         $post = Yii::$app->request->post();
         if(!empty($id)){
             $model = Division::findOne($id); //1. kalau ada id maka mode update
         }else{
             $model = new Division(); //2. kalau tidak ada idnya maka save jadi record baru
         }
         return CmController::set_save($model, $post);
     }

    /**
     * Updates an existing Division model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Division model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Division model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Division the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Division::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
