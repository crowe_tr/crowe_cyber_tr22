<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use common\components\CmHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\cm\search\OptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

CmHelper::set_title($this, 'Options', 'Data');

$column = [
            CmHelper::set_column_no(),
            CmHelper::set_column('options_name', 'Options Name', 6),
            CmHelper::set_column('options_value', 'Options Value', 3),
            CmHelper::set_column('effective_date', 'Effective Date', 3),
            [
                'header' => '<i class="fa fa-cogs"></i>',
                'headerOptions' => ['class' => 'bg-success text-center'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'no-padding'],
                'format' => 'raw',
                'value' => function($data){
                  return CmHelper::set_icon($data->seq, 'cm', 'options');
                },
            ],
          ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();

  $js = CmHelper::set_script('options');
  $this->registerjs($js, View::POS_END);
?>
