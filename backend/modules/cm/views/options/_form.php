<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\cm\Options */
/* @var $form yii\widgets\ActiveForm */

// $this->title = 'Update Options';
// $this->params['breadcrumbs'][] = ['label' => 'Entities', 'url' => ['index']];
//
//
// $form = ActiveForm::begin([
//   'id' => 'crud-form', //perhatiin
//   'enableClientValidation' => true,
//   'validateOnSubmit' => true,
//   'validateOnChange' => true,
//   'validateOnType' => true,
//   'fieldConfig' => [
//     'template' => '{label}{input}',
//     'options' => [
//       'class' => 'form-group form-group-default',
//     ],
//   ],
//   'errorSummaryCssClass' => 'alert alert-danger'
// ]);
$this->title = 'Update Options';
$this->params['breadcrumbs'][] = ['label' => 'Options', 'url' => ['index']];

$form = CmHelper::set_active_form();
echo $form->errorSummary($model);;
$disabled = (!$model->isNewRecord) ? true : false;
?>
<div class="alert alert-danger" id="error" style="display:none">
</div>

<div class="form-group-attached">
  <div class="row">
    <div class="col-md-3">
      <?= $form->field(
        $model,
        'options_name',
        [
          'options' => [
            'class' => ' form-group form-group-default form-group-default-select2'
            , 'disabled' => $disabled
          ],
        ]
      )->widget(
        Select2::classname(),
        [
          'data' => [
            /*'MIN WORKHOURS' => 'MIN WORKHOURS',,*/
            'MAX WORKHOURS' => 'MAX WORKHOURS',
            'MAX OVERTIME WORKDAY' => 'MAX OVERTIME WORKDAY',
            'MAX OVERTIME HOLIDAY' => 'MAX OVERTIME HOLIDAY',
            /*
            'MAX OVERTIME ONE MONTH' => 'MAX OVERTIME ONE MONTH',
            'LAST CLIENT NUMBER' => 'LAST CLIENT NUMBER',
            'PERCENTAGE ADMINISTRATIVE CHARGE' => 'PERCENTAGE ADMINISTRATIVE CHARGE'*/
          ],
          'options' => ['placeholder' => 'Select ...'],
        ]
      ) ?>
    </div>
    <div class="col-md-3">
      <?=
        $form->field($model, 'effective_date', [
          'template' => '{label}{input}', 'options' => ['class' => 'form-group form-group-default input-group'],
        ])->widget(DatePicker::classname(), [
          'type' => DatePicker::TYPE_INPUT,
          'value' => date('Y-m-d'),
          'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
          ],
        ])
      ?>
    </div>
    <div class="col-md-4">
      <?= $form->field($model, 'options_value')->textInput(['maxlength' => true]) ?>
    </div>
  </div>
</div>
<br />

<div class="row">
  <div class="col-md-12 text-right m-t-5">
    <hr class="m-b-5" />
    <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
    <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
  paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['cm/options/save', 'id' => $id]); ?>';
  $('#crud-form').on('beforeSubmit', function() {
    showFullLoading();

    var $form = new FormData($('#crud-form')[0]);
    $.ajax({
      url: paramJs.urlFormSave,
      type: 'POST',
      data: $form,
      async: false,
      cache: false,
      contentType: false,
      processData: false,

      success: function(data) {
        hideFullLoading();

        if (data != 1) {
          $('#error').show();
          $('#error').html(data);
        } else {
          $('#crud-modal').modal('hide');
          reload();
        }
      },
      error: function(jqXHR, errMsg) {
        hideFullLoading();
        $('#error').show();
        $('#error').html(data);
      }
    });
    return false;
  });
</script>
