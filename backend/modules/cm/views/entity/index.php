<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use common\components\CmHelper;

CmHelper::set_title($this, 'Entities', 'DATA');
$column = [
            CmHelper::set_column_no(),
            CmHelper::set_column('entity_code', 'Entity Code', 3),
            CmHelper::set_column('entity_name', 'Entity Name', 9),
            [
                'header' => '<i class="fa fa-cogs"></i>',
                'headerOptions' => ['class' => 'bg-success text-center'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'no-padding'],
                'format' => 'raw',
                'value' => function($data){
                  return CmHelper::set_icon($data->id, 'cm', 'entity');
                },
            ],
          ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();

  $js = CmHelper::set_script('entity');
  $this->registerjs($js, View::POS_END);

?>
