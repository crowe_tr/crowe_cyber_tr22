<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
use common\components\CmHelper;

$this->title = 'Create Entity';
$this->params['breadcrumbs'][] = ['label' => 'Entities', 'url' => ['index']];

$form = CmHelper::set_active_form();
echo $form->errorSummary($model);

echo CmHelper::row_group_header();
echo CmHelper::set_text_input('4', $form, $model, 'entity_code');
echo CmHelper::set_label_text_input('8', $form, $model, 'entity_name', 'Entity Name');
echo CmHelper::row_group_footer();
echo CmHelper::set_button_input();

ActiveForm::end();

$js = CmHelper::script_input('entity', $id, '$form');
$this->registerjs($js, View::POS_END);

?>
