<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use unclead\multipleinput\MultipleInput;
use kartik\widgets\Select2;
use yii\widgets\MaskedInput;
use yii\web\View;
use common\components\CmHelper;

// $this->title = 'Create Entity';
// $this->params['breadcrumbs'][] = ['label' => 'Entities', 'url' => ['index']];
//
// $form = ActiveForm::begin([
//   'id' => 'crud-form', //perhatiin
//   'enableClientValidation' => true,
//   'validateOnSubmit' => true,
//   'validateOnChange' => true,
//   'validateOnType' => true,
//   'fieldConfig' => [
//       'template' => '{label}{input}',
//       'options' => [
//           'class' => 'form-group form-group-default',
//       ],
// ],
// 'errorSummaryCssClass'=> 'alert alert-danger'
// ]);
$this->title = 'Create Level';
$this->params['breadcrumbs'][] = ['label' => 'Level', 'url' => ['index']];

$form = CmHelper::set_active_form();
echo $form->errorSummary($model);
echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

echo CmHelper::row_empty12();
echo CmHelper::row_group_header();
echo CmHelper::set_text_input('2', $form, $model, 'level_code');
echo CmHelper::set_text_input('5', $form, $model, 'level_name');
echo CmHelper::set_text_input('5', $form, $model, 'level_notes');
echo CmHelper::row_foot_head();
echo CmHelper::set_checkbox_input('4', $form, $model, 'auto_approve');
echo CmHelper::set_checkbox_input('4', $form, $model, 'is_not_boss');
echo CmHelper::set_checkbox_input('4', $form, $model, 'is_not_manager');
echo CmHelper::row_group_footer();

?>


    <div class="row">
      <div class="col-md-12">
      <?php
      echo $form->field($modelDetail, 'TabularInput', ['options'=>['class'=>'table']])->widget(
        MultipleInput::className(), [
          'iconSource' => 'fa',
          'theme'=>'default',
          'sortable'=>false,
          'addButtonPosition' => MultipleInput::POS_HEADER,
          'removeButtonOptions'=>[
            'class'=>'btn btn-danger btn-sm',
          ],
          'addButtonOptions'=>[
            'class'=>'btn btn-warning btn-sm',
          ],
          'columns' => [
            [
              'name'  => 'task_id',
              'title' => '',
              'value' => function($data) {
                return !empty($data['task_id']) ? $data['task_id'] : "";
              },
              'options' => [
                'class' => 'hidden',
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => '',
                'style'=>'padding:0 !Important; border:0',

              ],
              'headerOptions' => [
                'class'=>'',
                'style'=>'padding:0 !Important; border:0',
                'width'=>'10px'
              ]
            ],
            [
              'name'  => 'task_name',
              'title' => 'TASK NAME',
              'value' => function($data) {
                return !empty($data['task_name']) ? $data['task_name'] : "";
              },
              'options' => [
                'class' => 'form-control',
                'disabled'=>true,
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => 'col-md-9',

              ],
              'headerOptions' => [
                'class'=>'bg-info text-white text-center'
              ]

          ],
          [
            'name'  => 'percentage',
            'title' => 'PERCENTAGE',
            'type'=>MaskedInput::className(),
            'value' => function($data) {
              return $data['percentage'];
            },
            'options' => [
              'clientOptions' => [
                'alias' => 'decimal',
                'groupSeparator' => '',
                'autoGroup' => true,
              ],
              'options' => [
                'class' => 'form-control',
              ]
            ],
              'columnOptions' => [
                'class' => 'col-md-3',

              ],
              'headerOptions' => [
                'class'=>'bg-info text-white text-center'
              ]

          ],

        ]
      ])->label(false);
      ?>
      </div>
    </div>

<div class="row">
<div class="col-md-12 text-right m-t-5">
  <hr class="m-b-5"/>
  <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
  <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
</div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
  paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['cm/level/save', 'id'=>$id]); ?>';
  $('#crud-form').on('beforeSubmit', function() {

    var $form = new FormData($('#crud-form')[0]);
    $.ajax({
      url: paramJs.urlFormSave,
      type: 'POST',
      data: $form,
      async: false,
      cache: false,
      contentType: false,
      processData: false,

      success: function (data) {
        if(data != 1){
          alert(data);
        }else{
          $('#crud-modal').modal('hide');
          reload();
        }
      },
      error: function(jqXHR, errMsg) {
        alert(errMsg);
      }
    });
    return false;
  });

</script>
