<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\View;
use common\components\CmHelper;

$this->title = 'Create Division';
$this->params['breadcrumbs'][] = ['label' => 'Division', 'url' => ['index']];

$form = CmHelper::set_active_form();
echo $form->errorSummary($model);

echo CmHelper::row_group_header();
echo CmHelper::set_label_text_input('3', $form, $model, 'div_code', 'Code');
echo CmHelper::set_label_text_input('6', $form, $model, 'div_name', 'Division Name');
echo CmHelper::set_checkbox_input('3', $form, $model, 'overtime_project');
echo CmHelper::row_group_footer();
echo CmHelper::set_button_input();

ActiveForm::end();
$js = CmHelper::script_input('division', $id, '$form');
$this->registerjs($js, View::POS_END);

?>
