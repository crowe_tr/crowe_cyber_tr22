
<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use common\models\cm\Division;
use common\components\CmHelper;

CmHelper::set_title($this, 'DEPARTMENT', 'SETUP');
$this->params['crud_ajax'] = true;
$column = [
            CmHelper::set_column_no(),
            CmHelper::set_column('dept_code', 'Dept Code', 2),
            CmHelper::set_column('dept_name', 'Department Name', 4),
            [
                'attribute' => 'div_id',
                'headerOptions' => ['class' => 'col-sm-4 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => ''],
                'format' => 'raw',
                'filter' => ArrayHelper::map(Division::find()->distinct()->all(), 'id', 'div_name'),
                'value'=>'div.div_name'
            ],
            CmHelper::set_column_bool('flag','2','Active','Non Active',
                                        function($model){
                                              if ($model->flag == 1) {
                                                return 'Active';
                                              }
                                              else{
                                                return 'Non Active';
                                              }
                                            }
                                      ),
            [
                'header' => '<i class="fa fa-cogs"></i>',
                'headerOptions' => ['class' => 'bg-success text-center'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'no-padding'],
                'format' => 'raw',
                'value' => function($data){
                  return CmHelper::set_icon($data->id, 'cm', 'dept');
                },
            ],
          ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();

  $js = CmHelper::set_script('dept');
  $this->registerjs($js, View::POS_END);

?>
