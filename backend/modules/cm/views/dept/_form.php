<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use common\models\cm\Division;
use yii\web\View;
use common\components\CmHelper;

$this->title = 'Create Department';
$this->params['breadcrumbs'][] = ['label' => 'Department', 'url' => ['index']];

$form = CmHelper::set_active_form();
echo $form->errorSummary($model);
echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

echo CmHelper::row_empty12();
echo CmHelper::row_group_header();
echo CmHelper::set_lookup_input('8',
  $form->field($model, 'div_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
          Select2::classname(),
          [
                  'data' => ArrayHelper::map(Division::find()->all(), 'id', 'div_name'),
                  'options' => ['id' => 'div_name', 'placeholder' => 'Select ...'],
          ]
  )
);
echo CmHelper::set_text_input('4', $form, $model, 'dept_code');
echo CmHelper::row_foot_head();
echo CmHelper::set_text_input('8', $form, $model, 'dept_name');
echo CmHelper::set_checkbox_input('4', $form, $model, 'flag');
echo CmHelper::row_group_footer();

echo CmHelper::set_button_input();

ActiveForm::end();
$js = CmHelper::script_input('dept', $id, '$form');
$this->registerjs($js, View::POS_END);
?>
