<?php
use yii\helpers\Html;

$this->title = Yii::t('backend', '');
$icon_title = '<i class="fa fa-files-o"></i>';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'JOB APPROVAL LIST'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['grid']['emptytext'] = '<div class="text-center" style="padding: 2em 0">'
             .'<i class="ion-android-alert fa-5x text-warning"></i>'
             .'<br>'
             .Yii::t('backend', 'You do not have any Pending orders within your Filters. .')
             .'<br>'
             .Yii::t('backend', 'To create new Job click <b><i class="icon-plus"></i> NEW JOB</b> on the right top of corner')
             .'</div>';
?>


<?php \yii\widgets\Pjax::begin([
    'id' => 'grid',
    'timeout' => false,
    'enablePushState' => false,
    'clientOptions' => ['method' => 'post'],
]);

?>
    <?= $OrderButtons; ?>
	<?php echo $this->render('jobIndex', ['OrderButtons' => 9, 'searchModel' => $searchModel, 'dataProvider' => $dataProvider]); ?>

<?php \yii\widgets\Pjax::end(); ?>
