<?php
use yii\helpers\Html;
?>
<div class="card card-default m-t-5 m-b-5">
    <div class="card-body">
        <div class="item">
            <input type="checkbox" name="req[]" class="checkbox" value="R0190019" id="req_R0190019" style="display: none">
            <label class="row">
                <div class="col-md-3 col-sm-4">
                  <div class="row p-b-5">
                    <div class="col-sm-7">
                      <?php
                      echo Html::a(
                        '<i class="fa fa-file-text"></i>&nbsp;'.$model->JobCode,
                        ['view', 'id' => $model->JobID],
                        [
                          'class' => 'btn btn-warning btn-lg btn-block text-center',
                          'style' => 'font-size: 13px !important; font-weight: bold',
                        ]
                      );
                      ?>
                    </div>
                    <div class="col-sm-5">
                      <div class="col-sm-12 text-left">
                        <?php
                        switch($model->Status)
                        {
                          case 0 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">DRAFT</span>
                          </h3>
                          <?php
                            // echo "DRAFT";
                            break;
                          case 1 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">SUBMITTED</span>
                          </h3>
                          <?php
                            break;
                          case 2 :
                          ?>
                          <h3 class="bold fs-24">

                              <span class="badge badge-default fs-10 text-black">
                                <?php
                                  if ($model->StartDate < date("Y-m-d H:i:s") and  $model->EndDate > date("Y-m-d H:i:s")) {
                                    ECHO "ON GOING";
                                  }
                                  elseif ($model->EndDate < date("Y-m-d H:i:s")) {
                                    ECHO "EXPIRED";
                                  }
                                  else{
                                    echo "APPROVED";
                                  }
                                 ?>
                                <!-- APPROVED -->

                              </span>
                            
                              <?php
                              if ($model->Flag == 1)
                              {
                              ?>
                                  <span class="badge badge-default fs-10 text-black">  ( DRAFT REVISION )</span>
                              <?php
                              }
                              elseif ($model->Flag == 2)
                              {
                              ?>
                                   <span class="badge badge-default fs-10 text-black"> ( SUBMITTED REVISION )</span>
                              <?php
                              }
                               ?>

                          </h3>
                          <?php
                            break;
                          case 3 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">REJECTED</span>
                          </h3>
                          <?php
                            break;
                          case 4 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">CLOSED</span>
                          </h3>
                          <?php
                            break;
                          case 5 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">EXPIRED</span>
                          </h3>
                          <?php
                            break;
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class="row p-b-5">
                    <!-- <div class="col-sm-12"> -->

                    <!-- </div> -->
                  </div>
                </div>
                <div class="col-md-9 col-sm-8">
                    <div >
                        <div class="row p-b-5">
                            <div class="col-sm-5 text-left p-t-5 p-b-5">
                                CLIENT
                                <br>
                                <b class="bold no-padding no-margin"><?= isset($model->Client) ? $model->Client : ""; ?></b>
                            </div>
                            <div class="col-sm-3 p-t-5 p-b-5">
                                ENTITY / DIVISON
                                <br>
                                <b class="bold no-padding no-margin"><?= $model->Entity ?></b> /
                                <b class="bold no-padding no-margin"><?= $model->Divison ?></b>
                            </div>

                            <div class="col-sm-4 text-right p-t-5 p-b-5">
                                TOTAL FEE (IDR)
                                <br>
                                <h3 class="bold no-padding no-margin"><?= number_format($model->Fee); ?></h3>
                            </div>
                            <div class="col-sm-12">
                                <hr class="no-margin">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 text-left p-t-5 p-b-5">
                                JOB DESCRIPTION
                                <br>
                                <b class="bold no-padding no-margin">
                                <?= $model->Description; ?>
                                </b>
                            </div>
                            <div class="col-sm-3 text-left p-t-5 p-b-5">
                                PIC PROJECT ( MANAGER )
                                <br>
                                <b class="bold no-padding no-margin"><?= isset($model->ManagerName) ? $model->ManagerName : ""; ?></b>
                            </div>
                            <div class="col-sm-4 text-right p-t-5 p-b-5">
                              PERIODS
                              <br>
                              <b class="bold no-padding no-margin">
                                 <?php
                                   if (isset($model->StartDate)) {
                                     $date = date_create($model->StartDate);
                                     $dates = date_format($date, "M d, Y");
                                     echo $dates;
                                   }
                                ?> -
                                <?php
                                  if (isset($model->EndDate)) {
                                    $date = date_create( $model->EndDate);
                                    echo date_format($date, "M d, Y");
                                  }
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
          </div>
      </div>
</div>
