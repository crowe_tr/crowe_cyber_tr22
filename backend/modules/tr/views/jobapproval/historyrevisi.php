<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use common\components\CommonHelper;
// var_dump($modelJob);
 ?>

<?php
$n= 1;
switch($modelJob->Status)
{
	case 0 :
		$status = "DRAFT";
		// echo "DRAFT";
		break;
	case 1 :
		$status = "SUBMITTED";
		break;

	case 2 :
		if ($modelJob->StartDate < date("Y-m-d H:i:s") and  $modelJob->EndDate > date("Y-m-d H:i:s")) {
			$status = "ON GOING";
		}
		elseif ($modelJob->EndDate < date("Y-m-d H:i:s")) {
			$status = "EXPIRED";
		}
		else{
			$status = "APPROVED";
		}
				if ($modelJob->Flag == 1)
				{
					$status2 = "DRAFT REVISI";
				}
				elseif ($modelJob->Flag == 2)
				{
					$status2 = "SUBMITTED REVISI";
				}

		break;
	case 3 :
				$status = "REJECTED";
		break;
	case 4 :
					$status = "CLOSED";
	case 5 :
					$status = "EXPIRED";
	break;
}

$user = CommonHelper::getUserIndentity();
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'JOB LIST'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', $status), 'url' => ['view', 'id' => $modelJob->JobID]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', $status2), 'url' => ['viewrevisi', 'id' => $modelJob->JobID]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'HISTORY'.$this->title = ($modelJob->isNewRecord) ? ' Create New Job' : ' Job  : '.$modelJob->JobCode), 'url' => ['viewrevisi', 'id' => $modelJob->JobID]];
$this->params['breadcrumbs_btn'] =Html::a(Yii::t('app', 'BACK'), ['viewrevisi', 'id' => $modelJob->JobID], ['class' => 'btn btn-secondary text-white active ']);
?>
<div class="card card-default">
  <div class="card-body">
    <b class="fs-20">JOB : <?php echo $modelJob->JobCode.'-'.$modelJob->client->Name?></b>
  </div>
</div>
<?php
$aNo = 1;
foreach ($model as $key => $value) {

  ?>
  <div class="row">
   <div class="col-md-12">
    <div class="card card-default">
      <div class="card-body">
          <?php
          if ($value != '') {
            $data = json_decode($value);
            foreach ($data as $key2 => $val2) {
              ?>
              <b class="fs-14">REVISION JOB <?php echo $n++ ?> </b>
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>NO.</th>
                    <th class="col-md-6">DESCRIPTION CHANGES</th>
                    <th class="col-md-3">ORIGINAL</th>
                    <th class="col-md-3">REVISION</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                  <?php
                  $no = 1;
                  foreach ($val2->DescMaster as $key3 => $val3)
                  {
                    ?>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $val3->Desc ?></td>
                    <td>
                      <?php
                      switch ($val3->Desc) {
                        case 'FEE':
                        echo number_format($val3->BeforeValue);
                        break;
                        case 'MEAL ALLOWANCE AMOUNT':
                        echo number_format($val3->BeforeValue);
                        break;
                        case 'OUT OF OFFICE ALLOWANCE AMOUNT':
                        echo number_format($val3->BeforeValue);
                        break;
                        case 'TAXI ALLOWANCE AMOUNT':
                        echo number_format($val3->BeforeValue);
                        break;
                        case 'TAXI ALLOWANCE AMOUNT':
                        echo number_format($val3->BeforeValue);
                        break;
                        case 'TIME CHARGES':
                        echo number_format($val3->BeforeValue);
                        break;
                        case 'ADMINISTRATIVE CHARGE':
                        echo number_format($val3->BeforeValue);
                        break;
                        case 'OTHER EXPENSE':
                        echo number_format($val3->BeforeValue);
                        break;

                        default:
                        echo $val3->BeforeValue;
                        break;
                      }
                      ?>
                    </td>
                    <td>
                      <?php
                      switch ($val3->Desc) {
                        case 'FEE':
                        echo number_format($val3->AfterValue);
                        break;
                        case 'MEAL ALLOWANCE AMOUNT':
                        echo number_format($val3->AfterValue);
                        break;
                        case 'OUT OF OFFICE ALLOWANCE AMOUNT':
                        echo number_format($val3->AfterValue);
                        break;
                        case 'TAXI ALLOWANCE AMOUNT':
                        echo number_format($val3->AfterValue);
                        break;
                        case 'TAXI ALLOWANCE AMOUNT':
                        echo number_format($val3->AfterValue);
                        break;
                        case 'TIME CHARGES':
                        echo number_format($val3->AfterValue);
                        break;
                        case 'ADMINISTRATIVE CHARGE':
                        echo number_format($val3->AfterValue);
                        break;
                        case 'OTHER EXPENSE':
                        echo number_format($val3->AfterValue);
                        break;

                        default:
                        echo $val3->AfterValue;
                        break;
                      }
                      ?>
                    </td>
                  </tr>

                    <?php
                  }
                  foreach($val2->DescDetailAll as $key4 => $val4)
                  {
                    ?>
                    <tr>
                    <td><?php echo $no++?></td>
                    <td><?php echo $val4->Desc?></td>
                    <td><?php echo $val4->BeforeValue?></td>
                    <td><?php echo $val4->AfterValue?></td>
                    </tr>
                    <?php
                  }
                  ?>

                </tbody>
              </table>
              <?php
            }
          }
          else{
            Echo "Revise ".$aNo++ . " NO Changes";
          }
          ?>
        <br>
        <br>
      </div>
    </div>
   </div>
  </div>
  <?php
}

 ?>
