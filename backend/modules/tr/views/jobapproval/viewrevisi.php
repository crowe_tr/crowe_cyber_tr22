<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use common\components\CommonHelper;




switch($modelJob->Status)
{
	case 0 :
		$status = "DRAFT";
		// echo "DRAFT";
		break;
	case 1 :
		$status = "SUBMITTED";
		break;

	case 2 :
		if ($modelJob->StartDate < date("Y-m-d H:i:s") and  $modelJob->EndDate > date("Y-m-d H:i:s")) {
			$status = "ON GOING";
		}
		elseif ($modelJob->EndDate < date("Y-m-d H:i:s")) {
			$status = "EXPIRED";
		}
		else{
			$status = "APPROVED";
		}
				if ($modelJob->Flag == 1)
				{
					$status2 = "DRAFT REVISION";
				}
				elseif ($modelJob->Flag == 2)
				{
					$status2 = "SUBMITTED REVISION";
				}

		break;
	case 3 :
				$status = "REJECTED";
		break;
	case 4 :
					$status = "CLOSED";
	case 5 :
					$status = "EXPIRED";
	break;
}
$user = CommonHelper::getUserIndentity();
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'JOB APPROVAL LIST'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', $status), 'url' => ['view', 'id' => $modelJob->JobID]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', $status2.$this->title = ($modelJob->isNewRecord) ? ' Create New Job' : ' Job  : '.$modelJob->JobCode), 'url' => ['viewrevisi', 'id' => $modelJob->JobID]];

$data['button-right-back'] = Html::a(Yii::t('app', 'BACK'), ['view', 'id' => $modelJob->JobID], ['class' => 'btn btn-secondary text-white active ']);
$buttons =[];
foreach($data as $key => $value){
	array_push($buttons, $value);
}
$btn = implode('&nbsp;', $buttons);
$this->params['breadcrumbs_btn'] =
$btn.'&nbsp;'.
Html::a('<i class="pg-comment"></i> <span class="badge badge-warning">'.$commentCount['count'].'</span> ',
			false,
			[
					'class' => 'btn btn-success text-white',
					'onclick' => 'reload_comment();',
					'data-toggle' => 'modal',
					'data-target' => '#ModalComments',
					'title' => ''.$commentCount['count'].'Comments',
			]
	);
;
// $this->title = 'Job : '.$modelJob->JobCode;
\yii\web\YiiAsset::register($this);
?>

<div class="modal fade slide-right" id="ModalComments" role="dialog" aria-hidden="false">
	<div class="modal-dialog no-border">
		<div class="modal-content no-border">
			<div class="modal-header no-border bg-success">
				<p class="text-white">
					<b><i class="pg-comment fax2"></i> COMMENTS</b>
				</p>
			</div>
			<div id="detail" class="modal-body no-padding">
				<div class=" notification-toggle" role="menu" aria-labelledby="notification-center">
				    <div class="notification-panel" style="width: 100%; border: none;">
				        <div class="notification-body scrollable scroll-content scroll-scrolly_visible bg-pattern">
				            <div id="comments" style="height: 300px; overflow: auto; box-shadow: none; max-height: 320px; height: 320px; overflow:auto; background: url('/../../themes/pages/assets/img/pattern.jpg') repeat scroll 0% 0% !important;">

				            </div>
				        </div>
				        <div class="notification-footer" id="ModalCommentsForm">
				            <?php
				                $form = ActiveForm::begin([
				                    'id' => 'comment-form',
				                    'enableClientValidation' => true,
				                    'validateOnSubmit' => true,
				                    'validateOnChange' => true,
				                    'validateOnType' => true,
				                ]);
				            ?>
				            <?= $form->field($modelComments, 'JobID', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
				            <?= $form->field($modelComments, 'Comments')->label(false)->widget(\yii\redactor\widgets\Redactor::className([
				                'clientOptions' => [
				                'imageManagerJson' => ['/redactor/upload/image-json'],
				                'imageUpload' => ['/redactor/upload/image'],
				                'fileUpload' => ['/redactor/upload/file'],
				                'plugins' => ['clips', 'fontcolor', 'imagemanager'],
				                ],
				            ]), ['options' => ['height' => '300px']]); ?>

				            <div class="row m-t-10">
				                <div class="col-sm-8">
				                    <a class=" btn btn-lg btn-warning btn-block text-white" onclick="addComment()" style="opacity: initial" href="javascript:;">
				                        <b><i class="fa fa-sends"></i> SEND COMMENT</b>
				                    </a>
				                </div>
				                <div class="col-sm-4">
				                    <a class=" btn btn-lg btn-info btn-block text-white" data-dismiss="modal" href="javascript:;">
				                        <b><i class="fa fa-sends"></i> CLOSE</b>
				                    </a>
				                </div>
				            </div>
				            <?php ActiveForm::end(); ?>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
 <div class="col-md-12">
	 <div class="card card-default">
		 <div class="card-body">
			 <b class="fs-14">REVISION JOB : <?php echo $modelJob->JobCode .' - '.$modelJob->client->Name ?> </b></p>
			 <?php
			 $revisi = $modelRevisi['vDescAll'];
			 if ($modelRevisi['vDescAll'] != false) {
				 if ($revisi['Status'] != 0) {
				 $descall = json_decode($revisi['@DescAll']);
				 // $master = json_decode($revisi['@DescAll']['Master']);
				 // $detail = json_decode($revisi['@DescAll']['Detail']);
				 ?>
						 <table class="table table-striped">
							 <thead>
								 <tr>
									 <th>NO.</th>
									 <th class="col-md-6">DESCRIPTION CHANGES</th>
									 <th class="col-md-3">ORIGINAL</th>
									 <th class="col-md-3">REVISION</th>
								 </tr>
							 </thead>
							 <tbody>
								 <?php
								 $no = 1;
								 if (isset($descall->Master)) {
									 foreach($descall->Master as $c)
									 {
										 ?>

										 <tr>
											 <td><?php echo $no++ ?></td>
											 <td><?php echo $c->Desc?> </td>
											 <td>
												 <?php
												 switch ($c->Desc) {
													 case 'FEE':
													 echo number_format($c->BeforeValue);
													 break;
													 case 'MEAL ALLOWANCE AMOUNT':
													 echo number_format($c->BeforeValue);
													 break;
													 case 'OUT OF OFFICE ALLOWANCE AMOUNT':
													 echo number_format($c->BeforeValue);
													 break;
													 case 'TAXI ALLOWANCE AMOUNT':
													 echo number_format($c->BeforeValue);
													 break;
													 case 'TAXI ALLOWANCE AMOUNT':
													 echo number_format($c->BeforeValue);
													 break;
													 case 'TIME CHARGES':
													 echo number_format($c->BeforeValue);
													 break;
													 case 'ADMINISTRATIVE CHARGE':
													 echo number_format($c->BeforeValue);
													 break;
													 case 'OTHER EXPENSE':
													 echo number_format($c->BeforeValue);
													 break;
													 default:
													 echo $c->BeforeValue;
													 break;
												 }
												 ?>
											 </td>
											 <td>
												 <?php
												 switch ($c->Desc) {
													 case 'FEE':
													 echo number_format($c->AfterValue);
													 break;
													 case 'MEAL ALLOWANCE AMOUNT':
													 echo number_format($c->AfterValue);
													 break;
													 case 'OUT OF OFFICE ALLOWANCE AMOUNT':
													 echo number_format($c->AfterValue);
													 break;
													 case 'TAXI ALLOWANCE AMOUNT':
													 echo number_format($c->AfterValue);
													 break;
													 case 'TAXI ALLOWANCE AMOUNT':
													 echo number_format($c->AfterValue);
													 break;
													 case 'TIME CHARGES':
													 echo number_format($c->AfterValue);
													 break;
													 case 'ADMINISTRATIVE CHARGE':
													 echo number_format($c->AfterValue);
													 break;
													 case 'OTHER EXPENSE':
													 echo number_format($c->AfterValue);
													 break;

													 default:
													 echo $c->AfterValue;
													 break;
												 }
												 ?>
											 </td>
										 </tr>
										 <?php
									 }
								 }
								 if (isset($descall->Detail)) {
									foreach ($descall->Detail as $key1 => $val1) {
										?>
										<tr>
											<td><?php echo $no++?></td>
											<td><?php echo $val1->Desc?></td>
											<td><?php echo $val1->BeforeValue?></td>
											<td><?php echo $val1->AfterValue?></td>
										</tr>

										<?php
								 }
								 }
								 ?>
							 </tbody>
						 </table>

					 <?php
				 }
				 else {
					 ?>
						 No Changes
					 <?php
				 }
			 }
			 else{
				 ?>
				 	Draft
				 <?php
			 }
			 ?>
			 <br>
			 <br>
			 <div class="pull-right">
				 <?php
				 if ($countHistory != 0) {
					 echo Html::a(Yii::t('app', '<i class="fa fa-search"></i> LOG HISTORY '), ['historyrevisi', 'id' => $modelJob->JobID], ['class' => 'btn btn-secondary text-white active ']);
				 }
				 else {
					echo "NOT HISTORY" ;
				 }
				 ?>
			 </div>


		 </div>
	 </div>
 </div>
</div>
<?php
if ($modelJob->Flag == 2) {
	// code...

		?>
		<!-- <div class="card card-default m-t-5 m-b-5"> -->
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<?php
						echo Html::a(Yii::t('app', '<i class="fa fa-check"></i> APPROVE '), ['approverevisi', 'id' => $modelJob->JobID], ['class' => 'btn btn-primary text-white active approveklik']);
						echo "&nbsp;";
						echo Html::a(Yii::t('app', '<i class="fa fa-close"></i> REJECT '), ['rejectrevisi', 'id' => $modelJob->JobID], ['class' => 'btn btn-danger text-white active ',  'data-method' => 'post',
                                'data-pjax' => 1,
                                'data-confirm' => 'ARE YOU SURE TO REJECT THIS JOB?',]);
						?>
					</div>
				</div>
			</div>
		<!-- </div> -->
		<?php
		}
 ?>

<?php
$this->registerJs(
    "
     var paramJs = (paramJs || {});
			 paramJs.notifurl = '".Yii::$app->urlManager->createAbsoluteUrl(['tr/jobapproval/show_notification'])."';
		 	paramJs.comments = '".Yii::$app->urlManager->createAbsoluteUrl(['tr/jobapproval/comments', 'id' => $modelJob->JobID])."';
		 	paramJs.commentsave = '".Yii::$app->urlManager->createAbsoluteUrl(['tr/jobapproval/commentsave'])."';    ",  \yii\web\View::POS_HEAD
);

?>
<script type="text/javascript">
    function showNotification(type, msg){
    $.ajax({
        url: paramJs.notifurl,
        data: {type:type, msg:msg},
        method: "POST",
        dataType: 'text',
        success: function(data) {
        $('#notification-wrapper').html(data);
        reload();
        },
        error: function(data) {
        $('#notification-wrapper').html(data);
        }
    });
    }

    $('.notification-footer').height($(window).height() / 2);

    function addComment() {
        $.ajax({
            url: paramJs.commentsave,
            type: 'POST',
            data: new FormData($('#comment-form')[0]),
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                reload_comment()
            },
            error: function(jqXHR, errMsg) {
                alert(errMsg);
            }
        });
        return false;
    }
    function reload() {
    $.pjax.defaults.timeout = false;
        $.pjax.reload({
            container: '#main-pjax'
        })
    }
    function reload_comment(){
        $(".redactor-editor").html("");
        $("#fnpcrreqcomments-comment").val("");
					showFullLoading();
        $.ajax({
            async: true,
            url: paramJs.comments,
            data: $('#comment-form').serialize(),
            method: "POST",
            contentType: "application/json",
            success: function(data) {
							hideFullLoading();
            $("#comments").html(data);
						// console.log()
						// element.scrollTo(element.get(0).scrollHeight);

            },
            error: function(data) {
            $("#comments").html(data);
            }
        });

    }

		$(document).ready(function(){
			$('.approveklik').on("click", function(){
					 showFullLoading();
			});
		});




</script>
