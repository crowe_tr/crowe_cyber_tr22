<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;

?>
<?php
    $create_btn = !empty($this->params['grid']['create_btn']) ? $this->params['grid']['create_btn'] : '';
    echo $this->render('jobSearch', ['model' => $searchModel, 'create_btn' => $create_btn]);

    Pjax::begin(['id' => rand().'pajaxgrid', 'enablePushState' => false]);
    echo ListView::widget([
      'id' => rand().'listview',
        'dataProvider' => $dataProvider,
        'emptyText' => '
          <div class="card panel-default no-margin">
            <div class="card-body no-padding text-center padding-20">
              <i class="fa fa-exclamation-circle fa-5x text-warning"></i>
              <br>
               '.Yii::t('backend', 'You do not have any Pending job within your Filters.').'
              <br>
              '.Yii::t('backend', 'To create new job, click <b><i class="icon-plus"></i> new job</b> on the right top of corner').'
              <br>
              <br>
            </div>
          </div>
          <div class="card pacardnel-default no-margin">
            <div class="card-body padding-20" style="border-top: 0">
              <div class="row">
                <div class="col-md-6 text-left sm-text-center">
                Showing 0 of 0 items.
                </div>
                <div class="col-md-6 text-right sm-text-center">
                </div>
              </div>
            </div>
          </div>

        ',
        'layout' => '
            <form id="lists-job">
              {items}
            </form>
						<div class="card card-default ">
							<div class="panel-body p-t-15 p-b-5 p-l-20 p-r-20" style="border-top: 0">
								<div class="row">
									<div class="col-md-6 text-left sm-text-center">
										{summary}
									</div>
									<div class="col-md-6 text-right sm-text-center">
										{pager}
									</div>
								</div>
							</div>
						</div>
						',
            'sorter' => [
            'class' => 'common\components\ButtonDropdownSorter',
            'label' => 'Sort Items',
        ],
        'itemOptions' => ['class' => 'item'],
        'itemView' => 'jobIndexDetail',
    ]);
    Pjax::end();
?>
