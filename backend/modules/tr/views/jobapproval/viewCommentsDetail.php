<?php
    use yii\helpers\Html;

?>

<div class="card social-card share item no-margin" style="width: 100%; border-radius: 0; background: transparent !important; cursor: pointer; border: 0;">
	<div class="card-header clearfix" style="background: none; border: none">
	<div class="pull-right">
		<span class=" time">
			<small>
				<?= date(' H:i - D, d M y', strtotime($model->CreatedDate)); ?>
			</small>
   		 </span>
	</div>
		<div class="user-pic">
			<img src="<?= !empty($model->employee->Photo) ? Yii::$app->homeUrl.Yii::$app->params['hrEmployeeBaseurl'].$model->employee->Photo : Yii::$app->homeUrl.Yii::$app->params['appUserDefaultPhotoBaseurl']; ?>"" width="35" height="35">
		</div>
		<h5><?= !empty($model->employee->fullName) ? $model->employee->fullName : '-'; ?></h5>
		<h6>
			<?= empty($model->employee->entity->entityName) ? "" : $model->employee->entity->entityName; ?>
			<?= empty($model->employee->division->divName) ? "" : " - ".$model->employee->division->divName; ?>
			<?= empty($model->employee->dept->deptName) ? "" : " - ".$model->employee->dept->deptName; ?>
		</h6>
	</div>
	<div class="card-description p-t-0 p-l-50">
		<div style="border-radius: 5px; border: 1px solid #ddd; -webkit-box-shadow: 0 0px 1px rgba(0, 0, 0, 0.2); -moz-box-shadow: 0 0px 1px rgba(0, 0, 0, 0.2); box-shadow: 0 0px 1px rgba(0, 0, 0, 0.2);" class=" bg-white padding-20">
		<p><?= Html::decode($model->Comments); ?></p>
		</div>
	</div>
</div>
