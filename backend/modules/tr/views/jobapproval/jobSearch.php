<?php
use yii\helpers\Html;
use yii\web\View;

use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use common\models\cm\CmCompanyBranch;
use common\models\cm\CmDept;
// use common\models\fn\view\vFnBdgtCtrlDeptActive;
use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK

$user = CommonHelper::getUserIndentity();
?>
	<?php
    $form = ActiveForm::begin([
            'id' => 'jobsearch-form',
                        'method' => 'post',
            'fieldConfig' => [
                'template' => '{label}{input}',
                'options' => [
                    'class' => 'form-group form-group-default',
                ],
            ],
        ]);
    ?>

<div class="card m-t-10 m-b-5" style="background: #f2f2f2">
    <div class="card-body">
        <div class="form-group-attached">
            <div class="row">
							<div class="col-sm-3">
								<?= $form->field($model, 'JobCode', ['options' => ['onchange' => '$("#jobsearch-form").submit()', 'placeholder' => 'Search by No& Item Name']])->label('Search'); ?>
							</div>
            </div>
        </div>
    </div>
</div>


	<?php ActiveForm::end(); ?>
