<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;

use kartik\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\grid\GridView;
use kolyunya\yii2\widgets\MapInputWidget;
// use backend\modules\fn\components\FnHelper;
use dosamigos\switchery\Switchery;

use common\components\CommonHelper;

use common\models\cl\Client;
use common\models\hr\Employee;
use common\models\cl\viewClientZone;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'JOB LIST'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title = ($modelJob->isNewRecord) ? 'Create New Job' : 'Update Job  : '.$modelJob->JobCode;


?>
<div class="modal fade stick-up " id="DetailModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg ">
		<div class="modal-content no-border">
			<div class="modal-header bg-success text-white">
			</div>
			<div id="detail" class="modal-body padding-20">
			</div>
		</div>
	</div>
</div>


<?php
$form = ActiveForm::begin([
        'id' => 'job-form',
        'enableClientValidation' => true,
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true,
        'fieldConfig' => [
            'template' => '{label}{input}',
            'options' => [
                'class' => 'form-group form-group-default',
            ],
		],
		'errorSummaryCssClass'=> 'alert alert-danger'
	]);

	echo $form->errorSummary($modelJob);;
?>
<?php \yii\widgets\Pjax::begin(['id' => rand().'pjax', 'enablePushState' => false, 'id' => 'job-pjax']); ?>
	<?php
		if(!$modelJob->isNewRecord) {

			?>
				<div class="row m-b-10">
					<div class="col-sm-6">
						<div class="row bold no-padding  text-black no-margin">
							<div class="col-sm-2 fs-16">
								CLIENT
							</div>
							<div class="col-sm-1 fs-16 mlr-30">
								:
							</div>
							<div class="col-sm-7 fs-16">
								<?php
									echo ''.$modelJob->client->Name.'';
								 ?>
							</div>
						</div>
						<div class="row bold no-padding text-black no-margin">
							<div class="col-sm-2 fs-16">
								JOB
							</div>
							<div class="col-sm-1 fs-16 mlr-30">
								:
							</div>
							<div class="col-sm-7 fs-16">
								<?php
									echo ''.$modelJob->JobCode.'';
								 ?>
								 <?php
								 switch($modelJob->Status)
								 {
									 case 0 :
									 ?>
											 <span class="badge badge-warning fs-13 text-black">DRAFT</span>
									 <?php
										 // echo "DRAFT";
										 break;
									 case 1 :
									 ?>
											 <span class="badge badge-warning fs-13 text-black">SUBMIT</span>
									 <?php
										 break;
									 case 2 :
									 ?>
									 <?php
										 break;
									 case 3 :
									 ?>
											 <span class="badge badge-warning fs-13 text-black">REJECT</span>
									 <?php
										 break;
									 case 4 :
									 ?>
											 <span class="badge badge-warning fs-13 text-black">CLOSED</span>
									 <?php
									 case 5 :
									 ?>
											 <span class="badge badge-warning fs-13 text-black">FINISHED</span>
									 <?php
										 break;
								 }
								 ?>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="row bold no-padding text-black no-margin">
							<div class="col-sm-2 fs-16">
								ENTITY
							</div>
							<div class="col-sm-1 fs-16 mlr-30">
								:
							</div>
							<div class="col-sm-7 fs-16">
								<?php
									echo ''.$modelJob->client->entity->entityName.'';
								 ?>
							</div>
						</div>
						<div class="row bold no-padding text-black no-margin">
							<div class="col-sm-2 fs-16">
								DIVISION
							</div>
							<div class="col-sm-1 fs-16 mlr-30">
								:
							</div>
							<div class="col-sm-7 fs-16">
								<?php
									echo ''.$modelJob->client->division->divName.'';
								 ?>
							</div>
						</div>
					</div>
				</div>
			<?php

		} else {
			// echo '<h3 class="bold no-padding no-margin">CREATE NEW JOB</h3>';
		}
	?>
  	<div class="card card-default m-b-10">
  		<div class="card-body">
				<?php
					if ($modelJob->JobID != null) {
						// code...

				?>
				<div class="form-group-attached">
					<div class="row">
						<div class="col-md-9">
							<?= $form->field($modelJob, 'JobID', ['options' => ['class' => '']])->hiddenInput(['id' => 'JobID'])->label(false); ?>
							<?= $form->field($modelJob, 'ClientID', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
							<?= $form->field($modelJob, 'JobCode', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>

							<?php
							echo $form->field($modelJob, 'Description')->textInput();
							?>
						</div>
						<div class="col-md-3">
							<?=
							$form->field($modelJob, 'Fee', ['options' => ['onchange' => 'reqsave()']])->widget(MaskedInput::className(), [
								'clientOptions' => [
									'alias' => 'decimal',
									'groupSeparator' => ',',
									'autoGroup' => true,
								],
							]);
							?>
						</div>

					</div>
				<div class="row">

						<div class="col-md-2">
							<?=
							$form->field($modelJob, 'StartDate', [
								'template' => '{label}{input}', 'options' => [ 'onchange' => 'reqsave()', 'class' => 'form-group form-group-default input-group'],
							]
							)->widget(DatePicker::classname(), [
								'type' => DatePicker::TYPE_INPUT,
								'value' => date('Y-m-d'),
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
								],
							])
							?>
						</div>
						<div class="col-md-2">
							<?=
							$form->field($modelJob, 'EndDate', [
								'template' => '{label}{input}', 'options' => ['onchange' => 'reqsave()', 'class' => 'form-group form-group-default input-group'],
							]
							)->widget(DatePicker::classname(), [
								'type' => DatePicker::TYPE_INPUT,
								'value' => date('Y-m-d'),
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
								],
							])
							?>
						</div>
						<div class="col-md-5">
							<?php
										$c = Client::find()->where(['Id' => $modelJob->ClientID])->one();
										$data = explode(',' , $c->ClientAreas);
										$cl = array();
										foreach($data as $d)
										{
											$cl[$d] = $d;
										}
									echo $form->field($modelJob, 'JobArea')->widget(Select2::classname(), [
									'data' => $cl,
									'options' => ['placeholder' => 'Select Zone ...', 'multiple' => true, 'onchange' => 'reqsave()','required'],
									'pluginOptions' => [
											'tokenSeparators' => [','],
										],
									]);
							?>
						</div>

					<div class="col-md-3">
						<?php
						echo $form->field($modelJob, 'IncludeOPE',
						['options' => ['class' => ' form-group form-group-default', 'onchange' => 'reqsave()']]
						)->widget(
						Switchery::className(),
						['options' => []]
						)->label(false);

						?>
					</div>
				</div>
				<?php
					}
					else {
						?>
						<div class="form-group-attached">
							<div class="row">
								<div class="col-md-4">
									<?= $form->field($modelJob, 'JobID', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
									<?php

									echo $form->field($modelJob, 'ClientID', ['options' => ['onchange' => 'lateJob()', 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
									Select2::classname(),
									[
									'data' =>  $data['Client'],
									'options' => ['id' => 'Class', 'placeholder' => 'Select ...' , 'class' => 'readonly'],

									]
									);
									?>
								</div>

								<div class="col-md-2">
									<?php
									echo $form->field($modelJob, 'JobCode', ['options' => ['class' => ' form-group form-group-default']])->textInput(['readonly'=> true , 'class' => "form-control readonly"]);
									?>
								</div>
								<div class="col-md-6">
									<?php
									echo $form->field($modelJob, 'Description')->textInput();
									?>
								</div>

							</div>
						<div class="row">

								<div class="col-md-2">
									<?=
									$form->field($modelJob, 'StartDate', [
										'template' => '{label}{input}', 'options' => [ 'class' => 'form-group form-group-default input-group'],
									]
									)->widget(DatePicker::classname(), [
										'type' => DatePicker::TYPE_INPUT,
										'value' => date('Y-m-d'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'yyyy-mm-dd',
										],
									])
									?>
								</div>
								<div class="col-md-2">
									<?=
									$form->field($modelJob, 'EndDate', [
										'template' => '{label}{input}', 'options' => [ 'class' => 'form-group form-group-default input-group'],
									]
									)->widget(DatePicker::classname(), [
										'type' => DatePicker::TYPE_INPUT,
										'value' => date('Y-m-d'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'yyyy-mm-dd',
										],
									])
									?>
								</div>
								<div class="col-md-2">
									<?=
									$form->field($modelJob, 'Fee', ['options' => ['onkeyup' => 'counttotal()']])->widget(MaskedInput::className(), [
										'clientOptions' => [
											'alias' => 'decimal',
											'groupSeparator' => ',',
											'autoGroup' => true,
										],
									]);
									?>
								</div>
								<div class="col-md-4">

									<?php
										$Manager = '';

										if (!$modelJob->isNewrecord || !empty($modelJob->Manager)) {
											$dataManager = Employee::findOne($modelJob->Manager);
											$Manager = empty($dataManager->fullName) ? $modelJob->Manager : $dataManager->Id.' - '.$dataManager->fullName.' - '.$dataManager->level->levelName;
										}
										echo $form->field($modelJob, 'Manager', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
											->widget(Select2::classname(), [
											'initValueText' => $Manager,
											'options' => ['placeholder' => 'Search Manager ... '],
											'pluginOptions' => [
											'minimumInputLength' => 3,
											'tags' => false,
											'ajax' => [
												'url' => Url::to(['helper/srcemployemanager' , 'id' => $modelJob->JobID]),
												'dataType' => 'json',
												'data' => new JsExpression('function(params) { return {q:params.term , JobID:$("#JobID").val()}; }'),
											],
											//'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
											//'templateResult' => new JsExpression('formatSelect2ClientID'),
											//'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
											],
										]);
									?>
								</div>




							<div class="col-md-2">
								<?php
								echo $form->field($modelJob, 'IncludeOPE',
								['options' => ['class' => ' form-group form-group-default']]
								)->widget(
								Switchery::className(),
								['options' => []]
								)->label(false);

							?>
							</div>
						</div>
					</div>
						<?php
					}
				?>
  		</div>
  	</div>
</div>
	<?php if(!$modelJob->isNewRecord) : ?>

	<div class="card card-default m-b-10 m-t-20">
		<div class="card-body">
			<?=
			Html::a(
				'<i class="pg-plus"></i> <span class="hidden-xs">REGISTER TEAM</span>',
				false,
				[
					'onclick' => 'BudgetCreate()',
					'class' => 'btn btn-warning text-white btn-rounded p-t-10 p-b-10',
					'style' => 'position: absolute; top: -1.1em; right: 1.8em; border-radius: 20px !important',
				]
			);
			?>

			<p><b>JOB BUDGETING</b></p>

			<?=
			 GridView::widget([
				'id' => 'job-details',
				'dataProvider' => $modelBudgeting,
				'emptyText' => '<div class="text-center" style="padding: 2em 0">'
								.'<i class="fa fa-exclamation-circle fa-5x text-warning"></i>'
								.'<br>'
								.Yii::t('backend', 'THERE IS NO ONE IN THIS TEAM. <br/>START REGISTER YOUR TEAM FOR THIS JOB ')
								.'</div>',
				'columns' => [
					[
						'class' => 'kartik\grid\SerialColumn',
						'headerOptions' => ['class' => 'bg-warning text-white b-r'],
						'contentOptions' => ['class' => 'text-right'],
						'width' => '36px',
						'header' => 'NO',
					],
					[
						'attribute' => 'EmployeeID',
						'headerOptions' => ['class' => 'bg-success col-sm-3'],
						'contentOptions' => ['class' => 'kv-align-middle'],
						'value'=>'employee.fullName'
					],
					[
						'attribute' => 'levelID',
						'headerOptions' => ['class' => 'bg-success col-sm-2'],
						'contentOptions' => ['class' => 'kv-align-middle'],
						'value'=>'employee.level.levelName'
					],
					[
						'attribute' => 'Total',
						'headerOptions' => ['class' => 'bg-success col-sm-1'],
						'contentOptions' => ['class' => 'kv-align-middle text-right'],
					],
					[
						'attribute' => 'Planning',
						'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
						'contentOptions' => ['class' => 'kv-align-middle text-right'],
					],
					[
						'attribute' => 'FieldWork',
						'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
						'contentOptions' => ['class' => 'kv-align-middle text-right'],
					],
					[
						'attribute' => 'Reporting',
						'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
						'contentOptions' => ['class' => 'kv-align-middle text-right'],
					],
					[
						'attribute' => 'WrapUp',
						'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
						'contentOptions' => ['class' => 'kv-align-middle text-right'],
					],
					[
						'attribute' => 'OverTime',
						'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
						'contentOptions' => ['class' => 'kv-align-middle text-right'],
					],
						[
						'header' => '',
						'headerOptions' => ['class' => 'bg-success text-center'],
						'filterOptions' => ['class' => 'b-b b-grey'],
						'contentOptions' => ['class' => 'no-padding'],
						'format' => 'raw',
						'value' => function ($data, $modelJob) {
									$edit = Html::a(
											'<i class="fa fa-edit"></i>',
											false,
											[
													'onclick' => "BudgetUpdate('".Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budget', 'id' => $data->EmployeeID])."')",
													'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
													'style' => 'bjob-radius: 5px !important',
											]
									);
									$delete = Html::a(
											'<i class="pg-trash"></i>',
											false,
											[
												'onclick' => "BudgetDelete('".$data->EmployeeID."','".$data->JobID."')",
												'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
												'style' => 'bjob-radius: 5px !important',
											]
										);
							$val = '<div class="tooltip-action">
								<div class="trigger">
									'.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
								</div>
								<div class="action-mask">
									<div class="action">
										'.$edit.'
										'.$delete.'
									</div>
								</div>
							</div>';

							return $val;
						},
					],
				],
				'layout' => '{items}',
				'resizableColumns' => false,
				'bordered' => false,
				'striped' => false,
				'condensed' => false,
				'responsive' => false,
				'hover' => false,
				'persistResize' => false,
			]);
			?>
		</div>
	</div>

	<div class="card card-default m-b-10">
		<div class="card-body">
			<p><b>PIC PROJECT</b></p>
			<div class="form-group-attached">
				<div class="row">
					<div class="col-md-4">
					<?php
						$Partner = '';

						if (!$modelJob->isNewrecord || !empty($modelJob->Partner)) {
							$dataPartner = Employee::findOne($modelJob->Partner);
							$Partner = empty($dataPartner->fullName) ? $modelJob->Partner : $dataPartner->Id.' - '.$dataPartner->fullName.' - '.$dataPartner->level->levelName;
						}
						echo $form->field($modelJob, 'Partner', ['options' => ['class' => ' form-group form-group-default form-group-default-select2', 'onchange' => 'reqsave()']])
							->widget(Select2::classname(), [
							'initValueText' => $Partner,
							'options' => ['placeholder' => 'Search Partner ... '],
							'pluginOptions' => [
							'tags' => false,
							'ajax' => [
								'url' => Url::to(['helper/srcemployejob' , 'id' => $modelJob->JobID]),
								'dataType' => 'json',
								'data' => new JsExpression('function(params) { return {q:params.term , JobID:$("#JobID").val()}; }'),
							],
							//'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
							//'templateResult' => new JsExpression('formatSelect2ClientID'),
							//'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
							],
						]);

					?>
					</div>
					<div class="col-md-4">
						<?php
							$Manager = '';

							if (!$modelJob->isNewrecord || !empty($modelJob->Manager)) {
								$dataManager = Employee::findOne($modelJob->Manager);
								$Manager = empty($dataManager->fullName) ? $modelJob->Manager : $dataManager->Id.' - '.$dataManager->fullName.' - '.$dataManager->level->levelName;
							}
							echo $form->field($modelJob, 'Manager', ['options' => ['class' => ' form-group form-group-default form-group-default-select2', 'onchange' => 'reqsave()']])
								->widget(Select2::classname(), [
								'initValueText' => $Manager,
								'options' => ['placeholder' => 'Search Manager ... '],
								'pluginOptions' => [
								'tags' => false,
								'ajax' => [
									'url' => Url::to(['helper/srcemployejob' , 'id' => $modelJob->JobID]),
									'dataType' => 'json',
									'data' => new JsExpression('function(params) { return {q:params.term , JobID:$("#JobID").val()}; }'),
								],
								//'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
								//'templateResult' => new JsExpression('formatSelect2ClientID'),
								//'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
								],
							]);
						?>
					</div>
					<div class="col-md-4">
					<?php
						$Supervisor = '';

						if (!$modelJob->isNewrecord || !empty($modelJob->Supervisor)) {
							$dataSupervisor = Employee::findOne($modelJob->Supervisor);
							$Supervisor = empty($dataSupervisor->fullName) ? $modelJob->Supervisor : $dataSupervisor->Id.' - '.$dataSupervisor->fullName.' - '.$dataSupervisor->level->levelName;
						}
						echo $form->field($modelJob, 'Supervisor', ['options' => ['class' => ' form-group form-group-default form-group-default-select2', 'onchange' => 'reqsave()']])
							->widget(Select2::classname(), [
							'initValueText' => $Supervisor,
							'options' => ['placeholder' => 'Search Supervisor ... '],
							'pluginOptions' => [
							'tags' => false,
							'ajax' => [
								'url' => Url::to(['helper/srcemployejob' , 'id' => $modelJob->JobID]),
								'dataType' => 'json',
								'data' => new JsExpression('function(params) { return {q:params.term , JobID:$("#JobID").val()}; }'),
							],
							//'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
							//'templateResult' => new JsExpression('formatSelect2ClientID'),
							//'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
							],
						]);
					?>

					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="row m-b-70">
		<div class="col-md-12 text-right">
			<?php
				if ($modelJob->JobID) {
					// code...
					$btn_text2 = ($modelJob->Flag == 0) ? Yii::t('app', 'SAVE & EDIT') : Yii::t('app', 'REVISE REQUEST');

					echo Html::submitButton($btn_text2, ['class' => 'btn btn-warning  btn-lg saveClick' , 'name' => 'submit[]', 'value' => '0']);
				}
				else {
					$btn_text2 = ($modelJob->Flag == 0) ? Yii::t('app', 'SAVE') : Yii::t('app', 'REVISE REQUEST');
					echo Html::submitButton($btn_text2, ['class' => 'btn btn-success  btn-lg']);
					echo '&nbsp;'.Html::a(Yii::t('app', 'CANCEL'), ($modelJob->Flag == 0) ? ['tr/job/index'] : ['tr/job/view', 'id' => $modelJob->JobCode], ['class' => 'btn btn-info btn-lg']);
				}

			?>
		</div>
	</div>


<?php \yii\widgets\Pjax::end(); ?>
<?php ActiveForm::end(); ?>



<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlBudgetDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budgetdelete']); ?>';
	paramJs.urlBudgetShowModal = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budget']); ?>';
	paramJs.urlOthersExpense = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/otherexpense']); ?>';
	paramJs.urlLatesJob = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/latesjob']); ?>';
	paramJs.urlReqsave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/reqsave']); ?>';
	paramJs.urlReqSaveAllowance = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/reqsaveallowance']); ?>';
	paramJs.urlDefault = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/default']) ?>'
	paramJs.urlOutOfOfficeAllowance = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/outofficeallowance']) ?>';

		function BudgetShowModal(link="") {
			var link = (link || paramJs.urlBudgetShowModal);
			// $('#detail').html("loading...");
			  showFullLoading();
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {
					  hideFullLoading();
					$('#detail').html(data);
				},
			});
		}

		function BudgetCreate() {
			BudgetShowModal();
			$('#DetailModal').modal('show');
		}
		function BudgetUpdate(link) {
			BudgetShowModal(link);
			$('#DetailModal').modal('show');
		}
		function BudgetDelete(EmployeeID, JobID) {
			if(confirm("Are you sure want to delete this item permanently ?")){
				$.ajax({
					url: paramJs.urlBudgetDelete,
					data: {EmployeeID:EmployeeID, JobID:JobID},
					method: "POST",
					dataType: 'json',
					success: function () {
						reload();
					},
					complete: function(data) {},
					error: function(data) {}
				});
			}
		}
		function OthersExpense() {
			OthersExpenseShowModal();
			$('#DetailModal').modal('show');
		}
		function OthersExpenseShowModal(link="") {
			var link = (link || paramJs.urlOthersExpense);
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {
					$('#detail').html(data);
				},
			});
		}
		window.closeModal = function(){
			reload();
		};
		function reload(){
			$.pjax.defaults.timeout = false;
			$.pjax.reload({
				container: '#job-pjax'
			})
			$('#DetailModal').modal('hide');
			$('#DetailModal').data('modal', null);
		}

		function lateJob(){

				var link = (link || paramJs.urlLatesJob);
				$.ajax({
					url: link,
					data:  $('#job-form').serialize(),
					method: "POST",
					dataType: 'html',
					success: function(data) {
						var d = JSON.parse(data)
						$('#CLientID').html(d.ClientID);
						$('#job-jobcode').val(d.job);
					},
				});

		}

		function reqsave()
		{
			var link = (link || paramJs.urlReqsave);
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {
					console.log(data);

				},
			});
		}

		function reqsaveallowance()
		{
			var link = (link || paramJs.urlReqSaveAllowance);
			 showFullLoading();
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {
					  hideFullLoading();
					console.log(data);
					var d = JSON.parse(data)
					$('#job-mealallowance').val(d.MealAllowance);
					$('#job-taxiallowance').val(d.TaxiAllowance);
					$('#job-outofofficeallowance').val(d.OutOfOfficeAllowance);
				},
			});
		}

		function defaults()
		{
			var link = (link  || paramJs.urlDefault);
			 showFullLoading();
			$.ajax({
				url : link,
				data : $('#job-form').serialize(),
				method : "POST",
				dataType : 'html',
				success:function(data){
					var d = JSON.parse(data);
					hideFullLoading();
					$('#job-mealallowance').val(d.MealAllowance);
					$('#job-taxiallowance').val(d.TaxiAllowance);
					$('#job-outofofficeallowance').val(d.OutOfOfficeAllowance);
					$('#job-administrativecharge').val(d.AdministrativeCharge);
					$('#job-otherexpenseallowance').val(d.OtherExpenseAllowance);
					$('.data-r').html(d.Percentage);


				},
			});
		}

		function EstimatedAllowance()
		{
			var link = (link || paramJs.urlOutOfOfficeAllowance);
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {

					var d = JSON.parse(data);

					$('#job-mealallowance').val(d.MealAllowance);
					$('#job-taxiallowance').val(d.TaxiAllowance);
					$('#job-outofofficeallowance').val(d.OutOfOfficeAllowance);
					$('#job-administrativecharge').val(d.AdministrativeCharge);
					$('#job-otherexpenseallowance').val(d.OtherExpenseAllowance);
					// $('#job-otherexpenseallowance').val(d.OutOfficeAllowance);
					$('.data-r').html(d.Percentage);

				},
			});
		}

		$(document).ready(function(){
			$('.submitClick').on("click", function(){
					 showFullLoading();
			});
			$('.saveClick').on("click", function(){
					 showFullLoading();
			});
		});

</script>
