<?php
  use yii\widgets\ActiveForm;
  use yii\web\View;
  use common\components\CmHelper;
  use common\components\HrHelper;
  use backend\modules\st\controllers\StController;

  use yii\helpers\Html;
  use yii\helpers\Url;
  use yii\web\JsExpression;
  // use yii\web\View;
  use yii\widgets\MaskedInput;
  use yii\helpers\ArrayHelper;

  // use kartik\widgets\ActiveForm;
  use kartik\widgets\DepDrop;
  use kartik\widgets\Select2;
  use kartik\widgets\DatePicker;
  use kartik\grid\GridView;
  use kolyunya\yii2\widgets\MapInputWidget;
  // use backend\modules\fn\components\FnHelper;
  use dosamigos\switchery\Switchery;

  use common\models\cl\Client;
  use common\models\hr\Employee;



  echo CmHelper::title_only($this, 'Create New Job');
  ?>
  <div class="modal fade stick-up " id="DetailModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
  	<div class="modal-dialog modal-lg ">
  		<div class="modal-content no-border">
  			<div class="modal-header bg-success text-white">
  			</div>
  			<div id="detail" class="modal-body padding-20">
  			</div>
  		</div>
  	</div>
  </div>

  <?php
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
  		],
  		'errorSummaryCssClass'=> 'alert alert-danger'
  	]);

  	echo $form->errorSummary($model);
  ?>
  <?php \yii\widgets\Pjax::begin(['id' => rand().'pjax', 'enablePushState' => false, 'id' => 'job-pjax']); ?>
  <?php
  $data_client_id = 0;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);
?>

<div class="card card-default m-b-10">
  <div class="card-body">
<?php
  echo CmHelper::row_group_header();
?>

<div class="col-md-4">
  <?= $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput(['id' => 'id'])->label(false); ?>

  <?php
  echo $form->field($model, 'client_id', ['options' => ['onchange' => 'lateJob()', 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
    Select2::classname(),
    [
      'data' =>  $data['Client'],
      'options' => ['id' => 'Class', 'placeholder' => 'Select ...', 'class' => 'readonly'],

    ]
  );
  ?>
</div>

<?php
  echo CmHelper::set_text_input('2', $form, $model, 'job_code');
  echo CmHelper::set_text_input('6', $form, $model, 'description');
  echo CmHelper::row_foot_head();
  echo CmHelper::set_field_date('2', $form, $model, 'start_date');
  echo CmHelper::set_field_date('2', $form, $model, 'end_date');
  echo CmHelper::set_field_num('2', $form, $model, 'job_fee');
  echo StController::lookup_input2('4', $form, $model, 'manager_id', Employee::find()->all(),
      function($model) {
          return $model['user_id'].' - '.$model['full_name'].' - '.$model->level['level_name'];
      }, 'user_id');
  echo CmHelper::set_switch('2', $form, $model, 'include_ope');
  echo CmHelper::row_group_footer();
?>
</div>
</div>
<?php
  echo CmHelper::set_button_input();

  echo Html::a(
    'latestjob',
    ['latestjob', 'id' => '2' ],
    [
      'class' => 'btn btn-info text-white ',
    ]);


  // ActiveForm::end();
  // $js = CmHelper::script_input2('$form');
  // $this->registerjs($js, View::POS_END);
?>
<br>
<?php

 ?>

<?php \yii\widgets\Pjax::end(); ?>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlLatesJob = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/jobcreate/budget_latesjob']); ?>';

	function lateJob() {
		var link = (link || paramJs.urlLatesJob);
		$.ajax({
			url: link,
			data: $('#job-form').serialize(),
			method: "POST",
			dataType: 'html',
			success: function(data) {
				var d = JSON.parse(data)
				$('#CLientID').html(d.ClientID);
				$('#job-job_code').val(d.job);
			},
		});
	}



</script>
