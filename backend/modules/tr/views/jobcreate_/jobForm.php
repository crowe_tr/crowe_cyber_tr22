<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;

use kartik\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\grid\GridView;
use kolyunya\yii2\widgets\MapInputWidget;
// use backend\modules\fn\components\FnHelper;
use dosamigos\switchery\Switchery;

use common\components\CommonHelper;

use common\models\cl\Client;
use common\models\hr\Employee;
use common\models\cl\viewClientZone;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'JOB LIST'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title = ($modelJob->isNewRecord) ? 'Create New Job' : 'Update Job  : '.$modelJob->job_code;


?>
<div class="modal fade stick-up " id="DetailModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg ">
		<div class="modal-content no-border">
			<div class="modal-header bg-success text-white">
			</div>
			<div id="detail" class="modal-body padding-20">
			</div>
		</div>
	</div>
</div>


<?php
$form = ActiveForm::begin([
        'id' => 'job-form',
        'enableClientValidation' => true,
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true,
        'fieldConfig' => [
            'template' => '{label}{input}',
            'options' => [
                'class' => 'form-group form-group-default',
            ],
		],
		'errorSummaryCssClass'=> 'alert alert-danger'
	]);

	echo $form->errorSummary($modelJob);;
?>
<?php \yii\widgets\Pjax::begin(['id' => rand().'pjax', 'enablePushState' => false, 'id' => 'job-pjax']); ?>

  	<div class="card card-default m-b-10">
  		<div class="card-body">
				<?php
					if ($modelJob->id != null) {
						// code...

				?>
				<div class="form-group-attached">
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($modelJob, 'id', ['options' => ['class' => '']])->hiddenInput(['id' => 'id'])->label(false); ?>

							<?php

							echo $form->field($modelJob, 'client_id', ['options' => ['onchange' => 'lateJob()', 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
							Select2::classname(),
							[
							'data' =>  $data['Client'],
							'options' => ['id' => 'Class', 'placeholder' => 'Select ...' , 'class' => 'readonly'],

							]
							);
							?>
						</div>

						<div class="col-md-2">
							<?php
							echo $form->field($modelJob, 'job_code', ['options' => ['class' => ' form-group form-group-default']])->textInput(['class' => "form-control"]);
							?>
						</div>
						<div class="col-md-6">
							<?php
							echo $form->field($modelJob, 'description')->textInput();
							?>
						</div>

					</div>
					<div class="row">

						<div class="col-md-2">
							<?=
							$form->field($modelJob, 'start_date', [
								'template' => '{label}{input}', 'options' => [ 'class' => 'form-group form-group-default input-group'],
							]
							)->widget(DatePicker::classname(), [
								'type' => DatePicker::TYPE_INPUT,
								'value' => date('Y-m-d'),
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
								],
							])
							?>
						</div>
						<div class="col-md-2">
							<?=
							$form->field($modelJob, 'end_date', [
								'template' => '{label}{input}', 'options' => [ 'class' => 'form-group form-group-default input-group'],
							]
							)->widget(DatePicker::classname(), [
								'type' => DatePicker::TYPE_INPUT,
								'value' => date('Y-m-d'),
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
								],
							])
							?>
						</div>
						<div class="col-md-2">
							<?=
							$form->field($modelJob, 'job_fee', ['options' => ['onkeyup' => 'counttotal()']])->widget(MaskedInput::className(), [
								'clientOptions' => [
									'alias' => 'decimal',
									'groupSeparator' => ',',
									'autoGroup' => true,
								],
							]);
							?>
						</div>
						<div class="col-md-4">

							<?php
								$Manager = '';

								if (!$modelJob->isNewrecord || !empty($modelJob->manager_id)) {
									$data_manager = Employee::findOne($modelJob->manager_id);
									$Manager = empty($data_manager->fullName) ? $modelJob->manager_id : $data_manager->id.' - '.$data_manager->full_name.' - '.$data_manager->level->level_name;
								}
								echo $form->field($modelJob, 'manager_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
									->widget(Select2::classname(), [
									'initValueText' => $Manager,
									'options' => ['placeholder' => 'Search Manager ... '],
									'pluginOptions' => [
									'minimumInputLength' => 3,
									'tags' => false,
									'ajax' => [
										'url' => Url::to(['helper/srcemployemanager' , 'id' => $modelJob->id]),
										'dataType' => 'json',
										'data' => new JsExpression('function(params) { return {q:params.term , JobID:$("#JobID").val()}; }'),
									],
									//'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
									//'templateResult' => new JsExpression('formatSelect2ClientID'),
									//'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
									],
								]);
							?>
						</div>




					<div class="col-md-2">
						<?php
						echo $form->field($modelJob, 'include_ope',
						['options' => ['class' => ' form-group form-group-default']]
						)->widget(
						Switchery::className(),
						['options' => []]
						)->label(false);

					?>
					</div>
				</div>
			</div>
				<?php
					}
					else {
						?>
						<div class="form-group-attached">
							<div class="row">
								<div class="col-md-4">
									<?= $form->field($modelJob, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
									<?php

									echo $form->field($modelJob, 'client_id', ['options' => ['onchange' => 'lateJob()', 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
									Select2::classname(),
									[
									'data' =>  $data['Client'],
									'options' => ['id' => 'Class', 'placeholder' => 'Select ...' , 'class' => 'readonly'],

									]
									);
									?>
								</div>

								<div class="col-md-2">
									<?php
									echo $form->field($modelJob, 'job_code', ['options' => ['class' => ' form-group form-group-default']])->textInput(['readonly'=> true , 'class' => "form-control readonly"]);
									?>
								</div>
								<div class="col-md-6">
									<?php
									echo $form->field($modelJob, 'description')->textInput();
									?>
								</div>

							</div>
							<div class="row">

								<div class="col-md-2">
									<?=
									$form->field($modelJob, 'start_date', [
										'template' => '{label}{input}', 'options' => [ 'class' => 'form-group form-group-default input-group'],
									]
									)->widget(DatePicker::classname(), [
										'type' => DatePicker::TYPE_INPUT,
										'value' => date('Y-m-d'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'yyyy-mm-dd',
										],
									])
									?>
								</div>
								<div class="col-md-2">
									<?=
									$form->field($modelJob, 'end_date', [
										'template' => '{label}{input}', 'options' => [ 'class' => 'form-group form-group-default input-group'],
									]
									)->widget(DatePicker::classname(), [
										'type' => DatePicker::TYPE_INPUT,
										'value' => date('Y-m-d'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'yyyy-mm-dd',
										],
									])
									?>
								</div>
								<div class="col-md-2">
									<?=
									$form->field($modelJob, 'job_fee', ['options' => ['onkeyup' => 'counttotal()']])->widget(MaskedInput::className(), [
										'clientOptions' => [
											'alias' => 'decimal',
											'groupSeparator' => ',',
											'autoGroup' => true,
										],
									]);
									?>
								</div>
								<div class="col-md-4">

									<?php
										$Manager = '';

										if (!$modelJob->isNewrecord || !empty($modelJob->manager_id)) {
											$data_manager = Employee::findOne($modelJob->manager_id);
											$Manager = empty($data_manager->full_name) ? $modelJob->manager_id : $data_manager->user_id.' - '.$data_manager->full_name.' - '.$data_manager->level->level_name;
										}
										echo $form->field($modelJob, 'manager_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
											->widget(Select2::classname(), [
											'initValueText' => $Manager,
											'options' => ['placeholder' => 'Search Manager ... '],
											'pluginOptions' => [
											'minimumInputLength' => 3,
											'tags' => false,
											'ajax' => [
												'url' => Url::to(['helper/srcemployemanager' , 'id' => $modelJob->id]),
												'dataType' => 'json',
												'data' => new JsExpression('function(params) { return {q:params.term , JobID:$("#JobID").val()}; }'),
											],
											//'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
											//'templateResult' => new JsExpression('formatSelect2ClientID'),
											//'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
											],
										]);
									?>
								</div>




							<div class="col-md-2">
								<?php
								echo $form->field($modelJob, 'include_ope',
								['options' => ['class' => ' form-group form-group-default']]
								)->widget(
								Switchery::className(),
								['options' => []]
								)->label(false);

							?>
							</div>
						</div>
					</div>
						<?php
					}
				?>
  		</div>
  	</div>
</div>
	<?php if(!$modelJob->isNewRecord) : ?>



	<?php endif; ?>
	<div class="row m-b-70">
		<div class="col-md-6 text-left">
			<?php
			if ($modelJob->id) {

				if ( $modelJob->Status == 0  ) {
					ECHO Html::a(Yii::t('app', '<i class="fa fa-trash"></i> DELETE '), ['delete', 'id' => $modelJob->id], ['class' => 'btn btn-danger  btn-lg text-white active ',  'data-method' => 'post',
															'data-pjax' => 1,
															'data-confirm' => 'ARE YOU SURE TO DELETE THIS JOB?',]);;
				}
				?>
				&nbsp;
				<?php
			}
			 ?>
		</div>
		<div class="col-md-6 text-right">
			<?php
				if ($modelJob->id) {


					// code...
					$btn_text2 = ($modelJob->flag == 0) ? Yii::t('app', 'EDIT') : Yii::t('app', 'REVISE REQUEST');

					echo Html::submitButton($btn_text2, ['class' => 'btn btn-warning  btn-lg saveClick' , 'name' => 'submit[]', 'value' => '0']);
					echo '&nbsp;'.Html::a(Yii::t('app', 'CANCEL'), ($modelJob->flag == 0) ? ['index'] : ['index', 'id' => $modelJob->job_code], ['class' => 'btn btn-info btn-lg']);
				}
				else {
					$btn_text2 = ($modelJob->flag == 0) ? Yii::t('app', 'SAVE') : Yii::t('app', 'REVISE REQUEST');
					echo Html::submitButton($btn_text2, ['class' => 'btn btn-success  btn-lg']);
					echo '&nbsp;'.Html::a(Yii::t('app', 'CANCEL'), ($modelJob->flag == 0) ? ['index'] : ['index', 'id' => $modelJob->JobCode], ['class' => 'btn btn-info btn-lg']);
				}






			?>
		</div>

	</div>


<?php \yii\widgets\Pjax::end(); ?>
<?php ActiveForm::end(); ?>



<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlBudgetDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budgetdelete']); ?>';
	paramJs.urlBudgetShowModal = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budget']); ?>';
	paramJs.urlOthersExpense = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/otherexpense']); ?>';
	paramJs.urlLatesJob = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/latesjob']); ?>';
	paramJs.urlReqsave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/reqsave']); ?>';
	paramJs.urlReqSaveAllowance = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/reqsaveallowance']); ?>';
	paramJs.urlDefault = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/default']) ?>'
	paramJs.urlOutOfOfficeAllowance = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/outofficeallowance']) ?>';

		function BudgetShowModal(link="") {
			var link = (link || paramJs.urlBudgetShowModal);
			// $('#detail').html("loading...");
			  showFullLoading();
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {
					  hideFullLoading();
					$('#detail').html(data);
				},
			});
		}

		function BudgetCreate() {
			BudgetShowModal();
			$('#DetailModal').modal('show');
		}
		function BudgetUpdate(link) {
			BudgetShowModal(link);
			$('#DetailModal').modal('show');
		}
		function BudgetDelete(EmployeeID, JobID) {
			if(confirm("Are you sure want to delete this item permanently ?")){
				$.ajax({
					url: paramJs.urlBudgetDelete,
					data: {EmployeeID:EmployeeID, JobID:JobID},
					method: "POST",
					dataType: 'json',
					success: function () {
						reload();
					},
					complete: function(data) {},
					error: function(data) {}
				});
			}
		}
		function OthersExpense() {
			OthersExpenseShowModal();
			$('#DetailModal').modal('show');
		}
		function OthersExpenseShowModal(link="") {
			var link = (link || paramJs.urlOthersExpense);
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {
					$('#detail').html(data);
				},
			});
		}
		window.closeModal = function(){
			reload();
		};
		function reload(){
			$.pjax.defaults.timeout = false;
			$.pjax.reload({
				container: '#job-pjax'
			})
			$('#DetailModal').modal('hide');
			$('#DetailModal').data('modal', null);
		}

		function lateJob(){

				var link = (link || paramJs.urlLatesJob);
				$.ajax({
					url: link,
					data:  $('#job-form').serialize(),
					method: "POST",
					dataType: 'html',
					success: function(data) {
						var d = JSON.parse(data)
						$('#CLientID').html(d.ClientID);
						$('#job-jobcode').val(d.job);
					},
				});

		}

		function reqsave()
		{
			var link = (link || paramJs.urlReqsave);
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {
					console.log(data);

				},
			});
		}

		function reqsaveallowance()
		{
			var link = (link || paramJs.urlReqSaveAllowance);
			 showFullLoading();
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {
					  hideFullLoading();
					console.log(data);
					var d = JSON.parse(data)
					$('#job-mealallowance').val(d.MealAllowance);
					$('#job-taxiallowance').val(d.TaxiAllowance);
					$('#job-outofofficeallowance').val(d.OutOfOfficeAllowance);
				},
			});
		}

		function defaults()
		{
			var link = (link  || paramJs.urlDefault);
			 showFullLoading();
			$.ajax({
				url : link,
				data : $('#job-form').serialize(),
				method : "POST",
				dataType : 'html',
				success:function(data){
					var d = JSON.parse(data);
					hideFullLoading();
					$('#job-mealallowance').val(d.MealAllowance);
					$('#job-taxiallowance').val(d.TaxiAllowance);
					$('#job-outofofficeallowance').val(d.OutOfOfficeAllowance);
					$('#job-administrativecharge').val(d.AdministrativeCharge);
					$('#job-otherexpenseallowance').val(d.OtherExpenseAllowance);
					$('.data-r').html(d.Percentage);


				},
			});
		}

		function EstimatedAllowance()
		{
			var link = (link || paramJs.urlOutOfOfficeAllowance);
			$.ajax({
				url: link,
				data:  $('#job-form').serialize(),
				method: "POST",
				dataType: 'html',
				success: function(data) {

					var d = JSON.parse(data);

					$('#job-mealallowance').val(d.MealAllowance);
					$('#job-taxiallowance').val(d.TaxiAllowance);
					$('#job-outofofficeallowance').val(d.OutOfOfficeAllowance);
					$('#job-administrativecharge').val(d.AdministrativeCharge);
					$('#job-otherexpenseallowance').val(d.OtherExpenseAllowance);
					// $('#job-otherexpenseallowance').val(d.OutOfficeAllowance);
					$('.data-r').html(d.Percentage);

				},
			});
		}

		$(document).ready(function(){
			$('.submitClick').on("click", function(){
					 showFullLoading();
			});
			$('.saveClick').on("click", function(){
					 showFullLoading();
			});
		});

</script>
