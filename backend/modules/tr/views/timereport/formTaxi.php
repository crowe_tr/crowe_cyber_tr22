<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

use yii\widgets\ListView;
use yii\widgets\Pjax;
use dosamigos\switchery\Switchery;
use yii\widgets\MaskedInput;
use kartik\widgets\TimePicker;
?>
<?php
$form = ActiveForm::begin([
        'id' => 'trdetail-form',
        'enableClientValidation' => true,
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true,
        'fieldConfig' => [
            'template' => '{label}{input}',
            'options' => [
                'class' => 'form-group form-group-default',
            ],
		],
		'errorSummaryCssClass'=> 'alert alert-danger'
	]);

    echo $form->errorSummary($modelTaxi);
    echo $form->field($modelTaxi, 'EmployeeId', ['options' => ['class' => '', 'id'=>'EmployeeId']])->hiddenInput()->label(false);
    echo $form->field($modelTaxi, 'Date', ['options' => ['class' => '']])->hiddenInput()->label(false);

    echo $form->field($modelTaxi, 'TimeReportID', ['options' => ['class' => '']])->hiddenInput()->label(false);
    echo $form->field($modelTaxi, 'Seq', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
<div class="alert alert-danger" id="error" style="display:none">
</div>

<p><b>TAXI DETAILS :</b></p>
<div class="form-group-attached p-b-5">
    <div class="row">
        <div class="col-sm-3">
            <?php
                echo $form->field($modelTaxi, 'cmTaxiID', ['options' => ['onchange' => '', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(
                    Select2::classname(),
                    [
                        'options' => ['placeholder' => 'Select...'],
                        'data' => $data['cmTaxiID'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'placeholder' => Yii::t('backend', 'Select..'),
                        ],
                        'options' => ['id' => 'cmTaxiID', 'placeholder' => 'Select ...'],
                    ]
                );
            ?>        
        </div>
        <div class="col-sm-9">
            <?php
                echo $form->field($modelTaxi, 'TrDetID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
                    'data' => $data['lookup'],
                    'options' => ['id' => 'lookup', 'placeholder' => 'Select ...'],
                    'type' => DepDrop::TYPE_SELECT2,
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'depends' => ['cmTaxiID', 'TaskTypeID'],
                        'url' => Url::to(['/cm/helper/loadtimereportlookuptaxi', 'TimeReportID'=>$modelTaxi->TimeReportID, 'TrDetID'=>empty($modelTaxi->TrDetID) ? 0 : $modelTaxi->TrDetID]),
                        'loadingText' => 'Loading ...',
                    ],
                ]);
            
            ?>        

        </div>
    </div>

</div>
<div class="form-group-attached">
    <div class="row p-b-5">
        <div class="col-sm-8">
            <?php echo $form->field($modelTaxi, 'VoucherNo')->textInput(); ?>

        </div>
        <div class="col-sm-2">
            <?php 
                echo $form->field($modelTaxi, 'Start')->widget(TimePicker::classname(), [
                    'class' => '',
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 1,
                        'secondStep' => 5,
                        'template' => false
                    ],
                ]);

            ?>	
        </div>
        <div class="col-sm-2">
            <?php 
                echo $form->field($modelTaxi, 'Finish')->widget(TimePicker::classname(), [
                    'class' => '',
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 1,
                        'secondStep' => 5,
                        'template' => false
                    ],
                ]);
            ?>	
        </div>
    </div>
</div>
<div class="form-group-attached">
    <div class="row p-b-5">
        <div class="col-sm-8">
            <?php 
                echo $form->field($modelTaxi, 'Destination')->textInput(); 
            ?>	
        </div>
        <div class="col-sm-4">
        <?=
                $form->field($modelTaxi, 'Amount')->widget(MaskedInput::className(), [
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'groupSeparator' => ',',
                            'autoGroup' => true,
                        ],
                    ]);
              ?>
        </div>
    </div>
    <div class="row p-b-5">
        <div class="col-sm-12">
            <?php 
                echo $form->field($modelTaxi, 'Description')->textArea(); 
            ?>	
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12 text-right">
        <hr class="m-b-5"/>
        <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
        <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" onclick="CloseModal()">CANCEL</button>
    </div>
</div>
 
<?php ActiveForm::end(); ?>

<script type="text/javascript">
	var paramJs = (paramJs || {});
    paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxisave']); ?>';
    $('#trdetail-form').on('beforeSubmit', function() {
        showFullLoading();

        var form = new FormData($('#trdetail-form')[0]);
        $.ajax({
            url: paramJs.urlFormSave,
            type: 'POST',
            data: form,
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function (data) {
                hideFullLoading();

                if(data != 1){
                    $('#error').show();
                    $('#error').html(data);
                }else{
                    $('#trdetail-modal').modal('hide');
                    reload();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown, data) {
                hideFullLoading();

                $('#error').show();
                var err = unescape(XMLHttpRequest.responseText);
                err = err.split('&#039;');
                err = err[3];
                err = escape(err);

                err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                err = unescape(err[1]);
                
                $('#error').html(err);
            }
        });
        return false;
    });
    function CloseModal(){
        $('#ChildModal').modal('hide');
    }


</script>