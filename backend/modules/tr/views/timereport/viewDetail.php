<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\grid\GridView;
use yii\web\View;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use dosamigos\switchery\Switchery;
use common\components\Helper;

                $column_task = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'header' => 'No',
                        'mergeHeader' => false,
                        'headerOptions' => ['class' => 'bg-success b-r'],
                        'contentOptions' => ['class' => 'text-right b-r'],
                        'filterOptions' => ['class' => 'b-b b-grey b-r'],
                        'width' => '36px',
                    ],
                    [
                        'label' => 'Description',
                        'attribute' => 'JobId',
                        'headerOptions' => ['class' => 'col-sm-9 col-9 bg-success'],
                        'filterOptions' => ['class' => 'b-b b-grey'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'filterInputOptions' => ['placeholder' => ''],
                        'format' => 'raw',
                        'value' => function($model){
                            $sql_job = "select TrJobName('".$model->TimeReportID."', '".$model->id."') as Description";
                            $job = Yii::$app->db->createCommand($sql_job)->queryOne();
                            return empty($job['Description']) ? "" : $job['Description'];
                        }
                    ],
                    [
                        'label' => 'WH',
                        'attribute' => 'WorkHour',
                        'headerOptions' => ['class' => 'col-sm-1 col-1 bg-success'],
                        'filterOptions' => ['class' => 'b-b b-grey'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                    ],
                    [
                        'label' => 'OT',
                        'attribute' => 'Overtime',
                        'headerOptions' => ['class' => 'col-sm-1 col-1 bg-success'],
                        'filterOptions' => ['class' => 'b-b b-grey'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                    ],
                    [
                        'label' => 'Status',
                        'attribute' => 'Status',
                        'headerOptions' => ['class' => 'col-sm-1 col-1 bg-success'],
                        'filterOptions' => ['class' => 'b-b b-grey'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'filterInputOptions' => ['placeholder' => ''],
                        'format' => 'raw',
                        'value' => function($model){
                            $sql_Status= "select TrTimeReportDetailStatus('".$model->Approval1."', '".$model->Approval2."', 1, 1, 1) as Status";
                            $Status = Yii::$app->db->createCommand($sql_Status)->queryOne();
                            return empty($Status['Status']) ? "" : $Status['Status'];
                        }
                    ],
                    [
                        'header' => '',
                        'headerOptions' => ['class' => 'bg-success text-center'],
                        'filterOptions' => ['class' => 'b-b b-grey'],
                        'contentOptions' => ['class' => 'no-padding'],

                        'format' => 'raw',
                        'value' => function ($model) {
                            $button = "";
                            if(($model->parent->Status == 0 or $model->Approval1 == 3) && ($model->Approval1 != 1)) {
                                $button = Html::a(
                                    '<i class="fa fa-clock-o"></i> Ovetime',
                                    false,
                                    [
                                        'onclick' => "update(1, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/overtime', 'id' => $model->id])."')",
                                        'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                        'style' => 'bjob-radius: 5px !important',
                                    ]).' '.
                                    Html::a(
                                    '<i class="fa fa-edit"></i> Update',
                                    false,
                                    [
                                        'onclick' => "update(1, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detail', 'id' => $model->id])."')",
                                        'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                        'style' => 'bjob-radius: 5px !important',
                                    ]).' ';
                            }
                            if(
                                ($model->timeReport->Status == 0 or $model->Approval1 == 3 or $model->Approval2 == 3) 
                            ) {
                                $button .= Html::a(
                                    '<i class="fa fa-trash"></i>',
                                    false,
                                    [
                                        'onclick' => "itemdelete(1, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detaildelete', 'id' => $model->id])."')",
                                        'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                        'style' => 'bjob-radius: 5px !important',
                                    ]);
                            }
                            $val = '<div class="tooltip-action">
                                    <div class="trigger">
                                        '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
                                    </div>
                                    <div class="action-mask">
                                        <div class="action">
                                            '.Html::a(
                                                '<i class="fa fa-comment"></i> Comments',
                                                false,
                                                [
                                                    'onclick' => "Comments('".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $model->TimeReportID, 'id' => $model->id, 'type'=>1])."')",
                                                    'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                                    'style' => 'bjob-radius: 5px !important',
                                                ]
                                            ).' '.$button.'
                                        </div>
                                    </div>
                                    </div>';

                            return $val;
                        },
                    ],
                ];
                echo GridView::widget(
                    [
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'columns' => $column_task,
                        'layout'=>'
                        <div class="card card-default">
                            <div class="row ">
                                <div class="col-md-12">
                                    {items}
                                </div>
                            </div>
                        </div>
                            ',
                        'resizableColumns' => false,
                        'bordered' => false,
                        'striped' => true,
                        'condensed' => false,
                        'responsive' => true,
                        'responsiveWrap' => false,
                        'hover' => false,
                        'persistResize' => false,
                    ]
                );

            ?>
