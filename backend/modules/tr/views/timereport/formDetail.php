<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

use yii\widgets\ListView;
use yii\widgets\Pjax;
use dosamigos\switchery\Switchery;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;
use kartik\widgets\TimePicker;
use common\models\cl\viewClientZone;
use common\components\ant;

?>

<?php

$form = ActiveForm::begin([
    'id' => 'trdetail-form',
    'options' => ['class' => 'ant-form', 'enctype' => 'multipart/form-data'],
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default ',
        ],
    ],
    'errorSummaryCssClass' => 'alert alert-danger'
]);

echo $form->field($modelDetail, 'EmployeeId', ['options' => ['class' => '', 'id' => 'EmployeeId']])->hiddenInput()->label(false);
echo $form->field($modelDetail, 'Date', ['options' => ['class' => '']])->hiddenInput()->label(false);

echo $form->field($modelDetail, 'TimeReportID', ['options' => ['class' => '']])->hiddenInput()->label(false);
echo $form->field($modelDetail, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
<div class="alert alert-danger" id="error" style="display:none">
</div>
<?= $form->errorSummary($modelDetail); ?>
<div class="form-group-attached p-b-5">
    <div class="row">
        <div class="col-sm-4">
            <?php
            echo $form->field($modelDetail, 'TaskTypeID', ['options' => ['onchange' => 'toggleJob();', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(
                Select2::classname(),
                [
                    'options' => ['placeholder' => 'Select...'],
                    'data' => $data['TaskTypeID'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'placeholder' => Yii::t('backend', 'Select..'),
                    ],
                    'options' => ['id' => 'TaskTypeID', 'placeholder' => 'Select ...'],
                ]
            );
            ?>
        </div>
        <div class="col-sm-8">
            <?php
            echo $form->field($modelDetail, 'TaskID', ['options' => ['onchange' => 'toggleAttachment();', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
                'data' => $data['TaskID'],
                'options' => ['id' => 'TaskID', 'placeholder' => 'Select ...'],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'pluginOptions' => [
                    'allowClear' => false,
                    'depends' => ['TaskTypeID'],
                    'url' => Url::to(['/cm/helper/loadtimereporttask']),
                    'loadingText' => 'Loading ...',
                ],
            ]);
            ?>

        </div>
    </div>
</div>
<div class="form-group-attached p-b-5">

    <div class="row">
        <div class="col-sm-12" id="job-wrapper" style="<?php echo ($modelDetail->TaskTypeID <> 1) ? "display:none;" : ""; ?>">
            <?php
            echo $form->field($modelDetail, 'JobId', [
                'options' => [
                    'class' => ' form-group form-group-default form-group-default-select2'
                ]
            ])->widget(DepDrop::classname(), [
                'data' => $data['JobId'],
                'options' => ['id' => 'JobId', 'placeholder' => 'Select ...'],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'pluginOptions' => [
                    'allowClear' => false,
                    'depends' => ['TaskTypeID', 'TimeReportID'],
                    'url' => Url::to(['/cm/helper/loadtimereportjobs', 'id' => $modelDetail->TimeReportID, 'job' => ($modelDetail->isNewRecord) ? 0 : $modelDetail->JobId]),
                    'loadingText' => 'Loading ...',
                ],
            ]);
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 TimeAvailable" style="<?php echo ($modelDetail->TaskTypeID <> 1) ? "display:none;" : ""; ?>">
        <?= !empty($TimeAvailable) ? $TimeAvailable : ""; ?>
    </div>
</div>
<p>
    <b>TASK & WORKHOURS</b>
</p>
<div class="form-group-attached">
    <div class="row p-b-5">
        <div class="col-sm-3">
            <?=
                $form->field($modelDetail, 'WorkHour')->widget(MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' => 'decimal',
                        'groupSeparator' => ',',
                        'autoGroup' => true,
                    ],
                ]);
            ?>
        </div>
        <div class="col-sm-9">
            <?= $form->field($modelDetail, 'Description')->textInput(); ?>
        </div>
    </div>
</div>
<p>
</p>

<div class="row">
    <div class="col-md-12" id="attachment" style="display: none;">
        <?= ant::inputfile($form, $modelDetail, 'attachment', 'thumbnail2', 'Attach attachments if needed'); ?>
    </div>
</div>


<div class="row">
    <div class="col-md-12 text-right">
        <hr class="m-b-5" />
        <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
        <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" onclick="CloseModal()">CANCEL</button>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    var paramJs = (paramJs || {});
    paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detailsave']); ?>';
    paramJs.urlJobTimeAvailable = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/getjobtimeavailable']); ?>';
    paramJs.urlAttachment = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/getattachment']); ?>';
    $('#trdetail-form').on('beforeSubmit', function() {
        showFullLoading();

        var form = new FormData($('#trdetail-form')[0]);
        $.ajax({
            url: paramJs.urlFormSave,
            type: 'POST',
            data: form,
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function(data) {
                hideFullLoading();

                if (data != 1) {
                    $('#error').show();
                    var err = unescape(XMLHttpRequest.responseText);
                    err = err.split('&#039;');
                    err = err[3];
                    err = escape(err);

                    err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                    err = unescape(err[1]);

                    if (err === "undefined") {
                        $('#error').html(data);
                    } else {
                        $('#error').html(err);
                    }
                } else {
                    $('#trdetail-modal').modal('hide');
                    reload();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown, data) {
                hideFullLoading();

                $('#error').show();
                var err = unescape(XMLHttpRequest.responseText);
                err = err.split('&#039;');
                err = err[3];
                err = escape(err);

                err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                err = unescape(err[1]);

                if (err === "undefined") {
                    $('#error').html(data);
                } else {
                    $('#error').html(err);
                }

            }
        });
        return false;
    });
    $('#JobId').on('change', function() {
        showFullLoading();
        $.ajax({
            url: paramJs.urlJobTimeAvailable,
            type: 'POST',
            data: {
                id: $('#JobId').val()
            },
            dataType: 'html',
            success: function(data) {
                hideFullLoading();
                $('.TimeAvailable').html(data);
            },
            error: function(data) {
                hideFullLoading();
            }
        });
    });

    function CloseModal() {
        $('#ChildModal').modal('hide');
    }

    function toggleJob() {
        if ($('#TaskTypeID').val() == 1) {
            $('#job-wrapper').show();
            $('.TimeAvailable').show();
        } else {
            $('#job-wrapper').hide();
            $('.TimeAvailable').hide();
        }
    }

    function toggleAttachment() {
        showFullLoading();
        $.ajax({
            url: paramJs.urlAttachment,
            type: 'POST',
            data: {
                id: $('#TaskID').val()
            },
            dataType: 'html',
            success: function(data) {
                hideFullLoading();
                if (data == 1) {
                    $('#attachment').show();
                } else {
                    $('#attachment').hide();
                }
            },
            error: function(data) {
                hideFullLoading();
            }
        });

    }
    toggleAttachment();
</script>