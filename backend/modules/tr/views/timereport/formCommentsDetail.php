<?php

use yii\widgets\ListView;
use kop\y2sp\ScrollPager;

echo ListView::widget([
        'dataProvider' => $dataProviderComments,
        'emptyText' => '',
        'layout' => '{items}{pager}',
        'itemView' => 'formCommentsItem',
        'pager' => [
        'class' => ScrollPager::className(),
        'enabledExtensions' => [
            ScrollPager::EXTENSION_TRIGGER => 1000,
            ScrollPager::EXTENSION_PAGING,
            ScrollPager::EXTENSION_NONE_LEFT,
            ScrollPager::EXTENSION_SPINNER,
        ],
        'noneLeftText' => 'There is no comment left',
        'overflowContainer' => '.notification-body',
    ],
]);
