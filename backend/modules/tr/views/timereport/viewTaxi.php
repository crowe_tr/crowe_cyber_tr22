<?php
    use yii\helpers\Html;
    use common\components\Helper;
    $sql_Status= "select TrTimeReportDetailStatus('".$model->Approval1."', '".$model->Approval2."', 1, 1, 0) as StatusText";
    $status = Yii::$app->db->createCommand($sql_Status)->queryOne();
    $StatusText = empty($status['StatusText']) ? "" : $status['StatusText'];

    $sql_Status= "select TrTimeReportDetailStatus('".$model->Approval1."', '".$model->Approval2."', 1, 1, 1) as StatusBadge";
    $status = Yii::$app->db->createCommand($sql_Status)->queryOne();
    $StatusBadge = empty($status['StatusBadge']) ? "" : $status['StatusBadge'];

    $buttons = "";
    if(
        ($model->timeReport->Status == 0 or $model->Approval1 == 3 or $model->Approval2 == 3) 
    ) {
        $buttons = Html::a(
            'Delete <i class="fa fa-trash"></i>',
            false,
            [
                'onclick' => "itemdelete(4, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxidelete', 'id' => $model->TimeReportID, 'seq' => $model->Seq])."')",
                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                'style' => 'bjob-radius: 5px !important',
            ])
            .'&nbsp;'
            .Html::a(
                'Update <i class="fa fa-edit"></i>',
                false,
                [
                    'onclick' => "update(4, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxi', 'id' => $model->TimeReportID, 'seq' => $model->Seq])."')",
                    'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                    'style' => 'bjob-radius: 5px !important',
                ]);
    }


    $Taxi = !empty($model->taxi->name) ? $model->taxi->name: "";
    $Start = !empty($model->Start) ? $model->Start: "";
    $Finish = !empty($model->Finish) ? $model->Finish: "";
    $VoucherNo = !empty($model->VoucherNo) ? $model->VoucherNo : "";
    $Destination = !empty($model->Destination) ? $model->Destination : "";
    $Amount = !empty($model->Amount) ? number_format($model->Amount, 0) : "";
    $Description = !empty($model->Description) ? $model->Description : "";

?>
<div class='card card-danger m-t-5 m-b-0'>
    <div class='card-body padding-10'>
        <div class='row'>
            <div class='col-sm-8'>
                <b>
                <?php
                    $sql_job = "select TrJobName(".$model->TimeReportID.", ".$model->TrDetID.") as Description";
                    $job = Yii::$app->db->createCommand($sql_job)->queryOne();
                    echo empty($job['Description']) ? "" : $job['Description'];
                ?>
                </b>

                <?=$StatusBadge;?>
                <br/>
                <br/>
                <table class="table table-bordered" >
                <tr>
                        <td width="120px">Type</td>
                        <td width="10px">:</td>
                        <td><?=$Taxi?></td>
                    </tr>
                    <tr>
                        <td width="100px">Start - Finish</td>
                        <td width="10px">:</td>
                        <td><?=date('H:i', strtotime($Start));?> - <?=date('H:i', strtotime($Finish));?></td>
                    </tr>
                    <tr>
                        <td>Voucher No</td>
                        <td width="10px">:</td>
                        <td><?=$VoucherNo?></td>
                    </tr>
                    <tr>
                        <td>Destination</td>
                        <td width="10px">:</td>
                        <td><?=$Destination?></td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td width="10px">:</td>
                        <td>Rp. <?=$Amount?></td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td width="10px">:</td>
                        <td><?=$Description?></td>
                    </tr>
                </table>

            </div>
            <div class='col-md-4 col-sm-12 text-right m-t-5'>
                <div class="btn-group ">
                    <?=$buttons;?>
                </div>
            </div>
        </div>
    </div>
</div>
