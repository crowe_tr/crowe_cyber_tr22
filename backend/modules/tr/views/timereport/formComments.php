<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\grid\GridView;
use kartik\widgets\FileInput;

use yii\widgets\ListView;
use yii\widgets\Pjax;

?>
<div class=" notification-toggle" role="menu" aria-labelledby="notification-center">
    <div class="notification-panel" style="width: 100%; border: none;">
        <div class="notification-body scrollable scroll-content scroll-scrolly_visible bg-pattern">
            <div id="ModalCommentsList" style="height: 300px; overflow: auto; box-shadow: none; max-height: 320px; height: 320px; overflow:auto; background: red url('/../../themes/pages/assets/img/pattern.jpg') repeat scroll 0% 0% !important;">
                <?php 
                    echo $list;
                ?>
            </div>
        </div>
        <div class="notification-footer" id="ModalCommentsForm">
            <?php
                $form = ActiveForm::begin([
                    'id' => 'comment-form',
                    'enableClientValidation' => true,
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                ]);
            ?>
            <?= $form->field($modelComments, 'TimeReportID', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
            <?= $form->field($modelComments, 'ParentID', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
            <?= $form->field($modelComments, 'Type', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
            <?php
            
                echo $form->field($modelComments, 'For', ['options' => ['onchange' => 'CommentsRefresh()', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(
                    Select2::classname(),
                    [
                        'options' => ['placeholder' => 'Select...'],
                        'data' => $forLookUp,
                        'options' => ['id' => 'lookup', 'placeholder' => 'Select ...'],
                    ]
                );
            ?>        

            <?= $form->field($modelComments, 'Notes')->label(false)->widget(\yii\redactor\widgets\Redactor::className([
                'clientOptions' => [
                    'imageManagerJson' => ['/redactor/upload/image-json'],
                    'imageUpload' => ['/redactor/upload/image'],
                    'fileUpload' => ['/redactor/upload/file'],
                    'plugins' => ['clips', 'fontcolor', 'imagemanager'],
                ],
            ]), ['options' => ['height' => '300px', 'id'=>'Notes', 'class'=>'m-t-20']]); ?>

            <div class="row m-t-10">
                <div class="col-sm-8">
                    <a class=" btn btn-lg btn-warning btn-block text-white" onclick="CommentsSave()" style="opacity: initial" href="javascript:;">
                        <b><i class="fa fa-sends"></i> SEND COMMENT</b>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a class=" btn btn-lg btn-info btn-block text-white" data-dismiss="modal" href="javascript:;">
                        <b><i class="fa fa-sends"></i> CLOSE</b>
                    </a>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlCommentsSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/commentssave']); ?>';
	paramJs.urlCommentsRefresh = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/commentsrefresh']); ?>';

    function CommentsSave(){
        showFullLoading();

        $('#ModalCommentsList').html("Loading ...");

        $.ajax({ 
            url: paramJs.urlCommentsSave,
            type: 'POST',
			data:  $('#comment-form').serialize(),
			success: function(data) {
                hideFullLoading();

				$('#ModalCommentsList').html(data);
                $('#timereportnote-notes').val("");
                $('.redactor-editor').html("");
			},
            error: function(XMLHttpRequest, textStatus, errorThrown, data) {
                hideFullLoading();

                $('.custom-error').show();
                var err = unescape(XMLHttpRequest.responseText);
                err = err.split('&#039;');
                err = err[3];
                err = escape(err);

                err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                err = unescape(err[1]);
                
                $('.custom-error').html(err);
            }
        });
        return false;
    };
    function CommentsRefresh(){
        showFullLoading();

        $('#ModalCommentsList').html("Loading ...");

        $.ajax({ 
            url: paramJs.urlCommentsRefresh,
            type: 'POST',
			data:  $('#comment-form').serialize(),
			success: function(data) {
                hideFullLoading();

				$('#ModalCommentsList').html(data);
                $('#timereportnote-notes').val("");
                $('.redactor-editor').html("");
			},
            error: function(XMLHttpRequest, textStatus, errorThrown, data) {
                hideFullLoading();

                $('.custom-error').show();
                var err = unescape(XMLHttpRequest.responseText);
                err = err.split('&#039;');
                err = err[3];
                err = escape(err);

                err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                err = unescape(err[1]);
                
                $('.custom-error').html(err);
            }
        });
        return false;
    };
</script>