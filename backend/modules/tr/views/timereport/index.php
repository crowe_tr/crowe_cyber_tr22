<?php
    use Carbon\Carbon;
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\helpers\Url;
    use common\models\cm\CmDept;
    use common\models\cm\CmCompanyBranch;
    use yii\web\View;
    use kartik\widgets\ActiveForm;
    use kartik\widgets\Select2;

    $realToday = Carbon::today();
    $today = Carbon::createFromDate($param['Year'], $param['Month'], 1);
    $tempDate = Carbon::createFromDate($param['Year'], $param['Month'], 1);
?>
<div  id="tr-calendar" >
    <div class="row m-b-10">
        <div class="col-sm-7 col-md-8 col-lg-9">
            <h3 class="bold">TIME REPORT</h3>
        </div>
        <div class="col-sm-5 col-md-4 col-lg-3">
            <?php
                $form = ActiveForm::begin([
                    'id' => 'tr-form',
                    'enableClientValidation' => true,
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                    'fieldConfig' => [
                        'template' => '{input}',
                        'options' => [
                            'class' => '',

                        ],
                    ],
                    'errorSummaryCssClass'=> 'alert alert-danger'
                ]);
            ?>
            <?php
                $_month = [];
                $now = strtotime(date("Y-m-01"));
                for($i = 0; $i<=6; $i++){
                    $_month[date('Y-m-t', strtotime("-{$i} months", $now))] = strtoupper(date('F Y', strtotime("-{$i} months", $now)));
                }
                $model->YearDate = !empty($param['YearDate']) ? $param['YearDate'] : date('Y-m-t');
                echo $form->field($model, 'YearDate', ['options' => ['class' => 'select-withoutlabel']])->widget(
                    Select2::classname(),
                    [
                        'data' =>  $_month,
                        'options' => ['id' => 'YearDate', 'placeholder' => 'Select ...'],
                    ]
                )->label(false);
            ?>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
    <div class="card card-default">
        <div class="card-body no-padding table-responsive">
            <table class="table table-bordered  tablecalendar">
                <thead>
                    <tr>
                        <th class="text-WorkHourite wc"><span>Sun</span><span class="d-none d-md-inline">day</span></th>
                        <th class="text-WorkHourite wc"><span>Mon</span><span class="d-none d-md-inline">day</span></th>
                        <th class="text-WorkHourite wc"><span>Tue</span><span class="d-none d-md-inline">sday</span></th>
                        <th class="text-WorkHourite wc"><span>Wed</span><span class="d-none d-md-inline">nesday</span></th>
                        <th class="text-WorkHourite wc"><span>Thu</span><span class="d-none d-md-inline">rsday</span></th>
                        <th class="text-WorkHourite wc"><span>Fri</span><span class="d-none d-md-inline">day</span></th>
                        <th class="text-WorkHourite wc"><span>Sat</span><span class="d-none d-md-inline">urday</span></th>
                    </tr>
                </thead>
                <?php
                    $calendar = array();
                    if(!empty($data['list'])){
                        foreach($data['list'] as $c){
                            $calendar[$c['Date']]['data'] = $c;
                        }
                    }

                    $skip = $tempDate->dayOfWeek;
                    for($i = 0; $i < $skip; $i++)
                    {
                        $tempDate->subDay();
                    }
                    //loops through month
                    do
                    {
                        echo '<tr>';
                        for($i=0; $i < 7; $i++)
                        {
                            $c = [];
                            $day = '';
                            $button = "javascript:;";

                            $TimeReportID = '';
                            $Date = '';
                            $WorkHour = '';
                            $Overtime = '';
                            $Status = '';
                            $StatusCSS = 'none';
                            $StatusLabel = '';
                            $CalendarSign = 0;
                            $CanEdit = 0;

                            $tooltip = '';

                            if(($today->month == $tempDate->month) && ($realToday->toDateString() >= $tempDate->toDateString())) {
                                $c = isset($calendar[$tempDate->toDateString()]) ? $calendar[$tempDate->toDateString()]['data'] : [];
                                $day = $tempDate->day;
                                $CalendarSign = empty($c['CalendarSign']) ? 0 : $c['CalendarSign'];
                                if($CalendarSign==1){
                                    $StatusCSS = "weekend";
                                    if(date('D', strtotime($c['Date'])) == "Sun"){
                                        $StatusCSS = "sunday";
                                    }elseif($c['isHoliday']==1){
                                        $StatusCSS = "dayoff";
                                    }
                                }

                                $TimeReportID = empty($c['TimeReportID']) ? $TimeReportID : $c['TimeReportID'];
                                $Date = empty($c['Date']) ? $Date : $c['Date'];
                                $WorkHour = empty($c['WorkHour']) ? $WorkHour : $c['WorkHour'];
                                $Overtime = empty($c['Overtime']) ? $Overtime : $c['Overtime'];
                                $Status = empty($c['Status']) ? $Status : $c['Status'];

                                $Status = str_replace("APPROVAL","A",$Status);

                                $StatusCSS = empty($c['StatusCSS']) ? $StatusCSS : $c['StatusCSS'];
                                $StatusLabel = empty($c['StatusLabel']) ? 'draft' : $c['StatusLabel'];

                                $CanEdit = empty($c['CanEdit']) ? $CanEdit : $c['CanEdit'];

                                if(!empty($Date) && $CanEdit == 1){
                                    $button = Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/update', 'id' => $Date]);
                                }

                                $tooltip = '
                                    <div class="tt" id="'.$TimeReportID.'" data-success="0">
                                        Loading ...
                                    </div>
                                ';
                            }

                            echo '<td class="calendar-date '.$StatusCSS.'">
                                    <a href="'.$button.'">
                                        <span class="day">
                                            '.$day.'
                                        </span>
                                        <small class="badge status-cl '.$StatusLabel.'">'.$Status.'</small>
                                        <br/>
                                        <span class="whc badge" id="cal-s">'.$WorkHour.'</span>
                                        <span class="whc badge">'.$Overtime.'</span>
                                        '.$tooltip.'
                                    </a>
                                </td>
                            ';
                            $tempDate->addDay();
                        }
                        echo '</tr>';

                    }while($tempDate->month == $today->month);
                ?>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlDetail = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/indexdetail']); ?>';
    var currentRequest = null;

    $(document).ready(function() {

        $('#YearDate').on('change', function() {
            $('#tr-form').submit();
        });
        $('.calendar-date').on('mouseenter', function(event) {
            var tooltip = $( event.target ).children(".tt");
            var id = $( event.target ).children(".tt").attr('id');

            if(tooltip.attr('data-success')==0){
                tooltip.html("Loading ...");
                currentRequest = $.ajax({
                    url: paramJs.urlDetail,
                    type: 'POST',
                    data:  {id: id},
                    dataType: 'html',
                    beforeSend : function()    {
                        if(currentRequest != null) {
                            currentRequest.abort();
                        }
                    },
                    success: function (data) {
                        tooltip.html(data);
                        tooltip.attr('data-success', 1)
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown, data) {
                        tooltip.html(data);
                    }
                });
            }
        });
    });
</script>
