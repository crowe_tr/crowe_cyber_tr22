<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\grid\GridView;
use yii\web\View;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use dosamigos\switchery\Switchery;
use common\components\Helper;
?>
<div class="modal" id="ChildModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content no-border">
            <div class="modal-header bg-success text-white">
            </div>
            <div id="ChildModaldetail" class="modal-body padding-20">
            </div>
        </div>
    </div>
</div>
<div class="modal fade slide-right" id="ModalComments" role="dialog" aria-hidden="false">
    <div class="modal-dialog no-border">
        <div class="modal-content no-border">
            <div class="modal-header no-border bg-success">
                <p class="text-white">
                    <b><i class="pg-comment fax2"></i> COMMENTS</b>
                </p>
            </div>
            <div id="ModalCommentsBody" class="modal-body no-padding">

            </div>
        </div>
    </div>
</div>

<?php \yii\widgets\Pjax::begin(['id' => 'trTimeReportDetail-pjax', 'enablePushState' => false]); ?>
<div class="padding-10">

    <?php

    $form = ActiveForm::begin([
        'id' => 'tr-form',
        'enableClientValidation' => true,
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true,
        'fieldConfig' => [
            'template' => '{label}{input}',
            'options' => [
                'class' => 'form-group form-group-default',
            ],
        ],
        'errorSummaryCssClass' => 'alert alert-danger'
    ]);

    echo $form->errorSummary($model);
    echo $form->field($model, 'ID', ['options' => ['class' => '']])->hiddenInput()->label(false);

    echo $form->field($model, 'EmployeeId', ['options' => ['class' => '', 'id' => 'EmployeeId']])->hiddenInput()->label(false);
    echo $form->field($model, 'Date', ['options' => ['class' => '']])->hiddenInput()->label(false);


    $model->EmployeeName = !empty($model->employee->fullName) ? $model->employee->fullName : "";
    $model->Level = !empty($model->employee->level->levelName) ? $model->employee->level->levelName : "";
    $isMobile = Helper::getIsMobile();

    $model->Date_label = date('l, F d, Y', strtotime($model->Date));
    ?>

    <div class="alert alert-danger custom-error" id="" style="display:none">
    </div>


    <?php
    if ($model->Status == 1) {
        $status = '<span class="badge badge-success fs-13">SUBMITTED</span>';
    } else {
        $status = '<span class="badge badge-warning fs-13 text-black">PENDING</span>';
    }
    echo '
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="bold fs-24">
                        TIME REPORT ' . $status . '
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 b-b m-b-10" style="border-bottom: thin solid #ccc">
                </div>
            </div>
        ';

    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="<?php echo ($isMobile) ? 'card card-default m-b-10' : ''; ?>">
                <div class="<?php echo ($isMobile) ? 'card-body p-b-0' : ''; ?>">
                    <div class="form-group-attached p-b-5">
                        <div class="row">
                            <div class="col-sm-5 col-md-3">
                                <?php
                                echo $form->field($model, 'Date_label')->textInput(['disabled' => true]);
                                ?>
                            </div>
                            <div class="col-sm-7 col-md-5">
                                <?php
                                echo $form->field($model, 'EmployeeName')->textInput(['disabled' => true]);
                                ?>
                            </div>
                            <div class="col-sm-5 col-md-2">
                                <?php
                                echo $form->field($model, 'Level')->textInput(['disabled' => true]);
                                ?>
                            </div>
                            <div class="col-sm-7 col-md-2">
                                <?php
                                echo $form->field(
                                    $model,
                                    'isStayed',
                                    ['options' => ['class' => 'form-group form-group-default']]
                                )->widget(
                                    Switchery::className(),
                                    [
                                        'options' => [
                                            'disabled' => ($model->Status == 1) ? true : false,
                                            //'onclick'=>'HeaderSave(0)',
                                        ]
                                    ]
                                )->label(false);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($model->Status != 1) {
        echo '<div class="row">
                    <div class="col-md-12 text-center m-t-20">';
        echo Html::a(
            '<i class="fa fa-list-ul"></i> <span class="hidden-xs">TASK</span>',
            false,
            [
                'onclick' => 'update(1)',
                'class' => 'btn btn-lg btn-warning text-white',
            ]
        );
        echo Html::a(
            '<i class="fa fa-spoon"></i> <span class="hidden-xs">MEALS</span>',
            false,
            [
                'onclick' => 'update(2)',
                'class' => 'btn btn-lg btn-primary text-white',
            ]
        );
        echo Html::a(
            '<i class="fa fa-building"></i> <span class="hidden-xs">OUT OF OFFICE</span>',
            false,
            [
                'onclick' => 'update(3)',
                'class' => 'btn btn-lg btn-primary text-white',
            ]
        );
        echo Html::a(
            '<i class="fa fa-taxi"></i> <span class="hidden-xs">TAXI</span>',
            false,
            [
                'onclick' => 'update(4)',
                'class' => 'btn btn-lg btn-primary text-white',
            ]
        );
        echo '</div>
            </div>';
    }
    ?>


    <div class="card card-default m-t-10 m-b-10" style="border-top: thick solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>TASK</b></p>
            <hr />
            <div class='m-t-5 m-b-0' style='overflow: scroll;'>
                <div class=''>
                    <table class='table table-bordered'>
                        <tr>
                            <th class='bg-primary col-10'>DESCRIPTION</th>
                            <th class='bg-primary col-2'>TASK</th>
                            <th class='bg-primary'>WH</th>
                            <th class='bg-primary'>OT</th>
                            <th class='bg-primary'>STATUS</th>
                            <th class='bg-primary'></th>
                        </tr>
                        <?php
                        if (!empty($data['Detail'])) {
                            foreach ($data['Detail'] as $task) {
                                $button = Html::a(
                                    '<i class="fa fa-comment"></i> <span class="d-none d-md-inline">Comments</span>',
                                    false,
                                    [
                                        'onclick' => "Comments('" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $task['TimeReportID'], 'id' => $task['TrDetID'], 'type' => 1]) . "')",
                                        'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                        'style' => 'bjob-radius: 5px !important',
                                    ]
                                );

                                if (
                                    ($model->Status == 0
                                        or $task["Status"] == "APPROVAL1-REVISED"
                                        or $task["Status"] == "APPROVAL2-REVISED"
                                        or $task["Status"] == "REVISED")
                                    and ($task["Status"] != "APPROVAL2-PENDING"
                                        and $task["Status"] != "COMPLETED")
                                ) {
                                    $button .= " " . Html::a(
                                        '<i class="fa fa-clock-o"></i> <span class="d-none d-md-inline">Overtime</span>',
                                        false,
                                        [
                                            'onclick' => "update(1, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/overtime', 'id' => $task['TrDetID']]) . "')",
                                            'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                            'style' => 'bjob-radius: 5px !important',
                                        ]
                                    );
                                    $button .= " " . Html::a(
                                        '<i class="fa fa-edit"></i> <span class="d-none d-md-inline">Update</span>',
                                        false,
                                        [
                                            'onclick' => "update(1, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detail', 'id' => $task['TrDetID']]) . "')",
                                            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                            'style' => 'bjob-radius: 5px !important',
                                        ]
                                    );
                                    $button .= " " . Html::a(
                                        '<i class="fa fa-trash"></i>',
                                        false,
                                        [
                                            'onclick' => "itemdelete(1, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detaildelete', 'id' => $task['TrDetID']]) . "')",
                                            'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                            'style' => 'bjob-radius: 5px !important',
                                        ]
                                    );
                                }

                                echo "
                                        <tr>
                                            <td valign='center'>{$task['Task']}</td>
                                            <td class='kv-align-middle'>{$task['TaskName']}</td>
                                            <td align='right' class='kv-align-middle'>{$task['WorkHour']}</td>
                                            <td align='right' class='kv-align-middle'>{$task['Overtime']}</td>
                                            <td align='right' class='kv-align-middle'><span class='badge {$task['StatusLabel']}'>{$task['Status']}</span></td>
                                            <td>
                                                <div class='tooltip-action pull-right'>
                                                    <div class='trigger'>
                                                        " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip']) . "
                                                    </div>
                                                    <div class='action-mask'>
                                                        <div class='action'>
                                                            " . $button . "
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    ";
                            }
                        }
                        /*   
                                echo $this->render('viewDetail', [
                                    'model' => $model,
                                    'dataProvider' => $dataProvider,
                                    'dataProvider_Overtime' => $dataProvider_Overtime,    
                        
                                ]);
                            */
                        ?>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>MEALS</b></p>
            <hr />
            <?php
            if (!empty($data['Meal'])) {
                echo "
                    <div class='m-t-5 m-b-0' style='overflow: scroll;'>
                        <div class=''>
                            <table class='table table-bordered'>
                                <tr>
                                    <th class='bg-primary col-9'>DESCRIPTION</th>
                                    <th class='bg-primary'>AMOUNT</th>
                                    <th class='bg-primary'>STATUS</th>
                                    <th class='bg-primary'></th>
                                </tr>
                    ";
                foreach ($data['Meal'] as $meal) {
                    $button = "";
                    if (
                        ($model->Status == 0
                            or $meal["Status"] == "APPROVAL1-REVISED"
                            or $meal["Status"] == "APPROVAL2-REVISED"
                            or $meal["Status"] == "REVISED")
                        and ($meal["Status"] != "APPROVAL1-REJECTED"
                            and $meal["Status"] != "APPROVAL2-REJECTED"
                            and $meal["Status"] != "APPROVAL2-PENDING"
                            and $meal["Status"] != "COMPLETED")
                    ) {
                        $button .= Html::a(
                            '<i class="fa fa-edit"></i> <span class="d-none d-md-inline">Update</span>',
                            false,
                            [
                                'onclick' => "update(2, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/meals', 'id' => $meal['TimeReportID'], 'seq' => $meal['Seq']]) . "')",
                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                        $button .= " " . Html::a(
                            '<i class="fa fa-trash"></i> <span class="d-none d-md-inline">Delete</span>',
                            false,
                            [
                                'onclick' => "itemdelete(2, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/mealsdelete', 'id' => $meal['TimeReportID'], 'seq' => $meal['Seq']]) . "')",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                    }
                    echo "
                                    <tr>
                                        <td class='bold'>{$meal['Task']}</td>
                                        <td align='right'>Rp." . number_format($meal['MealsValue'], 2) . "</td>
                                        <td align='right'><span class='badge {$meal['StatusLabel']}'>{$meal['Status']}</span></td>
                                        <td  align='right'>
                                            <div class='tooltip-action pull-right'>
                                                <div class='trigger'>
                                                    " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip']) . "
                                                </div>
                                                <div class='action-mask'>
                                                    <div class='action'>
                                                        " . $button . "
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>";
                }

                echo "</table>
                    </div>
                </div>
                ";
            }
            /*
                    echo $this->render('viewMeals', [
                        'model' => $model,
                        'dataProvider_Meals' => $dataProvider_Meals,
        
                    ]);
                */
            ?>
        </div>
    </div>
    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>OUT OF OFFICE</b></p>
            <hr />
            <?php
            if (!empty($data['OutOffice'])) {
                echo "
                    <div class='m-t-5 m-b-0' style='overflow: scroll;'>
                        <div class=''>
                        <table class='table table-bordered'>
                            <tr>
                                <th class='bg-primary col-9'>DESCRIPTION</th>
                                <th class='bg-primary'>ZONE</th>
                                <th class='bg-primary'>AMOUNT</th>
                                <th class='bg-primary'>STATUS</th>
                                <th class='bg-primary'></th>
                            </tr>";
                foreach ($data['OutOffice'] as $outofoffice) {
                    $button = "";
                    if (
                        ($model->Status == 0
                            or $outofoffice["Status"] == "APPROVAL1-REVISED"
                            or $outofoffice["Status"] == "APPROVAL2-REVISED"
                            or $outofoffice["Status"] == "REVISED")
                        and ($outofoffice["Status"] != "APPROVAL1-REJECTED"
                            and $outofoffice["Status"] != "APPROVAL2-REJECTED"
                            and $outofoffice["Status"] != "APPROVAL2-PENDING"
                            and $outofoffice["Status"] != "COMPLETED")
                    ) {
                        $button .= Html::a(
                            '<i class="fa fa-edit"></i> <span class="d-none d-md-inline">Update</span>',
                            false,
                            [
                                'onclick' => "update(3, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/ope', 'id' => $outofoffice['TimeReportID'], 'seq' => $outofoffice['Seq']]) . "')",
                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                        $button .= " " . Html::a(
                            '<i class="fa fa-trash"></i> <span class="d-none d-md-inline">Delete</span>',
                            false,
                            [
                                'onclick' => "itemdelete(3, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/opedelete', 'id' => $outofoffice['TimeReportID'], 'seq' => $outofoffice['Seq']]) . "')",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                    }

                    echo "
                                <tr>
                                    <td class='bold'>{$outofoffice['Task']}</td>
                                    <td align='right'>{$outofoffice['ZoneID']} </td>
                                    <td align='right'>Rp." . number_format($outofoffice['OutOfOfficeValue'], 2) . "</td>
                                    <td align='right'><span class='badge {$outofoffice['StatusLabel']}'>{$outofoffice['Status']}</span></td>
                                    <td align='right'>
                                        <div class='tooltip-action pull-right'>
                                            <div class='trigger'>
                                                " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip']) . "
                                            </div>
                                            <div class='action-mask'>
                                                <div class='action'>
                                                    " . $button . "
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>            
                        ";
                }
                echo "</table>    
                    </div>
                </div>";
            }
            /*
                    echo $this->render('viewOPE', [
                        'model' => $model,
                        'dataProvider_OPE' => $dataProvider_OPE,
        
                    ]);
                */
            ?>
        </div>
    </div>
    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>TAXI</b></p>
            <hr />
            <?php
            if (!empty($data['Taxi'])) {
                echo "
                    <div class='m-t-5 m-b-0' style='overflow: scroll;'>
                        <table class='table table-bordered'>
                        <tr>
                            <th class='bg-primary col-9'>DESCRIPTION</th>
                            <th class='bg-primary'>TYPE</th>
                            <th class='bg-primary'>AMOUNT</th>
                            <th class='bg-primary'>DESTINATION</th>
                            <th class='bg-primary'>STATUS</th>
                            <th class='bg-primary'></th>
                        </tr>
                    ";
                foreach ($data['Taxi'] as $taxi) {
                    $button = "";

                    if (
                        ($model->Status == 0
                            or $taxi["Status"] == "APPROVAL1-REVISED"
                            or $taxi["Status"] == "APPROVAL2-REVISED"
                            or $taxi["Status"] == "REVISED")
                        and ($taxi["Status"] != "APPROVAL1-REJECTED"
                            and $taxi["Status"] != "APPROVAL2-REJECTED"
                            and $taxi["Status"] != "APPROVAL2-PENDING"
                            and $taxi["Status"] != "COMPLETED")
                    ) {
                        $button .= Html::a(
                            '<i class="fa fa-edit"></i> <span class="d-none d-md-inline">Update</span>',
                            false,
                            [
                                'onclick' => "update(4, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxi', 'id' => $taxi['TimeReportID'], 'seq' => $taxi['Seq']]) . "')",
                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                        $button .= " " . Html::a(
                            '<i class="fa fa-trash"></i> <span class="d-none d-md-inline">Delete</span>',
                            false,
                            [
                                'onclick' => "itemdelete(4, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxidelete', 'id' => $taxi['TimeReportID'], 'seq' => $taxi['Seq']]) . "')",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                    }

                    echo "
                                <tr>
                                    <td class='bold'>{$taxi['Task']}</td>
                                    <td>{$taxi['TaxiName']}</td>
                                    <td align='right'>Rp." . number_format($taxi['Amount'], 0) . "</td>
                                    <td>{$taxi['Destination']} </td>
                                    <td align='right'><span class='badge {$taxi['StatusLabel']}'>{$taxi['Status']}</span></td>
                                    <td align='right'>
                                        <div class='tooltip-action pull-right'>
                                            <div class='trigger'>
                                                " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip']) . "
                                            </div>
                                            <div class='action-mask'>
                                                <div class='action'>
                                                    " . $button . "
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>";
                }
                echo "</table>
                </div>";
            }
            /*
                echo ListView::widget([
                    'dataProvider' => $dataProvider_TAXI,
                    'itemView' => 'viewTaxi',
                    'layout' => '{items}',
                ]);
                */
            ?>
        </div>
    </div>

    <div class="row m-b-10">
        <div class="col-md-12  <?php echo ($isMobile) ? 'text-center' : 'text-right'; ?>">

            <?php
            echo Html::a(
                'BACK',
                ['/tr/timereport/index'],
                [
                    'class' => 'btn btn-info text-white p-t-10 p-b-10',
                    'style' => 'bjob-radius: 5px !important',
                ]
            ) . '&nbsp;';
            if ($model->Status == 0) {
                echo '&nbsp;<a class="btn btn-info p-t-10 p-b-10" onclick="HeaderSave(0)" href="javascript:;"><i class="fa fa-edit"></i> SAVE<a/>';
                echo '&nbsp;<a class="btn btn-warning p-t-10 p-b-10" onclick="HeaderSave(1)" href="javascript:;">SUBMIT<a/>';
            }
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>


<script type="text/javascript">
    var paramJs = (paramJs || {});
    paramJs.urlDetailAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detail']); ?>';
    paramJs.urlMealAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/meals']); ?>';
    paramJs.urlOPEAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/ope']); ?>';
    paramJs.urlTaxiAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxi']); ?>';

    paramJs.urlDetailDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detaildelete']); ?>';
    paramJs.urlMealDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/mealsdelete']); ?>';
    paramJs.urlOPEDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/opedelete']); ?>';
    paramJs.urlTaxiDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxidelete']); ?>';

    paramJs.urlComments = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/trsupervisorapproval/comments']); ?>';

    $('#tr-form').submit(function() {
        HeaderSave(0);
    });
    paramJs.urlFormHeaderSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/updatesave', 'id' => $model->Date]); ?>';
    paramJs.urlFormHeaderSubmitedSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/updatesave', 'id' => $model->Date, 'submit' => true]); ?>';

    function HeaderSave($submited) {
        showFullLoading();

        if ($submited == 1) {
            link = paramJs.urlFormHeaderSubmitedSave
        } else {
            link = paramJs.urlFormHeaderSave
        }

        $.ajax({
            url: link,
            type: 'POST',
            data: $('#tr-form').serialize(),
            success: function(data) {
                reload();
                hideFullLoading();

            },
            error: function(XMLHttpRequest, textStatus, errorThrown, data) {

                $('.custom-error').show();
                var err = unescape(XMLHttpRequest.responseText);
                err = err.split('&#039;');
                err = err[3];
                err = escape(err);

                err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                err = unescape(err[1]);
                if (err === "undefined") {
                    $('.custom-error').html(data);
                } else {
                    $('.custom-error').html(err);
                }


                $('html, body').animate({
                    scrollTop: 0
                }, 0);
                $('.switchery').trigger('click');
                hideFullLoading();

            }
        });
        return false;
    }

    function Comments(link) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");

        $.ajax({
            url: link,
            data: {},
            method: "POST",
            dataType: 'html',
            success: function(data) {
                $('#ModalCommentsBody').html(data);
                hideFullLoading();

            },
        });
    }

    function CommentsSave(id) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");
        var link = (link || paramJs.urlComments);

        $.ajax({
            url: link,
            data: {
                id: id
            },
            method: "POST",
            dataType: 'html',
            success: function(data) {
                $('#ModalCommentsBody').html(data);
                hideFullLoading();

            },
        });
    }

    function update(type, link = "") {
        if (link != "") {
            Modal(link);
        } else {
            if (type == 1) {
                Modal(paramJs.urlDetailAdd);
            } else if (type == 2) {
                Modal(paramJs.urlMealAdd);
            } else if (type == 3) {
                Modal(paramJs.urlOPEAdd);
            } else if (type == 4) {
                Modal(paramJs.urlTaxiAdd);
            }
        }
    }

    function itemdelete(type, link) {
        //showFullLoading();

        if (confirm("Are you sure want to delete this item permanently ?")) {
            $.ajax({
                url: link,
                data: {},
                method: "POST",
                dataType: 'json',
                success: function() {
                    reload();
                    hideFullLoading();

                    var err = unescape(XMLHttpRequest.responseText);
                    err = err.split('&#039;');
                    err = err[3];
                    err = escape(err);

                    err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                    err = unescape(err[1]);
                    //alert(err);
                    //$('.custom-error').html(err);
                    if (err === "undefined") {
                        //$('.custom-error').html(data);
                    } else {
                        $('.custom-error').show();
                        $('.custom-error').html(err);
                    }

                    $('html, body').animate({
                        scrollTop: 0
                    }, 0);
                    hideFullLoading();

                },
                error: function(XMLHttpRequest, textStatus, errorThrown, data) {

                    $('.custom-error').show();
                    var err = unescape(XMLHttpRequest.responseText);
                    err = err.split('&#039;');
                    err = err[3];
                    err = escape(err);

                    err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                    err = unescape(err[1]);
                    //alert(err);
                    $('.custom-error').html(err);
                    $('html, body').animate({
                        scrollTop: 0
                    }, 0);
                    hideFullLoading();

                }
            });
        } else {
            hideFullLoading();
        }
    }

    function Modal(link = "") {
        showFullLoading();
        var link = (link || paramJs.urlBudgetShowModal);
        $.ajax({
            url: link,
            data: $('#tr-form').serialize(),
            method: "POST",
            dataType: 'html',
            success: function(data) {
                hideFullLoading();
                $('#ChildModal').modal('show');
                $('#ChildModaldetail').html("Loading ...");

                $('#ChildModaldetail').html(data);
            },
        });
    }

    window.closeModal = function() {
        reload();
        hideFullLoading();
    };

    function reload() {
        $.pjax.defaults.timeout = false;
        $.pjax.reload({
            container: '#trTimeReportDetail-pjax'
        })
        $('#ChildModal').modal('hide');
        $('#ChildModal').data('modal', null);
    }
</script>
<?php \yii\widgets\Pjax::end(); ?>