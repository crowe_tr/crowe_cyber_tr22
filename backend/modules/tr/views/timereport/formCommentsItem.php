<?php
	use yii\helpers\Html;
	use common\components\CommonHelper;

	$user = CommonHelper::getUserIndentity();
?>

<?php if($model->EmployeeId == $user->Id) { ?>
	<div class="card social-card share item no-margin" style="width: 100%; border-radius: 0; background: transparent !important; cursor: pointer; border: 0;">
		<div class="card-header clearfix p-b-5" style="background: none; border: none">
			<div class="text-right">
				<span class="user-pic" style="float: right; margin-left: 10px;">
					<img src="<?= !empty($model->employee->Photo) ? Yii::$app->homeUrl.Yii::$app->params['hrEmployeeBaseurl'].$model->employee->Photo : Yii::$app->homeUrl.Yii::$app->params['appUserDefaultPhotoBaseurl']; ?>"" width="35" height="35">
				</span>

				<h5><?= !empty($model->employee->fullName) ? $model->employee->fullName : '-'; ?></h5>
				<h6>
					<?= empty($model->employee->entity->entityName) ? "" : $model->employee->entity->entityName; ?> 
					<?= empty($model->employee->division->divName) ? "" : " - ".$model->employee->division->divName; ?> 
					<?= empty($model->employee->dept->deptName) ? "" : " - ".$model->employee->dept->deptName; ?>
				</h6>
			</div>
		</div>
		<div class="card-description p-t-0 p-l-50">
			<div class="padding-10 p-t-20 p-b-20" style="background: #d5ec81; color: black; border-radius: 5px; border: 1px solid #ddd; -webkit-box-shadow: 0 0px 1px rgba(0, 0, 0, 0.2); -moz-box-shadow: 0 0px 1px rgba(0, 0, 0, 0.2); box-shadow: 0 0px 1px rgba(0, 0, 0, 0.2);">
				<p><?= Html::decode($model->Notes); ?></p>
				<small class="pull-right">
				<?= date('F d, Y H:i', strtotime($model->CreatedDate)); ?>   
				</small>
			</div>
		</div>
	</div>
<?php } else { ?>
	<div class="card social-card share item no-margin" style="width: 100%; border-radius: 0; background: transparent !important; cursor: pointer; border: 0;">
		<div class="card-header clearfix p-b-5" style="background: none; border: none">
			<div class="user-pic">
				<img src="<?= !empty($model->employee->Photo) ? Yii::$app->homeUrl.Yii::$app->params['hrEmployeeBaseurl'].$model->employee->Photo : Yii::$app->homeUrl.Yii::$app->params['appUserDefaultPhotoBaseurl']; ?>" width="35" height="35">
			</div>
			<h5><?= !empty($model->employee->fullName) ? $model->employee->fullName : '-'; ?></h5>
			<h6>
				<?= empty($model->employee->entity->entityName) ? "" : $model->employee->entity->entityName; ?> 
				<?= empty($model->employee->division->divName) ? "" : " - ".$model->employee->division->divName; ?> 
				<?= empty($model->employee->dept->deptName) ? "" : " - ".$model->employee->dept->deptName; ?>
			</h6>
		</div>
		<div class="card-description p-t-0 p-r-50">
			<div class="padding-10 p-t-20 p-b-20 bg-white" style="border-radius: 5px; border: 1px solid #ddd; -webkit-box-shadow: 0 0px 1px rgba(0, 0, 0, 0.2); -moz-box-shadow: 0 0px 1px rgba(0, 0, 0, 0.2); box-shadow: 0 0px 1px rgba(0, 0, 0, 0.2);">
				<p><?= Html::decode($model->Notes); ?></p>
				<small class="pull-right">
					<?= date('F d, Y H:i', strtotime($model->CreatedDate)); ?>   
				</small>
			</div>
		</div>
	</div>
<?php } ?>