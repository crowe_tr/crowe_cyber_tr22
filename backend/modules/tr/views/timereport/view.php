<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\grid\GridView;

use yii\widgets\ListView;
use yii\widgets\Pjax;
use dosamigos\switchery\Switchery;

?>
<?php \yii\widgets\Pjax::begin(['id' => 'trTimeReportDetail-pjax', 'enablePushState' => false]); ?>

<?php
$form = ActiveForm::begin([
        'id' => 'tr-form',
        'enableClientValidation' => false,
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'validateOnType' => false,
        'fieldConfig' => [
            'template' => '{label}{input}',
            'options' => [
                'class' => 'form-group form-group-default',
            ],
		],
		'errorSummaryCssClass'=> 'alert alert-danger'
	]);

    echo $form->errorSummary($model);
    echo $form->field($model, 'ID', ['options' => ['class' => '']])->hiddenInput()->label(false);

    echo $form->field($model, 'EmployeeId', ['options' => ['class' => '', 'id'=>'EmployeeId']])->hiddenInput()->label(false);
    echo $form->field($model, 'Date', ['options' => ['class' => '']])->hiddenInput()->label(false);


    $model->EmployeeName = !empty($model->employee->fullName) ? $model->employee->fullName : "";
    $model->Level = !empty($model->employee->level->levelName) ? $model->employee->level->levelName : "";

?>
<h3 class="bold fs-24">TIME REPORT : <?= date('d F Y', strtotime($model->Date)); ?></h3>
<div class="row">
    <div class="col-sm-12 b-b m-b-10" style="border-bottom: thin solid #ccc"></div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group-attached p-b-5">
            <div class="row">
                <div class="col-sm-6">
                    <?php 
                        echo $form->field($model, 'EmployeeName')->textInput(['disabled'=>true]); 
                    ?>	
                </div>
                <div class="col-sm-3">
                    <?php 
                        echo $form->field($model, 'Level')->textInput(['disabled'=>true]);
                    ?>	
                </div>
                <div class="col-sm-3">
                    <?php 
                        echo $form->field($model, 'isStayed',
                                ['options' => ['class' => 'form-group form-group-default']]
                                )->widget(
                                Switchery::className(),
                                ['options' => []]
                                )->label(false);
                    ?>	
                </div>
            </div>
        </div>
    </div>

</div>
<div class="card card-default m-t-10 m-b-10">
    <div class="card-body p-t-40 p-b-0">
        <p><b>WORKHOURS</b></p>
        <div class="row">
            <div class="col-sm-12">
                <?php
                $column = [
                    [
                        'label'=>'Description',
                        'attribute' => 'lookup',
                        'headerOptions' => ['class' => 'bg-success col-sm-8'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $TaskType = !empty($data->tasktype->taskTypeName) ? $data->tasktype->taskTypeName : "";
                            $Task = !empty($data->task->taskName) ? " - ".$data->task->taskName : "";
                            $Job = !empty($data->job->Description) ? " - ".$data->job->Description : "";

                            return $TaskType.$Task.$Job;
                        }
                    ],

                    [
                        'label'=>'Workhours / Overtimes',
                        'attribute' => 'WorkHour',
                        'headerOptions' => ['class' => 'bg-success col-sm-2'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $WorkHour = !empty($data->WorkHour) ? $data->WorkHour : "0";
                            $Overtime = !empty($data->Overtime) ? $data->Overtime : "0";
                            return $WorkHour.' / '.$Overtime.' Hours';
                        }

                    ],
                    [
                        'attribute' => 'status',
                        'headerOptions' => ['class' => 'bg-success col-sm-2'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $status = !empty($data->status) ? $data->status : "";
                            switch($status){
                                case 1 :
                                    return "APPROVED";
                                case 2 : 
                                    return "REJECT";
                                default :
                                    return "PENDING";
                            }
                        }
                    ],
                    [
                        'header' => '<i class="fa fa-cogs"></i>',
                        'headerOptions' => ['class' => 'bg-success text-center'],
                        'filterOptions' => ['class' => 'b-b b-grey'],
                        'contentOptions' => ['class' => 'no-padding'],
                        
                        'format' => 'raw',
                        'value' => function ($data) {
                            $val = '<div class="tooltip-action">
                                        <div class="trigger">
                                        '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
                                        </div>
                                        <div class="action-mask">
                                        <div class="action"> 
                                        '.Html::a(
                                            '<i class="fa fa-edit"></i>',
                                            false,
                                            [
                                                'onclick' => "update(1, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detail', 'id' => $data->id])."')",
                                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                            '.Html::a(
                                            '<i class="fa fa-trash"></i>',
                                            false,
                                            [
                                                'onclick' => "itemdelete(1, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detaildelete', 'id' => $data->id])."')",
                                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                            '.Html::a(
                                            '<i class="fa fa-comment"></i>',
                                            false,
                                            [
                                                'onclick' => "Comments('".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $data->TimeReportID, 'id' => $data->id, 'type'=>1])."')",
                                                'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                        </div>
                                        </div>
                                    </div>';
        
                            return $val;
                        },
                    ],

                ];
                ?>
                
                <?php
                    echo GridView::widget(
                        [
                            'dataProvider' => $dataProvider,
                            'columns' => $column,
                            'layout'=>'
                            <div class="card card-default">
                                <div class="row ">
                                    <div class="col-md-12">
                                        {items}
                                    </div>
                                </div>
                            </div>
                                ',
                            'resizableColumns' => false,
                            'bordered' => false,
                            'striped' => true,
                            'condensed' => false,
                            'responsive' => false,
                            'hover' => false,
                            'persistResize' => false,
                        ]
                    );
                ?>
                
            </div>
        </div>
        <p><b>MEALS</b></p>
        <div class="row">
            <div class="col-sm-12">
                <?php
                $column = [
                    
                    [
                        'label'=>'Description',
                        'attribute' => 'lookup',
                        'headerOptions' => ['class' => 'bg-success col-sm-10'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $TaskType = !empty($data->tasktype->taskTypeName) ? $data->tasktype->taskTypeName : "";
                            $Task = !empty($data->task->taskName) ? " - ".$data->task->taskName : "";
                            $Job = !empty($data->job->Description) ? " - ".$data->job->Description : "";

                            return $TaskType.$Task.$Job;
                        }
                    ],


                    [
                        'attribute' => 'status',
                        'headerOptions' => ['class' => 'bg-success col-sm-2'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $status = !empty($data->status) ? $data->status : "";
                            switch($status){
                                case 1 :
                                    return "APPROVED";
                                case 2 : 
                                    return "REJECT";
                                default :
                                    return "PENDING";
                            }
                        }
                    ],

                    [
                        'header' => '<i class="fa fa-cogs"></i>',
                        'headerOptions' => ['class' => 'bg-success text-center'],
                        'filterOptions' => ['class' => 'b-b b-grey'],
                        'contentOptions' => ['class' => 'no-padding'],
                        
                        'format' => 'raw',
                        'value' => function ($data) {
                            $val = '<div class="tooltip-action">
                                        <div class="trigger">
                                        '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
                                        </div>
                                        <div class="action-mask">
                                        <div class="action"> 
                                        '.Html::a(
                                            '<i class="fa fa-edit"></i>',
                                            false,
                                            [
                                                'onclick' => "update(2, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/meals', 'id' => $data->TimeReportID, 'seq' => $data->Seq])."')",
                                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                            '.Html::a(
                                            '<i class="fa fa-trash"></i>',
                                            false,
                                            [
                                                'onclick' => "itemdelete(2, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/mealsdelete', 'id' => $data->TimeReportID, 'seq' => $data->Seq])."')",
                                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                            '.Html::a(
                                            '<i class="fa fa-comment"></i>',
                                            false,
                                            [
                                                'onclick' => "Comments('".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $data->TimeReportID, 'id' => $data->Seq, 'type'=>2])."')",
                                                'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                        </div>
                                        </div>
                                    </div>';
        
                            return $val;
                        },
                    ],

                ];
                ?>
                
                <?php
                    echo GridView::widget(
                        [
                            'dataProvider' => $dataProvider_Meals,
                            'columns' => $column,
                            'layout'=>'
                            <div class="card card-default">
                                <div class="row ">
                                    <div class="col-md-12">
                                        {items}
                                    </div>
                                </div>
                            </div>
                                ',
                            'resizableColumns' => false,
                            'bordered' => false,
                            'striped' => true,
                            'condensed' => false,
                            'responsive' => false,
                            'hover' => false,
                            'persistResize' => false,
                        ]
                    );
                ?>
                
            </div>
        </div>
        <p><b>OUT OF OFFICE</b></p>
        <div class="row">
            <div class="col-sm-12">
                <?php
                $column = [
                    
                    [
                        'label'=>'Description / Zone',
                        'attribute' => 'lookup',
                        'headerOptions' => ['class' => 'bg-success col-sm-8'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $TaskType = !empty($data->tasktype->taskTypeName) ? $data->tasktype->taskTypeName : "";
                            $Task = !empty($data->task->taskName) ? " - ".$data->task->taskName : "";
                            $Job = !empty($data->job->Description) ? " - ".$data->job->Description : "";
                            return $TaskType.$Task.$Job;
                        }
                    ],
                    [
                        'attribute' => 'ZoneID',
                        'headerOptions' => ['class' => 'bg-success col-sm-2'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $ZoneID = !empty($data->ZoneID) ? $data->ZoneID : "";
                            return $ZoneID;
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'headerOptions' => ['class' => 'bg-success col-sm-2'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $status = !empty($data->status) ? $data->status : "";
                            switch($status){
                                case 1 :
                                    return "APPROVED";
                                case 2 : 
                                    return "REJECT";
                                default :
                                    return "PENDING";
                            }
                        }
                    ],


                    [
                        'header' => '<i class="fa fa-cogs"></i>',
                        'headerOptions' => ['class' => 'bg-success text-center'],
                        'filterOptions' => ['class' => 'b-b b-grey'],
                        'contentOptions' => ['class' => 'no-padding'],
                        
                        'format' => 'raw',
                        'value' => function ($data) {
                            $val = '<div class="tooltip-action">
                                        <div class="trigger">
                                        '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
                                        </div>
                                        <div class="action-mask">
                                        <div class="action"> 
                                        '.Html::a(
                                            '<i class="fa fa-edit"></i>',
                                            false,
                                            [
                                                'onclick' => "update(3, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/ope', 'id' => $data->TimeReportID, 'seq' => $data->Seq])."')",
                                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                            '.Html::a(
                                            '<i class="fa fa-trash"></i>',
                                            false,
                                            [
                                                'onclick' => "itemdelete(3, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/opedelete', 'id' => $data->TimeReportID, 'seq' => $data->Seq])."')",
                                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                            '.Html::a(
                                            '<i class="fa fa-comment"></i>',
                                            false,
                                            [
                                                'onclick' => "Comments('".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $data->TimeReportID, 'id' => $data->Seq, 'type'=>3])."')",
                                                'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                        </div>
                                        </div>
                                    </div>';
        
                            return $val;
                        },
                    ],

                ];
                ?>
                
                <?php
                    echo GridView::widget(
                        [
                            'dataProvider' => $dataProvider_OPE,
                            'columns' => $column,
                            'layout'=>'
                            <div class="card card-default">
                                <div class="row ">
                                    <div class="col-md-12">
                                        {items}
                                    </div>
                                </div>
                            </div>
                                ',
                            'resizableColumns' => false,
                            'bordered' => false,
                            'striped' => true,
                            'condensed' => false,
                            'responsive' => false,
                            'hover' => false,
                            'persistResize' => false,
                        ]
                    );
                ?>
                
            </div>
        </div>
        <p><b>TAXI</b></p>
        <div class="row">
            <div class="col-sm-12">
                <?php
                $column = [
                    
                    [
                        'label'=>'Description',
                        'attribute' => 'lookup',
                        'headerOptions' => ['class' => 'bg-success col-sm-10'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $TaskType = !empty($data->tasktype->taskTypeName) ? $data->tasktype->taskTypeName : "";
                            $Task = !empty($data->task->taskName) ? " - ".$data->task->taskName : "";
                            $Job = !empty($data->job->Description) ? " - ".$data->job->Description : "";

                            return $TaskType.$Task.$Job;
                        }
                    ],


                    [
                        'attribute' => 'status',
                        'headerOptions' => ['class' => 'bg-success col-sm-2'],
                        'contentOptions' => ['class' => 'kv-align-middle'],
                        'value'=>function($data){
                            $status = !empty($data->status) ? $data->status : "";
                            switch($status){
                                case 1 :
                                    return "APPROVED";
                                case 2 : 
                                    return "REJECT";
                                default :
                                    return "PENDING";
                            }
                        }
                    ],
                    [
                        'header' => '<i class="fa fa-cogs"></i>',
                        'headerOptions' => ['class' => 'bg-success text-center'],
                        'filterOptions' => ['class' => 'b-b b-grey'],
                        'contentOptions' => ['class' => 'no-padding'],
                        
                        'format' => 'raw',
                        'value' => function ($data) {
                            $val = '<div class="tooltip-action">
                                        <div class="trigger">
                                        '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
                                        </div>
                                        <div class="action-mask">
                                        <div class="action"> 
                                        '.Html::a(
                                            '<i class="fa fa-edit"></i>',
                                            false,
                                            [
                                                'onclick' => "update(4, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxi', 'id' => $data->TimeReportID, 'seq' => $data->Seq])."')",
                                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                            '.Html::a(
                                            '<i class="fa fa-trash"></i>',
                                            false,
                                            [
                                                'onclick' => "itemdelete(4, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxidelete', 'id' => $data->TimeReportID, 'seq' => $data->Seq])."')",
                                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                            '.Html::a(
                                            '<i class="fa fa-comment"></i>',
                                            false,
                                            [
                                                'onclick' => "Comments('".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $data->TimeReportID, 'id' => $data->Seq, 'type'=>4])."')",
                                                'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                                'style' => 'bjob-radius: 5px !important',
                                            ]
                                            ).'
                                        </div>
                                        </div>
                                    </div>';
        
                            return $val;
                        },
                    ],

                ];
                ?>
                
                <?php
                    echo GridView::widget(
                        [
                            'dataProvider' => $dataProvider_TAXI,
                            'columns' => $column,
                            'layout'=>'
                                <div class="card card-default">
                                    <div class="row ">
                                        <div class="col-md-12">
                                            {items}
                                        </div>
                                    </div>
                                </div>',
                            'resizableColumns' => false,
                            'bordered' => false,
                            'striped' => true,
                            'condensed' => false,
                            'responsive' => false,
                            'hover' => false,
                            'persistResize' => false,
                        ]
                    );
                ?>
                
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php \yii\widgets\Pjax::end(); ?>

<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlDetailAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detail']); ?>';
	paramJs.urlMealAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/meals']); ?>';
	paramJs.urlOPEAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/ope']); ?>';
	paramJs.urlTaxiAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxi']); ?>';	
    
	paramJs.urlDetailDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detaildelete']); ?>';
	paramJs.urlMealDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/mealsdelete']); ?>';
	paramJs.urlOPEDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/opedelete']); ?>';
	paramJs.urlTaxiDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxidelete']); ?>';	

    paramJs.urlComments = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments']); ?>';

	function Comments(link) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");

		$.ajax({
			url: link,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#ModalCommentsBody').html(data);
			},
		});
	}

	function CommentsSave(id) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");
        var link = (link || paramJs.urlComments);

		$.ajax({
			url: link,
			data:  {id:id},
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#ModalCommentsBody').html(data);
			},
		});
	}
	function update(type, link="") {
        if (link != "") {
            Modal(link);
        } else {
            if(type==1) {
                Modal(paramJs.urlDetailAdd);
            } else if(type==2) {
                Modal(paramJs.urlMealAdd);
            } else if(type==3) {
                Modal(paramJs.urlOPEAdd);
            } else if(type==4) {
                Modal(paramJs.urlTaxiAdd);
            }
        }
	}

	function itemdelete(type, link) {
		if(confirm("Are you sure want to delete this item permanently ?")){
			$.ajax({
				url: link,
				data: {},
				method: "POST",
				dataType: 'json',
				success: function () {
					reload();
				},
				complete: function(data) {},
				error: function(data) {}
			});
		}
	}
	function Modal(link="") {
        $('#ChildModal').modal('show');
        $('#ChildModaldetail').html("Loading ...");

		var link = (link || paramJs.urlBudgetShowModal);
		$.ajax({
			url: link,
			data:  $('#tr-form').serialize(),
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#ChildModaldetail').html(data);
			},
		});
	}

	window.closeModal = function(){
		reload();
	};
	function reload(){
		$.pjax.defaults.timeout = false;
		$.pjax.reload({
			container: '#trTimeReportDetail-pjax'
		})
		$('#ChildModal').modal('hide');
		$('#ChildModal').data('modal', null);
	}

</script>
