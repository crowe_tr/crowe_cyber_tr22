<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\grid\GridView;

use yii\widgets\ListView;
use yii\widgets\Pjax;
use dosamigos\switchery\Switchery;
use common\components\Helper;
?>
<div class="modal slide-up" id="ChildModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content no-border">
			<div class="modal-header bg-success text-white">
			</div>
			<div id="ChildModaldetail" class="modal-body padding-20">
			</div>
		</div>
	</div>
</div>
<div class="modal fade slide-right" id="ModalComments" role="dialog" aria-hidden="false">
	<div class="modal-dialog no-border">
		<div class="modal-content no-border">
			<div class="modal-header no-border bg-success">
				<p class="text-white">
					<b><i class="pg-comment fax2"></i> COMMENTS</b>
				</p>
			</div>
			<div id="ModalCommentsBody" class="modal-body no-padding">

			</div>
		</div>
	</div>
</div>

<?php \yii\widgets\Pjax::begin(['id' => 'trTimeReportDetail-pjax', 'enablePushState' => false]); ?>
<div class="padding-10">

<?php
    
    $form = ActiveForm::begin([
        'id' => 'tr-form',
        'enableClientValidation' => true,
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true,
        'fieldConfig' => [
            'template' => '{label}{input}',
            'options' => [
                'class' => 'form-group form-group-default',
            ],
		],
		'errorSummaryCssClass'=> 'alert alert-danger'
	]);

    echo $form->errorSummary($model);
    echo $form->field($model, 'ID', ['options' => ['class' => '']])->hiddenInput()->label(false);

    echo $form->field($model, 'EmployeeId', ['options' => ['class' => '', 'id'=>'EmployeeId']])->hiddenInput()->label(false);
    echo $form->field($model, 'Date', ['options' => ['class' => '']])->hiddenInput()->label(false);


    $model->EmployeeName = !empty($model->employee->fullName) ? $model->employee->fullName : "";
    $model->Level = !empty($model->employee->level->levelName) ? $model->employee->level->levelName : "";
    $isMobile = Helper::getIsMobile();

    $model->Date_label = date('l, d F Y', strtotime($model->Date)); 
?>

<div class="alert alert-danger" id="error" style="display:none">
</div>


<?php 
    if($model->Status == 1){
        $status = '<span class="badge badge-success fs-13">SUBMITTED</span>';
    }else{
        $status = '<span class="badge badge-warning fs-13 text-black">PENDING</span>';
    }
        echo '
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="bold fs-24">
                        TIME REPORT '.$status.'
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 b-b m-b-10" style="border-bottom: thin solid #ccc">
                </div>
            </div>
        ';

?>
    <div class="row">
        <div class="col-md-12">
            <div class="<?php echo ($isMobile) ? 'card card-default m-b-10' : ''; ?>">
                <div class="<?php echo ($isMobile) ? 'card-body p-b-0' : ''; ?>">
                    <div class="form-group-attached p-b-5">
                        <div class="row">
                            <div class="col-sm-5 col-md-3">
                                <?php 
                                    echo $form->field($model, 'Date_label')->textInput(['disabled'=>true]); 
                                ?>	
                            </div>
                            <div class="col-sm-7 col-md-5">
                                <?php 
                                    echo $form->field($model, 'EmployeeName')->textInput(['disabled'=>true]);
                                ?>	
                            </div>
                            <div class="col-sm-5 col-md-2">
                                <?php 
                                    echo $form->field($model, 'Level')->textInput(['disabled'=>true]);
                                ?>	
                            </div>
                            <div class="col-sm-7 col-md-2">
                                <?php 
                                    echo $form->field($model, 'isStayed',
                                            ['options' => ['class' => 'form-group form-group-default']]
                                            )->widget(
                                            Switchery::className(),
                                            [
                                                'options' => [
                                                    'disabled'=> ($model->Status==1) ? true : false,
                                                    //'onclick'=>'HeaderSave(0)',
                                                ]
                                            ]
                                            )->label(false);
                                ?>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center m-t-20">
            <div class="btn-">
                <?=
                    Html::a(
                        '<i class="fa fa-list-ul"></i> <span class="hidden-xs">TASK</span>',
                        false,
                        [
                            'onclick' => 'update(1)',
                            'class' => 'btn btn-lg btn-warning text-white',
                        ]
                    );
                ?>
                <?=
                    Html::a(
                        '<i class="fa fa-spoon"></i> <span class="hidden-xs">MEALS</span>',
                        false,
                        [
                            'onclick' => 'update(2)',
                            'class' => 'btn btn-lg btn-primary text-white',
                        ]
                    );
                ?>
                <?=
                    Html::a(
                        '<i class="fa fa-building"></i> <span class="hidden-xs">OUT OF OFFICE</span>',
                        false,
                        [
                            'onclick' => 'update(3)',
                            'class' => 'btn btn-lg btn-primary text-white',
                        ]
                    );
                ?>
                <?=
                    Html::a(
                        '<i class="fa fa-taxi"></i> <span class="hidden-xs">TAXI</span>',
                        false,
                        [
                            'onclick' => 'update(4)',
                            'class' => 'btn btn-lg btn-primary text-white',
                        ]
                    );
                ?>
            </div>
        </div>
    </div>

    <div class="card card-default m-t-10 m-b-10" style="border-top: thick solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>TASK</b></p>
            <hr/>
            <?php
                echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => 'viewDetail',
                    'layout' => '{items}',
                ]);
            ?>
        </div>
    </div>

    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>MEALS</b></p>
            <hr/>
            <?php
                echo ListView::widget([
                    'dataProvider' => $dataProvider_Meals,
                    'itemView' => 'viewMeals',
                    'layout' => '{items}',
                ]);
            ?>
        </div>
    </div>
    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>OUT OF OFFICE</b></p>
            <hr/>
            <?php
                echo ListView::widget([
                    'dataProvider' => $dataProvider_OPE,
                    'itemView' => 'viewOPE',
                    'layout' => '{items}',
                ]);
            ?>
        </div>
    </div>
    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>TAXI</b></p>
            <hr/>
            <?php
                echo ListView::widget([
                    'dataProvider' => $dataProvider_TAXI,
                    'itemView' => 'viewTaxi',
                    'layout' => '{items}',
                ]);
            ?>
        </div>
    </div>

    <div class="row m-b-10">
        <div class="col-md-12  <?php echo ($isMobile) ? 'text-center' : 'text-right'; ?>">
                    
            <?php 
                echo Html::a(
                'BACK',
                ['/tr/timereport/index'],
                [
                    'class' => 'btn btn-info text-white p-t-10 p-b-10',
                    'style' => 'bjob-radius: 5px !important',
                ]).'&nbsp;';
                if($model->Status == 0) {
                    echo '&nbsp;<a class="btn btn-info p-t-10 p-b-10" onclick="HeaderSave(0)" href="javascript:;"><i class="fa fa-edit"></i> SAVE<a/>';
                    echo '&nbsp;<a class="btn btn-warning p-t-10 p-b-10" onclick="HeaderSave(1)" href="javascript:;">SUBMIT<a/>';
                }
            ?>       
        </div>
    </div>

<?php ActiveForm::end(); ?>
</div>

<?php \yii\widgets\Pjax::end(); ?>

<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlDetailAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detail']); ?>';
	paramJs.urlMealAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/meals']); ?>';
	paramJs.urlOPEAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/ope']); ?>';
	paramJs.urlTaxiAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxi']); ?>';	
    
	paramJs.urlDetailDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detaildelete']); ?>';
	paramJs.urlMealDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/mealsdelete']); ?>';
	paramJs.urlOPEDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/opedelete']); ?>';
	paramJs.urlTaxiDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxidelete']); ?>';	

    paramJs.urlComments = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/trsupervisorapproval/comments']); ?>';

    $('#tr-form').submit(function() {
        HeaderSave(0);
    });
    paramJs.urlFormHeaderSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/updatesave', 'id'=>$model->Date]); ?>';
    paramJs.urlFormHeaderSubmitedSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/updatesave', 'id'=>$model->Date, 'submit'=>true]); ?>';
    function HeaderSave($submited){
        showFullLoading();

        if($submited == 1){
            link = paramJs.urlFormHeaderSubmitedSave
        }else{
            link = paramJs.urlFormHeaderSave
        }

        $.ajax({
            url: link,
            type: 'POST',
			data:  $('#tr-form').serialize(),
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function (data) {
                reload();
                hideFullLoading();

            },
            error: function(XMLHttpRequest, textStatus, errorThrown, data) {

                $('#error').show();
                var err = unescape(XMLHttpRequest.responseText);
                err = err.split('&#039;');
                err = err[3];
                err = escape(err);

                err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                err = unescape(err[1]);
                //alert(err);
                $('#error').html(err);
                $('html, body').animate({ scrollTop: 0 }, 0);
                $('.switchery').trigger('click');
                hideFullLoading();

            }
        });
        return false;
    }

	function Comments(link) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");

		$.ajax({
			url: link,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#ModalCommentsBody').html(data);
                hideFullLoading();

			},
		});
	}

	function CommentsSave(id) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");
        var link = (link || paramJs.urlComments);

		$.ajax({
			url: link,
			data:  {id:id},
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#ModalCommentsBody').html(data);
                hideFullLoading();

			},
		});
	}
	function update(type, link="") {
        if (link != "") {
            Modal(link);
        } else {
            if(type==1) {
                Modal(paramJs.urlDetailAdd);
            } else if(type==2) {
                Modal(paramJs.urlMealAdd);
            } else if(type==3) {
                Modal(paramJs.urlOPEAdd);
            } else if(type==4) {
                Modal(paramJs.urlTaxiAdd);
            }
        }
	}

	function itemdelete(type, link) {
        showFullLoading();

		if(confirm("Are you sure want to delete this item permanently ?")){
			$.ajax({
				url: link,
				data: {},
				method: "POST",
				dataType: 'json',
				success: function () {
					reload();
                    hideFullLoading();
				},
                error: function(XMLHttpRequest, textStatus, errorThrown, data) {

                    $('#error').show();
                    var err = unescape(XMLHttpRequest.responseText);
                    err = err.split('&#039;');
                    err = err[3];
                    err = escape(err);

                    err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                    err = unescape(err[1]);
                    //alert(err);
                    $('#error').html(err);
                    $('html, body').animate({ scrollTop: 0 }, 0);
                    hideFullLoading();

                }
			});
		}
	}
	function Modal(link="") {
        showFullLoading();
		var link = (link || paramJs.urlBudgetShowModal);
		$.ajax({
			url: link,
			data:  $('#tr-form').serialize(),
			method: "POST",
			dataType: 'html',
			success: function(data) {
                hideFullLoading();
                $('#ChildModal').modal('show');
                $('#ChildModaldetail').html("Loading ...");

				$('#ChildModaldetail').html(data);
			},
		});
	}

	window.closeModal = function(){
		reload();
        hideFullLoading();
	};
	function reload(){
		$.pjax.defaults.timeout = false;
		$.pjax.reload({
			container: '#trTimeReportDetail-pjax'
		})
		$('#ChildModal').modal('hide');
		$('#ChildModal').data('modal', null);
	}

</script>
