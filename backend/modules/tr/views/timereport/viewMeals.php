<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\grid\GridView;
use yii\web\View;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use dosamigos\switchery\Switchery;
use common\components\Helper;

    $column = [
        [
            'class' => 'kartik\grid\SerialColumn',
            'header' => 'No',
            'mergeHeader' => false,
            'headerOptions' => ['class' => 'bg-success b-r'],
            'contentOptions' => ['class' => 'text-right b-r'],
            'filterOptions' => ['class' => 'b-b b-grey b-r'],
            'width' => '36px',
        ],
        [
            'label' => 'Description',
            'attribute' => 'JobId',
            'headerOptions' => ['class' => 'col-sm-11 col-11 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
            'filterInputOptions' => ['placeholder' => ''],
            'format' => 'raw',
            'value' => function($model){
                $sql_job = "select TrJobName('".$model->TimeReportID."', '".$model->TrDetID."') as Description";
                $job = Yii::$app->db->createCommand($sql_job)->queryOne();
                return empty($job['Description']) ? "" : $job['Description'];
            }
        ],
        [
            'label' => 'Status',
            'attribute' => 'Status',
            'headerOptions' => ['class' => 'col-sm-1 col-1 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
            'filterInputOptions' => ['placeholder' => ''],
            'format' => 'raw',
            'value' => function($model){
                $sql_Status= "select TrTimeReportDetailStatus('".$model->Approval1."', '".$model->Approval2."', 1, 1, 1) as Status";
                $Status = Yii::$app->db->createCommand($sql_Status)->queryOne();
                return empty($Status['Status']) ? "" : $Status['Status'];

            }
        ],
        [
            'header' => '',
            'headerOptions' => ['class' => 'bg-success text-center'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'no-padding'],

            'format' => 'raw',
            'value' => function ($model) {
                $val = "";
                $button = "";
                if(
                    ($model->timeReport->Status == 0 or $model->Approval1 == 3 or $model->Approval2 == 3) 
                ) {
                    $button = Html::a(
                        '<i class="fa fa-edit"></i> Update',
                        false,
                        [
                            'onclick' => "update(2, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/meals', 'id' => $model->TimeReportID, 'seq' => $model->Seq])."')",
                            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                            'style' => 'bjob-radius: 5px !important',
                        ]
                    ).' '.Html::a(
                        '<i class="fa fa-trash"></i> Delete',
                        false,
                        [
                            'onclick' => "itemdelete(2, '".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/mealsdelete', 'id' => $model->TimeReportID, 'seq' => $model->Seq])."')",
                            'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                            'style' => 'bjob-radius: 5px !important',
                        ]
                    );
                    $val = '<div class="tooltip-action">
                        <div class="trigger">
                            '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
                        </div>
                        <div class="action-mask">
                            <div class="action">
                            '.$button.'
                            </div>
                        </div>
                    </div>';

                }

                return $val;
            },
        ],
    ];
    echo GridView::widget(
        [
            'dataProvider' => $dataProvider_Meals,
            //'filterModel' => $searchModel,
            'columns' => $column,
            'layout'=>'
            <div class="card card-default">
                <div class="row ">
                    <div class="col-md-12">
                        {items}
                    </div>
                </div>
            </div>
                ',
            'resizableColumns' => false,
            'bordered' => false,
            'striped' => true,
            'condensed' => false,
            'responsive' => true,
            'responsiveWrap' => false,
            'hover' => false,
            'persistResize' => false,
        ]
    );

?>
