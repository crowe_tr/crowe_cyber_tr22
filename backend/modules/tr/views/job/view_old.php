<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use common\components\CommonHelper;
use common\models\tr\JobTemp;
use mdm\admin\components\Helper;

switch ($modelJob->job_status) {
	case 0:
		$status = "DRAFT";
		// echo "DRAFT";
		break;
	case 1:
		$status = "SUBMITTED";
		break;

	case 2:
		if ($modelJob->StartDate < date("Y-m-d H:i:s") and  $modelJob->EndDate > date("Y-m-d H:i:s")) {
			$status = "ON GOING";
		} elseif ($modelJob->EndDate < date("Y-m-d H:i:s")) {
			$status = "EXPIRED";
		} else {
			$status = "APPROVED";
		}

		if ($modelJob->Flag == 1) {
			if ($modelJob->StartDate < date("Y-m-d H:i:s") and  $modelJob->EndDate > date("Y-m-d H:i:s")) {
				$status = "ON GOING (DRAFT REVISION)";
			} elseif ($modelJob->EndDate < date("Y-m-d H:i:s")) {
				$status = "EXPIRED (DRAFT REVISION)";
			} else {
				$status = "APPROVED (DRAFT REVISION)";
			}
		} elseif ($modelJob->Flag == 2) {
			if ($modelJob->StartDate < date("Y-m-d H:i:s") and  $modelJob->EndDate > date("Y-m-d H:i:s")) {
				$status = "ON GOING (SUBMITTED REVISION)";
			} elseif ($modelJob->EndDate < date("Y-m-d H:i:s")) {
				$status = "EXPIRED (SUBMITTED REVISION)";
			} else {
				$status = "APPROVED (SUBMITTED REVISION)";
			}
		}

		break;
	case 3:
		$status = "REJECTED";
		break;
	case 4:
		$status = "CLOSED";
		break;
	case 5:
		$status = "EXPIRED";
		break;
}

$user = CommonHelper::getUserIndentity();
// $jobTem = JobTemp::findOne($modelJob->JobID);

// var_dump(isset($jobTem) ? $jobTem : '');
// die();
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'JOB LIST'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', $status . $this->title = ($modelJob->isNewRecord) ? ' Create New Job' : ' Job  : ' . $modelJob->job_code), 'url' => ['view', 'id' => $modelJob->id]];
$this->title = 'VIEW JOB : ' . $modelJob->job_code;

$data = [];

if ($modelJob->job_status == 0 or $modelJob->job_status == 3 and ($user->user_id == $modelJob->manager_id or $user->is_admin == 1)) {
	if (Helper::checkRoute('/tr/jobcreate/n') && $user->user_id != $modelJob->manager_id && $user->is_admin != 1) {
		$data['button-right1'] = Html::a(Yii::t('app', '<i class="fa fa-edit"></i> UPDATE HEADER'), ['request', 'id' => $modelJob->id], ['class' => 'btn btn-info text-white active ']);;
	} else {
		$data['button-right1'] = Html::a(Yii::t('app', '<i class="fa fa-edit"></i> UPDATE'), ['request', 'id' => $modelJob->id], ['class' => 'btn btn-warning text-white active ']);;
	}
}
if ($modelJob->job_status == 0  and ($user->user_id == $modelJob->manager_id or $user->is_admin == 1 or $modelJob->created_by == $user->user_id)) {
	$data['button-right11'] =  Html::a(Yii::t('app', '<i class="fa fa-trash"></i> DELETE '), ['delete', 'id' => $modelJob->id], [
		'class' => 'btn btn-danger text-white active ',  'data-method' => 'post',
		'data-pjax' => 1,
		'data-confirm' => 'ARE YOU SURE TO DELETE THIS JOB?',
	]);;
}
if ($modelJob->job_status == 2 and ($user->user_id == $modelJob->manager_id or $user->is_admin == 1) and $modelJob->flag == 0) {
	$data['button-right2'] = Html::a(Yii::t('app', 'REVISION'), ['revisi', 'id' => $modelJob->id], ['class' => 'btn btn-info text-white active ']);
}
if ($modelJob->job_status == 2 and ($user->user_id == $modelJob->manager_id or $user->is_admin == 1) and $modelJob->flag == 1) {
	$data['button-right2'] = Html::a(Yii::t('app', 'Update Revision'), ['revisi', 'id' => $modelJob->id], ['class' => 'btn btn-warning text-white active ']);
}
// if ($modelJob->Status == 2 AND ($user->Id == $modelJob->Manager OR $user->IsAdmin == 1) AND $modelJob->Flag == 1 ) {
// 	$data['button-right3'] = Html::a(Yii::t('app', 'VIEW CHANGES'), ['viewrevisi', 'id' => $modelJob->JobID], ['class' => 'btn btn-info text-white active ']);
// }
if ($modelJob->job_status == 2 and ($user->user_id == $modelJob->manager_id or $user->is_admin == 1) and $modelJob->flag == 2) {
	$data['button-right4'] = Html::a(Yii::t('app', 'VIEW CHANGES'), ['viewrevisi', 'id' => $modelJob->id], ['class' => 'btn btn-info text-white active ']);
}

$data['button-right-back'] = Html::a(Yii::t('app', 'BACK'), ['back', 'id' => $modelJob->id], ['class' => 'btn btn-secondary text-white active ']);
$buttons = [];
foreach ($data as $key => $value) {
	array_push($buttons, $value);
}
$btn = implode('&nbsp;', $buttons);
$this->params['breadcrumbs_btn'] =
	$btn . '&nbsp;' .
	Html::a(
		'<i class="pg-comment"></i> <span class="badge badge-warning">' . $commentCount['count'] . '</span> ',
		false,
		[
			'class' => 'btn btn-success text-white',
			'onclick' => 'reload_comment();',
			'data-toggle' => 'modal',
			'data-target' => '#ModalComments',
			'title' => '' . $commentCount['count'] . 'Comments',
		]
	);;
\yii\web\YiiAsset::register($this);


$st = isset($_GET['st']) ? $_GET['st'] : false;
if ($st != false) {
	echo '<div class="alert alert-info" role="alert">
			  REVISE NO CHANGES
			</div>';
}
?>

<div class="modal fade slide-right" id="ModalComments" role="dialog" aria-hidden="false">
	<div class="modal-dialog no-border">
		<div class="modal-content no-border">
			<div class="modal-header no-border bg-success">
				<p class="text-white">
					<b><i class="pg-comment fax2"></i> COMMENTS</b>
				</p>
			</div>
			<div id="detail" class="modal-body no-padding">
				<div class=" notification-toggle" role="menu" aria-labelledby="notification-center">
					<div class="notification-panel" style="width: 100%; border: none;">
						<div class="notification-body scrollable scroll-content scroll-scrolly_visible bg-pattern">
							<div id="comments" style="height: 300px; overflow: auto; box-shadow: none; max-height: 320px; height: 320px; overflow:auto; background: url('/../../themes/pages/assets/img/pattern.jpg') repeat scroll 0% 0% !important;">

							</div>
						</div>
						<div class="notification-footer" id="ModalCommentsForm">
							<?php
							$form = ActiveForm::begin([
								'id' => 'comment-form',
								'enableClientValidation' => true,
								'validateOnSubmit' => true,
								'validateOnChange' => true,
								'validateOnType' => true,
							]);
							?>
							<?= $form->field($modelComments, 'job_id', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
							<?= $form->field($modelComments, 'comments')->label(false)->widget(\yii\redactor\widgets\Redactor::className([
								'clientOptions' => [
									'imageManagerJson' => ['/redactor/upload/image-json'],
									'imageUpload' => ['/redactor/upload/image'],
									'fileUpload' => ['/redactor/upload/file'],
									'plugins' => ['clips', 'fontcolor', 'imagemanager'],
								],
							]), ['options' => ['height' => '300px']]); ?>

							<div class="row m-t-10">
								<div class="col-sm-8">
									<a class=" btn btn-lg btn-warning btn-block text-white" onclick="addComment()" style="opacity: initial" href="javascript:;">
										<b><i class="fa fa-sends"></i> SEND COMMENT</b>
									</a>
								</div>
								<div class="col-sm-4">
									<a class=" btn btn-lg btn-info btn-block text-white" data-dismiss="modal" href="javascript:;">
										<b><i class="fa fa-sends"></i> CLOSE</b>
									</a>
								</div>
							</div>
							<?php ActiveForm::end(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card card-default m-t-5 m-b-5">
	<div class="card-body">
		<div class="item ">
			<label class="row">
				<div class="col-md-12">
					<div>
						<div class="row p-b-5">
							<div class="col-sm-5 text-left p-t-5 p-b-5">
								JOB DESCRIPTION
								<br>
								<b class="bold no-padding no-margin">
									<?= $modelJob->Description; ?>
								</b>
							</div>
							<div class="col-sm-4 text-left p-t-5 p-b-5">
								CLIENT
								<br>
								<b class="bold no-padding no-margin"><?= $modelJob->client->Name; ?></b>
							</div>

							<div class="col-sm-3 text-right p-t-5 p-b-5">
								TOTAL FEE (IDR)
								<br>
								<b class="bold no-padding no-margin"><?= ($modelJob->Fee > 0) ? (number_format($modelJob->Fee)) : "" ?> (<?= ($modelJob->IncludeOPE) ? "Include OPE" : "Exclude OPE"; ?>)</b>
							</div>
							<div class="col-sm-12">
								<hr class="no-margin">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 text-left p-t-5 p-b-5">
								PERIODS
								<br>
								<b class="bold no-padding no-margin">
									<?php
									$date = date_create($modelJob->StartDate);
									echo date_format($date, "M d, Y");
									?> -
									<?php
									$date = date_create($modelJob->EndDate);
									echo date_format($date, "M d, Y");
									?></b>
							</div>

							<div class="col-sm-4 text-left p-t-5 p-b-5">
								ENTITY / DIVISION
								<br>
								<b class="bold no-padding no-margin"><?= isset($modelJob->client->entity->entityName) ? $modelJob->client->entity->entityName : "-"; ?> / <?= isset($modelJob->client->division->divName) ? $modelJob->client->division->divName : "-"; ?></b>
							</div>
							<!-- <div class="col-sm-2 text-right p-t-5 p-b-5">
                                CLASS
                                <br>
                                <b class="bold no-padding no-margin">belum di set</b>
                            </div> -->

							<div class="col-sm-3 text-right p-t-5 p-b-5">
								JOB AREA
								<br>
								<b class="bold no-padding no-margin"> <?= isset($modelJob->JobArea) ? $modelJob->JobArea : "-"; ?></b>
							</div>
							<!-- <div class="col-sm-3 text-right p-t-5 p-b-5">
                                PPN
                                <br>
                                <b class="bold no-padding no-margin">
																</b>
                            </div> -->

						</div>
					</div>
				</div>
			</label>
		</div>
	</div>
</div>
<div class="card card-default m-t-10 m-b-5">
	<div class="card-body">
		<p><b>ALLOWANCE</b></p>
		<table class="table table-striped panel panel-default text-center">
			<thead>
				<tr>
					<td class="btn-success text-white">MEAL PROVIDED</td>
					<td class="btn-success text-white">TRANSPORT PROVIDED</td>
					<td class="btn-success text-white">Out Of Office Allowance</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-center">
						<?= ($modelJob->IsMeal == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>'; ?>
					</td>
					<td class="center">
						<?= ($modelJob->IsTaxi == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>'; ?>
					</td>
					<td class="center">
						<?= ($modelJob->IsOutOfOffice == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>'; ?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="card card-default m-t-5 m-b-5">
	<div class="card-body">
		<p><b>JOB BUDGETING</b></p>
		<?= GridView::widget([
			'id' => 'job-details',
			'dataProvider' => $dataProviderBudget,
			'emptyText' => '<div class="text-center" style="padding: 2em 0">'
				. '<i class="fa fa-exclamation-circle fa-5x text-warning"></i>'
				. '<br>'
				. Yii::t('backend', 'THERE IS NO ONE IN THIS TEAM. <br/>START REGISTER YOUR TEAM FOR THIS JOB ')
				. '</div>',
			'columns' => [
				[
					'class' => 'kartik\grid\SerialColumn',
					'headerOptions' => ['class' => 'bg-warning text-white b-r'],
					'contentOptions' => ['class' => 'text-right'],
					'width' => '36px',
					'header' => 'NO',
				],
				[
					'attribute' => 'EmployeeID',
					'headerOptions' => ['class' => 'bg-success col-sm-2'],
					'contentOptions' => ['class' => 'kv-align-middle'],
					'value' => 'employee.fullName'
				],
				[
					'attribute' => 'levelID',
					'headerOptions' => ['class' => 'bg-success col-sm-2'],
					'contentOptions' => ['class' => 'kv-align-middle'],
					'value' => 'employee.level.levelName'
				],
				[
					'attribute' => 'Total',
					'headerOptions' => ['class' => 'bg-success col-sm-1'],
					'contentOptions' => ['class' => 'kv-align-middle text-right'],
				],
				[
					'attribute' => 'Planning',
					'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
					'contentOptions' => ['class' => 'kv-align-middle text-right '],
				],
				[
					'attribute' => 'FieldWork',
					'headerOptions' => ['class' => 'bg-success col-sm-2 text-center'],
					'contentOptions' => ['class' => 'kv-align-middle text-right'],
				],
				[
					'attribute' => 'Reporting',
					'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
					'contentOptions' => ['class' => 'kv-align-middle text-right'],
				],
				[
					'attribute' => 'WrapUp',
					'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
					'contentOptions' => ['class' => 'kv-align-middle text-right'],
				],
				[
					'attribute' => 'OverTime',
					'headerOptions' => ['class' => 'bg-success col-sm-2 text-center'],
					'contentOptions' => ['class' => 'kv-align-middle text-right'],
				],

			],
			'layout' => '{items}',
			'resizableColumns' => false,
			'bordered' => false,
			'striped' => false,
			'condensed' => false,
			'responsive' => false,
			'hover' => false,
			'persistResize' => false,
		]);
		?>
	</div>
</div>
<div class="card card-default m-b-10">
	<div class="card-body">
		<p><b>PIC PROJECT</b></p>
		<hr />
		<div class="form-group-attached">
			<div class="row">
				<hr />
				<div class="col-md-4">
					Partner
					<br>
					<b class="bold no-padding no-margin">
						<?= empty($modelJob->partner->fullName) ? $modelJob->Partner : $modelJob->partner->fullName . ' - ' . $modelJob->partner->level->levelName; ?>
					</b>
				</div>
				<div class="col-md-4 ">
					MANAGER
					<br>
					<b class="bold no-padding no-margin">
						<?= empty($modelJob->manager->fullName) ? $modelJob->Manager : $modelJob->manager->fullName . ' - ' . $modelJob->manager->level->levelName; ?>

					</b>
				</div>
				<div class="col-md-4">
					SUPERVISOR
					<br>
					<b class="bold no-padding no-margin">
						<?= empty($modelJob->supervisor->fullName) ? $modelJob->Supervisor : $modelJob->supervisor->fullName . ' - ' . $modelJob->supervisor->level->levelName; ?>

					</b>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card card-default m-t-5 m-b-5">
	<div class="card-body">
		<p><b>JOB REALITATION</b></p>

		<div class="row">
			<div class="col-md-7">
				<table class="table table-striped">
					<thead>
						<tr>
							<td class="text-center btn-success text-white"></td>
							<td class="text-center btn-success text-white">ESTIMATED</td>
							<td class="text-center btn-success text-white">ACTUAL</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Time Charges</td>
							<td>
								<span class="pull-right">
									<?=
									number_format($modelJob->TimeCharges);
									?>
								</span>
							</td>
							<td>
								<span class="pull-right">
									<?= isset($actual['tr_detail_act']) ? number_format($actual['tr_detail_act']) : "0"; ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Meals Allowed</td>
							<td>
								<span class="pull-right">
									<?=
									number_format($modelJob->MealAllowance);
									?>
								</span>
							</td>
							<td>
								<span class="pull-right">
									<?= isset($actual['meal_act']) ? number_format($actual['meal_act']) : "0"; ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Taxi Used Allowed</td>
							<td>
								<span class="pull-right">
									<?=
									number_format($modelJob->TaxiAllowance);
									?>
								</span>
							</td>
							<td>
								<span class="pull-right">
									<?= isset($actual['taxi_act']) ? number_format($actual['taxi_act']) : "0"; ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Out Of Office Allowance</td>
							<td>
								<span class="pull-right">
									<?=
									number_format($modelJob->OutOfOfficeAllowance);
									?>
								</span>
							</td>
							<td>
								<span class="pull-right">
									<?= isset($actual['ope_act']) ? number_format($actual['ope_act']) : "0"; ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Administrative Charge</td>
							<td>
								<span class="pull-right">
									<?=
									number_format($modelJob->AdministrativeCharge);
									?>
								</span>
							</td>
							<td>
								<span class="pull-right">
									<?= isset($actual['adm_act']) ? number_format($actual['adm_act']) : "0"; ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Other Expense Allowance</td>
							<td>
								<span class="pull-right">
									<?=
									number_format($modelJob->OtherExpenseAllowance);
									?>
								</span>
							</td>
							<td>
								<span class="pull-right">
									<?= isset($actual['other_exp_act']) ? number_format($actual['other_exp_act']) : "0"; ?>
								</span>
							</td>
						</tr>

						<td>Recovery Rate (%)</td>
						<td>
							<span class="pull-right">
								<?= isset($modelJob['Percentage']) ? $modelJob['Percentage'] : "0"; ?>
							</span>
						</td>
						<td>
							<span class="pull-right">
								<?= isset($actual['recovery_rate_act']) ? $actual['recovery_rate_act'] : "0"; ?>
							</span>
						</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<?php
if ($modelJob->Flag == 2) {
	// code...

?>
	<!-- <div class="card card-default m-t-5 m-b-5">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12"> -->
	<?php
	// echo Html::a(Yii::t('app', 'APPROVE REVISI'), ['approverevisi', 'id' => $modelJob->JobID], ['class' => 'btn btn-success text-white active ']);
	// echo "&nbsp;";
	// echo Html::a(Yii::t('app', 'REJECT REVISI'), ['rejectrevisi', 'id' => $modelJob->JobID], ['class' => 'btn btn-danger text-white active ']);
	?>
	<!-- </div>
				</div>
			</div>
		</div> -->
<?php
}
?>

<?php
$this->registerJs(
	"
     var paramJs = (paramJs || {});
      paramJs.notifurl = '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/show_notification']) . "';
      paramJs.comments = '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/comments', 'id' => $modelJob->id]) . "';
      paramJs.commentsave = '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/commentsave']) . "';

    ",
	\yii\web\View::POS_HEAD
);

?>
<script type="text/javascript">
	function showNotification(type, msg) {
		$.ajax({
			url: paramJs.notifurl,
			data: {
				type: type,
				msg: msg
			},
			method: "POST",
			dataType: 'text',
			success: function(data) {
				$('#notification-wrapper').html(data);
				reload();
			},
			error: function(data) {
				$('#notification-wrapper').html(data);
			}
		});
	}

	$('.notification-footer').height($(window).height() / 2);

	function addComment() {
		$.ajax({
			url: paramJs.commentsave,
			type: 'POST',
			data: new FormData($('#comment-form')[0]),
			async: false,
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				reload_comment()
			},
			error: function(jqXHR, errMsg) {
				alert(errMsg);
			}
		});
		return false;
	}

	function reload() {
		$.pjax.defaults.timeout = false;
		$.pjax.reload({
			container: '#main-pjax'
		})
	}

	function reload_comment() {
		$(".redactor-editor").html("");
		$("#fnpcrreqcomments-comment").val("");
		showFullLoading();
		$.ajax({
			async: true,
			url: paramJs.comments,
			data: $('#comment-form').serialize(),
			method: "POST",
			contentType: "application/json",
			success: function(data) {
				hideFullLoading();
				$("#comments").html(data);
				// console.log()
				// element.scrollTo(element.get(0).scrollHeight);

			},
			error: function(data) {
				$("#comments").html(data);
			}
		});

	}
</script>
