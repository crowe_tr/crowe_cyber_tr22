<?php
use yii\helpers\Html;
?>
<div class="card card-default m-t-5 m-b-5">
    <div class="card-body">
        <div class="item">
            <input type="checkbox" name="req[]" class="checkbox" value="R0190019" id="req_R0190019" style="display: none">
            <label class="row">
                <div class="col-md-3 col-sm-4">
                  <div class="row p-b-5">
                    <div class="col-sm-7">
                      <?php
                      echo Html::a(
                        '<i class="fa fa-file-text"></i>&nbsp;'.$model->job_code,
                        ['view', 'id' => $model->job_id],
                        [
                          'class' => 'btn btn-warning btn-lg btn-block text-center',
                          'style' => 'font-size: 13px !important; font-weight: bold',
                        ]
                      );
                      ?>
                    </div>
                    <div class="col-sm-5">
                      <div class="col-sm-12 text-left">
                        <?php
                        switch($model->job_status)
                        {
                          case 0 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">DRAFT</span>
                          </h3>
                          <?php
                            // echo "DRAFT";
                            break;
                          case 1 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">SUBMITTED</span>
                          </h3>
                          <?php
                            break;
                          case 2 :
                          ?>
                          <h3 class="bold fs-24">

                              <span class="badge badge-default fs-10 text-black">
                                <?php
                                  if ($model->start_date < date("Y-m-d H:i:s") and  $model->end_date > date("Y-m-d H:i:s")) {
                                    ECHO "ON GOING";
                                  }
                                  elseif ($model->end_date < date("Y-m-d H:i:s")) {
                                    ECHO "EXPIRED";
                                  }
                                  else{
                                    echo "APPROVED";
                                  }
                                 ?>
                                <!-- APPROVED -->

                              </span>
                              <br>
                                <br>
                              <?php
                              if ($model->flag == 1)
                              {
                              ?>
                                  <span class="badge badge-default fs-10 text-black">  ( DRAFT REVISION )</span>
                              <?php
                              }
                              elseif ($model->flag == 2)
                              {
                              ?>
                                   <span class="badge badge-default fs-10 text-black"> ( SUBMITTED REVISION )</span>
                              <?php
                              }
                               ?>

                          </h3>
                          <?php
                            break;
                          case 3 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">REJECTED</span>
                          </h3>
                          <?php
                            break;
                          case 4 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">CLOSED</span>
                          </h3>
                          <?php
                            break;
                          case 5 :
                          ?>
                          <h3 class="bold fs-24">
                              <span class="badge badge-default fs-10 text-black">EXPIRED</span>
                          </h3>
                          <?php
                            break;
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class="row p-b-5">
                    <!-- <div class="col-sm-12"> -->

                    <!-- </div> -->
                  </div>
                </div>
                <div class="col-md-9 col-sm-8">
                    <div >
                        <div class="row p-b-5">
                            <div class="col-sm-5 text-left p-t-5 p-b-5">
                                CLIENT
                                <br>
                                <b class="bold no-padding no-margin"><?= isset($model->client_name) ? $model->client_name : ""; ?></b>
                            </div>
                            <div class="col-sm-3 p-t-5 p-b-5">
                                ENTITY / DIVISON
                                <br>
                                <b class="bold no-padding no-margin"><?= $model->entity_name ?></b> /
                                <b class="bold no-padding no-margin"><?= $model->div_name ?></b>
                            </div>

                            <div class="col-sm-4 text-right p-t-5 p-b-5">
                                TOTAL FEE (IDR)
                                <br>
                                <h3 class="bold no-padding no-margin"><?= number_format($model->job_fee); ?></h3>
                            </div>
                            <div class="col-sm-12">
                                <hr class="no-margin">
                            </div>
                        </div>
                        <div class="row p-b-5">
                            <div class="col-sm-5 text-left p-t-5 p-b-5">
                                JOB DESCRIPTION
                                <br>
                                <b class="bold no-padding no-margin">
                                <?= $model->description; ?>
                                </b>
                            </div>
                            <div class="col-sm-3 text-left p-t-5 p-b-5">
                                PIC PROJECT ( MANAGER )
                                <br>
                                <b class="bold no-padding no-margin"><?= isset($model->mc_full_name) ? $model->mc_full_name : ""; ?></b>
                            </div>
                            <div class="col-sm-4 text-right p-t-5 p-b-5">
                                <!-- AREA
                                <br>
                                <b class="bold no-padding no-margin"><?php
                                // isset($model->JobArea) ? $model->JobArea : "-"
                                 ?></b> -->
                                 PERIODS
                                 <br>
                                 <b class="bold no-padding no-margin">
                                    <?php
                                      if (isset($model->start_date)) {
                                        $date = date_create($model->start_date);
                                        $dates = date_format($date, "M d, Y");
                                        echo $dates;
                                      }
                                   ?> -
                                   <?php
                                     if (isset($model->end_date)) {
                                       $date = date_create( $model->end_date);
                                       echo date_format($date, "M d, Y");
                                     }
                                   ?>
                                 </b>
                            </div>

                        </div>
                    </div>
                </div>
          </div>
      </div>
</div>
