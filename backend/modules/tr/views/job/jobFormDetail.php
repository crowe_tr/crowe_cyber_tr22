<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\MaskedInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;


use common\components\CommonHelper;
use common\models\cm\CmClient;
use common\models\hr\Employee;

$form = ActiveForm::begin([
  'id' => 'budget-form',
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'fieldConfig' => [
      'template' => '{label}{input}',
      'options' => [
          'class' => 'form-group form-group-default',
      ],
],
'errorSummaryCssClass'=> 'alert alert-danger'
]);
// echo $form->errorSummary($modelBudget);;
// echo "<div class='error'></div>";
	if(!empty($modelBudget->errors)){
    echo "<div class='alert alert-danger'>".$form->errorSummary($modelBudget)."</div>";
		$this->params['alerts']['header'] = "Can't Save data !";
		$this->params['alerts']['body'] = $form->errorSummary($modelBudget);
    $this->params['alerts']['class'] = "alert-danger";
	}
?>
<div class="alert alert-danger" id="error" style="display:none">
</div>
      <div class="form-group-attached">
        <div class="row">
          <div class="col-md-6 col-6">
            <?= $form->field($modelBudget, 'job_id', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
              <?php
                echo $form->field($modelBudget, 'Entity', ['options' => [ 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
                  Select2::classname(),
                  [
                    // 'data' =>  $data['Entity'],
                    // 'options' => ['id' => 'Entity', 'placeholder' => 'Select ...'],
                    'options' => ['placeholder' => 'Select...'],
                    'data' => $data['Entity'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'placeholder' => Yii::t('backend', 'Select..'),
                    ],
                    'options' => ['id' => 'Entity', 'placeholder' => 'Select ...'],
                  ]);
               ?>
          </div>
          <div class="col-md-6 col-6">
              <?php
              echo $form->field($modelBudget, 'Division', ['options' => [ 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
                Select2::classname(),
                [
                  // 'data' =>  $data['Division'],
                  // 'options' => ['Id' => 'Class', 'placeholder' => 'Select ...'],
                  'options' => ['placeholder' => 'Select...'],
                  'data' => $data['Division'],
                  'pluginOptions' => [
                      'allowClear' => true,
                      'placeholder' => Yii::t('backend', 'Select..'),
                  ],
                  'options' => ['id' => 'Division', 'placeholder' => 'Select ...'],

                ]);
               ?>
          </div>
        </div>
      </div>
      <!-- <div class="form-group-attached">
        <div class="row">

        </div>
      </div> -->
      <div class="form-group-attached">
        <div class="row">
          <div class="col-md-12">
              <?php
              // echo $form->field($modelBudget, 'Grup', ['options' => ['onchange' => 'showemployee()', 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
              //   Select2::classname(),
              //   [
              //     'data' =>  $data['VempGroup'],
              //     'options' => ['Id' => 'Class', 'placeholder' => 'Select ...'],
              //
              //   ]);

              echo $form->field($modelBudget, 'Grup', ['options' => [ 'class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
                  'data' => $data['VempGroup'],
                  'options' => ['placeholder' => 'Select ...'],
                  'type' => DepDrop::TYPE_SELECT2,
                  'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                  'pluginOptions' => [
                      'allowClear' => false,
                      'depends' => ['Entity', 'Division'],
                      'url' => Url::to(['/tr/job/grupdata']),
                      'loadingText' => 'Loading ...',
                  ],
                    'options' => ['id' => 'Grup', 'placeholder' => 'Select ...'],
              ]);
               ?>
          </div>
        </div>
      </div>

			<div class="form-group-attached">
        <div class="row">
          <div class="col-md-12">
            <?php
                // echo $form->field($modelBudget, 'EmployeeID', ['options' => ['onchange' => 'showemployee()', 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
                //   Select2::classname(),
                //   [
                //     'data' =>  $data['Employee'],
                //     'options' => ['Id' => 'Class', 'placeholder' => 'Select ...'],
                //
                //   ]);

              $Employee = '';

              // echo $modelBudget->Entity;


              if (!$modelBudget->isNewrecord || !empty($modelBudget->EmployeeID)) {
                $dataEmployee = Employee::findOne($modelBudget->EmployeeID);
                $Employee = empty($dataEmployee->fullName) ? $modelBudget->EmployeeID : $dataEmployee->Id.' - '.$dataEmployee->fullName;
              }
              echo $form->field($modelBudget, 'EmployeeID', ['options' => [ 'onchange' => 'getLevelId()', 'class' => ' form-group form-group-default form-group-default-select2']])
                ->widget(Select2::classname(), [
                'initValueText' => $Employee,
                'options' => ['placeholder' => 'Search Employee ... '],
                'pluginOptions' => [
                  'tags' => false,
                  'ajax' => [
                    'url' => Url::to(['helper/srcemployeeopt']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term, entity:$("#Entity").val() , division:$("#Division").val() , grup:$("#Grup").val()}; }'),
                    'loadingText' => 'Loading ...',
                  ],
                  // 'loadingText' => 'Loading ...',
                  //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                  //'templateResult' => new JsExpression('formatSelect2ClientID'),
                  //'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
                ],
              ]);
            ?>
          </div>
        </div>
      </div>
      <div class="form-group-attached">
        <div class="row">
          <div class="col-12 TimeAvailable EA" id="EA">

          </div>
        </div>
      </div>
      <p class="m-t-10 work-time"><b>Estimated Work Time : </b></p>
      <div class="form-group-attached work-time" >
        <div class="row">
          <div class="col-md-12">
            <?= $form->field($modelBudget, 'levelName', ['options' => ['class' => '']])->hiddenInput(['id' => 'getLevel'])->label(false); ?>
            <?= $form->field($modelBudget, 'fullName', ['options' => ['class' => '']])->hiddenInput(['id' => 'getFullName'])->label(false); ?>
            <?php
              echo $form->field($modelBudget, 'Total' , ['options' => ['onchange' => 'totalWH()']])->textInput(['id' => 'totalWH']);
            ?>
          </div>
          <div class="col-md-2">
            <?php
              echo $form->field($modelBudget, 'Planning')->textInput(['id' => 'planning']);
            ?>
          </div>
          <div class="col-md-3">
            <?php
              echo $form->field($modelBudget, 'FieldWork')->textInput(['id' => 'fieldwork']);
            ?>
          </div>
          <div class="col-md-2">
            <?php
              echo $form->field($modelBudget, 'Reporting')->textInput(['id' => 'reporting']);
            ?>
          </div>
          <div class="col-md-2">
            <?php
              echo $form->field($modelBudget, 'WrapUp')->textInput(['id' => 'WrapUp']);
            ?>
          </div>
          <div class="col-md-3">
            <?=
              $form->field($modelBudget, 'OverTime', ['options' => ['onkeyup' => 'counttotal()']])->widget(MaskedInput::className(), [
                  'clientOptions' => [
                    'alias' => 'decimal',
                  ],
                  'value'=> 0
                ]);
            ?>
          </div>
        </div>
      </div>

      <div class="row m-t-20">
        <div class="col-md-12 text-right">
          <hr/>
          <button type="submit"  name="add_new_item" class="btn btn-success btn-cons p-t-10 p-b-10" style="font-size: 12px">
  				ADD
  			  </button>

          <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
        </div>
      </div>

  <?php ActiveForm::end(); ?>

  <script type="text/javascript">
    var paramJs = ( paramJs || {});
    paramJs.urlItemSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budgetsave', 'id'=>$id]); ?>';
    paramJs.urlGetLevelID = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/getlevelid']); ?>';
    paramJs.urlTotalWH = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/totalwh']); ?>';

    paramJs.formDetail = $('#budget-form');
    $('#budget-form').on('beforeSubmit', function() {

      var $form = new FormData($('#budget-form')[0]);
      $.ajax({
        url: paramJs.urlItemSave,
        type: 'POST',
        data: $form,
        async: false,
        cache: false,
        contentType: false,
        processData: false,

        success: function (data) {
          if(data != 1){
            // $('.error').html(data);

            $('#error').show();
            $('#error').html(data);
          }else{
            $('#budgetModal').modal('hide');

            reload();
          }
        },
        error: function(jqXHR, errMsg) {
          $('#error').show();
          $('#error').html("You Calculate is Wrong");
        }
      });
      return false;
    });

    function getLevelId()
    {
      var link = (link || paramJs.urlGetLevelID);
      showFullLoading();
      $.ajax({
        url: link,
        data:  $('#budget-form').serialize(),
        method: "POST",
        dataType: 'html',
        success: function(data) {
          var d = JSON.parse(data);
          	  hideFullLoading();
          // console.log(d);
          // console.log(a.fieldwork);
          $('.work-time').show();
          $('#getLevel').val(d.levelName);
          $('#getFullName').val(d.fullName);
          $('#totalWH').val(0);
          $('#planning').val(0);
          $('#fieldwork').val(0);
          $('#reporting').val(0);
          $('#WrapUp').val(0);
          $('#EA').html(d.eA);

        },
      });
    }

    function totalWH()
    {
      var link = (link || paramJs.urlTotalWH);
      $.ajax({
        url: link,
        data:  $('#budget-form').serialize(),
        method: "POST",
        dataType: 'html',
        success: function(data) {
          var d = JSON.parse(data);
          // console.log(d);
          // console.log(a.fieldwork);
          $('#planning').val(d.planning);
          $('#fieldwork').val(d.fieldwork);
          $('#WrapUp').val(d.WrapUp);
          $('#reporting').val(d.reporting);
        },
      });
    }

    // function workTime()
    // {

    // }

  </script>
