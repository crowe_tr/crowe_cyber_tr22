<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;
use common\components\JobHelper;

?>
<?php
$create_btn = !empty($this->params['grid']['create_btn']) ? $this->params['grid']['create_btn'] : '';
echo $this->render('jobSearch', ['model' => $searchModel, 'create_btn' => $create_btn]);

echo ListView::widget([
  'id' => rand() . 'listview',
  'dataProvider' => $dataProvider,
  'emptyText' => JobHelper::set_empty_text(),
  'layout' => JobHelper::set_layout('lists-job'),
  'sorter' => [
    'class' => 'common\components\ButtonDropdownSorter',
    'label' => 'Sort Items',
  ],
  'itemOptions' => ['class' => 'item'],
  'itemView' => 'jobIndexDetail',
]);
?>
