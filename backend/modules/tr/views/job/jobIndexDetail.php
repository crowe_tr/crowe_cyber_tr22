<?php

use yii\helpers\Html;
?>
<div class="card card-default m-t-5 m-b-5">
  <div class="card-body">
    <div class="item">
      <input type="checkbox" name="req[]" class="checkbox" value="R0190019" id="req_R0190019" style="display: none">
      <label class="row">
        <div class="col-lg-2 col-md-3 col-sm-4 text-center">
          <?php
          echo Html::a(
            '<i class="fa fa-file-text"></i>&nbsp;' . $model->job_code,
            ['view', 'id' => $model->job_id],
            [
              'class' => 'btn btn-warning btn-lg btn-block',
            ]
          );
          $status_job = ($model->job_status == 'CLOSED') ? 'JOB CLOSED' : $model->job_status;
          ?>
          <?= '<span class="badge badge-default mt-3">' . $status_job . '</span>'; ?>

        </div>
        <div class="col-lg-10 col-md-9 col-sm-8">
          <div>
            <div class="row p-b-5">
              <div class="col-sm-5 text-left p-t-5 p-b-5">
                CLIENT
                <br>
                <b class="bold no-padding no-margin"><?= isset($model->client_name) ? $model->client_name : ""; ?></b>
              </div>
              <div class="col-sm-3 p-t-5 p-b-5">
                ENTITY / DIVISION
                <br>
                <b class="bold no-padding no-margin"><?= $model->entity_name ?></b> /
                <b class="bold no-padding no-margin"><?= $model->div_name ?></b>
              </div>

              <div class="col-sm-4 text-right p-t-5 p-b-5">
                TOTAL FEE (IDR)
                <br>
                <h3 class="bold no-padding no-margin"><?= number_format($model->job_fee); ?></h3>
              </div>
              <div class="col-sm-12">
                <hr class="no-margin">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5 text-left p-t-5 p-b-5">
                JOB DESCRIPTION
                <br>
                <b class="bold no-padding no-margin">
                  <?= $model->job_description; ?>
                </b>
              </div>
              <div class="col-sm-3 text-left p-t-5 p-b-5">
                PIC PROJECT
                <br>
                <b class="bold no-padding no-margin"><?= isset($model->ManagerName) ? $model->ManagerName : ""; ?></b>
              </div>
              <div class="col-sm-4 text-right p-t-5 p-b-5">
                PERIODS
                <br>
                <b class="bold no-padding no-margin">
                  <?php
                  if (isset($model->StartDate)) {
                    $date = date_create($model->StartDate);
                    $dates = date_format($date, "M d, Y");
                    echo $dates;
                  }
                  ?> -
                  <?php
                  if (isset($model->EndDate)) {
                    $date = date_create($model->EndDate);
                    echo date_format($date, "M d, Y");
                  }
                  ?>
              </div>

            </div>
          </div>
        </div>
    </div>
  </div>
</div>
