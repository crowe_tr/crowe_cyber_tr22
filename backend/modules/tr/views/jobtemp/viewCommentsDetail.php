<?php
    use yii\helpers\Html;

?>

<div class="card social-card share full-width">
	<div class="card-header">
		<div class="user-pic">
			<img src="<?= !empty($model->employee->Photo) ? Yii::$app->homeUrl.Yii::$app->params['hrEmployeeBaseurl'].$model->employee->Photo : ''; ?>"" width="35" height="35">
		</div>
		<h5><?= !empty($model->employee->Name) ? $model->employee->Name : '-'; ?></h5>
		<h6>
			<?= !empty($model->employee->dept->Departement) ? $model->employee->dept->Departement : '-'; ?>
		</h6>
		<p><?= Html::decode($model->Comments); ?></p>
		<div class="via">By : <?= $model->CreatedBy; ?></div>
	</div>
</div>