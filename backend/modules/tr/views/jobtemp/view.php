<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use common\components\CommonHelper;

$this->title = 'Job : '.$modelJob->JobCode;
\yii\web\YiiAsset::register($this);
?>

<div class="modal fade slide-right" id="ModalComments" role="dialog" aria-hidden="false">
	<div class="modal-dialog no-border">
		<div class="modal-content no-border">
			<div class="modal-header no-border bg-success">
				<p class="text-white">
					<b><i class="pg-comment fax2"></i> COMMENTS</b>
				</p>
			</div>
			<div id="detail" class="modal-body no-padding">
				<div class=" notification-toggle" role="menu" aria-labelledby="notification-center">
					<div class="notification-panel" style="width: 100%; border: none;">
						<div class="notification-body scrollable scroll-content scroll-scrolly_visible bg-pattern">
							<div id="comments" style="" class="padding-20"></div>
						</div>
					    <div class="notification-footer">
                            <?php
                                $form = ActiveForm::begin([
                                    'id' => 'comment-form',
                                    'enableClientValidation' => true,
                                    'validateOnSubmit' => true,
                                    'validateOnChange' => true,
                                    'validateOnType' => true,
                                ]);
                            ?>
                            <?php
														// $form->field($modelComments, 'JobID', ['options' => ['class' => '']])->hiddenInput()->label(false);
														?>
                            <?= $form->field($modelComments, 'Comments')->label(false)->widget(\yii\redactor\widgets\Redactor::className([
                                'clientOptions' => [
                                'imageManagerJson' => ['/redactor/upload/image-json'],
                                'imageUpload' => ['/redactor/upload/image'],
                                'fileUpload' => ['/redactor/upload/file'],
                                'plugins' => ['clips', 'fontcolor', 'imagemanager'],
                                ],
                            ]), ['options' => ['height' => '300px']]); ?>

                            <div class="row m-t-10">
                                <div class="col-sm-8">
                                    <a class=" btn btn-lg btn-warning btn-block text-white" onclick="addComment()" style="opacity: initial" href="javascript:;">
                                        <b><i class="fa fa-sends"></i> SEND COMMENT</b>
                                    </a>
                                </div>
                                <div class="col-sm-4">
                                    <a class=" btn btn-lg btn-info btn-block text-white" data-dismiss="modal" href="javascript:;">
                                        <b><i class="fa fa-sends"></i> CLOSE</b>
                                    </a>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-6">
        <h3 class="bold no-padding no-margin">JOB : <?=$modelJob->JobCode; ?></h3>
    </div>
    <div class="col-md-6 text-right">
        <?php
				 $user = CommonHelper::getUserIndentity();
					if ( ($modelJob->Status == 0 OR $modelJob->Status == 3) && $user->levelID != 2) {
						echo Html::a(Yii::t('app', 'UPDATE'), ['request', 'id' => $modelJob->JobID], ['class' => 'btn btn-warning text-white active ']);
					}
				?>


				<?php
			  $user = CommonHelper::getUserIndentity();
				// print_r($modelJob->Status);
				if ($modelJob->Status == 1 && $user->levelID == 2) {
					echo Html::a(Yii::t('app', 'APPROVAL'), ['approve', 'id' => $modelJob->JobID], ['class' => 'btn btn-success text-white active ']);
					echo "&nbsp;";
					echo Html::a(Yii::t('app', 'REJECT'), ['reject', 'id' => $modelJob->JobID], ['class' => 'btn btn-danger text-white active ']);
					// code...
				}

				if ($modelJob->Status == 2) {
					echo Html::a(Yii::t('app', 'REVISI'), ['revisi', 'id' => $modelJob->JobID], ['class' => 'btn btn-info text-white active ']);
				}
				?>


        <?= Html::a(Yii::t('app', 'PRINT'), ['view', 'id' => $modelJob->JobID], ['class' => 'btn btn-primary']) ?>
        <?php
					echo Html::a(Yii::t('app', 'BACK'), ['back', 'id' => $modelJob->JobID], ['class' => 'btn btn-secondary text-white active ']);
				 // Html::a(Yii::t('app', 'BACK'), ($modelJob->Flag == 0) ? ['index'] : ['view', 'id' => $modelJob->JobID], ['class' => 'btn btn-info'])
				 ?>
        <?= Html::a('<i class="pg-comment"></i> ',
                false,
                [
                    'class' => 'btn btn-success text-white',
                    'onclick' => 'reload_comment();',
                    'data-toggle' => 'modal',
                    'data-target' => '#ModalComments',
                    'title' => 'Comments',
                ]
            ); ?>
    </div>
</div>
<div class="card card-default m-t-5 m-b-5">
    <div class="card-body">
        <div class="item " >
            <label class="row">
                <div class="col-md-12">
                    <div >
                        <div class="row p-b-5">
                            <div class="col-sm-5 text-left p-t-5 p-b-5">
                                JOB DESCRIPTION
                                <br>
                                <b class="bold no-padding no-margin">
                                <?=$modelJob->Description; ?>
                                </b>
                            </div>
                            <div class="col-sm-4 text-left p-t-5 p-b-5">
                                CLIENT
                                <br>
                                <b class="bold no-padding no-margin"><?=$modelJob->client->Name; ?></b>
                            </div>

                            <div class="col-sm-3 text-right p-t-5 p-b-5">
                                TOTAL FEE (IDR)
                                <br>
                                <b class="bold no-padding no-margin"><?= ($modelJob->Fee > 0) ? (number_format($modelJob->Fee)) : ""?> (<?= ($modelJob->IncludeOPE) ? "Include OPE" : "Exclude OPE"; ?>)</b>
                            </div>
                            <div class="col-sm-12">
                                <hr class="no-margin">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 text-left p-t-5 p-b-5">
                                PERIODE
                                <br>
                                <b class="bold no-padding no-margin"> <?=$modelJob->StartDate; ?> s.d. <?=$modelJob->EndDate; ?></b>
                            </div>

                            <div class="col-sm-4 text-left p-t-5 p-b-5">
                                ENTITY / DIVISION
                                <br>
                                <b class="bold no-padding no-margin"><?= isset($modelJob->client->entity->entityName) ? $modelJob->client->entity->entityName : "-"; ?> / <?= isset($modelJob->client->division->divName) ? $modelJob->client->division->divName : "-"; ?></b>
                            </div>
                            <!-- <div class="col-sm-2 text-right p-t-5 p-b-5">
                                CLASS
                                <br>
                                <b class="bold no-padding no-margin">belum di set</b>
                            </div> -->

                            <div class="col-sm-3 text-right p-t-5 p-b-5">
                                JOB AREA
                                <br>
                                <b class="bold no-padding no-margin"> <?= isset($modelJob->JobArea) ? $modelJob->JobArea : "-"; ?></b>
                            </div>
                            <!-- <div class="col-sm-3 text-right p-t-5 p-b-5">
                                PPN
                                <br>
                                <b class="bold no-padding no-margin">
																</b>
                            </div> -->

                        </div>
                    </div>
                </div>
            </label>
        </div>
    </div>
</div>
<div class="card card-default m-t-10 m-b-5">
    <div class="card-body">
        <p><b>ALLOWANCE</b></p>
        <table class="table table-striped panel panel-default text-center" >
            <thead>
                <tr>
                    <td class="btn-success text-white">MEAL PROVIDED</td>
                    <td class="btn-success text-white">TRANSPORT PROVIDED</td>
                    <td class="btn-success text-white">Out Of Office Allowance</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">
                        <?=($modelJob->IsMeal == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>';?>
                    </td>
                    <td class="center">
                        <?=($modelJob->IsTaxi == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>';?>
                    </td>
                    <td class="center">
                        <?=($modelJob->IsOutOfOffice == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>';?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="card card-default m-t-5 m-b-5">
    <div class="card-body">
        <p><b>JOB BUDGETING</b></p>
        <?= GridView::widget([
            'id' => 'job-details',
            'dataProvider' => $dataProviderBudget,
            'emptyText' => '<div class="text-center" style="padding: 2em 0">'
                            .'<i class="fa fa-exclamation-circle fa-5x text-warning"></i>'
                            .'<br>'
                            .Yii::t('backend', 'THERE IS NO ONE IN THIS TEAM. <br/>START REGISTER YOUR TEAM FOR THIS JOB ')
                            .'</div>',
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'headerOptions' => ['class' => 'bg-warning text-white b-r'],
                    'contentOptions' => ['class' => 'text-right'],
                    'width' => '36px',
                    'header' => 'NO',
                ],
								[
									'attribute' => 'EmployeeID',
									'headerOptions' => ['class' => 'bg-success col-sm-2'],
									'contentOptions' => ['class' => 'kv-align-middle'],
									'value'=>'employee.fullName'
								],
								[
									'attribute' => 'levelID',
									'headerOptions' => ['class' => 'bg-success col-sm-2'],
									'contentOptions' => ['class' => 'kv-align-middle'],
									'value'=>'employee.level.levelName'
								],
								[
									'attribute' => 'Total',
									'headerOptions' => ['class' => 'bg-success col-sm-1'],
									'contentOptions' => ['class' => 'kv-align-middle text-right'],
								],
								[
									'attribute' => 'Planning',
									'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
									'contentOptions' => ['class' => 'kv-align-middle text-right '],
								],
								[
									'attribute' => 'FieldWork',
									'headerOptions' => ['class' => 'bg-success col-sm-2 text-center'],
									'contentOptions' => ['class' => 'kv-align-middle text-right'],
								],
								[
									'attribute' => 'Reporting',
									'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
									'contentOptions' => ['class' => 'kv-align-middle text-right'],
								],
								[
									'attribute' => 'WrapUp',
									'headerOptions' => ['class' => 'bg-success col-sm-1 text-center'],
									'contentOptions' => ['class' => 'kv-align-middle text-right'],
								],
								[
									'attribute' => 'OverTime',
									'headerOptions' => ['class' => 'bg-success col-sm-2 text-center'],
									'contentOptions' => ['class' => 'kv-align-middle text-right'],
								],

            ],
            'layout' => '{items}',
            'resizableColumns' => false,
            'bordered' => false,
            'striped' => false,
            'condensed' => false,
            'responsive' => false,
            'hover' => false,
            'persistResize' => false,
        ]);
        ?>
     </div>
</div>
<div class="card card-default m-b-10">
		<div class="card-body">
			<p><b>PIC PROJECT</b></p>
            <hr/>
			<div class="form-group-attached">
				<div class="row">
                <hr/>
					<div class="col-md-4">
                        Partner
                        <br>
                        <b class="bold no-padding no-margin">
                        <?=isset($modelJob->partner->fullName) ? $modelJob->partner->fullName : "";?>
                        </b>
					</div>
					<div class="col-md-4 ">
                        MANAGER
                        <br>
                        <b class="bold no-padding no-margin">
                        <?=isset($modelJob->manager->fullName) ? $modelJob->manager->fullName : "";?>

                        </b>
					</div>
					<div class="col-md-4">
                        SUPERVISOR
                        <br>
                        <b class="bold no-padding no-margin">
                        <?=isset($modelJob->supervisor->fullName) ? $modelJob->supervisor->fullName : "";?>

                        </b>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="card card-default m-t-5 m-b-5">
    <div class="card-body">
        <p><b>JOB REALITATION</b></p>

        <div class="row">
            <div class="col-md-7">
                <table class="table table-striped" >
                    <thead>
                        <tr>
                            <td class="text-center btn-success text-white"></td>
                            <td class="text-center btn-success text-white">ESTIMATED</td>
                            <td class="text-center btn-success text-white">ACTUAL</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Time Charges</td>
                            <td>
															<span class="pull-right">
																<?=
																number_format($modelJob->TimeCharges);
																?>
															</span>
														</td>
                            <td>
															<span class="pull-right">
																0
															</span>
														</td>
                        </tr>
                        <tr>
                            <td>Meals Allowed</td>
                            <td>
															<span class="pull-right">
																<?=
																number_format($modelJob->MealAllowance);
																?>
															</span>
														</td>
                            <td>
															<span class="pull-right">
																0
															</span>
														</td>
                        </tr>
                        <tr>
                            <td>Taxi Used Allowed</td>
                            <td>
															<span class="pull-right">
																<?=
																number_format($modelJob->TaxiAllowance);
																?>
															</span>
														</td>
                            <td>
															<span class="pull-right">
																0
															</span>
														</td>
                        </tr>
                        <tr>
                            <td>Out Of Office Allowance</td>
                            <td>
															<span class="pull-right">
																<?=
																number_format($modelJob->OutOfOfficeAllowance);
																?>
															</span>
														</td>
                            <td>
															<span class="pull-right">
																0
															</span>
														</td>
                        </tr>
                        <tr>
                            <td>Administrative Charge</td>
                            <td>
															<span class="pull-right">
																<?=
																number_format($modelJob->AdministrativeCharge);
																?>
															</span>
														</td>
                            <td>
															<span class="pull-right">
																0
															</span>
														</td>
                        </tr>
                        <tr>
                            <td>Other Expense Allowance</td>
                            <td>
															<span class="pull-right">
																<?=
																number_format($modelJob->OtherExpenseAllowance);
																?>
															</span>
														</td>
                            <td>
															<span class="pull-right">
																0
															</span>
														</td>
                        </tr>

                            <td>Recovery Rate (%)</td>
                            <td>
															<span class="pull-right">
																<?=
																$modelJob['Percentage'];
																?>
															</span>
														</td>
                            <td>
															<span class="pull-right">
																0
															</span>
														</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs(
    "
     var paramJs = (paramJs || {});
      paramJs.notifurl = '".Yii::$app->urlManager->createAbsoluteUrl(['tr/job/show_notification'])."';
      paramJs.comments = '".Yii::$app->urlManager->createAbsoluteUrl(['tr/job/comments', 'id' => $modelJob->JobID])."';
      paramJs.commentsave = '".Yii::$app->urlManager->createAbsoluteUrl(['tr/job/commentsave'])."';

    ",  \yii\web\View::POS_HEAD
);

?>
<script type="text/javascript">
    function showNotification(type, msg){
    $.ajax({
        url: paramJs.notifurl,
        data: {type:type, msg:msg},
        method: "POST",
        dataType: 'text',
        success: function(data) {
        $('#notification-wrapper').html(data);
        reload();
        },
        error: function(data) {
        $('#notification-wrapper').html(data);
        }
    });
    }

    $('.notification-footer').height($(window).height() / 2);

    function addComment() {
        $.ajax({
            url: paramJs.commentsave,
            type: 'POST',
            data: new FormData($('#comment-form')[0]),
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                reload_comment()
            },
            error: function(jqXHR, errMsg) {
                alert(errMsg);
            }
        });
        return false;
    }
    function reload() {
    $.pjax.defaults.timeout = false;
        $.pjax.reload({
            container: '#main-pjax'
        })
    }
    function reload_comment(){
        $(".redactor-editor").html("");
        $("#fnpcrreqcomments-comment").val("");
        $.ajax({
            async: true,
            url: paramJs.comments,
            //data: $('#comment-form').serialize(),
            method: "POST",
            contentType: "application/json",
            success: function(data) {
            $("#comments").html(data);
            },
            error: function(data) {
            $("#comments").html(data);
            }
        });

    }


</script>
