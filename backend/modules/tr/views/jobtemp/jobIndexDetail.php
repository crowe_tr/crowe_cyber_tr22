<?php
use yii\helpers\Html;
?>
<div class="card card-default m-t-5 m-b-5">
    <div class="card-body">
        <div class="item">
            <input type="checkbox" name="req[]" class="checkbox" value="R0190019" id="req_R0190019" style="display: none">
            <label class="row">
                <div class="col-md-2 col-sm-3">

                    <?php
                        echo Html::a(
                            '<i class="fa fa-file-text"></i>&nbsp;'.$model->JobCode,
                            ['request', 'id' => $model->JobID],
                            [
                                'class' => 'btn btn-warning btn-lg btn-block text-center',
                                'style' => 'font-size: 13px !important; font-weight: bold',
                            ]
                        );
                    ?>
                </div>
                <div class="col-md-10 col-sm-9">
                    <div >
                        <div class="row p-b-5">
                            <div class="col-sm-6 text-left p-t-5 p-b-5">
                                JOB DESCRIPTION
                                <br>
                                <b class="bold no-padding no-margin">
                                <?= $model->Description; ?>
                                </b>
                            </div>
                            <div class="col-sm-3 p-t-5 p-b-5">
                                CLASS / DIVISION
                                <br>
                                <b class="bold no-padding no-margin"><?= $model->client->entity->entityName ?></b>
                            </div>

                            <div class="col-sm-3 text-right p-t-5 p-b-5">
                                TOTAL FEE (IDR)
                                <br>
                                <h3 class="bold no-padding no-margin"><?= number_format($model->Fee); ?></h3>
                            </div>
                            <div class="col-sm-12">
                                <hr class="no-margin">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 text-left p-t-5 p-b-5">
                                CLIENT
                                <br>
                                <b class="bold no-padding no-margin"><?= isset($model->client->Name) ? $model->client->Name : ""; ?></b>
                            </div>
                            <div class="col-sm-3 text-left p-t-5 p-b-5">
                                PIC PROJECT
                                <br>
                                <b class="bold no-padding no-margin"><?= isset($model->Partner->fullName) ? $model->Partner->fullName : ""; ?></b>
                            </div>
                            <div class="col-sm-3 text-right p-t-5 p-b-5">
                                AREA
                                <br>
                                <b class="bold no-padding no-margin"><?= isset($model->client->ClientAreas) ? $model->client->ClientAreas : "-" ?></b>
                            </div>

                        </div>
                    </div>
                </div>
          </div>
      </div>
</div>
