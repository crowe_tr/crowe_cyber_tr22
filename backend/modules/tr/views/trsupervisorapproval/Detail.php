<?php
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\helpers\Url;
    use common\models\cm\CmDept;
    use common\models\cm\CmCompanyBranch;
    use yii\web\View;
    use kartik\widgets\ActiveForm;
	use kartik\widgets\Select2;
	$session = Yii::$app->session;
?>
<div class="modal slide-up" id="ChildModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content no-border">
			<div class="modal-header bg-success text-white">
			</div>
			<div id="ChildModaldetail" class="modal-body padding-20">
			</div>
		</div>
	</div>
</div>

<div class="modal fade slide-right" id="ModalComments" role="dialog" aria-hidden="false">
	<div class="modal-dialog no-border">
		<div class="modal-content no-border">
			<div class="modal-header no-border bg-success">
				<p class="text-white">
					<b><i class="pg-comment fax2"></i> COMMENTS</b>
				</p>
			</div>
			<div id="ModalCommentsBody" class="modal-body no-padding">

			</div>
		</div>
	</div>
</div>
<?php \yii\widgets\Pjax::begin(['id' => 'trTimeReportDetail-pjax', 'enablePushState' => true]); ?>
<div class="row m-b-10">
	<div class="col-sm-8">
		<h3 class="no-margin no-padding bold header-tra">TIMEREPORT / APPROVAL-1 LIST</h3>
		<h3 class="no-margin no-padding fs-15 header-tra" style="line-height: 1em"><?=date('l, d F Y', strtotime($param['Date'])); ;?><h3>
	</div>
	<div class="col-sm-4 mt-10-cs text-right">
		<?php
			echo Html::a(
				'BACK',
				['all'],
				[
					'class' => 'btn btn-info text-white',
					'style' => 'bjob-radius: 5px !important',
				]
			);
		?>
	</div>
</div>

<div class="row">
    <div class="col-md-12 table-responsive" >
		<table class="table table-bordered">
			<thead>
				<tr>
					<td class="bg-warning text-black text-center bold align-middle" rowspan="2" width="40%">TASK DESCRIPTION</td>
					<td class="bg-primary text-center align-middle" rowspan="2">WH</td>
					<td class="bg-primary text-center align-middle" rowspan="2">OT</td>
					<td class="bg-primary text-center align-middle" colspan="3">ALLOWANCE</td>
					<td class="bg-primary text-center align-middle" rowspan="2">STATUS</td>
					<td class="bg-info" rowspan="2" ></td>
				</tr>
				<tr>
					<td class="bg-primary text-center">MEALS</td>
					<td class="bg-primary text-center">OUT OF OFFICE</td>
					<td class="bg-primary text-center">TAXI</td>
				</tr>
			</thead>
			<tbody>
			<?php
				if(!empty($data['list'])){
					$i = 0;
					foreach($data['list'] as $list){
						if(!isset($session['_supervisor_'.$list['Date'].$list['Description']])){
							$rand = rand();
							$session->set('_supervisor_'.$list['Date'].$list['Description'], $rand);
						}else{
							$rand = $session['_supervisor_'.$list['Date'].$list['Description']];
						}
						$i++;
						$btn_approve_all = "";
						if($list['Status'] == "APPROVAL1-PENDING" or $list['Status'] == "APPROVAL1-ONPROGRESS" ){
							$btn_approve_all = Html::a(
								'APPROVE ALL',
								false,
								[
									'onclick' => "approvalall('".Yii::$app->urlManager->createAbsoluteUrl(
										['tr/trsupervisorapproval/detailapprovalall'
										, 'id' => $list['Date']
										, 'Description' => $list['Description']
									])

									."')",
									'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10 pull-right m-t-10',
								]);
						}
						echo "
							<tr>
								<td class='align-middle'>".$list['Description']."</td>
								<td class='text-right align-middle'>".$list['WorkHour']."</td>
								<td class='text-right align-middle'>".$list['Overtime']."</td>
								<td class='text-right align-middle'>Rp. ".number_format($list['MealsAmount'], 2)."</td>
								<td class='text-right align-middle'>Rp. ".number_format($list['OutOfOfficeAmount'], 2)."</td>
								<td class='text-right align-middle'>Rp. ".number_format($list['TaxiAmount'], 2)."</td>
								<td class='text-right align-middle'><span class='badge ".$list['StatusCSS']."'>".$list['StatusLabel']."</span></td>
								<td class='text-center'>"
								. Html::a(
									'<i class="pg-arrow_minimize"></i> Show',
									false,
									[
										'id'=>'_btn_show'.$rand,
										'class' => 'btn btn-warning text-white',
										'style' => 'border-radius: 5px !important',
										'onclick'=>"
											$('#_btn_hide".$rand."').show();
											$('#_btn_show".$rand."').hide();
											$('#_table_".$rand."').toggle();

											saveSession('".$rand."');
										"
									])
								. Html::a(
									'<i class="pg-arrow_maximize"></i> Hide',
									false,
									[
										'id'=>'_btn_hide'.$rand,
										'class' => 'btn btn-warning text-white',
										'style' => 'border-radius: 5px !important; display:none',
										'onclick'=>"
											$('#_btn_hide".$rand."').hide();
											$('#_btn_show".$rand."').show();
											$('#_table_".$rand."').toggle();
											destroySession('".$rand."');
										"

									])
								. "</td>
							</tr>
						";
						echo "<tr id='_table_".$rand."' style='display:none;'><td colspan='9' style='background: #fafafa;padding: 30px !important;'>";
						echo "<div class='table-responsive '>";
						echo "<table class='table '>";
						echo '
						<thead>
							<tr>
								<td class="bg-info text-white text-center">NO .</td>
								<td class="bg-info text-white text-center">EMPLOYEE NAME</td>
								<td class="bg-info text-white text-center">TASK</td>
								<td class="bg-info text-white text-center">POSITION</td>
								<td class="bg-info text-white text-center">WORKHOUR</td>
								<td class="bg-info text-white text-center">OVERTIME</td>
								<td class="bg-info text-white text-center">MEALS</td>
								<td class="bg-info text-white text-center">OUT OF OFFICE</td>
								<td class="bg-info text-white text-center">TAXI</td>
								<td class="bg-info text-white text-center">STATUS</td>
								<td class="bg-info"></td>
							</tr>
						</thead>
						';
						echo "<tbody>";
						$list['JobID'] = empty($list['JobID']) ? 0 : $list['JobID'];
						if(!empty($list['Supervisor']) && !empty($list['Date'])){
							/* fix bugs untuk timereport approval1 jika non project @ 06022020 19:18*/
							$sql_detail = "call antTrListApproval1DetailList('".$list['Supervisor']."', '".$list['JobID']."','".$list['Date']."','".$list['Description']."')";
							/* fix bugs untuk timereport approval1 jika non project @ 06022020 19:18*/
							$detail['detail'] = Yii::$app->db->createCommand($sql_detail)->queryAll();
							$i = 0;
							if(!empty($detail)){
								foreach($detail['detail'] as $detail){
									$i ++;
									echo $this->render('DetailList', [
										'i' => $i,
										'detail' => $detail,
										'param' => $param
									]);
								}
							}
						}else{
							echo "<tr><td colspan='10'>There is no approval request for this item</td></tr>";
						}
						echo "</tbody>";
						echo "</table>";
						echo "</div>";
						echo $btn_approve_all;
						echo "</td></tr>";
					}
				}
			?>
			</tbody>
		</table>
    </div>
</div>
<script type="text/javascript">
	var allay = (JSON.parse(localStorage.getItem('_supervisor_allay')) || []);
	if(allay.length > 0){
		for (var i = 0; i < allay.length; i++) {
			if (($('#_table_'+allay[i]).length > 0)){
				$('#_btn_hide'+allay[i]).show();
				$('#_btn_show'+allay[i]).hide();
				$('#_table_'+allay[i]).show();

				if(i == (allay.length - 1)){
					$('html, body').animate({
						scrollTop: $('#_table_'+allay[i]).offset().top
					});
				}
			}else{
				allay.pop(i);
				localStorage.setItem('_manager_allay', JSON.stringify(allay));
			}
		}
	}
</script>
<?php \yii\widgets\Pjax::end(); ?>

<?php

    $this->registerjs("
		$(document).ready(function(){
			$('#select-all').click(function(){
				$('.checkbox').prop('checked',true);
				$('#select-all').hide();
				$('#unselect-all').show();
			});
			$('#unselect-all').click(function(){
				$('.checkbox').prop('checked',false);
				$('#select-all').show();
				$('#unselect-all').hide();
			});
		});
	", View::POS_END, 'sorted_table');

 ?>


 <script type="text/javascript">
	var paramJs = (paramJs || {});
	
	function saveSession(val){
		var allay = (JSON.parse(localStorage.getItem('_supervisor_allay')) || []);
		allay.push(val);
		localStorage.setItem('_supervisor_allay', JSON.stringify(allay));

	}
	function destroySession(val){
		var allay = (JSON.parse(localStorage.getItem('_supervisor_allay')) || []);
		if(allay.length > 0){
			for (var i = 0; i < allay.length; i++) {
				if(allay[i]==val) {
					allay.pop(i);
				}
			}
		}

		localStorage.setItem('_supervisor_allay', JSON.stringify(allay));
	}

	paramJs.urlDetailsApprove = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/trsupervisorapproval/details_approve']); ?>';
	paramJs.urlDetailsReject = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/trsupervisorapproval/details_reject']); ?>';
	function approvalall(link) {
		showFullLoading();

		$.ajax({
			url: link,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
                reload();
                hideFullLoading();
			},
			error: function(data) {
                reload();
                hideFullLoading();
			},
		});
	}

	function approve() {
		$.ajax({
			url: paramJs.urlDetailsApprove,
			data:  $('#tr-approvals').serialize(),
			method: "POST",
			dataType: 'html',
			success: function(data) {
				reload();
			},
		});
	}

	function reject() {
		$.ajax({
			url: paramJs.urlDetailsReject,
			data:  $('#tr-approvals').serialize(),
			method: "POST",
			dataType: 'html',
			success: function(data) {
				reload();
			},
		});
	}

	function approvedetail(link) {
        $('#ChildModal').modal('show');
        $('#ChildModaldetail').html("Loading ...");

		$.ajax({
			url: link,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#ChildModaldetail').html(data);
			},
		});
	}

	function Comments(link) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");

		$.ajax({
			url: link,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#ModalCommentsBody').html(data);
			},
		});
	}


	function CommentsSave(id) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");
        var link = (link || paramJs.urlComments);

		$.ajax({
			url: link,
			data:  {id:id},
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#ModalCommentsBody').html(data);
			},
		});
	}

	window.closeModal = function(){
		reload();
	};
	function reload(){
		$.pjax.defaults.timeout = false;
		$.pjax.reload({
			container: '#trTimeReportDetail-pjax'
		})
		$('#ChildModal').modal('hide');
		$('#ChildModal').data('modal', null);
	}

</script>
