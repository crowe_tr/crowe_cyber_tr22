<?php
    use yii\helpers\Html;
    if(!empty($data['Detail'])){
        echo "<p><b>TASK</b></p>";
        foreach($data['Detail'] as $task){
            $button = "";
            if(!empty($task['attachment'])) {
                $button .="<tr>"
                . "<td>Attachment</td>"
                . "<td>:</td>"
                . "<td>"
                . Html::a('Download file', Yii::getAlias('@public_baseurl/timereport_detail/') . $task['attachment'], ['class'=>'btn btn-info padding-5 p-l-10 p-r-10', 'download' => 'true'])
                . "</td>"
                . "</tr>";
            }
            if($task['Status'] == "APPROVAL2-PENDING"){
                $button .= "<tr>
                    <td colspan='3' class='text-right'>";
                $button .= " ".Html::a(
                    '<i class="fa fa-check-square"></i> Approve',
                    false,
                    [
                        'onclick' => "approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/detailapproval', 'id' => $task['TimeReportID'] , 'TrDetID' => $task['TrDetID'], 'set'=>1])."')",
                        'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                    ]);
                $button .= " ".Html::a(
                    '<i class="fa fa-edit"></i> Revise',
                    false,
                    [
                        'onclick' => "
                            if(confirm('Are you sure want to reject this item ?')) {
                                approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/detailapproval', 'id' => $task['TimeReportID'] , 'TrDetID' => $task['TrDetID'], 'set'=>3])."')                            
                            }else{
                                return false;
                            }
                        ",
                        'class' => 'btn btn-warning text-white padding-5 p-l-10 p-r-10',
                    ]);
                $button .= "</tr>";
            }
            echo "
            <div class='m-t-5 m-b-0'>
                <div class=''>
                    <table class='table table-bordered'>
                        <tr>
                            <td width='100px'>WorkHour</td>
                            <td width='10px'>:</td>
                            <td>{$task['WorkHour']} Hours</td>
                        </tr>
                        <tr>
                            <td width='100px'>Overtime</td>
                            <td width='10px'>:</td>
                            <td>{$task['Overtime']} Hours</td>
                        </tr>
                        <tr>
                            <td width='100px'>Description</td>
                            <td width='10px'>:</td>
                            <td>{$task['Description']}</td>
                        </tr>
                        <tr>
                            <td width='100px'>Status</td>
                            <td width='10px'>:</td>
                            <td><span class='badge {$task['StatusLabel']}'>{$task['StatusLabel']}</span></td>
                        </tr>
                        ".$button."
                    </table>
                </div>
            </div>
            ";
        }
    }
    if(!empty($data['Meal'])){
        echo "<br/><p><b>MEALS</b></p>";
        foreach($data['Meal'] as $meal){
            $button = "";
            if($meal['Status'] == "APPROVAL2-PENDING" && $task['Status'] == "COMPLETED"){
                $button .= "<tr>
                <td colspan='3' class='text-right'>";

                $button .=  Html::a(
                        '<i class="fa fa-check-square"></i> Approve',
                        false,
                        [
                            'onclick' => "approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/mealapproval', 'id' => $meal['TimeReportID'] , 'TrDetID' => $meal['TrDetID'], 'Seq' => $meal['Seq'], 'set'=>1])."')",
                            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                            'style' => 'bjob-radius: 5px !important',
                        ])
                        ." ".
                        Html::a(
                            '<i class="fa fa-close"></i> Reject',
                            false,
                            [
                                'onclick' => "
                                    if(confirm('Are you sure want to reject this item ?')) {
                                        approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/mealapproval', 'id' => $meal['TimeReportID'] , 'TrDetID' => $meal['TrDetID'], 'Seq' => $meal['Seq'], 'set'=>2 ])."')
                                    }else{
                                        return false;
                                    }
                                ",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        )
                        ." ".
                        Html::a(
                            '<i class="fa fa-edit"></i> Revise',
                            false,
                            [
                                'onclick' => "
                                if(confirm('Are you sure want to denie this item and ask user to revise ?')) {
                                    approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/mealapproval', 'id' => $meal['TimeReportID'] , 'TrDetID' => $meal['TrDetID'], 'Seq' => $meal['Seq'], 'set'=>3 ])."')                   
                                }else{
                                    return false;
                                }
                                ",
                                'class' => 'btn btn-warning text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                $button .= "</tr>";
            }
            echo "
            <div class='m-t-5 m-b-0'>
                <div class=''>
                    <table class='table table-bordered'>
                        <tr>
                            <td width='100px'>Description</td>
                            <td width='10px'>:</td>
                            <td>{$meal['Description']}</td>
                        </tr>
                        <tr>
                            <td width='100px'>Amount</td>
                            <td width='10px'>:</td>
                            <td>Rp. ".number_format($meal['MealsValue'], 2)."</td>
                        </tr>
                        <tr>
                            <td width='100px'>Status</td>
                            <td width='10px'>:</td>
                            <td><span class='badge {$meal['StatusLabel']}'>{$meal['StatusLabel']}</span></td>
                        </tr>

                        ".$button."

                    </table>
                </div>
            </div>
            ";
        }
    }
    if(!empty($data['OutOffice'])){
        echo "<br/><p><b>OUT OF OFFICE</b></p>";
        foreach($data['OutOffice'] as $outofoffice) {
            $button = "";
            if($outofoffice['Status'] == "APPROVAL2-PENDING" && $task['Status'] == "COMPLETED"){
                    $button .= "<tr>
                    <td colspan='3' class='text-right'>";
                    $button .=  Html::a(
                        '<i class="fa fa-check-square"></i> Approve',
                        false,
                        [
                            
                            'onclick' => "approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/outofficeapproval', 'id' => $outofoffice['TimeReportID'], 'TrDetID' => $outofoffice['TrDetID'], 'Seq' => $outofoffice['Seq'], 'set'=>1])."')",
                            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                            'style' => 'bjob-radius: 5px !important',
                        ])
                        ." ".
                        Html::a(
                            '<i class="fa fa-close"></i> Reject',
                            false,
                            [
                                'onclick' => "
                                    if(confirm('Are you sure want to reject this item ?')) {
                                        approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/outofficeapproval', 'id' => $outofoffice['TimeReportID'], 'TrDetID' => $outofoffice['TrDetID'], 'Seq' => $outofoffice['Seq'], 'set'=>2 ])."')                                    }else{
                                        return false;
                                    }
                                ",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        )
                        ." ".
                        Html::a(
                            '<i class="fa fa-edit"></i> Revise',
                            false,
                            [
                                'onclick' => "
                                if(confirm('Are you sure want to denie this item and ask user to revise ?')) {
                                    approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/outofficeapproval', 'id' => $outofoffice['TimeReportID'] , 'TrDetID' => $outofoffice['TrDetID'], 'Seq' => $outofoffice['Seq'], 'set'=>3 ])."')                   
                                }else{
                                    return false;
                                }
                                ",
                                'class' => 'btn btn-warning text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                $button .= "</tr>";
            }
            echo "
            <div class='m-t-5 m-b-0'>
                <div class=''>
                    <table class='table table-bordered'>
                        <tr>
                            <td width='100px'>Description</td>
                            <td width='10px'>:</td>
                            <td>{$outofoffice['Description']}</td>
                        </tr>
                        <tr>
                            <td width='100px'>Zone</td>
                            <td width='10px'>:</td>
                            <td>{$outofoffice['ZoneID']} </td>
                        </tr>

                        <tr>
                            <td width='100px'>Amount</td>
                            <td width='10px'>:</td>
                            <td>Rp. ".number_format($outofoffice['OutOfOfficeValue'], 2)."</td>
                        </tr>
                        <tr>
                            <td width='100px'>Status</td>
                            <td width='10px'>:</td>
                            <td><span class='badge {$outofoffice['StatusLabel']}'>{$outofoffice['StatusLabel']}</span></td>
                        </tr>

                        ".$button."

                    </table>
                </div>
            </div>

            ";
        }
    }
    if(!empty($data['Taxi'])){


        echo "<br/><p><b>TAXI</b></p>";
        foreach($data['Taxi'] as $taxi){
            $button = "";

            if($taxi['Status'] == "APPROVAL2-PENDING" && $task['Status'] == "COMPLETED"){
                $button .= "<tr>
                <td colspan='3' class='text-right'>";

                $button .=  Html::a(
                        '<i class="fa fa-check-square"></i> Approve',
                        false,
                        [
                            'onclick' => "approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/taxiapproval', 'id' => $taxi['TimeReportID'] , 'TrDetID' => $taxi['TrDetID'], 'Seq' => $taxi['Seq'], 'set'=>1])."')",
                            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                            'style' => 'bjob-radius: 5px !important',
                        ])
                        ." ".
                        Html::a(
                            '<i class="fa fa-close"></i> Reject',
                            false,
                            [
                                'onclick' => "
                                    if(confirm('Are you sure want to reject this item ?')) {
                                        approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/taxiapproval', 'id' => $taxi['TimeReportID'] , 'TrDetID' => $taxi['TrDetID'], 'Seq' => $taxi['Seq'],  'set'=>2 ])."')                   
                                    }else{
                                        return false;
                                    }
                                ",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        )
                        ." ".
                        Html::a(
                            '<i class="fa fa-edit"></i> Revise',
                            false,
                            [
                                'onclick' => "
                                    if(confirm('Are you sure want to denie this item and ask user to revise ?')) {
                                        approval('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/taxiapproval', 'id' => $taxi['TimeReportID'] , 'TrDetID' => $taxi['TrDetID'], 'Seq' => $taxi['Seq'], 'set'=>3 ])."')                   
                                    }else{
                                        return false;
                                    }
                                ",
                                'class' => 'btn btn-warning text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                $button .= "</tr>";
            }

            echo "
            <div class='m-t-5 m-b-0'>
            <div class=''>
                <table class='table table-bordered'>
                    <tr>
                        <td width='100px'>Type</td>
                        <td width='10px'>:</td>
                        <td>{$taxi['TaxiName']}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>:</td>
                        <td>Rp. ".number_format($taxi['Amount'], 0)."</td>
                    </tr>
                    <tr>
                        <td>Destination</td>
                        <td>:</td>
                        <td>{$taxi['Destination']} </td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>:</td>
                        <td>{$taxi['Description']} </td>
                    </tr>
                    <tr>
                        <td width='100px'>Status</td>
                        <td width='10px'>:</td>
                        <td><span class='badge {$taxi['StatusLabel']}'>{$taxi['StatusLabel']}</span></td>
                    </tr>

                    ".$button."

                </table>
            </div>
        </div>            ";
        }
    }

?>
<div class="row">
    <div class="col-md-12 text-right">
        <hr class="m-b-5"/>
        <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" onclick="CloseModal()">BACK</button>
    </div>
</div>

<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.showModal = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/view', 'id'=>$param['TrID'], 'TrDetID'=>$param['TrDetID']]); ?>';

	function approval(link) {
        showFullLoading();

		$.ajax({
			url: link,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
                hideFullLoading();
                showModal();
			},
			error: function(data) {
                hideFullLoading();
			},
		});
	}
    function showModal() {
        showFullLoading();

        $('#ChildModal').modal('show');
        $('#ChildModaldetail').html("Loading ...");

		$.ajax({
			url: paramJs.showModal,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
                hideFullLoading();
				$('#ChildModaldetail').html(data);
			},
			error: function(data) {
                hideFullLoading();
			},
		});
	}

    function CloseModal(){
        $('#ChildModal').modal('hide');
        reload();
    }

	window.closeModal = function(){
		reload();
	};
	function reload(){
		$.pjax.defaults.timeout = false;
		$.pjax.reload({
			container: '#trTimeReportDetail-pjax'
		})
		$('#ChildModal').modal('hide');
		$('#ChildModal').data('modal', null);
	}

</script>
