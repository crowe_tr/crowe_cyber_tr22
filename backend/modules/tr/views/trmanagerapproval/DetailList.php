<?php 

    use yii\helpers\Html;
    use yii\widgets\DetailView;
    $button = ""; 
    if($detail['Status'] == 'APPROVAL2-PENDING' ){ 
        $button = Html::a(
            '<i class="fa fa-check-square"></i> Approve',
            false,
            [
                'onclick' => "approvalall('".Yii::$app->urlManager->createAbsoluteUrl(
                    ['tr/trmanagerapproval/detaillistapproveall'
                    , 'id' => $detail['Date'] 
                    , 'Description' => $detail['Description'] 
                    , 'TimeReportID' => $detail['TimeReportID'] 
                    , 'TrDetID' => $detail['TrDetID']
                ])
                
                ."')",
                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
            ]);
    }

    
    $button .= Html::a(
        '<i class="fa fa-comment"></i>',
        false,
        [
            'onclick' => "Comments('".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $detail['TimeReportID'], 'id' => $detail['TrDetID'], 'type'=>1])."')",
            'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
        ]);
    $button .= Html::a(
        '<i class="fa fa-edit"></i> ',
        false,
        [
            'onclick' => "approvedetail('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/view', 'id' => $detail['TimeReportID'] , 'TrDetID' => $detail['TrDetID'] ])."')",
            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
        ]
    );
    echo "
    <tr>
        <td class='text-right align-middle'>".$i.".</td>
        <td class='align-middle'>".$detail['EmployeeName']."</td>
        <td class='align-middle'>".$detail['Task']."</td>
        <td class='align-middle'>".$detail['Position']."</td>
        <td class='text-right align-middle'>".$detail['WorkHour']."</td>
        <td class='text-right align-middle'>".$detail['Overtime']."</td>
        <td class='text-right align-middle'>Rp. ".number_format($detail['MealsAmount'], 2)."</td>
        <td class='text-right align-middle'>Rp. ".number_format($detail['OutOfOfficeAmount'], 2)."</td>
        <td class='text-right align-middle'>Rp. ".number_format($detail['TaxiAmount'], 2)."</td>
        <td class='text-right align-middle'><span class='badge ".$detail['StatusCSS']."'>".$detail['StatusLabel']."</span></td>
        <td class='text-right align-middle'>"
        . "<div class='tooltip-action'>
            <div class='trigger'>
                ".Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!'])."
            </div>
            <div class='action-mask'>
                <div class='action'>
                    '.$button.'
                </div>
            </div>
        </div>"
        ."</td>
    </tr>
    ";
?>