<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use common\models\cm\CmDept;
use common\models\cm\CmCompanyBranch;
use yii\web\View;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
?>
<div class="modal fade stick-up " id="TrModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content no-border">
            <div class="modal-header bg-success text-white">
            </div>
            <div id="detail" class="modal-body padding-20">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-10">
                <h3 class="no-margin no-padding bold">TIMEREPORT / APPROVAL-2</h3>
            </div>
            <div class="col-sm-2">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'tr-form',
                    'enableClientValidation' => true,
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                    'fieldConfig' => [
                        'template' => '{input}',
                        'options' => [
                            'class' => '',

                        ],
                    ],
                    'errorSummaryCssClass' => 'alert alert-danger'
                ]);
                ?>
                <?php
                $_month = [];
                $now = strtotime(date("Y-m-01"));
                for ($i = 0; $i <= 6; $i++) {
                    $_month[date('Y-m-t', strtotime("-{$i} months", $now))] = strtoupper(date('F Y', strtotime("-{$i} months", $now)));
                }
                $model->YearDate = !empty($param['YearDate']) ? $param['YearDate'] : date('Y-m-t');
                echo $form->field($model, 'YearDate', ['options' => ['class' => 'select-withoutlabel']])->widget(
                    Select2::classname(),
                    [
                        'data' =>  $_month,
                        'options' => ['id' => 'YearDate', 'placeholder' => 'Select ...'],
                    ]
                )->label(false);
                ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div id="tr-calendar-approval" class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td class="bg-info text-center align-middle" rowspan="2"></td>
                        <td class="bg-primary text-center align-middle" rowspan="2">TOTAL WORKHOUR</td>
                        <td class="bg-primary text-center align-middle" rowspan="2">TOTAL OVERTIME</td>
                        <td class="bg-primary text-center align-middle" colspan="3">TOTAL ALLOWANCE</td>
                        <td class="bg-primary text-center align-middle" rowspan="2">STATUS</td>
                        <td class="bg-info" rowspan="2"></td>
                    </tr>
                    <tr>
                        <td class="bg-primary text-center">MEALS</td>
                        <td class="bg-primary text-center">OUT OF OFFICE</td>
                        <td class="bg-primary text-center">TAXI</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($data['list'])) {
                        foreach ($data['list'] as $list) {
                            $holidayType = "bg-warning text-black";
                            if ($list['CalendarSign'] != 0) {
                                $holidayType = "bg-danger text-white";
                            }

                            echo "<tr>";
                            echo "  <td class='" . $holidayType . " align-middle'>"
                                . date('l', strtotime($list['Date'])) . "<br/>"
                                . "<b>" . date('F d, Y', strtotime($list['Date'])) . "</b>"
                                . "</td>";
                            if (empty($list['WorkHour']) and empty($list['WorkHour'])) {
                                echo "<td colspan='7' class='text-center align-middle' style='color: #dedede;'>There are no requests for approval on this date.</td>";
                            } else {
                                echo "
                                <td class='text-right align-middle'>" . $list['WorkHour'] . "</td>
                                <td class='text-right align-middle'>" . $list['Overtime'] . "</td>
                                <td class='text-right align-middle'>Rp. " . number_format($list['MealsAmount'], 2) . "</td>
                                <td class='text-right align-middle'>Rp. " . number_format($list['OutOfOfficeAmount'], 2) . "</td>
                                <td class='text-right align-middle'>Rp. " . number_format($list['TaxiAmount'], 2) . "</td>
                                <td class='text-right align-middle'><span class='badge " . $list['StatusCSS'] . "'>" . $list['StatusLabel'] . "</span></td>
                                <td class='text-center  align-middle'>"
                                    . Html::a(
                                        '<i class="fa fa-edit"></i>',
                                        empty($list['WorkHour']) ? false : ['detail', 'id' => $list['Date']],
                                        [
                                            'class' => 'btn btn-success text-white',
                                            'style' => 'border-radius: 5px !important',
                                        ]
                                    )
                                    . "</td>";
                            }
                            echo "</tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php

$this->registerjs("
		$(document).ready(function(){
            localStorage.setItem('_manager_allay', JSON.stringify(''));

			$('#select-all').click(function(){
				$('.checkbox').prop('checked',true);
				$('#select-all').hide();
				$('#unselect-all').show();
			});
			$('#unselect-all').click(function(){
				$('.checkbox').prop('checked',false);
				$('#select-all').show();
				$('#unselect-all').hide();
			});
		});
	", View::POS_END, 'sorted_table');

?>



<script type="text/javascript">
    $(document).ready(function() {
        $('#YearDate').on('change', function() {
            $('#tr-form').submit();
        });
    });

    var paramJs = (paramJs || {});
    paramJs.urlTimeReportShowModal = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/update']); ?>';

    function TimeReportShowModal(link = "") {
        var link = (link || paramJs.urlBudgetShowModal);
        $.ajax({
            url: link,
            data: $('#tr-form').serialize(),
            method: "POST",
            dataType: 'html',
            success: function(data) {
                $('#detail').html(data);
            },
        });
    }

    function TimeReportUpdate(link) {
        TimeReportShowModal(link);
        $('#TrModal').modal('show');
    }

    window.closeModal = function() {
        reload();
    };

    function reload() {
        $.pjax.defaults.timeout = false;
        $.pjax.reload({
            container: '#tr-pjax'
        })
        $('#TrModal').modal('hide');
        $('#TrModal').data('modal', null);
    }
</script>