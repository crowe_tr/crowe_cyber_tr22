<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\tr\JobBudgetingTemp */

$this->title = $model->JobID;
$this->params['breadcrumbs'][] = ['label' => 'Job Budgeting Temps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="job-budgeting-temp-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'JobID' => $model->JobID, 'EmployeeID' => $model->EmployeeID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'JobID' => $model->JobID, 'EmployeeID' => $model->EmployeeID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'JobID',
            'Seq',
            'EmployeeID',
            'Planning',
            'FieldWork',
            'Reporting',
            'WrapUp',
            'OverTime',
            'Total',
            'CreatedBy',
            'CreatedAt',
            'UpdateBy',
            'UpdateAt',
        ],
    ]) ?>

</div>
