<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\tr\search\JobBudgetingTempSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-budgeting-temp-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JobID') ?>

    <?= $form->field($model, 'Seq') ?>

    <?= $form->field($model, 'EmployeeID') ?>

    <?= $form->field($model, 'Planning') ?>

    <?= $form->field($model, 'FieldWork') ?>

    <?php // echo $form->field($model, 'Reporting') ?>

    <?php // echo $form->field($model, 'WrapUp') ?>

    <?php // echo $form->field($model, 'OverTime') ?>

    <?php // echo $form->field($model, 'Total') ?>

    <?php // echo $form->field($model, 'CreatedBy') ?>

    <?php // echo $form->field($model, 'CreatedAt') ?>

    <?php // echo $form->field($model, 'UpdateBy') ?>

    <?php // echo $form->field($model, 'UpdateAt') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
