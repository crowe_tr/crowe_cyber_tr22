<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\tr\JobBudgetingTemp */

$this->title = 'Update Job Budgeting Temp: ' . $model->JobID;
$this->params['breadcrumbs'][] = ['label' => 'Job Budgeting Temps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->JobID, 'url' => ['view', 'JobID' => $model->JobID, 'EmployeeID' => $model->EmployeeID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="job-budgeting-temp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
