<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\tr\JobBudgetingTemp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-budgeting-temp-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'JobID')->textInput() ?>

    <?= $form->field($model, 'Seq')->textInput() ?>

    <?= $form->field($model, 'EmployeeID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Planning')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FieldWork')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Reporting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'WrapUp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'OverTime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Total')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CreatedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CreatedAt')->textInput() ?>

    <?= $form->field($model, 'UpdateBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UpdateAt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
