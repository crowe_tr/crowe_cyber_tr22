<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\tr\search\JobBudgetingTempSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Job Budgeting Temps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-budgeting-temp-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Job Budgeting Temp', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'JobID',
            'Seq',
            'EmployeeID',
            'Planning',
            'FieldWork',
            //'Reporting',
            //'WrapUp',
            //'OverTime',
            //'Total',
            //'CreatedBy',
            //'CreatedAt',
            //'UpdateBy',
            //'UpdateAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
