<?php echo '<style type="text/css">'.$css.'</style>'; ?>
<br/>
<table class="table table-bordered" border="1">
    <tr>
        <th>NO</th>
        <th>EMPLOYEE</th>
        <th>POSITION</th>
        <th>ENTITY</th>
        <th>DIVISION</th>
        <th>DAY</th>
        <th>DATE</th>
        <th>TASK TYPE</th>
        <th>TASK NAME</th>
        <th>JOB CODE</th>
        <th>CLIENT NAME</th>
        <th>JOB DESCRIPTION</th>
        <th>WH</th>
        <th>OT</th>
        <th>AREA</th>
        <th>WH FEE</th>
        <th>MEALS</th>
        <th>OPE</th>
        <th>TOTAL</th>
        <th>TYPE TAXI</th>
        <th>TAXI</th>
        <th>STATUS</th>
    </tr>
    <?php
        $i = 0;
        foreach($results as $data){
            $i++;
            echo "<tr>";
            echo "<td>".$i."</td>";
            echo "<td>".$data['EmployeeName']."</td>";
            echo "<td>".$data['Position']."</td>";
            echo "<td>".$data['Entity']."</td>";
            echo "<td>".$data['Division']."</td>";
            echo "<td>".date("l", strtotime($data['Date'])) ."</td>";
            echo "<td>".$data['Date']."</td>";
            echo "<td>".$data['TypeName']."</td>";
            echo "<td>".$data['TaskName']."</td>";
            if ($data['JobID'] != 0) {
              echo "<td>".$data['JobCode']."</td>";
              echo "<td>".$data['ClientName']."</td>";
            }
            else{
              echo "<td>Not a Project</td>";
              echo "<td>Not a Project</td>";
            }

            echo "<td>".$data['JobDescription']."</td>";

            echo "<td>".$data['WorkHour']."</td>";
            echo "<td>".$data['Overtime']."</td>";
            echo "<td>".$data['Zone']."</td>";
            echo "<td>".$data['WHAmount']."</td>";
            echo "<td>".$data['MealsAmount']."</td>";
            echo "<td>".$data['OutOfOfficeAmount']."</td>";
            echo "<td>".($data['MealsAmount'] + $data['OutOfOfficeAmount'])."</td>";
            echo "<td>".$data['TypeTaxi']."</td>";
            echo "<td>".$data['TaxiAmount']."</td>";
            echo "<td>".$data['Status']."</td>";
            echo "</tr>";

        }
    ?>
</table>
