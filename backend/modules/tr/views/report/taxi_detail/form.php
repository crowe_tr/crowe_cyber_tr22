<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;
use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK
use common\models\cl\search\Client;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;

use common\models\cm\CmMaster;
use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;
use common\models\tr\Job;
use common\models\hr\Employee;
use common\models\cm\Taxi as cmTaxi;

$this->title = $options['name'];
?>
<?php
$form = ActiveForm::begin([
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'options' => [
    'enctype' => 'multipart/form-data',
  ],
  'fieldConfig' => [
    'template' => '{label}{input}',
    'options' => [
      'class' => 'form-group form-group-default',
    ],
  ],
]);
?>

<div class="row" style="overflow:scroll; min-height: 132px;">
  <div class="col-8 bg-white padding-10 p-l-30 p-r-30">
    <h2 style="font-size: 18px !important" class="no-margin no-padding"><?= $this->title ?></h2>
  </div>
  <div class="col-4 bg-white padding-10 p-t-20 p-l-30 p-r-30 text-right">
    <?= $form->field($model, 'Custom[fields][]', ['options' => ['class' => '']])->dropDownList(
      $fields['data'],
      [
        'class' => 'ant-multiselect',
        'multiple' => true,
        'options' => $fields['options']
      ]
    )->label(false); ?>
  </div>
  <div class="col-12 bg-white padding-10 p-l-30 p-r-30">
    <div class="form-group-attached">

      <div class="row">
        <div class="col-md-2">
          <?php
          $model->Date = empty($model->Date) ? date('Y-m-d') . ' TO ' . date('Y-m-d') : $model->Date;
          echo $form->field($model, 'Date')->widget(DateRangePicker::classname(), [
            'pluginOptions' => [
              'locale' => [
                'format' => 'Y-MM-DD',
                'separator' => ' TO ',
              ],
            ],

          ]);
          ?>
        </div>
        <div class="col-md-2">
          <?php
          echo $form->field($model, 'Custom[Entity]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
              'options' => ['id' => 'Entity', 'placeholder' => 'SELECT ALL'],
              'data' => ArrayHelper::map(Entity::find()->asArray()->all(), 'entityName', 'entityName'),
              'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => Yii::t('backend', 'SELECT ALL'),
              ],
            ]
          )->label('ENTITY');
          ?>
        </div>

        <div class="col-md-2">
          <?php
          echo $form->field($model, 'Custom[Division]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
              'options' => ['id' => 'divisionID', 'placeholder' => 'SELECT ALL'],
              'data' => ArrayHelper::map(Division::find()->distinct()->all(), 'divName', 'divName'),
              'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => Yii::t('backend', 'SELECT ALL'),
              ],
            ]
          )->label('DIVISION');
          ?>
        </div>
        <div class="col-md-2">
          <?php
          echo $form->field($model, 'Custom[deptID]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
            'data' => empty($model->Custom['Division']) ? [] : ArrayHelper::map(Dept::find()->joinwith('div')->where(['cmDivision.divName' => $model->Custom['Division']])->asArray()->all(), 'deptName', 'deptName'),
            'options' => ['id' => 'deptID', 'placeholder' => 'Select ALL ...'],
            'type' => DepDrop::TYPE_SELECT2,
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
              'depends' => ['divisionID'],
              'url' => Url::to(['/cm/helper/loaddept_ver2']),
              'loadingText' => 'Loading ...',
              'allowClear' => true,
              'placeholder' => Yii::t('backend', 'SELECT ALL'),
            ],
          ])->label('Departement');
          ?>
        </div>
        <div class="col-md-2">
          <?php
          $parent = empty($model->Custom['parentID']) ? [] : Employee::find()->where(['Id' => $model->Custom['parentID']])->one();
          echo $form->field($model, 'Custom[parentID]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
            ->widget(Select2::classname(), [
              'initValueText' => !empty($parent->id) ? $parent->Id . ' - ' . $parent->fullName : "",
              'options' => ['placeholder' => 'Search Leader ...'],
              'pluginOptions' => [
                'tags' => false,
                'allowClear' => true,

                //'minimumInputLength' => 1,
                'ajax' => [
                  'url' => Url::to(['/hr/helper/srcinitial']),
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                //'templateResult' => new JsExpression('formatSelect2SupplierId'),
                //'templateSelection' => new JsExpression('formatSelect2SupplierIdSelection'),
              ],
            ])->label('Group');
          ?>
        </div>
        <div class="col-md-2">
          <?php
          $manager = empty($model->Custom['ManagerID']) ? [] : Employee::find()->where(['initial' => $model->Custom['ManagerID']])->one();
          echo $form->field($model, 'Custom[ManagerID]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
            ->widget(Select2::classname(), [
              'initValueText' => !empty($parent->id) ? $parent->initial . ' - ' . $parent->fullName : "",
              'options' => ['placeholder' => 'Search Leader ...'],
              'pluginOptions' => [
                'tags' => false,
                'allowClear' => true,

                //'minimumInputLength' => 1,
                'ajax' => [
                  'url' => Url::to(['/hr/helper/srcinitial']),
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                //'templateResult' => new JsExpression('formatSelect2SupplierId'),
                //'templateSelection' => new JsExpression('formatSelect2SupplierIdSelection'),
              ],
            ])->label('MANAGER');
          ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-2">
          <?php
          echo $form->field($model, 'Custom[TaxiType]', ['options' => ['onchange' => '', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
              'options' => ['placeholder' => 'Select All...'],
              'data' => ArrayHelper::map(cmTaxi::find()->asArray()->all(), 'name', 'name'),
              'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => Yii::t('backend', 'Select All..'),
              ],
              'options' => ['id' => 'TaxiType', 'placeholder' => 'Select All ...'],
            ]
          )->label('Taxi Type');
          ?>
        </div>
        <div class="col-sm-2">
          <?php
          echo $form->field($model, 'Custom[TaskTypeID]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
              'options' => ['placeholder' => 'Select All ....'],
              'data' => $init['TaskTypeID'],
              'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => Yii::t('backend', 'Select All ...'),
              ],
              'options' => ['id' => 'TaskTypeID', 'placeholder' => 'Select All ...'],
            ]
          )->label('Task Type');
          ?>
        </div>
        <div class="col-sm-2">
          <?php
          echo $form->field($model, 'Custom[TaskID]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
            'data' => $init['TaskID'],
            'options' => ['id' => 'TaskID', 'placeholder' => 'Select All ...'],
            'type' => DepDrop::TYPE_SELECT2,
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
              'allowClear' => false,
              'depends' => ['TaskTypeID'],
              'url' => Url::to(['/cm/helper/loadtimereporttask']),
              'loadingText' => 'Loading ...',
            ],
          ])->label('Task');
          ?>
        </div>
        <div class="col-md-2">
          <?php
          $client = empty($model->Custom['ClientCode']) ? [] : Client::find()->where(['Code' => $model->Custom['ClientCode']])->one();
          echo $form->field($model, 'Custom[ClientCode]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
            ->widget(Select2::classname(), [
              'initValueText' => !empty($client->Code) ? $client->Code . ' - ' . $client->Name : "",

              'options' => ['placeholder' => 'SELECT ALL'],
              'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'minimumInputLength' => 2,
                'ajax' => [
                  'url' => Url::to(['/cm/helper/srcclient_code']),
                  'delay' => 250,
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) { return {q:params.term} }'),
                  'loadingText' => 'Loading ...',
                ],
                // 'loadingText' => 'Loading ...',
                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                //'templateResult' => new JsExpression('formatSelect2ClientID'),
                //'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
              ],
            ])->label('CLIENT');
          ?>
        </div>

        <div class="col-sm-4">
          <?php
          $job = empty($model->Custom['JobCode']) ? [] : Job::find()->where(['JobCode' => $model->Custom['JobCode']])->one();
          echo $form->field($model, 'Custom[JobCode]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
            ->widget(Select2::classname(), [
              'initValueText' => !empty($job->JobCode) ? $job->JobCode . ' - ' . $job->Description : "",
              'options' => ['placeholder' => 'SELECT ALL ...'],
              'pluginOptions' => [
                'tags' => false,
                'allowClear' => true,
                //'minimumInputLength' => 1,
                'ajax' => [
                  'url' => Url::to(['/cm/helper/srcjob']),
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                //'templateResult' => new JsExpression('formatSelect2SupplierId'),
                //'templateSelection' => new JsExpression('formatSelect2SupplierIdSelection'),
              ],
            ])->label('Job');;
          ?>
        </div>
      </div>
      <div class="row">


        <div class="col-md-2">
          <?php
          echo $form->field($model, 'Custom[levelID]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
              'options' => ['id' => 'levelID', 'placeholder' => 'Select All ....'],
              'data' => ArrayHelper::map(Level::find()->asArray()->all(), 'levelName', 'levelName'),
              'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => Yii::t('backend', 'Select All ...'),
              ],
            ]
          )->label('Level');
          ?>

        </div>
        <div class="col-md-2">
          <?php
          $emp = empty($model->Custom['EmployeeID']) ? [] : Employee::find()->where(['Id' => $model->Custom['EmployeeID']])->one();

          echo $form->field($model, 'Custom[EmployeeID]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
            ->widget(Select2::classname(), [
              'initValueText' => !empty($emp->Id) ? $emp->Id . ' - ' . $emp->fullName : "",

              'options' => ['placeholder' => 'SELECT ALL'],
              'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'minimumInputLength' => 2,
                'ajax' => [
                  'url' => Url::to(['/cm/helper/srcemployee']),
                  'delay' => 250,
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) { return {q:params.term} }'),
                  'loadingText' => 'Loading ...',
                ],
                // 'loadingText' => 'Loading ...',
                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                //'templateResult' => new JsExpression('formatSelect2ClientID'),
                //'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
              ],
            ])->label('EMPLOYEE NAME');
          ?>
        </div>

        <div class="col-md-1">
          <?php
          $model->ReportFormat = 'view';
          echo $form->field($model, 'ReportFormat', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
              'options' => ['id' => 'ReportFormat', 'placeholder' => 'format'],
              'data' => ['view' => 'VIEW', 'xls' => 'XLS', 'pdf' => 'PDF'],
              'pluginOptions' => [
                'allowClear' => false,
                'placeholder' => 'format ..',
              ],
            ]
          );
          ?>
        </div>
        <div class="col-md-1">
          <?= Html::submitButton('SEARCH', ['class' => 'btn btn-success btn-block active padding-15 btn-lg']) ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row" style="height: 382px;overflow:scroll !important;  ">
  <div class="p-l-10 p-r-10" style="background: #dedede; min-height: 1020px; min-width: 2020px">
    <?= $data; ?>
  </div>
</div>
<?php ActiveForm::end(); ?>

<script>
  $(document).ready(function() {
    $('input[type="checkbox"]').click(function() {
      var inputValue = $(this).attr("value");
      $("." + inputValue).toggle();
    });
  });
</script>