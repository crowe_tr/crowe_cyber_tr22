
<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;
use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK
use yii\helpers\Url;
use yii\web\JsExpression;

$this->title = 'MONTHLY REPORT';
$this->params['breadcrumbs'][] = 'TIMEREPORT';
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
    ],
]);
?>
<div class="form-group-attached">

  <div class="row">
  <div class="col-md-1">
    <?php
    $model->Month = !empty($model->Month) ? $model->Month : intval(date('m'));

    echo $form->field($model, 'Month', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
      Select2::classname(),
      [
        'options' => ['id' => 'Month', 'placeholder' => 'Pilih'],
        'data' => CommonHelper::getMonths(),
          'pluginOptions' => [
            'allowClear' => false,
            'placeholder' => 'pilih cabang ..',
          ],

      ]
    );
    ?>
  </div>

  <div class="col-md-1">
    
  <?php
  $model->Year = !empty($model->Year) ? $model->Year : intval(date('Y'));
  for ($i = date('Y'); $i >= (date('Y') - 5); --$i) {
    $years[$i] = $i;
  }
  echo $form->field($model, 'Year', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
    Select2::classname(),
    [
      'options' => ['id' => 'Year', 'placeholder' => 'Pilih'],
      'data' => $years,
      'pluginOptions' => [
        'allowClear' => false,
        'placeholder' => 'pilih cabang ..',
      ],
    ]
  );
    ?>
  </div>
  <div class="col-md-3">
    <?php
      echo $form->field($model, 'Custom', ['options' => [ 'class' => ' form-group form-group-default form-group-default-select2']])
        ->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Search Employee ... '],
        'pluginOptions' => [
          'tags' => false,
          'ajax' => [
            'url' => Url::to(['helper/srcemployeeopt']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term} }'),
            'loadingText' => 'Loading ...',
          ],
          // 'loadingText' => 'Loading ...',
          //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
          //'templateResult' => new JsExpression('formatSelect2ClientID'),
          //'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
        ],
      ]);
    ?>
  </div>

  <div class="col-md-1">
    <?php
    $model->ReportFormat = 'view';
      echo $form->field($model, 'ReportFormat', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
          Select2::classname(),
          [
              'options' => ['id' => 'ReportFormat', 'placeholder' => 'format'],
              'data' => [ 'view' => 'VIEW', 'xls' => 'XLS'],
                  'pluginOptions' => [
                      'allowClear' => false,
                      'placeholder' => 'format ..',
                  ],
          ]
      );
    ?>
  </div>
  <div class="col-md-1">
    <?= Html::submitButton('SEARCH', ['class' => 'btn btn-success btn-block active padding-15 btn-lg']) ?>
  </div>
  </div>

</div>
<div class="row">
    <div class="col-md-12" style="overflow:auto">      
      <?=$data;?>
    </div>
</div>

<?php  ActiveForm::end(); ?>
