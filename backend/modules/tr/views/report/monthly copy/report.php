<?php echo '<style type="text/css">'.$css.'</style>'; ?>
<br/>
<table class="table table-bordered">
    <tr>
        <th rowspan="2">NO</th>
        <th rowspan="2">TYPE</th>
        <th rowspan="2">CLIENT NAME AND DESCRIPTION OR WORK</th>
        <th colspan="31">MONTH</th>
        <th rowspan="2">TOTAL</th>
        <th rowspan="2">CLIENT CODE OR PROJECT NO</th>
        <th rowspan="2">JOB NO</th>
        <th rowspan="2">JOB</th>
    </tr>

    <tr>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th>6</th>
        <th>7</th>
        <th>8</th>
        <th>9</th>
        <th>10</th>
        <th>11</th>
        <th>12</th>
        <th>13</th>
        <th>14</th>
        <th>15</th>
        <th>16</th>
        <th>17</th>
        <th>18</th>
        <th>19</th>
        <th>20</th>
        <th>21</th>
        <th>22</th>
        <th>23</th>
        <th>24</th>
        <th>25</th>
        <th>26</th>
        <th>27</th>
        <th>28</th>
        <th>29</th>
        <th>30</th>
        <th>31</th>
    </tr>
    <?php 
        $i=0;
        foreach($results as $data){
            $i++;

            $Description = explode(' - ', $data['Description']);
            $ClientCode = !empty($Description[0]) ? $Description[0] : "";
            $ClientCode_ = explode('-', $ClientCode);
            $JobNo = !empty($ClientCode_[2]) ? $ClientCode_[2] : "";

            $Job = !empty($Description[1]) ? $Description[1] : "";
            $Title = !empty($Description[2]) ? $Description[2] : "";
    ?>
        <tr>
            <td><?=$i;?></td>
            <td><?=$data['Type'];?></td>
            <td><?=$Job;?></td>
            <td align="right"><?=$data['1'];?></td>
            <td align="right"><?=$data['2'];?></td>
            <td align="right"><?=$data['3'];?></td>
            <td align="right"><?=$data['4'];?></td>
            <td align="right"><?=$data['5'];?></td>
            <td align="right"><?=$data['6'];?></td>
            <td align="right"><?=$data['7'];?></td>
            <td align="right"><?=$data['8'];?></td>
            <td align="right"><?=$data['9'];?></td>
            <td align="right"><?=$data['10'];?></td>
            <td align="right"><?=$data['11'];?></td>
            <td align="right"><?=$data['12'];?></td>
            <td align="right"><?=$data['13'];?></td>
            <td align="right"><?=$data['14'];?></td>
            <td align="right"><?=$data['15'];?></td>
            <td align="right"><?=$data['16'];?></td>
            <td align="right"><?=$data['17'];?></td>
            <td align="right"><?=$data['18'];?></td>
            <td align="right"><?=$data['19'];?></td>
            <td align="right"><?=$data['20'];?></td>
            <td align="right"><?=$data['21'];?></td>
            <td align="right"><?=$data['22'];?></td>
            <td align="right"><?=$data['23'];?></td>
            <td align="right"><?=$data['24'];?></td>
            <td align="right"><?=$data['25'];?></td>
            <td align="right"><?=$data['26'];?></td>
            <td align="right"><?=$data['27'];?></td>
            <td align="right"><?=$data['28'];?></td>
            <td align="right"><?=$data['29'];?></td>
            <td align="right"><?=$data['30'];?></td>
            <td align="right"><?=$data['31'];?></td>
            <td align="right"><?=$data['TotalWorkHour'];?></td>
            <td><?=$ClientCode;?></td>
            <td><?=$JobNo;?></td>
            <td><?=$Title;?></td>

        </tr>
    <?php 
        } 
    ?>
</table>