<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;
use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;

use common\models\cm\CmMaster;
use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;
use common\models\cl\Client;
use common\models\hr\Employee;

$this->title = $options['name'];
?>
<?php
$form = ActiveForm::begin([
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'options' => [
    'enctype' => 'multipart/form-data',
  ],
  'fieldConfig' => [
    'template' => '{label}{input}',
    'options' => [
      'class' => 'form-group form-group-default',
    ],
  ],
]);
?>

<div class="row" style="overflow:scroll; min-height: 132px;">
  <div class="col-8 bg-white padding-10 p-l-30 p-r-30">
    <h2 style="font-size: 18px !important" class="no-margin no-padding"><?=$this->title?></h2>
  </div>
  <div class="col-4 bg-white padding-10 p-t-20 p-l-30 p-r-30 text-right">
    <?= $form->field($model, 'Custom[fields][]', ['options' => ['class' => '']])->dropDownList(
      $fields['data'],
      [
        'class' => 'ant-multiselect',
        'multiple' => true,
        'options' => $fields['options']
      ]
    )->label(false); ?>
  </div>
  <div class="col-12 bg-white padding-10 p-l-30 p-r-30">
    <div class="form-group-attached">

      <div class="row">
        <div class="col-md-1">
        <div class="col-md-2" style="display:none;">
          <?php
          $model->Date = empty($model->Date) ? date('Y-m-d') . ' TO ' . date('Y-m-d') : $model->Date;
          echo $form->field($model, 'Date')->widget(DateRangePicker::classname(), [
            'pluginOptions' => [
              'locale' => [
                'format' => 'Y-MM-DD',
                'separator' => ' TO ',
              ],
            ],

          ]);
          ?>
        </div>
          <?php
            for ($i = date('Y'); $i >= (date('Y') - 5); --$i) {
                $years[$i] = $i;
            }
            $model->Custom['year'] = !empty($model->Custom['year']) ? $model->Custom['year'] : intval(date('Y'));
            echo $form->field($model, 'Custom[year]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
              Select2::classname(),
              [
                'options' => ['id' => 'year', 'placeholder' => 'SELECT ALL'],
                'data' => $years,
                'pluginOptions' => [
                  'allowClear' => true,
                  'placeholder' => Yii::t('backend', 'SELECT ALL'),
                ],
              ]
            )->label('YEAR');
            // $model->Year = !empty($model->Year) ? $model->Year : intval(date('Y'));
            // for ($i = date('Y'); $i >= (date('Y') - 5); --$i) {
            //   $years[$i] = $i;
            // }
            // echo $form->field($model, 'Year', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            //   Select2::classname(),
            //   [
            //     'options' => ['id' => 'Year', 'placeholder' => 'Pilih'],
            //     'data' => $years,
            //     'pluginOptions' => [
            //       'allowClear' => false,
            //       'placeholder' => 'pilih ..',
            //     ],
            //   ]
            // );
          ?>
        </div>


        <div class="col-md-2">
          <?php
          echo $form->field($model, 'Custom[Employee]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
            ->widget(Select2::classname(), [
              'options' => ['placeholder' => 'SELECT ALL'],
              'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'minimumInputLength' => 2,
                'ajax' => [
                  'url' => Url::to(['/cm/helper/srcemployee']),
                  'delay' => 250,
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) { return {q:params.term} }'),
                  'loadingText' => 'Loading ...',
                ],
                // 'loadingText' => 'Loading ...',
                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                //'templateResult' => new JsExpression('formatSelect2ClientID'),
                //'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
              ],
            ])->label('EMPLOYEE NAME');
          ?>
        </div>

        <div class="col-md-1">
          <?php
          $model->ReportFormat = 'view';
          echo $form->field($model, 'ReportFormat', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
              'options' => ['id' => 'ReportFormat', 'placeholder' => 'format'],
              'data' => ['view' => 'VIEW', 'xls' => 'XLS', 'pdf' => 'PDF'],
              'pluginOptions' => [
                'allowClear' => false,
                'placeholder' => 'format ..',
              ],
            ]
          );
          ?>
        </div>
        <div class="col-md-1">
          <?= Html::submitButton('SEARCH', ['class' => 'btn btn-success btn-block active padding-15 btn-lg']) ?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" style="height: 382px;overflow:scroll !important;  ">
  <div class="p-l-10 p-r-10" style="background: #dedede; min-height: 1020px; min-width: 1080px">
    <?= $data; ?>
  </div>
</div>
<?php ActiveForm::end(); ?>
<script>
  $(document).ready(function() {
    $('input[type="checkbox"]').click(function() {
      var inputValue = $(this).attr("value");
      $("." + inputValue).toggle();
    });
  });
</script>