<span><b>ANNUAL LEAVE REPORT - DETAIL</b></span>
<?php
  $data_header = Yii::$app->db->createCommand("call get_employee_profile( '" . $model->Custom['Employee'] ."' );")->queryAll();
  $res_val = $data_header[0];
?>

<span>
  <br><b>EMPLOYEE</b>
  <?php
    echo ": " . $res_val['Id'] . " - " . $res_val['fullName'] . " ( " . $res_val['level_name'] . " )";
  ?>
</span>
<span>
  <br><b>ENTITY / DIVISION / DEPARTMENT</b>
  <?php
    echo ": " . $res_val['entity_name'] . " / " . $res_val['division_name'] . " / " . $res_val['dept_name'];
  ?>
</span>   
<span>
  <br><b>GROUP / MANAGER / SUPERVISOR</b>
  <?php
    echo ": " . $res_val['group_name'] . " / " . $res_val['manager_name'] . " / " . $res_val['supervisor_name'];
  ?>
</span>
<span>
  <br><b>YEAR</b>
  <?php
    echo ": " . $model->Custom['year'];
  ?>
</span>    


<table class="table table-bordered" border="1" style="width: 100%">
  <?php
  echo "<tr>";
  echo "<th class='bg-primary'>NO.</th>";
  foreach ($fields['selected'] as $f ) {
    echo "<th class='".$f." bg-primary'>" . $fields['data'][$f] . "</th>";
  }
  echo "</tr>";

  $arr_ctr = array( 'employee_id', 'annual_year', 'trans_date' );
  $arr_dec = array( 'annual_used', 'res_balance' );

  $i = 0;
  foreach ($data as $d) {
    $i++;
    // $bg = ($d['calendar_sign'] == 1) ? 'background: #ffcccc' : '';

    echo "<tr>";
    echo "<td style=''>" . $i . "</td>";
    foreach ($fields['selected'] as $f) {
      if ( in_array($f, $arr_dec) ) {
        echo "<td class='".$f."' style='' align='right'>" . number_format ( $d[$f], 1, '.', ',' ) . "</td>";
      } elseif( in_array($f, $arr_ctr) ) {
        echo "<td class='".$f."' style='' align='center'>" . $d[$f] . "</td>";
      } else {
        echo "<td class='".$f."' style=''>" . $d[$f] . "</td>";
      }    
    }
    echo "</tr>";
  }
  ?>
</table>