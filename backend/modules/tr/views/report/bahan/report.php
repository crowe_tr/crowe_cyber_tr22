<table class="table table-bordered" border="1" style="width: 100%" >
  <?php
  echo "<tr>";
    echo "<th class='bg-primary'>NO.</th>";
    foreach($fields as $field=>$f) {
      echo "<th class='bg-primary'>".$f."</th>";
    }
  echo "</tr>";
  
  $i = 0;
  foreach ($results as $data) {
    $i++;
    echo "<tr>";
    echo "<td>" . $i . "</td>";
    foreach($fields as $field=>$f) {
      echo "<td>".$data[$field]."</td>";
    }
    echo "</tr>";
  }
  ?>
</table>