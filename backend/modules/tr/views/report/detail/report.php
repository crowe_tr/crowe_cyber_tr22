<table class="table table-bordered" border="1" style="width: 100%">
  <?php
  echo "<tr>";
  echo "<th class='bg-primary'>NO.</th>";
  foreach ($fields['selected'] as $f) {
    echo "<th class='".$f." bg-primary'>" . $fields['data'][$f] . "</th>";
  }
  echo "</tr>";

  $arr_number = array( 'WHAmount', 'MealsAmount', 'OutOfOfficeAmount', 'TaxiAmount', 'meals_ope' );
  $arr_dec = array( 'WorkHour', 'Overtime' );
  $arr_id = array('EmployeeID', 'group_id', 'approval2_id', 'approval1_id');

  $i = 0;
  foreach ($data as $d) {
    $i++;
    $bg = (false) ? 'background: #ffcccc' : '';

    echo "<tr>";
    echo "<td style='" . $bg . "'>" . $i . "</td>";
    foreach ($fields['selected'] as $f) {
      if ( in_array($f, $arr_number)) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 0, '.', ',' ) . "</td>";
      } elseif ( in_array($f, $arr_dec)) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 1, '.', ',' ) . "</td>";
      } elseif ( in_array($f, $arr_id)) {
        echo "<td class='".$f."' style='" . $bg . "' align='center'>" . $d[$f] . "</td>";
      } else {
        echo "<td class='".$f."' style='" . $bg . "' align='left'>" . $d[$f] . "</td>";
      }    
    }
    echo "</tr>";
  }
  ?>
</table>