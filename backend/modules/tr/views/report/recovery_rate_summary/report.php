<table class="table table-bordered" border="1" style="width: 100%">
  <?php
  echo "<tr>";
  echo "<th class='bg-primary'>NO.</th>";
  foreach ($fields['selected'] as $f) {
    echo "<th class='".$f." bg-primary'>" . $fields['data'][$f] . "</th>";
  }
  echo "</tr>";


  $arr_number = array( 'job_fee', 'tc_partner', 'tc_associate_director', 'tc_director', 'tc_senior_manager', 'tc_manager',
  'tc_associate_manager', 'tc_managing_partner', 'tc_secretary', 'tc_deputy_managing_partner',
  'tc_chairman', 'tc_principal', 'tc_manager_3', 'tc_supervisor', 'tc_supervisor_1', 'tc_supervisor_2',
  'tc_supervisor_3', 'tc_senior', 'tc_senior_1', 'tc_senior_2', 'tc_senior_3', 'tc_junior', 'tc_internship',
  'tc_admin_staff', 'tc_administrasi', 'tc_total', 'tc_other', 'total_charges' );

  $arr_pct = array( 'rate_time_charges', 'rate_total_charges', 'progress_time' );

  $i = 0;
  foreach ($data as $d) {
    $i++;
    $bg = (false) ? 'background: #ffcccc' : '';
    echo "<tr>";
    echo "<td style='" . $bg . "'>" . $i . "</td>";
    foreach ($fields['selected'] as $f) {
      if ( in_array($f, $arr_number)) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 0, '.', ',' ) . "</td>";
      } elseif ( in_array($f, $arr_pct) ) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . $d[$f] . "</td>";
      } else {
        echo "<td class='".$f."' style='" . $bg . "'>" . $d[$f] . "</td>";
      }
    }
    echo "</tr>";
  }
  ?>
</table>