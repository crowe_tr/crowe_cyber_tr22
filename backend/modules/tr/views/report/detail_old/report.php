<?php echo '<style type="text/css">' . $css . '</style>'; ?>
<br />
<table class="table table-bordered" border="1">
  <tr>
    <th class="bg-primary">NO.</th>
    <th class="bg-primary">DATE</th>
    <th class="bg-primary">SIGN</th>
    <th class="bg-primary">EMPLOYEE ID</th>
    <th class="bg-primary">FULL NAME</th>
    <th class="bg-primary">GROUP ID</th>
    <th class="bg-primary">GROUP NAME</th>
    <th class="bg-primary">LEVEL</th>
    <th class="bg-primary">DESCRIPTION</th>
    <th class="bg-primary">ENTITY</th>
    <th class="bg-primary">DIVISION</th>
    <th class="bg-primary">Departement</th>
    <th class="bg-primary">WH</th>
    <th class="bg-primary">OT</th>
    <th class="bg-primary">OUT OF OFFICE</th>
    <th class="bg-primary">MEALS</th>
    <th class="bg-primary">TAXI</th>
  </tr>
  <?php
  $i = 0;
  foreach ($results as $data) {
    $i++;
    echo "<tr>";
    echo "<td>" . $i . "</td>";
    echo "<td>" . date("D, M Y", strtotime($data['calendar_date'])) . "</td>";
    echo "<td>" . $data['calendar_sign'] . "</td>";
    echo "<td>" . $data['employee_id'] . "</td>";
    echo "<td>" . $data['employee_name'] . "</td>";
    echo "<td>" . $data['group_id'] . "</td>";
    echo "<td>" . $data['group_name'] . "</td>";
    echo "<td>" . $data['employee_level'] . "</td>";
    echo "<td>" . $data['description'] . "</td>";
    echo "<td>" . $data['entity_name'] . "</td>";
    echo "<td>" . $data['division_name'] . "</td>";
    echo "<td>" . $data['dept_name'] . "</td>";
    echo "<td>" . $data['wh_amount'] . "</td>";
    echo "<td>" . $data['ot_amount'] . "</td>";
    echo "<td>" . $data['outofoffice_amount'] . "</td>";
    echo "<td>" . $data['meals_amount'] . "</td>";
    echo "<td>" . $data['taxi_amount'] . "</td>";

    echo "</tr>";
  }
  ?>
</table>