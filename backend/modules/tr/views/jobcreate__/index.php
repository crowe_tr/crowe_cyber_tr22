<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use common\components\CmHelper;
use common\models\hr\Employee;
use common\models\cl\Client;
use backend\modules\st\controllers\StController;


// CmHelper::set_title($this, 'Job List', 'DATA');
$this->title = 'Job List';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = 'DATA';
$this->params['breadcrumbs_btn'] = Html::a(
                                                  '<i class="pg-plus"></i> <span class="hidden-xs">CREATE NEW</span>',
                                                  false,
                                                  [
                                                    'onclick' => 'FormCreate()',
                                                    'class' => 'btn btn-warning text-white ',
                                                  ]
                                                );
$column = [
            CmHelper::set_column_no(),
            CmHelper::set_column('job_code', 'Job Code', 2),
            CmHelper::set_column_bool2('manager_id','3',
                ArrayHelper::map(Employee::find()->distinct()->all(), 'user_id', 'full_name'),
                'manager.full_name'),
            CmHelper::set_column_bool2('client_id','3',
                ArrayHelper::map(Client::find()->distinct()->all(), 'id', 'client_name'),
                'client.client_name'),
            CmHelper::set_column('description', 'Description', '4'),
            StController::set_icon('tr', 'jobcreate'),
            // [
            //     'header' => '<i class="fa fa-cogs"></i>',
            //     'headerOptions' => ['class' => 'bg-success text-center'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'no-padding'],
            //     'format' => 'raw',
            //     'value' => function($data){
            //       return CmHelper::set_icon($data->id, 'cm', 'entity');
            //     },
            // ],
          ];

  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();

  // $js = CmHelper::set_script('entity');
  // $this->registerjs($js, View::POS_END);

?>

<script type="text/javascript">
var paramJs = (paramJs || {});
paramJs.urlFormShowModal = '<?= Yii::$app->urlManager->createAbsoluteUrl(['/tr/jobcreate/form']); ?>"';

window.closeModal = function(){
  reload();
};

function FormShowModal(link='') {
  var link = (link || paramJs.urlFormShowModal);
  $.ajax({
    url: link,
    data:  $('#crud-form').serialize(),
    method: 'POST',
    dataType: 'html',
    success: function(data) {
      $('#detail').html(data);
    },
  });
}

function FormCreate() {
  FormShowModal();
  $('#crud-modal').modal('show');
}
function FormUpdate(link) {
  FormShowModal(link);
  $('#crud-modal').modal('show');
}

function reload(){
  $.pjax.defaults.timeout = false;
  $.pjax.reload({
    container: '#crud-pjax'
  })
  $('#crud-form').modal('hide');
  $('#crud-form').data('modal', null);
  $('.modal-backdrop').modal('hide');
  $( '.modal-backdrop' ).removeClass( 'show' ).addClass( 'hide' );
}



</script>
