<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\SwitchInput;
use yii\widgets\MaskedInput;

use common\components\JobHelper;
use common\models\hr\Employee;

JobHelper::set_title_txt($this, ['label' => Yii::t('backend', 'JOB LIST'), 'url' => ['all']], ($modelJob->isNewRecord) ? 'Create New Job' : 'Update Job  : ' . $modelJob->job_code);
echo JobHelper::widget_pjax('DetailModal');
$form = ActiveForm::begin(JobHelper::begin_form('job-form'));
echo $form->errorSummary($modelJob);
\yii\widgets\Pjax::begin(['id' => rand() . 'pjax', 'enablePushState' => false, 'id' => 'job-pjax']);
?>
<div class="alert alert-danger" id="error" style="display:none"></div>

<b>Client</b>
<div class="form-group-attached mb-3">
	<div class="row">
		<div class="col-md-8">
			<?php
			echo
			$form->field($modelJob, 'id', ['options' => ['class' => '']])->hiddenInput(['id' => 'id'])->label(false);
			?>

			<?php
			echo $form->field($modelJob, 'client_id', ['options' => ['onchange' => 'lateJob()', 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
				Select2::classname(),
				[
					'data' =>  $data['Client'],
					'options' => ['id' => 'Class', 'placeholder' => 'Select ...', 'class' => 'readonly'],

				]
			);
			?>
		</div>
		<div class="col-md-4">
			<?php
			echo $form->field($modelJob, 'job_code', ['options' => ['class' => ' form-group form-group-default']])->textInput(['class' => "form-control"]);
			?>
		</div>
	</div>
</div>
<b>Job Description</b>
<div class="form-group-attached mb-3">

	<div class="row">
		<div class="col-md-2">
			<?php
			echo
			$form->field(
				$modelJob,
				'start_date',
				[
					'template' => '{label}{input}', 'options' => ['class' => 'form-group form-group-default input-group'],
				]
			)->widget(DatePicker::classname(), [
				'type' => DatePicker::TYPE_INPUT,
				'value' => date('Y-m-d'),
				'pluginOptions' => [
					'autoclose' => true,
					'format' => 'yyyy-mm-dd',
				],
			])
			?>
		</div>
		<div class="col-md-2">
			<?php
			echo
			$form->field(
				$modelJob,
				'end_date',
				[
					'template' => '{label}{input}', 'options' => ['class' => 'form-group form-group-default input-group'],
				]
			)->widget(DatePicker::classname(), [
				'type' => DatePicker::TYPE_INPUT,
				'value' => date('Y-m-d'),
				'pluginOptions' => [
					'autoclose' => true,
					'format' => 'yyyy-mm-dd',
				],
			])
			?>
		</div>
		<div class="col-md-8">
			<?php
			echo $form->field($modelJob, 'description')->textInput();
			?>
		</div>

	</div>
</div>
<b>Manager & Fee</b>
<div class="form-group-attached mb-3">
	<div class="row">

		<div class="col-md-4">

			<?php
			$Manager = '';
			$dataManager = null;
			if (!$modelJob->isNewrecord || !empty($modelJob->Manager)) {
				$dataManager = Employee::findOne($modelJob->manager_id);
				$Manager = empty($dataManager->full_name) ? $modelJob->manager_id : $dataManager->user_id . ' - ' . $dataManager->full_name . ' - ' . $dataManager->level->level_name;
			}

			echo $form->field($modelJob, 'manager_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
				->widget(Select2::classname(), [
					'initValueText' => $Manager,
					'options' => ['placeholder' => 'Search Manager ... '],
					'pluginOptions' => [
						'minimumInputLength' => 2,
						'tags' => false,
						'ajax' => [
							'url' => Url::to(['helper/srcemployemanager', 'user_id' => $modelJob->manager_id]),
							'dataType' => 'json',
							'data' => new JsExpression('function(params) { return {q:params.term , JobID:$("#JobID").val()}; }'),
						],
					],
				]);
			?>
		</div>
		<div class="col-md-4">
			<?php
			echo
			$form->field($modelJob, 'job_fee')->widget(MaskedInput::className(), [
				'clientOptions' => [
					'alias' => 'decimal',
					'groupSeparator' => ',',
					'autoGroup' => true,
				],
			]);
			?>
		</div>

		<div class="col-md-4">
			<?php
			echo $form->field(
				$modelJob,
				'include_ope',
				['options' => ['class' => ' form-group form-group-default']]
			)->widget(
				SwitchInput::className(),
				['pluginOptions' => [
					'size' => 'small',
					'onText' => 'Yes',
					'offText' => 'No',
				]]
			);

			?>
		</div>
	</div>
</div>

<?php
echo '<button type="button" class="btn btn-info btn-lg float-right" data-dismiss="modal">CANCEL</button>';
echo Html::submitButton('SAVE', ['class' => 'btn btn-success btn-lg float-right']);
echo $id;
?>

<?php
	\yii\widgets\Pjax::end();
 	ActiveForm::end();
 ?>
 <script type="text/javascript">
 	var paramJs = (paramJs || {});
 	paramJs.urlLatesJob = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/jobcreate/budget_latesjob']); ?>';

 	function lateJob() {
 		var link = (link || paramJs.urlLatesJob);
 		$.ajax({
 			url: link,
 			data: $('#job-form').serialize(),
 			method: "POST",
 			dataType: 'html',
 			success: function(data) {
 				var d = JSON.parse(data)
 				$('#client_id').html(d.ClientID);
 				$('#job-job_code').val(d.job);
 			},
 		});
 	}

 	$('#job-form').on('beforeSubmit', function() {
 		var url = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/jobcreate/save', 'id' => $id]); ?>';
 		var form_data = new FormData($('#job-form')[0]);
 		return HelperSaveAjax(url, form_data);
		alert('tes');
 	});
 </script>
