<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\SwitchInput;
use yii\widgets\MaskedInput;

use common\components\JobHelper;
use common\models\hr\Employee;

JobHelper::set_title_txt($this, ['label' => Yii::t('backend', 'JOB LIST'), 'url' => ['all']], ($modelJob->isNewRecord) ? 'Create New Job' : 'Update Job  : ' . $modelJob->JobCode);
echo JobHelper::widget_pjax('DetailModal');
$form = ActiveForm::begin(JobHelper::begin_form('job-form'));
echo $form->errorSummary($modelJob);
\yii\widgets\Pjax::begin(['id' => rand() . 'pjax', 'enablePushState' => false, 'id' => 'job-pjax']);
?>
<div class="alert alert-danger" id="error" style="display:none"></div>

<b>Update Job Code</b>
<div class="form-group-attached mb-3 mt-3">
	<div class="row">
		<div class="col-md-12">
			<?php
			echo $form->field($modelJob, 'JobCode', ['options' => ['class' => ' form-group form-group-default']])->textInput(['class' => "form-control"]);
			?>
		</div>
	</div>
</div>

<?php
echo '<button type="button" class="btn btn-info btn-lg float-right" data-dismiss="modal">CANCEL</button>';
echo Html::submitButton('SAVE', ['class' => 'btn btn-success btn-lg float-right']);
?>

<?php \yii\widgets\Pjax::end(); ?>
<?php ActiveForm::end(); ?>


<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlLatesJob = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budget_latesjob']); ?>';

	function lateJob() {
		var link = (link || paramJs.urlLatesJob);
		$.ajax({
			url: link,
			data: $('#job-form').serialize(),
			method: "POST",
			dataType: 'html',
			success: function(data) {
				var d = JSON.parse(data)
				$('#CLientID').html(d.ClientID);
				$('#job-jobcode').val(d.job);
			},
		});
	}

	$('#job-form').on('beforeSubmit', function() {
		var url = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/jobcreate/save', 'id' => $id]); ?>';
		var form_data = new FormData($('#job-form')[0]);
		return HelperSaveAjax(url, form_data);
	});
</script>