<?php

namespace backend\modules\tr;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\tr\controllers';

    public function init()
    {
        parent::init();
        $this->layout = '@backend/views/layouts/main';
    }
}
