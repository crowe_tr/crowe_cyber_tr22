<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\helpers\Html;

use common\components\CommonHelper;
use common\components\TimeReportHelper;

use common\models\tr\Job;
use common\models\tr\JobBudgeting;
use common\models\tr\JobTemp;
use common\models\tr\JobBudgetingTemp;
use common\models\tr\JobComment;
use common\models\tr\TrPreviousChangesJob;
use common\models\tr\ms\Entity;
use common\models\tr\search\JobSearch;
use common\models\tr\search\JobviewSearch;
use common\models\tr\search\JobBudgetingSearch;
use common\models\tr\search\JobBudgetingTempSearch;
use common\models\tr\search\JobCommentSearch;
use common\models\cl\Client;
use common\models\hr\VempGroup;
use common\models\hr\Employee;
use  yii\data\ArrayDataProvider;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;

// use Yii;
// use common\models\cm\Dept;
// use common\models\cm\search\Dept as DeptSearch;
// use yii\web\Controller;
// use yii\web\NotFoundHttpException;
// use yii\filters\VerbFilter;
// use yii\web\MethodNotAllowedHttpException;
use backend\modules\cm\controllers\CmController;

class JobcreateController extends Controller
{
    protected function findModel($id)
    {
        if (($model = Job::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelTemp($id)
    {
        if (($model = JobTemp::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findModelBudget($EmployeeID, $JobID, $param = [])
    {
        if (($modelBudget = JobBudgeting::find()->where(['EmployeeID' => $EmployeeID, 'JobID' => $JobID])->one()) !== null) {
            return $modelBudget;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelEmployee($JobID, $param = [])
    {
        if (($modelBudget = JobBudgeting::find()->where(['JobID' => $JobID])->all()) !== null) {
            return $modelBudget;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function findModelBudgetTemp($EmployeeID, $JobID, $param = [])
    {
        if (($modelBudget = JobBudgetingTemp::find()->where(['EmployeeID' => $EmployeeID, 'JobID' => $JobID])->one()) !== null) {
            return $modelBudget;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelHistoryJob($JobID)
    {
      if ($model = TrPreviousChangesJob::find()->where(['JobID' => $JobID])->all()) {
        return $model;
      }
      else {
          throw new NotFoundHttpException('The requested page does not exist.');
      }
    }


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function getDataForForm($model)
    {
        $data = array();
        $data['Division'] = TimeReportHelper::getDivision();
        $data['Entity'] = TimeReportHelper::getEntity();
        $data['Client'] = TimeReportHelper::getClient();
        $data['Employee'] =TimeReportHelper::getEmployee();
        // $data['VempGroup'] =TimeReportHelper::getVempGroup();
        // $data['EmployeeJob'] =TimeReportHelper::getEmployeeJob();
        return $data;
    }

    public function actionFormnew($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
          var_dump($id, 'tes');
          die();
            $post = Yii::$app->request->post();
            $model = new Job();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);
            } else {
                $model = new Job();
            }
            return CmController::render_form($this, $model, $id, $action);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionJobnew($id = '')
    {
      $user = CommonHelper::getUserIndentity();
      // $modelJob = null;
      // if (Yii::$app->request->isAjax) {
      // $post = Yii::$app->request->post();
      // $modelJob = new Job();
      // $modelJob->load($post);
      // var_dump($user->user_id, $post);
      // die();
      // if ($modelJob->load($post)) {
      //   if ($modelJob->validate()) {
      //   var_dump($user->user_name, $modelJob);
      //   die();

        if (empty($id)) {
          $modelJob = new Job();
          $modelJob->created_by = $user->user_id;
          $modelJob->flag = 0;
          $modelJob->job_status = 0;
        } else {
          $modelJob = $this->findModel($id);
        }

        $data = $this->getDataForForm($modelJob);
        $modelBudgeting = null;
        return $this->render('_form', [
             'data' => $data,
             'model' => $modelJob,
             'model_detail' => $modelBudgeting,
         ]);
     //   }
     // }
       // } else {
       //     throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
       // }
    }

    public function actionLatestjob($id)
    {
      $data = null;
      if (!empty($id)) {
        $sql = "CALL common_list('latest-job', ".$id.");";
        $data = Yii::$app->db->createCommand($sql)->queryOne();
      }

      return $data['job_code'];
    }

    public function actionBudget_latesjob()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();

      $modelJob = new Job();
      $modelJob->load($post);

      $result = \Yii::$app->db->createCommand("select common_val('new-job-no',:paramName1)")
        ->bindValue(':paramName1', $modelJob->client_id);
      $data['job'] = $result->queryScalar();
      $data['ClientID'] = $modelJob->client_id;

      return $data;
    }


    public function actionSave($id = null)
    {
      $user = CommonHelper::getUserIndentity();
      $post = Yii::$app->request->post();
      $modelJob = empty($id) ? new Job() : $this->findModel($id);
      $modelJob->scenario = 'create';

      $result = [
        'model' => [],
        'state' => [
          'status' => false,
          'message' => 'Error : default'
        ]
      ];

      $transaction = \Yii::$app->db->beginTransaction();
      if ($modelJob->load($post)) {
        if ($modelJob->validate()) {
          try {
            if ($modelJob->isNewRecord) {
              $modelJob->job_status = 0;
              $modelJob->flag = 0;
              $modelJob->created_by = $user->user_id;

              $sql = "call cud_data('insert-job-manager','" . $modelJob->JobID . ";" . $modelJob->Manager . ";" . $user->Id . "')";
            } else {
              $modelJob_old = $this->findModel($id);

              $modelJob->UpdateBy = $user->Id;

              $sql = "call cud_data('insert-job-manager','" . $modelJob->JobID . ";" . $modelJob_old->Manager . ";" . $modelJob->Manager . ";" . $user->Id . "')";
            }
            $modelJob->Fee = CommonHelper::ResetDecimal($modelJob->Fee);

            Yii::$app->db->createCommand($sql)->queryOne();
            $result = HelperDB::save($modelJob);

            if ($result['state']['status'] == true) {
              $mail = $modelJob->manager->email;
              if (!empty($mail)) {
                $send_email = $this->Email($mail, 'THERE IS A NEW DRAFT JOB', $modelJob);
                if ($send_email) {
                  $result['state']['status'] = true;
                  $result['state']['message'] = "Success";
                  $transaction->commit();
                } else {
                  $result['state']['status'] = false;
                  $result['state']['message'] = "Can't Send Email, please cek email address " . $mail;
                  $transaction->rollback();
                }
              } else {
                $result['state']['status'] = false;
                $result['state']['message'] = "Can't Send Email, Manager's email is empty";
              }
            }
          } catch (Exception $e) {
            $result['state']['status'] = false;
            $result['state']['message'] = HelperDB::db_trigger_error($e->getMessage());
            $transaction->rollBack();
          }
        }
      }

      return json_encode($result['state']);
    }

    // public function actionLatestjob2()
    // {
    //   // var_dump('tes');
    //   // die();
    //   \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //   $post = Yii::$app->request->post();
    //
    //   $modelJob = new Job();
    //   $modelJob->load($post);
    //
    //   $result = \Yii::$app->db->createCommand("CALL getJobNo(:paramName1)")
    //     ->bindValue(':paramName1', $modelJob->client_id);
    //   $data['job'] = $result->queryScalar();
    //   $data['client_id'] = $modelJob->client_id;
    //   // var_dump($modelJob->ClientID);
    //
    //   return $data;
    // }
    //
    // public function actionLatestjob3()
    // {
    //   // var_dump('tes');
    //   // die();
    //   // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //   // $post = Yii::$app->request->post();
    //   //
    //   // $modelJob = new Job();
    //   // $modelJob->load($post);
    //   //
    //   // $result = \Yii::$app->db->createCommand("CALL getJobNo(:paramName1)")
    //   //   ->bindValue(':paramName1', $modelJob->client_id)
    //   //   ->queryOne();
    //   $data['job_code'] = '123';
    //   // $data['job'] = $result->queryScalar();
    //   // $data['client_id'] = $modelJob->client_id;
    //   // var_dump($modelJob->ClientID);
    //
    //   return $data;
    // }

    public function actionN($id = '')
    {

        $user = CommonHelper::getUserIndentity();
        if (empty($id)) {
            $modelJob = new Job();
            $modelJob->created_by = $user->user_id;
            $modelJob->flag = 0;
            $modelJob->job_status = 0;

        } else {
            $modelJob = $this->findModel($id);
            $Manager_id = $modelJob->manager_id;
            switch ($modelJob->job_status) {
              case 1:
                $status = 'NEW VERSION';
              break;
              case 3:
                $status = 'REVISED VERSION';
              break;
              default:
                $status = 'NEW VERSION';
            }
            $modelJob->updated_by = $user->user_id;
            $modelJob->scenario = 'update';
            // $modelJob->JobArea = explode(',', $modelJob->JobArea);

            // if ($modelJob->Status != 0  ) {
            //   if ($modelJob->Status != 3) {
            //     return $this->redirect(['n', 'id' => $modelJob->JobID]);
            //   }
            //
            // }
        }

        $data = $this->getDataForForm($modelJob);
        $searchModelBudgeting = new JobBudgetingSearch();
        $searchModelBudgeting->job_id = $modelJob->id;
        $modelBudgeting = $searchModelBudgeting->search($modelJob->id);

        $post = Yii::$app->request->post();

        if ($modelJob->load($post)) {
            $valid = $modelJob->validate();
            if ($valid) {

                $submittedType = \Yii::$app->request->post('submit');
                switch(isset($submittedType[0]) ? $submittedType[0] : 0) {
                   //your code
                   case '0' :
                      $modelJob->Status = 0 ;
                 //my code
                  break;

                     case '1' :
                        $modelJob->Status = 1 ;


                }


                $modelJob->Fee = empty($modelJob->Fee) ? 0 : str_replace(',', '', $modelJob->Fee);
                $modelJob->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
                $modelJob->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
                $modelJob->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
                $modelJob->OtherExpenseAllowance = empty($modelJob->OtherExpenseAllowance) ? 0 : str_replace(',', '', $modelJob->OtherExpenseAllowance);
                $modelJob->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);

                // $modelJob->JobArea = empty($modelJob->JobArea) ? '' : implode(',', $modelJob->JobArea);

                $modelJob->Total =  $modelJob->Fee;
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelJob->save(false)) {

                    } else {
                        $transaction->rollBack();
                    }

                    if ($flag) {
                        $transaction->commit();
                        if($modelJob->isNewRecord){



                          var_dump("haha");
                          die();

                            return $this->redirect(['n', 'id' => $modelJob->id]);
                        }
                        elseif ($submittedType == null) {
                          try {
                            $sql = "call insertJobManager(".$modelJob->id.",null, '".$modelJob->Manager."' , '".$user->Id."')";
                            $task = Yii::$app->db->createCommand($sql)->queryOne();
                          } catch (Exception $ex) {
                              echo 'Query failed', $ex->getMessage();
                          }
                          return $this->redirect(['/tr/jobcreate/index']);
                          // code...
                        }
                        else{
                          $submittedType = \Yii::$app->request->post('submit');

                            try {
                              $sql = "call insertJobManager(".$modelJob->id.",'".$Manager_id."', '".$modelJob->Manager."' , '".$user->Id."')";
                              $task = Yii::$app->db->createCommand($sql)->queryOne();
                            } catch (Exception $ex) {
                                echo 'Query failed', $ex->getMessage();
                            }

                          switch($submittedType[0]) {



                             //your code
                             case '0' :
                                return $this->redirect(['/tr/jobcreate/index', 'id' => $modelJob->id]);
                           //my code
                            break;

                               case '1' :
                                try {
                                  $send_email = $this->Email($modelJob->partner->email, 'THERE IS A NEW JOB NEEDS YOUR APPROVAL' ,$modelJob, $status);
                                  if ($send_email == 'true') {
                                    return $this->redirect(['/tr/jobcreate/index', 'id' => $modelJob->JobID]);
                                  }
                                } catch (\Exception $e)
                                {
                                  return $this->redirect(['/tr/jobcreate/index', 'id' => $modelJob->JobID]);
                                }
                          }
                        }
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        // var_dump('tes aja', $post);
        // die();

        return $this->render('jobForm', [
             'data' => $data,
             'modelJob' => $modelJob,
             'modelBudgeting' => $modelBudgeting,
         ]);

    }

    public function actionIndex()
    {
        $searchModel = new JobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id)
    {
      $modelJob = $this->findModel($id);
      $modelJob->delete();
      // var_dump($modelJob);
      // die();
      try {
        return $this->redirect(['index']);
      } catch (\Exception $e) {
        return $this->redirect(['index']);
      }

    }


}
