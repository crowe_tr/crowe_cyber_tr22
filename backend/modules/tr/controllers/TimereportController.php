<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\base\Exception;
use common\models\tr\Job;
use common\models\tr\Taxi;
use common\models\tr\TimeReport;
use common\models\tr\TimeReportNote;
use common\models\tr\TimeReportDetail;
use common\models\tr\TimeReportMeals;
use common\models\tr\TimeReportOutOfOffice;
use common\models\tr\TimeReportTaxi;

use common\models\st\TaskType;
use common\models\st\Task;
use common\models\cm\Taxi as cmTaxi;

use common\models\tr\search\TimeReportSearch;
use common\models\tr\search\TimeReportDetailSearch;
use common\models\tr\search\TimeReportNoteSearch;
use common\models\tr\search\TimeReportMealsSearch;
use common\models\tr\search\TimeReportOutOfOfficeSearch;
use common\models\tr\search\TimeReportTaxiSearch;

use yii\base\ErrorException;

use common\components\CommonHelper;
use common\components\Helper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class TimereportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->index(0);
    }
    public function actionAll()
    {
        return $this->index(0);
    }
    public function IndexFilter()
    {
        $session = Yii::$app->session;
        $session->open();

        $post = Yii::$app->request->post();
        $model = new TimeReport();
        if ($model->load($post)) {
            $data = [];
            $date = explode("-", $model->YearDate);

            $data['Year'] = $date[0];
            $data['Month'] = $date[1];
            $data['YearDate'] = $model->YearDate;
            $session->set('__trtimreport_index', $data);
        }
    }

    public function index($Status = null)
    {
        $user = CommonHelper::getUserIndentity();
        $session = Yii::$app->session;
        $session->remove('__timereport_index');

        $data = array();

        $this->IndexFilter();

        $model = new TimeReport();
        $param = isset($_SESSION['__trtimreport_index']) ? $_SESSION['__trtimreport_index'] : [];
        $param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;
        $param['Year'] = isset($param['Year']) ? $param['Year'] : date('Y');
        $param['Month'] = isset($param['Month']) ? $param['Month'] : date('m');
        $param['Status'] = isset($Status) ?  $Status : 0;
        $param['YearDate'] = isset($param['YearDate']) ? $param['YearDate'] : date('Y-m-t');

        $sql_list = "call antTrList('" . $param['EmployeeId'] . "', " . $param['Year'] . ", " . $param['Month'] . ")";
        $data['list'] = Yii::$app->db->createCommand($sql_list)->queryAll();
        return $this->render('index', [
            'param' => $param,
            'data' => $data,
            'model' => $model
        ]);
    }
    public function actionIndexdetail()
    {
        $session = Yii::$app->session;
        $session->open();

        $post = Yii::$app->request->post();
        if (!empty($post['id'])) {
            $id = $post['id'];

            $stored_session = isset($session['__timereport_index']) ? $session['__timereport_index'] : [];
            if (!empty($stored_session['Indexdetail'][$id])) {
                $details = $stored_session['Indexdetail'][$id];
            } else {
                $sql_job = "call TrTimeReportHint({$id})";
                $details = Yii::$app->db->createCommand($sql_job)->queryAll();

                $stored_session['Indexdetail'][$id] = $details;
                $session->set('__timereport_index', $stored_session);
            }

            if (!empty($details)) {
                echo '
                    <b>Summary :</b>
                    <table class="table table-bordered ">
                        <tr>
                            <th class="bg-primary">Description</th>
                            <th class="bg-primary">WH</th>
                            <th class="bg-primary">OT</th>
                            <th class="bg-primary">Meals</th>
                            <th class="bg-primary">Out Of Office</th>
                            <th class="bg-primary">Taxi</th>
                            <th class="bg-primary">Status</th>
                        </tr>';
                foreach ($details as $d) {
                    echo '<tr>
                                <th>' . $d['Description'] . '</th>
                                <th>' . $d['WorkHour'] . '</th>
                                <th>' . $d['Overtime'] . '</th>
                                <th>Rp.' . number_format($d['MealsAmount']) . '</th>
                                <th>Rp.' . number_format($d['OutOfOfficeAmount']) . '</th>
                                <th>Rp.' . number_format($d['TaxiAmount']) . '</th>
                                <th>' . $d['Status'] . '</th>
                            </tr>';
                }
                echo '</table>';
            } else {
                echo "No work has been inputted on this date";
            }
        } else {
            echo "No work has been inputted on this date";
        }
    }
    public function actionView($id = null, $EmployeeId = null, $action = null)
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $EmployeeId = empty($EmployeeId) ? $user->Id : $EmployeeId;
        $Date = $id;

        $model = TimeReport::find()->with('employee')->where(['EmployeeId' => $EmployeeId, 'Date' => $Date])->one();
        if ($model == null) {
            $model = new TimeReport();
            $model->EmployeeId = $EmployeeId;
            $model->Date = $Date;
            $model->isStayed = 0;
            $model->Status = 0;
            $model->save();
        }
        $searchModel = new TimeReportDetailSearch();
        $searchModel->TimeReportID = $model->ID;
        $dataProvider = $searchModel->search([]);

        $searchModel_Meals = new TimeReportMealsSearch();
        $searchModel_Meals->TimeReportID = $model->ID;
        $dataProvider_Meals = $searchModel_Meals->search([]);

        $searchModel_OPE = new TimeReportOutOfOfficeSearch();
        $searchModel_OPE->TimeReportID = $model->ID;
        $dataProvider_OPE = $searchModel_OPE->search([]);

        $searchModel_TAXI = new TimeReportTaxiSearch();
        $searchModel_TAXI->TimeReportID = $model->ID;
        $dataProvider_TAXI = $searchModel_TAXI->search([]);

        if ($model->load(Yii::$app->request->post())) {
            if (!isset($post['submit'])) {
                $model->Status = 0;
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            } else {
                $sql = "call getTrValidate('" . $model->ID . "');";
                $data = Yii::$app->db->createCommand($sql)->queryone();

                if ($data['vSummaryValidate']) {
                    $model->Status = 1;
                    if ($model->save()) {
                        return $this->redirect(['index']);
                    }
                } else {
                    if ($data['vDetailValidate'] == 0) {
                        $model->addError('ID', $data['vDetailDescription']);
                    }
                    if ($data['vMealValidate'] == 0) {
                        $model->addError('ID', $data['vMealDescription']);
                    }
                    if ($data['vOutOfficeValidate'] == 0) {
                        $model->addError('ID', $data['vOutOfficeDescription']);
                    }
                }
            }
        }

        return $this->renderAjax('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'dataProvider_Meals' => $dataProvider_Meals,
            'dataProvider_OPE' => $dataProvider_OPE,
            'dataProvider_TAXI' => $dataProvider_TAXI,
        ]);
    }
    public function actionUpdate($id = null, $EmployeeId = null, $action = null)
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();
        $EmployeeId = empty($EmployeeId) ? $user->Id : $EmployeeId;
        $Date = $id;

        $model = TimeReport::find()->with('employee')->where(['EmployeeId' => $EmployeeId, 'Date' => $Date])->one();
        if ($model == null) {
            $model = new TimeReport();
            $model->EmployeeId = $EmployeeId;
            $model->Date = $Date;
            $model->isStayed = 0;
            $model->Status = 0;
            $model->save();
        }

        $data = array();
        $data['Detail'] = Yii::$app->db->createCommand("call antTrUpdateDetail(" . $model->ID . ")")->queryAll();
        $data['Meal'] = Yii::$app->db->createCommand("call antTrUpdateMeals(" . $model->ID . ")")->queryAll();
        $data['OutOffice'] = Yii::$app->db->createCommand("call antTrUpdateOutOfOffice(" . $model->ID . ")")->queryAll();
        $data['Taxi'] = Yii::$app->db->createCommand("call antTrUpdateTaxi(" . $model->ID . ")")->queryAll();

        return $this->render('form', [
            'model' => $model,
            'data' => $data,
        ]);
    }
    public function actionGetjobtimeavailable()
    {
        $html = '';
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();

            if (!empty($post['id'])) {
                $id = intval($post['id']);
                $html = $this->Getjobtimeavailable($id);
            }
        }
        return $html;
    }
    public function Getjobtimeavailable($id)
    {
        $user = CommonHelper::getUserIndentity();

        $EmployeeId = $user->Id; //empty($EmployeeId) ? $user->Id : $EmployeeId;
        $sql = "call TrJobTimeAvailable({$id}, '{$EmployeeId}')";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        $html = "";
        if (!empty($data)) {
            $html .= "<div class='well' style='margin: 10px 0 30px; overflow-x: scroll'>";
            $html .= "<table class='table table-primary'>";
            $html .= "<tr>
                <td class='bg-primary'>Planning</td>
                <td class='bg-primary'>FieldWork</td>
                <td class='bg-primary'>Reporting</td>
                <td class='bg-primary'>WrapUp</td>
                <td class='bg-primary'>Overtime</td>
            </tr>";

            foreach ($data as $d) {
                $html .= "<tr>";
                $html .= "<td>" . $d['AvailablePlanning'] . "</td>";
                $html .= "<td>" . $d['AvailableFieldWork'] . "</td>";
                $html .= "<td>" . $d['AvailableReporting'] . "</td>";
                $html .= "<td>" . $d['AvailableWrapUp'] . "</td>";
                $html .= "<td>" . $d['AvailableOverTime'] . "</td>";
                $html .= "<tr>";
            }

            $html .= "</table>";
            $html .= "</div>";
        } else {
            $html .= "";
        }
        return $html;
    }
    public function AutoApproveItem($sql, $type)
    {
        $DetailList = Yii::$app->db->createCommand($sql)->queryAll();
        $flag = 1;
        if (!empty($DetailList) && !empty($type)) {
            foreach ($DetailList as $d) {
                if ($type == 'detail') {
                    $modelDetail = TimeReportDetail::findOne(['id' => $d['id']]);
                } else if ($type == 'meals') {
                    $modelDetail = TimeReportMeals::find()->where(['TimeReportID' => $d['trid'], 'Seq' => $d['id']])->one();
                } else if ($type == 'outofoffice') {
                    $modelDetail = TimeReportOutOfOffice::find()->where(['TimeReportID' => $d['trid'], 'Seq' => $d['id']])->one();
                } else if ($type == 'taxi') {
                    $modelDetail = TimeReportTaxi::find()->where(['TimeReportID' => $d['trid'], 'Seq' => $d['id']])->one();
                }
                if ($d['Approval1'] == 1 or $d['Approval2'] == 1) {
                    if ($d['Approval1'] == 1) {
                        $modelDetail->Approval1 = 1;
                    }
                    if ($d['Approval2'] == 1) {
                        $modelDetail->Approval2 = 1;
                    }

                    if (!($flag = $modelDetail->save(false))) {
                        $flag = 0;
                        break;
                    }
                }
            }
            return $flag;
        } else {
            return $flag;
        }
    }
    public function actionUpdatesave($id = null, $EmployeeId = null, $submit = false)
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $EmployeeId = empty($EmployeeId) ? $user->Id : $EmployeeId;
        $model = TimeReport::find()->with('employee')->where(['EmployeeId' => $EmployeeId, 'Date' => $id])->one();

        if ($submit) {
            $model->Status = 1;
        } else {
            $model->Status = 0;
        }
        if ($model->load($post)) {
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                    } else {
                        if ($model->Status == 1) {
                            $sqlDetailList = "call TrAutoApproveDetailList('" . $model->ID . "')";
                            if (!($flag = $this->AutoApproveItem($sqlDetailList, 'detail'))) {
                                $transaction->rollBack();
                                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                            }

                            $sqlMealsList = "call TrAutoApproveMealsList('" . $model->ID . "')";
                            if (!($flag = $this->AutoApproveItem($sqlMealsList, 'meals'))) {
                                $transaction->rollBack();
                                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                            }

                            $sqlOutOfOfficeList = "call TrAutoApproveOutOfOfficeList('" . $model->ID . "')";
                            if (!($flag = $this->AutoApproveItem($sqlOutOfOfficeList, 'outofoffice'))) {
                                $transaction->rollBack();
                                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                            }

                            $sqlTaxiList = "call TrAutoApproveTaxiList('" . $model->ID . "')";
                            if (!($flag = $this->AutoApproveItem($sqlTaxiList, 'taxi'))) {
                                $transaction->rollBack();
                                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (ErrorException $e) {
                    $transaction->rollBack();
                    $return = "Error : " . $e;
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = "Can't load post";
        }
        return $return;
    }
    public function GetFormData($model)
    {
        $user = CommonHelper::getUserIndentity();

        $data = [];
        $data['JobId'] = [];
        if ($model->TaskTypeID == 1) { //if PROJECTS
            $job_id = ($model->isNewRecord) ? 0 : $model->JobId;
            $sql_list = "call TrJobList('" . $model->TimeReportID . "', " . $job_id . ")"; //"call getTrJob('".$user->Id."')";
            $list = Yii::$app->db->createCommand($sql_list)->queryAll();

            if (!empty($list)) {
                foreach ($list as $i => $ls) {
                    $data['JobId'][$ls['JobID']] = $ls['Description'];
                }
            }
        } else {
            $data['JobId'][] = "";
        }

        $data['Zone'] = empty($model->JobId) ? [] : ArrayHelper::map(
            Yii::$app->db->createCommand("call getJobZone('" . $model->JobId . "')")->queryAll(),
            'id',
            'ClientZone'
        );
        $data['TaskTypeID'] = ArrayHelper::map(
            Yii::$app->db->createCommand("call getTaskType('" . $user->Id . "')")->queryAll(),
            'id',
            'taskTypeName'
        );
        $data['TaskID'] = "";
        if (!empty($model->TaskTypeID)) {
            $data['TaskID'] = ArrayHelper::map(
                Yii::$app->db->createCommand("call getTask('" . $user->Id . "', $model->TaskTypeID)")->queryAll(),
                'id',
                'taskName'
            );
        }

        ArrayHelper::map(TaskType::find()->where(['flag' => 1])->asArray()->all(), 'id', 'taskTypeName');;
        $data['TaskID'] = empty($model->TaskTypeID) ? [] : ArrayHelper::map(Task::find()->where(['taskTypeID' => $model->TaskTypeID])->asArray()->all(), 'id', 'taskName');;
        return $data;
    }

    //------------------------------------------------------------
    public function GetrulesDetails($modelDetail)
    {
        $JobId = empty($modelDetail->JobId) ? 0 : $modelDetail->JobId;
        $sql = "call CmOptionTr('" . $modelDetail->Date . "')";
        $data = Yii::$app->db->createCommand($sql)->queryOne();

        $id = empty($modelDetail->id) ? 0 : $modelDetail->id;
        $sql_allocation = "call getTimeReportAllocation('" . $modelDetail->TimeReportID . "', " . $id . ")";
        $data_allocation = Yii::$app->db->createCommand($sql_allocation)->queryOne();

        $modelDetail->WorkHour = empty($modelDetail->WorkHour) ? 0 : CommonHelper::ResetDecimal($modelDetail->WorkHour);
        $modelDetail->Overtime = empty($modelDetail->Overtime) ? 0 : CommonHelper::ResetDecimal($modelDetail->Overtime);

        $modelDetail->MaxOvertime = $data['MaxOvertime'];
        $modelDetail->MaxWorkhour = $data['MaxWorkhour'];
        $modelDetail->MinWorkhour = $data['MinWorkhour'];

        $data_allocation['Overtime'] = empty($data_allocation['Overtime']) ? 0 : $data_allocation['Overtime'];
        $data_allocation['WorkHour'] = empty($data_allocation['WorkHour']) ? 0 : $data_allocation['WorkHour'];

        $modelDetail->OvertimeAllocation = $modelDetail->Overtime + $data_allocation['Overtime'];
        $modelDetail->WorkHourAllocation = $modelDetail->WorkHour + $data_allocation['WorkHour'];

        return $modelDetail;
    }

    public function actionDetail($id = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];
            $TimeAvailable = "";

            $model = new TimeReport();
            $modelDetail = new TimeReportDetail();
            if ($model->load($post)) {
                if (!empty($id)) {
                    $modelDetail = TimeReportDetail::find()->where(['id' => $id])->one();
                    $TimeAvailable = $this->Getjobtimeavailable($modelDetail->JobId);
                }


                $modelDetail->TimeReportID = $model->ID;
                $modelDetail->EmployeeId = $model->EmployeeId;
                $modelDetail->Date = $model->Date;
                $modelDetail->isStayed = $model->isStayed;

                $rule = Yii::$app->db->createCommand("select TrRuleTaskAdd('{$modelDetail->TimeReportID}') as 'Valid'")->queryOne();
                $valid = $rule['Valid'];
                $valid = ($modelDetail->isNewRecord) ? $valid : 1;
                if ($valid) {
                    $data = $this->GetFormData($modelDetail);
                    return $this->renderAjax('formDetail', [
                        'modelDetail' => $modelDetail,
                        'data' => $data,
                        'TimeAvailable' => $TimeAvailable
                    ]);
                } else {
                    $return = "<div class='alert alert-danger'>Cannot add a new Task because it has reached its maximum limit</div>";
                    $return .= '<button type="button" class="btn btn-info p-t-10 p-b-10 pull-right" style="font-size: 12px" onclick="CloseModal()">CLOSE</button>';
                    return $return;
                }
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionOvertime($id = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];

            $model = new TimeReport();
            $modelDetail = new TimeReportDetail();
            if ($model->load($post)) {
                if (!empty($id)) {
                    $modelDetail = TimeReportDetail::find()->where(['id' => $id])->one();
                }
                $modelDetail->TimeReportID = $model->ID;
                $modelDetail->EmployeeId = $model->EmployeeId;
                $modelDetail->Date = $model->Date;
                $modelDetail->isStayed = $model->isStayed;

                $rule = Yii::$app->db->createCommand("select TrRuleTaskAdd('{$modelDetail->TimeReportID}') as 'Valid'")->queryOne();
                $valid = $rule['Valid'];
                $valid = ($modelDetail->isNewRecord) ? $valid : 1;
                if ($valid) {
                    $data = $this->GetFormData($modelDetail);
                    return $this->renderAjax('formOvertime', [
                        'modelDetail' => $modelDetail,
                        'data' => $data,
                    ]);
                } else {
                    $return = "<div class='alert alert-danger'>Cannot add a new Task because it has reached its maximum limit</div>";
                    $return .= '<button type="button" class="btn btn-info p-t-10 p-b-10 pull-right" style="font-size: 12px" onclick="CloseModal()">CLOSE</button>';
                    return $return;
                }
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionDetailsave()
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $model = new TimeReport();
        $modelDetail = new TimeReportDetail();
        $model_old = $modelDetail;

        if ($modelDetail->load($post)) {
            if (!empty($modelDetail->id)) {
                $modelDetail = TimeReportDetail::findOne(['id' => $modelDetail->id]);
                $model_old = TimeReportDetail::findOne(['id' => $modelDetail->id]);
            }

            $modelDetail->load($post);

            $filename = 'trd_' . $model->ID;
            $modelDetail->attachment = Helper::upload_model($modelDetail, 'attachment', $model_old->attachment, 'timereport_detail/', $filename . '_attachment_');

            $modelDetail = $this->GetrulesDetails($modelDetail);
            if ($modelDetail->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $modelDetail->JobId = empty($modelDetail->JobId) ? 0 : $modelDetail->JobId;
                    if (!($flag = $modelDetail->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($modelDetail, ['encode' => true]);
                    }

                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (ErrorException $e) {
                    $transaction->rollBack();
                    $return = "Error : " . $e;
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($modelDetail, ['encode' => true]);
            }
        } else {
            $return = "Error : Can't load model";
        }
        return $return;
    }


    //------------------------------------------------------------
    /*
    public function GetrulesMeals($modelMeals){

        $sql = "
            call getTrMealRule2('".$modelMeals->EmployeeId."', '".$modelMeals->Date."');
        ";
        $data = Yii::$app->db->createCommand($sql)->queryone();

        $id = empty($modelMeals->id) ? 0 : $modelMeals->id;
        $sql_allocation = "call getTimeReportAllocation('".$modelMeals->TimeReportID."', ".$id.")";
        $data_allocation = Yii::$app->db->createCommand($sql_allocation)->queryOne();

        $lookup = explode('-', $modelMeals->lookup);
        $modelMeals->TaskTypeID = $lookup[0];
        $modelMeals->TaskID = $lookup[1];
        $modelMeals->JobId = $lookup[2];

        $modelMeals->MealProvided = $data['IsClaim'];
        $modelMeals->MealAllocation = 1 + $data_allocation['Meals'];
        $modelMeals->MealClaimQty = $data['ClaimQty'];
        $modelMeals->MealClaimQtyStayed = $data['ClaimQtyStayed'];
        $modelMeals->MealMinOvertime = $data['MinOvertime'];

        return $modelMeals;

    }
    */
    public function actionMeals($id = null, $seq = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];

            $model = new TimeReport();
            $modelMeals = new TimeReportMeals();
            if ($model->load($post)) {
                if (!empty($id) && !empty($seq)) {
                    $modelMeals = TimeReportMeals::find()->where(['TimeReportID' => $id, 'Seq' => $seq])->one();
                }
                $modelMeals->TimeReportID = $model->ID;
                $modelMeals->TrDetID = ($modelMeals->isNewRecord) ? '' : $modelMeals->TrDetID;

                $rule = Yii::$app->db->createCommand("select TrRuleMealAdd('{$modelMeals->TimeReportID}') as 'Valid'")->queryOne();

                $valid = $rule['Valid'];
                $valid = ($modelMeals->isNewRecord) ? $valid : 1;
                if ($valid) {
                    $TrDetID_lookups = empty($modelMeals->TrDetID) ? 0 : $modelMeals->TrDetID;
                    $sql_lookups = "call TrJobMealList('{$modelMeals->TimeReportID}', {$TrDetID_lookups})";

                    $lookups = Yii::$app->db->createCommand($sql_lookups)->queryAll();

                    $data['lookup'] = [];
                    if (!empty($lookups)) {
                        foreach ($lookups as $lookup) {
                            $data['lookup'][$lookup['TrDetID']] = $lookup['Description'];
                        }
                    }

                    return $this->renderAjax('formMeals', [
                        'modelMeals' => $modelMeals,
                        'data' => $data,
                    ]);
                } else {
                    $return = "<div class='alert alert-danger'>Cannot add a new Meals because it has reached its maximum limit</div>";
                    $return .= '<button type="button" class="btn btn-info p-t-10 p-b-10 pull-right" style="font-size: 12px" onclick="CloseModal()">CLOSE</button>';
                    return $return;
                }
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionMealssave()
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();
        Yii::$app->cache->flush();

        $modelMeals = new TimeReportMeals();
        if ($modelMeals->load($post)) {
            if (!empty($modelMeals->Seq)) {
                $modelMeals = TimeReportMeals::find()->where(['TimeReportID' => $modelMeals->TimeReportID, 'Seq' => $modelMeals->Seq])->one();
            }
            $modelMeals->load($post);
            $modelMeals->Approval1 = 0;
            /*
            if(!empty($modelMeals->TimeReportID) && !empty($modelMeals->TrDetID)){
                $cekBoss = Yii::$app->db->createCommand("select ifnull(antHrIsNotBoss(".$modelMeals->TimeReportID.", ".$modelMeals->TrDetID."), 0) as IsNotBoss")->queryOne();
                if($cekBoss['IsNotBoss']==0){
                    $modelMeals->Approval1 = 1;
                }
            }
            */
            $modelMeals->Approval2 = 0;
            //$modelMeals = $this->GetrulesMeals($modelMeals);
            if ($modelMeals->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $modelMeals->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($modelMeals, ['encode' => true]);
                    }

                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return = "Error : " . $e;
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($modelMeals, ['encode' => true]);
            }
        } else {
            $return = "Error : Can't load model";
        }
        return $return;
    }


    //------------------------------------------------------------
    /*public function GetrulesOPE($modelOutOfOffice){
        $sql = "call getTrOutOfOfficeRule2('".$modelOutOfOffice->EmployeeId."', '".$modelOutOfOffice->Date."')";
        $data = Yii::$app->db->createCommand($sql)->queryOne();

        $id = empty($modelOutOfOffice->id) ? 0 : $modelOutOfOffice->id;
        $sql_allocation = "call getTimeReportAllocation('".$modelOutOfOffice->TimeReportID."', ".$id.")";
        $data_allocation = Yii::$app->db->createCommand($sql_allocation)->queryOne();

        $lookup = explode('-', $modelOutOfOffice->lookup);
        $modelOutOfOffice->TaskTypeID = $lookup[0];
        $modelOutOfOffice->TaskID = $lookup[1];
        $modelOutOfOffice->JobId = $lookup[2];

        $modelOutOfOffice->OPEProvided = $data['IsClaim'];
        $modelOutOfOffice->OPEAllocation = 1 + $data_allocation['OutOfOffice'];
        $modelOutOfOffice->OPEClaimQty = $data['ClaimQty'];
        $modelOutOfOffice->OPEClaimQtyStayed = $data['ClaimQtyStayed'];
        return $modelOutOfOffice;
    }
    */

    public function actionOpe($id = null, $seq = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];

            $model = new TimeReport();
            $modelOutOfOffice = new TimeReportOutOfOffice();
            if ($model->load($post)) {
                if (!empty($id) && !empty($seq)) {
                    $modelOutOfOffice = TimeReportOutOfOffice::find()->where(['TimeReportID' => $id, 'Seq' => $seq])->one();
                }
                $modelOutOfOffice->TimeReportID = $model->ID;
                $modelOutOfOffice->TrDetID = ($modelOutOfOffice->isNewRecord) ? 0 : $modelOutOfOffice->TrDetID;

                $rule = Yii::$app->db->createCommand("select TrRuleOutOffAdd('{$modelOutOfOffice->TimeReportID}') as 'Valid'")->queryOne();
                $valid = $rule['Valid'];
                $valid = ($modelOutOfOffice->isNewRecord) ? $valid : 1;
                if ($valid) {
                    $lookups = Yii::$app->db->createCommand("call TrJobOutOffList('{$modelOutOfOffice->TimeReportID}', '{$modelOutOfOffice->TrDetID}')")->queryAll();
                    $data['lookup'] = [];
                    $data['Zone'] = [];
                    foreach ($lookups as $lookup) {
                        if ($modelOutOfOffice->TrDetID == $lookup['TrDetID']) {
                            $JobArea = explode(',', $lookup['JobArea']);
                            foreach ($JobArea as $Area) {
                                $data['Zone'][$Area] = $Area;
                            }
                        }
                        $data['lookup'][$lookup['TrDetID']] = $lookup['Description'];
                    }

                    return $this->renderAjax('formOPE', [
                        'modelOutOfOffice' => $modelOutOfOffice,
                        'data' => $data,
                    ]);
                } else {
                    $return = "<div class='alert alert-danger'>Cannot add a new Out Of Office because it has reached its maximum limit</div>";
                    $return .= '<button type="button" class="btn btn-info p-t-10 p-b-10 pull-right" style="font-size: 12px" onclick="CloseModal()">CLOSE</button>';
                    return $return;
                }
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionOpesave()
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $modelOutOfOffice = new TimeReportOutOfOffice();
        if ($modelOutOfOffice->load($post)) {
            if (!empty($modelOutOfOffice->Seq)) {
                $modelOutOfOffice = TimeReportOutOfOffice::find()->where(['TimeReportID' => $modelOutOfOffice->TimeReportID, 'Seq' => $modelOutOfOffice->Seq])->one();
            }

            $modelOutOfOffice->load($post);
            if ($modelOutOfOffice->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                $modelOutOfOffice->Approval1 = 0;
                /*
                if(!empty($modelOutOfOffice->TimeReportID) && !empty($modelOutOfOffice->TrDetID)){
                    $cekBoss = Yii::$app->db->createCommand("select ifnull(antHrIsNotBoss(".$modelOutOfOffice->TimeReportID.", ".$modelOutOfOffice->TrDetID."), 0) as IsNotBoss")->queryOne();
                    if($cekBoss['IsNotBoss']==0){
                        $modelOutOfOffice->Approval1 = 1;
                    }
                }
                */
                $modelOutOfOffice->Approval2 = 0;

                try {
                    if (!($flag = $modelOutOfOffice->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($modelOutOfOffice, ['encode' => true]);
                    }

                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return = "Error : " . $e;
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($modelOutOfOffice, ['encode' => true]);
            }
        } else {
            $return = "Error : Can't load model";
        }
        return $return;
    }

    //------------------------------------------------------------
    /*public function GetrulesTaxi($modelTaxi){
        $JobId = empty($modelTaxi->JobId) ? 0 : $modelTaxi->JobId;
        $sql = "call getTrTaxiRule2('".$modelTaxi->EmployeeId."', '".$modelTaxi->Date."')";
        $data = Yii::$app->db->createCommand($sql)->queryOne();

        $id = empty($modelTaxi->id) ? 0 : $modelTaxi->id;
        $sql_allocation = "call getTimeReportAllocation('".$modelTaxi->TimeReportID."', ".$id.")";
        $data_allocation = Yii::$app->db->createCommand($sql_allocation)->queryOne();

        $lookup = explode('-', $modelTaxi->lookup);
        $modelTaxi->TaskTypeID = $lookup[0];
        $modelTaxi->TaskID = $lookup[1];
        $modelTaxi->JobId = $lookup[2];

        $modelTaxi->TaxiProvided = $data['IsClaim'];
        $modelTaxi->TaxiAllocation = 1 + $data_allocation['Taxi'];
        $modelTaxi->TaxiClaimQty = $data['ClaimQty'];
        $modelTaxi->TaxiClaimQtyStayed = $data['ClaimQtyStayed'];
        $modelTaxi->TaxiMinOvertime = $data['MinOvertime'];
        $modelTaxi->TaxiMinClock = $data['MinClock'];

        $modelTaxi->Amount = CommonHelper::ResetDecimal($modelTaxi->Amount);

        return $modelTaxi;
    }
    */
    public function actionTaxi($id = null, $seq = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];

            $model = new TimeReport();
            $modelTaxi = new TimeReportTaxi();
            if ($model->load($post)) {

                if (!empty($id) && !empty($seq)) {
                    $modelTaxi = TimeReportTaxi::find()->where(['TimeReportID' => $id, 'Seq' => $seq])->one();
                }

                $modelTaxi->TimeReportID = $model->ID;
                $modelTaxi->EmployeeId = $model->EmployeeId;
                $modelTaxi->Date = $model->Date;
                $modelTaxi->isStayed = $model->isStayed;

                $data['TaskTypeID'] = [];
                if (!empty($modelTaxi->cmTaxiID)) {
                    $class = Yii::$app->db->createCommand("call getTrTaxiClassList($modelTaxi->cmTaxiID)")->queryAll();
                    foreach ($class as $c) {
                        $data['TaskTypeID'][$c['ID']] = $c['taskTypeName'];
                    }
                }

                $data['lookup'] = [];
                if (!empty($modelTaxi->cmTaxiID)) {
                    $TrDetID = empty($modelTaxi->TrDetID) ? 0 : $modelTaxi->TrDetID;
                    $sql = "call TrJobTaxiList({$modelTaxi->TimeReportID}, {$modelTaxi->cmTaxiID}, {$TrDetID})";
                    $lookups = Yii::$app->db->createCommand($sql)->queryAll();
                    foreach ($lookups as $lookup) {
                        $data['lookup'][$lookup['TrDetID']] = $lookup['Description'];
                    }
                }

                $sql_boss = "select antTrCanClaimTaxi('{$modelTaxi->EmployeeId}') as CanClaim";
                $boss = Yii::$app->db->createCommand($sql_boss)->queryOne();
                $CanClaim = empty($boss['CanClaim']) ? 0 : 1;
                if (intval($CanClaim) != 1) {
                    $data['cmTaxiID'] = ArrayHelper::map(cmTaxi::find()->where('overtime!=1')->asArray()->all(), 'id', 'name');
                } else {
                    $data['cmTaxiID'] = ArrayHelper::map(cmTaxi::find()->where('id!=7')->asArray()->all(), 'id', 'name');
                }

                return $this->renderAjax('formTaxi', [
                    'modelTaxi' => $modelTaxi,
                    'data' => $data,
                ]);
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionTaxisave()
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $modelTaxi = new TimeReportTaxi();

        if ($modelTaxi->load($post)) {
            if (!empty($modelTaxi->Seq)) {
                $modelTaxi = TimeReportTaxi::find()->where(['TimeReportID' => $modelTaxi->TimeReportID, 'Seq' => $modelTaxi->Seq])->one();
            }

            $modelTaxi->load($post);
            $modelTaxi->Amount = CommonHelper::ResetDecimal($modelTaxi->Amount);
            if ($modelTaxi->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                $modelTaxi->Approval1 = 0;
                /*
                if(!empty($modelTaxi->TimeReportID) && !empty($modelTaxi->TrDetID)){
                    $cekBoss = Yii::$app->db->createCommand("select ifnull(antHrIsNotBoss(".$modelTaxi->TimeReportID.", ".$modelTaxi->TrDetID."), 0) as IsNotBoss")->queryOne();
                    if($cekBoss['IsNotBoss']==0){
                        $modelTaxi->Approval1 = 1;
                    }
                }
                */
                $modelTaxi->Approval2 = 0;

                try {
                    if (!($flag = $modelTaxi->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($modelTaxi, ['encode' => true]);
                    }

                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return = "Error : " . $e;
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($modelTaxi, ['encode' => true]);
            }
        } else {
            $return = "Error : Can't load model";
        }
        return $return;
    }


    public function actionComments($tr, $id, $type)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();
        $user = CommonHelper::getUserIndentity();

        $modelComments = new TimeReportNote();
        $modelComments->TimeReportID = $tr;
        $modelComments->ParentID = $id;
        $modelComments->Type = $type;
        $for = Yii::$app->db->createCommand("select ifnull(antTrCommentFor(" . $modelComments->TimeReportID . ", " . $modelComments->ParentID . "), 1) as f")->queryOne();
        $modelComments->For = empty($for['f']) ? 1 : $for['f'];

        $forLookUps = Yii::$app->db->createCommand("select ifnull(antTrCommentForLookUp(" . $modelComments->TimeReportID . ", " . $modelComments->ParentID . ", '" . $user->Id . "'), 1) as fl")->queryOne();
        $forLookUps = empty($forLookUps['fl']) ? 0 : $forLookUps['fl'];

        if ($forLookUps == 2) {
            $forLookUp = [2 => 'Junior'];
        } elseif ($forLookUps == 1) {
            $forLookUp = [1 => 'Junior'];
        } else {
            $forLookUp = [1 => 'Supervisor', 2 => 'Manager'];
        }

        $searchmodelComments = new TimeReportNoteSearch([
            'TimeReportID' => $modelComments->TimeReportID,
            'ParentID' => $modelComments->ParentID,
            'Type' => $modelComments->Type,
            'For' => $modelComments->For,
        ]);
        $dataProviderComments = $searchmodelComments->search([]);

        $list = $this->renderPartial('formCommentsDetail', [
            'dataProviderComments' => $dataProviderComments,
        ]);
        return $this->renderAjax('formComments', [
            'list' => $list,
            'modelComments' => $modelComments,
            'dataProviderComments' => $dataProviderComments,
            'for' => $for,
            'forLookUp' => $forLookUp,
        ]);
    }

    public function actionCommentssave()
    {
        $user = CommonHelper::getUserIndentity();
        $post = Yii::$app->request->post();
        $modelComments = new TimeReportNote();
        if ($modelComments->load($post)) {
            $modelComments->EmployeeId = $user->Id;
            $modelComments->Date = date('Y-m-d H:i:s');

            if ($modelComments->save()) {
                $this->CommentsEmail($modelComments);
                $searchmodelComments = new TimeReportNoteSearch([
                    'TimeReportID' => $modelComments->TimeReportID,
                    'ParentID' => $modelComments->ParentID,
                    'Type' => $modelComments->Type,
                    'For' => $modelComments->For,

                ]);
                $dataProviderComments = $searchmodelComments->search([]);

                $return = $this->renderAjax('formCommentsDetail', [
                    'dataProviderComments' => $dataProviderComments,
                ]);
            } else {
                $return = "error : cant save data";
            }
        } else {
            $return = "error : cant load post";
        }

        return $return;
    }
    public function actionCommentsrefresh()
    {
        $user = CommonHelper::getUserIndentity();
        $post = Yii::$app->request->post();
        $modelComments = new TimeReportNote();
        if ($modelComments->load($post)) {
            $modelComments->EmployeeId = $user->Id;
            $modelComments->Date = date('Y-m-d H:i:s');

            $searchmodelComments = new TimeReportNoteSearch([
                'TimeReportID' => $modelComments->TimeReportID,
                'ParentID' => $modelComments->ParentID,
                'Type' => $modelComments->Type,
                'For' => $modelComments->For,

            ]);
            $dataProviderComments = $searchmodelComments->search([]);

            $return = $this->renderAjax('formCommentsDetail', [
                'dataProviderComments' => $dataProviderComments,
            ]);
        } else {
            $return = "error : cant load post";
        }

        return $return;
    }

    public function CommentsEmail($model)
    {
        if (!empty($model->Id)) {
            $modelComments = TimeReportNote::find()->where(['Id' => $model->Id])->one();

            if (!empty($modelComments)) {
                $sql = "CALL antTrCommentsEmailList({$modelComments->TimeReportID}, {$modelComments->ParentID})";
                $lookups = Yii::$app->db->createCommand($sql)->queryAll();

                $email = [];
                if (!empty($lookups)) {
                    foreach ($lookups as $r) {
                        if (!empty($r['EmployeeEmail'])) {
                            if (filter_var($r['EmployeeEmail'], FILTER_VALIDATE_EMAIL)) {
                                $email[$r['EmployeeEmail']] = $r['EmployeeEmail'];
                            }
                        }

                        if (!empty($r['SupervisorEmail'])) {
                            if (filter_var($r['SupervisorEmail'], FILTER_VALIDATE_EMAIL)) {
                                $email[$r['SupervisorEmail']] = $r['SupervisorEmail'];
                            }
                        }

                        if (!empty($r['ManagerEmail'])) {
                            if (filter_var($r['ManagerEmail'], FILTER_VALIDATE_EMAIL)) {
                                $email[$r['ManagerEmail']] = $r['ManagerEmail'];
                            }
                        }
                    }
                }
                return $this->Email(
                    $email,
                    'Crowe TimeReport - New Comment in your TimeReport #' . $modelComments->TimeReportID,
                    ['model' => $modelComments]
                );
            }
        }
    }

    public function actionGetattachment()
    {
        $post = Yii::$app->request->post();
        $return = 0;
        if (!empty($post['id'])) {
            $id = $post['id'];
            $task = Task::find()->where(['id' => $id])->one();
            if (!empty($task->id)) {
                $return = $task->is_attachment;
            }
        }

        return $return;
    }
    public function Email($sendto, $subject, $param, $layouts = 'layouts/newcomments')
    {
        Yii::$app->cache->flush();

        $from = Yii::$app->params['appNotificationEmail'];
        $cc = Yii::$app->params['appNotificationEmailCC'];

        $to = $sendto;

        if (!empty($to)) {
            $html = ['html' => $layouts];

            $compose = Yii::$app->mailer->compose($html, $param);
            $compose->setFrom($from);
            $compose->setTo($to);
            if (!empty($cc)) {
                $compose->setCc($cc);
            }
            $compose->setSubject($subject);

            $compose->send();
        }
    }
    public function actionDetaildelete($id)
    {
        $post = Yii::$app->request->post();

        $return = false;
        if (!empty($id)) {
            $model = TimeReportDetail::find()->where(['id' => $id])->one();
            if (!empty($model)) {
                $return = $model->delete();
            }
        }
        echo $return;
    }
    public function actionMealsdelete($id, $seq)
    {
        $post = Yii::$app->request->post();

        $return = false;
        if (!empty($id) && !empty($seq)) {
            $model = TimeReportMeals::find()->where(['TimeReportID' => $id, 'Seq' => $seq])->one();
            if (!empty($model)) {
                $return = $model->delete();
            }
        }
        return $return;
    }
    public function actionOpedelete($id, $seq)
    {
        $post = Yii::$app->request->post();

        $return = false;
        if (!empty($id) && !empty($seq)) {
            $model = TimeReportOutOfOffice::find()->where(['TimeReportID' => $id, 'Seq' => $seq])->one();
            if (!empty($model)) {
                $return = $model->delete();
            }
        }
        return $return;
    }
    public function actionTaxidelete($id, $seq)
    {
        $post = Yii::$app->request->post();

        $return = false;
        if (!empty($id) && !empty($seq)) {
            $model = TimeReportTaxi::find()->where(['TimeReportID' => $id, 'Seq' => $seq])->one();
            if (!empty($model)) {
                $return = $model->delete();
            }
        }
        return $return;
    }
    protected function findModelDetail($id)
    {
        if (($model = TimeReportDetail::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    protected function findModel($EmployeeId, $Date)
    {
        if (($model = TimeReport::findOne(['EmployeeId' => $EmployeeId, 'Date' => $Date])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
