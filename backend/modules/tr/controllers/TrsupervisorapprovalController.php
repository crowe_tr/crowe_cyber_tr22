<?php

namespace backend\modules\tr\controllers;

use Yii;
use common\models\tr\Job;
use common\models\tr\Taxi;
use common\models\tr\TimeReport;
use common\models\tr\TimeReportDetail;
use common\models\tr\TimeReportMeals;
use common\models\tr\TimeReportOutOfOffice;
use common\models\tr\TimeReportTaxi;

use common\models\st\TaskType;
use common\models\st\Task;
use common\models\cm\Taxi as cmTaxi;

use common\models\tr\search\TimeReportSearch;
use common\models\tr\search\TimeReportDetailSearch;
use common\components\CommonHelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class TrsupervisorapprovalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    //========================================================
    public function actionAll(){
        return $this->actionIndex();
    }
    public function IndexFilter(){
		$session = Yii::$app->session;
        $session->open();

		$post = Yii::$app->request->post();
		$model = new TimeReport();
		if($model->load($post)){
            $data = [];
            $date = explode("-",$model->YearDate);

			$data['Year'] = $date[0];
			$data['Month'] = $date[1];
			$data['YearDate'] = $model->YearDate;
			$session->set('__trapproval_index', $data);
		}
	}

    public function actionIndex($Status=null)
    {
        $user = CommonHelper::getUserIndentity();
		$session = Yii::$app->session;
        $data = array();

        $this->IndexFilter();
        
        $model = new TimeReport(); 
		$param = isset($_SESSION['__trapproval_index']) ? $_SESSION['__trapproval_index'] : [];
		$param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;
		$param['Year'] = isset($param['Year']) ? $param['Year'] : date('Y');
		$param['Month'] = isset($param['Month']) ? $param['Month'] : date('m');
		$param['Status'] = isset($Status) ?  $Status : 0;
        $param['YearDate'] = isset($param['YearDate']) ? $param['YearDate'] : date('Y-m-t');
        $param['Date'] = date('Y-m-d');

        $sql_list = "call antTrListApproval1('".$param['EmployeeId']."', '".$param['Year']."', '".$param['Month']."')";
        $data['list'] = Yii::$app->db->createCommand($sql_list)->queryAll();

        return $this->render('index', [
            'param' => $param,
            'data' => $data,
            'model'=>$model 
        ]);
    }

    public function actionDetail($id, $status=null)
    { 
        $user = CommonHelper::getUserIndentity();
		$session = Yii::$app->session;
        $data = array();

        $model = new TimeReport();
		$param = isset($_SESSION['__trapproval_details']) ? $_SESSION['__trapproval_details'] : [];
		$param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;
		$param['Status'] = isset($status) ?  $status : 0;
		$param['Date'] = isset($id) ? $id : date('Y-m-d');

        $sql_list = "call antTrListApproval1Detail('".$param['EmployeeId']."', '".$param['Date']."')";
        $data['list'] = Yii::$app->db->createCommand($sql_list)->queryAll();
        return $this->render('Detail', [
            'param' => $param,
            'data' => $data,
            'model'=>$model 
        ]);
        
    }
    public function actionView($id, $TrDetID)
    {
        $user = CommonHelper::getUserIndentity();
		$session = Yii::$app->session;
        $data = array();
        $param = [];

        $data['Detail'] = Yii::$app->db->createCommand("call antTrItemDetail(".$id.", ".$TrDetID.")")->queryAll();
        $data['Meal'] = Yii::$app->db->createCommand("call antTrItemMeals(".$id.", ".$TrDetID.")")->queryAll();
        $data['OutOffice'] = Yii::$app->db->createCommand("call antTrItemOutOfOffice(".$id.", ".$TrDetID.")")->queryAll();
        $data['Taxi'] = Yii::$app->db->createCommand("call antTrItemTaxi(".$id.", ".$TrDetID.")")->queryAll();
        
        $param['TrID'] = $id;
        $param['TrDetID'] = $TrDetID;

        return $this->renderPartial('DetailListView', [
            'param' => $param,
            'data' => $data,
        ]);
    }

    //========================================================
    public function actionDetailapprovalall($id, $Description){
        $user = CommonHelper::getUserIndentity();
		$session = Yii::$app->session;
        $param = array();

        $param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;

        $sql = "call Approval1UpdateTask('".$param['EmployeeId']."', '".$id."',  '".$Description."')";
        echo $sql;
        $update = Yii::$app->db->createCommand($sql)->queryOne();
        if($update['Valid']==1){
            $return = true;
        }else{
            $return = false;
        }
        return $return;
    }

    public function actionDetaillistapproveall($id, $Description, $TimeReportID, $TrDetID){
        $user = CommonHelper::getUserIndentity();
		$session = Yii::$app->session;
        $param = array();

        $param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;

        $sql = "call Approval1UpdateTaskHour('".$param['EmployeeId']."', '".$id."',  '".$Description."', '".$TimeReportID."', '".$TrDetID."')";
        $update = Yii::$app->db->createCommand($sql)->queryOne();
        if($update['Valid']==1){
            $return = true;
        }else{
            $return = false;
        }
        return $return;
    }

    public function actionDetailapproval($id, $TrDetID, $set){
        $modelDetail = TimeReportDetail::find()->where(['TimeReportID' => $id, 'id' => $TrDetID])->one();
        if(!empty($modelDetail)){
            $modelDetail->Approval1 = $set;
            if($modelDetail->validate()){
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $modelDetail->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($modelDetail, ['encode' => true]);
                    }
    
                    if ($flag) {
                        if($set == 2){
                            $this->ApprovalsEmail($id, $TrDetID, $modelDetail, "REJECTED");
                        }elseif($set == 3){
                            $this->ApprovalsEmail($id, $TrDetID, $modelDetail, "REVISED");
                        }

                        $transaction->commit();
                        $return = true;
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return = "Error : ".$e;
                }   
            }else{
                $return = \yii\helpers\Html::errorSummary($modelDetail, ['encode' => true]);
            }

        }
    }
    public function actionMealapproval($id, $TrDetID, $Seq, $set){
        $modelMeals = TimeReportMeals::find()->where(['TimeReportID' => $id, 'TrDetID' => $TrDetID, 'Seq' => $Seq])->one();
        if(!empty($modelMeals)){
            $modelMeals->Approval1 = $set;
            if($modelMeals->validate()){
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $modelMeals->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($modelMeals, ['encode' => true]);
                    }
    
                    if ($flag) {
                        if($set == 2){
                            $this->ApprovalsEmail($id, $TrDetID, $modelMeals, "REJECTED");
                        }elseif($set == 3){
                            $this->ApprovalsEmail($id, $TrDetID, $modelMeals, "REVISED");
                        }

                        $transaction->commit();
                        $return = true;
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return = "Error : ".$e;
                }   
            }else{
                $return = \yii\helpers\Html::errorSummary($modelMeals, ['encode' => true]);
            }

        }
    }
    public function actionOutofficeapproval($id, $TrDetID, $Seq, $set){
        $modelOutoffice = TimeReportOutOfOffice::find()->where(['TimeReportID' => $id, 'TrDetID' => $TrDetID, 'Seq' => $Seq])->one();
        if(!empty($modelOutoffice)){
            $modelOutoffice->Approval1 = $set;
            if($modelOutoffice->validate()){
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $modelOutoffice->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($modelOutoffice, ['encode' => true]);
                    }
    
                    if ($flag) {
                        if($set == 2){
                            $this->ApprovalsEmail($id, $TrDetID, $modelOutoffice, "REJECTED");
                        }elseif($set == 3){
                            $this->ApprovalsEmail($id, $TrDetID, $modelOutoffice, "REVISED");
                        }


                        $transaction->commit();
                        $return = true;
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return = "Error : ".$e;
                }   
            }else{
                $return = \yii\helpers\Html::errorSummary($modelOutoffice, ['encode' => true]);
            }

        }
    }
    public function actionTaxiapproval($id, $TrDetID, $Seq, $set){
        $modelTaxi = TimeReportTaxi::find()->where(['TimeReportID' => $id, 'TrDetID' => $TrDetID, 'Seq' => $Seq])->one();
        if(!empty($modelTaxi)){
            $modelTaxi->Approval1 = $set;
            if($modelTaxi->validate()){
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $modelTaxi->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($modelTaxi, ['encode' => true]);
                    }
    
                    if ($flag) {
                        if($set == 2){
                            $this->ApprovalsEmail($id, $TrDetID, $modelTaxi, "REJECTED");
                        }elseif($set == 3){
                            $this->ApprovalsEmail($id, $TrDetID, $modelTaxi, "REVISED");
                        }

                        $transaction->commit();
                        $return = true;
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return = "Error : ".$e;
                }   
            }else{
                $return = \yii\helpers\Html::errorSummary($modelTaxi, ['encode' => true]);
            }

        }
    }

    protected function findModel($EmployeeId, $Date)
    {
        if (($model = TimeReport::findOne(['EmployeeId' => $EmployeeId, 'Date' => $Date])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function ApprovalsEmail($TimeReportID, $TrDetID, $model, $label){
        $sql = "CALL antTrCommentsEmailList({$TimeReportID}, {$TrDetID})";
        $lookups = Yii::$app->db->createCommand($sql)->queryAll();

        $email = [];
        if(!empty($lookups)){
            foreach($lookups as $r) {
                if(!empty($r['EmployeeEmail'])){
                    if(filter_var($r['EmployeeEmail'], FILTER_VALIDATE_EMAIL)) {
                        $email[$r['EmployeeEmail']] = $r['EmployeeEmail'];
                    }
                }
            }
        }
        $date = !empty($model->Date) ? $model->Date : $model->timeReport->Date;
        $date = date('l, d F Y', strtotime($date));
        return $this->Email (
            $email, 
            'Crowe TimeReport - Your timereport submission on '.$date.' was '.$label.' #'.$TimeReportID,
            [
                'model'=>$model,
                'label'=>$label
            ]
        );
    }
    public function Email($sendto, $subject, $param, $layouts='layouts/newapprovals'){
        Yii::$app->cache->flush();

        $from = Yii::$app->params['appNotificationEmail'];
        $cc = Yii::$app->params['appNotificationEmailCC'];

        $to = $sendto;
        
        if (!empty($to)) {
            $html = ['html' => $layouts];

            $compose = Yii::$app->mailer->compose($html, $param);
            $compose->setFrom($from);
            $compose->setTo($to);
            if (!empty($cc)) {
                $compose->setCc($cc);
            }
            $compose->setSubject($subject);

            $compose->send();
        }

    }

}
