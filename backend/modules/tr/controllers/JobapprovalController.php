<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\helpers\Html;

use common\components\CommonHelper;
use common\components\TimeReportHelper;

use common\models\tr\Job;
use common\models\tr\JobBudgeting;
use common\models\tr\JobTemp;
use common\models\tr\JobBudgetingTemp;
use common\models\tr\JobComment;
use common\models\tr\ms\Entity;
use common\models\tr\TrPreviousChangesJob;
use common\models\tr\search\JobSearch;
use common\models\tr\search\JobviewSearch;
use common\models\tr\search\JobBudgetingSearch;
use common\models\tr\search\JobBudgetingTempSearch;
use common\models\tr\search\JobCommentSearch;
use common\models\cl\Client;
use common\models\hr\VempGroup;
use common\models\hr\Employee;
use  yii\data\ArrayDataProvider;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class JobapprovalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionAll()
    {
        return $this->index(9);
    }
    public function actionIndex()
    {
        return $this->index(8);
    }
    public function actionDraft()
    {
        return $this->index(8);
    }
    public function actionSubmit()
    {
        return $this->index(Job::StatusSubmit);
    }
    public function actionApproved()
    {
        return $this->index(Job::StatusApproved);
    }
    public function actionOnprogress()
    {
        return $this->index(Job::StatusOnProgress, 'op');
    }
    public function actionDenied()
    {
        return $this->index(Job::StatusDenied);
    }
    public function actionCanceled()
    {
        return $this->index(Job::StatusCanceled);
    }
    public function actionSuccess()
    {
        return $this->index(Job::StatusSuccess);
    }
	public function IndexFilter(){
		$session = Yii::$app->session;
        $session->open();

		$post = Yii::$app->request->post();
		$model = new JobviewSearch();
		if($model->load($post)){
			$data = [];

			$data['JobCode'] = $model->JobCode;
      // $data['Requester'] = $model->Requester;

			$session->set('__trjobapprove_index', $data);
		}
	}
    public function index($status = '',$desc= '')
    {

        $user = CommonHelper::getUserIndentity();
        $post = Yii::$app->request->post();
		    $session = Yii::$app->session;
        $data = array();

		      $this->IndexFilter();

    		$param = isset($_SESSION['__trjobapprove_index']) ? $_SESSION['__trjobapprove_index'] : [];
    		$param['JobCode'] = isset($param['JobCode']) ? $param['JobCode'] : '';

        $searchModel = new JobviewSearch();

    		$searchModel->JobCode = $param['JobCode'];
    		$searchModel->StartDate = $desc;



        $searchModel->Nondraft = 1;

      if ($user->IsAdmin != 1) {
          $searchModel->Partner = $user->Id;
      }

        if ($status != 9) {
            if ($status == 8) {
                $searchModel->Status = 0;
            } else {
                $searchModel->Status = $status;
            }
        }
        else{
            $searchModel->Status = $status;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->post());

        // print_r($dataProvider);



        return $this->render('job', [
            'OrderButtons' => Job::getButtonTotalApprovalFromStatus($status),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
            // 'param' => $param,
        ]);
    }

    public function getDataForForm($model)
    {
        $data = array();
        $data['Division'] = TimeReportHelper::getDivision();
        $data['Entity'] = TimeReportHelper::getEntity();
        $data['Client'] =TimeReportHelper::getClient();
        $data['Employee'] =TimeReportHelper::getEmployee();
        $data['VempGroup'] =TimeReportHelper::getVempGroup();
        $data['EmployeeJob'] =TimeReportHelper::getEmployeeJob();
        return $data;
    }

    public function actionRequest($id = '')
    {

        $user = CommonHelper::getUserIndentity();
        if (empty($id)) {
            $modelJob = new Job();
            $modelJob->CreatedBy = $user->Id ;
            $modelJob->Flag = 0;
            $modelJob->Status = 0;
        } else {
            $modelJob = $this->findModel($id);
            $modelJob->UpdateBy = $user->Id ;
            $modelJob->scenario = 'update';
            $modelJob->JobArea = explode(',', $modelJob->JobArea);

            if ($modelJob->Status != 0  ) {
              if ($modelJob->Status != 3) {
                return $this->redirect(['view', 'id' => $modelJob->JobID]);
              }

            }
        }
        $data = $this->getDataForForm($modelJob);
        $searchModelBudgeting = new JobBudgetingSearch();
        $searchModelBudgeting->JobID = $modelJob->JobID;
        $modelBudgeting = $searchModelBudgeting->search($modelJob->JobID);

        $post = Yii::$app->request->post();
        if ($modelJob->load($post)) {
            $valid = $modelJob->validate();
            if ($valid) {
                if (Yii::$app->request->post('submit') == 0) {
                    $modelJob->Status = 0 ;
                }
                elseif (Yii::$app->request->post('submit') == 1) {
                    $modelJob->Status = 1 ;
                    // $modelJob->Flag = 1 ;
                }


                $modelJob->Fee = empty($modelJob->Fee) ? 0 : str_replace(',', '', $modelJob->Fee);
                $modelJob->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
                $modelJob->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
                $modelJob->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
                $modelJob->OtherExpenseAllowance = empty($modelJob->OtherExpenseAllowance) ? 0 : str_replace(',', '', $modelJob->OtherExpenseAllowance);
                $modelJob->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);

                $modelJob->JobArea = empty($modelJob->JobArea) ? '' : implode(',', $modelJob->JobArea);

                $modelJob->Total =  $modelJob->Fee;
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelJob->save(false)) {

                    } else {
                        $transaction->rollBack();
                    }

                    if ($flag) {
                        $transaction->commit();
                        if($modelJob->isNewRecord){
                            return $this->redirect(['request', 'id' => $modelJob->JobID]);
                        }else{
                            return $this->redirect(['view', 'id' => $modelJob->JobID]);
                        }
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }
        return $this->render('jobForm', [
             'data' => $data,
             'modelJob' => $modelJob,
             'modelBudgeting' => $modelBudgeting,
         ]);

    }
    public function actionView($id, $comment = '', $frommail = 0)
    {
        if ($frommail == 1) {
            return $this->frommail($id, $comment = '');
        } else {
            return $this->view($id, $comment = '');
        }
    }
    public function view($id, $comment = '')
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $modelJob = $this->findModel($id);
        $modelComments = new JobComment();
        $modelComments->JobID = $id;

        // $param['isCurrentApprover'] = ($modelJob->Partner == $user->EmployeeID) ? true : false;
        // $param['isRequester'] = ($modelJob->Manager == $user->EmployeeID) ? true : false;
        //$param['budget'] = Job::countTotalBudget($modelJob);

        $searchModelBudget = new JobBudgetingSearch([
                            'JobID' => $id,
                        ]);
        $dataProviderBudget = $searchModelBudget->search(Yii::$app->request->queryParams);

        $count = JobComment::find()
          ->select(['COUNT(*) AS jml'])
          ->where(['JobID' => $id])
          ->andWhere(['like','ListEmployee' , $user->Id])
          ->count();
            $commentCount['count'] = $count;

        if (!empty($id)) {
          // code...
          // $sql = "call JobEstimated('".$id."')";
          // $task = Yii::$app->db->createCommand($sql)->queryOne();
          // $modelTask['TimeCharges'] =  $task['TimeCharges'];
          // $modelTask['RecoveryRate'] =  $task['RecoveryRate'];
          // $modelTask['MealAllowance'] =  $task['MealAllowance'];
          // $modelTask['TaxiAllowance'] =  $task['TaxiAllowance'];
          // $modelTask['OutOfOfficeAllowance'] =  $task['OutOfOfficeAllowance'];
          // $modelTask['TimeCharges'] =  0;
          $modelTask['RecoveryRate'] =  0;
          $modelTask['MealAllowance'] =  0;
          $modelTask['TaxiAllowance'] =  0;
          $modelTask['OutOfOfficeAllowance'] =  0;
        }
        else{
          // $modelTask['TimeCharges'] =  0;
          $modelTask['RecoveryRate'] =  0;
          // $modelTask['OutOfOfficeAllowance'] =  0;
          // $modelTask['TaxiAllowance'] =  0;
          // $modelTask['OutOfOfficeAllowance'] =  0;

        }

        return $this->render('view', [
            'modelJob' => $modelJob,
            'modelComments' => $modelComments,
            'searchModelBudget' => $searchModelBudget,
            'dataProviderBudget' => $dataProviderBudget,
            'modelTask' => $modelTask,
            'commentCount' => $commentCount,
            // 'param' => $param,
        ]);
    }
    public function actionBudget($id = null, $action=null)
    {
        $session = Yii::$app->session;
             $session->open();

       	$post = Yii::$app->request->post();
        $model = new Job();
        $model->load($post);
       	$modelJob = Job::find()->where(['JobID'=> $model->JobID])->one();
        if ($modelJob->load($post))
        {
          $session->set('_trjob_form'.$modelJob->JobID, $modelJob);
            if (Yii::$app->request->isAjax)
            {
              if (!empty($id))
              {
                $modelBudget = $this->findModelBudget($id, $modelJob->JobID);
              }
              else{
                $modelBudget = new JobBudgeting();
                $modelBudget->JobID = $modelJob->JobID;
                if (empty($action)) {
                  // $modelBudget->EmployeeID = null;
                  $modelBudget->isNewRecord;
                }
              }
              $data = $this->getDataForForm($modelJob);
              return $this->renderAjax('jobFormDetail', [
                'modelJob' => $modelJob,
                'modelBudget' => $modelBudget,
                'id' => empty($action) ? $id : "",
                'data' => $data
              ]);
            }
            else
            {
              throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
            }

        }
    }
    // public function actionBudget($id = null, $action=null)
    // {
    //   // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //   $session = Yii::$app->session;
    //       $session->open();
    //
  	// 	$post = Yii::$app->request->post();
  	// 	$modelJob = Job::find()->where(['JobID'=> 13])->one();
  	// 	if($modelJob->load($post)){
  	// 		$session->set('_trjob_form', $modelJob);
    //     if (Yii::$app->request->isAjax)
    //     {
    //       if (!empty($id)) {
    //          $modelBudget = $this->findModelBudget($id, $modelJob->JobID);
    //       }
    //       else{
    //         $modelBudget = new JobBudgeting();
    //             $modelBudget->JobID = $modelJob->JobID;
    //             if (empty($action)) {
    //               // $modelBudget->EmployeeID = null;
    //               $modelBudget->isNewRecord;
    //             }
    //       }
    //       $data = $this->getDataForForm($modelJob);
    //       return $this->renderAjax('jobFormDetail', [
    //            'modelJob' => $modelJob,
    //            'modelBudget' => $modelBudget,
    //            'id' => empty($action) ? $id : "",
    //            'data' => $data
    //          ]);
    //     }
    //      else
    //      {
    //        throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
    //      }
    //
    //     // if (Yii::$app->request->isAjax) {
    //     //   $post = Yii::$app->request->post();
    //     //   $user = CommonHelper::getUserIndentity();
    //     //
    //     //   $modelJob = new Job();
    //     //   $modelJob->load($post);
    //     //   if (!empty($id)) {
    //     //     $modelBudget = $this->findModelBudget($id, $modelJob->JobID);
    //     //   } else {
    //     //     $modelBudget = new JobBudgeting();
    //     //     $modelBudget->JobID = $modelJob->JobID;
    //     //     if (empty($action)) {
    //     //       // $modelBudget->EmployeeID = null;
    //     //       $modelBudget->isNewRecord;
    //     //     }
    //     //   }
    //     //
    //     //   $data = $this->getDataForForm($modelJob);
    //     //
    //     //   return $this->renderAjax('jobFormDetail', [
    //     //     'modelJob' => $modelJob,
    //     //     'modelBudget' => $modelBudget,
    //     //     'id' => empty($action) ? $id : "",
    //     //     'data' => $data
    //     //   ]);
    //     // } else {
    //     //   throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
    //     // }
  	// 	// }
    //
    // }
    public function actionBudgetsave($id = null)
    {
        // $return = false;
        // $post = Yii::$app->request->post();
        //
        // $session = Yii::$app->session;
        // $session->open();
        // $modelBudget = new JobBudgeting();
        // // $modalBudget = $post['JobBudgeting'];
        //
        // if ($modelBudget->load($post))
        // {
        //   $a[] =  $modelBudget;
        //
        //   if (isset($_SESSION['_trjob_budget_form'.$modelBudget->JobID])) {
        //     // code...
        //     foreach ($_SESSION['_trjob_budget_form'.$modelBudget->JobID] as $key1 => $val) {
        //       array_push($a, $val);
        //     }
        //     $session->set('_trjob_budget_form'.$modelBudget->JobID,  $a);
        //   }
        //   else{
        //     $session->set('_trjob_budget_form'.$modelBudget->JobID,  $a);
        //   }
        //
        //   var_dump($modelBudget);
        //
        // }

        //
        // $a[] =  $modalBudget;
        //
        // if (isset($_SESSION['_trjob_budget_form'.$modalBudget['JobID']])) {
        //   // code...
        //   foreach ($_SESSION['_trjob_budget_form'.$modalBudget['JobID']] as $key1 => $val) {
        //     array_push($a, $val);
        //   }
        //   $session->set('_trjob_budget_form'.$modalBudget['JobID'],  $a);
        // }
        // else{
        //   $session->set('_trjob_budget_form'.$modalBudget['JobID'],  $a);
        // }

        // $b =[] ;
        // array_push($a,$s);

        // $se = $_SESSION['_trjob_budget_form'.$modalBudget['JobID']];
        // foreach ($se as $key => $value) {
        //   array_push($a, $value);
        // }

        // var_dump($a);
        // var_dump($se);

        // $a['modal'] =$modalBudget ;
        // if (isset($_SESSION['_trjob_budget_form'.$modalBudget['JobID']])) {
        //   // code...
        //   $se = $_SESSION['_trjob_budget_form'.$modalBudget['JobID']];
        //   // $a = array($modalBudget, $se);
        //   array_push($a, $se);
        //
        //
        //   $session->set('_trjob_budget_form'.$modalBudget['JobID'],  $a);
        //
        //   var_dump($se);
        // }
        //
        // else{
        //   // $se = ;
        //   array_push($a, $modalBudget);
        //   $session->set('_trjob_budget_form'.$modalBudget['JobID'], $a );
        //   var_dump($_SESSION['_trjob_budget_form'.$modalBudget['JobID']]);
        // }



        // var_dump($_SESSION['_trjob_budget_form'.$modalBudget['JobID']]);

        // var_dump($a);


        // $modalBudget = $_SESSION['_trjob_budget_form'.$modalBudget['JobID']];
        // var_dump($modalBudget);
        // $session = $_SESSION['_trjob_budget_form'.$modalBudget['JobID']];
        //
        // $array[]['model'] = $modalBudget;
        // // $array[]['model'] = $modalBudget;
        // foreach($session as $a)
        // {
        //   // var_dump($a);
        //   array_push($array , $a);
        //
        //   // var_dump($a);
        //   // var_dump($a);
        // }
        // $session->set('_trjob_budget_form'.$modalBudget['JobID'],  $array);
        // var_dump($modalBudget);

        // var_dump($_SESSION['_trjob_budget_form'.$modalBudget['JobID']]);
        // $modelBudget = new JobBudgeting();
        // if ($modelBudget->load($post)){
        // }
        // var_dump( $_SESSION['_trjob_budget_form'.$post->JobID] );

        // var_dump($post);
        // $modelBudget = new JobBudgeting();
        // if ($modelBudget->load($post)){
        // }


          // if ($modelBudget->load($post))
          // {
          //   if (isset($_SESSION['_trjob_budget_form'.$modelBudget->JobID]) ? $_SESSION['_trjob_budget_form'.$modelBudget->JobID] : [])
          //   {
          //       $modelBudgetArray = $_SESSION['_trjob_budget_form'.$modelBudget->JobID] ;
          //
          //
          //       $modelBudgetArray[$modelBudget->EmployeeID]['model'] = $modelBudget;
          //
          //       $session->set('_trjob_budget_form'.$modelBudget->JobID, $modelBudgetArray);
          //
          //       var_dump($_SESSION['_trjob_budget_form'.$modelBudget->JobID]);
          //
          //
          //
          //   }
          //   else {
          //     $session->set('_trjob_budget_form'.$modelBudget->JobID, $modelBudget);
          //   }
          //
          //
          //
          // }


          // var_dump(array('session' => $modelBudgetArray));
        $user = CommonHelper::getUserIndentity();

        $post = Yii::$app->request->post();
        $modelBudget = new JobBudgeting();
        if ($modelBudget->load($post)) {


            if(!empty($id)) {
                $modelBudget = $this->findModelBudget($id, $modelBudget->JobID);
                $modelBudget->load($post);

                // print_r($modelBudget->OverTime);
            }
            $valid = $modelBudget->validate();
            if ($valid) {
              $modelBudget->CreatedBy = $user->Id;
              if ($modelBudget->OverTime == null) {
                $modelBudget->OverTime = 0;
              }
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $modelBudget->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    }
                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $return = 'error : validation not valid'.Html::errorSummary($modelBudget, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }
    public function actionBudgetdelete()
    {
        $return = false;
        $post = Yii::$app->request->post();

        $EmployeeID = $post['EmployeeID'];
        $JobID = $post['JobID'];
        $modelBudget = $this->findModelBudget($EmployeeID, $JobID);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($modelBudget->delete()) {
                $transaction->commit();
                $return = true;
            } else {
                $transaction->rollback();
            }
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        return $return;
    }
    public function actionOtherexpense($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();

            $modelJob = new Job();
            $modelJob->load($post);

            return $this->renderAjax('jobOthersExpense', [
                'modelJob' => $modelJob,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionComments($id = '')
    {
        $user = CommonHelper::getUserIndentity();
        $modelComments = new JobComment();
        $post = Yii::$app->request->post();
        $modelComments->load($post);

        //Start read comment
        $count = JobComment::find()
          ->select(['COUNT(*) AS jml'])
          ->where(['JobID' => $id])
          ->andWhere(['like','ListEmployee' , $user->Id])
          ->count();
          if ($count != 0) {
            $modelCommentsRead = JobComment::find()
                                            ->where(['JobID' => $id])
                                            ->andWhere(['like','ListEmployee' , $user->Id])->one();
            foreach (explode(',' ,$modelCommentsRead->ListEmployee) as $key => $value) {
              if ($value != $user->Id) {
                $data[] = $value;
              }
            }
            $readcomment = implode(', ', $data);
            $modelCommentsRead->ListEmployee =$readcomment;
            Yii::$app->db->createCommand()
                 ->update('trJobComment', ['ListEmployee' => $readcomment], 'JobID = '.$id.'')
                 ->execute();
          }
        //End read comment

        $searchmodelComments = new JobCommentSearch([
          'JobID' => !empty($id) ? $id : $modelComments->JobID,
        ]);
        $dataProviderComments = $searchmodelComments->search(Yii::$app->request->queryParams);

        return $this->renderAjax('viewComments', [
          'dataProviderComments' => $dataProviderComments,
          'JobID' => $modelComments->JobID,
        ]);
    }
    public function actionCommentsave()
    {
        $post = Yii::$app->request->post();
        $html = '';
        $modelComments = new JobComment();
        if ($modelComments->load($post)) {
            $user = CommonHelper::getUserIndentity();

            $modelComments->CreatedBy = $user->Id;
            if ($modelComments->save()) {

                $html = '';
            }
        }
        return $html;
    }

    protected function findModel($id)
    {
        if (($model = Job::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelTemp($id)
    {
        if (($model = JobTemp::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findModelBudget($EmployeeID, $JobID, $param = [])
    {
        if (($modelBudget = JobBudgeting::find()->where(['EmployeeID' => $EmployeeID, 'JobID' => $JobID])->one()) !== null) {
            return $modelBudget;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelEmployee($JobID, $param = [])
    {
        if (($modelBudget = JobBudgeting::find()->where(['JobID' => $JobID])->all()) !== null) {
            return $modelBudget;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModelEmployeeRevisi($JobID, $param = [])
    {
        if (($modelBudget = JobBudgetingTemp::find()->where(['JobID' => $JobID])->all()) !== null) {
            return $modelBudget;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelBudgetTemp($EmployeeID, $JobID, $param = [])
    {
        if (($modelBudget = JobBudgetingTemp::find()->where(['EmployeeID' => $EmployeeID, 'JobID' => $JobID])->one()) !== null) {
            return $modelBudget;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelHistoryJob($JobID)
    {
      if ($model = TrPreviousChangesJob::find()->where(['JobID' => $JobID])->all()) {
        return $model;
      }
      else {
          throw new NotFoundHttpException('The requested page does not exist.');
      }
    }

    public function actionLatesjob()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();

      $modelJob = new Job();
      $modelJob->load($post);

      $result = \Yii::$app->db->createCommand("CALL getJobNo(:paramName1)")
                      ->bindValue(':paramName1' , $modelJob->ClientID );
      $data['job'] = $result->queryScalar();
      $data['ClientID'] = $modelJob->ClientID;
      // var_dump($modelJob->ClientID);

      return $data;
    }

    public function actionEmployeedata()
    {
      $post = Yii::$app->request->post();

      // $modelJob = new Job();
      // $modelJob->load($post);
      //
      // $result = \Yii::$app->db->createCommand("CALL getJobNo(:paramName1)")
      //                 ->bindValue(':paramName1' , $modelJob->ClientID );
      // $job = $result->queryScalar();

      return "ha";
    }

    public function actionGrupdata()
    {
      $out = [];
      if (isset($_POST['depdrop_parents'])) {

          $id = $_POST['depdrop_parents'];

            $list = VempGroup::find()->orWhere(['or' ,['entityID' => $id[0]] , ['divisionID' => $id[1]]])->asArray()->all();
            $listall = VempGroup::find()->asArray()->all();

            if ($id != null && count($list) != 0) {
              $selected = '';
              foreach ($list as $i => $ls) {
                $out[] = ['id' => $ls['id'], 'name' => $ls['id'] .' - '. $ls['fullName'].' - '.$ls['levelName']];
              }
              echo json_encode(['output' => $out, 'selected' => $selected]);
            }
            else{
              if ($id[0] == null AND $id[1] == null) {
                $selected = '';
                foreach ($listall as $i => $ls) {
                  $out[] = ['id' => $ls['id'], 'name' => $ls['id'] .' - '. $ls['fullName'].' - '.$ls['levelName']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);
              }
              else{
                echo json_encode(['output' => array(), 'selected' => '']);
              }
            }
      }
    }

    public function actionTotalwh()
    {
      $post = Yii::$app->request->post();
      $modelJob = new JobBudgeting();
      $modelJob->load($post);

      $sql = "call getValueTaskAE('".$modelJob->EmployeeID."','".$modelJob->Total."')";
      $task = Yii::$app->db->createCommand($sql)->queryOne();

      return json_encode($task);
    }

    public function actionGetlevelid()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob = new JobBudgeting();
      $modelJob->load($post);

      $data['fullName'] =$modelJob->employee->fullName;
      $data['levelName'] = $modelJob->employee->level->levelName;

      return $data;
    }

    public function actionOutofficeallowance()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob = new Job();
      $modelJob->load($post);

      $model = Job::find()->where(['JobID' => $modelJob->JobID])->one();
      $model->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
      $model->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
      $model->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
      $model->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);
      $model->OtherExpenseAllowance = empty($modelJob->OtherExpenseAllowance) ? 0 : str_replace(',', '', $modelJob->OtherExpenseAllowance);

      // print_r($model->OutOfOfficeAllowance);

      if ($model->save(false)) {

        $modelAllow = $this->findModel($model->JobID);
        // print_r($model);
        // $sql = "call JobEstimated('".$modelJob->JobID."')";
        // $task = Yii::$app->db->createCommand($sql)->queryOne();
        //
        //
        // $data['Ope'] = $model->MealAllowance;
        // $data['Taxi'] = $model->TaxiAllowance;
        // $data['Ot'] = $model->OutOfOfficeAllowance;
        // $data['rt'] = $model->Percentage;
        // $data['Recovery'] = "20%";
        return $modelAllow;
        // code...
      }

    }

    public function actionApprove($id = "")
    {
      if (!empty($id)) {
        $modelJob = $this->findModel($id);
        $modelJob->Status = 2;
        if ($modelJob->save(false)) {
          $modelBudget = $this->findModelEmployee($id);
          // $send_partner = $this->EmailPartner($modelJob->partner->email, 'YOU HAVE APPROVED JOB ' ,$modelJob);
          // if ($send_partner == true) {
          try {
            $send_manager =  $this->EmailManager($modelJob->manager->email, 'YOUR NEW JOB HAS BEEN APPROVED ' ,$modelJob);
            if ($send_manager == true) {
              foreach ($modelBudget as $key => $value) {
                if ($modelJob->Manager != $value->EmployeeID AND $modelJob->Partner != $value->EmployeeID) {
                  $send_all =  $this->EmailToAll($value, 'YOU HAVE A NEW JOB ' ,$modelJob);
                  if ($send_all == true) {
                    return $this->redirect(['view', 'id' => $modelJob->JobID]);
                  }
                  else{
                    return $this->redirect(['view', 'id' => $modelJob->JobID]);
                  }
                }
              }
            }
          } catch (\Exception $e) {
            return $this->redirect(['view', 'id' => $modelJob->JobID]);
          }

          // }
        }
      }
    }

    public function actionReject($id = "")
    {
      if (!empty($id)) {
        $modelJob = $this->findModel($id);
        $modelJob->Status = 3;
        $modelBudget = $this->findModelEmployee($id);
        if ($modelJob->save(false)) {
          try {
            $send_email = $this->EmailManager($modelJob->manager->email, 'THERE IS A REJECTED JOB ' ,$modelJob);
            // var_dump($send_email);
            if ($send_email == true) {
              return $this->redirect(['view', 'id' => $modelJob->JobID]);
            }
          } catch (\Exception $e) {
              return $this->redirect(['view', 'id' => $modelJob->JobID]);
          }
        }
      }
    }





    public function actionJobtemp($id = "")
    {

    }

    public function actionBack($id = "")
    {
      return $this->redirect('all');
    }




    public function actionReqsave()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob = new Job();
      if ($modelJob->load($post)) {
          $model = $this->findModel($modelJob->JobID);
          if ($model->load($post)) {
            $model->Fee = empty($modelJob->Fee) ? 0 : str_replace(',', '', $modelJob->Fee);
            $model->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
            $model->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
            $model->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
            $model->JobArea = empty($modelJob->JobArea) ? [] : implode(',', $modelJob->JobArea);
            if ($model->save(false)) {
              return "save";
            }
          }
      }
    }
    public function actionReqsaveallowance()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob = new Job();
      if ($modelJob->load($post)) {
          $model = $this->findModel($modelJob->JobID);
          $model->IsMeal = $modelJob->IsMeal;
          $model->IsTaxi = $modelJob->IsTaxi;
          $model->IsOutOfOffice = $modelJob->IsOutOfOffice;
          if ($modelJob->IsMeal != 1) {
            $model->MealAllowance = 0;
          }
          if ($modelJob->IsTaxi != 1) {
            $model->TaxiAllowance = 0;
          }
          if ($modelJob->IsOutOfOffice != 1) {
            $model->OutOfOfficeAllowance =0;
          }

          if ($model->save(false)) {
            return $model;
          }
      }
    }

    public function actionAllowance()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob= new Job();
      if ($modelJob->load($post))
      {
        // return $modelJob->IsTaxi;
        $model = $this->findModel($modelJob->JobID);
        if ($modelJob->IsMeal != 1)
        {
          $model->MealAllowance = 0;
        }
        if($modelJob->IsOutOfOffice != 1)
        {
          $model->OutOfOfficeAllowance = 0;
        }
        if($modelJob->IsTaxi != 1)
        {
          $model->TaxiAllowance = 0;
        }

        if ($model->save(true)) {
          return $model;
        }
      }
    }

    public function actionCalculate()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob= new Job();
      if ($modelJob->load($post)) {
        // code...
        $sql = "call JobEstimated('".$modelJob->JobID."', '1' , '0')";
        $task = Yii::$app->db->createCommand($sql)->queryOne();

        // if ($task) {
        $model = $this->findModel($modelJob->JobID);

        return $model->JobID;
        // }

      }

    }

    public function actionDefault()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob= new Job();
      if ($modelJob->load($post)) {
        // code...
        $sql = "call JobEstimated('".$modelJob->JobID."', '1' , '0')";
        $task = Yii::$app->db->createCommand($sql)->queryOne();

        if ($task) {

          $model = $this->findModel($modelJob->JobID);
          return $model;
        }
        // return $task;
        // if ($modelJob->JobID) {
        //   // code...
        // }

      }


    }

    public function actionRevisi($id = "")
    {
      $model = $this->findModel($id);
      $model->Flag = 1;
      if ($model->save(false)) {
        // code...
        $user = CommonHelper::getUserIndentity();
        if (empty($id)) {
          $modelJob = new JobTemp();
          $modelJob->CreatedBy = $user->Id ;
          $modelJob->Flag = 0;
          $modelJob->Status = 0;
        } else {
          $modelJob = $this->findModelTemp($id);
          $modelJob->UpdateBy = $user->Id ;
          $modelJob->scenario = 'update';
          $modelJob->JobArea = explode(',', $modelJob->JobArea);

          if ($modelJob->Status != 2  ) {
            return $this->redirect(['view', 'id' => $modelJob->JobID]);

          }
        }
        $data = $this->getDataForForm($modelJob);
        $searchModelBudgeting = new JobBudgetingTempSearch();
        $searchModelBudgeting->JobID = $modelJob->JobID;
        $modelBudgeting = $searchModelBudgeting->search($modelJob->JobID);

        $post = Yii::$app->request->post();
        if ($modelJob->load($post)) {
          $valid = $modelJob->validate();
          if ($valid) {
            if (Yii::$app->request->post('submit') == 0) {
              $modelJob->Status = 0 ;
            }
            elseif (Yii::$app->request->post('submit') == 1) {
              $modelJob->Status = 1 ;
              // $modelJob->Flag = 1 ;
            }


            $modelJob->Fee = empty($modelJob->Fee) ? 0 : str_replace(',', '', $modelJob->Fee);
            $modelJob->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
            $modelJob->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
            $modelJob->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
            $modelJob->OtherExpenseAllowance = empty($modelJob->OtherExpenseAllowance) ? 0 : str_replace(',', '', $modelJob->OtherExpenseAllowance);
            $modelJob->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);


            $modelJob->JobArea = empty($modelJob->JobArea) ? [] : implode(',', $modelJob->JobArea);
            $modelJob->Total =  $modelJob->Fee;
            $transaction = \Yii::$app->db->beginTransaction();
            try {
              if ($flag = $modelJob->save(false)) {

              } else {
                $transaction->rollBack();
              }

              if ($flag) {
                $transaction->commit();
                if($modelJob->isNewRecord){
                  return $this->redirect(['request', 'id' => $modelJob->JobID]);
                }else{
                  return $this->redirect(['view', 'id' => $modelJob->JobID]);
                }
              }
            } catch (Exception $e) {
              $transaction->rollBack();
            }
          }
        }
        return $this->render('jobFormTemp', [
        'data' => $data,
        'modelJob' => $modelJob,
        'modelBudgeting' => $modelBudgeting,
        ]);
      }


    }

    public function actionBudgettemp($id = null, $action=null)
    {
        $session = Yii::$app->session;
             $session->open();

        $post = Yii::$app->request->post();
        $model = new JobTemp();
        $model->load($post);
        $modelJob = JobTemp::find()->where(['JobID'=> $model->JobID])->one();
        if ($modelJob->load($post))
        {
          $session->set('_trjob_form'.$modelJob->JobID, $modelJob);
            if (Yii::$app->request->isAjax)
            {
              if (!empty($id))
              {
                $modelBudget = $this->findModelBudget($id, $modelJob->JobID);
              }
              else{
                $modelBudget = new JobBudgetingTemp();
                $modelBudget->JobID = $modelJob->JobID;
                if (empty($action)) {
                  // $modelBudget->EmployeeID = null;
                  $modelBudget->isNewRecord;
                }
              }
              $data = $this->getDataForForm($modelJob);
              return $this->renderAjax('jobFormDetailTemp', [
                'modelJob' => $modelJob,
                'modelBudget' => $modelBudget,
                'id' => empty($action) ? $id : "",
                'data' => $data
              ]);
            }
            else
            {
              throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
            }

        }
    }

    public function actionBudgetsavetemp($id = null)
    {
        $user = CommonHelper::getUserIndentity();

        $post = Yii::$app->request->post();
        $modelBudget = new JobBudgetingTemp();
        if ($modelBudget->load($post)) {


            if(!empty($id)) {
                $modelBudget = $this->findModelBudgetTemp($id, $modelBudget->JobID);
                $modelBudget->load($post);

                // print_r($modelBudget->OverTime);
            }
            $valid = $modelBudget->validate();
            if ($valid) {
              $modelBudget->CreatedBy = $user->Id;
              if ($modelBudget->OverTime == null) {
                $modelBudget->OverTime = 0;
              }
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $modelBudget->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    }
                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $return = 'error : validation not valid'.Html::errorSummary($modelBudget, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }

    public function actionReqsavetemp()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob = new JobTemp();
      if ($modelJob->load($post)) {

          $model = $this->findModelTemp($modelJob->JobID);
          if ($model->load($post)) {
            $model->Fee = empty($modelJob->Fee) ? 0 : str_replace(',', '', $modelJob->Fee);
            $model->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
            $model->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
            $model->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
            $model->JobArea = empty($modelJob->JobArea) ? [] : implode(',', $modelJob->JobArea);
            if ($model->save(false)) {
              return "save";
            }
          }
      }
    }

    public function actionReqsaveallowancetemp()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob = new JobTemp();
      if ($modelJob->load($post)) {
          $model = $this->findModelTemp($modelJob->JobID);
          $model->IsMeal = $modelJob->IsMeal;
          $model->IsTaxi = $modelJob->IsTaxi;
          $model->IsOutOfOffice = $modelJob->IsOutOfOffice;
          if ($modelJob->IsMeal != 1) {
            $model->MealAllowance = 0;
          }
          if ($modelJob->IsTaxi != 1) {
            $model->TaxiAllowance = 0;
          }
          if ($modelJob->IsOutOfOffice != 1) {
            $model->OutOfOfficeAllowance =0;
          }

          if ($model->save(false)) {
            return $model;
          }
      }
    }

    public function actionGetlevelidtemp()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $post = Yii::$app->request->post();
      $modelJob = new JobBudgetingTemp();
      $modelJob->load($post);

      $data['fullName'] =$modelJob->employee->fullName;
      $data['levelName'] = $modelJob->employee->level->levelName;

      return $data;
    }

    public function actionTotalwhtemp()
    {
      $post = Yii::$app->request->post();
      $modelJob = new JobBudgetingTemp();
      $modelJob->load($post);

      $sql = "call getValueTaskAE('".$modelJob->EmployeeID."','".$modelJob->Total."')";
      $task = Yii::$app->db->createCommand($sql)->queryOne();

      return json_encode($task);
    }

    public function actionRejectrevisi($id = "")
    {
        $model = $this->findModel($id);
        $model->Flag = 1;
        if ($model->save()) {
          $modelRevisi = $this->findModelTemp($id);
          $modelRevisi->Flag = 1;
          if ($modelRevisi->save()) {
            $modelBudget = $this->findModelEmployee($id);
            try {
              $send_manager =  $this->EmailManager($modelRevisi->manager->email, 'THERE IS A REJECTED REVISION JOB ' ,$modelRevisi);
              if ($send_manager == true) {
                foreach ($modelBudget as $key => $value) {
                  if ($modelRevisi->Manager != $value->EmployeeID AND $modelRevisi->Partner != $value->EmployeeID) {
                    $send_all =  $this->EmailToAll($value, 'YOU HAVE A NEW REJECTED REVISION JOB ' ,$modelRevisi);
                    if ($send_all == true) {
                      return $this->redirect(['view', 'id' => $modelRevisi->JobID]);
                    }
                  }
                }
              }

            } catch (\Exception $e) {
                return $this->redirect(['view', 'id' => $modelRevisi->JobID]);
            }

          }

        }
    }

    public function actionViewrevisi($id = "")
    {
      $post = Yii::$app->request->post();
      $user = CommonHelper::getUserIndentity();

      $modelJob = $this->findModelTemp($id);
      $modelComments = new JobComment();
      $modelComments->JobID = $id;

      // $param['isCurrentApprover'] = ($modelJob->Partner == $user->EmployeeID) ? true : false;
      // $param['isRequester'] = ($modelJob->Manager == $user->EmployeeID) ? true : false;
      //$param['budget'] = Job::countTotalBudget($modelJob);

      $searchModelBudget = new JobBudgetingTempSearch([
                          'JobID' => $id,
                      ]);
      $dataProviderBudget = $searchModelBudget->search(Yii::$app->request->queryParams);
      $count = JobComment::find()
        ->select(['COUNT(*) AS jml'])
        ->where(['JobID' => $id])
        ->andWhere(['like','ListEmployee' , $user->Id])
        ->count();
          $commentCount['count'] = $count;
          $pcr = 0;
          if (!empty($id))
          {
            $sql = "SELECT * FROM AuditTrail where TransID = '$id' order by ID desc LIMIT 1";
            $auditID = Yii::$app->db->createCommand($sql)->queryOne();
            if ($auditID != false)
            {
              $sqlp = "call AuditJsonAll('".$auditID['ID']."')";
              $pAudit = Yii::$app->db->createCommand($sqlp)->queryOne();
              $modelRevisi['vDescAll'] =  $pAudit;
              $pc = "SELECT COUNT(*) FROM trPreviousChangesJob where JobID = '$id' group by JobID";
              $pcr = Yii::$app->db->createCommand($pc)->queryOne();
            }
            else{
              $modelRevisi['vDescAll'] =  false;
            }
          }
          else{
            $modelRevisi['vDescAll'] =  "NOT DESCRIPTION";
          }

      return $this->render('viewrevisi', [
          'modelJob' => $modelJob,
          'modelComments' => $modelComments,
          'searchModelBudget' => $searchModelBudget,
          'dataProviderBudget' => $dataProviderBudget,
          'modelRevisi' => $modelRevisi,
          'commentCount' =>   $commentCount,
          'countHistory' =>   $pcr,
          // 'param' => $param,
      ]);
    }

    public function actionHistoryrevisi($id)
    {
      $modelJob = $this->findModelTemp($id);
      $model = $this->findModelHistoryJob($id);
      $data = [];
      foreach ($model as $key => $value) {
        array_push($data, $value['PreviousChanges']);
      }



      return $this->render('historyrevisi', [
          'model' => $data,
          'modelJob' => $modelJob,
      ]);


    }




    protected function updateJobRevisi($id)
    {
      // var_dump($id);
      $model = $this->findModelTemp($id);
      $modelJob = $this->findModel($model->JobID);
      $modelJob->Description = $model->Description;
      $modelJob->JobArea = $model->JobArea;
      $modelJob->StartDate = $model->StartDate;
      $modelJob->EndDate = $model->EndDate;
      $modelJob->Partner = $model->Partner;
      $modelJob->Manager = $model->Manager;
      $modelJob->Supervisor = $model->Supervisor;
      $modelJob->IncludeOPE = $model->IncludeOPE;
      $modelJob->IsMeal = $model->IsMeal;
      $modelJob->IsOutOfOffice = $model->IsOutOfOffice;
      $modelJob->IsTaxi = $model->IsTaxi;
      $modelJob->MealAllowance = $model->MealAllowance;
      $modelJob->OutOfOfficeAllowance = $model->OutOfOfficeAllowance;
      $modelJob->AdministrativeCharge = $model->AdministrativeCharge;
      $modelJob->OtherExpenseAllowance = $model->OtherExpenseAllowance;
      $modelJob->TaxiAllowance = $model->TaxiAllowance;
      $modelJob->Fee = $model->Fee;
      $modelJob->Flag = 0;
      return $modelJob->save();
    }

    protected function updateJobBudgetRevisi($id)
    {

      $modelJobBudget = JobBudgetingTemp::find()->where(['JobID' => $id])->all();
      foreach($modelJobBudget as $mb)
      {
        $modelJobBg = JobBudgeting::find()->where(['JobID' => $mb->JobID ])->andWhere(['EmployeeID' => $mb->EmployeeID ])->one();
        if (empty($modelJobBg)) {
          $modelJobBg = new JobBudgeting;
          $modelJobBg->JobID = $mb->JobID;
          $modelJobBg->EmployeeID = $mb->EmployeeID;
          $modelJobBg->Planning = $mb->Planning;
          $modelJobBg->FieldWork = $mb->FieldWork;
          $modelJobBg->Reporting = $mb->Reporting;
          $modelJobBg->WrapUp = $mb->WrapUp;
          $modelJobBg->OverTime = $mb->OverTime;
          $modelJobBg->Total = $mb->Total;
          $modelJobBg->UpdateBy = $mb->UpdateBy;
          $modelJobBg->UpdateAt = $mb->UpdateAt;
          $modelJobBg->save();
        }

        else{
          $modelJobBg->JobID = $mb->JobID;
          $modelJobBg->EmployeeID = $mb->EmployeeID;
          $modelJobBg->Planning = $mb->Planning;
          $modelJobBg->FieldWork = $mb->FieldWork;
          $modelJobBg->Reporting = $mb->Reporting;
          $modelJobBg->WrapUp = $mb->WrapUp;
          $modelJobBg->OverTime = $mb->OverTime;
          $modelJobBg->Total = $mb->Total;
          $modelJobBg->UpdateBy = $mb->UpdateBy;
          $modelJobBg->UpdateAt = $mb->UpdateAt;
          $modelJobBg->save();
        }
      }

      $modelDelete = JobBudgeting::find()->where(['JobID' => $id])->all();
      foreach ($modelDelete as $key => $del)
      {
        $modelJobBgTemp = JobBudgetingTemp::find()->where(['JobID' => $del->JobID ])->andWhere(['EmployeeID' => $del->EmployeeID ])->one();
        $modelJobBgDel = JobBudgeting::find()->where(['JobID' => $del->JobID ])->andWhere(['EmployeeID' => $del->EmployeeID ])->one();

        if (empty($modelJobBgTemp))
        {
          $modelJobBgDel->delete();
        }
      }

    }

    protected function saveChanges($id)
    {
      $model = $this->findModelTemp($id);
      $sql = "SELECT * FROM AuditTrail where TransID = '$model->JobID' order by ID desc LIMIT 1";
      $auditID = Yii::$app->db->createCommand($sql)->queryOne();
      // var_dump($auditID);
      if ($auditID['ID'] != null) {
          $sqlp = "call getAuditDescriptionAll('".$auditID['ID']."')";
          $sqlp2 = "call getAuditDescriptionAll_2('".$auditID['ID']."')";
          try
          {
              $pAudit = Yii::$app->db->createCommand($sqlp)->queryOne();
          } catch (\Exception $e)
          {
            try
            {
              $pAudit = Yii::$app->db->createCommand($sqlp2)->queryOne();
            } catch (\Exception $e)
            {
              $pAudit['vDescAll'] =  '';
            }
          }


          $pcj = New TrPreviousChangesJob;
          $pcj->JobID = $model->JobID;
          $pcj->PreviousChanges = $pAudit['vDescAll'];
          $pcj->CreatedBy = $model->CreatedBy;
          $pcj->UpdatedBy = $model->UpdateBy;
          $pcj->CreatedTime = date("Y-m-d H:i:s");
          $pcj->UpdatedTime = date("Y-m-d H:i:s");
          return   $pcj->save();

      }
      else {
        return "error";
      }
    }


    public function actionApproverevisi($id = "")
    {
       $model = $this->findModelTemp($id);

       $saveChanges = $this->saveChanges($id);
       $up = $this->updateJobRevisi($model->JobID);
       $upJb = $this->updateJobBudgetRevisi($model->JobID);

        if ($up == true) {
          $modelJob = $this->findModel($model->JobID);
          $modelJobListRevisi = $this->findModelEmployeeRevisi($id);

          $status = '';
          try {
            $send_manager =  $this->EmailManager($modelJob->manager->email, 'YOUR REVISION HAS BEEN APPROVED ' ,$modelJob);
            if ($send_manager == true) {
              foreach ($modelJobListRevisi as $key => $value) {
                if ($modelJob->Manager != $value->EmployeeID AND $modelJob->Partner != $value->EmployeeID) {
                  $send_all =  $this->EmailToAll($value, 'YOU HAVE A REVISION JOB ' ,$modelJob);
                  if ($send_all == true) {
                    return $this->redirect(['view', 'id' => $id]);
                  }
                }
              }
            }
          } catch (\Exception $e) {
            return $this->redirect(['view', 'id' => $id]);
          }

          \Yii::$app
          ->db
          ->createCommand()
          ->delete('trJobBudgetingTemp', ['JobID' => $id])
          ->execute();
          if ($model->delete()) {
            return $this->redirect(['view', 'id' => $id]);
          }
        }
        // var_dump($upJb);
        // die();
        // $modelJob = $this->findModel($model->JobID);
        // $modelJob->Description = $model->Description;
        // $modelJob->JobArea = $model->JobArea;
        // $modelJob->StartDate = $model->StartDate;
        // $modelJob->EndDate = $model->EndDate;
        // $modelJob->Partner = $model->Partner;
        // $modelJob->Manager = $model->Manager;
        // $modelJob->Supervisor = $model->Supervisor;
        // $modelJob->IncludeOPE = $model->IncludeOPE;
        // $modelJob->IsMeal = $model->IsMeal;
        // $modelJob->IsOutOfOffice = $model->IsOutOfOffice;
        // $modelJob->IsTaxi = $model->IsTaxi;
        // $modelJob->MealAllowance = $model->MealAllowance;
        // $modelJob->OutOfOfficeAllowance = $model->OutOfOfficeAllowance;
        // $modelJob->AdministrativeCharge = $model->AdministrativeCharge;
        // $modelJob->OtherExpenseAllowance = $model->OtherExpenseAllowance;
        // $modelJob->TaxiAllowance = $model->TaxiAllowance;
        // $modelJob->Fee = $model->Fee;
        // $modelJob->Flag = 0;
        // if ($modelJob->save(false)) {
        //   $modelJobBudget = JobBudgetingTemp::find()->where(['JobID' => $id])->all();
        //   foreach($modelJobBudget as $mb)
        //   {
        //     $modelJobBg = JobBudgeting::find()->where(['JobID' => $mb->JobID ])->andWhere(['EmployeeID' => $mb->EmployeeID ])->one();
        //     if (empty($modelJobBg)) {
        //       $modelJobBg = new JobBudgeting;
        //       $modelJobBg->JobID = $mb->JobID;
        //       $modelJobBg->EmployeeID = $mb->EmployeeID;
        //       $modelJobBg->Planning = $mb->Planning;
        //       $modelJobBg->FieldWork = $mb->FieldWork;
        //       $modelJobBg->Reporting = $mb->Reporting;
        //       $modelJobBg->WrapUp = $mb->WrapUp;
        //       $modelJobBg->OverTime = $mb->OverTime;
        //       $modelJobBg->Total = $mb->Total;
        //       $modelJobBg->UpdateBy = $mb->UpdateBy;
        //       $modelJobBg->UpdateAt = $mb->UpdateAt;
        //       $modelJobBg->save();
        //     }
        //
        //     else{
        //       $modelJobBg->JobID = $mb->JobID;
        //       $modelJobBg->EmployeeID = $mb->EmployeeID;
        //       $modelJobBg->Planning = $mb->Planning;
        //       $modelJobBg->FieldWork = $mb->FieldWork;
        //       $modelJobBg->Reporting = $mb->Reporting;
        //       $modelJobBg->WrapUp = $mb->WrapUp;
        //       $modelJobBg->OverTime = $mb->OverTime;
        //       $modelJobBg->Total = $mb->Total;
        //       $modelJobBg->UpdateBy = $mb->UpdateBy;
        //       $modelJobBg->UpdateAt = $mb->UpdateAt;
        //       $modelJobBg->save();
        //     }
        //   }
        //
        //   $modelDelete = JobBudgeting::find()->where(['JobID' => $id])->all();
        //   foreach ($modelDelete as $key => $del)
        //   {
        //     $modelJobBgTemp = JobBudgetingTemp::find()->where(['JobID' => $del->JobID ])->andWhere(['EmployeeID' => $del->EmployeeID ])->one();
        //     $modelJobBgDel = JobBudgeting::find()->where(['JobID' => $del->JobID ])->andWhere(['EmployeeID' => $del->EmployeeID ])->one();
        //
        //     if (empty($modelJobBgTemp))
        //     {
        //       $modelJobBgDel->delete();
        //     }
        //   }
        //
        //   $modelJobListRevisi = $this->findModelEmployeeRevisi($id);
        //
        //   $status = '';
        //   try {
        //     $send_manager =  $this->EmailManager($modelJob->manager->email, 'YOUR REVISION HAS BEEN APPROVED ' ,$modelJob);
        //     if ($send_manager == true) {
        //       foreach ($modelJobListRevisi as $key => $value) {
        //         if ($modelJob->Manager != $value->EmployeeID AND $modelJob->Partner != $value->EmployeeID) {
        //           $send_all =  $this->EmailToAll($value, 'YOU HAVE A REVISION JOB ' ,$modelJob);
        //           if ($send_all == true) {
        //             return $this->redirect(['view', 'id' => $id]);
        //           }
        //         }
        //       }
        //     }
        //   } catch (\Exception $e) {
        //     return $this->redirect(['view', 'id' => $id]);
        //   }
        //
        //   \Yii::$app
        //   ->db
        //   ->createCommand()
        //   ->delete('trJobBudgetingTemp', ['JobID' => $id])
        //   ->execute();
        //   if ($model->delete()) {
        //     return $this->redirect(['view', 'id' => $id]);
        //   }
        // }
    }

    public function actionClosed($id)
    {
      $dt = $this->findModel($id);
      $dt->Status = 4;
      $dt->save();
      if ($dt) {
        return $this->redirect(['view', 'id' => $dt->JobID]);
      }

    }


    protected function EmailPartner($sendto, $subject, $modelJob , $layouts='layouts/newjobsapproval'){
        Yii::$app->cache->flush();

        $from = Yii::$app->params['appNotificationEmail'];
        $cc = Yii::$app->params['appNotificationEmailCC'];

        $to = array();
        $to[$sendto] = $sendto;

        if (!empty($to)) {
            $to = $to;
            $html = ['html' => $layouts];
            $html_bind = ['title' => $subject , 'modelJob' => $modelJob, 'status' => ''];

            $compose = Yii::$app->mailer->compose($html, $html_bind);
            $compose->setFrom($from);
            $compose->setTo($to);
            if (!empty($cc)) {
                $compose->setCc($cc);
            }
            $compose->setSubject($subject);

            if ($compose->send()) {
              return "true";
            }

        }

    }
    protected function EmailManager($sendto, $subject, $modelJob , $layouts='layouts/newjobs'){
        Yii::$app->cache->flush();

        $from = Yii::$app->params['appNotificationEmail'];
        $cc = Yii::$app->params['appNotificationEmailCC'];


        $to = array();
        $to[$sendto] = $sendto;

        if (!empty($to)) {
            $to = $to;
            $html = ['html' => $layouts];
            $html_bind = ['title' => $subject , 'modelJob' => $modelJob , 'status' => ''];

            $compose = Yii::$app->mailer->compose($html, $html_bind);
            $compose->setFrom($from);
            $compose->setTo($to);
            if (!empty($cc)) {
                $compose->setCc($cc);
            }
            $compose->setSubject($subject);

            if ($compose->send()) {
              return "true";
            }

        }

    }
    protected function EmailToAll($sendto, $subject, $modelJob , $layouts='layouts/newjobsToAll'){
        Yii::$app->cache->flush();

        $from = Yii::$app->params['appNotificationEmail'];
        $cc = Yii::$app->params['appNotificationEmailCC'];


        $to = array();
        $to[$sendto->employee->email] = $sendto->employee->email;

        if (!empty($to)) {
            $to = $to;
            $html = ['html' => $layouts];
            $html_bind = ['title' => $subject , 'modelJob' => $modelJob ,'sendto' => $sendto];

            $compose = Yii::$app->mailer->compose($html, $html_bind);
            $compose->setFrom($from);
            $compose->setTo($to);
            if (!empty($cc)) {
                $compose->setCc($cc);
            }
            $compose->setSubject($subject);

            if ($compose->send()) {
              return "true";
            }

        }

    }




}
