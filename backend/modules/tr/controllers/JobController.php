<?php

namespace backend\modules\tr\controllers;

use Yii;
use common\components\CommonHelper;
use common\components\TimeReportHelper;

use common\models\tr\JobList;
use common\models\tr\Job;
use common\models\tr\JobTemp;
use common\models\tr\JobBudgeting;
use common\models\tr\JobBudgetingTemp;
use common\models\tr\JobComment;
use common\models\tr\TrPreviousChangesJob;
use common\models\tr\AuditTrail;
use common\models\tr\AuditTrailDet;


use common\models\tr\search\JobviewSearch;
use common\models\tr\search\JobBudgetingSearch;
use common\models\tr\search\JobBudgetingTempSearch;
use common\models\tr\search\JobCommentSearch;
use common\models\tr\search\AuditTrailSearch;

use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\MethodNotAllowedHttpException;

use common\components\HelperDB;

class JobController extends Controller
{

  protected function findModelView($id)
  {
    if (($model = JobList::findOne($id)) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }

  protected function findModel($id)
  {
    if (($model = Job::findOne($id)) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }

  protected function findModelTemp($id)
  {
    if (($model = JobTemp::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException('The requested page does not exist.');
  }

  protected function findModelBudget($EmployeeID, $JobID, $param = [])
  {
    if (($modelBudget = JobBudgeting::find()->where(['EmployeeID' => $EmployeeID, 'JobID' => $JobID])->one()) !== null) {
      return $modelBudget;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
  protected function findModelBudgetTemp($EmployeeID, $JobID, $param = [])
  {
    if (($modelBudget = JobBudgetingTemp::find()->where(['EmployeeID' => $EmployeeID, 'JobID' => $JobID])->one()) !== null) {
      return $modelBudget;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  protected function findModelHistoryJob($JobID)
  {
    if ($model = TrPreviousChangesJob::find()->where(['JobID' => $JobID])->all()) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }


  public function behaviors()
  {
    return HelperDB::set_behaviors();
  }


  public function actionAll()
  {
    return $this->index("");
  }
  public function actionIndex()
  {
    return $this->redirect(['draft']);
  }
  public function actionDraft()
  {
    return $this->index('DRAFT');
  }
  public function actionSubmitted()
  {
    return $this->index("SUBMITTED");
  }
  public function actionApproved()
  {
    return $this->index("APPROVED");
  }
  public function actionRejected()
  {
    return $this->index("REJECTED");
  }
  public function actionOngoing()
  {
    return $this->index("ONGOING");
  }
  public function actionExpired()
  {
    return $this->index("EXPIRED");
  }
  public function actionClosed()
  {
    return $this->index("CLOSED");
  }



  public function IndexFilter()
  {
    $session = Yii::$app->session;
    $session->open();

    $post = Yii::$app->request->post();
    $model = new JobviewSearch();
    if ($model->load($post)) {
      $data = [];

      $data['client_id'] = $model->client_id;
      $data['job_code'] = $model->job_code;
      $data['job_description'] = $model->job_description;
      $data['periode'] = $model->periode;
      $session->set('__job_index', $data);
    }
  }

  public function index($status_job)
  {
    $this->IndexFilter();

    $user = CommonHelper::getUserIndentity();
    $param = isset($_SESSION['__job_index']) ? $_SESSION['__job_index'] : [];
    $param['job_code'] = isset($param['job_code']) ? $param['job_code'] : '';
    $param['client_id'] = isset($param['client_id']) ? $param['client_id'] : '';
    $param['job_description'] = isset($param['job_description']) ? $param['job_description'] : '';
    $param['periode'] = isset($param['periode']) ? $param['periode'] : '';

    $searchModel = new JobviewSearch();
    $searchModel->client_id = $param['client_id'];
    $searchModel->job_code = $param['job_code'];
    $searchModel->job_description = $param['job_description'];
    $searchModel->periode = $param['periode'];

    if ($user->is_admin != 1) {
      $searchModel->manager_id = $user->user_id;
    }

    if (!empty($status_job) and $status_job != "all") {
      $searchModel->job_status = $status_job;
    }

    $dataProvider = $searchModel->search(Yii::$app->request->post());
    // var_dump($user->is_admin, $user->user_id);
    // die();
    return $this->render('job', [
      'OrderButtons' => JobList::set_button_header($status_job),
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function getDataForForm($model)
  {
    $data = array();
    $data['Division'] = TimeReportHelper::getDivision();
    $data['Entity'] = TimeReportHelper::getEntity();
    $data['Client'] = TimeReportHelper::getClient();
    $data['Employee'] = TimeReportHelper::getEmployee();
    return $data;
  }


  // REQUEST :

  public function actionRequest($id)
  {
    $modelView = $this->findModelView($id);
    $model = $this->findModel($id);
    $model->scenario = 'update';

    $model->JobArea = explode(',', $model->JobArea);

    if ($model->Status != 0 and $model->Status != 3) {
      return $this->redirect(['view', 'id' => $model->JobID]);
    }

    $data = $this->getDataForForm($model);

    $search = new JobBudgetingSearch();
    $search->JobID = $model->JobID;
    $modelBudgeting = $search->search($model->JobID);

    return $this->render('Request', [
      'data' => $data,
      'modelView' => $modelView,
      'model' => $model,
      'modelBudgeting' => $modelBudgeting,
    ]);
  }

  public function actionRequest_save($submit = 0)
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $post = Yii::$app->request->post();
    $user = CommonHelper::getUserIndentity();

    $result = HelperDB::result_default();

    $model = new Job();
    $model->scenario = 'update';

    if ($model->load($post)) {
      $model = $this->findModel($model->JobID);

      if ($model->load($post)) {
        $model->Fee = empty($model->Fee) ? 0 : str_replace(',', '', $model->Fee);
        $model->MealAllowance = empty($model->MealAllowance) ? 0 : str_replace(',', '', $model->MealAllowance);
        $model->TaxiAllowance = empty($model->TaxiAllowance) ? 0 : str_replace(',', '', $model->TaxiAllowance);
        $model->OutOfOfficeAllowance = empty($model->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $model->OutOfOfficeAllowance);
        $model->AdministrativeCharge = empty($model->AdministrativeCharge) ? 0 : str_replace(',', '', $model->AdministrativeCharge);
        //$model->AdministrativeChargeAct = empty($model->AdministrativeCharge) ? 0 : str_replace(',', '', $model->AdministrativeCharge);
        $model->OtherExpenseAllowance = empty($model->OtherExpenseAllowance) ? 0 : str_replace(',', '', $model->OtherExpenseAllowance);
        //$model->OtherExpenseAllowanceAct = empty($model->OtherExpenseAllowance) ? 0 : str_replace(',', '', $model->OtherExpenseAllowance);


        $model->JobArea = empty($model->JobArea) ? null : implode(',', $model->JobArea);

        $model->Partner = empty($model->Partner) ? null : $model->Partner;
        $model->Manager = empty($model->Manager) ? null : $model->Manager;
        $model->Supervisor = empty($model->Supervisor) ? null : $model->Supervisor;
        $model->UpdateBy = $user->Id;

        if ($submit == 1) {
          $model->Status = 1;
        }

        $result = HelperDB::save($model, false);
        $model = $this->findModel($model->JobID);
        $result['state']['model']['isSubmit'] = false;

        $result['state']['model']['IsMeal'] = $model->IsMeal;
        $result['state']['model']['IsOutOfOffice'] = $model->IsOutOfOffice;
        $result['state']['model']['IsTaxi'] = $model->IsTaxi;

        $result['state']['model']['TimeCharges'] = empty($model->TimeCharges) ? 0 : $model->TimeCharges;
        $result['state']['model']['TimeChargesAct'] = empty($model->TimeChargesAct) ? 0 : number_format($model->TimeChargesAct);

        $result['state']['model']['MealAllowance'] = empty($model->MealAllowance) ? 0 : $model->MealAllowance;
        $result['state']['model']['MealAllowanceAct'] = empty($model->MealAllowanceAct) ? 0 : number_format($model->MealAllowanceAct);

        $result['state']['model']['TaxiAllowance'] = empty($model->TaxiAllowance) ? 0 : $model->TaxiAllowance;
        $result['state']['model']['TaxiAllowanceAct'] = empty($model->TaxiAllowanceAct) ? 0 : number_format($model->TaxiAllowanceAct);

        $result['state']['model']['OutOfOfficeAllowance'] = empty($model->OutOfOfficeAllowance) ? 0 : $model->OutOfOfficeAllowance;
        $result['state']['model']['OutOfOfficeAllowanceAct'] = empty($model->OutOfOfficeAllowanceAct) ? 0 : number_format($model->OutOfOfficeAllowanceAct);

        $result['state']['model']['AdministrativeCharge'] = empty($model->AdministrativeCharge) ? 0 : $model->AdministrativeCharge;
        $result['state']['model']['AdministrativeChargeAct'] = empty($model->AdministrativeCharge) ? 0 : number_format($model->AdministrativeCharge);

        $result['state']['model']['OtherExpenseAllowance'] = empty($model->OtherExpenseAllowance) ? 0 : $model->OtherExpenseAllowance;
        $result['state']['model']['OtherExpenseAllowanceAct'] = empty($model->OtherExpenseAllowance) ? 0 : number_format($model->OtherExpenseAllowance);

        $result['state']['model']['Percentage'] = empty($model->Percentage) ? 0 : $model->Percentage;
        $result['state']['model']['PercentageAct'] = empty($model->PercentageAct) ? 0 : $model->PercentageAct;

        if ($submit == 1) {
          $result['state']['model']['isSubmit'] = true;
          if (!empty($model->partner->email)) {
            $send_email = $this->Email($model->partner->email, 'THERE IS A NEW JOB NEEDS YOUR APPROVAL', $model);
            if ($send_email) {
              //return $this->redirect(['view', 'id' => $model->JobID]);
            } else {
              $result['state']['status'] = false;
              $result['state']['message'] = "Can't Send Email to Partner, please cek email address";
            }
          } else {
            $result['state']['status'] = false;
            $result['state']['message'] = "Partner's email not found";
          }
        }
      } else {
        $result = HelperDB::result_noloaded($model);
      }
    } else {
      $result = HelperDB::result_noloaded($model);
    }

    $result['model'] = [];

    return $result['state'];
  }







  public function actionEstimated_default()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new Job();
    if ($modelJob->load($post)) {
      $sql = "call job_estimated('" . $modelJob->JobID . "', 0)";
      Yii::$app->db->createCommand($sql)->queryOne();

      $results = $this->findModel($modelJob->JobID);
    } else {
      $results = "Cannot load module";
    }

    return $results;
  }

  public function actionEstimated_default_temp()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobTemp();
    if ($modelJob->load($post)) {
      $sql = "call job_estimated('" . $modelJob->JobID . "', 0)";
      Yii::$app->db->createCommand($sql)->queryOne();

      $results = $this->findModel($modelJob->JobID);
    } else {
      $results = "Cannot load module";
    }

    return $results;
  }
  public function actionApprove($id)
  {
    $user = CommonHelper::getUserIndentity();
    $modelJob = $this->findModel($id);
    $modelJob->Status = 2;
    $haveRight = ($modelJob->Partner == $user->Id or $user->IsAdmin == 1) ? 1 : 0;

    $email = [];
    $JobBudgeting = JobBudgeting::find()->where(['JobID' => $modelJob->JobID])->all();
    foreach ($JobBudgeting as $jb) {
      $email[] = $jb->employee->email;
    }
    return $this->approval($haveRight, $modelJob, 'YOU HAVE A NEW JOB', $email);
  }
  public function actionReject($id)
  {
    $user = CommonHelper::getUserIndentity();
    $modelJob = $this->findModel($id);
    $modelJob->Status = 3;
    $haveRight = ($modelJob->Partner == $user->Id or $user->IsAdmin == 1) ? 1 : 0;

    $email = [];
    $email[] = $modelJob->manager->email;
    return $this->approval($haveRight, $modelJob, 'YOUR JOB REQUEST HAS REJECTED', $email);
  }
  public function actionClose($id)
  {
    $user = CommonHelper::getUserIndentity();
    $modelJob = $this->findModel($id);
    $modelJob->Status = 4;
    $haveRight = ($modelJob->Partner == $user->Id or $user->IsAdmin == 1) ? 1 : 0;

    $email = [];
    $JobBudgeting = JobBudgeting::find()->where(['JobID' => $modelJob->JobID])->all();
    foreach ($JobBudgeting as $jb) {
      $email[] = $jb->employee->email;
    }
    return $this->approval($haveRight, $modelJob, 'A JOB HAS BEEN CLOSED', $email);
  }

  public function approval($haveRight, $model, $subject, $email)
  {
    $result = HelperDB::result_default();

    if ($haveRight) {
      if ($model->save(false)) {
        if (!empty($email)) {
          foreach ($email as $mail) {
            $send_email = $this->Email($mail, $subject, $model);
            if ($send_email) {
              $result['state']['status'] = true;
              $result['state']['message'] = "Success";
            } else {
              $result['state']['status'] = false;
              $result['state']['message'] = "Can't Send Email, please cek email address " . $mail;
              break;
            }
          }
        } else {
          $result['state']['status'] = false;
          $result['state']['message'] = "Email not found";
        }
      } else {
        $result['state']['status'] = false;
        $result['state']['message'] = "Can not approve the Job";
      }
    } else {
      $result['state']['status'] = false;
      $result['state']['message'] = "You do not have right to make approval for this Job";
    }
    return json_encode($result['state']);
  }


  // BUDGET

  public function actionBudget($id = null, $action = null)
  {
    $session = Yii::$app->session;
    $session->open();
    $data = [];

    $post = Yii::$app->request->post();
    $model = new Job();
    $model->load($post);

    $modelJob = Job::find()->where(['JobID' => $model->JobID])->one();
    if ($modelJob->load($post)) {
      $session->set('_trjob_form' . $modelJob->JobID, $modelJob);
      if (Yii::$app->request->isAjax) {
        if (!empty($id)) {
          $modelBudget = $this->findModelBudget($id, $modelJob->JobID);
        } else {
          $modelBudget = new JobBudgeting();
          $modelBudget->JobID = $modelJob->JobID;
          if (empty($action)) {
            $modelBudget->isNewRecord;
          }
        }
        $data = $this->getDataForForm($modelJob);


        return $this->renderAjax('RequestDetail', [
          'id' => empty($action) ? $id : "",
          'modelJob' => $modelJob,
          'modelBudget' => $modelBudget,
          'data' => $data
        ]);
      } else {
        throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }
  }
  public function actionBudgetdelete()
  {
    $return = false;
    $post = Yii::$app->request->post();

    $EmployeeID = $post['EmployeeID'];
    $JobID = $post['JobID'];
    $modelBudget = $this->findModelBudget($EmployeeID, $JobID);

    $transaction = \Yii::$app->db->beginTransaction();
    try {
      if ($modelBudget->delete()) {
        $transaction->commit();
        $return = true;
      } else {
        $transaction->rollback();
      }
    } catch (Exception $e) {
      $transaction->rollBack();
    }

    return $return;
  }

  public function actionBudgetsave($id = null)
  {
    $result = HelperDB::result_default();
    $user = CommonHelper::getUserIndentity();
    $post = Yii::$app->request->post();

    $modelBudget = new JobBudgeting();
    if ($modelBudget->load($post)) {
      if (!empty($id)) {
        $modelBudget = $this->findModelBudget($id, $modelBudget->JobID);
        $modelBudget->load($post);
      }

      $transaction = \Yii::$app->db->beginTransaction();
      try {
        $valid = $modelBudget->validate();
        $modelBudget->CreatedBy = $user->Id;
        if ($modelBudget->OverTime == null) {
          $modelBudget->OverTime = 0;
        }
        if ($modelBudget->save()) {
          $transaction->commit();
          $result = HelperDB::result_success();
        } else {
          $result['state']['status'] = false;
          $result['state']['message'] = HelperDB::db_field_error(Html::errorSummary($modelBudget));
        }
      } catch (\yii\db\Exception $e) {
        $result['state']['status'] = false;
        $result['state']['message'] = HelperDB::db_trigger_error($e->getMessage());
      }
    } else {
      $result = HelperDB::result_noloaded($modelBudget);
    }

    return json_encode($result['state']);
  }














  // REVISE :

  public function actionRevise($id)
  {
    $model = $this->findModel($id);
    if ($model->Flag == 0) {
      $model->Flag = 1;
      $model->save();
    }

    $modelView = $this->findModelView($id);
    $model = $this->findModelTemp($model->JobID);
    $model->JobArea = explode(',', $model->JobArea);

    $data = $this->getDataForForm($model);

    $search = new JobBudgetingTempSearch();
    $search->JobID = $model->JobID;
    $modelBudgeting = $search->search($model->JobID);

    return $this->render('Revise', [
      'data' => $data,
      'modelView' => $modelView,
      'model' => $model,
      'modelBudgeting' => $modelBudgeting,
    ]);
  }

  public function actionRevise_save($submit = 0)
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $post = Yii::$app->request->post();
    $user = CommonHelper::getUserIndentity();

    $result = HelperDB::result_default();

    $model = new JobTemp();
    $model->scenario = 'update';


    if ($model->load($post)) {
      $model = $this->findModelTemp($model->JobID);

      if ($model->load($post)) {
        $model->Fee = empty($model->Fee) ? 0 : str_replace(',', '', $model->Fee);
        $model->MealAllowance = empty($model->MealAllowance) ? 0 : str_replace(',', '', $model->MealAllowance);
        $model->TaxiAllowance = empty($model->TaxiAllowance) ? 0 : str_replace(',', '', $model->TaxiAllowance);
        $model->OutOfOfficeAllowance = empty($model->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $model->OutOfOfficeAllowance);
        $model->AdministrativeCharge = empty($model->AdministrativeCharge) ? 0 : str_replace(',', '', $model->AdministrativeCharge);
        //$model->AdministrativeChargeAct = empty($model->AdministrativeCharge) ? 0 : str_replace(',', '', $model->AdministrativeCharge);
        $model->OtherExpenseAllowance = empty($model->OtherExpenseAllowance) ? 0 : str_replace(',', '', $model->OtherExpenseAllowance);
        //$model->OtherExpenseAllowanceAct = empty($model->OtherExpenseAllowance) ? 0 : str_replace(',', '', $model->OtherExpenseAllowance);


        $model->JobArea = empty($model->JobArea) ? null : implode(',', $model->JobArea);

        $model->Partner = empty($model->Partner) ? null : $model->Partner;
        $model->Manager = empty($model->Manager) ? null : $model->Manager;
        $model->Supervisor = empty($model->Supervisor) ? null : $model->Supervisor;
        $model->UpdateBy = $user->Id;
        $model->Status = 1;



        $result = HelperDB::save($model, false);

        $model = $this->findModelTemp($model->JobID);
        $result['state']['model']['isSubmit'] = false;

        $result['state']['model']['IsMeal'] = $model->IsMeal;
        $result['state']['model']['IsOutOfOffice'] = $model->IsOutOfOffice;
        $result['state']['model']['IsTaxi'] = $model->IsTaxi;

        $result['state']['model']['TimeCharges'] = empty($model->TimeCharges) ? 0 : $model->TimeCharges;
        $result['state']['model']['TimeChargesAct'] = empty($model->TimeChargesAct) ? 0 : number_format($model->TimeChargesAct);

        $result['state']['model']['MealAllowance'] = empty($model->MealAllowance) ? 0 : $model->MealAllowance;
        $result['state']['model']['MealAllowanceAct'] = empty($model->MealAllowanceAct) ? 0 : number_format($model->MealAllowanceAct);

        $result['state']['model']['TaxiAllowance'] = empty($model->TaxiAllowance) ? 0 : $model->TaxiAllowance;
        $result['state']['model']['TaxiAllowanceAct'] = empty($model->TaxiAllowanceAct) ? 0 : number_format($model->TaxiAllowanceAct);

        $result['state']['model']['OutOfOfficeAllowance'] = empty($model->OutOfOfficeAllowance) ? 0 : $model->OutOfOfficeAllowance;
        $result['state']['model']['OutOfOfficeAllowanceAct'] = empty($model->OutOfOfficeAllowanceAct) ? 0 : number_format($model->OutOfOfficeAllowanceAct);

        $result['state']['model']['AdministrativeCharge'] = empty($model->AdministrativeCharge) ? 0 : $model->AdministrativeCharge;
        $result['state']['model']['AdministrativeChargeAct'] = empty($model->AdministrativeCharge) ? 0 : number_format($model->AdministrativeCharge);

        $result['state']['model']['OtherExpenseAllowance'] = empty($model->OtherExpenseAllowance) ? 0 : $model->OtherExpenseAllowance;
        $result['state']['model']['OtherExpenseAllowanceAct'] = empty($model->OtherExpenseAllowance) ? 0 : number_format($model->OtherExpenseAllowance);

        $result['state']['model']['Percentage'] = empty($model->Percentage) ? 0 : $model->Percentage;
        $result['state']['model']['PercentageAct'] = empty($model->PercentageAct) ? 0 : $model->PercentageAct;

        if ($result['state']['status'] == true) {
          if ($submit == 1) {
            $result['state']['model']['isSubmit'] = true;

            $modelReal = $this->findModel($model->JobID);
            $modelReal->Flag = 2;

            $resultReal = HelperDB::save($modelReal, false);

            $result['state']['status'] = $resultReal['state']['status'];
            $result['state']['message'] = $resultReal['state']['message'];
            if ($result['state']['status'] == true) {
              if (!empty($model->partner->email)) {
                $send_email = $this->Email($model->partner->email, 'THERE IS A NEW REVISED JOB NEEDS YOUR APPROVAL', $model);
                if ($send_email) {
                  //return $this->redirect(['view', 'id' => $model->JobID]);
                } else {
                  $result['state']['status'] = false;
                  $result['state']['message'] = "Can't Send Email to Partner, please cek email address";
                }
              } else {
                $result['state']['status'] = false;
                $result['state']['message'] = "Partner's email not found";
              }
            }
          }
        }
      } else {
        $result = HelperDB::result_noloaded($model);
      }
    } else {
      $result = HelperDB::result_noloaded($model);
    }

    $result['model'] = [];

    return $result['state'];
  }

  public function actionRevise_view($id)
  {
    $model = $this->findModel($id);
    $header = AuditTrail::find()->where(['TransID' => $id])->orderBy('DateCreated DESC')->one();
    $details = AuditTrailDet::find()->where(['AuditTrailID' => $header->ID])->andWhere("AuditDescription  != '' and AuditDescription is not null")->all();

    return $this->renderAjax('ReviseView', [
      'model' => $model,
      'header' => $header,
      'details' => $details,
    ]);
  }

  public function actionRevise_log($id)
  {
    $modelView = $this->findModelView($id);

    $searchModel = new AuditTrailSearch();
    $searchModel->TransID = $id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('ReviseLog', [
      'modelView' => $modelView,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }
  public function actionRevise_log_detail()
  {
    $post = Yii::$app->request->post();

    $id = $post['expandRowKey'];
    $header = AuditTrail::find()->where(['ID' => $id])->one();
    $details = AuditTrailDet::find()->where(['AuditTrailID' => $header->ID])->andWhere("AuditDescription  != '' and AuditDescription is not null")->all();

    return $this->renderPartial('ReviseLogDetail', [
      'header' => $header,
      'details' => $details,
    ]);
  }

  public function actionRevise_approve($id = "")
  {
    $modelTemp = $this->findModelTemp($id);
    $model = $this->findModel($modelTemp->JobID);

    $transaction = \Yii::$app->db->beginTransaction();
    try {
      $model->Description = $modelTemp->Description;
      $model->JobArea = $modelTemp->JobArea;
      $model->StartDate = $modelTemp->StartDate;
      $model->EndDate = $modelTemp->EndDate;
      $model->Partner = $modelTemp->Partner;
      $model->Manager = $modelTemp->Manager;
      $model->Supervisor = $modelTemp->Supervisor;
      $model->IncludeOPE = $modelTemp->IncludeOPE;
      $model->IsMeal = $modelTemp->IsMeal;
      $model->IsOutOfOffice = $modelTemp->IsOutOfOffice;
      $model->IsTaxi = $modelTemp->IsTaxi;
      $model->MealAllowance = $modelTemp->MealAllowance;
      $model->OutOfOfficeAllowance = $modelTemp->OutOfOfficeAllowance;
      $model->AdministrativeCharge = $modelTemp->AdministrativeCharge;
      $model->OtherExpenseAllowance = $modelTemp->OtherExpenseAllowance;
      $model->TaxiAllowance = $modelTemp->TaxiAllowance;
      $model->Fee = $modelTemp->Fee;
      $model->Flag = 0;


      $result = HelperDB::save($model, false);
      if ($result['state']['status'] == true) {

        $temps = JobBudgetingTemp::find()->where(['JobID' => $id])->all();
        if (!empty($temps)) {

          //Update all Job Budgeting from Temporary to Real
          $tmps_list = '';
          foreach ($temps as $tmp) {
            $JobBudgeting = JobBudgeting::find()->where(['JobID' => $tmp->JobID, 'EmployeeID' => $tmp->EmployeeID])->one();
            if (empty($JobBudgeting)) {
              $JobBudgeting = new JobBudgeting();
            }

            if (empty($tmps_list)) {
              $tmps_list .= "'" . $tmp->EmployeeID . "'";
            } else {
              $tmps_list .= ", '" . $tmp->EmployeeID . "'";
            }

            $JobBudgeting->JobID = $tmp->JobID;
            $JobBudgeting->EmployeeID = $tmp->EmployeeID;
            $JobBudgeting->Planning = $tmp->Planning;
            $JobBudgeting->FieldWork = $tmp->FieldWork;
            $JobBudgeting->Reporting = $tmp->Reporting;
            $JobBudgeting->WrapUp = $tmp->WrapUp;
            $JobBudgeting->OverTime = $tmp->OverTime;
            $JobBudgeting->Total = $tmp->Total;
            $JobBudgeting->UpdateBy = $tmp->UpdateBy;
            $JobBudgeting->UpdateAt = $tmp->UpdateAt;

            $result = HelperDB::save($JobBudgeting, false);
            if ($result['state']['status'] == true) {
              //Delete Temporary Job Budgeting :
              $JobBudgetingTemp = JobBudgetingTemp::find()->where(['JobID' => $tmp->JobID, 'EmployeeID' => $tmp->EmployeeID])->one();
              if (!$JobBudgetingTemp->delete()) {
                $result['state']['status'] = false;
                $result['state']['message'] = $JobBudgetingTemp->EmployeeID . " : " . HelperDB::db_field_error(Html::errorSummary($JobBudgetingTemp));
                break;
              }
            } else {
              $result['state']['status'] = false;
              $result['state']['message'] = $JobBudgeting->EmployeeID . " : " . HelperDB::db_field_error(Html::errorSummary($JobBudgeting));
              break;
            }
          }
        }
      } else {
        $result['state']['status'] = false;
        $result['state']['message'] = "Can not save Job Temp : " . HelperDB::db_field_error(Html::errorSummary($model));
      }

      if ($result['state']['status'] == true) {
        //Delete Job Budgeting which not ini Job Budgeting Temp
        if (!empty($tmps_list)) {
          JobBudgeting::deleteAll('JobID = ' . $id . ' and (EmployeeID NOT IN (' . $tmps_list . ')) ');
        }

        //Delete Job Temp
        $modelTemp->delete();
        $transaction->commit();
      } else {
        $transaction->rollBack();
      }
    } catch (\yii\db\Exception $e) {
      $result['state']['status'] = false;
      $result['state']['message'] = HelperDB::db_trigger_error($e->getMessage());
    }

    return json_encode($result['state']);
  }

  public function actionRevise_reject($id = "")
  {
    $model = $this->findModel($id);
    $model->Flag = 1;

    $result = HelperDB::save($model, false);
    if ($result['state']['status'] == false) {
      $result['state']['status'] = false;
      $result['state']['message'] = HelperDB::db_field_error(Html::errorSummary($model));
    }
    return json_encode($result['state']);
  }



  // BUDGET TEMPORARY

  public function actionBudget_temp($id = null, $action = null)
  {
    $session = Yii::$app->session;
    $session->open();

    $post = Yii::$app->request->post();
    $model = new JobTemp();
    $model->load($post);

    $modelJob = JobTemp::find()->where(['JobID' => $model->JobID])->one();
    if ($modelJob->load($post)) {
      $session->set('_trjob_form' . $modelJob->JobID, $modelJob);
      if (Yii::$app->request->isAjax) {
        if (!empty($id)) {
          $modelBudget = $this->findModelBudgetTemp($id, $modelJob->JobID);
        } else {
          $modelBudget = new JobBudgetingTemp();
          $modelBudget->JobID = $modelJob->JobID;
          if (empty($action)) {
            $modelBudget->isNewRecord;
          }
        }
        $data = $this->getDataForForm($modelJob);
        return $this->renderAjax('ReviseDetail', [
          'id' => empty($action) ? $id : "",
          'modelJob' => $modelJob,
          'modelBudget' => $modelBudget,
          'data' => $data
        ]);
      } else {
        throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }
  }
  public function actionBudgetsave_temp($id = null)
  {
    $result = HelperDB::result_default();
    $user = CommonHelper::getUserIndentity();
    $post = Yii::$app->request->post();

    $modelBudget = new JobBudgetingTemp();
    if ($modelBudget->load($post)) {
      if (!empty($id)) {
        $modelBudget = $this->findModelBudgetTemp($id, $modelBudget->JobID);
        $modelBudget->load($post);
      }

      $transaction = \Yii::$app->db->beginTransaction();
      try {
        $valid = $modelBudget->validate();
        $modelBudget->CreatedBy = $user->Id;
        if ($modelBudget->OverTime == null) {
          $modelBudget->OverTime = 0;
        }
        if ($modelBudget->save()) {
          $transaction->commit();
          $result = HelperDB::result_success();
        } else {
          $result['state']['status'] = false;
          $result['state']['message'] = HelperDB::db_field_error(Html::errorSummary($modelBudget));
        }
      } catch (\yii\db\Exception $e) {
        $result['state']['status'] = false;
        $result['state']['message'] = HelperDB::db_trigger_error($e->getMessage());
      }
    } else {
      $result = HelperDB::result_noloaded($modelBudget);
    }

    return json_encode($result['state']);
  }

  public function actionBudgetdelete_temp()
  {
    $return = false;
    $post = Yii::$app->request->post();

    $EmployeeID = $post['EmployeeID'];
    $JobID = $post['JobID'];
    $modelBudget = $this->findModelBudgetTemp($EmployeeID, $JobID);

    $transaction = \Yii::$app->db->beginTransaction();
    try {
      if ($modelBudget->delete()) {
        $transaction->commit();
        $return = true;
      } else {
        $transaction->rollback();
      }
    } catch (Exception $e) {
      $transaction->rollBack();
    }

    return $return;
  }


















  //UTIITES


  public function actionBudget_totalwh()
  {
    $post = Yii::$app->request->post();
    $modelJob = new JobBudgeting();
    if (!$modelJob->load($post)) {
      $modelJob = new JobBudgetingTemp();
      $modelJob->load($post);
    }

    $sql = "call getValueTaskAE('" . $modelJob->EmployeeID . "','" . $modelJob->Total . "')";
    $task = Yii::$app->db->createCommand($sql)->queryOne();

    return json_encode($task);
  }

  public function actionBudget_latesjob()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();

    $modelJob = new Job();
    $modelJob->load($post);

    $result = \Yii::$app->db->createCommand("select name_val('new-job-no',:paramName1,null)")
      ->bindValue(':paramName1', $modelJob->ClientID);
    $data['job'] = $result->queryScalar();
    $data['ClientID'] = $modelJob->ClientID;

    return $data;
  }

  public function actionBudget_getlevelid()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobBudgeting();
    $modelJob->load($post);

    $data['fullName'] = empty($modelJob->employee->fullName) ? "" : $modelJob->employee->fullName;
    $data['levelName'] = empty($modelJob->employee->level->levelName) ? "" : $modelJob->employee->level->levelName;
    $data['eA'] = $this->GetEmployeeAvailable($modelJob->EmployeeID);

    return $data;
  }

  public function GetEmployeeAvailable($id)
  {
    $sql = "call antTrJobEmployeeAvailableK('{$id}')";
    $data = Yii::$app->db->createCommand($sql)->queryAll();
    $html = "";
    if (!empty($data)) {
      $html .= "<div class='well table-responsive' style='margin: 10px 0 30px; max-height : 130px;'>";
      $html .= "<table class='table table-primary'>";
      $html .= "<tr>
            <td class='bg-primary'>No</td>
            <td class='bg-primary'>Client</td>
            <td class='bg-primary'>Description</td>
            <td class='bg-primary'>WH</td>
        </tr>";
      $no = 1;
      foreach ($data as $d) {
        $html .= "<tr>";
        $html .= "<td>" . $no++ . "</td>";
        $html .= "<td>" . $d['Client'] . "</td>";
        $html .= "<td>" . $d['Description'] . "</td>";
        $html .= "<td>" . $d['WH'] . "</td>";
        $html .= "<tr>";
      }

      $html .= "</table>";
      $html .= "</div>";
    } else {
      $html .= "";
    }
    return $html;
  }




  public function actionView($id, $comment = '')
  {
    Yii::$app->cache->flush();

    $modelJob = $this->findModel($id);
    $modelView = $this->findModelView($id);
    $modelComments = new JobComment();
    $modelComments->job_id = $id;

    $searchModelBudget = new JobBudgetingSearch([
      'job_id' => $id,
    ]);
    $dataProviderBudget = $searchModelBudget->search(Yii::$app->request->queryParams);

    return $this->render('view', [
      'modelJob' => $modelJob,
      'modelView' => $modelView,
      'modelComments' => $modelComments,
      'dataProviderBudget' => $dataProviderBudget,
    ]);
  }


  public function actionComments($id = '')
  {
    // var_dump($id);
    $user = CommonHelper::getUserIndentity();
    $modelComments = new JobComment();
    $post = Yii::$app->request->post();
    $modelComments->load($post);

    //Start read comment
    $count = JobComment::find()
      ->select(['COUNT(*) AS jml'])
      ->where(['JobID' => $id])
      ->andWhere(['like', 'ListEmployee', $user->Id])
      ->count();

    if ($count != 0) {
      $modelCommentsRead = JobComment::find()
        ->where(['JobID' => $id])
        ->andWhere(['like', 'ListEmployee', $user->Id])->one();
      foreach (explode(',', $modelCommentsRead->ListEmployee) as $key => $value) {
        if ($value != $user->Id) {
          $data[] = $value;
        }
      }
      $readcomment = implode(', ', $data);

      $modelCommentsRead->ListEmployee = $readcomment;
      Yii::$app->db->createCommand()
        ->update('tr_job_comment', ['ListEmployee' => $readcomment], 'JobID = ' . $id . '')
        ->execute();
    }
    //End read comment

    $searchmodelComments = new JobCommentSearch([
      'JobID' => !empty($id) ? $id : $modelComments->JobID,
    ]);
    $dataProviderComments = $searchmodelComments->search(Yii::$app->request->queryParams);

    return $this->renderAjax('viewComments', [
      'dataProviderComments' => $dataProviderComments,
      'JobID' => $modelComments->JobID,
    ]);
  }
  public function actionCommentsave()
  {
    $post = Yii::$app->request->post();
    $html = '';
    $modelComments = new JobComment();
    if ($modelComments->load($post)) {
      $user = CommonHelper::getUserIndentity();

      $modelComments->CreatedBy = $user->Id;
      if ($modelComments->save()) {
        $html = '';
      }
    }
    return $html;
  }




  protected function Email($sendto, $subject, $modelJob, $status = "", $layouts = 'layouts/newjobsapproval')
  {
    Yii::$app->cache->flush();

    $from = Yii::$app->params['appNotificationEmail'];
    $cc = Yii::$app->params['appNotificationEmailCC'];

    $to = array();
    $to[$sendto] = $sendto;

    if (!empty($to)) {
      $to = $to;
      $html = ['html' => $layouts];
      $html_bind = ['title' => $subject, 'modelJob' => $modelJob, 'status' => $status];

      $compose = Yii::$app->mailer->compose($html, $html_bind);
      $compose->setFrom($from);
      $compose->setTo($to);
      if (!empty($cc)) {
        $compose->setCc($cc);
      }
      $compose->setSubject($subject);

      if ($compose->send()) {
        return "true";
      } else {
        return "false";
      }
    }
  }

  /* END RAPIH */
}
