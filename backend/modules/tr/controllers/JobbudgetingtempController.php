<?php

namespace backend\modules\tr\controllers;

use Yii;
use common\models\tr\JobBudgetingTemp;
use common\models\tr\search\JobBudgetingTempSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JobbudgetingtempController implements the CRUD actions for JobBudgetingTemp model.
 */
class JobbudgetingtempController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all JobBudgetingTemp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobBudgetingTempSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JobBudgetingTemp model.
     * @param integer $JobID
     * @param string $EmployeeID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($JobID, $EmployeeID)
    {
        return $this->render('view', [
            'model' => $this->findModel($JobID, $EmployeeID),
        ]);
    }

    /**
     * Creates a new JobBudgetingTemp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JobBudgetingTemp();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'JobID' => $model->JobID, 'EmployeeID' => $model->EmployeeID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing JobBudgetingTemp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $JobID
     * @param string $EmployeeID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($JobID, $EmployeeID)
    {
        $model = $this->findModel($JobID, $EmployeeID);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'JobID' => $model->JobID, 'EmployeeID' => $model->EmployeeID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing JobBudgetingTemp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $JobID
     * @param string $EmployeeID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($JobID, $EmployeeID)
    {
        $this->findModel($JobID, $EmployeeID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JobBudgetingTemp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $JobID
     * @param string $EmployeeID
     * @return JobBudgetingTemp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($JobID, $EmployeeID)
    {
        if (($model = JobBudgetingTemp::findOne(['JobID' => $JobID, 'EmployeeID' => $EmployeeID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
