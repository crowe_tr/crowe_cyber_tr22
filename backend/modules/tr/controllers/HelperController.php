<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

use common\components\CommonHelper;
use common\components\TimeReportHelper;

use common\models\cl\Client;
use common\models\hr\Employee;
use common\models\tr\JobBudgeting;
use common\models\tr\JobBudgetingTemp;

class HelperController extends Controller
{
    public $layout;
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSrcclientopt($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = Client::find()
                ->select('Id as id, Name as text, OfficeCP, OfficePhone, OfficeFax, OfficeAddress')
                ->where(
                  "OfficeCP LIKE '%{$q}%' OR Name LIKE '%{$q}%' OR Id = '{$q}' "
                )
                ->limit(10)
                ->asArray()
                ->all();
            $out['results'] = array();
            foreach ($model as $data) {
                $name = $data['text'];
                $cp = $data['OfficeCP'];
                $phone = $data['OfficePhone'];
                $fax = $data['OfficeFax'];
                $address = $data['OfficeAddress'];

                $array = array(
                          'id' => $data['id'],
                          'text' => $data['id'].' - '.$name,
                    );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Client::findOne($id)->Name];
        }

        return $out;
    }
    public function actionSrcemployeeopt($q = null, $id = null , $entity = null, $division=null, $grup=null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
          if ($entity != null OR $division != null) {
            if ($grup != null) {
              $model = Employee::find()

              ->where(
                "parentID = '{$grup}'  AND fullName LIKE '%{$q}%' OR Id LIKE '%{$q}%'"
                )
                ->all();
            }
            else{
              $model = Employee::find()

              ->where(
                "entityId ='{$entity}' OR divisionID = '{$division}' AND fullName LIKE '%{$q}%' OR Id LIKE '%{$q}%'"
                )
                ->all();
            }
          }

          elseif ($grup != null) {
            $model = Employee::find()
            ->joinwith('level')
            ->where(
              "parentID = '{$grup}'  AND hrEmployee.fullName LIKE '%{$q}%' OR hrEmployee.Id LIKE '%{$q}%' OR cmLevel.levelName LIKE '{$q}'"
              )
              ->all();
          }
          elseif ($entity == null AND $division == null AND $grup == null) {
            $model = Employee::find()
            ->joinwith('level')
            ->where(
              "hrEmployee.fullName LIKE '%{$q}%' OR hrEmployee.Id LIKE '%{$q}%' OR cmLevel.levelName LIKE '{$q}'"
              )
              ->all();
          }
            // $model = Employee::find()
            //     ->select('Id as id, fullName as text, entityId')
            //     ->where(
            //       "entityId ='{$entity}' OR divisionID = '{$division}' OR parentID ='{$grup}' AND fullName LIKE '%{$q}%' OR Id = '{$q}'"
            //     )
            //     ->limit(10)
            //     ->asArray()
            //     ->all();
            $out['results'] = array();
            foreach ($model as $key => $data) {
                // $fullName = $data['text'];
                $array = array(
                          'id' => $data['id'],
                          'text' => $data['id'].' - '.$data['fullName'].' - '.$data['level']['levelName'],
                    );
                array_push($out['results'], $array);
            }
        }

        elseif ($grup != null) {
          if ($grup != null) {
            $model = Employee::find()
            ->where(
              "parentID = '{$grup}'  AND fullName LIKE '%{$q}%' OR Id = '{$q}'"
              )

              ->all();
          }

            // $model = Employee::find()
            //     ->select('Id as id, fullName as text, entityId')
            //     ->where(
            //       "entityId ='{$entity}' OR divisionID = '{$division}' OR parentID ='{$grup}' AND fullName LIKE '%{$q}%' OR Id = '{$q}'"
            //     )
            //     ->limit(10)
            //     ->asArray()
            //     ->all();
            $out['results'] = array();
            foreach ($model as $key => $data) {
                // $fullName = $data['text'];
                $array = array(
                          'id' => $data['id'],
                          'text' => $data['id'].' - '.$data['fullName'].' - '.$data['level']['levelName'],
                    );
                array_push($out['results'], $array);
            }
        }

         elseif ($id > 0) {
            $data = Employee::findOne($id);
            $out['results'] = ['id' => $id, 'text' => $data->Id.' - '.$data->fullName];
        }
        return $out;
    }

    public function actionSrcemployejob($q=null , $JobID =null)
    {
      \Yii::$app->response->format = Response::FORMAT_JSON;
      $out = ['results' => ['id' => '', 'text' => '']];
      if (!is_null($q)) {
            // $model = JobBudgeting::find()
            // ->where(
            //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
            //   )
            //   // ->limit(10)
            //   // ->asArray()
            //   ->all();

            $model = JobBudgeting::find()
            ->where(
              "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
              )
            ->all();

          $out['results'] = array();
          foreach ($model as $key => $data) {
            // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
              // $fullName = $data['text'];
              // print_r($data['employee']['fullName']);
              $array = array(
                        'id' => $data['EmployeeID'],
                        'text' => $data['EmployeeID'].' - '.$data['employee']['fullName'].' - '.$data['employee']['level']['levelName'],
                  );
              array_push($out['results'], $array);
          }
      }

      elseif (is_null($q)) {
            // $model = JobBudgeting::find()
            // ->where(
            //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
            //   )
            //   // ->limit(10)
            //   // ->asArray()
            //   ->all();

            $model = JobBudgeting::find()
            ->where(
              "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
              )
            ->all();

          $out['results'] = array();
          foreach ($model as $key => $data) {
            // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
              // $fullName = $data['text'];
              // print_r($data['employee']['fullName']);
              $array = array(
                        'id' => $data['EmployeeID'],
                        'text' => $data['EmployeeID'].' - '.$data['employee']['fullName'].' - '.$data['employee']['level']['levelName'],
                  );
              array_push($out['results'], $array);
          }
      }

       elseif ($id > 0) {
          $data = JobBudgeting::findOne($id);
          $out['results'] = ['id' => $id, 'text' => $data->JobID.' - '.$data->employee->fullName.' - '.$data->employee->level->levelName];
      }
      return $out;
    }

    public function actionSrcemployemanager($q=null , $JobID =null)
    {


      \Yii::$app->response->format = Response::FORMAT_JSON;
      $out = ['results' => ['id' => '', 'text' => '']];
      if (!is_null($q)) {
            // $model = JobBudgeting::find()
            // ->where(
            //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
            //   )
            //   // ->limit(10)
            //   // ->asArray()
            //   ->all();

            $model = Employee::find()
            ->where(
              "full_name LIKE '%{$q}%'"
              )
            ->all();

          $out['results'] = array();
          foreach ($model as $key => $data) {
            // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
              // $fullName = $data['text'];
              // print_r($data['employee']['fullName']);
              $array = array(
                    'id' => $data['user_id'],
                    'text' => $data['user_id'].' - '.$data['full_name'].' - '.$data['level']['level_name'],
                  );
              array_push($out['results'], $array);
          }
      }

      elseif (is_null($q)) {
            // $model = JobBudgeting::find()
            // ->where(
            //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
            //   )
            //   // ->limit(10)
            //   // ->asArray()
            //   ->all();

            $model = Employee::find()
            ->where(
              "fullName LIKE '%{$q}%'"
              )
            ->all();

          $out['results'] = array();
          foreach ($model as $key => $data) {
            // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
              // $fullName = $data['text'];
              // print_r($data['employee']['fullName']);
              $array = array(
                        'id' => $data['Id'],
                        'text' => $data['Id'].' - '.$data['fullName'].' - '.$data['level']['levelName'],
                  );
              array_push($out['results'], $array);
          }
      }

       elseif ($id > 0) {
          $data = JobBudgeting::findOne($id);
          $out['results'] = ['id' => $id, 'text' => $data->JobID.' - '.$data->employee->fullName.' - '.$data->employee->level->levelName];
      }
      return $out;
    }

    public function actionSrcemployejobtemp($q=null , $JobID =null)
    {
      \Yii::$app->response->format = Response::FORMAT_JSON;
      $out = ['results' => ['id' => '', 'text' => '']];
      if (!is_null($q)) {
            // $model = JobBudgeting::find()
            // ->where(
            //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
            //   )
            //   // ->limit(10)
            //   // ->asArray()
            //   ->all();

            $model = JobBudgetingTemp::find()
            ->where(
              "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
              )
            ->all();

          $out['results'] = array();
          foreach ($model as $key => $data) {
            // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
              // $fullName = $data['text'];
              // print_r($data['employee']['fullName']);
              $array = array(
                        'id' => $data['EmployeeID'],
                        'text' => $data['EmployeeID'].' - '.$data['employee']['fullName'].' - '.$data['employee']['level']['levelName'],
                  );
              array_push($out['results'], $array);
          }
      }

      elseif (is_null($q)) {
            // $model = JobBudgeting::find()
            // ->where(
            //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
            //   )
            //   // ->limit(10)
            //   // ->asArray()
            //   ->all();

            $model = JobBudgetingTemp::find()
            ->where(
              "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
              )
            ->all();

          $out['results'] = array();
          foreach ($model as $key => $data) {
            // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
              // $fullName = $data['text'];
              // print_r($data['employee']['fullName']);
              $array = array(
                        'id' => $data['EmployeeID'],
                        'text' => $data['EmployeeID'].' - '.$data['employee']['fullName'].' - '.$data['employee']['level']['levelName'],
                  );
              array_push($out['results'], $array);
          }
      }

       elseif ($id > 0) {
          $data = JobBudgetingTemp::findOne($id);
          $out['results'] = ['id' => $id, 'text' => $data->JobID.' - '.$data->employee->fullName.' - '.$data->employee->level->levelName];
      }
      return $out;
    }

    public function searchJobTeam($q, $where=null)
    {
            /*
        $model = JobBudgeting::find()
        ->JoinWith('employee')
        ->select('hrEmployee.Id as id, hrEmployee.Name as text')
        ->where(
          "
            (hrEmployee.Name LIKE '%{$q}%' OR hrEmployee.Id = '{$q}')
          "
        )
        ->limit(10)
        ->asArray()
        ->all();
        */
        $model = Employee::find()
        ->select('Id as id, Name as text')
        ->where(
          "Name LIKE '%{$q}%' OR Id = '{$q}' "
        )
        ->limit(10)
        ->asArray()
        ->all();

        return $model;
    }
    public function actionSrcPartneropt($q = null, $id = null, $where=null)
    {
            \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = $this->searchJobTeam($q);
            $out['results'] = array();
            foreach ($model as $data) {
                $name = $data['text'];
                $array = array(
                          'id' => $data['id'],
                          'text' => $data['id'].' - '.$name,
                    );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $data = Employee::findOne($id);
            $out['results'] = ['id' => $id, 'text' => $data->Id.' - '.$data->Name];
        }
        return $out;
    }
    public function actionSrcmanageropt($q = null, $id = null, $where=null)
    {
            \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = $this->searchJobTeam($q);
            $out['results'] = array();
            foreach ($model as $data) {
                $name = $data['text'];
                $array = array(
                          'id' => $data['id'],
                          'text' => $data['id'].' - '.$name,
                    );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $data = Employee::findOne($id);
            $out['results'] = ['id' => $id, 'text' => $data->Id.' - '.$data->Name];
        }
        return $out;
    }
    public function actionSrcsupervisoropt($q = null, $id = null, $where=null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = $this->searchJobTeam($q);
            $out['results'] = array();
            foreach ($model as $data) {
                $name = $data['text'];
                $array = array(
                          'id' => $data['id'],
                          'text' => $data['id'].' - '.$name,
                    );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $data = Employee::findOne($id);
            $out['results'] = ['id' => $id, 'text' => $data->Id.' - '.$data->Name];
        }
        return $out;
    }

    public function actionLoaddepartement()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = CmDept::find()->andWhere(['CmMsCpOffice_Id' => $id])->asArray()->all();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['Id'], 'name' => $ls['Departement']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);

                return;
            }
        }
        echo json_encode(['output' => array(), 'selected' => '']);
    }
}
