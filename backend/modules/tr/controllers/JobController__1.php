<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\helpers\Html;

use common\components\CommonHelper;
use common\components\TimeReportHelper;

use common\models\tr\Job;
use common\models\tr\JobBudgeting;
use common\models\tr\JobTemp;
use common\models\tr\JobBudgetingTemp;
use common\models\tr\JobComment;
use common\models\tr\TrPreviousChangesJob;
use common\models\tr\ms\Entity;
use common\models\tr\search\JobSearch;
use common\models\tr\search\JobviewSearch;
use common\models\tr\search\JobBudgetingSearch;
use common\models\tr\search\JobBudgetingTempSearch;
use common\models\tr\search\JobCommentSearch;
use common\models\cl\Client;
use common\models\hr\VempGroup;
use common\models\hr\Employee;
use  yii\data\ArrayDataProvider;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class JobController extends Controller
{
  protected function findModel($id)
  {
    if (($model = Job::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException('The requested page does not exist.');
  }

  protected function findModelTemp($id)
  {
    if (($model = JobTemp::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException('The requested page does not exist.');
  }
  protected function findModelBudget($EmployeeID, $JobID, $param = [])
  {
    if (($modelBudget = JobBudgeting::find()->where(['EmployeeID' => $EmployeeID, 'JobID' => $JobID])->one()) !== null) {
      return $modelBudget;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  protected function findModelEmployee($JobID, $param = [])
  {
    if (($modelBudget = JobBudgeting::find()->where(['JobID' => $JobID])->all()) !== null) {
      return $modelBudget;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }


  protected function findModelBudgetTemp($EmployeeID, $JobID, $param = [])
  {
    if (($modelBudget = JobBudgetingTemp::find()->where(['EmployeeID' => $EmployeeID, 'JobID' => $JobID])->one()) !== null) {
      return $modelBudget;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  protected function findModelHistoryJob($JobID)
  {
    if ($model = TrPreviousChangesJob::find()->where(['JobID' => $JobID])->all()) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }


  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }


  public function actionAll()
  {
    return $this->index(9);
  }
  public function actionIndex()
  {
    return $this->index(8);
  }
  public function actionDraft()
  {
    return $this->index(8);
  }
  public function actionSubmit()
  {
    return $this->index(Job::StatusSubmit);
  }
  public function actionApproved()
  {
    return $this->index(Job::StatusApproved);
  }
  public function actionOnprogress()
  {
    return $this->index(Job::StatusOnProgress, 'op');
  }
  public function actionDenied()
  {
    return $this->index(Job::StatusDenied);
  }
  public function actionCanceled()
  {
    return $this->index(Job::StatusCanceled);
  }
  public function actionSuccess()
  {
    return $this->index(Job::StatusSuccess, 's');
  }
  public function IndexFilter()
  {
    $session = Yii::$app->session;
    $session->open();

    $post = Yii::$app->request->post();
    $model = new JobviewSearch();
    if ($model->load($post)) {
      $data = [];
      $data['job_code'] = $model->job_code;
      $session->set('__trjob_index', $data);
    }
  }
  public function index($status = '', $desc = '')
  {
    echo "<div class='text-center'>";
    print_r($desc);
    echo "</div>";
    $user = CommonHelper::getUserIndentity();
    $post = Yii::$app->request->post();
    $session = Yii::$app->session;
    $data = array();
    $this->IndexFilter();

    $param = isset($_SESSION['__trjob_index']) ? $_SESSION['__trjob_index'] : [];
    $param['job_code'] = isset($param['job_code']) ? $param['job_code'] : '';
    $searchModel = new JobviewSearch();
    $searchModel->job_code = $param['job_code'];

    var_dump($searchModel->description);

    die();
    if ($user->is_admin != 1) {
      $searchModel->manager_id = $user->user_id;
      $searchModel->created_by = $user->user_id;
    }


    if ($status != 9) {
      if ($status == 8) {
        $searchModel->job_status = 0;
      } else {
        $searchModel->job_status = $status;
        // $searchModel->Status = 1;
      }
    } else {
      $searchModel->job_status = $status;
    }

    $dataProvider = $searchModel->search(Yii::$app->request->post());
    var_dump($dataProvider);
    die();

    // var_dump($dataProvider);



    return $this->render('job', [
      'OrderButtons' => Job::getButtonTotalJobFromStatus($status),
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'data' => $data,
      // 'param' => $param,
    ]);
  }

  public function getDataForForm($model)
  {
    $data = array();
    $data['Division'] = TimeReportHelper::getDivision();
    $data['Entity'] = TimeReportHelper::getEntity();
    $data['Client'] = TimeReportHelper::getClient();
    $data['Employee'] = TimeReportHelper::getEmployee();
    // $data['VempGroup'] = TimeReportHelper::getVempGroup();
    // $data['EmployeeJob'] =TimeReportHelper::getEmployeeJob();
    return $data;
  }

  public function actionRequest($id = '')
  {

    $user = CommonHelper::getUserIndentity();
    if (empty($id)) {
      $modelJob = new Job();
      $modelJob->CreatedBy = $user->Id;
      $modelJob->Flag = 0;
      $modelJob->Status = 0;
    } else {
      $modelJob = $this->findModel($id);
      switch ($modelJob->Status) {
        case 1:
          $status = 'NEW VERSION';
          break;
        case 3:
          $status = 'REVISED VERSION';
          break;
        default:
          $status = 'NEW VERSION';
      }
      $modelJob->UpdateBy = $user->Id;
      $modelJob->scenario = 'update';
      $modelJob->JobArea = explode(',', $modelJob->JobArea);

      if ($modelJob->Status != 0) {
        if ($modelJob->Status != 3) {
          return $this->redirect(['view', 'id' => $modelJob->JobID]);
        }
      }
    }
    $data = $this->getDataForForm($modelJob);
    $searchModelBudgeting = new JobBudgetingSearch();
    $searchModelBudgeting->JobID = $modelJob->JobID;
    $modelBudgeting = $searchModelBudgeting->search($modelJob->JobID);

    $post = Yii::$app->request->post();
    if ($modelJob->load($post)) {
      $valid = $modelJob->validate();
      if ($valid) {

        $submittedType = \Yii::$app->request->post('submit');
        switch (isset($submittedType[0]) ? $submittedType[0] : 0) {
            //your code
          case '0':
            $modelJob->Status = 0;
            //my code
            break;

          case '1':
            $modelJob->Status = 1;
        }


        $modelJob->Fee = empty($modelJob->Fee) ? 0 : str_replace(',', '', $modelJob->Fee);
        $modelJob->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
        $modelJob->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
        $modelJob->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
        $modelJob->OtherExpenseAllowance = empty($modelJob->OtherExpenseAllowance) ? 0 : str_replace(',', '', $modelJob->OtherExpenseAllowance);
        $modelJob->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);

        $modelJob->JobArea = empty($modelJob->JobArea) ? '' : implode(',', $modelJob->JobArea);

        $modelJob->Total =  $modelJob->Fee;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
          if ($flag = $modelJob->save(false)) {
          } else {
            $transaction->rollBack();
          }

          if ($flag) {
            $transaction->commit();
            if ($modelJob->isNewRecord) {
              return $this->redirect(['request', 'id' => $modelJob->JobID]);
            } elseif ($submittedType == null) {
              return $this->redirect(['request', 'id' => $modelJob->JobID]);
              // code...
            } else {
              $submittedType = \Yii::$app->request->post('submit');
              switch ($submittedType[0]) {
                  //your code
                case '0':
                  return $this->redirect(['view', 'id' => $modelJob->JobID]);
                  //my code
                  break;

                case '1':
                  try {
                    $send_email = $this->Email($modelJob->partner->email, 'THERE IS A NEW JOB NEEDS YOUR APPROVAL', $modelJob, $status);
                    if ($send_email == 'true') {
                      return $this->redirect(['view', 'id' => $modelJob->JobID]);
                    }
                  } catch (\Exception $e) {
                    return $this->redirect(['view', 'id' => $modelJob->JobID]);
                  }
              }
            }
          }
        } catch (Exception $e) {
          $transaction->rollBack();
        }
      }
    }
    return $this->render('jobForm', [
      'data' => $data,
      'modelJob' => $modelJob,
      'modelBudgeting' => $modelBudgeting,
    ]);
  }

  public function actionDelete($id)
  {
    $modelJob = $this->findModel($id);
    $modelJob->delete();
    // var_dump($modelJob);
    // die();
    try {
      return $this->redirect(['index']);
    } catch (\Exception $e) {
      return $this->redirect(['index']);
    }
  }

  public function actionView($id, $comment = '', $frommail = 0)
  {
    if ($frommail == 1) {
      return $this->frommail($id, $comment = '');
    } else {
      return $this->view($id, $comment = '');
    }
  }
  public function view($id, $comment = '')
  {
    $post = Yii::$app->request->post();
    $user = CommonHelper::getUserIndentity();

    $modelJob = $this->findModel($id);
    $modelComments = new JobComment();
    $modelComments->JobID = $id;

    // $param['isCurrentApprover'] = ($modelJob->Partner == $user->EmployeeID) ? true : false;
    // $param['isRequester'] = ($modelJob->Manager == $user->EmployeeID) ? true : false;
    //$param['budget'] = Job::countTotalBudget($modelJob);

    $searchModelBudget = new JobBudgetingSearch([
      'JobID' => $id,
    ]);
    $dataProviderBudget = $searchModelBudget->search(Yii::$app->request->queryParams);
    $dataProviderBudget->pagination->pageSize=100;

    $count = JobComment::find()
      ->select(['COUNT(*) AS jml'])
      ->where(['JobID' => $id])
      ->andWhere(['like', 'ListEmployee', $user->Id])
      ->count();
    $commentCount['count'] = $count;
    // var_dump($commentCount['count']);

    if (!empty($id)) {
      // code...
      // $sql = "call JobEstimated('".$id."')";
      // $task = Yii::$app->db->createCommand($sql)->queryOne();
      // $modelTask['TimeCharges'] =  $task['TimeCharges'];
      // $modelTask['RecoveryRate'] =  $task['RecoveryRate'];
      // $modelTask['MealAllowance'] =  $task['MealAllowance'];
      // $modelTask['TaxiAllowance'] =  $task['TaxiAllowance'];
      // $modelTask['OutOfOfficeAllowance'] =  $task['OutOfOfficeAllowance'];
      // $modelTask['TimeCharges'] =  0;
      $modelTask['RecoveryRate'] =  0;
      $modelTask['MealAllowance'] =  0;
      $modelTask['TaxiAllowance'] =  0;
      $modelTask['OutOfOfficeAllowance'] =  0;
    } else {
      // $modelTask['TimeCharges'] =  0;
      $modelTask['RecoveryRate'] =  0;
      // $modelTask['OutOfOfficeAllowance'] =  0;
      // $modelTask['TaxiAllowance'] =  0;
      // $modelTask['OutOfOfficeAllowance'] =  0;

    }

    $sql = "call job_actual('" . $modelJob->JobCode . "');";
    $actual = Yii::$app->db->createCommand($sql)->queryone();


    return $this->render('view', [
      'modelJob' => $modelJob,
      'modelComments' => $modelComments,
      'searchModelBudget' => $searchModelBudget,
      'dataProviderBudget' => $dataProviderBudget,
      'modelTask' => $modelTask,
      'commentCount' => $commentCount,
      'actual' => $actual
      // 'param' => $param,
    ]);
  }
  public function actionBudget($id = null, $action = null)
  {
    $session = Yii::$app->session;
    $session->open();

    $post = Yii::$app->request->post();
    $model = new Job();
    $model->load($post);
    $modelJob = Job::find()->where(['JobID' => $model->JobID])->one();
    if ($modelJob->load($post)) {
      $session->set('_trjob_form' . $modelJob->JobID, $modelJob);
      if (Yii::$app->request->isAjax) {
        if (!empty($id)) {
          $modelBudget = $this->findModelBudget($id, $modelJob->JobID);
        } else {
          $modelBudget = new JobBudgeting();
          $modelBudget->JobID = $modelJob->JobID;
          if (empty($action)) {
            // $modelBudget->EmployeeID = null;
            $modelBudget->isNewRecord;
          }
        }
        $data = $this->getDataForForm($modelJob);
        return $this->renderAjax('jobFormDetail', [
          'modelJob' => $modelJob,
          'modelBudget' => $modelBudget,
          'id' => empty($action) ? $id : "",
          'data' => $data
        ]);
      } else {
        throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }
  }

  public function actionBudgetsave($id = null)
  {

    $user = CommonHelper::getUserIndentity();

    $post = Yii::$app->request->post();
    $modelBudget = new JobBudgeting();
    if ($modelBudget->load($post)) {


      if (!empty($id)) {
        $modelBudget = $this->findModelBudget($id, $modelBudget->JobID);
        $modelBudget->load($post);

        // print_r($modelBudget->OverTime);
      }
      $valid = $modelBudget->validate();
      if ($valid) {
        $modelBudget->CreatedBy = $user->Id;
        if ($modelBudget->OverTime == null) {
          $modelBudget->OverTime = 0;
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
          if (!($flag = $modelBudget->save(false))) {
            $transaction->rollBack();
            $return = 'error : rollback';
          }
          if ($flag) {
            $transaction->commit();
            $return = true;
          }
        } catch (Exception $e) {
          $transaction->rollBack();
        }
      } else {
        $return = Html::errorSummary($modelBudget, ['encode' => true]);
      }
    } else {
      $return = 'error : model not loaded';
    }

    return $return;
  }
  public function actionBudgetdelete()
  {
    $return = false;
    $post = Yii::$app->request->post();

    $EmployeeID = $post['EmployeeID'];
    $JobID = $post['JobID'];
    $modelBudget = $this->findModelBudget($EmployeeID, $JobID);

    $transaction = \Yii::$app->db->beginTransaction();
    try {
      if ($modelBudget->delete()) {
        $transaction->commit();
        $return = true;
      } else {
        $transaction->rollback();
      }
    } catch (Exception $e) {
      $transaction->rollBack();
    }

    return $return;
  }
  public function actionOtherexpense($id = null, $action = null)
  {
    if (Yii::$app->request->isAjax) {
      $post = Yii::$app->request->post();
      $user = CommonHelper::getUserIndentity();

      $modelJob = new Job();
      $modelJob->load($post);

      return $this->renderAjax('jobOthersExpense', [
        'modelJob' => $modelJob,
      ]);
    } else {
      throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
    }
  }
  public function actionComments($id = '')
  {
    // var_dump($id);
    $user = CommonHelper::getUserIndentity();
    $modelComments = new JobComment();
    $post = Yii::$app->request->post();
    $modelComments->load($post);

    //Start read comment
    $count = JobComment::find()
      ->select(['COUNT(*) AS jml'])
      ->where(['JobID' => $id])
      ->andWhere(['like', 'ListEmployee', $user->Id])
      ->count();

    if ($count != 0) {
      $modelCommentsRead = JobComment::find()
        ->where(['JobID' => $id])
        ->andWhere(['like', 'ListEmployee', $user->Id])->one();
      foreach (explode(',', $modelCommentsRead->ListEmployee) as $key => $value) {
        if ($value != $user->Id) {
          $data[] = $value;
        }
      }
      $readcomment = implode(', ', $data);

      $modelCommentsRead->ListEmployee = $readcomment;
      Yii::$app->db->createCommand()
        ->update('trJobComment', ['ListEmployee' => $readcomment], 'JobID = ' . $id . '')
        ->execute();
    }
    //End read comment

    $searchmodelComments = new JobCommentSearch([
      'JobID' => !empty($id) ? $id : $modelComments->JobID,
    ]);
    $dataProviderComments = $searchmodelComments->search(Yii::$app->request->queryParams);

    return $this->renderAjax('viewComments', [
      'dataProviderComments' => $dataProviderComments,
      'JobID' => $modelComments->JobID,
    ]);
  }
  public function actionCommentsave()
  {
    $post = Yii::$app->request->post();
    $html = '';
    $modelComments = new JobComment();
    if ($modelComments->load($post)) {
      $user = CommonHelper::getUserIndentity();

      $modelComments->CreatedBy = $user->Id;
      if ($modelComments->save()) {
        $html = '';
      }
    }
    return $html;
  }


  public function actionLatesjob()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();

    $modelJob = new Job();
    $modelJob->load($post);

    $result = \Yii::$app->db->createCommand("CALL getJobNo(:paramName1)")
      ->bindValue(':paramName1', $modelJob->ClientID);
    $data['job'] = $result->queryScalar();
    $data['ClientID'] = $modelJob->ClientID;
    // var_dump($modelJob->ClientID);

    return $data;
  }

  public function actionGrupdata()
  {
    $out = [];
    if (isset($_POST['depdrop_parents'])) {

      $id = $_POST['depdrop_parents'];

      $list = VempGroup::find()->orWhere(['or', ['entityID' => $id[0]], ['divisionID' => $id[1]]])->asArray()->all();
      $listall = VempGroup::find()->asArray()->all();

      if ($id != null && count($list) != 0) {
        $selected = '';
        foreach ($list as $i => $ls) {
          $out[] = ['id' => $ls['id'], 'name' => $ls['id'] . ' - ' . $ls['fullName'] . ' - ' . $ls['levelName']];
        }
        echo json_encode(['output' => $out, 'selected' => $selected]);
      } else {
        if ($id[0] == null and $id[1] == null) {
          $selected = '';
          foreach ($listall as $i => $ls) {
            $out[] = ['id' => $ls['id'], 'name' => $ls['id'] . ' - ' . $ls['fullName'] . ' - ' . $ls['levelName']];
          }
          echo json_encode(['output' => $out, 'selected' => $selected]);
        } else {
          echo json_encode(['output' => array(), 'selected' => '']);
        }
      }
    }
  }

  public function actionTotalwh()
  {
    $post = Yii::$app->request->post();
    $modelJob = new JobBudgeting();
    $modelJob->load($post);

    $sql = "call getValueTaskAE('" . $modelJob->EmployeeID . "','" . $modelJob->Total . "')";
    $task = Yii::$app->db->createCommand($sql)->queryOne();

    return json_encode($task);
  }

  public function GetEmployeeAvailable($id)
  {
    $sql = "call antTrJobEmployeeAvailableK('{$id}')";
    $data = Yii::$app->db->createCommand($sql)->queryAll();
    $html = "";
    if (!empty($data)) {
      $html .= "<div class='well table-responsive' style='margin: 10px 0 30px; max-height : 130px;'>";
      $html .= "<table class='table table-primary'>";
      $html .= "<tr>
                  <td class='bg-primary'>No</td>
                  <td class='bg-primary'>Client</td>
                  <td class='bg-primary'>Description</td>
                  <td class='bg-primary'>WH</td>
              </tr>";
      $no = 1;
      foreach ($data as $d) {
        $html .= "<tr>";
        $html .= "<td>" . $no++ . "</td>";
        $html .= "<td>" . $d['Client'] . "</td>";
        $html .= "<td>" . $d['Description'] . "</td>";
        $html .= "<td>" . $d['WH'] . "</td>";
        $html .= "<tr>";
      }

      $html .= "</table>";
      $html .= "</div>";
    } else {
      $html .= "";
    }
    return $html;
  }

  public function actionGetlevelid()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobBudgeting();
    $modelJob->load($post);

    $data['fullName'] = $modelJob->employee->fullName;
    $data['levelName'] = $modelJob->employee->level->levelName;

    // $sql = "call antTrJobEmployeeAvailableK('".$modelJob->EmployeeID."')";
    // $task = Yii::$app->db->createCommand($sql)->queryOne();
    $data['eA'] = $this->GetEmployeeAvailable($modelJob->EmployeeID);

    // var_dump($EA);
    // die();

    return $data;
  }

  public function actionOutofficeallowance()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new Job();
    $modelJob->load($post);

    $model = Job::find()->where(['JobID' => $modelJob->JobID])->one();
    $model->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
    $model->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
    $model->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
    $model->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);
    $model->OtherExpenseAllowance = empty($modelJob->OtherExpenseAllowance) ? 0 : str_replace(',', '', $modelJob->OtherExpenseAllowance);

    // print_r($model->OutOfOfficeAllowance);

    if ($model->save(false)) {

      $modelAllow = $this->findModel($model->JobID);

      return $modelAllow;
      // code...
    }
  }

  public function actionApprove($id = "")
  {
    if (!empty($id)) {
      $modelJob = $this->findModel($id);
      // $modelJob->Status = 2;
      if ($modelJob->save(false)) {
        $modelBudget = $this->findModelEmployee($id);
        // $send_email = $this->Email($modelJob->partner->email, 'THERE IS A NEW JOB NEEDS YOUR APPROVAL' ,$modelJob);
        return $this->redirect(['view', 'id' => $modelJob->JobID]);
      }

      // print_r($modelJob);
      // $modelJob->UpdateBy = $user->Id ;
    }
  }

  public function actionReject($id = "")
  {
    if (!empty($id)) {
      $modelJob = $this->findModel($id);
      $modelJob->Status = 3;
      if ($modelJob->save(false)) {
        return $this->redirect(['view', 'id' => $modelJob->JobID]);
      }
    }
  }

  public function actionBack($id = "")
  {
    return $this->redirect('all');
  }




  public function actionReqsave()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new Job();
    if ($modelJob->load($post)) {
      $model = $this->findModel($modelJob->JobID);
      if ($model->load($post)) {
        $model->Fee = empty($modelJob->Fee) ? 0 : str_replace(',', '', $modelJob->Fee);
        $model->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
        $model->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
        $model->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
        $model->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);
        $model->JobArea = empty($modelJob->JobArea) ? [] : implode(',', $modelJob->JobArea);
        if ($model->save(false)) {
          return "save";
        }
      }
    }
  }
  public function actionReqsaveallowance()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new Job();
    if ($modelJob->load($post)) {
      $model = $this->findModel($modelJob->JobID);
      $model->IsMeal = $modelJob->IsMeal;
      $model->IsTaxi = $modelJob->IsTaxi;
      $model->IsOutOfOffice = $modelJob->IsOutOfOffice;
      if ($modelJob->IsMeal != 1) {
        $model->MealAllowance = 0;
      }
      if ($modelJob->IsTaxi != 1) {
        $model->TaxiAllowance = 0;
      }
      if ($modelJob->IsOutOfOffice != 1) {
        $model->OutOfOfficeAllowance = 0;
      }

      if ($model->save(false)) {
        return $model;
      }
    }
  }

  public function actionAllowance()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new Job();
    if ($modelJob->load($post)) {
      // return $modelJob->IsTaxi;
      $model = $this->findModel($modelJob->JobID);
      if ($modelJob->IsMeal != 1) {
        $model->MealAllowance = 0;
      }
      if ($modelJob->IsOutOfOffice != 1) {
        $model->OutOfOfficeAllowance = 0;
      }
      if ($modelJob->IsTaxi != 1) {
        $model->TaxiAllowance = 0;
      }

      if ($model->save(true)) {
        return $model;
      }
    }
  }


  public function actionDefault()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new Job();
    if ($modelJob->load($post)) {
      // code...
      $sql = "call JobEstimated('" . $modelJob->JobID . "', '1' , '0')";
      $task = Yii::$app->db->createCommand($sql)->queryOne();

      if ($task) {

        $model = $this->findModel($modelJob->JobID);
        return $model;
      }
      // return $task;
      // if ($modelJob->JobID) {
      //   // code...
      // }

    }
  }




  public function actionRevisi($id = "")
  {

    $user = CommonHelper::getUserIndentity();

    $model = $this->findModel($id);

    if ($model->Flag == 0) {
      $model->Flag = 1;
      $model->save();
      $modelJob = $this->findModelTemp($model->JobID);
    } else {
      $modelJob = $this->findModelTemp($id);
    }


    switch ($modelJob->Status) {
      case 1:
        $status = 'NEW VERSION';
        break;
      case 2:
        $status = 'REVISED VERSION';
        break;
    }
    $modelJob->UpdateBy = $user->Id;
    $modelJob->JobArea = explode(',', $modelJob->JobArea);

    $modelAS = $this->findModel($id);
    $data = $this->getDataForForm($modelJob);
    $searchModelBudgeting = new JobBudgetingTempSearch();
    $searchModelBudgeting->JobID = $modelJob->JobID;
    $modelBudgeting = $searchModelBudgeting->search($modelJob->JobID);
    $modelBudgeting->pagination->pageSize=100;

    $post = Yii::$app->request->post();
    if ($modelJob->load($post)) {
      $valid = $modelJob->validate();
      if ($valid) {

        $submittedType = \Yii::$app->request->post('submit');
        switch ($submittedType[0]) {
          case '0':
            $modelJob->Flag = 1;
            $modelAS->Flag = 1;
            break;
          case '1':
            $modelAS->Flag = 2;
            $modelJob->Flag = 2;
            break;
        }
        $modelJob->Fee = empty($modelJob->Fee) ? 0 : str_replace(',', '', $modelJob->Fee);
        $modelJob->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
        $modelJob->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
        $modelJob->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
        $modelJob->OtherExpenseAllowance = empty($modelJob->OtherExpenseAllowance) ? 0 : str_replace(',', '', $modelJob->OtherExpenseAllowance);
        $modelJob->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);
        $modelJob->JobArea = empty($modelJob->JobArea) ? "" : implode(',', $modelJob->JobArea);
        $modelJob->Total =  $modelJob->Fee;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
          if ($modelJob->save()) {
            $modelAS->save();
            $transaction->commit();
            if ($modelJob->isNewRecord) {
              return $this->redirect(['request', 'id' => $modelJob->JobID]);
            } else {
              try {
                if ($modelAS->Flag == 2) {
                  $send_email = $this->Email($modelJob->partner->email, 'THERE IS A REVISION JOB NEEDS YOUR APPROVAL', $modelJob, $status);
                }
                return $this->redirect(['viewrevisi', 'id' => $modelJob->JobID]);
              } catch (\Exception $e) {
                return $this->redirect(['viewrevisi', 'id' => $modelJob->JobID]);
              }
            }
          }
        } catch (Exception $e) {
          $transaction->rollBack();
        }
      }
    }

    $sql = "call job_actual('" . $modelJob->JobCode . "');";
    $actual = Yii::$app->db->createCommand($sql)->queryone();

    return $this->render('jobFormTemp', [
      'data' => $data,
      'modelJob' => $modelJob,
      'modelBudgeting' => $modelBudgeting,
      'actual' => $actual,
    ]);
  }

  public function actionBudgettemp($id = null, $action = null)
  {

    $post = Yii::$app->request->post();
    $model = new JobTemp();
    $model->load($post);
    $modelJob = JobTemp::find()->where(['JobID' => $model->JobID])->one();
    // var_dump($model);
    if ($modelJob->load($post)) {
      if (Yii::$app->request->isAjax) {
        if (!empty($id)) {
          $modelBudget = $this->findModelBudgetTemp($id, $modelJob->JobID);
        } else {
          $modelBudget = new JobBudgetingTemp();
          $modelBudget->JobID = $modelJob->JobID;
          if (empty($action)) {
            // $modelBudget->EmployeeID = null;
            $modelBudget->isNewRecord;
          }
        }
        $data = $this->getDataForForm($modelJob);
        return $this->renderAjax('jobFormDetailTemp', [
          'modelJob' => $modelJob,
          'modelBudget' => $modelBudget,
          'id' => empty($action) ? $id : "",
          'data' => $data
        ]);
      } else {
        throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }
  }

  public function actionBudgetsavetemp($id = null)
  {
    $user = CommonHelper::getUserIndentity();

    $post = Yii::$app->request->post();
    $modelBudget = new JobBudgetingTemp();
    if ($modelBudget->load($post)) {


      if (!empty($id)) {
        $modelBudget = $this->findModelBudgetTemp($id, $modelBudget->JobID);
        $modelBudget->load($post);

        // print_r($modelBudget->OverTime);
      }
      $valid = $modelBudget->validate();
      if ($valid) {
        $modelBudget->CreatedBy = $user->Id;
        if ($modelBudget->OverTime == null) {
          $modelBudget->OverTime = 0;
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
          if (!($flag = $modelBudget->save(false))) {
            $transaction->rollBack();
            $return = 'error : rollback';
          }
          if ($flag) {
            $transaction->commit();
            $return = true;
          }
        } catch (Exception $e) {
          $transaction->rollBack();
        }
      } else {
        $return = 'error : validation not valid' . Html::errorSummary($modelBudget, ['encode' => true]);
      }
    } else {
      $return = 'error : model not loaded';
    }

    return $return;
  }

  public function actionReqsavetemp()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobTemp();
    if ($modelJob->load($post)) {

      $model = $this->findModelTemp($modelJob->JobID);
      if ($model->load($post)) {
        $model->Fee = empty($modelJob->Fee) ? 0 : str_replace(',', '', $modelJob->Fee);
        $model->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
        $model->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
        $model->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
        $model->JobArea = empty($modelJob->JobArea) ? [] : implode(',', $modelJob->JobArea);
        $model->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);
        $model->OtherExpenseAllowance = empty($modelJob->OtherExpenseAllowance) ? 0 : str_replace(',', '', $modelJob->OtherExpenseAllowance);
        if ($model->save(false)) {
          return "save";
        }
      }
    }
  }

  public function actionOutofficeallowancetemp()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobTemp();
    $modelJob->load($post);

    $model = JobTemp::find()->where(['JobID' => $modelJob->JobID])->one();
    $model->MealAllowance = empty($modelJob->MealAllowance) ? 0 : str_replace(',', '', $modelJob->MealAllowance);
    $model->TaxiAllowance = empty($modelJob->TaxiAllowance) ? 0 : str_replace(',', '', $modelJob->TaxiAllowance);
    $model->OutOfOfficeAllowance = empty($modelJob->OutOfOfficeAllowance) ? 0 : str_replace(',', '', $modelJob->OutOfOfficeAllowance);
    $model->AdministrativeCharge = empty($modelJob->AdministrativeCharge) ? 0 : str_replace(',', '', $modelJob->AdministrativeCharge);
    $model->OtherExpenseAllowance = empty($modelJob->OtherExpenseAllowance) ? 0 : str_replace(',', '', $modelJob->OtherExpenseAllowance);

    // print_r($model->OutOfOfficeAllowance);

    if ($model->save(false)) {

      $modelAllow = $this->findModelTemp($model->JobID);
      // print_r($model);
      // $sql = "call JobEstimated('".$modelJob->JobID."')";
      // $task = Yii::$app->db->createCommand($sql)->queryOne();
      //
      //
      // $data['Ope'] = $model->MealAllowance;
      // $data['Taxi'] = $model->TaxiAllowance;
      // $data['Ot'] = $model->OutOfOfficeAllowance;
      // $data['rt'] = $model->Percentage;
      // $data['Recovery'] = "20%";
      return $modelAllow;
      // code...
    }
  }

  public function actionReqsaveallowancetemp()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobTemp();
    if ($modelJob->load($post)) {
      $model = $this->findModelTemp($modelJob->JobID);
      $model->IsMeal = $modelJob->IsMeal;
      $model->IsTaxi = $modelJob->IsTaxi;
      $model->IsOutOfOffice = $modelJob->IsOutOfOffice;
      if ($modelJob->IsMeal != 1) {
        $model->MealAllowance = 0;
      }
      if ($modelJob->IsTaxi != 1) {
        $model->TaxiAllowance = 0;
      }
      if ($modelJob->IsOutOfOffice != 1) {
        $model->OutOfOfficeAllowance = 0;
      }

      if ($model->save(false)) {
        return $model;
      }
    }
  }

  public function actionGetlevelidtemp()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobBudgetingTemp();
    $modelJob->load($post);

    $data['fullName'] = $modelJob->employee->fullName;
    $data['levelName'] = $modelJob->employee->level->levelName;
    $data['eA'] = $this->GetEmployeeAvailable($modelJob->EmployeeID);

    return $data;
  }

  public function actionTotalwhtemp()
  {
    $post = Yii::$app->request->post();
    $modelJob = new JobBudgetingTemp();
    $modelJob->load($post);

    $sql = "call getValueTaskAE('" . $modelJob->EmployeeID . "','" . $modelJob->Total . "')";
    $task = Yii::$app->db->createCommand($sql)->queryOne();

    return json_encode($task);
  }

  public function actionRejectrevisi($id = "")
  {
    $model = $this->findModel($id);
    $model->Flag = 1;
    if ($model->save()) {
      return $this->redirect(['view', 'id' => $model->JobID]);
    }
  }

  public function actionViewrevisi($id = "")
  {
    $post = Yii::$app->request->post();
    $user = CommonHelper::getUserIndentity();

    $modelJob = $this->findModelTemp($id);
    if ($modelJob->Flag == 1) {
      return $this->redirect(['view', 'id' => $id]);
    }
    $model = $this->findModel($id);
    $modelComments = new JobComment();
    $modelComments->JobID = $id;

    // $param['isCurrentApprover'] = ($modelJob->Partner == $user->EmployeeID) ? true : false;
    // $param['isRequester'] = ($modelJob->Manager == $user->EmployeeID) ? true : false;
    //$param['budget'] = Job::countTotalBudget($modelJob);

    $searchModelBudget = new JobBudgetingTempSearch([
      'JobID' => $id,
    ]);

    // var_dump($modelJob);
    // die();
    $dataProviderBudget = $searchModelBudget->search(Yii::$app->request->queryParams);
    $count = JobComment::find()
      ->select(['COUNT(*) AS jml'])
      ->where(['JobID' => $id])
      ->andWhere(['like', 'ListEmployee', $user->Id])
      ->count();
    $commentCount['count'] = $count;
    $pcr = 0;
    if (!empty($id)) {
      $sql = "SELECT * FROM AuditTrail where TransID = '$id' order by ID desc LIMIT 1";
      $auditID = Yii::$app->db->createCommand($sql)->queryOne();
      if ($auditID != false) {
        $sqlp = "call AuditJsonAll('" . $auditID['ID'] . "')";
        $pAudit = Yii::$app->db->createCommand($sqlp)->queryOne();

        if ($pAudit['Status'] != 0) {
          $modelRevisi['vDescAll'] =  $pAudit;
          // var_dump($modelRevisi['vDescAll']);
          // die();
          $pc = "SELECT COUNT(*) FROM trPreviousChangesJob where JobID = '$id' group by JobID";
          $pcr = Yii::$app->db->createCommand($pc)->queryOne();
          // var_dump($pcr);
          // die();
        } else {
          if ($modelJob->Flag == 1) {
            $modelRevisi['vDescAll'] = false;
          } else {
            $model->Flag = 0;
            if ($model->save()) {
              if ($modelJob->delete()) {
                \Yii::$app
                  ->db
                  ->createCommand()
                  ->delete('trJobBudgetingTemp', ['JobID' => $id])
                  ->execute();
                return $this->redirect(['view', 'id' => $id, 'st' => true]);
              }
            }
          }
        }
      } else {
        $modelRevisi['vDescAll'] =  false;
      }
    } else {
      $modelRevisi['vDescAll'] =  "NOT DESCRIPTION";
    }

    return $this->render('viewrevisi', [
      'modelJob' => $modelJob,
      'modelComments' => $modelComments,
      'searchModelBudget' => $searchModelBudget,
      'dataProviderBudget' => $dataProviderBudget,
      'modelRevisi' => $modelRevisi,
      'commentCount' =>   $commentCount,
      'countHistory' =>   $pcr,
      // 'param' => $param,
    ]);
  }

  public function actionHistoryrevisi($id)
  {
    $modelJob = $this->findModelTemp($id);
    $model = $this->findModelHistoryJob($id);
    $data = [];
    foreach ($model as $key => $value) {
      array_push($data, $value['PreviousChanges']);
    }



    return $this->render('historyrevisi', [
      'model' => $data,
      'modelJob' => $modelJob,
    ]);
  }

  public function actionApproverevisi($id = "")
  {
    $model = $this->findModelTemp($id);
    $modelJob = $this->findModel($model->JobID);
    $modelJob->Description = $model->Description;
    $modelJob->JobArea = $model->JobArea;
    $modelJob->StartDate = $model->StartDate;
    $modelJob->EndDate = $model->EndDate;
    $modelJob->Partner = $model->Partner;
    $modelJob->Manager = $model->Manager;
    $modelJob->Supervisor = $model->Supervisor;
    $modelJob->IncludeOPE = $model->IncludeOPE;
    $modelJob->IsMeal = $model->IsMeal;
    $modelJob->IsOutOfOffice = $model->IsOutOfOffice;
    $modelJob->IsTaxi = $model->IsTaxi;
    $modelJob->MealAllowance = $model->MealAllowance;
    $modelJob->OutOfOfficeAllowance = $model->OutOfOfficeAllowance;
    $modelJob->AdministrativeCharge = $model->AdministrativeCharge;
    $modelJob->OtherExpenseAllowance = $model->OtherExpenseAllowance;
    $modelJob->TaxiAllowance = $model->TaxiAllowance;
    $modelJob->Fee = $model->Fee;
    $modelJob->Flag = 0;
    if ($modelJob->save(false)) {
      $modelJobBuddel = JobBudgeting::find()->where(['JobID' => $id])->one();
      $modelJobBuddel->delete();
      $modelJobBudget = JobBudgetingTemp::find()->where(['JobID' => $id])->all();
      $mdeltemp = JobBudgetingTemp::find()->where(['JobID' => $id])->one();

      foreach ($modelJobBudget as $mb) {
        $modelJobBg = new JobBudgeting;
        $modelJobBg->JobID = $mb->JobID;
        $modelJobBg->EmployeeID = $mb->EmployeeID;
        $modelJobBg->Planning = $mb->Planning;
        $modelJobBg->FieldWork = $mb->FieldWork;
        $modelJobBg->Reporting = $mb->Reporting;
        $modelJobBg->WrapUp = $mb->WrapUp;
        $modelJobBg->OverTime = $mb->OverTime;
        $modelJobBg->Total = $mb->Total;
        $modelJobBg->CreatedBy = $mb->CreatedBy;
        $modelJobBg->CreatedAt = $mb->CreatedAt;
        $modelJobBg->UpdateBy = $mb->UpdateBy;
        $modelJobBg->UpdateAt = $mb->UpdateAt;
        if ($modelJobBg->save()) {
          \Yii::$app
            ->db
            ->createCommand()
            ->delete('trJobBudgetingTemp', ['JobID' => $id])
            ->execute();
          $model->delete();
          // if ($model->delete()) {
          //
          // }
        }
      }

      return $this->redirect(['view', 'id' => $id]);

      // var_dump($modelJobBudget);
      // $model->delete();
    }

    // $modelJob->Description = $model->Description;

    // var_dump($modelJob);

  }

  public function actionDefaulttemp()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobTemp();
    if ($modelJob->load($post)) {
      // code...
      $sql = "call JobEstimated('" . $modelJob->JobID . "', '1' , '1')";
      $task = Yii::$app->db->createCommand($sql)->queryOne();

      if ($task) {

        $model = $this->findModelTemp($modelJob->JobID);
        return $model;
      }
      // return $task;
      // if ($modelJob->JobID) {
      //   // code...
      // }

    }
  }

  public function actionBudgetdeletetemp()
  {
    $return = false;
    $post = Yii::$app->request->post();

    $EmployeeID = $post['EmployeeID'];
    $JobID = $post['JobID'];
    $modelBudget = $this->findModelBudgetTemp($EmployeeID, $JobID);

    $transaction = \Yii::$app->db->beginTransaction();
    try {
      if ($modelBudget->delete()) {
        $transaction->commit();
        $return = true;
      } else {
        $transaction->rollback();
      }
    } catch (Exception $e) {
      $transaction->rollBack();
    }

    return $return;
  }

  protected function Email($sendto, $subject, $modelJob, $status, $layouts = 'layouts/newjobsapproval')
  {
    Yii::$app->cache->flush();

    $from = Yii::$app->params['appNotificationEmail'];
    $cc = Yii::$app->params['appNotificationEmailCC'];

    $to = array();
    $to[$sendto] = $sendto;

    if (!empty($to)) {
      $to = $to;
      $html = ['html' => $layouts];
      $html_bind = ['title' => $subject, 'modelJob' => $modelJob, 'status' => $status];

      $compose = Yii::$app->mailer->compose($html, $html_bind);
      $compose->setFrom($from);
      $compose->setTo($to);
      if (!empty($cc)) {
        $compose->setCc($cc);
      }
      $compose->setSubject($subject);

      if ($compose->send()) {
        return "true";
      } else {
        return "false";
      }
    }
  }
}
