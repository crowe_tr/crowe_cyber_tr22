<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\ErrorException;
use common\models\cm\CmReport;
use common\models\tr\views\ReportTimeReport;
use common\models\tr\views\ReportTimeReportSearch;
use common\components\CommonHelper;
use common\components\TimeReportHelper;
use kartik\mpdf\Pdf;
use common\models\st\TaskType;
use common\models\st\Task;

class ReportController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    //=============== START REPORT GENERATOR ===============
    public function generate_results($view, $model, $sql, $fields)
    {
        // var_dump($sql);
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        if ($model->ReportFormat == "xls") {
            $results = $this->renderPartial($view, [
                'data' => $data,
                'fields' => $fields,
                'format' => $model->ReportFormat,
                'model' => $model,
            ]);
        } else {
            $results = $this->renderPartial($view, [
                'data' => $data,
                'fields' => $fields,
                'format' => $model->ReportFormat,
                'model' => $model,
            ]);
        }

        return $results;
    }
    public function generate_report($data, $filename, $format, $param = '')
    {
        if ($format == 'xls') {
            $filename = $filename . '.xls';
            $fopen = fopen($filename, 'w');
            file_put_contents($filename, $data);
            fclose($fopen);

            Yii::$app->response->SendFile($filename);
            unlink($filename);
        } else if ($format == 'pdf') {
            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_LANDSCAPE,
                'destination' => Pdf::DEST_DOWNLOAD,
                'marginTop' => 8,
                'marginBottom' => 8,
                'marginRight' => 8,
                'marginLeft' => 8,
                'filename' => 'REPORT # ' . rand() . '.pdf',

                'cssInline' => '
                *, p, span, b {
                    font-family: arial;
                    font-size: 13px !important;
                    padding:0;
                    margin:0;
                    text-size-adjust: none;
                    text-transform: uppercase;
                }
                .table {
                    border-collapse: collapse;
                    display: block;
                    text-transform: uppercase;
                    text-size-adjust: none;            
                }
                .table th{
                    padding: 5px;
                    vertical-align: top;
                    font-size: 12px !important;
                    background: #007be8 !important;
                }
                .table td {
                    padding: 5px;
                    vertical-align: top;
                    font-size: 12px !important;
                }
                .table .bg-primary {
                    background: #007be8 !important;
                }
                ',
                'content' => $data,
            ]);
            return $pdf->render();
        } else {
            return $data;
        }
        return $data;
    }
    public function get_fields_options($fields)
    {
        $options = [];
        $fields = array_keys($fields);
        foreach ($fields as $f) {
            $options[$f] = ['selected' => 'selected'];
        }
        return $options;
    }
    public function report($_view, $_sql, $_report)
    {
        $this->layout = "../../../../views/layouts/report";

        $posts = Yii::$app->request->post();
        $model = new CmReport();

        $data = "";
        $fields = [];
        $fields['data'] = $_sql['field'];
        $fields['selected'] = $fields['data'];
        $fields['options'] = $this->get_fields_options($fields['selected']);

        if ($model->load($posts)) {
            if (!empty($model->Date)) {
                $param = $this->parameter($model);
                $fields['selected'] = empty($param['fields']) ? $fields['selected'] : $param['fields'];
                $fields['options'] = $this->get_fields_options($fields['selected']);

                $sql = $this->sql($_sql['name'], $param);
                $results = $this->generate_results($_view['results'], $model, $sql, $fields);
                $data = $this->generate_report($results, $_report['name'] . date('ymdhis'), $model->ReportFormat);
            }
        }

        $init = $this->get_init($model);
        return $this->render($_view['form'], [
            'model' => $model,
            'init' => $init,
            'data' => $data,
            'fields' => $fields,
            'options' => $_report,
        ]);
    }
    public function get_init($model)
    {
        $param = [];
        $param['TaskTypeID'] = ArrayHelper::map(TaskType::find()->where(['flag' => 1])->asArray()->all(), 'id', 'taskTypeName');;
        $param['TaskID'] = empty($model->Custom['TaskTypeID']) ? [] : ArrayHelper::map(Task::find()->where(['taskTypeID' => $model->Custom['TaskTypeID']])->asArray()->all(), 'id', 'taskName');;
        return $param;
    }
    public function parameter($model)
    {
        $param = [];
        $Custom = $model->Custom;
        $param['use_date'] = empty($Custom['use_date']) ? 0 : $Custom['use_date'];

        $param['date'] = explode(" TO ", $model->Date);
        $param['date1'] = date('Y-m-d', strtotime($param['date'][0]));
        $param['date2'] = date('Y-m-d', strtotime($param['date'][1]));
        $param['entity'] = empty($Custom['Entity']) ? "null" : "'" . $Custom['Entity'] . "'";
        $param['division'] = empty($Custom['Division']) ? "null" : "'" . $Custom['Division'] . "'";
        $param['dept'] = empty($Custom['deptID']) ? "null" : "'" . $Custom['deptID'] . "'";
        $param['group'] = empty($Custom['parentID']) ? "null" : "'" . $Custom['parentID'] . "'";
        $param['manager'] = empty($Custom['ManagerID']) ? "null" : "'" . $Custom['ManagerID'] . "'";
        $param['supervisor'] = empty($Custom['SupervisorID']) ? "null" : "'" . $Custom['SupervisorID'] . "'";
        $param['level'] = empty($Custom['levelID']) ? "null" : "'" . $Custom['levelID'] . "'";
        $param['employee'] = empty($Custom['Employee']) ? "null" : "'" . $Custom['Employee'] . "'";

        $param['TypeName'] = empty($Custom['TypeName']) ? "null" : "'" . $Custom['TypeName'] . "'";
        $param['TaskName'] = empty($Custom['TaskName']) ? "null" : "'" . $Custom['TaskName'] . "'";
        $param['ClientCode'] = empty($Custom['ClientCode']) ? "null" : "'" . $Custom['ClientCode'] . "'";
        $param['JobCode'] = empty($Custom['JobCode']) ? "null" : "'" . $Custom['JobCode'] . "'";
        $param['status'] = empty($Custom['status']) ? "null" : "'" . $Custom['status'] . "'";
        $param['partner'] = empty($Custom['PartnerID']) ? "null" : "'" . $Custom['PartnerID'] . "'";
        $param['TaxiType'] = empty($Custom['TaxiType']) ? "null" : "'" . $Custom['TaxiType'] . "'";

        $param['year'] = empty($Custom['year']) ? "null" : "'" . $Custom['year'] . "'";

        $param['fields'] = empty($Custom['fields']) ? [] : $Custom['fields'];
        return $param;
    }
    public function sql($_sql, $param)
    {
        switch ($_sql) {
            case "rpt_time_report_daily":
                $sql = "call rpt_time_report_daily(
                    '" . $param['date1'] . "', 
                    '" . $param['date2'] . "', 
                    " . $param['entity'] . ", 
                    " . $param['division'] . ", 
                    " . $param['dept'] . ", 
                    " . $param['group'] . ", 
                    " . $param['manager'] . ", 
                    " . $param['level'] . ", 
                    " . $param['employee'] . "
                );";
                break;
            case "rpt_time_report_detail":
                $sql = "
                    call rpt_time_report_detail(
                        '" . $param['date1'] . "', 
                        '" . $param['date2'] . "', 
                        " . $param['entity'] . ", 
                        " . $param['division'] . ", 
                        " . $param['TypeName'] . ", 
                        " . $param['TaskName'] . ", 
                        " . $param['ClientCode'] . ", 
                        " . $param['JobCode'] . ", 
                        " . $param['manager'] . ", 
                        " . $param['group'] . ",
                        " . $param['employee'] . ",  
                        " . $param['status'] . "
                    );
                    ";
                break;
            case "rpt_recovery_rate_summary":
                $sql = "
                        call rpt_recovery_rate_summary_v3(
                            " . $param['use_date'] . ", 
                            '" . $param['date1'] . "', 
                            '" . $param['date2'] . "',         
                            " . $param['entity'] . ", 
                            " . $param['division'] . ", 
                            " . $param['partner'] . ", 
                            " . $param['manager'] . ", 
                            " . $param['ClientCode'] . ", 
                            " . $param['JobCode'] . ", 
                            " . $param['status'] . "
                        );
                    ";
                break;
            case "rpt_taxi_detail":
                $sql = "
                    call rpt_taxi_detail(
                        '" . $param['date1'] . "', 
                        '" . $param['date2'] . "', 
                        " . $param['entity'] . ", 
                        " . $param['division'] . ", 
                        " . $param['dept'] . ", 
                        " . $param['TaxiType'] . ", 
                        " . $param['group'] . ", 
                        " . $param['manager'] . ", 
                        " . $param['TypeName'] . ", 
                        " . $param['TaskName'] . ", 
                        " . $param['ClientCode'] . ", 
                        " . $param['JobCode'] . ", 
                        " . $param['level'] . ", 
                        " . $param['employee'] . ",
                        " . $param['status'] . "
                    );
                ";
                break;
            case "rpt_annual_report_summary":
                $sql = "call rpt_annual_report_summary2(
                    " . $param['year'] . ", 
                    " . $param['entity'] . ", 
                    " . $param['division'] . ", 
                    " . $param['dept'] . ", 
                    " . $param['group'] . ", 
                    " . $param['manager'] . ", 
                    " . $param['level'] . ", 
                    " . $param['employee'] . "
                );";
                break;
            case "rpt_annual_report_detail":
                $sql = "call rpt_annual_report_detail(
                    " . $param['employee'] . ", 
                    " . $param['year'] . "
                );";
                break;
            case "rpt_sick_list":
                $sql = "call rpt_sick_list(
                    '" . $param['date1'] . "', 
                    '" . $param['date2'] . "', 
                    " . $param['entity'] . ", 
                    " . $param['division'] . ", 
                    " . $param['group'] . ", 
                    " . $param['manager'] . ", 
                    " . $param['supervisor'] . ", 
                    " . $param['employee'] . "
                );";
                break;
            case "rpt_recovery_rate_detail":
                $sql = "call rpt_recovery_rate_detail(
                    " . $param['JobCode'] . "
                );";
                break;
            default:
                $sql = "";
        }
        return $sql;
    }
    //=============== END REPORT GENERATOR ===============

    public function actionIndex()
    {

        return $this->render('index', []);
    }

    public function actionDaily()
    {
        $_view['form'] = 'daily/form';
        $_view['results'] = 'daily/report';

        $_sql['name'] = "rpt_time_report_daily";
        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', // ada
            'employee_name' => 'EMPLOYEE NAME', // ada
            'employee_level' => 'EMPLOYEE LEVEL', // ada
            'entity_name' => 'ENTITY', // ada
            'division_name' => 'DIVISION', // ada
            'dept_name' => 'DEPARTMENT', // ada
            'group_id' => 'GROUP ID', // ada
            'group_name' => 'GROUP NAME', // ada
            'group_initial' => 'GROUP INITIAL', // ada
            'manager_id' => 'MANAGER ID', // ada
            'manager_name' => 'MANAGER NAME', // ada
            'manager_initial' => 'MANAGER INITIAL', // ada
            'calendar_day' => 'DAY', // ada
            'calendar_date' => 'DATE', // ada
            'description' => 'CALENDAR DESCRIPTION', // ada
            'wh_amount' => 'WH', // ada
            'ot_amount' => 'OT', // ada
            'meals_amount' => 'MEALS', // ada
            'outofoffice_amount' => 'OPE', // ada
            'taxi_amount' => 'TAXI', // ada
            // 'status' => 'Status', 
        ];

        $_report['name'] = 'Time Report - Daily';
        return $this->report($_view, $_sql, $_report);
    }
    public function actionDetail()
    {
        $_view['form'] = 'detail/form';
        $_view['results'] = 'detail/report';

        $_sql['name'] = "rpt_time_report_detail";

        $_sql['field'] = [
            'EmployeeID' => 'EMPLOYEE ID', // ada
            'EmployeeName' => 'NAME', // ada
            'Position' => 'POSITION', // ada
            'Entity' => 'ENTITY', // ada
            'Division' => 'DIVISION', // ada
            'group_id' => 'GROUP ID',  // ada
            'group_name' => 'GROUP NAME',  // ada
            'group_initial' => 'GROUP INITIAL',  // ada
            'approval2_id' => 'MANAGER ID',  // ada
            'approval2_name' => 'MANAGER NAME',  // ada
            'approval2_initial' => 'MANAGER INITIAL',  // ada            
            'approval1_id' => 'SUPERVISOR ID',  // ada
            'approval1_name' => 'SUPERVISOR NAME',  // ada
            'approval1_initial' => 'SUPERVISOR INITIAL',  // ada
            'calendar_day' => 'DAY', // ada
            'Date' => 'DATE', // ada
            'TypeName' => 'TYPE NAME', // ada
            'TaskName' => 'TASK NAME', // ada
            'JobCode' => 'JOB CODE', // ada
            'ClientCode' => 'CLIENT CODE', // ada
            'ClientName' => 'CLIENT NAME', // ada
            'JobDescription' => 'JOB DESCRIPTION', // ada
            'Zone' => 'ZONE', // ada
            'WHAmount' => 'MAN HOURS', // ada
            'WorkHour' => 'WH', // ada
            'Overtime' => 'OT', // ada
            'MealsAmount' => 'MEALS', // ada
            'OutOfOfficeAmount' => 'OPE', // ada
            'meals_ope' => 'TOTAL',
            'TypeTaxi' => 'TAXI TYPE', // ada
            'TaxiAmount' => 'TAXI AMOUNT', // ada
            'TimeReportDescription' => 'TR DESCRIPTION', // ada
            'StatusDescription' => 'Status', // ada
        ];

        $_report['name'] = 'Time Report - Detail';
        return $this->report($_view, $_sql, $_report);
    }
    public function actionRecovery_rate_summary()
    {
        $_view['form'] = 'recovery_rate_summary/form';
        $_view['results'] = 'recovery_rate_summary/report';

        $_sql['name'] = "rpt_recovery_rate_summary";

        $_sql['field'] = [
            'entity_name' => 'ENTITY', // ada
            'div_name' => 'DIVISION', // ada
            'cust_code' => 'CUSTOMER', // ada
            'client_name' => 'CLIENT', // ada
            'industry_name' => 'INDUSTRY', // ada
            'job_code' => 'JOB CODE', // ada
            'job_description' => 'JOB DESCRIPTION', // ada
            'job_start_date' => 'START DATE', // ada
            'job_finish_date' => 'FINISH DATE', // ada
            'job_status' => 'STATUS', // ada
            'manager_id' => 'MANAGER ID', // ada
            'manager_name' => 'MANAGER NAME', // ada
            'manager_initial' => 'MANAGER INITIAL', // ada
            'partner_id' => 'PARTNER ID', // ada
            'partner_name' => 'PARTNER NAME', // ada
            'partner_initial' => 'PARTNER INITIAL', // ada
            'job_fee' => 'JOB FEE', // ada
            'tc_partner' => 'PARTNER', // ada
            'tc_associate_director' => 'ASSOCIATE DIRECTOR', // ada
            'tc_director' => 'DIRECTOR', // ada
            'tc_senior_manager' => 'SENIOR MANAGER', // ada
            'tc_manager' => 'MANAGER', // ada
            'tc_associate_manager' => 'ASSOCIATE MANAGER', // ada
            'tc_managing_partner' => 'MANAGING PARTNER', // ada
            'tc_secretary' => 'SECRETARY', // ada
            'tc_deputy_managing_partner' => 'DEPUTY MANAGING PARTNER', // ada
            'tc_chairman' => 'CHAIRMAN', // ada
            'tc_principal' => 'PRINCIPAL', // ada
            'tc_manager_3' => 'MANAGER 3', // ada
            'tc_supervisor' => 'SUPERVISOR', // ada
            'tc_supervisor_1' => 'SUPERVISOR 1', // ada
            'tc_supervisor_2' => 'SUPERVISOR 2', // ada
            'tc_supervisor_3' => 'SUPERVISOR 3', // ada
            'tc_senior' => 'SENIOR', // ada
            'tc_senior_1' => 'SENIOR 1', // ada
            'tc_senior_2' => 'SENIOR 2', // ada
            'tc_senior_3' => 'SENIOR 3', // ada
            'tc_junior' => 'JUNIOR', // ada
            'tc_internship' => 'INTERNSHIP', // ada
            'tc_admin_staff' => 'ADMIN STAFF', // ada
            'tc_administrasi' => 'ADMINISTRASI', // ada
            'tc_total' => 'TOTAL TIME CHARGE', // ada
            'tc_other' => 'OTHER EXPENSE', // ada
            'total_charges' => 'TOTAL CHARGES', // ada
            'rate_time_charges' => 'RECOVERY TIME CHARGES', // ada
            'rate_total_charges' => 'RECOVERY TOTAL CHARGES', // ada
            'progress_time' => 'PROGRESS TIME',  // ada

        ];
        $_report['name'] = 'Recovery Rate - Summary';
        return $this->report($_view, $_sql, $_report);
    }
    public function actionTaxi_detail()
    {
        $_view['form'] = 'taxi_detail/form';
        $_view['results'] = 'taxi_detail/report';

        $_sql['name'] = "rpt_taxi_detail";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', // ada
            'employee_name' => 'NAME', // ada
            'timereport_date' => 'DATE', // ada
            'timereport_day' => 'DAY', // ada
            'level_name' => 'LEVEL', // ada
            'entity_name' => 'ENTITY', // ada
            'division_name' => 'DIVISION', // ada
            'dept_name' => 'DEPARTEMENT', // ada
            'group_id' => 'GROUP ID', // ada 
            'group_name' => 'GROUP NAME', // ada 
            'group_initial' => 'GROUP INITIAL', // ada
            'approval2_id' => 'MANAGER ID', // ada
            'approval2_name' => 'MANAGER NAME', // ada
            'approval2_initial' => 'MANAGER INITIAL', // ada
            'approval1_id' => 'SUPERVISOR ID', // ada
            'approval1_name' => 'SUPERVISOR NAME', // ada
            'approval1_initial' => 'SUPERVISOR INITIAL', // ada
            'job_description' => 'JOB DESCRIPTION', // ada
            'job_memo' => 'MEMO', // ada
            'taxi_type' => 'TAXI', // ada
            'taxi_voucher_no' => 'VOUCHER NO', // ada
            'taxi_start' => 'START', // ada
            'taxi_finish' => 'FINISH', // ada
            'taxi_destination' => 'DESTINATION', // ada
            'taxi_amount' => 'AMOUNT', // ada
            'taxi_description' => 'DESCRIPTION', // ada
            'approval_status' => 'STATUS', // ada
        ];
        $_report['name'] = 'Time Report - Taxi';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionAnnual_leave_summary()
    {
        $_view['form'] = 'annual_leave_summary/form';
        $_view['results'] = 'annual_leave_summary/report';

        $_sql['name'] = "rpt_annual_report_summary";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', // ada
            'employee_name' => 'NAME', // ada
            'employee_level' => 'LEVEL', // ada
            'annual_days_ob' => 'OPEN', // ada
            'annual_days_used' => 'USED', // ada
            'annual_days_balance' => 'BALANCE', // ada
            'entity_name' => 'ENTITY', // ada
            'division_name' => 'DIVISION', // ada
            'dept_name' => 'DEPARTEMENT', // ada
            'group_id' => 'GROUP ID', // ada 
            'group_name' => 'GROUP NAME', // ada 
            'group_initial' => 'GROUP INITIAL', // ada
            'manager_id' => 'MANAGER ID', // ada
            'manager_name' => 'MANAGER NAME', // ada
            'manager_initial' => 'MANAGER INITIAL', // ada
            'supervisor_id' => 'SUPERVISOR ID', // ada
            'supervisor_name' => 'SUPERVISOR NAME', // ada
            'supervisor_initial' => 'SUPERVISOR INITIAL', // ada
        ];
        $_report['name'] = 'Annual Leave - Summary';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionAnnual_leave_detail()
    {
        $_view['form'] = 'annual_leave_detail/form';
        $_view['results'] = 'annual_leave_detail/report';

        $_sql['name'] = "rpt_annual_report_detail";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', // ada
            'annual_year' => 'YEAR', // ada
            'trans_date' => 'DATE', // ada
            'description' => 'DESCRIPTION', // ada
            'annual_used' => 'USED', // ada
            'res_balance' => 'BALANCE', // ada
            'annual_status' => 'STATUS', // ada
        ];
        $_report['name'] = 'Annual Leave - Detail';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionSick_list()
    {
        $_view['form'] = 'sick_list/form'; //view form
        $_view['results'] = 'sick_list/report'; //view results

        $_sql['name'] = "rpt_sick_list"; //nama function sql di controller ini

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', // ada
            'employee_name' => 'EMPLOYEE NAME', // ada
            'entity_name' => 'ENTITY', // ada
            'division_name' => 'DIVISION', // ada
            'group_id' => 'GROUP ID', // ada
            'group_name' => 'GROUP', // ada
            'group_initial' => 'GROUP INITIAL', // ada
            'manager_id' => 'MANAGER ID', // ada
            'manager_name' => 'MANAGER NAME', // ada
            'manager_initial' => 'MANAGER INITIAL', // ada
            'supervisor_id' => 'SUPERVISOR ID', // ada
            'supervisor_name' => 'SUPERVISOR', // ada
            'supervisor_initial' => 'SUPERVISOR INITIAL', // ada
            'supervisor_name' => 'SUPERVISOR NAME', // ada
            'timereport_day' => 'DAY', // ada
            'timereport_date' => 'DATE', // ada
            'attachment' => 'attachment', // ada
        ];
        $_report['name'] = 'Annual Leave - Sick List';
        return $this->report($_view, $_sql, $_report);
    }


    public function actionRecovery_rate_detail()
    {
        $_view['form'] = 'recovery_rate_detail/form';
        $_view['results'] = 'recovery_rate_detail/report';

        $_sql['name'] = "rpt_recovery_rate_detail";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', // ada
            'employee_name' => 'EMPLOYEE NAME', // ada
            'employee_level' => 'LEVEL', // ada
            'job_planning' => 'PLANNING', // ada
            'job_fieldwork' => 'FIELD WORK', // ada
            'job_reporting' => 'REPORTING', // ada
            'job_wrapup' => 'WRAP UP', // ada
            'job_total' => 'TOTAL', // ada
            'job_overtime' => 'OVERTIME', // ada
            'job_planning_act' => 'PLANNING', // ada
            'job_fieldwork_act' => 'FIELD WORK', // ada
            'job_reporting_act' => 'REPORTING', // ada
            'job_wrapup_act' => 'WRAP UP', // ada
            'job_total_act' => 'TOTAL', // ada
            'job_overtime_act' => 'OVERTIME', // ada
            'job_total_diff' => 'DIFF WH',
            'job_overtime_diff' => 'DIFF OT',
        ];
        $_report['name'] = 'Recovery Rate - Detail';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionEmail_monthly_supervisor()
    {
        echo TimeReportHelper::emailMonthlySupervisor();
    }
    public function actionEmail_monthly_manager()
    {
        echo TimeReportHelper::emailMonthlyManager();
    }
}
