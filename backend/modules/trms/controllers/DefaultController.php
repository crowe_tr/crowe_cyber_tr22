<?php

namespace backend\modules\trms\controllers;

use yii\web\Controller;

/**
 * Default controller for the `trms` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
