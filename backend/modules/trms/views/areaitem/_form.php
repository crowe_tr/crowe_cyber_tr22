<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\trms\Area;


/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-item-form">

    <?php
    $form = ActiveForm::begin([
            'id' => 'job-form',
            'enableClientValidation' => true,
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => true,
            'fieldConfig' => [
                'template' => '{label}{input}',
                'options' => [
                    'class' => 'form-group form-group-default',
                ],
            ],
        ]);
    ?>

    <div class="card card-default m-b-10">
      <div class="card-boy">
        <div class="form-group-attached">
          <div class="row">
            <div class="col-md-6">
              <?php
  							  echo $form->field($model, 'areaItemName')->textInput(['maxlength' => true])
  						?>
            </div>
            <div class="col-md-6">
                <?php

                echo $form->field($model, 'areaItemDesc')->textInput(['maxlength' => true])
                ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <?= $form->field($model, 'areatermname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
              <?= $form->field($model, 'conditionType')->textInput(['maxlength' => true]) ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-10">
              <?php
                  echo $form->field($model, 'areaID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                                  'data' => ArrayHelper::map(Area::find()->all(), 'id', 'areaName'),
                                  'options' => ['id' => 'areaID', 'placeholder' => 'Select ...'],
                          ]
                  );
              ?>
            </div>
            <div class="col-md-2">
              <?=  $form->field($model, 'suspended')->checkbox()->label('status'); ?>

            </div>
          </div>
        </div>
      </div>
    </div>







    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
