<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\BillingRateDetail */

$this->title = 'Create Billing Rate Detail';
$this->params['breadcrumbs'][] = ['label' => 'Billing Rate Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billing-rate-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
