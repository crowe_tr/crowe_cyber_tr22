<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\trms\BillingRate;
use common\models\trms\Position;

/* @var $this yii\web\View */
/* @var $model common\models\trms\BillingRateDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="billing-rate-detail-form">

  <?php
  // $form = ActiveForm::begin();
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);

  ?>

  <div class="card card-default m-b-10">
    <div class="card-boy">
      <div class="form-group-attached">
        <div class="row">
          <div class="col-md-8">
            <?php
                echo $form->field($model, 'billingRateID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                        Select2::classname(),
                        [
                                'data' => ArrayHelper::map(BillingRate::find()->all(), 'id', 'description'),
                                'options' => ['id' => 'billingRateID', 'placeholder' => 'Select ...'],
                        ]
                );
            ?>
          </div>
          <div class="col-md-4">
            <?php
                echo $form->field($model, 'projectPositionID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                        Select2::classname(),
                        [
                                'data' => ArrayHelper::map(Position::find()->all(), 'id', 'positionName'),
                                'options' => ['id' => 'projectPositionID', 'placeholder' => 'Select ...'],
                        ]
                );
            ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?= $form->field($model, 'BillingRateValue')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
      </div>
    </div>
  </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
