<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaTerm */

$this->title = 'Create Area Term';
$this->params['breadcrumbs'][] = ['label' => 'Area Terms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="area-term-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
