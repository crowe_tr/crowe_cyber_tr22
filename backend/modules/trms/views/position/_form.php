<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\trms\PositionType;


/* @var $this yii\web\View */
/* @var $model common\models\trms\Position */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="position-form">

  <?php
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);
    ?>
    <div class="card card-default m-b-10">
      <div class="card-boy">
        <div class="form-group-attached">
          <div class="row">
            <div class="col-md-4">
              <?= $form->field($model, 'positionCode')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
              <?= $form->field($model, 'positionName')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
              <?php
                  echo $form->field($model, 'positionTypeId', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                                  'data' => ArrayHelper::map(PositionType::find()->all(), 'id', 'name'),
                                  'options' => ['id' => 'positionTypeId', 'placeholder' => 'Select ...'],
                          ]
                  );
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
