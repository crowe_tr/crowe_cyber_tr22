<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\trms\Area */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-form">

  <?php
    $form = ActiveForm::begin([
      'id' => 'crud-form', //perhatiin
      'enableClientValidation' => true,
      'validateOnSubmit' => true,
      'validateOnChange' => true,
      'validateOnType' => true,
      'fieldConfig' => [
          'template' => '{label}{input}',
          'options' => [
              'class' => 'form-group form-group-default',
          ],
    ],
    'errorSummaryCssClass'=> 'alert alert-danger'
    ]);
    echo $form->errorSummary($model);;
  ?>


  <div class="form-group-attached">
    <div class="row">
      <div class="col-md-4">
        <?=  $form->field($model, 'areaName')->textInput(); ?>
      </div>
      <div class="col-md-8">
          <?=  $form->field($model, 'suspended')->checkbox()->label('status'); ?>
      </div>
    </div>
  </div>
  <br/>

<div class="row">
  <div class="col-md-12 text-right m-t-5">
    <hr class="m-b-5"/>
    <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
    <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
  paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['trms/area/save', 'id'=>$id]); ?>';
  $('#crud-form').on('beforeSubmit', function() {

     // $(':input[type="submit"]').attr('disabled', true);
    var $form = new FormData($('#crud-form')[0]);
    $.ajax({
      url: paramJs.urlFormSave,
      type: 'POST',
      data: $form,
      async: false,
      cache: false,
      contentType: false,
      processData: false,

      success: function (data) {
        if(data != 1){
          alert(data);
        }else{
          $('#crud-modal').modal('hide');
          reload();
        }
      },
      error: function(jqXHR, errMsg) {
        alert(errMsg);
      }
    });
    return false;
  });

</script>
