<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\trms\search\Rule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rule-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'ruleType') ?>

    <?= $form->field($model, 'claimMeal') ?>

    <?= $form->field($model, 'useTaxi') ?>

    <?php // echo $form->field($model, 'claimTransportation') ?>

    <?php // echo $form->field($model, 'claimOutOffice') ?>

    <?php // echo $form->field($model, 'day') ?>

    <?php // echo $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'workHour') ?>

    <?php // echo $form->field($model, 'totalOvertimeHour') ?>

    <?php // echo $form->field($model, 'itemOvertime') ?>

    <?php // echo $form->field($model, 'isMealProvided') ?>

    <?php // echo $form->field($model, 'isTransportationProvided') ?>

    <?php // echo $form->field($model, 'isAccomodationProvided') ?>

    <?php // echo $form->field($model, 'claimTransportationAmount') ?>

    <?php // echo $form->field($model, 'withOvertimeTransport') ?>

    <?php // echo $form->field($model, 'applicablePosition') ?>

    <?php // echo $form->field($model, 'source') ?>

    <?php // echo $form->field($model, 'allowance') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
