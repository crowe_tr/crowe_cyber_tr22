<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use common\models\cm\CmDept;
use yii\web\View;
use common\models\trms\AreaEffective;
use common\models\trms\AreaItem;
use common\models\trms\AreaTerm;
use common\models\trms\AreaZone;


/* @var $this yii\web\View */
/* @var $searchModel common\models\trms\search\AreaEffectiveDet */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Area Effective Dets';
$this->params['breadcrumbs'][] = $this->title;
$column = [
            [
              'class' => 'kartik\grid\SerialColumn',
              'header' => '#',
              'mergeHeader' => false,
              // 'rowspan' => '1',
              // 'attribute' => 'id',
              'headerOptions' => ['class' => 'bg-success b-r'],
              'contentOptions' => ['class' => 'text-right'],
              'width' => '36px',
              'filterOptions' => ['class' => 'b-b b-grey'],
                // 'filterType' => GridView::FILTER_SELECT2,
                // 'filterWidgetOptions' => [
                //     'pluginOptions' => ['allowClear' => true],
                // ],
                // 'filterInputOptions' => ['placeholder' => ''],
                // 'format' => 'raw',
                // 'filter' => ArrayHelper::map(CmDept::find()->distinct()->all(), 'Id', 'Departement'),
                // 'value' => 'id',
            ],
            [
                'attribute' => 'areaEffectiveID',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => ''],
                'format' => 'raw',
                'filter' => ArrayHelper::map(AreaEffective::find()->distinct()->all(), 'id', 'description'),
                'value' => 'areaEffective.description',
                 // 'dateControlDisplayTimezone'=>  'Europe/Rome',
            ],
            [
                'attribute' => 'itemID',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => ''],
                'format' => 'raw',
                'filter' => ArrayHelper::map(AreaItem::find()->distinct()->all(), 'id', 'areaItemName'),
                'value' => 'item.areaItemName',
            ],
            [
                'attribute' => 'termID',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => ''],
                'format' => 'raw',
                'filter' => ArrayHelper::map(AreaTerm::find()->distinct()->all(), 'id', 'areaTermName'),
                'value' => 'term.areaTermName',
            ],
            [
                'attribute' => 'effectiveWorkTime',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'attribute' => 'effectiveWorkClock',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'attribute' => 'useTaxi',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'attribute' => 'zonaID',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => ''],
                'format' => 'raw',
                'filter' => ArrayHelper::map(AreaZone::find()->distinct()->all(), 'id', 'areaZoneCode'),
                'value' => 'zona.areaZoneCode',
            ],
            [
                'attribute' => 'overnightStay',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'attribute' => 'claimQty',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'attribute' => 'claimMax',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'attribute' => 'allowanceAmount',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'header' => '<i class="fa fa-cogs"></i>',
                'headerOptions' => ['class' => 'bg-success text-center'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'no-padding'],
                'format' => 'raw',
                'value' => function ($data) {
                    $val = '<div class="tooltip-action">
              <div class="trigger">
                '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
              </div>
              <div class="action-mask">
                <div class="action">
                  '.Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $data->id], [
                                            'class' => 'btn btn-default b-rounded padding-5 p-l-10 p-r-10',
                                            'style' => 'border-radius: 5px !important',
                                        ]).'
                  '.Html::a('<i class="pg-trash"></i>',
                                        ['delete', 'id' => $data->id],
                                        [
                                        'data-method' => 'post',
                                        'data-confirm' => 'Are you sure to delete this item?',
                                        'class' => 'btn btn-default b-rounded padding-5 p-l-10 p-r-10',
                                        'style' => 'border-radius: 5px !important',
                                        ]).'
                </div>
              </div>
            </div>';

                    return $val;
                },
            ],

        ];
?>
<div class="panel panel-default p-t-20 p-b-10 p-l-20 p-r-20" style="background: white">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
      <div class="col-md-6">
        <div class="pull-left">
          <h1><?= Html::encode($this->title) ?></h1>
        </div>
      </div>
      <div class="col-md-6">
        <div class="pull-right">
          <?= Html::a('<i class="pg-plus"></i> <span class="hidden-xs">ADD NEW</span>', ['create'], ['class' => 'btn btn-warning']) ?>
        </div>
      </div>
    </div>
    <?php
        echo GridView::widget(
            [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $column,

            'emptyText' => '
  									<div class="text-center" style="padding: 2em 0">
  										<i class="fa fa-exclamation-circle fa-5x text-warning"></i>
  										<br>
  										<br>
  										'.Yii::t('backend', 'You do not have any data within your Filters.').'
  										<br>
  										'.Yii::t('backend', 'To create a new data, click <b><i class="icon-plus-circle"></i> ADD NEW</b> on the right top of corner').'
  								</div>',

            'resizableColumns' => true,
            'bordered' => false,
            'striped' => false,
            'condensed' => false,
            'responsive' => false,
            'hover' => true,
            'persistResize' => false,
            ]
        );
    ?>
</div>
