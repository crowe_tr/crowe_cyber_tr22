<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\trms\search\AreaEffectiveDet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-effective-det-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'areaEffectiveID') ?>

    <?= $form->field($model, 'itemID') ?>

    <?= $form->field($model, 'termID') ?>

    <?= $form->field($model, 'effectiveWorkTime') ?>

    <?php // echo $form->field($model, 'effectiveWorkClock') ?>

    <?php // echo $form->field($model, 'useTaxi') ?>

    <?php // echo $form->field($model, 'zonaID') ?>

    <?php // echo $form->field($model, 'overnightStay') ?>

    <?php // echo $form->field($model, 'claimQty') ?>

    <?php // echo $form->field($model, 'claimMax') ?>

    <?php // echo $form->field($model, 'allowanceAmount') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
