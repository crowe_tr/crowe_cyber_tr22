<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaEffectiveDet */

$this->title = 'Create Area Effective Det';
$this->params['breadcrumbs'][] = ['label' => 'Area Effective Dets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="area-effective-det-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
