<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\trms\AreaEffective;
use common\models\trms\AreaItem;
use common\models\trms\AreaTerm;
use common\models\trms\AreaZone;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaEffectiveDet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-effective-det-form">

  <?php
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);
    ?>
    <div class="card card-default m-b-10">
      <div class="card-boy">
        <div class="form-group-attached">
          <div class="row">
            <div class="col-md-4">
              <?php
                  echo $form->field($model, 'areaEffectiveID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                                  'data' => ArrayHelper::map(AreaEffective::find()->all(), 'id', 'description'),
                                  'options' => ['id' => 'areaEffectiveID', 'placeholder' => 'Select ...'],
                          ]
                  );
              ?>
            </div>
            <div class="col-md-4">
              <?php
                  echo $form->field($model, 'itemID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                                  'data' => ArrayHelper::map(AreaItem::find()->all(), 'id', 'areaItemName'),
                                  'options' => ['id' => 'itemID', 'placeholder' => 'Select ...'],
                          ]
                  );
              ?>
            </div>
            <div class="col-md-4">
              <?php
                  echo $form->field($model, 'termID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                                  'data' => ArrayHelper::map(AreaTerm::find()->all(), 'id', 'areaTermName'),
                                  'options' => ['id' => 'termID', 'placeholder' => 'Select ...'],
                          ]
                  );
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <?php
                  echo $form->field($model, 'zonaID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                                  'data' => ArrayHelper::map(AreaZone::find()->all(), 'id', 'areaZoneCode'),
                                  'options' => ['id' => 'zonaID', 'placeholder' => 'Select ...'],
                          ]
                  );
              ?>
            </div>
            <div class="col-md-4">
              <?= $form->field($model, 'effectiveWorkTime')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
              <?= $form->field($model, 'effectiveWorkClock')->textInput() ?>
            </div>

          </div>
          <div class="row">
            <div class="col-md-4">
              <?= $form->field($model, 'claimQty')->textInput() ?>
            </div>
            <div class="col-md-4">
              <?= $form->field($model, 'claimMax')->textInput() ?>
            </div>
            <div class="col-md-4">
              <?= $form->field($model, 'allowanceAmount')->textInput(['maxlength' => true]) ?>
            </div>

          </div>
        </div>
      </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
