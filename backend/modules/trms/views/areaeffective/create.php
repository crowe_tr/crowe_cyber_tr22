<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaEffective */

$this->title = 'Create Area Effective';
$this->params['breadcrumbs'][] = ['label' => 'Area Effectives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="area-effective-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
