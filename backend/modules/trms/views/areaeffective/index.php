<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
// use common\models\cm\CmDept;


/* @var $this yii\web\View */
/* @var $searchModel common\models\trms\search\AreaEffectiveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Area Effectives';
$this->params['breadcrumbs'][] = $this->title;

$column = [
            [
              'class' => 'kartik\grid\SerialColumn',
              'header' => '#',
              'mergeHeader' => false,
              // 'rowspan' => '1',
              // 'attribute' => 'id',
              'headerOptions' => ['class' => 'bg-success b-r'],
              'contentOptions' => ['class' => 'text-right'],
              'width' => '36px',
              'filterOptions' => ['class' => 'b-b b-grey'],
                // 'attribute' => 'id',
                // 'headerOptions' => ['class' => 'col-sm-2 col-md-2 bg-success'],
                // 'filterOptions' => ['class' => 'b-b b-grey'],
                // 'contentOptions' => ['class' => 'kv-align-middle'],
                // 'filterType' => GridView::FILTER_SELECT2,
                // 'filterWidgetOptions' => [
                //     'pluginOptions' => ['allowClear' => true],
                // ],
                // 'filterInputOptions' => ['placeholder' => ''],
                // 'format' => 'raw',
                // 'filter' => ArrayHelper::map(CmDept::find()->distinct()->all(), 'Id', 'Departement'),
                // 'value' => 'id',
            ],
            [
                'attribute' => 'effectivedate',
                'headerOptions' => ['class' => 'col-sm-3 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
                 // 'dateControlDisplayTimezone'=>  'Europe/Rome',
            ],
            [
                'attribute' => 'description',
                'headerOptions' => ['class' => 'col-sm-8 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'header' => '<i class="fa fa-cogs"></i>',
                'headerOptions' => ['class' => 'bg-success text-center'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'no-padding'],
                'format' => 'raw',
                'value' => function ($data) {
                    $val = '<div class="tooltip-action">
              <div class="trigger">
                '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
              </div>
              <div class="action-mask">
                <div class="action">
                  '.Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $data->id], [
                                            'class' => 'btn btn-default b-rounded padding-5 p-l-10 p-r-10',
                                            'style' => 'border-radius: 5px !important',
                                        ]).'
                  '.Html::a('<i class="pg-trash"></i>',
                                        ['delete', 'id' => $data->id],
                                        [
                                        'data-method' => 'post',
                                        'data-confirm' => 'Are you sure to delete this item?',
                                        'class' => 'btn btn-default b-rounded padding-5 p-l-10 p-r-10',
                                        'style' => 'border-radius: 5px !important',
                                        ]).'
                </div>
              </div>
            </div>';

                    return $val;
                },
            ],

        ];
?>
<div class="panel panel-default p-t-20 p-b-10 p-l-20 p-r-20" style="background: white">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
      <div class="col-md-6">
        <div class="pull-left">
          <h1><?= Html::encode($this->title) ?></h1>
        </div>
      </div>
      <div class="col-md-6">
        <div class="pull-right">
          <?= Html::a('<i class="pg-plus"></i> <span class="hidden-xs">ADD NEW</span>', ['create'], ['class' => 'btn btn-warning']) ?>
        </div>
      </div>
    </div>
    <?php
        echo GridView::widget(
            [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $column,



            'resizableColumns' => true,
            'bordered' => false,
            'striped' => false,
            'condensed' => false,
            'responsive' => false,
            'hover' => true,
            'persistResize' => false,
            ]
        );
    ?>
</div>

<div class="card">

</div>
