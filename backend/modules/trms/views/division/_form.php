<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\trms\Division */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="division-form">

  <?php
  // $form = ActiveForm::begin();
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);

  ?>

  <div class="card card-default m-b-10">
    <div class="card-boy">
      <div class="form-group-attached">
        <div class="row">
          <div class="col-md-3">
            <?= $form->field($model, 'divCode')->label('Code')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-7">
            <?= $form->field($model, 'divName')->label('Division Name')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-2">
            <?= $form->field($model, 'suspended')->checkbox()->label('Status') ?>
          </div>
        </div>
      </div>
    </div>
  </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
