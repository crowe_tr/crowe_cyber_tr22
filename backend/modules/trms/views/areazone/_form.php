<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaZone */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-zone-form">

  <?php
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);
    ?>

    <div class="card card-default m-b-10">
      <div class="card-boy">
        <div class="form-group-attached">
          <div class="row">
            <div class="col-md-2">
              <?= $form->field($model, 'areaZoneCode')->label('Code')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-8">
              <?= $form->field($model, 'areaZoneDescription')->label('Description')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'suspended')->checkbox()->label('Status') ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
