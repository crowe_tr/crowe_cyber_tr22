<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\BillingRate */

$this->title = 'Create Billing Rate';
$this->params['breadcrumbs'][] = ['label' => 'Billing Rates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billing-rate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
