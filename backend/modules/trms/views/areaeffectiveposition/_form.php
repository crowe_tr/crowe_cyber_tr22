<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\trms\AreaEffectiveDet;
use common\models\trms\Position;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaEffectivePosition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-effective-position-form">

  <?php
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);
    ?>

    <div class="card card-default m-b-10">
      <div class="card-boy">
        <div class="form-group-attached">
          <div class="row">
            <div class="col-md-6">
              <?php
                  echo $form->field($model, 'areaEffectiveDetID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                                  'data' => ArrayHelper::map(AreaEffectiveDet::find()->all(), 'id', 'areaEffectiveID'),
                                  'options' => ['id' => 'areaEffectiveDetID', 'placeholder' => 'Select ...'],
                          ]
                  );
              ?>
            </div>
            <div class="col-md-6">
              <?= $form->field($model, 'seq')->textInput() ?>
            </div>

          </div>
          <div class="row">
            <div class="col-md-6">
              <?php
                  echo $form->field($model, 'empPositionID', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                          Select2::classname(),
                          [
                                  'data' => ArrayHelper::map(Position::find()->all(), 'id', 'positionName'),
                                  'options' => ['id' => 'empPositionID', 'placeholder' => 'Select ...'],
                          ]
                  );
              ?>
            </div>
            <div class="col-md-6">
              <?= $form->field($model, 'AllowanceValue')->textInput(['maxlength' => true]) ?>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
