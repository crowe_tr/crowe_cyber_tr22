<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaEffectivePosition */

$this->title = 'Update Area Effective Position: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Area Effective Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="area-effective-position-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
