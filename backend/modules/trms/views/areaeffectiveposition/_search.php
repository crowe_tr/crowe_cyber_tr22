<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\trms\search\AreaEffectivePosition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-effective-position-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'areaEffectiveDetID') ?>

    <?= $form->field($model, 'seq') ?>

    <?= $form->field($model, 'empPositionID') ?>

    <?= $form->field($model, 'AllowanceValue') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
