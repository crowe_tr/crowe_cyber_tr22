<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\trms\search\RuleField */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rule-field-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sourceRule') ?>

    <?= $form->field($model, 'attribute') ?>

    <?= $form->field($model, 'label') ?>

    <?= $form->field($model, 'filedType') ?>

    <?php // echo $form->field($model, 'range') ?>

    <?php // echo $form->field($model, 'widget') ?>

    <?php // echo $form->field($model, 'position') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
