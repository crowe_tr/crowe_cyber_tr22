<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\trms\RuleField */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rule-field-form">

  <?php
  // $form = ActiveForm::begin();
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);

  ?>

  <div class="card card-default m-b-10">
    <div class="card-boy">
      <div class="form-group-attached">
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'sourceRule')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-6">
            <?= $form->field($model, 'attribute')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-6">
            <?= $form->field($model, 'filedType')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <?= $form->field($model, 'range')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-4">
            <?= $form->field($model, 'widget')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-4">
            <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
      </div>
    </div>
  </div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
