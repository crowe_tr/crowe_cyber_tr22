Note CROWE Developer

========_________==========
Jumat, 23 Agustus 2019

- Add Module CmClient (CRUD) via Gii
    - Model : @app/models/cm/CmClient.php
    - Model Search : @app/models/cm/search/CmClientSearch.php
    - Controller : @app/backend/modules/common/controllers/ClientControllers.php
    - Modules : @app/backend/modules/common/controllers/client/

    - Update _form Layout

- Add 3 Field di Table cmClient
    - 1. OfficeZipCode
    - 2. BillingZipCode
    - 3. OtherZipCode

==========================
Sabtu, 24 Agustus 2019

- Update _form Layout CmClient

- Add Module CmClientIndustries (CRUD) via Gii
    - Model : @app/models/cm/CmClient.php
    - Model Search : @app/models/cm/search/CmClientIndustriesSearch.php
    - Controller : @app/backend/modules/common/controllers/ClientindustriesControllers.php
    - Modules : @app/backend/modules/common/controllers/clientindustries/

    - Update _form Layout

- Update Table cmClient
    - Add :
        - Status
        - Flag
        - CreatedBy
        - CreatedDate
        - LastUpdateBy
        - LastUpdateDate
        - DeletedBy
        - DeletedDate

- Update Table cmClientIndustries
    - Add :
        - Status
        - Flag
        - CreatedBy
        - CreatedDate
        - LastUpdateBy
        - LastUpdateDate
        - DeletedBy
        - DeletedDate

==========================
Minggu, 25 Agustus 2019

	- Update _form Layout CmClient & CmClientIndustries (Sample from Proku)

	- Update table menu, add :
		21	Create Client	2	/common/client/index			1	{"icon": "c", "module": "tr"}
		22	Industries		2	/common/clientindustries/index	2	{"icon": "i", "module": "tr"}
