<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>
  <style type="text/css">
    * {
      -webkit-font-smoothing: antialiased;
    }

    body {
      Margin: 0;
      padding: 0;
      min-width: 100%;
      font-family: Arial, sans-serif;
      -webkit-font-smoothing: antialiased;
      mso-line-height-rule: exactly;
    }

    table {
      border-spacing: 0;
      color: #333333;
      font-family: Arial, sans-serif;
    }

    img {
      border: 0;
    }

    .wrapper {
      width: 100%;
      table-layout: fixed;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    .webkit {
      max-width: 600px;
    }

    .outer {
      Margin: 0 auto;
      width: 100%;
      max-width: 600px;
    }

    .full-width-image img {
      width: 100%;
      max-width: 600px;
      height: auto;
    }

    .inner {
      padding: 10px;
    }

    p {
      Margin: 0;
      padding-bottom: 10px;
    }

    .h1 {
      font-size: 21px;
      font-weight: bold;
      Margin-top: 15px;
      Margin-bottom: 5px;
      font-family: Arial, sans-serif;
      -webkit-font-smoothing: antialiased;
    }

    .h2 {
      font-size: 18px;
      font-weight: bold;
      Margin-top: 10px;
      Margin-bottom: 5px;
      font-family: Arial, sans-serif;
      -webkit-font-smoothing: antialiased;
    }

    .one-column .contents {
      text-align: left;
      font-family: Arial, sans-serif;
      -webkit-font-smoothing: antialiased;
    }

    .one-column p {
      font-size: 14px;
      Margin-bottom: 10px;
      font-family: Arial, sans-serif;
      -webkit-font-smoothing: antialiased;
    }

    .two-column {
      text-align: center;
      font-size: 0;
    }

    .two-column .column {
      width: 100%;
      max-width: 300px;
      display: inline-block;
      vertical-align: top;
    }

    .contents {
      width: 100%;
    }

    .two-column .contents {
      font-size: 14px;
      text-align: left;
    }

    .two-column img {
      width: 100%;
      max-width: 280px;
      height: auto;
    }

    .two-column .text {
      padding-top: 10px;
    }

    .three-column {
      text-align: center;
      font-size: 0;
      padding-top: 10px;
      padding-bottom: 10px;
    }

    .three-column .column {
      width: 100%;
      max-width: 200px;
      display: inline-block;
      vertical-align: top;
    }

    .three-column .contents {
      font-size: 14px;
      text-align: center;
    }

    .three-column img {
      width: 100%;
      max-width: 180px;
      height: auto;
    }

    .three-column .text {
      padding-top: 10px;
    }

    .img-align-vertical img {
      display: inline-block;
      vertical-align: middle;
    }

    @media only screen and (max-device-width: 480px) {
      table[class=hide],
      img[class=hide],
      td[class=hide] {
        display: none !important;
      }
  </style>
  <!--[if (gte mso 9)|(IE)]>
	<style type="text/css">
		table {border-collapse: collapse !important;}
	</style>
	<![endif]-->
</head>

<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#eee;">
  <center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#fafafa;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#eee" bgcolor="#eee;">
      <tbody>
        <tr>
          <td width="100%">
            <div class="webkit" style="max-width:600px;Margin:0 auto;">

              <!--[if (gte mso 9)|(IE)]>

						<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0" >


              <!-- ======= start main body ======= -->
              <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;Margin:0 auto;width:100%;max-width:600px;">
                <tbody>
                  <tr>
                    <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                      <!-- ======= start header ======= -->

                      <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tbody>
                          <tr>
                            <td>
                              <table style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                                <tbody>
                                  <tr>
                                    <td align="center">
                                      <center>
                                        <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" style="Margin: 0 auto;">
                                          <tbody>
                                            <tr>
                                              <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0">
                                                  <tbody>
                                                    <tr>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                      <td align="center">
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td align="center">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                      <td height="6" bgcolor="#0062cc" class="contents" style="width:100%; border-top-left-radius:10px; border-top-right-radius:10px"></td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </center>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>

                      <table class="one-column" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0" bgcolor="#FFFFFF">
                        <tbody>
                          <tr>
                            <td align="left" style="padding:0px 40px 40px 40px">
                              <p style="color:#5b5f65; font-size:14px; text-align:center; font-family: Open Sans, Verdana, Geneva, sans-serif">
                                <b><?= $title ?></b>
                                <br><?= $status?>
                              </p>

                              <table style="border-radius: 8px; font-size: 14px; background: #f1f1f1; padding: 24px 10px; width: 100%">
                                <tr style="border-bottom: thin solid #000">
                                  <td colspan="3">
                                      <small>JOB DESCRIPTION </small>
                                      <br><b> <?= $modelJob->JobCode; ?> -  <?= $modelJob->Description; ?> </b>
                                  </td>
                                </tr>
                                <tr style=" border-bottom: thin solid #000">
                                  <td colspan="3">
                                    <br/>
                                      <small> CLIENT </small>
                                      <br><b>  <?= $modelJob->client->Name; ?>  </b>
                                  </td>
                                </tr>
                                <tr style=" border-bottom: thin solid #000">
                                  <td colspan="3">
                                    <br/>
                                    <small> PERIODE  </small>
                                    <br><b>  <?= date('d F Y', strtotime($modelJob->StartDate)) ; ?> s.d.  <?= date('d F Y', strtotime($modelJob->EndDate)) ; ?></b>
                                  </td>
                                </tr>
                                <tr style=" border-bottom: thin solid #000">
                                  <td>
                                    <br/>
                                      <small>  JOB AREA </small>
                                      <br><b>   <?= $modelJob->JobArea ?>  </b>
                                  </td>
                                  <td>
                                    <br/>
                                      <small> TOTAL FEE </small>
                                      <br><b>    Rp <?= number_format($modelJob->Fee) ?> (<?= ($modelJob->IncludeOPE) ? "Include OPE" : "Exclude OPE"; ?>)  </b>
                                  </td>
                                </tr>
                              </table>





                              <center>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                  <tbody>
                                    <tr>
                                      <td>
                                        <table border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td height="20" width="100%" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <table border="0" align="center" cellpadding="0" cellspacing="0" style="Margin:0 auto;">
                                          <tbody>
                                            <tr>
                                              <td align="center">
                                                <table border="0" cellpadding="0" cellspacing="0" style="Margin:0 auto;">
                                                  <tbody>
                                                    <tr>
                                                      <td width="250" height="50" align="center" bgcolor="#f3a005" style="
                                                          border-radius: 30px;
                                                      ">

                                                      <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/jobapproval/view', 'id' => $modelJob->JobID]); ?>" style="width:125; display:block;
                                                      text-decoration:none; border:0; text-align:center;
                                                      font-weight:bold;font-size:16px; font-family: Arial,
                                                      sans-serif; color: #ffffff;"
                                                      class="button_link">VIEW HERE</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </center>

                              <!-- END BUTTON -->
                            </td>
                          </tr>
                          <tr>
                            <td class="contents" style="width:100%; border-bottom-left-radius:10px; border-bottom-right-radius:10px" height="6" bgcolor="#0062cc"></td>
                          </tr>
                        </tbody>
                      </table>





                      <!-- ======= end article1 ======= -->
                      <p style="text-align: center">
                        <br/>
                        <small>
                          This is automated email notification. Please dont reply
                          <br/>Copyright © 2019 Crowe - Powered by <a target="_blank" href="https://antariksa.org">antariksa</a>
                        </small>
                      </p>

                    </td>
                  </tr>
                </tbody>
              </table>
              <!--[if (gte mso 9)|(IE)]>
					</td>
				</tr>
			</table>
			<![endif]-->
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </center>


</body>

</html>
