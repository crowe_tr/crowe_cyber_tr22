<?php

$table = "<table border='1' class='table' style='border-collapse: collapse;'>";
$table .= "<tr>";
$table .= "<th>Employee Name</th>";
$table .= "<th>Job</th>";
$table .= "<th>WH</th>";
$table .= "<th>OT</th>";
$table .= "</tr>";

if (!empty($h['approval_id'])) {
    $detail = Yii::$app->db->createCommand("call monthly_approval1_detail('" . date('Y-m-01') . "', '" . date('Y-m-t') . "', '" . $h['approval_id'] . "')")->queryAll();
    $array_detail = [];
    foreach ($detail as $d) {
        $array_detail[$d['date']]['date'] = $d;
        $array_detail[$d['date']]['data'][] = $d;
    }

    foreach ($array_detail as $detail) {
        $table .= "<tr>";
        $table .= "<th colspan='2'><h2 style='font-size: 14px; font-weight:bold; margin: 0; padding: 0'>" . $d['date'] . "</h2></th>";
        $table .= "<th colspan='2'><a style='background: orange; font-size: 14px; color: black; padding: 6px 13px; margin: 0; text-align:center' href='" . Yii::$app->urlManager->createAbsoluteUrl(['/tr/trsupervisorapproval/detail', 'id' => $d['date']]) . "'>Go to Approval page</a></th>";
        $table .= "</tr>";

        foreach ($detail['data'] as $d) {
            $table .= "<tr>";
            $table .= "<td>" . $d['employee_name'] . "<br><small>" . $d['employee_level'] . "</small></td>";
            $table .= "<td>" . $d['job_description'] . "</td>";
            $table .= "<td>" . $d['workhour'] . "</td>";
            $table .= "<td>" . $d['overtime'] . "</td>";
            $table .= "</tr>";
        }
    }
}
$table .= "<table>";
echo $table;
