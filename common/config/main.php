<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)).'/vendor',
    'language' => 'en',
    'components' => [
        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ], 
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
		    'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
				'class' => 'Swift_SmtpTransport',
                'host' => '116.197.129.181',
                'username' => 'robot@apps.dutacendana.co.id',
                'password' => 'dcaRobot123!',
                'port' => '26',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],
    ],
];
