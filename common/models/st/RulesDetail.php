<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stRulesDetail".
 *
 * @property int $id
 * @property int $rulesID
 * @property int $itemID
 * @property int $termID
 * @property string $minOvertime
 * @property int $isClaim
 * @property int $claimQty
 * @property int $useTaxi
 * @property string $minClock
 * @property int $stayed
 * @property string $allowanceAmount
 *
 * @property StRuleItem $item
 * @property StRules $rules
 * @property StRuleTermZone $term
 * @property StRulesLevel[] $stRulesLevels
 */
class RulesDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $TabularInput;

    public static function tableName()
    {
        return 'st_rules_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'rules_id', 'item_id', 'term_id', 'TabularInput'], 'safe'],

            [['claim_qty'], 'required'],
            [['rules_id', 'item_id', 'term_id', 'is_claim', 'claim_qty', 'use_taxi', 'stayed'], 'integer'],
            [['min_overtime', 'allowance_amount'], 'number'],
            [['min_clock'], 'safe'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => RuleItem::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['rules_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rules::className(), 'targetAttribute' => ['rules_id' => 'id']],
            [['term_id'], 'exist', 'skipOnError' => true, 'targetClass' => RuleTermZone::className(), 'targetAttribute' => ['term_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rules_id' => Yii::t('app', 'Rules ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'term_id' => Yii::t('app', 'Term ID'),
            'min_overtime' => Yii::t('app', 'Min Overtime'),
            'is_claim' => Yii::t('app', 'Is Claim'),
            'claim_qty' => Yii::t('app', 'Claim Qty'),
            'use_taxi' => Yii::t('app', 'Use Taxi'),
            'min_clock' => Yii::t('app', 'Min Clock'),
            'stayed' => Yii::t('app', 'Stayed'),
            'allowance_amount' => Yii::t('app', 'Allowance Amount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleItem()
    {
        return $this->hasOne(RuleItem::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRules()
    {
        return $this->hasOne(Rules::className(), ['id' => 'rules_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleTermZone()
    {
        return $this->hasOne(RuleTermZone::className(), ['id' => 'term_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRulesLevels()
    {
        return $this->hasMany(RulesLevel::className(), ['rules_id' => 'id']);
    }
}
