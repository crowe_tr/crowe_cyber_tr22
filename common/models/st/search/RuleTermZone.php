<?php

namespace common\models\st\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\st\RuleTermZone as RuleTermZoneModel;

/**
 * RuleTermZone represents the model behind the search form of `common\models\st\RuleTermZone`.
 */
class RuleTermZone extends RuleTermZoneModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],

            [['id', 'is_stayed', 'status'], 'integer'],
            [['term_zone_name', 'term_zone_description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RuleTermZoneModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_stayed' => $this->is_stayed,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'term_zone_name', $this->term_zone_name])
            ->andFilterWhere(['like', 'term_zone_description', $this->term_zone_description]);

        return $dataProvider;
    }
}
