<?php

namespace common\models\st\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\st\BillingRateDetail as BillingRateDetailModel;

/**
 * BillingRateDetail represents the model behind the search form of `common\models\st\BillingRateDetail`.
 */
class BillingRateDetail extends BillingRateDetailModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'billing_rate_id', 'level_id'], 'integer'],
            [['billing_rate_value'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BillingRateDetailModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'billing_rate_id' => $this->billing_rate_id,
            'level_id' => $this->level_id,
            'billing_rate_value' => $this->billing_rate_value,
        ]);

        return $dataProvider;
    }
}
