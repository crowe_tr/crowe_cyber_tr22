<?php

namespace common\models\st\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\st\RuleItem as RuleItemModel;

/**
 * RuleItem represents the model behind the search form of `common\models\st\RuleItem`.
 */
class RuleItem extends RuleItemModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['id','is_client_zone'], 'integer'],
            [['term_item_name', 'term_item_desc', 'term_zone_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RuleItemModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_client_zone' => $this->is_client_zone,
        ]);

        $query->andFilterWhere(['like', 'term_item_name', $this->term_item_name])
            ->andFilterWhere(['like', 'term_item_desc', $this->term_item_desc])
            ->andFilterWhere(['like', 'term_zone_name', $this->term_zone_name]);

        return $dataProvider;
    }
}
