<?php

namespace common\models\st;

use Yii;

class HolidayTimeOff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'st_holiday_timeOff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['holiday_date', 'type_id','description'], 'required'],

            [['type_id'], 'integer'],
            [['description'], 'string', 'max' => 45],
            [['holiday_date'], 'unique'],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => HolidayType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'holiday_date' => Yii::t('app', 'Date'),
            'type_id' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(HolidayType::className(), ['id' => 'type_id']);
    }
}
