<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stRules".
 *
 * @property int $id
 * @property string $effectiveDate
 * @property string $description
 *
 * @property StRulesDetail[] $stRulesDetails
 */
class Rules extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'st_rules';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['effective_date', 'description'], 'required'],
            [['effective_date'], 'safe'],
            [['description'], 'string', 'max' => 240],
            [['effective_date'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'effective_date' => Yii::t('app', 'Effective Date'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRulesDetails()
    {
        return $this->hasMany(RulesDetail::className(), ['rules_id' => 'id']);
    }
}
