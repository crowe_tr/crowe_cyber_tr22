<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stRuleTermZone".
 *
 * @property int $id
 * @property string $termZoneName
 * @property string $termZoneDescription
 * @property int $isStayed
 *
 * @property StRulesDetail[] $stRulesDetails
 */
class RuleTermZone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'st_rule_term_zone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','is_stayed', 'status'], 'safe'],
            [['term_zone_name'], 'required'],
            [['term_zone_name'], 'string', 'max' => 30],
            [['term_zone_description'], 'string', 'max' => 255],
            [['term_zone_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'term_zone_name' => Yii::t('app', 'Name'),
            'term_zone_description' => Yii::t('app', 'Description'),
            'is_stayed' => Yii::t('app', 'Is Stayed'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRulesDetails()
    {
        return $this->hasMany(RulesDetail::className(), ['term_id' => 'id']);
    }
}
