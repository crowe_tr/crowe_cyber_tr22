<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stHolidayType".
 *
 * @property int $id
 * @property string $holidayTypeName
 * @property int $flag
 *
 * @property StHolidayTimeOff[] $stHolidayTimeOffs
 */
class HolidayType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'st_holiday_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','is_holiday'], 'safe'],
            [['holiday_type_name'], 'required'],
            [['flag'], 'integer'],
            [['holiday_type_name'], 'string', 'max' => 20],
            [['holiday_type_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'holiday_type_name' => Yii::t('app', 'Type'),
            'flag' => Yii::t('app', 'Status'),
            'is_holiday'=>'Holiday'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHolidayTimeOffs()
    {
        return $this->hasMany(HolidayTimeOff::className(), ['type_id' => 'id']);
    }
}
