<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stBillingRate".
 *
 * @property int $id
 * @property string $description
 * @property string $effectiveDate
 *
 * @property StBillingRateDetail[] $stBillingRateDetails
 * @property CmLevel[] $levels
 */
class BillingRate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'st_billing_rate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],

            [['effective_date', 'description'], 'required'],
            [['description'], 'string', 'max' => 255],
            [['effective_date'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'effective_date' => Yii::t('app', 'Efective Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStBillingRateDetails()
    {
        return $this->hasMany(StBillingRateDetail::className(), ['billing_rate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevels()
    {
        return $this->hasMany(CmLevel::className(), ['id' => 'level_id'])->viaTable('st_billing_rate_detail', ['billing_rate_id' => 'id']);
    }
}
