<?php

namespace common\models\st;

use Yii;
use common\models\cm\Level;

class BillingRateDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $TabularInput;
    public $Level;

    public static function tableName()
    {
        return 'st_billing_rate_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['billing_rate_id', 'level_id', 'billing_rate_value'], 'required'],
            [['billing_rate_id', 'level_id'], 'integer'],
            [['billing_rate_value'], 'number'],
            [['TabularInput'], 'safe'],
            [['billing_rate_id', 'level_id'], 'unique', 'targetAttribute' => ['billing_rate_id', 'level_id']],
            [['billing_rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillingRate::className(), 'targetAttribute' => ['billing_rate_id' => 'id']],
            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['level_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'billing_rate_id' => Yii::t('app', 'Billing Rate ID'),
            'level_id' => Yii::t('app', 'Level ID'),
            'billing_rate_value' => Yii::t('app', 'Billing Rate Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingRate()
    {
        return $this->hasOne(BillingRate::className(), ['id' => 'billing_rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'level_id']);
    }
}
