<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stRuleTaskAE".
 *
 * @property int $levelID
 * @property int $taskID
 * @property int $percentage
 */
class RuleTaskAE extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $TabularInput;
    public static function tableName()
    {
        return 'st_rule_task_ae';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TabularInput'], 'safe'],
            [['level_id', 'task_id', 'percentage'], 'required'],
            [['level_id', 'task_id', 'percentage'], 'integer'],
            [['level_id', 'task_id'], 'unique', 'targetAttribute' => ['level_id', 'task_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'level_id' => Yii::t('app', 'Level'),
            'task_id' => Yii::t('app', 'Task'),
            'percentage' => Yii::t('app', 'Percentage'),
        ];
    }
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

}
