<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stTaskType".
 *
 * @property int $id
 * @property string $taskTypeName
 * @property int $flag
 */
class TaskType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'st_task_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','is_meal','is_ope','is_taxi','used_project'], 'safe'],
            [['task_type_name'], 'required'],
            [['flag'], 'integer'],
            [['task_type_name'], 'string', 'max' => 30],
            [['task_type_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'task_type_name' => Yii::t('app', 'Type Name'),
            'flag' => Yii::t('app', 'Status'),
            'is_meal' => Yii::t('app', 'Meal'),
            'is_ope' => Yii::t('app', 'OPE'),
            'is_taxi' => Yii::t('app', 'Taxi'),
            'used_project'=> 'Project',
        ];
    }
}
