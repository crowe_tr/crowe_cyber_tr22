<?php

namespace common\models\rg;

use Yii;

class RgAuthAssigment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auth_assigment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            [['item_name'], 'string', 'max' => 64],
        ];
    }

    public function primaryKey()
    {
        return ['item_name', 'user_id'];
    }

    public function attributeLabels()
    {
        return [
            'item_name' => Yii::t('backend', 'Item Name'),
            'user_id' => Yii::t('backend', 'User ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
