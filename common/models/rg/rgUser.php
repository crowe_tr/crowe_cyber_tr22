<?php

namespace common\models\rg;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\hr\Employee;
use common\models\cm\CmCompanyBranch;
use common\models\cm\CmCompany;

class rgUser extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 10;
    public $Captcha;
    public $PasswordRepeat;
    public $Permissions;
    public $Permissions2;
    public $Permissions3;

    public static function tableName()
    {
        return '{{%rgUser}}';
    }

    public function rules()
    {
        return [
            [['Id'], 'unique'],
            [['Id', 'Email', 'EmployeeId', 'CompanyId', 'BranchId', 'PasswordHash', 'PasswordRepeat', 'DeptId'], 'required'],
            [['canChangeCompany', 'canChangeBranch', 'canChangeDept', 'Permissions', 'Permissions2', 'Permissions3'], 'safe'],
            [['PasswordResetToken', 'Status', 'Flag'], 'integer'],
            [['LastUpdateDate', 'CreatedDate'], 'safe'],
            [['Id'], 'string', 'max' => 16],
            [['CompanyId'], 'string', 'max' => 9],
            [['BranchId'], 'string', 'max' => 12],
            [['EmployeeId'], 'string', 'max' => 32],
            [['Email', 'AuthKey'], 'string', 'max' => 32],
            [['PasswordHash', 'AccountActivationToken'], 'string', 'max' => 255],
            [['LastUpdateBy', 'CreatedBy'], 'string', 'max' => 64],
            ['PasswordRepeat', 'compare', 'compareAttribute' => 'PasswordHash', 'message' => "Passwords don't match"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'canChangeCompany' => 'Company',
            'canChangeBranch' => 'Branch',
            'canChangeDept' => 'Departement',
            'Id' => 'Username',
            'CompanyId' => 'Company',
            'BranchId' => 'Branch',
            'DeptId' => 'Departement',
            'Email' => 'Email',
            'PasswordHash' => 'Password',
            'PasswordRepeat' => 'Repeat Password',
            'AuthKey' => 'Auth Key',
            'PasswordResetToken' => 'Password Reset Token',
            'AccountActivationToken' => 'Account Activation Token',
            'Status' => 'Status',
            'LastUpdateBy' => 'Last Update By',
            'LastUpdateDate' => 'Last Update Date',
            'CreatedBy' => 'Created By',
            'CreatedDate' => 'Created Date',
            'Flag' => 'Flag',
        ];
    }

    public static function findIdentity($username)
    {
        return static::findOne(['Id' => $username, 'Status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findById($username)
    {
        return static::findOne(['Id' => $username, 'Status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['EMAIL' => $email, 'STATUS' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'PasswordResetToken' => $token,
            'Status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->AuthKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        if (is_null($this->PasswordHash)) {
            return false;
        }

        return Yii::$app->security->validatePassword($password, $this->PasswordHash);
    }

    public function setPassword($password)
    {
        $this->PasswordHash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->AuthKey = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString().'_'.time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['Id' => 'EmployeeId']);
    }
    public function getCompany()
    {
        return $this->hasOne(CmCompany::className(), ['Id' => 'CompanyId']);
    }
    public function getBranch()
    {
        return $this->hasOne(CmCompanyBranch::className(), ['CompanyId' => 'CompanyId', 'Id' => 'BranchId']);
    }
}
