<?php

namespace common\models\cl\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\cl\ClientIndustries as ClientIndustriesModel;

/**
 * ClientIndustries represents the model behind the search form of `common\models\cl\ClientIndustries`.
 */
class ClientIndustries extends ClientIndustriesModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'flag'], 'integer'],
            [['industries', 'created_by', 'created_date', 'last_update_by', 'last_update_date',
              'deleted_by', 'deleted_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientIndustriesModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'flag' => $this->flag,
            'created_date' => $this->created_date,
            'last_update_date' => $this->last_update_date,
            'deleted_date' => $this->deleted_date,
        ]);

        $query->andFilterWhere(['like', 'industries', $this->industries])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'last_update_by', $this->last_update_by])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by]);

        return $dataProvider;
    }
}
