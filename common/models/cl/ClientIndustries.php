<?php

namespace common\models\cl;

use Yii;

/**
 * This is the model class for table "clClientIndustries".
 *
 * @property int $Id
 * @property string $Industries
 * @property int $Status
 * @property int $Flag
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property string $LastUpdateBy
 * @property string $LastUpdateDate
 * @property string $DeletedBy
 * @property string $DeletedDate
 */
class ClientIndustries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cl_client_industries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'Status'], 'required'],
            [['id', 'Status', 'flag'], 'integer'],
            [['created_date', 'last_update_date', 'deleted_date'], 'safe'],
            [['industries'], 'string', 'max' => 128],
            [['created_by', 'last_update_by', 'deleted_by'], 'string', 'max' => 64],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'industries' => Yii::t('app', 'Industries'),
            'status' => Yii::t('app', 'Status'),
            'flag' => Yii::t('app', 'Flag'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_date' => Yii::t('app', 'Created Date'),
            'last_update_by' => Yii::t('app', 'Last Update By'),
            'last_update_date' => Yii::t('app', 'Last Update Date'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'deleted_date' => Yii::t('app', 'Deleted Date'),
        ];
    }
}
