<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsAreaEffectiveDet".
 *
 * @property int $id
 * @property int $areaEffectiveID
 * @property int $itemID
 * @property int $termID
 * @property string $effectiveWorkTime
 * @property string $effectiveWorkClock
 * @property int $useTaxi
 * @property int $zonaID
 * @property int $overnightStay
 * @property int $claimQty
 * @property int $claimMax
 * @property string $allowanceAmount
 *
 * @property TrMsAreaEffective $areaEffective
 * @property TrMsAreaItem $item
 * @property TrMsAreaTerm $term
 * @property TrMsAreaZone $zona
 * @property TrMsAreaEffectivePosition[] $trMsAreaEffectivePositions
 */
class AreaEffectiveDet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsAreaEffectiveDet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['areaEffectiveID', 'itemID', 'termID', 'claimQty'], 'required'],
            [['areaEffectiveID', 'itemID', 'termID', 'useTaxi', 'zonaID', 'overnightStay', 'claimQty', 'claimMax'], 'integer'],
            [['effectiveWorkTime', 'allowanceAmount'], 'number'],
            [['effectiveWorkClock'], 'safe'],
            [['areaEffectiveID'], 'exist', 'skipOnError' => true, 'targetClass' => AreaEffective::className(), 'targetAttribute' => ['areaEffectiveID' => 'id']],
            [['itemID'], 'exist', 'skipOnError' => true, 'targetClass' => AreaItem::className(), 'targetAttribute' => ['itemID' => 'id']],
            [['termID'], 'exist', 'skipOnError' => true, 'targetClass' => AreaTerm::className(), 'targetAttribute' => ['termID' => 'id']],
            [['zonaID'], 'exist', 'skipOnError' => true, 'targetClass' => AreaZone::className(), 'targetAttribute' => ['zonaID' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'areaEffectiveID' => 'Area Effective ID',
            'itemID' => 'Item ID',
            'termID' => 'Term ID',
            'effectiveWorkTime' => 'Effective Work Time',
            'effectiveWorkClock' => 'Effective Work Clock',
            'useTaxi' => 'Use Taxi',
            'zonaID' => 'Zona ID',
            'overnightStay' => 'Overnight Stay',
            'claimQty' => 'Claim Qty',
            'claimMax' => 'Claim Max',
            'allowanceAmount' => 'Allowance Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreaEffective()
    {
        return $this->hasOne(AreaEffective::className(), ['id' => 'areaEffectiveID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(AreaItem::className(), ['id' => 'itemID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerm()
    {
        return $this->hasOne(AreaTerm::className(), ['id' => 'termID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZona()
    {
        return $this->hasOne(AreaZone::className(), ['id' => 'zonaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsAreaEffectivePositions()
    {
        return $this->hasMany(EffectivePosition::className(), ['areaEffectiveDetID' => 'id']);
    }
}
