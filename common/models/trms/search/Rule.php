<?php

namespace common\models\trms\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\trms\Rule as RuleModel;

/**
 * Rule represents the model behind the search form of `common\models\trms\Rule`.
 */
class Rule extends RuleModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'ruleType', 'claimMeal', 'useTaxi', 'claimTransportation', 'claimOutOffice', 'day', 'area', 'workHour', 'totalOvertimeHour', 'itemOvertime', 'isMealProvided', 'isTransportationProvided', 'isAccomodationProvided', 'claimTransportationAmount', 'withOvertimeTransport', 'applicablePosition', 'source', 'allowance'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RuleModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'ruleType', $this->ruleType])
            ->andFilterWhere(['like', 'claimMeal', $this->claimMeal])
            ->andFilterWhere(['like', 'useTaxi', $this->useTaxi])
            ->andFilterWhere(['like', 'claimTransportation', $this->claimTransportation])
            ->andFilterWhere(['like', 'claimOutOffice', $this->claimOutOffice])
            ->andFilterWhere(['like', 'day', $this->day])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'workHour', $this->workHour])
            ->andFilterWhere(['like', 'totalOvertimeHour', $this->totalOvertimeHour])
            ->andFilterWhere(['like', 'itemOvertime', $this->itemOvertime])
            ->andFilterWhere(['like', 'isMealProvided', $this->isMealProvided])
            ->andFilterWhere(['like', 'isTransportationProvided', $this->isTransportationProvided])
            ->andFilterWhere(['like', 'isAccomodationProvided', $this->isAccomodationProvided])
            ->andFilterWhere(['like', 'claimTransportationAmount', $this->claimTransportationAmount])
            ->andFilterWhere(['like', 'withOvertimeTransport', $this->withOvertimeTransport])
            ->andFilterWhere(['like', 'applicablePosition', $this->applicablePosition])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'allowance', $this->allowance]);

        return $dataProvider;
    }
}
