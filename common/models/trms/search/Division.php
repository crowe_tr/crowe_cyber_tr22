<?php

namespace common\models\trms\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\trms\Division as DivisionModel;

/**
 * Division represents the model behind the search form of `common\models\trms\Division`.
 */
class Division extends DivisionModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['divCode', 'divName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DivisionModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'divCode', $this->divCode])
            ->andFilterWhere(['like', 'divName', $this->divName]);

        return $dataProvider;
    }
}
