<?php

namespace common\models\trms\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\trms\RuleField as RuleFieldModel;

/**
 * RuleField represents the model behind the search form of `common\models\trms\RuleField`.
 */
class RuleField extends RuleFieldModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['sourceRule', 'attribute', 'label', 'filedType', 'range', 'widget', 'position'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RuleFieldModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'sourceRule', $this->sourceRule])
            ->andFilterWhere(['like', 'attribute', $this->attribute])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'filedType', $this->filedType])
            ->andFilterWhere(['like', 'range', $this->range])
            ->andFilterWhere(['like', 'widget', $this->widget])
            ->andFilterWhere(['like', 'position', $this->position]);

        return $dataProvider;
    }
}
