<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsDivision".
 *
 * @property int $id
 * @property string $divCode
 * @property string $divName
 */
class Division extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cmDivision';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['divCode'], 'string', 'max' => 4],
            [['divName'], 'string', 'max' => 50],
            [['divCode'], 'unique'],
            [['divName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'divCode' => 'Div Code',
            'divName' => 'Div Name',
        ];
    }
}
