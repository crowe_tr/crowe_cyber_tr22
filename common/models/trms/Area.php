<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsArea".
 *
 * @property int $id
 * @property string $areaName
 * @property int $suspended
 *
 * @property TrMsAreaItem[] $trMsAreaItems
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsArea';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['areaName'], 'required'],
            [['suspended'], 'integer'],
            [['areaName'], 'string', 'max' => 30],
            [['areaName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'areaName' => 'Area Name',
            'suspended' => 'Suspended',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsAreaItems()
    {
        return $this->hasMany(TrMsAreaItem::className(), ['areaID' => 'id']);
    }
}
