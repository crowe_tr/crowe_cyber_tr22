<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsAreaTerm".
 *
 * @property int $id
 * @property string $areaTermName
 * @property int $suspended
 *
 * @property TrMsAreaEffectiveDet[] $trMsAreaEffectiveDets
 */
class AreaTerm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsAreaTerm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['areaTermName'], 'required'],
            [['suspended'], 'integer'],
            [['areaTermName'], 'string', 'max' => 30],
            [['areaTermName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'areaTermName' => 'Area Term Name',
            'suspended' => 'Suspended',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsAreaEffectiveDets()
    {
        return $this->hasMany(TrMsAreaEffectiveDet::className(), ['termID' => 'id']);
    }
}
