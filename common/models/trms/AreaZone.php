<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsAreaZone".
 *
 * @property int $id
 * @property string $areaZoneCode
 * @property string $areaZoneDescription
 * @property int $suspended
 *
 * @property TrMsAreaEffectiveDet[] $trMsAreaEffectiveDets
 */
class AreaZone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsAreaZone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['areaZoneCode', 'areaZoneDescription'], 'required'],
            [['suspended'], 'integer'],
            [['areaZoneCode'], 'string', 'max' => 10],
            [['areaZoneDescription'], 'string', 'max' => 255],
            [['areaZoneCode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'areaZoneCode' => 'Area Zone Code',
            'areaZoneDescription' => 'Area Zone Description',
            'suspended' => 'Suspended',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsAreaEffectiveDets()
    {
        return $this->hasMany(TrMsAreaEffectiveDet::className(), ['zonaID' => 'id']);
    }
}
