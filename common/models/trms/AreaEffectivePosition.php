<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsAreaEffectivePosition".
 *
 * @property int $id
 * @property int $areaEffectiveDetID
 * @property int $seq
 * @property int $empPositionID
 * @property string $AllowanceValue
 *
 * @property AreaEffectiveDet $areaEffectiveDet
 * @property Position $empPosition
 */
class AreaEffectivePosition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsAreaEffectivePosition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['areaEffectiveDetID', 'seq', 'empPositionID'], 'required'],
            [['areaEffectiveDetID', 'seq', 'empPositionID'], 'integer'],
            [['AllowanceValue'], 'number'],
            [['areaEffectiveDetID'], 'exist', 'skipOnError' => true, 'targetClass' => AreaEffectiveDet::className(), 'targetAttribute' => ['areaEffectiveDetID' => 'id']],
            [['empPositionID'], 'exist', 'skipOnError' => true, 'targetClass' => Position::className(), 'targetAttribute' => ['empPositionID' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'areaEffectiveDetID' => 'Area Effective Det ID',
            'seq' => 'Seq',
            'empPositionID' => 'Emp Position ID',
            'AllowanceValue' => 'Allowance Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreaEffectiveDet()
    {
        return $this->hasOne(AreaEffectiveDet::className(), ['id' => 'areaEffectiveDetID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'empPositionID']);
    }
}
