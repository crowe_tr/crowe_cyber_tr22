<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsTaskType".
 *
 * @property int $id
 * @property string $taskTypeName
 * @property int $suspended
 *
 * @property TrMsTask[] $trMsTasks
 */
class TaskType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsTaskType';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taskTypeName'], 'required'],
            [['suspended'], 'integer'],
            [['taskTypeName'], 'string', 'max' => 30],
            [['taskTypeName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'taskTypeName' => 'Task Type Name',
            'suspended' => 'Suspended',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsTasks()
    {
        return $this->hasMany(TrMsTask::className(), ['taskType' => 'id']);
    }
}
