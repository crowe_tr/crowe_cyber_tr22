<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsHolidayType".
 *
 * @property int $id
 * @property string $holidayTypeName
 * @property int $suspended
 *
 * @property TrMsHolidayTimeOff[] $trMsHolidayTimeOffs
 */
class HolidayType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsHolidayType';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['holidayTypeName'], 'required'],
            [['suspended'], 'integer'],
            [['holidayTypeName'], 'string', 'max' => 20],
            [['holidayTypeName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'holidayTypeName' => 'Holiday Type Name',
            'suspended' => 'Suspended',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsHolidayTimeOffs()
    {
        return $this->hasMany(TrMsHolidayTimeOff::className(), ['type' => 'id']);
    }
}
