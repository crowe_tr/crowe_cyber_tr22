<?php

namespace common\models\trms;

use Yii;

class Entity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cmEntity';
    }

    public function rules()
    {
        return [
            [['entityCode'], 'string', 'max' => 6],
            [['entityName'], 'string', 'max' => 120],
            [['entityCode', 'entityName'], 'unique'],
            [['entityCode', 'entityName'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entityCode' => 'Entity Code',
            'name' => 'Name',
            'address' => 'Address',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'npwp' => 'Npwp',
            'logo' => 'Logo',
            'hrd' => 'Hrd',
            'managerInCharge' => 'Manager In Charge',
            'finance' => 'Finance',
            'suspended' => 'Suspended',
        ];
    }
}
