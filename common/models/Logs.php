<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "logs".
 *
 * @property int $id
 * @property string $ip
 * @property string $username
 * @property string $action
 * @property string $timestamp
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'ip', 'username', 'action'], 'safe'],
            [['ip'], 'string', 'max' => 16],
            [['username'], 'string', 'max' => 64],
            [['action'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Client IP Address',
            'username' => 'User Name',
            'action' => 'Action',
            'created_at' => 'Created At',
        ];
    }
}
