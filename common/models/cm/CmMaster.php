<?php

namespace common\models\cm;

class CmMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cmMaster';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Code', 'Value'], 'required'],
            [['Code'], 'string', 'max' => 12],
            [['Value'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'Value' => 'Value',
        ];
    }

    public static function getValue($parent, $code = '')
    {
        if ($code == '') {
            $data = CmMasterDetail::find()->where(['ParentCode' => $parent])->all();
            $return = $data;
        } else {
            $data = CmMasterDetail::find()->where(['ParentCode' => $parent, 'Code' => $code])->one();
            $return = $data->Value;
        }

        return $return;
    }

    public static function getOpt($parent)
    {
        $array = [];
        $datas = Self::getValue($parent);
        foreach ($datas as $data) {
            $array[$data->Code] = $data->Value;
        }

        return $array;
    }

    public static function getCmMasterDetails()
    {
        return $this->hasMany(CmMasterDetail::className(), ['ParentCode' => 'Code']);
    }
}
