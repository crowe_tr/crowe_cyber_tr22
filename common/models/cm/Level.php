<?php

namespace common\models\cm;
use common\models\st\RuleTaskAE;
use Yii;

/**
 * This is the model class for table "cmLevel".
 *
 * @property int $id
 * @property string $levelCode
 * @property string $levelName
 * @property string $notes
 *
 * @property HrEmployee[] $hrEmployees
 * @property StBillingRateDetail[] $stBillingRateDetails
 * @property StBillingRate[] $billingRates
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $taskae;
    public static function tableName()
    {
        return 'cm_level';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level_code', 'level_name'], 'required'],
            [['level_code'], 'string', 'max' => 3],
            [['level_name', 'level_notes'], 'string', 'max' => 45],
            [['level_name'], 'unique'],
            [['auto_approve', 'is_not_boss', 'is_not_manager'], 'integer'],
            [['taskae', 'id'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'level_code' => Yii::t('app', 'Code'),
            'level_name' => Yii::t('app', 'Name'),
            'level_notes' => Yii::t('app', 'Notes'),
            'auto_approve' => Yii::t('app', 'Auto Approve'),
            'is_not_boss' => Yii::t('app', 'Not Boss'),
            'is_not_manager' => Yii::t('app', 'Not Manager'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHrEmployees()
    {
        return $this->hasMany(Employee::className(), ['level_id' => 'id']);
    }

    public function getTaskae()
    {
        return $this->hasMany(RuleTaskAE::className(), ['level_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStBillingRateDetails()
    {
        return $this->hasMany(BillingRateDetail::className(), ['level_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingRates()
    {
        return $this->hasMany(BillingRate::className(), ['id' => 'billingRateID'])->viaTable('stBillingRateDetail', ['level_id' => 'id']);
    }
}
