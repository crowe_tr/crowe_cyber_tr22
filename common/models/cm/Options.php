<?php

namespace common\models\cm;

use Yii;

/**
 * This is the model class for table "cmOptions".
 *
 * @property string $optionsName
 * @property string $optionsValue
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cm_options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['options_value', 'effective_date'], 'required'],
            [['options_name'], 'required', 'on'=>'new'],

            [['options_name'], 'string', 'max' => 60],
            [['options_value'], 'string', 'max' => 45],
            [['options_name', 'effective_date'], 'unique', 'targetAttribute' => ['options_name', 'effective_date'], 'on'=>'new']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'options_name' => 'Options Name',
            'options_value' => 'Options Value',
        ];
    }
}
