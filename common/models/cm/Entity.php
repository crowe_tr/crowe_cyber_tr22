<?php

namespace common\models\cm;

use Yii;

class Entity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cm_entity';
    }

    public function rules()
    {
        return [
            [['entity_code'], 'string', 'max' => 6],
            [['entity_name'], 'string', 'max' => 120],
            [['entity_code', 'entity_name'], 'unique'],
            [['entity_code', 'entity_name'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_code' => 'Entity Code',
            'entity_name' => 'Entity Name',
        ];
    }
}
