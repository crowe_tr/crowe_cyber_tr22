<?php

namespace common\models\cm;

use Yii;

/**
 * This is the model class for table "cmDept".
 *
 * @property int $id
 * @property string $deptCode
 * @property string $deptName
 * @property int $divID
 * @property int $flag
 *
 * @property HrEmployee[] $hrEmployees
 */
class Dept extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cm_dept';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dept_code', 'div_id', 'dept_name'], 'required'],
            [['div_id', 'flag'], 'integer'],
            [['dept_code'], 'string', 'max' => 6],
            [['dept_name'], 'string', 'max' => 128],
            [['dept_code'], 'unique'],
            [['div_id', 'dept_name'], 'unique', 'targetAttribute' => ['div_id', 'dept_name']],
            [['div_id'], 'exist', 'skipOnError' => true, 'targetClass' => Division::className(), 'targetAttribute' => ['div_id' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dept_code' => Yii::t('app', 'Code'),
            'dept_name' => Yii::t('app', 'DEPARTMENT'),
            'div_id' => Yii::t('app', 'Division'),
            'flag' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHrEmployees()
    {
        return $this->hasMany(HrEmployee::className(), ['dept_id' => 'id']);
    }
    public function getDiv()
    {
        return $this->hasOne(Division::className(), ['id' => 'div_id']);
    }
}
