<?php

namespace common\models\cm\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\cm\Entity as EntityModel;

/**
 * Entity represents the model behind the search form of `common\models\trms\Entity`.
 */
class Entity extends EntityModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['entity_code', 'entity_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EntityModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'entity_code', $this->entity_code])
              ->andFilterWhere(['like', 'entity_name', $this->entity_name]);

        return $dataProvider;
    }
}
