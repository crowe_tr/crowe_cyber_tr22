<?php

namespace common\models\cm;

use Yii;

/**
 * This is the model class for table "cmTaxi".
 *
 * @property int $id
 * @property string $name
 * @property int $overtime
 */
class Taxi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cmTaxi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['overtime'], 'integer'],
            [['name'], 'string', 'max' => 30],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'overtime' => Yii::t('app', 'Overtime'),
        ];
    }
}
