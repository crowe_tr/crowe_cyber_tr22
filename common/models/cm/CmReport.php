<?php

namespace common\models\cm;

use Yii;

class CmReport extends yii\base\Model
{
    public $Date;
    public $BranchId;
    public $ReportFormat;
    public $Month;
    public $Year;
    public $Custom;

    public function rules()
    {
        return [
            ['Date', 'required'],
            ['BranchId', 'required'],
            ['ReportFormat', 'required'],
            [['Month', 'Year'], 'string'],
            [['Month', 'Year'], 'required', 'on'=>'byMonth'],
            [['Custom'], 'safe'],
            [['Custom'], 'required', 'on'=>'withCustom'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'Date' => 'Date',
            'BranchId' => 'Branch',
            'ReportFormat' => 'Format',
        ];
    }
}
