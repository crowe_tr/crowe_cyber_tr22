<?php

namespace common\models\tr;
use common\models\st\Task;
use common\models\st\TaskType;

use Yii;

/**
 * This is the model class for table "trTimeReportMeals".
 *
 * @property int $TimeReportID
 * @property int $Seq
 * @property int $TaskTypeID
 * @property int $JobId
 * @property int $TaskID
 * @property int $Status
 *
 * @property Job $job
 * @property Task $task
 * @property TaskType $taskType
 * @property TimeReport $timeReport
 */
class TimeReportMeals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trTimeReportMeals';
    }

    public $EmployeeId;
    public $Date;
    public $isStayed; 

    public $Meal;
    public $MealProvided;
    public $MealAllocation;
    public $MealClaimQty;
    public $MealClaimQtyStayed;
    public $MealMinOvertime;

    public $lookup;

    public function rules()
    {
        return [
            
            [['EmployeeId', 'Date', 'isStayed', 'lookup', 'Description'], 'safe'],
            [['TimeReportID', 'Seq'], 'safe'],
            [['TimeReportID', 'TrDetID'], 'required'],
            //[['TimeReportID', 'TaskTypeID', 'TaskID'], 'integer'],

            [['TimeReportID', 'TrDetID'], 'unique', 'targetAttribute' => ['TimeReportID', 'TrDetID']],


            [['JobId'], 'exist', 'skipOnError' => true, 'targetClass' => Job::className(), 'targetAttribute' => ['JobId' => 'JobID']],
            [['TaskID'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['TaskID' => 'id']],
            [['TaskTypeID'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['TaskTypeID' => 'id']],
            [['TimeReportID'], 'exist', 'skipOnError' => true, 'targetClass' => TimeReport::className(), 'targetAttribute' => ['TimeReportID' => 'ID']],
            [['Approval1','Approval2'], 'safe'],

            //[['Meal', 'MealProvided', 'MealAllocation', 'MealClaimQty', 'MealClaimQtyStayed','MealMinOvertime'], 'safe'],
            //[['Meal'], 'validateMeals', 'enableClientValidation' => false],

        ];
    }
    public function validateMeals($attribute_name, $params){
        $valid = true;
        if($this->Meal > 0){
            if($this->MealProvided == true){
                $ClaimQty = ($this->isStayed == 0) ? $this->MealClaimQty : $this->MealClaimQtyStayed;
                if ($ClaimQty >= $this->MealAllocation){
                    if($this->MealMinOvertime <= $this->OvertimeAllocation){
                        $this->addError($attribute_name, "Total Overtime tidak memenuhi syarat untuk claim makan, minimal Overtime : ".$this->TaxiMinOvertime);
                        $valid = false;
                    }
                }else{
                    $this->addError($attribute_name, "Batas claim meal sudah mencapai batas, hak claim meal : ".$ClaimQty);
                    $valid = false;
                }
            }else{
                $this->addError($attribute_name, "Anda tidak bisa claim meal");
                $valid = false;
            }
        }
        return $valid;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TimeReportID' => Yii::t('app', 'Time Report'),
            'Seq' => Yii::t('app', 'Seq'),
            'TaskTypeID' => Yii::t('app', 'Class/Task Type'),
            'JobId' => Yii::t('app', 'Job'),
            'TaskID' => Yii::t('app', 'Task'),
            'TrDetID' => 'Task Description',
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['JobId' => 'JobId']);
    }
    public function getTasktype()
    {
        return $this->hasOne(TaskType::className(), ['Id' => 'TaskTypeID']);
    }
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['Id' => 'TaskID']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeReport()
    {
        return $this->hasOne(TimeReport::className(), ['ID' => 'TimeReportID']);
    }
}
