<?php

namespace common\models\tr;

use common\models\cl\Client;
use common\models\hr\Employee;

use Yii;

/**
 * This is the model class for table "trJobTemp".
 *
 * @property int $JobID
 * @property string $JobCode
 * @property string $Description
 * @property int $ClientID
 * @property string $JobArea
 * @property string $JobCreated
 * @property string $StartDate
 * @property string $EndDate
 * @property string $Partner
 * @property string $Manager
 * @property string $Supervisor
 * @property int $IncludeOPE
 * @property int $IsMeal
 * @property int $IsOutOfOffice
 * @property int $IsTaxi
 * @property string $MealAllowance
 * @property string $OutOfOfficeAllowance
 * @property string $TaxiAllowance
 * @property string $AdministrativeCharge
 * @property string $OtherExpenseAllowance
 * @property string $Fee
 * @property string $TimeCharges
 * @property string $Percentage
 * @property int $Status 0:Draff; 1:Submit; 2:Reject; 3:Approve; 4:Finish; 5:Close
 * @property int $Flag
 * @property string $CreatedBy
 * @property string $CreatedAt
 * @property string $UpdateBy
 * @property string $UpdateAt
 *
 * @property ClClient $client
 */
class JobTemp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

       public $Total;
    public static function tableName()
    {
        return 'trJobTemp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JobID', 'Description', 'ClientID', 'StartDate', 'EndDate', 'JobArea'], 'required'],
            [['JobID', 'ClientID', 'IncludeOPE', 'IsMeal', 'IsOutOfOffice', 'IsTaxi', 'Status', 'Flag'], 'integer'],
            [['JobCreated', 'StartDate', 'EndDate', 'CreatedAt', 'UpdateAt', 'Total','MealAllowance', 'OutOfOfficeAllowance', 'TaxiAllowance', 'AdministrativeCharge', 'OtherExpenseAllowance', 'Fee', 'TimeCharges', 'JobArea'], 'safe'],
            // [[], 'number'],
            [['JobCode', 'Partner', 'Manager', 'Supervisor'], 'string', 'max' => 16],
            [['Description'], 'string', 'max' => 255],
            [['Percentage', 'CreatedBy', 'UpdateBy'], 'string', 'max' => 32],
            [['JobID'], 'unique'],
            [['ClientID'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['ClientID' => 'Id']],
            [['Partner'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['Partner' => 'Id']],
            [['Manager'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['Manager' => 'Id']],
            [['Supervisor'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['Supervisor' => 'Id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'JobID' => 'Job ID',
            'JobCode' => 'Job Code',
            'Description' => 'Description',
            'ClientID' => 'Client ID',
            'JobArea' => 'Job Area',
            'JobCreated' => 'Job Created',
            'StartDate' => 'Start Date',
            'EndDate' => 'End Date',
            'Partner' => 'Partner',
            'Manager' => 'Manager',
            'Supervisor' => 'Supervisor',
            'IncludeOPE' => 'Include Ope',
            'IsMeal' => 'Is Meal',
            'IsOutOfOffice' => 'Is Out Of Office',
            'IsTaxi' => 'Is Taxi',
            'MealAllowance' => 'Meal Allowance',
            'OutOfOfficeAllowance' => 'Out Of Office Allowance',
            'TaxiAllowance' => 'Taxi Allowance',
            'AdministrativeCharge' => 'Administrative Charge',
            'OtherExpenseAllowance' => 'Other Expense Allowance',
            'Fee' => 'Fee',
            'TimeCharges' => 'Time Charges',
            'Percentage' => 'Percentage',
            'Status' => 'Status',
            'Flag' => 'Flag',
            'CreatedBy' => 'Created By',
            'CreatedAt' => 'Created At',
            'UpdateBy' => 'Update By',
            'UpdateAt' => 'Update At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['Id' => 'ClientID']);
    }
    public function getPartner()
    {
        return $this->hasOne(Employee::className(), ['Id' => 'Partner']);
    }
    public function getManager()
    {
        return $this->hasOne(Employee::className(), ['Id' => 'Manager']);
    }
    public function getSupervisor()
    {
        return $this->hasOne(Employee::className(), ['Id' => 'Supervisor']);
    }
}
