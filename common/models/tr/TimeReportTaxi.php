<?php

namespace common\models\tr;

use Yii;
use common\models\cm\Taxi;
use common\models\st\Task;
use common\models\st\TaskType;

/**
 * This is the model class for table "trTimeReportTaxi".
 *
 * @property int $TimeReportID
 * @property int $Seq
 * @property int $cmTaxiID
 * @property int $TaskTypeID
 * @property int $JobId
 * @property int $TaskID
 * @property string $Start
 * @property string $Finish
 * @property string $VoucherNo
 * @property string $Destination
 * @property string $Amount
 * @property string $Description
 *
 * @property Taxi $cmTaxi
 * @property Job $job
 * @property Task $task
 * @property TaskType $taskType
 * @property TimeReport $timeReport
 */
class TimeReportTaxi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trTimeReportTaxi';
    }
    public $EmployeeId;
    public $Date;
    public $isStayed; 


    public $Taxi;
    public $TaxiProvided;
    public $TaxiAllocation;
    public $TaxiClaimQty;
    public $TaxiClaimQtyStayed;
    public $TaxiMinOvertime;
    public $TaxiMinClock;
    public $TaxiStart;
    public $TaxiFinish;

    
    public $lookup;

    public function rules()
    {
        return [
            [['EmployeeId', 'Date', 'isStayed', 'TrDetID'], 'safe'],

            [['Seq'], 'safe'],
            [['TimeReportID', 'cmTaxiID', 'Start', 'Finish', 'VoucherNo', 'Destination', 'Amount'], 'required'],
            [['TimeReportID', 'Seq', 'cmTaxiID'], 'integer'],

            //[['Start', 'Finish'], 'date', 'dateFormat' => 'H:i:s'],
            [['Description'], 'string'],
            [['VoucherNo'], 'string', 'max' => 30],
            [['Destination'], 'string', 'max' => 100],
            [['TimeReportID', 'Seq'], 'unique', 'targetAttribute' => ['TimeReportID', 'Seq']],
            [['cmTaxiID'], 'exist', 'skipOnError' => true, 'targetClass' => Taxi::className(), 'targetAttribute' => ['cmTaxiID' => 'id']],
            [['TimeReportID'], 'exist', 'skipOnError' => true, 'targetClass' => TimeReport::className(), 'targetAttribute' => ['TimeReportID' => 'ID']],
            
            //['Start', 'compare', 'compareAttribute' => 'Finish', 'operator' => '<'],
            [['TrDetID'], 'required'],
            /*
            [['TrDetID'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->cmTaxiID == 5){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            */

            [['Description'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->cmTaxiID == 6){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],

            [['JobId'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->TaskTypeID == 1){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            [['Approval1','Approval2'], 'safe'],

            [['Taxi', 'TaxiProvided', 'TaxiAllocation','TaxiClaimQty', 'TaxiClaimQtyStayed', 'TaxiMinOvertime', 'TaxiMinClock', 'TaxiStart', 'TaxiEnd'], 'safe'],
            //[['Taxi'], 'validateTaxi', 'enableClientValidation' => false],

        ];
    }
    public function validateTaxi($attribute_name, $params){
        $valid = true;
        if($this->Taxi > 0){
            if($this->TaxiProvided == true){
                $ClaimQty = ($this->isStayed == 0) ? $this->TaxiClaimQty : $this->TaxiClaimQtyStayed;
                if ($ClaimQty >= $this->TaxiAllocation){
                    if($this->TaxiMinOvertime <= $this->OvertimeAllocation) {
                        if($this->TaxiMinClock <= $this->TaxiStart){
                            $this->addError($attribute_name, "Total Minimal Jam Taxi tidak memenuhi syarat untuk claim taxi, minimal Time :".$this->TaxiMinClock);
                            $valid = false;
                        }
                    } else {
                        $this->addError($attribute_name, "Total Overtime tidak memenuhi syarat untuk claim taxi, minimal Overtime : ".$this->TaxiMinOvertime);
                        $valid = false;
                    }
                }else{
                    $this->addError($attribute_name, "Batas claim Taxi sudah mencapai batas, hak claim taxi : ".$ClaimQty);
                    $valid = false;
                }
            }else{
                $this->addError($attribute_name, "Anda tidak bisa claim Taxi");
                $valid = false;
            }
        }
        return $valid;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TimeReportID' => Yii::t('app', 'Time Report ID'),
            'Seq' => Yii::t('app', 'Seq'),
            'cmTaxiID' => Yii::t('app', 'Taxi'),
            'TaskTypeID' => Yii::t('app', 'Class / Task Type'),
            'JobId' => Yii::t('app', 'Job'),
            'TaskID' => Yii::t('app', 'Task'),
            'Start' => Yii::t('app', 'Start'),
            'Finish' => Yii::t('app', 'Finish'),
            'VoucherNo' => Yii::t('app', 'Voucher No'),
            'Destination' => Yii::t('app', 'Destination'),
            'Amount' => Yii::t('app', 'Amount'),
            'Description' => Yii::t('app', 'Description'),
            'TrDetID' => 'Task Description',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxi()
    {
        return $this->hasOne(Taxi::className(), ['id' => 'cmTaxiID']);
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['JobId' => 'JobId']);
    }
    public function getTasktype()
    {
        return $this->hasOne(TaskType::className(), ['Id' => 'TaskTypeID']);
    }
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['Id' => 'TaskID']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeReport()
    {
        return $this->hasOne(TimeReport::className(), ['ID' => 'TimeReportID']);
    }
}
