<?php

namespace common\models\tr;

use Yii;

/**
 * This is the model class for table "trPreviousChangesJob".
 *
 * @property int $Id
 * @property int $JobID
 * @property string $PreviousChanges
 * @property string $CreatedBy
 * @property string $UpdatedBy
 * @property string $CreatedTime
 * @property string $UpdatedTime
 */
class TrPreviousChangesJob extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trPreviousChangesJob';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JobID'], 'integer'],
            [['PreviousChanges'], 'string'],
            [['CreatedTime', 'UpdatedTime'], 'safe'],
            [['CreatedBy', 'UpdatedBy'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'JobID' => 'Job ID',
            'PreviousChanges' => 'Previous Changes',
            'CreatedBy' => 'Created By',
            'UpdatedBy' => 'Updated By',
            'CreatedTime' => 'Created Time',
            'UpdatedTime' => 'Updated Time',
        ];
    }
}
