<?php

namespace common\models\tr;

use Yii;
use common\models\hr\Employee;

/**
 * This is the model class for table "JobComment".
 *
 * @property int $CommentId
 * @property int $JobID
 * @property string $Comments
 * @property string $CreatedBy
 * @property string $CreatedDate
 *
 * @property Job $job
 */
class JobComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tr_job_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id'], 'integer'],
            [['comments'], 'string'],
            // [['CreatedDate'], 'safe'],
            [['created_by'], 'string', 'max' => 16],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Job::className(), 'targetAttribute' => ['id' => 'job_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Comment ID'),
            'job_id' => Yii::t('app', 'Job ID'),
            'comments' => Yii::t('app', 'Comments'),
            'created_by' => Yii::t('app', 'Created By'),
            // 'CreatedDate' => Yii::t('app', 'Created Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }

    public function getEmployee()
    {
      return $this->hasOne(Employee::className(), ['user_id' => 'created_by']);
    }
}
