<?php

namespace common\models\tr;

use Yii;

/**
 * This is the model class for table "vJob".
 *
 * @property int $JobID
 * @property string $JobCode
 * @property string $Description
 * @property int $ClientID
 * @property string $JobArea
 * @property string $JobCreated
 * @property string $StartDate
 * @property string $EndDate
 * @property string $Partner
 * @property string $Manager
 * @property string $Supervisor
 * @property int $IncludeOPE
 * @property int $IsMeal
 * @property int $IsOutOfOffice
 * @property int $IsTaxi
 * @property string $MealAllowance
 * @property string $MealAllowanceAct
 * @property string $OutOfOfficeAllowance
 * @property string $OutOfOfficeAllowanceAct
 * @property string $TaxiAllowance
 * @property string $TaxiAllowanceAct
 * @property string $AdministrativeCharge
 * @property string $AdministrativeChargeAct
 * @property string $OtherExpenseAllowance
 * @property string $OtherExpenseAllowanceAct
 * @property string $Fee
 * @property string $FeeAct
 * @property string $TimeCharges
 * @property string $TimeChargesAct
 * @property string $Percentage
 * @property string $PercentageAct
 * @property int $Status 0:Draff; 1:Submit; 2:Approve; 3:Reject; 4:Close; 5:Finish
 * @property int $Flag 0:Nothing; 1:Draff Revise; 2:Submit Revise; 3:Approve Revise; 4:Reject Revise
 * @property string $CreatedBy
 * @property string $CreatedAt
 * @property string $UpdateBy
 * @property string $UpdateAt
 * @property string $Client
 * @property string $Entity
 * @property string $Divison
 * @property string $PartnerName
 * @property string $ManagerName
 * @property string $SupervisorName
 */
class VJob extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'job_list_only';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'client_id', 'include_ope', 'is_meal', 'is_ope', 'is_taxi', 'job_status', 'flag'], 'integer'],
            [['description', 'client_id', 'start_date', 'end_date'], 'required'],
            [['JobCreated', 'StartDate', 'EndDate', 'CreatedAt', 'UpdateAt'], 'safe'],
            // [['MealAllowance', 'MealAllowanceAct', 'OutOfOfficeAllowance', 'OutOfOfficeAllowanceAct',
            //   'TaxiAllowance', 'TaxiAllowanceAct', 'AdministrativeCharge', 'AdministrativeChargeAct',
            //   'OtherExpenseAllowance', 'OtherExpenseAllowanceAct', 'Fee', 'FeeAct', 'TimeCharges', 'TimeChargesAct'], 'number'],
            [['job_code'], 'string', 'max' => 16],
            [['description', 'job_area'], 'string', 'max' => 255],
            [['partner_id', 'manager_id', 'supervisor_id', 'Percentage', 'percentage_act',
              'created_by', 'update_by'], 'string', 'max' => 32],
            [['client_name'], 'string', 'max' => 100],
            [['entity_name'], 'string', 'max' => 120],
            [['div_name'], 'string', 'max' => 50],
            [['pc_full_name', 'mc_full_name', 'sc_full_name'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'job_code' => 'Job Code',
            'description' => 'Description',
            // 'ClientID' => 'Client ID',
            // 'JobArea' => 'Job Area',
            // 'JobCreated' => 'Job Created',
            // 'StartDate' => 'Start Date',
            // 'EndDate' => 'End Date',
            // 'Partner' => 'Partner',
            // 'Manager' => 'Manager',
            // 'Supervisor' => 'Supervisor',
            // 'IncludeOPE' => 'Include Ope',
            // 'IsMeal' => 'Is Meal',
            // 'IsOutOfOffice' => 'Is Out Of Office',
            // 'IsTaxi' => 'Is Taxi',
            // 'MealAllowance' => 'Meal Allowance',
            // 'MealAllowanceAct' => 'Meal Allowance Act',
            // 'OutOfOfficeAllowance' => 'Out Of Office Allowance',
            // 'OutOfOfficeAllowanceAct' => 'Out Of Office Allowance Act',
            // 'TaxiAllowance' => 'Taxi Allowance',
            // 'TaxiAllowanceAct' => 'Taxi Allowance Act',
            // 'AdministrativeCharge' => 'Administrative Charge',
            // 'AdministrativeChargeAct' => 'Administrative Charge Act',
            // 'OtherExpenseAllowance' => 'Other Expense Allowance',
            // 'OtherExpenseAllowanceAct' => 'Other Expense Allowance Act',
            // 'Fee' => 'Fee',
            // 'FeeAct' => 'Fee Act',
            // 'TimeCharges' => 'Time Charges',
            // 'TimeChargesAct' => 'Time Charges Act',
            // 'Percentage' => 'Percentage',
            // 'PercentageAct' => 'Percentage Act',
            // 'Status' => 'Status',
            // 'Flag' => 'Flag',
            'created_by' => 'Created By',
            // 'CreatedAt' => 'Created At',
            // 'UpdateBy' => 'Update By',
            // 'UpdateAt' => 'Update At',
            // 'Client' => 'Client',
            // 'Entity' => 'Entity',
            // 'Divison' => 'Divison',
            // 'PartnerName' => 'Partner Name',
            // 'ManagerName' => 'Manager Name',
            // 'SupervisorName' => 'Supervisor Name',
        ];
    }
}
