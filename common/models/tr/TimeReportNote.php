<?php

namespace common\models\tr;

use Yii;

/**
 * This is the model class for table "trTimeReportNote".
 *
 * @property int $CommentId
 * @property string $EmployeeId
 * @property string $Date
 * @property string $Notes
 * @property string $CreatedBy
 * @property string $CreatedDate
 */
use common\models\hr\Employee;

class TimeReportNote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trTimeReportNote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Date', 'CreatedDate', 'TimeReportID', 'ParentID', 'Type', 'For'], 'safe'],
            [['Notes'], 'string'],
            [['EmployeeId', 'CreatedBy'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CommentId' => Yii::t('app', 'Comment ID'),
            'EmployeeId' => Yii::t('app', 'Employee'),
            'ParentID' => Yii::t('app', 'Parent'),
            'Date' => Yii::t('app', 'Date'),
            'Notes' => Yii::t('app', 'Notes'),
            'CreatedBy' => Yii::t('app', 'Created By'),
            'CreatedDate' => Yii::t('app', 'Created Date'),
        ];
    }
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['Id' => 'EmployeeId']);
    }

}
