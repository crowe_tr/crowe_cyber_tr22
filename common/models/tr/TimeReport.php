<?php

namespace common\models\tr;

use Yii;

use common\models\hr\Employee;
use common\models\cm\Level;

class TimeReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $YearDate;
    public $Level;
    public $Date_label;

    public $EmployeeName;
    public static function tableName()
    {
        return 'trTimeReport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'YearDate', 'EmployeeName', 'isStayed'], 'safe'],

            [['EmployeeId', 'Date', 'isStayed', 'Status'], 'required'],
            [['Date'], 'safe'],
            [['isStayed', 'Status'], 'integer'],
            [['EmployeeId'], 'string', 'max' => 32],
            [['maxOverTime'], 'string', 'max' => 45],
            [['EmployeeId', 'Date'], 'unique', 'targetAttribute' => ['EmployeeId', 'Date']],
            [['EmployeeId'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['EmployeeId' => 'Id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'EmployeeId' => Yii::t('app', 'Employee'),
            'Date' => Yii::t('app', 'Date'),
            'isStayed' => Yii::t('app', 'Stayed'),
            'maxOverTime' => Yii::t('app', 'Max Over Time'),
            'maxMeal' => Yii::t('app', 'Max Meal'),
            'maxOPE' => Yii::t('app', 'Max Ope'),
            'Status' => Yii::t('app', 'Status'),
            'Date_label'=>'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['Id' => 'EmployeeId']);
    }
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'levelID'])->via('employee');
    }

}
