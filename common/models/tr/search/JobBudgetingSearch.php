<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\JobBudgeting;

/**
 * JobBudgetingSearch represents the model behind the search form of `common\models\tr\JobBudgeting`.
 */
class JobBudgetingSearch extends JobBudgeting
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'planning', 'field_work', 'reporting', 'wrap_up', 'over_time', 'total_wh'], 'integer'],
            [['employee_id', 'created_by', 'created_at', 'update_by', 'update_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JobBudgeting::find();
        $query->joinWith('employee');
        $query->orderBy(['hr_employee.full_name' => SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_id' => $this->job_id,
            'planning' => $this->planning,
            'field_work' => $this->field_work,
            'reporting' => $this->reporting,
            'wrap_up' => $this->wrap_up,
            'over_time' => $this->over_time,
            'total_wh' => $this->total_wh,
            'created_at' => $this->created_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'employee_id', $this->employee_id])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
