<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\Job;
use common\models\tr\VJob;

/**
 * JobSearch represents the model behind the search form of `common\models\tr\Job`.
 */
class JobviewSearch extends VJob
{
    /**
     * {@inheritdoc}
     */

     public $Nondraft;
    public function rules()
    {
        return [
            [['job_id', 'include_ope', 'is_meal', 'is_taxi', 'is_ope', 'job_fee', 'job_status',
              'ope_allowance', 'taxi_allowance','other_expense_allowance'], 'integer'],
            [['job_code', 'description', 'client_id', 'job_area',
              'job_created', 'start_date', 'end_date',
              'partner_id', 'manager_id', 'supervisor_id','Nondraft' ,
              'created_by', 'created_at', 'update_by', 'update_at',
              'client_name','pc_full_name', 'mc_full_name', 'sc_full_name'], 'safe'],
            [['entity_name', 'division_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

     public function search($params)
     {
        $query = VJob::find();

        $dataProvider = new ActiveDataProvider([
           'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
          return $dataProvider;
        }

        if ($this->Nondraft == 1) {
           $query->where(['<>','job_status' , 0]);
        }

        $query->orderBy(['start_date' => SORT_DESC]);

        switch ($this->job_status) {
          case 21:
            $query->andWhere("start_date < NOW() AND end_date > NOW() AND job_status = '2'");
            break;
          case 22:
            $query->andWhere("end_date < NOW() AND job_status = '2'");
            break;
          case 2:
            $query->andWhere("start_date > NOW() AND job_status = '2'");
            break;
          case 9:
            $query->andWhere("");
            break;
          default:
              $query->andWhere("job_status = '$this->job_status'");
            break;
        }

        // $query->andFilterWhere([
        //     'JobID' => $this->JobID,
        //     'JobCreated' => $this->JobCreated,
        //     // 'StartDate' => $this->StartDate,
        //     'EndDate' => $this->EndDate,
        //     'IncludeOPE' => $this->IncludeOPE,
        //     'IsMeal' => $this->IsMeal,
        //     'IsTaxi' => $this->IsTaxi,
        //     'IsOutOfOffice' => $this->IsOutOfOffice,
        //     'OutOfOfficeAllowance' => $this->OutOfOfficeAllowance,
        //     'TaxiAllowance' => $this->TaxiAllowance,
        //     'OtherExpenseAllowance' => $this->OtherExpenseAllowance,
        //     'Fee' => $this->Fee,
        //     // 'Status' => $this->Status,
        //     'CreatedAt' => $this->CreatedAt,
        //     'UpdateAt' => $this->UpdateAt,
        // ]);

        if ($this->manager_id != null) {
            $query->andWhere(['or',
            ['manager_id'=>$this->manager_id],
            ['created_by'=>$this->created_by]
          ]);
        }

        $query->andFilterWhere(['like', 'job_code', $this->job_code])
        //     ->orFilterWhere(['like', 'Description', $this->JobCode])
        //     ->orFilterWhere(['like', 'Client', $this->JobCode])
        //     ->orFilterWhere(['like', 'PartnerName', $this->JobCode])
        //     ->orFilterWhere(['like', 'ManagerName', $this->JobCode])
        //     ->orFilterWhere(['like', 'SupervisorName', $this->JobCode])
        //     ->orFilterWhere(['like', 'Entity', $this->JobCode])
        //     ->orFilterWhere(['like', 'Divison', $this->JobCode])
        //     ->orFilterWhere(['like', 'JobArea', $this->JobCode])
        //     ->andFilterWhere(['like', 'Partner', $this->Partner])
        //     // ->andFilterWhere(['like', 'Manager', $this->Manager])
        //     // ->andFilterWhere(['like', 'Status', $this->Status])
        //     ->orFilterWhere(['like', 'Supervisor', $this->Supervisor])
        //     // ->andFilterWhere('or',['like', 'created_by', $this->created_by], ['like', 'Manager', $this->Manager])
            ->orFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
     }

    public function search__2($params)
    {

        $query = VJob::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if ($this->Nondraft == 1) {
          $query->where(['<>','Status' , 0]);
        }

        $query->orderBy(['StartDate' => SORT_DESC]);

        // print_r($this->Status);

        // print_r($now);
        // if ($this->StartDate > $now and  $this->Status == 2) {

        // }

        // grid filtering conditions
        // $date = Job::find()->all();
         // $command = $date->createCommand();
         // $a = new Job;
         // $a->load($date);
         // $date = [];
         // $now = date('now
         // foreach ($date as $key => $val) {
           // code...
           // $searchModel->StartDate = $val->StartDate;
           // print_r($val->JobID);
           // $now = date("Y-m-d H:i:s");
           // if ($this->Status == 2 AND   $val->StartDate  ) {
             // code...
            switch ($this->Status) {
              case 21:
                $query->andWhere("StartDate < NOW() AND EndDate > NOW() AND Status = '2'");
                break;
              case 22:
                $query->andWhere("EndDate < NOW() AND Status = '2'");
                break;
              case 2:
                $query->andWhere("StartDate > NOW() AND Status = '2'");
                break;
              case 9:
                $query->andWhere("");
                break;
              default:
                  $query->andWhere("Status = '$this->Status'");
                break;
            }

        $query->andFilterWhere([
            'JobID' => $this->JobID,
            'JobCreated' => $this->JobCreated,
            // 'StartDate' => $this->StartDate,
            'EndDate' => $this->EndDate,
            'IncludeOPE' => $this->IncludeOPE,
            'IsMeal' => $this->IsMeal,
            'IsTaxi' => $this->IsTaxi,
            'IsOutOfOffice' => $this->IsOutOfOffice,
            'OutOfOfficeAllowance' => $this->OutOfOfficeAllowance,
            'TaxiAllowance' => $this->TaxiAllowance,
            'OtherExpenseAllowance' => $this->OtherExpenseAllowance,
            'Fee' => $this->Fee,
            // 'Status' => $this->Status,
            'CreatedAt' => $this->CreatedAt,
            'UpdateAt' => $this->UpdateAt,
        ]);

        if ($this->Manager != null) {
            $query->andWhere(['or',
            ['Manager'=>$this->Manager],
            ['CreatedBy'=>$this->CreatedBy]
          ]);
        }

        $query->andFilterWhere(['like', 'JobCode', $this->JobCode])
            ->orFilterWhere(['like', 'Description', $this->JobCode])
            ->orFilterWhere(['like', 'Client', $this->JobCode])
            ->orFilterWhere(['like', 'PartnerName', $this->JobCode])
            ->orFilterWhere(['like', 'ManagerName', $this->JobCode])
            ->orFilterWhere(['like', 'SupervisorName', $this->JobCode])
            ->orFilterWhere(['like', 'Entity', $this->JobCode])
            ->orFilterWhere(['like', 'Divison', $this->JobCode])
            ->orFilterWhere(['like', 'JobArea', $this->JobCode])
            ->andFilterWhere(['like', 'Partner', $this->Partner])
            // ->andFilterWhere(['like', 'Manager', $this->Manager])
            // ->andFilterWhere(['like', 'Status', $this->Status])
            ->orFilterWhere(['like', 'Supervisor', $this->Supervisor])
            // ->andFilterWhere('or',['like', 'CreatedBy', $this->CreatedBy], ['like', 'Manager', $this->Manager])
            ->orFilterWhere(['like', 'UpdateBy', $this->UpdateBy]);

        return $dataProvider;
    }
}
