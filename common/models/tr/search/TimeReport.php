<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\TimeReport as TimeReportModel;

/**
 * TimeReport represents the model behind the search form of `common\models\tr\TimeReport`.
 */
class TimeReport extends TimeReportModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'isStayed', 'maxMeal', 'maxOPE', 'Status'], 'integer'],
            [['EmployeeId', 'Date', 'maxOverTime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeReportModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Date' => $this->Date,
            'isStayed' => $this->isStayed,
            'maxMeal' => $this->maxMeal,
            'maxOPE' => $this->maxOPE,
            'Status' => $this->Status,
        ]);

        $query->andFilterWhere(['like', 'EmployeeId', $this->EmployeeId])
            ->andFilterWhere(['like', 'maxOverTime', $this->maxOverTime]);

        return $dataProvider;
    }
}
