<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\Job;

/**
 * JobSearch represents the model behind the search form of `common\models\tr\Job`.
 */
class JobSearch extends Job
{
    /**
     * {@inheritdoc}
     */

     public $Nondraft;
    public function rules()
    {
        return [
            [['id', 'include_ope', 'is_meal', 'is_taxi', 'is_ope', 'job_fee',
              // 'Total',
              'job_status',
              'ope_allowance', 'taxi_allowance','other_expense_allowance'], 'integer'],
            [['job_code', 'description', 'client_id', 'job_area', 'job_created',
              'start_date', 'end_date', 'partner_id', 'manager_id', 'supervisor_id','Nondraft' ,
              'created_by', 'created_at', 'updated_by', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Job::find();
        $query->joinwith('client')->orderBy(['created_at' => SORT_DESC]);


        // $model = Job::find()->all();
        // // print_r($model->StartDate);
        // $query->andwhere("startdate < {$now} AND status = {$status}");


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if ($this->Nondraft == 1) {
          $query->where(['<>','job_status' , 0]);
        }
        // print_r($this->Status);

        // print_r($now);
        // if ($this->StartDate > $now and  $this->Status == 2) {

        // }

        // grid filtering conditions
        // $date = Job::find()->all();
         // $command = $date->createCommand();
         // $a = new Job;
         // $a->load($date);
         // $date = [];
         // $now = date('now
         // foreach ($date as $key => $val) {
           // code...
           // $searchModel->StartDate = $val->StartDate;
           // print_r($val->JobID);
           // $now = date("Y-m-d H:i:s");
           // if ($this->Status == 2 AND   $val->StartDate  ) {
             // code...
             if ($this->job_status == 2 and $this->start_date == 'op') {
               $query->andWhere("start_date < now() AND job_status = '$this->job_status'");
               // code...
             }
           // }
         // }
        $query->andFilterWhere([
            'id' => $this->id,
            'job_created' => $this->job_created,
            // 'StartDate' => $this->StartDate,
            'end_date' => $this->end_date,
            'include_ope' => $this->include_ope,
            'is_meal' => $this->is_meal,
            'is_taxi' => $this->is_taxi,
            'is_ope' => $this->is_ope,
            'ope_allowance' => $this->ope_allowance,
            'taxi_allowance' => $this->taxi_allowance,
            'other_expense_allowance' => $this->other_expense_allowance,
            'job_fee' => $this->job_fee,
            // 'Total' => $this->Total,
            'job_status' => $this->job_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'job_code', $this->job_code])
            ->andFilterWhere(['like', 'description', $this->description])
            // ->andFilterWhere(['like', 'clClient.Name', $this->ClientID])
            ->andFilterWhere(['like', 'client_id', $this->client_id])
            ->andFilterWhere(['like', 'job_area', $this->job_area])
            ->andFilterWhere(['like', 'partner_id', $this->partner_id])
            ->andFilterWhere(['like', 'manager_id', $this->manager_id])
            ->andFilterWhere(['like', 'supervisor_id', $this->supervisor_id])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
