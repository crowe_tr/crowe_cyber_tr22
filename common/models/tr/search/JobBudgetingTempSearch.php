<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\JobBudgetingTemp;

/**
 * JobBudgetingTempSearch represents the model behind the search form of `common\models\tr\JobBudgetingTemp`.
 */
class JobBudgetingTempSearch extends JobBudgetingTemp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JobID', 'Seq'], 'integer'],
            [['EmployeeID', 'CreatedBy', 'CreatedAt', 'UpdateBy', 'UpdateAt'], 'safe'],
            [['Planning', 'FieldWork', 'Reporting', 'WrapUp', 'OverTime', 'Total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JobBudgetingTemp::find();
        $query->joinWith('employee');
        $query->orderBy(['hrEmployee.fullName' => SORT_ASC]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'JobID' => $this->JobID,
            'Seq' => $this->Seq,
            'Planning' => $this->Planning,
            'FieldWork' => $this->FieldWork,
            'Reporting' => $this->Reporting,
            'WrapUp' => $this->WrapUp,
            'OverTime' => $this->OverTime,
            'Total' => $this->Total,
            'CreatedAt' => $this->CreatedAt,
            'UpdateAt' => $this->UpdateAt,
        ]);

        $query->andFilterWhere(['like', 'EmployeeID', $this->EmployeeID])
            ->andFilterWhere(['like', 'CreatedBy', $this->CreatedBy])
            ->andFilterWhere(['like', 'UpdateBy', $this->UpdateBy]);

        return $dataProvider;
    }
}
