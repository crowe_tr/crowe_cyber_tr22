<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\views\vTimeReport;

/**
 * TimeReportSearch represents the model behind the search form of `common\models\tr\TimeReport`.
 */
class TimeReportSearch extends vTimeReport
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['EmployeeId', 'Date'], 'safe'],
            [['Status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = vTimeReport::find();
        $query->andWhere("Date Between '2019-08-01' AND '2019-08-31'");
        if(!empty($this->WorkHour)) {
            $query->andWhere("WorkHour < {$this->WorkHour}");
        }
        if(!empty($this->Overtime)) {
            $query->andWhere("Overtime > 0");
        }
        $query->andWhere([
            'EmployeeId' => $this->EmployeeId,
        ]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andWhere([
            'WorkHour' => $this->Status,
        ]);
        $query->andFilterWhere([
            'Status' => $this->Status
        ]);

        return $dataProvider;
    }
}
