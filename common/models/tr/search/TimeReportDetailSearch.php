<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\TimeReportDetail;

/**
 * TimeReportDetailSearch represents the model behind the search form of `common\models\tr\TimeReportDetail`.
 */
class TimeReportDetailSearch extends TimeReportDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeReportDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'TimeReportID' => $this->TimeReportID,
            'TaskTypeID' => $this->TaskTypeID,
            'JobId' => $this->JobId,
            'TaskID' => $this->TaskID,
            'WorkHour' => $this->WorkHour,
            'Overtime' => $this->Overtime,
        ]);

        return $dataProvider;
    }
}
