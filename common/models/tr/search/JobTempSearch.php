<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\JobTemp;

/**
 * JobTempSearch represents the model behind the search form of `common\models\tr\JobTemp`.
 */
class JobTempSearch extends JobTemp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JobID', 'ClientID', 'IncludeOPE', 'IsMeal', 'IsOutOfOffice', 'IsTaxi', 'Status', 'Flag'], 'integer'],
            [['JobCode', 'Description', 'JobArea', 'JobCreated', 'StartDate', 'EndDate', 'Partner', 'Manager', 'Supervisor', 'Percentage', 'CreatedBy', 'CreatedAt', 'UpdateBy', 'UpdateAt'], 'safe'],
            [['MealAllowance', 'OutOfOfficeAllowance', 'TaxiAllowance', 'AdministrativeCharge', 'OtherExpenseAllowance', 'Fee', 'TimeCharges'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JobTemp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'JobID' => $this->JobID,
            'ClientID' => $this->ClientID,
            'JobCreated' => $this->JobCreated,
            'StartDate' => $this->StartDate,
            'EndDate' => $this->EndDate,
            'IncludeOPE' => $this->IncludeOPE,
            'IsMeal' => $this->IsMeal,
            'IsOutOfOffice' => $this->IsOutOfOffice,
            'IsTaxi' => $this->IsTaxi,
            'MealAllowance' => $this->MealAllowance,
            'OutOfOfficeAllowance' => $this->OutOfOfficeAllowance,
            'TaxiAllowance' => $this->TaxiAllowance,
            'AdministrativeCharge' => $this->AdministrativeCharge,
            'OtherExpenseAllowance' => $this->OtherExpenseAllowance,
            'Fee' => $this->Fee,
            'TimeCharges' => $this->TimeCharges,
            'Status' => $this->Status,
            'Flag' => $this->Flag,
            'CreatedAt' => $this->CreatedAt,
            'UpdateAt' => $this->UpdateAt,
        ]);

        $query->andFilterWhere(['like', 'JobCode', $this->JobCode])
            ->andFilterWhere(['like', 'Description', $this->Description])
            ->andFilterWhere(['like', 'JobArea', $this->JobArea])
            ->andFilterWhere(['like', 'Partner', $this->Partner])
            ->andFilterWhere(['like', 'Manager', $this->Manager])
            ->andFilterWhere(['like', 'Supervisor', $this->Supervisor])
            ->andFilterWhere(['like', 'Percentage', $this->Percentage])
            ->andFilterWhere(['like', 'CreatedBy', $this->CreatedBy])
            ->andFilterWhere(['like', 'UpdateBy', $this->UpdateBy]);

        return $dataProvider;
    }
}
