<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\TimeReportTaxi;

/**
 * TimeReportTaxiSearch represents the model behind the search form of `common\models\tr\TimeReportTaxi`.
 */
class TimeReportTaxiSearch extends TimeReportTaxi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TimeReportID', 'Seq', 'cmTaxiID', 'TaskTypeID', 'JobId', 'TaskID'], 'integer'],
            [['Start', 'Finish', 'VoucherNo', 'Destination', 'Description'], 'safe'],
            [['Amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeReportTaxi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'TimeReportID' => $this->TimeReportID,
            'Seq' => $this->Seq,
            'cmTaxiID' => $this->cmTaxiID,
            'TaskTypeID' => $this->TaskTypeID,
            'JobId' => $this->JobId,
            'TaskID' => $this->TaskID,
            'Start' => $this->Start,
            'Finish' => $this->Finish,
            'Amount' => $this->Amount,
        ]);

        $query->andFilterWhere(['like', 'VoucherNo', $this->VoucherNo])
            ->andFilterWhere(['like', 'Destination', $this->Destination])
            ->andFilterWhere(['like', 'Description', $this->Description]);

        return $dataProvider;
    }
}
