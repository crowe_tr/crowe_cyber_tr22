<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\JobComment;

/**
 * JobCommentSearch represents the model behind the search form of `common\models\tr\JobComment`.
 */
class JobCommentSearch extends JobComment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CommentId', 'JobID'], 'integer'],
            [['Comments', 'CreatedBy', 'CreatedDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JobComment::find();
        $query->orderBy(['CreatedDate'=>SORT_DESC]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'CommentId' => $this->CommentId,
            'JobID' => $this->JobID,
            'CreatedDate' => $this->CreatedDate,
        ]);

        $query->andFilterWhere(['like', 'Comments', $this->Comments])
            ->andFilterWhere(['like', 'CreatedBy', $this->CreatedBy]);

        return $dataProvider;
    }
}
