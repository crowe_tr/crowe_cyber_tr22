<?php

namespace common\models\tr\search;

use yii\data\ActiveDataProvider;
use common\models\tr\JobList;
use common\components\CommonHelper;

class JobviewSearch extends JobList
{
  public function search($params)
  {
    $user = CommonHelper::getUserIndentity();

    $query = JobList::find();
    $query->orderBy(['start_date' => SORT_DESC]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);
    $this->load($params);

    if (!empty($this->periode)) {
      $periode = explode(" TO ", $this->periode);

      if(!empty($periode[0]) && !empty($periode[1]) ) {
        $from = date('Y-m-d', strtotime($periode[0]));
        $to = date('Y-m-d', strtotime($periode[1]));

        $query->andWhere("start_date between '".$from."' and '".$to."' ");
        $query->andWhere("end_date between '".$from."' and '".$to."' ");
      }
    }

    $query->andFilterWhere([
      'partner_id' => $this->partner_id,
      'manager_id' => $this->manager_id,
      'supervisor_id' => $this->supervisor_id,
      'job_status' => $this->job_status,
      'client_id' => $this->client_id,
      'flag' => $this->flag,
    ]);


    $query->andFilterWhere(['like', 'job_code', $this->job_code])
      ->andFilterWhere(['like', 'job_description', $this->job_description]);

    return $dataProvider;
  }
}
