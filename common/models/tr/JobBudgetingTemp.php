<?php

namespace common\models\tr;
use common\models\hr\Employee;

use Yii;

/**
 * This is the model class for table "trJobBudgetingTemp".
 *
 * @property int $JobID
 * @property int $Seq
 * @property string $EmployeeID
 * @property string $Planning
 * @property string $FieldWork
 * @property string $Reporting
 * @property string $WrapUp
 * @property string $OverTime
 * @property string $Total
 * @property string $CreatedBy
 * @property string $CreatedAt
 * @property string $UpdateBy
 * @property string $UpdateAt
 */
class JobBudgetingTemp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trJobBudgetingTemp';
    }

    public $Entity;
    public $Division;
    public $Grup;
    public $levelName;
    public $fullName;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JobID', 'EmployeeID'], 'required'],
            [['JobID'], 'integer'],
            [['Planning', 'FieldWork', 'Reporting', 'WrapUp', 'OverTime', 'Total'], 'number'],
            [['Entity', 'Division', 'Grup', 'levelName','fullName','CreatedAt', 'UpdateAt'], 'safe'],
            [['EmployeeID'], 'string', 'max' => 32],
            [['CreatedBy', 'UpdateBy'], 'string', 'max' => 16],
            // [['JobID'], 'unique', 'targetAttribute' => ['JobID', 'Seq']],
            [['JobID', 'EmployeeID'], 'unique', 'targetAttribute' => ['JobID', 'EmployeeID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'JobID' => 'Job ID',
            // 'Seq' => 'Seq',
            'EmployeeID' => 'Employee ID',
            'Planning' => 'Planning',
            'FieldWork' => 'Field Work',
            'Reporting' => 'Reporting',
            'WrapUp' => 'Wrap Up',
            'OverTime' => 'Over Time',
            'Total' => 'Total',
            'CreatedBy' => 'Created By',
            'CreatedAt' => 'Created At',
            'UpdateBy' => 'Update By',
            'UpdateAt' => 'Update At',
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['JobID' => 'JobID']);
    }
    public function getEmployee()
    {
        return $this->hasOne( Employee::className(), ['Id' => 'EmployeeID']);
    }
}
