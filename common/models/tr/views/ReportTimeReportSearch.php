<?php

namespace common\models\tr\views;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\views\ReportTimeReport;

class ReportTimeReportSearch extends ReportTimeReport
{
    public function rules()
    {
        return [
            [['Date'], 'safe'],
            [['Leader', 'Supervisor', 'Manager', 'EmployeeID', 'ClientID'], 'safe'],
            [['Position', 'Division', 'Entity'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }
    public function search($params)
    {

        $query = ReportTimeReport::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $query->where("
            year(Date) = year('".$this->Date."')
            date month(Date) = month('".$this->Date."')
        ");

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Leader' => $this->Leader,
            'Supervisor' => $this->Supervisor,
            'Manager' => $this->Manager,
            'EmployeeID' => $this->EmployeeID,
            'ClientID' => $this->ClientID,
            'JobID' => $this->JobID,
        ]);

        $query->andFilterWhere(['like', 'Position', $this->JobCode]);

        return $dataProvider;
    }
}
