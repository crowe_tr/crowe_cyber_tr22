<?php

namespace common\models\tr\views;

use Yii;

/**
 * This is the model class for table "ReportTimeReport".
 *
 * @property string $Leader
 * @property string $LeaderName
 * @property string $Supervisor
 * @property string $SupervisorName
 * @property string $Manager
 * @property string $ManagerName
 * @property string $EmployeeID
 * @property string $EmployeeName
 * @property string $Position
 * @property string $Division
 * @property string $Entity
 * @property string $Date
 * @property int $ClientID
 * @property string $ClientName
 * @property string $Zone
 * @property string $WorkHour
 * @property string $WHAmount
 * @property string $MealsAmount
 * @property string $OutOfOfficeAmount
 * @property string $TaxiAmount
 * @property string $Status
 */
class ReportTimeReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ReportTimeReport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['EmployeeID', 'EmployeeName', 'Date'], 'required'],
            [['Date'], 'safe'],
            [['ClientID'], 'integer'],
            [['WorkHour', 'WHAmount', 'MealsAmount', 'OutOfOfficeAmount', 'TaxiAmount'], 'number'],
            [['Leader', 'Supervisor', 'Manager', 'EmployeeID'], 'string', 'max' => 32],
            [['LeaderName', 'SupervisorName', 'ManagerName', 'EmployeeName'], 'string', 'max' => 128],
            [['Position'], 'string', 'max' => 45],
            [['Division'], 'string', 'max' => 50],
            [['Entity'], 'string', 'max' => 120],
            [['ClientName'], 'string', 'max' => 100],
            [['Zone'], 'string', 'max' => 256],
            [['Status'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Leader' => 'Leader',
            'LeaderName' => 'Leader Name',
            'Supervisor' => 'Supervisor',
            'SupervisorName' => 'Supervisor Name',
            'Manager' => 'Manager',
            'ManagerName' => 'Manager Name',
            'EmployeeID' => 'Employee ID',
            'EmployeeName' => 'Employee Name',
            'Position' => 'Position',
            'Division' => 'Division',
            'Entity' => 'Entity',
            'Date' => 'Date',
            'ClientID' => 'Client ID',
            'ClientName' => 'Client Name',
            'Zone' => 'Zone',
            'WorkHour' => 'Work Hour',
            'WHAmount' => 'Wh Amount',
            'MealsAmount' => 'Meals Amount',
            'OutOfOfficeAmount' => 'Out Of Office Amount',
            'TaxiAmount' => 'Taxi Amount',
            'Status' => 'Status',
        ];
    }
}
