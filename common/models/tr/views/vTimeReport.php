<?php

namespace common\models\tr\views;

use Yii;

/**
 * This is the model class for table "vtrTimeReport".
 *
 * @property string $EmployeeId
 * @property string $Name
 * @property string $Date
 * @property int $JobId
 * @property int $WorkHour
 * @property string $Overtime
 * @property int $Meal
 * @property int $Transportation
 * @property int $OutOfOffice
 * @property int $Status
 * @property int $Flag
 */
class vTimeReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtrTimeReport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['EmployeeId', 'Name', 'Date'], 'required'],
            [['Date', 'Overtime'], 'safe'],
            [['JobId', 'WorkHour', 'Meal', 'Transportation', 'OutOfOffice', 'Status'], 'integer'],
            [['EmployeeId'], 'string', 'max' => 32],
            [['Name'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'EmployeeId' => 'Employee ID',
            'Name' => 'Name',
            'Date' => 'Date',
            'JobId' => 'Job ID',
            'WorkHour' => 'Work Hour',
            'Overtime' => 'Overtime',
            'Meal' => 'Meal',
            'Transportation' => 'Transportation',
            'OutOfOffice' => 'Out Of Office',
            'Status' => 'Status'
        ];
    }
}
