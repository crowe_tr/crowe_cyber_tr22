<?php

namespace common\models\tr\ms;

use Yii;

/**
 * This is the model class for table "trMsEntity".
 *
 * @property int $id
 * @property string $entityCode
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $fax
 * @property string $npwp
 * @property string $logo
 * @property string $hrd
 * @property string $managerInCharge
 * @property string $finance
 * @property int $suspended
 */
class Entity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsEntity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['suspended'], 'integer'],
            [['entityCode'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 120],
            [['address', 'logo'], 'string', 'max' => 255],
            [['phone', 'fax'], 'string', 'max' => 20],
            [['npwp'], 'string', 'max' => 50],
            [['hrd', 'managerInCharge', 'finance'], 'string', 'max' => 45],
            [['entityCode'], 'unique'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'entityCode' => Yii::t('app', 'Entity Code'),
            'name' => Yii::t('app', 'Name'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'fax' => Yii::t('app', 'Fax'),
            'npwp' => Yii::t('app', 'Npwp'),
            'logo' => Yii::t('app', 'Logo'),
            'hrd' => Yii::t('app', 'Hrd'),
            'managerInCharge' => Yii::t('app', 'Manager In Charge'),
            'finance' => Yii::t('app', 'Finance'),
            'suspended' => Yii::t('app', 'Suspended'),
        ];
    }
}
