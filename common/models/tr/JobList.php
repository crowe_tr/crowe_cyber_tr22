<?php

namespace common\models\tr;

use Yii;
use yii\helpers\Html;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\CommonHelper;
use common\models\tr\JobComment;

class JobList extends \yii\db\ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'job_list_only';
  }

  public static function primaryKey()
  {
    return array('job_id');
  }

  public $search_text;
  public $periode;

  public function rules()
  {
    return [
      [
        [
          'search_text',
          'periode',
          'job_id',
          'job_code',
          'description',
          'client_id',
          'job_area',
          'client_areas',
          'job_created',
          'start_date',
          'end_date',
          'partner_id',
          'manager_id',
          'supervisor_id',
          'include_ope',
          'is_meal',
          'is_ope',
          'is_taxi',
          'meal_allowance',
          'meal_allowance_actual',
          'ope_allowance',
          'ope_allowance_actual',
          'taxi_allowance',
          'taxi_allowance_actual',
          'administrative_charge',
          'administrative_charge_actual',
          'other_expense_allowance',
          'other_expense_allowance_actual',
          'job_fee',
          'time_charges',
          'time_charges_actual',
          'percentage',
          'percentage_actual',
          'job_tatus',
          'flag',
          'created_by',
          'created_at',
          'update_by',
          'update_at',
          'total_other',
          'client_code',
          'client_name',
          // 'Industries',
          'entity_name',
          'div_name',
          'partner_full_name',
          'partner_initial',
          'manager_full_name',
          'manager_initial',
          'supervisor_full_name',
          'supervisor_initial',
          'status_job',
          'status_revision',
          'comment_count',
          'total_wh',
          'total_wh_act',
          'progress_time',
        ], 'safe'
      ],
    ];
  }

  public static function get_list_header($iemployee_id)
  {
    return Yii::$app->db->createCommand("call job_list('job-list-header','" . $iemployee_id . "')")->queryOne();
  }

  protected static function create_btn($status, $icap, $icount_data, $iname)
  {
    $active_btn = (strtolower($status) == $iname) ? ' btn-warning' : ' btn-primary';
    return ' ' . Html::a(
      Yii::t('app', $icap) . ' | ' . $icount_data,
      [$iname],
      ['class' => 'btn btn-cons ' . $active_btn]
    );
  }

  public static function set_button_header($status)
  {
    $user = CommonHelper::getUserIndentity();
    $employee_id = '';
    if ($user->is_admin != 1) {
      $employee_id = $user->user_id;
    }
    $count_header = self::get_list_header($employee_id);

    $status = (empty($status)) ? "all" : $status;

    $button = '';
    $button .= self::create_btn($status, 'ALL', $count_header['job_all'], 'all');
    $button .= self::create_btn($status, 'DRAFT', $count_header['job_draft'], 'draft');
    $button .= self::create_btn($status, 'SUBMITTED', $count_header['job_submitted'], 'submitted');
    $button .= self::create_btn($status, 'APPROVED', $count_header['job_approved'], 'approved');
    $button .= self::create_btn($status, 'REJECTED', $count_header['job_rejected'], 'rejected');
    $button .= self::create_btn($status, 'ON GOING', $count_header['job_on_going'], 'ongoing');
    $button .= self::create_btn($status, 'EXPIRED', $count_header['job_expired'], 'expired');
    $button .= self::create_btn($status, 'CLOSED', $count_header['job_closed'], 'closed');

    return $button;
  }
  public static function set_button_header_approval($status)
  {
    $user = CommonHelper::getUserIndentity();
    $employee_id = '';
    if ($user->IsAdmin != 1) {
      $employee_id = $user->Id;
    }
    $count_header = self::get_list_header($employee_id);

    $status = (empty($status)) ? "all" : $status;


    $button = '';
    $button .= self::create_btn($status, 'ALL', $count_header['job_all'], 'all');
    $button .= self::create_btn($status, 'SUBMITTED', $count_header['job_submitted'], 'submitted');
    $button .= self::create_btn($status, 'APPROVED', $count_header['job_approved'], 'approved');
    $button .= self::create_btn($status, 'REJECTED', $count_header['job_rejected'], 'rejected');
    $button .= self::create_btn($status, 'ON GOING', $count_header['job_on_going'], 'ongoing');
    $button .= self::create_btn($status, 'EXPIRED', $count_header['job_expired'], 'expired');
    $button .= self::create_btn($status, 'CLOSED', $count_header['job_closed'], 'closed');

    return $button;
  }

  public function getComments()
  {
    return $this->hasMany(JobComment::className(), ['JobID' => 'JobID']);
  }
}
