<?php

namespace common\models\tr;

use Yii;
use common\models\st\Task;
use common\models\st\TaskType;
use common\models\st\RuleTermZone;

/**
 * This is the model class for table "trTimeReportOutOfOffice".
 *
 * @property int $TimeReportID
 * @property int $Seq
 * @property int $TaskTypeID
 * @property int $JobId
 * @property int $TaskID
 * @property string $ZoneID
 *
 * @property Job $job
 * @property Task $task
 * @property TaskType $taskType
 * @property TimeReport $timeReport
 */
class TimeReportOutOfOffice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trTimeReportOutOfOffice';
    }
    public $EmployeeId;
    public $Date;
    public $isStayed; 


    public $OPEProvided;
    public $OPEAllocation;
    public $OPEClaimQty;
    public $OPEClaimQtyStayed;

    public $lookup;
    public function rules()
    {
        return [
            [['EmployeeId', 'Date', 'isStayed', 'Description'], 'safe'],

            [['TimeReportID', 'Seq', 'TrDetID', 'ZoneID'], 'safe'],
            [['TimeReportID', 'TrDetID', 'ZoneID'], 'required'],

            [['TimeReportID', 'Seq', 'TrDetID'], 'integer'],

            [['ZoneID'], 'string', 'max' => 255],
            [['TimeReportID', 'Seq'], 'unique', 'targetAttribute' => ['TimeReportID', 'Seq']],
            [['TimeReportID'], 'exist', 'skipOnError' => true, 'targetClass' => TimeReport::className(), 'targetAttribute' => ['TimeReportID' => 'ID']],
            

            [['JobId'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->TaskTypeID == 1){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            [['ZoneID'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->TaskTypeID == 1){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            [['Approval1','Approval2'], 'safe'],

            [['OutOfOffice', 'OPEProvided', 'OPEAllocation','OPEClaimQty', 'OPEClaimQtyStayed'], 'safe'],
            //[['OutOfOffice'], 'validateOPE', 'enableClientValidation' => false],
        ];
    }
    public function validateOPE($attribute_name, $params){
        $valid = true;
        if($this->OutOfOffice > 0){
            if($this->OPEProvided == true){
                $OPEClaimQty = ($this->isStayed == 0) ? $this->OPEClaimQty : $this->OPEClaimQtyStayed;
                if ($OPEClaimQty < $this->OPEAllocation){
                    $this->addError($attribute_name, "OPE: ".$this->OPEAllocation.", Batas claim OPE sudah mencapai batas, hak claim OPE : ".$OPEClaimQty);
                    $valid = false;
                }
            }else{
                $this->addError($attribute_name, "Anda tidak bisa claim ope");
                $valid = false;
            }
        }
        return $valid;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TimeReportID' => Yii::t('app', 'Time Report'),
            'Seq' => Yii::t('app', 'Seq'),
            'TrDetID' => Yii::t('app', 'Task Description'),
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['JobId' => 'JobId']);
    }
    public function getTasktype()
    {
        return $this->hasOne(TaskType::className(), ['Id' => 'TaskTypeID']);
    }
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['Id' => 'TaskID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeReport()
    {
        return $this->hasOne(TimeReport::className(), ['ID' => 'TimeReportID']);
    }
    public function getZone()
    {
        return $this->hasOne(RuleTermZone::className(), ['id' => 'ZoneID']);
    }

}
