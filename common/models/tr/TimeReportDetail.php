<?php

namespace common\models\tr;

use Yii;
use common\models\tr\TimeReport;
use common\models\st\Task;
use common\models\st\TaskType;
use common\models\tr\Job;


class TimeReportDetail extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'trTimeReportDetail';
    }

    public $EmployeeId;
    public $Date;
    public $isStayed; 

    public $MaxOvertime;
    public $OvertimeAllocation;

    public $MinWorkhour;
    public $MaxWorkhour;
    public $WorkHourAllocation;
    public $lookup;

    public function rules()
    {
        return [
            [['id','WorkHour', 'Overtime', 'EmployeeId', 'Date', 'isStayed', 'lookup', 'Description', 'attachment'], 'safe'],
            ['attachment', 'file', 'extensions' => ['png', 'jpg', 'pdf', 'jpeg'], 'maxSize' => 1024 * 1000 * 3],


            [['TimeReportID', 'TaskTypeID', 'TaskID', 'WorkHour'], 'required'],

            [['TimeReportID', 'TaskTypeID', 'JobId', 'TaskID'], 'integer'],
            
            [['TimeReportID', 'TaskTypeID', 'TaskID'], 'unique', 
                'targetAttribute' => ['TimeReportID', 'TaskTypeID', 'TaskID'],
                'message' => 'Tasks has already been taken',
                'when' => function ($model) { 
                    return $model->TaskID == 1; 
                }, 
            ],
            [['TimeReportID', 'TaskTypeID', 'TaskID', 'JobId'], 'unique', 
                'targetAttribute' => ['TimeReportID', 'TaskTypeID', 'TaskID', 'JobId'],
                'message' => 'Projects has already been taken',
                'when' => function ($model) { 
                    return $model->TaskID <> 1; 
                }, 
            ],

            [['JobId'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->TaskTypeID == 1){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            [['Approval1','Approval2'], 'safe'],
            [['MaxOvertime', 'MinWorkhour', 'MaxWorkhour','OvertimeAllocation', 'WorkHourAllocation'], 'safe'],
            [['Overtime'], 'validateOvertime', 'enableClientValidation' => true],
            [['WorkHour'], 'validateWorkHour', 'enableClientValidation' => true],
            
        ];
    }
    public function beforeSave($insert) {
        $this->WorkHour = abs(round($this->WorkHour, 1));
        $this->Overtime = abs(round($this->Overtime, 1));
        
        $this->WorkHour = Yii::$app->formatter->asDecimal($this->WorkHour);
        $this->Overtime = Yii::$app->formatter->asDecimal($this->Overtime);


        $sql = "CALL `antTrDetailBeforeSave`({$this->TimeReportID}, '{$this->EmployeeId}', {$this->TaskID}, {$this->TaskTypeID}, {$this->WorkHour})";
        $rule = Yii::$app->db->createCommand($sql)->queryOne();
        if(!empty($rule)) {
            $return = true;
            if($rule['is_day_valid']==0){
                $taskName = $rule['taskName'];
                $this->addError('Description', "Hari ini tidak diijinkan untuk mengajukan permohonan : ".$taskName);
                $return = false;
            }
            if($rule['is_max_days_valid']==0){
                $max_days = $rule['max_days'];
                $total_day = $rule['total_day'];
                $this->addError('Description', "Maximal mengajukan permohonan ini adalah ".$max_days.". Anda sudah mengajukan sebanyak : ".$total_day);
                $return = false;
            }
            if($rule['is_max_hours_valid']==0){
                $WorkHour = $rule['max_hours'];
                $this->addError('Description', "Jam melebihi ".$WorkHour);
                $return = false;
            }
            if($rule['is_attachment']==1 && empty($this->attachment)){
                $this->addError('attachment', "Attachment wajib diisi");
                $return = false;
            }
            return $return;
        }else{
            return true;
        }
    }
    public function validateOvertime($attribute_name, $params){
        $valid = true;
        /*
        if(($this->WorkHourAllocation < $this->MaxWorkhour) && ($this->OvertimeAllocation > 0 AND $this->OvertimeAllocation <> NULL)){
            $valid = false;
            $this->addError($attribute_name, "Tidak bisa claim Overtime karena Total WorkHour : ".$this->WorkHourAllocation." Jam, belum mencapai : ".$this->MaxWorkhour." Jam");
        }else{
            if($this->MaxOvertime < $this->OvertimeAllocation){
                $valid = false;
                $this->addError($attribute_name, "Total Overtime : ".$this->OvertimeAllocation." Jam, dalam sehari tidak boleh melebihi : ".$this->MaxOvertime." Jam");
            }
        }
        */
        if(fmod($this->Overtime,0.5) != 0){
            $valid = false;
            $this->addError($attribute_name, "Overtime must be a multiple of 0.5");
        }

        return $valid;
    }
    public function validateWorkHour($attribute_name, $params){
        $valid = true;
        /*
        $WorkHour = $this->WorkHourAllocation + $this->OvertimeAllocation;
        if($this->MinWorkhour > $WorkHour){
            $valid = false;
            $this->addError($attribute_name, "Total WorkHour : ".$this->WorkHourAllocation." Jam, dalam sehari tidak boleh kurang dari : ".$this->MinWorkhour." Jam");
        } else if($this->MaxWorkhour < $WorkHour){
            $valid = false;
            $this->addError($attribute_name, "Total WorkHour : ".$this->WorkHourAllocation." Jam, dalam sehari tidak boleh melebihi : ".$this->MaxWorkhour." Jam");
        }
        */
        if(fmod($this->WorkHour,0.5) != 0){
            $valid = false;
            $this->addError($attribute_name, "WorkHour must be a multiple of 0.5");
        }
        return $valid;
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'TimeReportID' => Yii::t('app', 'Time Report ID'),
            'TaskTypeID' => Yii::t('app', 'Class / Task Type'),
            'JobId' => Yii::t('app', 'Job'),
            'TaskID' => Yii::t('app', 'Task'),
            'WorkHour' => Yii::t('app', 'Work Hours'),
            'Overtime' => Yii::t('app', 'Overtimes'),
            'attachment' => Yii::t('app', 'File Attachment'),
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['JobId' => 'JobId']);
    }
    public function getTasktype()
    {
        return $this->hasOne(TaskType::className(), ['Id' => 'TaskTypeID']);
    }
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['Id' => 'TaskID']);
    }

    public function getParent()
    { 
        return $this->hasOne(TimeReport::className(), ['id' => 'TimeReportID']);
    }
    public function getTimeReport()
    { 
        return $this->hasOne(TimeReport::className(), ['id' => 'TimeReportID']);
    }


}
