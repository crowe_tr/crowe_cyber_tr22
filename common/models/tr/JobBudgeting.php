<?php

namespace common\models\tr;

use Yii;
use common\models\hr\Employee;

class JobBudgeting extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tr_job_budgeting';
    }

    public $Entity;
    public $Division;
    public $Grup;
    public $levelName;
    public $fullName;

    public function rules()
    {
        return [
            [['job_id', 'employee_id'], 'required'],
            [['job_id'], 'integer'],
            [['created_at', 'updated_at' , 'Entity', 'Division', 'Grup', 'levelName','fullName',
              'planning', 'field_work', 'reporting', 'wrap_up', 'over_time', 'total_wh'], 'safe'],
            [['employee_id', 'created_by', 'update_by'], 'string', 'max' => 16],
            [['job_id', 'employee_id'], 'unique', 'targetAttribute' => ['job_id', 'employee_id']],
            // [['EmployeeID'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['EmployeeID' => 'Id']],

        ];
    }

    public function attributeLabels()
    {
        return [
            'job_id' => Yii::t('app', 'Job ID'),
            'employee_id' => Yii::t('app', 'Employee'),
            'planning' => Yii::t('app', 'Planning'),
            'field_work' => Yii::t('app', 'Field Work'),
            'reporting' => Yii::t('app', 'Reporting'),
            'wrap_up' => Yii::t('app', 'Warp Up'),
            'over_time' => Yii::t('app', 'Over Time'),
            'total_wh' => Yii::t('app', 'Total'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'update_by' => Yii::t('app', 'Update By'),
            'update_at' => Yii::t('app', 'Update At'),
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }
    public function getEmployee()
    {
        return $this->hasOne( Employee::className(), ['user_id' => 'employee_id']);
    }
}
