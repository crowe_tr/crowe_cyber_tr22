<?php

namespace common\models\hr;

use Yii;
use common\models\cm\Level;


/**
 * This is the model class for table "vempGroup".
 *
 * @property string $id
 * @property string $fullName
 * @property int $entityID
 * @property int $divisionID
 */
class VempGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vempGroup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'fullName'], 'required'],
            [['entityID', 'divisionID'], 'integer'],
            [['id'], 'string', 'max' => 32],
            [['fullName', 'levelID'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullName' => 'Full Name',
            'entityID' => 'Entity ID',
            'divisionID' => 'Division ID',
            'levelID' => 'level ID',
        ];
    }

    public function getLevel()
    {
      return $this->hasOne(Level::className(), ['id' => 'levelID']);
    }
}
