<?php

namespace common\models\hr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\hr\Employee as EmployeeModel;

class Employee extends EmployeeModel
{

    public function rules()
    {
        return [
            [['user_name', 'password_hash', 'auth_kkey', 'account_activation_token', 'user_email', 'user_id',
              'full_name', 'initial', 'gender', 'place_of_birth', 'date_of_birth', 'join_date', 'education',
              'certificate', 'religion', 'marital_status', 'nationality', 'resign_date',
              'resign_description', 'address', 'postal_code', 'mobile_phone', 'photo', 'created_by',
              'created_date', 'last_update_by', 'last_update_date', 'deleted_by', 'deleted_date',
              'parent_id'], 'safe'],
            [['password_reset_token', 'flag', 'entity_id', 'level_id', 'div_id', 'dept_id',
              'keep_vacation_balance', 'annual_vacation'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = EmployeeModel::find();
        $query->where("user_id<>'0'");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'password_reset_token' => $this->password_reset_token,
            'flag' => $this->flag,
            'date_of_birth' => $this->date_of_birth,
            'join_date' => $this->join_date,
            'entity_id' => $this->entity_id,
            'level_id' => $this->level_id,
            'div_id' => $this->div_id,
            'dept_id' => $this->dept_id,
            'resign_date' => $this->resign_date,
            'keep_vacation_balance' => $this->keep_vacation_balance,
            'annual_vacation' => $this->annual_vacation,
            'created_date' => $this->created_date,
            'last_update_date' => $this->last_update_date,
            'deleted_date' => $this->deleted_date,
        ]);

        $query->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'account_activation_token', $this->account_activation_token])
            ->andFilterWhere(['like', 'user_email', $this->user_email])
            ->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'initial', $this->initial])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'place_of_birth', $this->place_of_birth])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'certificate', $this->certificate])
            ->andFilterWhere(['like', 'religion', $this->religion])
            ->andFilterWhere(['like', 'marital_status', $this->marital_status])
            ->andFilterWhere(['like', 'nationality', $this->nationality])
            ->andFilterWhere(['like', 'resign_description', $this->resign_description])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'mobile_phone', $this->mobile_phone])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'last_update_by', $this->last_update_by])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by])
            ->andFilterWhere(['like', 'parent_id', $this->parent_id]);

        return $dataProvider;
    }
}
