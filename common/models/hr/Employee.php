<?php

namespace common\models\hr;
use yii\web\IdentityInterface;

use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Entity;
use common\models\cm\Level;
use common\models\hr\HrAnnualLeave;


use Yii;

class Employee extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 1;
    public $id_label;
    public $Captcha;
    public $passwordRepeat;
    public $Permissions;
    public $year;

    public static function tableName()
    {
        return 'hr_employee';
    }

    public function rules()
    {
        return [
            [['guid','manager_id', 'supervisor_id', 'Permissions', 'PasswordRepeat', 'Captcha', 'parent_id', 'religion',
              'marital_status'], 'safe'],
            [['password_reset_token', 'flag', 'entity_id', 'level_id', 'div_id', 'keep_vacation_balance',
              'annual_vacation'], 'integer'],
            [['date_of_birth', 'join_date', 'resign_date', 'created_date', 'last_update_date', 'dept_id', 'address',
              'deleted_date', 'place_of_birth', 'education', 'max_view_time_report'], 'safe'],
            [['certificate', 'resign_description', 'address'], 'string'],
            [['user_name', 'auth_key', 'user_id', 'place_of_birth', 'nationality', 'mobile_phone'], 'string', 'max' => 32],
            [['password_hash', 'account_activation_token', 'education'], 'string', 'max' => 255],
            [['created_by', 'last_update_by', 'deleted_by'], 'string', 'max' => 64],
            [['user_email'], 'email'],
            [['full_name', 'photo'], 'string', 'max' => 128],
            [['initial'], 'string', 'max' => 4],
            [['gender', 'religion', 'marital_status'], 'string'],
            [['postal_code'], 'string', 'max' => 10],

            [['user_id', 'user_name'], 'unique'] ,

            [['user_id', 'full_name', 'entity_id', 'div_id', 'level_id', 'user_name', 'flag'], 'required'],

            ['passwordRepeat', 'compare', 'on'=>'changepassword', 'compareAttribute'=>'password_hash', 'message'=>"Passwords don't match" ],
            [['password_hash', 'passwordRepeat'], 'required', 'on'=>'changepassword'],

            [['dept_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dept::className(), 'targetAttribute' => ['dept_id' => 'id']],
            [['div_id'], 'exist', 'skipOnError' => true, 'targetClass' => Division::className(), 'targetAttribute' => ['div_id' => 'id']],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'id']],
            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['level_id' => 'id']],

            [['year', 'annual_vacation'], 'required', 'on'=>'resetannualleave']
        ];
    }

    public function attributeLabels()
    {
        return [
            'manager_id' => Yii::t('app', 'Manager'),
            'supervisor_id' => Yii::t('app', 'Supervisor'),
            'user_name' => Yii::t('app', 'Username'),
            'password_hash' => Yii::t('app', 'Password'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'account_activation_token' => Yii::t('app', 'Account Activation Token'),
            'user_email' => Yii::t('app', 'Email'),
            'flag' => Yii::t('app', 'Status'),
            'user_id' => Yii::t('app', 'ID'),
            'id_label'=>'ID.',
            'parent_id' => 'Group',
            'full_name' => Yii::t('app', 'Full Name'),
            'initial' => Yii::t('app', 'Initial'),
            'gender' => Yii::t('app', 'Gender'),
            'place_of_birth' => Yii::t('app', 'Place Of Birth'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'join_date' => Yii::t('app', 'Join Date'),
            'entity_id' => Yii::t('app', 'Entity'),
            'level_id' => Yii::t('app', 'Level'),
            'div_id' => Yii::t('app', 'Division'),
            'dept_id' => Yii::t('app', 'Department'),
            'education' => Yii::t('app', 'Education'),
            'certificate' => Yii::t('app', 'Certificate'),
            'religion' => Yii::t('app', 'Religion'),
            'marital_status' => Yii::t('app', 'Marital Status'),
            'nationality' => Yii::t('app', 'Nationality'),
            'resign_date' => Yii::t('app', 'Resign Date'),
            'resign_description' => Yii::t('app', 'Resign Description'),
            'address' => Yii::t('app', 'Address'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'mobile_phone' => Yii::t('app', 'Mobile Phone'),
            'photo' => Yii::t('app', 'Photo'),
            'keep_vacation_balance' => Yii::t('app', 'Keep Vacation Balance'),
            'annual_vacation' => Yii::t('app', 'Annual Vacation'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_date' => Yii::t('app', 'Created Date'),
            'last_update_by' => Yii::t('app', 'Last Update By'),
            'last_update_date' => Yii::t('app', 'Last Update Date'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'deleted_date' => Yii::t('app', 'Deleted Date'),
            'max_view_time_report' => 'Max TimeReport',
        ];
    }
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    public function validatePassword($password)
    {
        if (is_null($this->password_hash)) {
            return false;
        }

        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    public static function findIdentity($username)
    {
        return static::findOne(['user_id' => $username, 'flag' => self::STATUS_ACTIVE]);
    }

    public function getDept()
    {
        return $this->hasOne(Dept::className(), ['id' => 'dept_id']);
    }

    public function getDivision()
    {
        return $this->hasOne(Division::className(), ['id' => 'div_id']);
    }


    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['id' => 'entity_id']);
    }

    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'level_id']);
    }
    public function getLeader()
    {
        return $this->hasOne(Employee::className(), ['guid' => 'parent_id']);
        // $data = $this->hasOne(Employee::className(), ['guid' => 'parent_id']);
        // var_dump($data);
        // die();

    }
    public function getSupervisor()
    {
        return $this->hasOne(Employee::className(), ['guid' => 'supervisor_id']);
    }
    public function getAnnualLeave()
    {
        return $this->hasOne(HrAnnualLeave::className(), ['user_id' => 'employee_id']);
    }
    public function getManager()
    {
        return $this->hasOne(Employee::className(), ['guid' => 'manager_id']);
    }
    public function getProjectmanager()
    {
        $sql = "select get_superior_all_project(:params_1, :params_2) project_manager";
        $params = [':params_1'=>$this->id, ':params_2'=>'2'];
        return Yii::$app->db->createCommand($sql, $params)->queryAll();
    }
    public function getProjectsupervisor()
    {
        $sql = "select get_superior_all_project(:params_1, :params_2) project_supervisor";
        $params = [':params_1'=>$this->id, ':params_2'=>'1'];
        return Yii::$app->db->createCommand($sql, $params)->queryAll();
    }
}
