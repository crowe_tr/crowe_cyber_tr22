<?php

namespace common\models\hr;

use Yii;

/**
 * This is the model class for table "HrAnnualLeave".
 *
 * @property int $id
 * @property string $employee_id
 * @property int $annual_year
 * @property int $annual_days Annual Amount in Day
 * @property string $annual_hours Annual Amount in Hours ( Auto from Day * 8 Hour )
 * @property string $annual_used_hours_ob
 * @property string $annual_used_hours
 * @property string $annual_balance_hours
 * @property string $annual_balance_days
 *
 * @property HrEmployee $employee
 */
class HrAnnualLeave extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hr_annual_leave';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'annual_year', 'annual_days', 'annual_used_hours_ob'], 'safe'],
            [['annual_year', 'annual_days', 'annual_used_hours_ob'], 'required'],
            [['annual_year', 'annual_days'], 'integer'],
            [['annual_hours', 'annual_used_hours_ob', 'annual_used_hours', 'annual_balance_hours', 'annual_balance_days'], 'number'],
            [['employee_id'], 'string', 'max' => 32],
            [['employee_id', 'annual_year'], 'unique', 'targetAttribute' => ['employee_id', 'annual_year']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'annual_year' => 'Year',
            'annual_days' => 'Days',
            'annual_hours' => 'Hours',
            'annual_used_hours_ob' => 'Used Hours Ob',
            'annual_used_hours' => 'Used Hours',
            'annual_balance_hours' => 'Balance Hours',
            'annual_balance_days' => 'Balance Days',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(HrEmployee::className(), ['Id' => 'employee_id']);
    }
}
