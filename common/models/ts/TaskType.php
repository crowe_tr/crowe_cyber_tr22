<?php

namespace common\models\ts;

use Yii;

/**
 * This is the model class for table "stTaskType".
 *
 * @property int $id
 * @property string $taskTypeName
 * @property int $flag
 */
class TaskType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stTaskType';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taskTypeName'], 'required'],
            [['flag'], 'integer'],
            [['taskTypeName'], 'string', 'max' => 30],
            [['taskTypeName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'taskTypeName' => Yii::t('app', 'Task Type Name'),
            'flag' => Yii::t('app', 'Flag'),
        ];
    }
}
