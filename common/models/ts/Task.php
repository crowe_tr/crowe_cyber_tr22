<?php

namespace common\models\ts;

use Yii;

/**
 * This is the model class for table "stTask".
 *
 * @property int $id
 * @property int $seq
 * @property string $taskName
 * @property int $taskTypeID
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stTask';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seq', 'taskTypeID'], 'integer'],
            [['taskName'], 'string', 'max' => 60],
            [['taskTypeID', 'taskName'], 'unique', 'targetAttribute' => ['taskTypeID', 'taskName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seq' => Yii::t('app', 'Seq'),
            'taskName' => Yii::t('app', 'Task Name'),
            'taskTypeID' => Yii::t('app', 'Task Type ID'),
        ];
    }
}
