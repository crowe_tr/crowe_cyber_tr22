<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ListView;

use yii\widgets\Pjax;
use kartik\widgets\FileInput;

/****** common HELPER SMS :
 * fungsi-fungsi yang biasa dipakai / dibutuhkan di semua module buatan si AA
 * "buat hidup lebih mudah"
 *
 * ada beberapa initial, yaitu :
 * - get* => return berupa value (berbagai type),
 * - set* => return berupa value juga tapi dikhususkan untuk ganti value,contoh: $a = setValue(123);
 * - is* => return berupa boolean (1/0), contoh: if(isEmpty($a)) ? ...
 * - js => return berupa function js
 * *mau buat baru ? ikutin initial ini biar jadi standar !
 *
 * PENTING !!!!
 * Jangan sebarang hapus/ganti fungsi dibawah jika tidak tau apa yang dilakukan !!!!
 * banyak module yang menggunakan helper ini jika mau ubah sesuatu disarankan buat fungsi baru !
 ***********************/

class ant
{
    public static function form_template()
    {
        return '
            <div class="row row-input">
                <div class="col-12 col-sm-4 col-md-4 col-lg-3">
                    {label}
                    <span class="subtitle">{hint}</span>
                </div>
                <div class="col-12 col-sm-8 col-md-8 col-lg-9 col-12">
                    {input}
                    {error}
                </div>
            </div>
        ';
    }
    public static function form_search_template()
    {
        return '
            <div class="row">
                <div class="col-12">
                    {input}
                </div>
            </div>
        ';
    }
    public static function field_config($type, $suffix = "")
    {
        $r = [];
        $r['template'] = '<div class="row row-input">
            <div class="col-3">
                {label}
                <span class="subtitle">{hint}</span>
            </div>
            <div class="col-9">
                {input}
                {error}
                ' . $suffix . '
            </div>
        </div>';
        $r['accept'] = 'application/pdf, application/vnd.ms-excel, image/*';
        $r['inputfile_initialPreviewAsData'] = true;
        $r['inputfile_overwriteInitial'] = true;
        $r['inputfile_showBrowse'] = false;
        $r['inputfile_showPreview'] = true;
        $r['inputfile_showCaption'] = false;

        switch ($type) {
            case 'thumbnail2':
                $r['template'] = '<div class="row ">
                                <div class="col-4 col-sm-3">
                                    <b>{label}</b>
                                    <small style="font-size: 13px">{hint}</small>
                                </div>
                                <div class="col-6 col-sm-4">
                                    {input}
                                    {error}
                                    ' . $suffix . '
                                </div>
                            </div>';
                $r['accept'] = 'image/*';
                break;
            case 'thumbnail':
                $r['template'] = '<div class="row row-input">
                                    <div class="col-6 col-sm-3 col-md-3 col-lg-3">
                                        {label}
                                        <small class="subtitle">{hint}</small>
                                    </div>
                                    <div class="col-6 col-sm-4">
                                        {input}
                                        {error}
                                        ' . $suffix . '
                                    </div>
                                </div>';
                $r['accept'] = 'image/*';
                break;
            case 'image':
                $r['template'] = '{input}<div class="fileinput-label">{label}</div>';
                $r['accept'] = 'image/*';
                break;
            case 'headerimage':
                $r['template'] = '{input}';
                $r['accept'] = 'image/*';
                break;
            case 'bigimage':
                $r['accept'] = 'image/*';
                break;
            case 'file':
                $r['inputfile_initialPreviewAsData'] = false;
                $r['inputfile_overwriteInitial'] = false;
                $r['inputfile_showBrowse'] = true;
                $r['inputfile_showPreview'] = false;
                $r['inputfile_showCaption'] = true;

                break;
        }

        return $r;
    }

    public static function list($provider, $search, $param = [])
    {
        $param['itemOptions'] = !empty($param['itemOptions']) ? $param['itemOptions'] : ['tag' => 'div', 'class' => 'col-12 col-sm-4 col-md-6 col-lg-3 p-1 ant-item'];
        if (empty($param['layout'])) {
            $param['layout'] = '<div class="row mb-3">
                                    <div class="col">
                                        {summary}
                                    </div>
                                    <div class="col-auto" data-select2-id="25">
                                        <div class="dropdown d-block d-md-none" data-select2-id="24">
                                            <a href="javascript:;" onclick="ant_list_filter();" class="btn btn-white btn-sm"><i class="fe fe-sliders mr-1"></i> Filter </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    {items}
                                </div>
                                {pager}
                            ';
        }
        $list = ListView::widget([
            'dataProvider' => $provider,
            //'filterModel' => $search,
            'options' => [
                'tag' => 'div',
                'class' => '',
                'id' => 'ant-list',
            ],
            'layout' => $param['layout'],
            'itemView' => 'i_item',
            'itemOptions' => $param['itemOptions'],
            'pager' => [
                'options' => [
                    'class' => 'pagination pagination-sm',
                    'linkOptions' => ['class' => 'page-link'],

                    'prevPageLabel' => 'Previous',   // Set the label for the “previous” page button
                    'nextPageLabel' => 'Next',   // Set the label for the “next” page button
                    'firstPageLabel' => 'First',   // Set the label for the “first” page button
                    'lastPageLabel' => 'Last',    // Set the label for the “last” page button
                    'nextPageCssClass' => 'next',    // Set CSS class for the “next” page button
                    'prevPageCssClass' => 'prev',    // Set CSS class for the “previous” page button
                    'firstPageCssClass' => 'first',    // Set CSS class for the “first” page button
                    'lastPageCssClass' => 'last',    // Set CSS class for the “last” page button
                    'maxButtonCount' => 10,    // Set maximum number of page buttons that can be displayed 
                ]
            ],
        ]);

        return $list;
    }
    public static function grid($provider, $search, $column)
    {
        $grid = GridView::widget([
            'id' => 'ant-grid',
            'dataProvider' => $provider,
            'filterModel' => $search,
            'columns' => $column,
            'layout' => '
                    <div class="row">
                        <div class="col-12">
                            <div style="overflow-x: auto;">
                                {items}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer pb-0">
                        <div class="row">
                            <div class="col-9">
                                {pager}
                            </div>
                            <div class="col-3 text-right">
                                {summary}
                            </div>
                        </div>
                    </div>
                ',
            'tableOptions' => [
                'class' => 'table-sm',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pagination-sm',
                    'linkOptions' => ['class' => 'page-link'],

                    'prevPageLabel' => 'Previous',   // Set the label for the “previous” page button
                    'nextPageLabel' => 'Next',   // Set the label for the “next” page button
                    'firstPageLabel' => 'First',   // Set the label for the “first” page button
                    'lastPageLabel' => 'Last',    // Set the label for the “last” page button
                    'nextPageCssClass' => 'next',    // Set CSS class for the “next” page button
                    'prevPageCssClass' => 'prev',    // Set CSS class for the “previous” page button
                    'firstPageCssClass' => 'first',    // Set CSS class for the “first” page button
                    'lastPageCssClass' => 'last',    // Set CSS class for the “last” page button
                    'maxButtonCount' => 10,    // Set maximum number of page buttons that can be displayed 
                ]
            ],

            'resizableColumns' => false,
            'bordered' => false,
            'striped' => false,
            'condensed' => false,
            'responsive' => false,
            'hover' => true,
            'persistResize' => false,
            'responsiveWrap' => false,
            /*
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'beforeGrid' => '',
                'afterGrid' => '',
                'options'=> [
                    'id' => 'ant-pjax',
                ],
                'clientOptions' => ['method' => 'POST']
            ]
            */
        ]);

        return $grid;
    }

    public static function inputfile($form, $model, $field, $type, $hint = "", $dir = "timereport_detail/")
    {
        $suffix  = (empty($model->$field)) ? '' : '<div class="row">
                                                <div class="col-9 col-offset-3">
                                                    ' . Html::a('Download file', Yii::getAlias('@public_baseurl/' . $dir) . $model->$field, ['download' => 'true']) . '
                                                </div>
                                            </div>';

        $config = ant::field_config($type, $suffix);
        $form = $form->field(
            $model,
            $field,
            ['template' => $config['template'], 'options' => ['class' => '']]
        )->widget(FileInput::classname(), [
            'options' => ['accept' => $config['accept']],
            'pluginOptions' => [
                'title' => '',
                'initialPreview' => empty($model->$field) ? false : Yii::getAlias('@public_baseurl/' . $dir) . $model->$field,
                'overwriteInitial' => $config['inputfile_overwriteInitial'],
                'initialPreviewAsData' => $config['inputfile_initialPreviewAsData'],
                'showBrowse' => $config['inputfile_showBrowse'],
                'showPreview' => $config['inputfile_showPreview'],
                'showCaption' => $config['inputfile_showCaption'],

                'browseOnZoneClick' => true,
                'showCancel' => false,
                'showRemove' => false,
                'showUpload' => false,
                'maxFileSize' => 10240,
                //'browseClass' => 'btn btn-sm btn-primary',
                'browseIcon' => '<i class="fa fa-file"></i> Browse',
                //'removeClass' => 'btn btn-sm btn-danger text-white',
                //'removeIcon' => '<i class="fa fa-trash"></i> Remove',

            ]
        ])->hint($hint);
        return $form;
    }

    public static function availablebutton($available, $link, $class = "")
    {
        $available_confirm = ($available) ? 'Apakah anda yakin produk ini sudah tidak tersedia ? semua iklan aktif produk ini akan di sembunyikan' : 'Apakah anda yakin produk ini sudah tersedia kembali ? semua iklan aktif akan dimunculkan kembali';
        $available_label = ($available) ? 'Tandai Tidak tersedia' : 'Tandai Tersedia';

        return Html::a($available_label, $link, ['class' => $class, 'data-method' => 'post', 'data-confirm' => $available_confirm]);
    }


    public static function antpload_input($model, $field, $path)
    {
        $src = "";
        if (!empty($model->$field)) {
            $src = Yii::getAlias('@public_baseurl/' . $path)  . $model->$field;
        }

        $file = '
                <div class="antpload">
                    <span></span>
                    <input class="antfile-input" type="file" accept="image/*">
                    ' . Html::activeHiddenInput($model, $field, ['class' => 'file-label']) . '
                    <img class="antfile-preview" src="' . $src . '"/>
                    <a class="antfile-remove" href="javascript:;"></a>
                    <div class="antfile-loading"></div>
                </div>
            ';

        return $file;
    }

    public static function antpload_upload($_from, $_old_from, $_to, $topath)
    {
        $path = \Yii::$app->basePath . "/web/public/";
        $from = $path . "tmp/" . $_from;
        $to = $path . $topath . $_to;

        $return = $_from;
        if ($_from != $_old_from) {
            if (!empty($_from)) {
                shell_exec('mv ' . $from . ' ' . $to);
                $return = $_to;
            }    
        }

        return $return;
    }
}
