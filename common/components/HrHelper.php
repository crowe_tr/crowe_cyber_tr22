<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
// use common\components\HrHelper;
use common\components\CommonHelper;
use common\models\hr\Employee;

/****** common HELPER :
* fungsi-fungsi yang biasa dipakai / dibutuhkan di semua module buatan si AA
* "buat hidup lebih mudah"
*
* ada beberapa initial, yaitu :
* - get* => return berupa value (berbagai type),
* - set* => return berupa value juga tapi dikhususkan untuk ganti value,contoh: $a = setValue(123);
* - is* => return berupa boolean (1/0), contoh: if(isEmpty($a)) ? ...
* - js => return berupa function js
* *mau buat baru ? ikutin initial ini biar jadi standar !
*
* PENTING !!!!
* Jangan sebarang hapus/ganti fungsi dibawah jika tidak tau apa yang dilakukan !!!!
* banyak module yang menggunakan helper ini jika mau ubah sesuatu disarankan buat fungsi baru !
***********************/

class HrHelper
{

    public static function getAll($param = '')
    {
        $results = [];
        $user = CommonHelper::getUserIndentity();
        Yii::$app->cache->flush();

        if (!empty($user->CompanyId)) {
            if(isset($param['withAll'])){
                $branch[0] = 'SELECT All';
            }

            $where = array('CompanyId' => $user->CompanyId);
            if(!empty($param['BranchId'])){
                $where = array_merge($where, array('BranchId' => $param['BranchId']));
            }
            if(!empty($param['DeptId'])){
                $where = array_merge($where, array('DeptId' => $param['DeptId']));
            }
            if(!empty($param['Position'])){
                $where = array_merge($where, array('Position' => $param['Position']));
            }
            if(isset($param['withsis'])){
                if($param['withsis'] == true){
                    $results['SIS'] = 'SIS';
                }
            }

            $data = \common\models\hr\HrMsEmployee::find()->where($where)->all();
            foreach ($data as $child) {
                $results[$child->Id] = $child->Id.' - '.$child->Name;
            }
        }
        return $results;
    }

    public static function getSales()
    {
        $sales = [];
        $user = CommonHelper::getUserIndentity();
        Yii::$app->cache->flush();

        if (!empty($user->CompanyId)) {
            if(isset($param['withAll'])){
                $sales[0] = 'SELECT All';
            }

            $where = array('CompanyCode' => $user->CompanyId);
            if(!empty($param['BranchCode'])){
                $where = array_merge($where, array('BranchCode' => $param['BranchCode']));
            }
            $data = \common\models\hr\VHrMstSales::find()->where($where)->all();
            foreach ($data as $child) {
                $sales[$child->EmployeeID] = $child->EmployeeID.' - '.$child->EmployeeName;
            }
        }

        return $sales;
    }

    public static function getParent($id){
        $parent = Employee::find()->where(['guid' => $id])->one();
        return $parent;
    }
    public static function getEmployee($id){
        $parent = Employee::find()->where(['user_id' => $id])->one();
        return $parent;
    }
}
