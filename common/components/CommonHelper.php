<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use common\models\cm\CmCompanyBranch;

/****** common HELPER :
 * fungsi-fungsi yang biasa dipakai / dibutuhkan di semua module buatan si AA
 * "buat hidup lebih mudah"
 *
 * ada beberapa initial, yaitu :
 * - get* => return berupa value (berbagai type),
 * - set* => return berupa value juga tapi dikhususkan untuk ganti value,contoh: $a = setValue(123);
 * - is* => return berupa boolean (1/0), contoh: if(isEmpty($a)) ? ...
 * - js => return berupa function js
 * *mau buat baru ? ikutin initial ini biar jadi standar !
 *
 * PENTING !!!!
 * Jangan sebarang hapus/ganti fungsi dibawah jika tidak tau apa yang dilakukan !!!!
 * banyak module yang menggunakan helper ini jika mau ubah sesuatu disarankan buat fungsi baru !
 ***********************/

class CommonHelper
{

    public static function getVersion()
    {
        $commitHash = trim(exec('git describe --tags'));

        $commitDate = new \DateTime(trim(exec('git log -n1 --pretty=%ci HEAD')));
        $commitDate->setTimezone(new \DateTimeZone('UTC'));

        return sprintf('ver. %s', $commitHash);
    }
    public static function CreateLeftMenuBasedOnMDM($menus, $html_menu, $child, $parent, $id_menu, $module = '')
    { /*custom sendiri, field data diubah jadi json didb dan di sini juga */
        if (!empty($menus)) {
            if ($child == 1 && $parent == 1) {
                $html_menu .= '<ul class="menu-items">';
            } else {
                $html_menu .= '<ul class="sub-menu">';
            }
            $i = 1;
            foreach ($menus as $menu) {
                if (!isset($menu['data']['module']) or ($menu['data']['module'] == '' or $menu['data']['module'] == $module)) {
                    $url = '/' . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
                    $active = ($menu['url'][0] == $url) ? 'current' : '';
                    $first = ($parent == 1 && $i == 1) ? 'm-t-30' : '';
                    $html_menu .= '<li class="' . $first . ' ' . $active . '">';

                    $arrow = '';
                    if (!empty($menu['items'])) {
                        $arrow = '<span class=" arrow"></span>';
                    }
                    $menu['label'] = '<span class="title">' . $menu['label'] . '</span>';
                    $html_menu .= Html::a(
                        $menu['label']
                            . $arrow,
                        !empty($menu['items']) ? 'javascript:;' : [$menu['url']],
                        []
                    );
                    if (!empty($menu['data']['icon'])) {
                        $html_menu .= '<span class="icon-thumbnail">' . $menu['data']['icon'] . '</span>';
                    }

                    if (!empty($menu['items'])) {
                        if ($child == 1) {
                            $nol = 0;
                            $_id_menu = $id_menu . $nol;
                        }
                        $html_menu .= self::CreateLeftMenuBasedOnMDM($menu['items'], '', 1, 0, '', $module);
                    }

                    $html_menu .= '</li>';
                    ++$id_menu;
                    ++$i;
                }
            }
            if ($child == 1) {
                $html_menu .= '</ul>';
            }
        }

        return $html_menu;
    }

    public static function formatBytes($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
    public static function ResetMail($str)
    {
        $rmv = array('_');
        $str = str_replace($rmv, '', $str);

        return $str;
    }
    public static function ResetPhoneOrFax($str)
    {
        $rmv = array(' ', '_', '(', ')');
        $str = str_replace($rmv, '', $str);

        return $str;
    }
    public static function ResetDecimal($str)
    {
        $rmv = array(',', '_');
        $str = str_replace($rmv, '', $str);

        return $str;
    }

    public static function getMonths($param = "")
    {
        $months = array();
        if (isset($param['withAll'])) {
            $months[-1] = 'All';
        }

        for ($m = 1; $m <= 12; $m++) {
            $date = "2000-{$m}-01";
            $months[$m] = STRTOUPPER(date('M', strtotime($date)));
        }
        return $months;
    }

    public static function setDefaultValueIfEmpty($val = '', $var_type = 'str')
    {
        if (empty($val)) {
            switch ($var_type) {
                case 'string':
                    $val = '(not set)';
                    break;

                case 'int':
                    $val = 0;
                    break;

                default:
                    $val = null;
                    break;
            }
        }

        return $val;
    }

    public static function getUserIndentity($_id = '')
    {
        $session = Yii::$app->session;

        return $session['__user'];
    }

    public static function getSessionIdUser()
    {
        if (!empty(Yii::$app->user->identity->id)) {
            $_id = Yii::$app->user->identity->id;

            return $_id;
        } else {
            Yii::$app->user->logout();

            //return $this->redirect(['login']);
        }
    }

    /*
    public static function getUserAssignedOffice($param = '')
    {
        $branch = [];
        $user = Self::getUserIndentity();
        Yii::$app->cache->flush();

        if (!empty($user->BranchId) && !empty($user->CompanyId)) {
            $where = ['CompanyId' => $user->CompanyId, 'Id' => $user->BranchId];
            if ($user->branch->isHolding) {
                $where = ['CompanyId' => $user->CompanyId];

                if(isset($param['selectall'])){
                    if($param['selectall'] == true){
                        $branch[0] = 'SELECT ALL';
                    }
                }
            }
            $data = CmCompanyBranch::find()->where($where)->all();
            foreach ($data as $child) {
                $branch[$child->Id] = $child->Id.' - '.$child->Name;
            }
        }

        return $branch;
    }
    public static function getUserAssignedWarehouse($param = '')
    {
        $branch = [];
        $user = Self::getUserIndentity();
        Yii::$app->cache->flush();

        if (!empty($user->BranchId) && !empty($user->CompanyId)) {
            $where = ['BranchCode' => $user->BranchId];
            if ($user->branch->isHolding) {
                $where = [];

                if(isset($param['selectall'])){
                    if($param['selectall'] == true){
                        $branch[0] = 'SELECT ALL';
                    }
                }

            }
            if(isset($param['withsis'])){
                if($param['withsis'] == true){
                    $branch['SIS'] = 'SIS';
                }
            }
            $data = \common\models\sdms\view\vSalesMsWarehouse::find()->where($where)->all();
            foreach ($data as $child) {
                $branch[$child->Id] = $child->Id.' - '.$child->Warehouse;
            }
        }

        return $branch;
    }

    public static function getWarehouse($param = '')
    {
        $user = Self::getUserIndentity();
        Yii::$app->cache->flush();

        $data = \common\models\sdms\view\vSalesMsWarehouse::find();
        $data->where(['CompanyCode' => $user->CompanyId]);
        if(!empty($param['BranchCode'])){
            $data->andwhere(['BranchCode' => $param['BranchCode']]);
        }
        if(!empty($param['exceptBranchCode'])){
            $data->andwhere('BranchCode != :exceptBranchCode', ['exceptBranchCode' => $param['exceptBranchCode']]);
        }
        if(!empty($param['exceptWarehouse'])){
            $data->andwhere('Id != :exceptWarehouse', ['exceptWarehouse' => $param['exceptWarehouse']]);
        }
        $data = $data->all();

        $branch = [];
        if(isset($param['withsis'])){
            if($param['withsis'] == true){
                $branch['SIS'] = 'SIS';
            }
        }
        if(!empty($data)){
            foreach ($data as $child) {
                $branch[$child->Id] = $child->Id.' - '.$child->Warehouse;
            }
        }

        return $branch;
    }
    public static function getUserAssignedCoaDept($param = '')
    {
        $branch = [];
        $user = Self::getUserIndentity();
        Yii::$app->cache->flush();

        if (!empty($user->BranchId) && !empty($user->CompanyId)) {
			if($user->canChangeDept == 1){
				$where = ['CompanyId' => $user->CompanyId, 'BranchId' => $user->BranchId];
				if(isset($param['selectall'])){
                    if($param['selectall'] == true){
                        $branch[0] = 'SELECT ALL';
                    }
                }
			}else{
				$where = ['CompanyId' => $user->CompanyId, 'BranchId' => $user->BranchId, 'DeptId' => $user->DeptId];
			}
            // $data = \common\models\fn\view\vFnBdgtCtrlDeptActive::find()->where($where)->all();
            // foreach ($data as $child) {
            //     $branch[$child->DeptId] = $child->Departement;
            // }
        }

        return $branch;
    }
    public static function getAllBranch($param = '')
    {
        $branch = [];
        $user = Self::getUserIndentity();
        Yii::$app->cache->flush();

        if (!empty($user->CompanyId)) {
            if(isset($param['withAll'])){
                $branch[0] = 'SELECT All';
            }

            $where = ['CompanyId' => $user->CompanyId];
            $data = CmCompanyBranch::find()->where($where)->all();
            foreach ($data as $child) {
                $branch[$child->Id] = $child->Id.' - '.$child->Name;
            }
        }

        return $branch;
    }
        */

    public static function jsClock()
    {
        $js = '
            function updateClock(id)
            {
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var d = new Date( );
                var currentHours = d.getHours( );
                var currentMinutes = d.getMinutes( );
                var currentSeconds = d.getSeconds( );

                var curr_date = d.getDate();
				if(curr_date < 10) curr_date = "0" + curr_date;
                var curr_month = months[d.getMonth()];
                var curr_year = d.getFullYear();

                // Pad the minutes and seconds with leading zeros, if required
                currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
                currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;


                // Compose the string for display
                var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + "<br>" + curr_date + " " + curr_month + " " + curr_year;
                $("#"+id).html(currentTimeString);
            }';

        return $js;
    }

    // kamal 16 Mar 2021
    public static function db_trigger_error($ierr_msg)
    {
        $res_db_trigger_error = $ierr_msg;
        if (strpos($res_db_trigger_error, "___") != 0) {
            $res_db_trigger_error = explode("___", $res_db_trigger_error)[1];
        }
        return $res_db_trigger_error;
    }

    public static function db_field_error($ierr_msg)
    {
        $verr_msg = $ierr_msg;
        $start = stripos($verr_msg, "<li>") + 4;
        $end = stripos($verr_msg, "</li>");
        $length = $end - $start;

        $res_db_field_error = str_replace("&quot;", '"', substr($verr_msg, $start, $length));

        return $res_db_field_error;
    }
}
