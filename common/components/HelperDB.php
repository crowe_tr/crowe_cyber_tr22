<?php

namespace common\components;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\filters\VerbFilter;

class HelperDB
{
    public static function result_default(){
        $result = [
            'model' => [],
            'state' => [
                'status' => false,
                'message' => 'Error : default'
            ]
        ];

        return $result;
    }
    public static function result_success(){
        $result = [
            'model' => [],
            'state' => [
                'status' => true,
                'message' => 'Success'
            ]
        ];

        return $result;
    }
    public static function result_noloaded($model){
        $result = [
            'model' => $model,
            'state' => [
                'status' => false,
                'message' => 'Error : Model not loaded'
            ]
        ];

        return $result;
    }
    public static function saveAjax($model, $data)
    {
        $res = static::loadsave($model, $data);
        return json_encode($res['state']);
    }

    public static function loadsave($model, $data)
    {
        $result = static::result_noloaded($model);
        if ($model->load($data)) {
            $result = static::save($model);
        }

        return $result;
    }

    public static function save($model, $validation=true)
    {
        $result = static::result_default();
        try {
            if ($model->save($validation)) {
                $result['state']['status'] = true;
                $result['state']['message'] = 'success';
            } else {
                $result['state']['status'] = false;
                $result['state']['message'] = static::db_field_error(Html::errorSummary($model));
            }
        } catch (\yii\db\Exception $e) {
            $result['state']['status'] = false;
            $result['state']['message'] = static::db_trigger_error($e->getMessage());
        }

        return $result;
    }




    // kamal 16 Mar 2021
    public static function db_trigger_error($ierr_msg)
    {
        $res_db_trigger_error = $ierr_msg;
        if (strpos($res_db_trigger_error, "___") != 0) {
            $res_db_trigger_error = explode("___", $res_db_trigger_error)[1];
        }
        return $res_db_trigger_error;
    }

    public static function db_field_error($ierr_msg)
    {
        $verr_msg = $ierr_msg;
        $start = stripos($verr_msg, "<li>") + 4;
        $end = stripos($verr_msg, "</li>");
        $length = $end - $start;

        $res_db_field_error = str_replace("&quot;", '"', substr($verr_msg, $start, $length));

        return $res_db_field_error;
    }

    public static function set_behaviors()
    {
        $data = [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];

        return $data;
    }

    public static function set_index($iform, $imodel)
    {
        $searchModel = $imodel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data =  $iform->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        return $data;
    }
}
