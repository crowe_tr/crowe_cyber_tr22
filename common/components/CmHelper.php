<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\switchery\Switchery;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\widgets\MaskedInput;


class CmHelper
{
  public static function set_title($this_form, $title1, $title2 = null){
    $this_form->title = $title1;
    $this_form->params['breadcrumbs'][] = $this_form->title;
    $this_form->params['breadcrumbs'][] = $title2;
    $this_form->params['breadcrumbs_btn'] = Html::a(
                                                      '<i class="pg-plus"></i> <span class="hidden-xs">CREATE NEW</span>',
                                                      false,
                                                      [
                                                        'onclick' => 'FormCreate()',
                                                        'class' => 'btn btn-warning text-white ',
                                                      ]
                                                    );
  }

  public static function title_only($this_form, $title1)
  {
    $this_form->title = $title1;
    $this_form->params['breadcrumbs'][] = $this_form->title;
  }

  public static function set_column($iattribute, $ilabel, $icol_sm){
    return  [
              'attribute' => $iattribute,
              'label' => $ilabel,
              'headerOptions' => ['class' => 'col-sm-'.$icol_sm.' bg-success'],
              'filterOptions' => ['class' => 'b-b b-grey'],
              'contentOptions' => ['class' => 'kv-align-middle'],
            ];
  }

  public static function set_column_search($iattribute, $ilabel, $icol_sm, $ifilter, $ivalue){
    return  [
              'attribute' => $iattribute,
              'label' => $ilabel,
              'headerOptions' => ['class' => 'col-sm-'.$icol_sm.' bg-success'],
              'filterOptions' => ['class' => 'b-b b-grey'],
              'contentOptions' => ['class' => 'kv-align-middle'],
              'filterType' => GridView::FILTER_SELECT2,
              'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
              ],
              'filterInputOptions' => ['placeholder' => ''],
              'format' => 'raw',
              'filter' => $ifilter,
              'value'=> $ivalue,
            ];
  }

  public static function set_column_bool($iattribute, $icol_sm, $iyes, $ino, $ivalue)
  {
    return
      [
          'attribute' => $iattribute,
          'headerOptions' => ['class' => 'col-sm-'.$icol_sm.' bg-success'],
          'filterOptions' => ['class' => 'b-b b-grey'],
          'contentOptions' => ['class' => 'kv-align-middle'],
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
              'pluginOptions' => ['allowClear' => true],
          ],
          'filterInputOptions' => ['placeholder' => ''],
          'contentOptions' => ['class' => 'kv-align-middle'],
          'filterInputOptions' => ['placeholder' => ''],
          'format' => 'raw',
          'filter' => [1 => $iyes , 0 => $ino ],
          'value' => $ivalue,
      ];
  }

  public static function set_column_bool2($iattribute, $icol_sm, $ifilter, $ivalue)
  {
    return
      [
          'attribute' => $iattribute,
          'headerOptions' => ['class' => 'col-sm-'.$icol_sm.' bg-success'],
          'filterOptions' => ['class' => 'b-b b-grey'],
          'contentOptions' => ['class' => 'kv-align-middle'],
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
              'pluginOptions' => ['allowClear' => true],
          ],
          'filterInputOptions' => ['placeholder' => ''],
          'contentOptions' => ['class' => 'kv-align-middle'],
          'filterInputOptions' => ['placeholder' => ''],
          'format' => 'raw',
          'filter' => $ifilter,
          'value' => $ivalue,
      ];
  }

  public static function set_switch($icol, $imain_form, $imodel, $iattribute)
  {
    $data = ' <div class="col-sm-'.$icol.'">'.
                $imain_form->field($imodel, $iattribute,
                ['options' => ['class' => ' form-group form-group-default']]
                )->widget(
                Switchery::className(),
                ['options' => []])->label(false)
            .' </div>';
    return $data;
  }

  public static function set_field_date($icol, $imain_form, $imodel, $iattribute)
  {
    $data = '<div class="col-md-'.$icol.'">'.
                $imain_form->field($imodel, $iattribute, [
                                        'template' => '{label}{input}', 'options' => ['onchange' => 'ReqSave()', 'class' => 'form-group form-group-default input-group'],
                                      ]
                                  )->widget(DatePicker::classname(), [
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => date('Y-m-d'),
                                    'pluginOptions' => [
                                      'autoclose' => true,
                                      'format' => 'yyyy-mm-dd',
                                    ],
                                  ])
            .'</div>';
    return $data;
  }

  public static function set_field_num($icol, $imain_form, $imodel, $iattribute)
  {
    $data = '<div class="col-md-'.$icol.'">'.
                $imain_form->field($imodel, $iattribute, ['options' => ['onkeyup' => 'counttotal()']])->widget(MaskedInput::className(), [
                    								'clientOptions' => [
                    									'alias' => 'decimal',
                    									'groupSeparator' => ',',
                    									'autoGroup' => true,
                    								],
                    							])
            .'</div>';
    return $data;
  }

  public static function set_field_bool($icol, $imain_form, $imodel, $iattribute, $iinit)
  {
    if ($iinit == 1) {
      $vyes = 'Yes';
      $vno = 'No';
    } elseif ($iinit == 2) {
      $vyes = 'Active';
      $vno = 'Non Active';
    }

    $data = '<div class="col-md-'.$icol.'">'.
                $imain_form->field($imodel, $iattribute, ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                Select2::classname(),
                                [
                                        'data' => [1=>$vyes, 0=>$vno],
                                        'options' => ['id' => 'type', 'placeholder' => 'Select ...'],
                                ]
                              )
            .'</div>';
    return $data;
  }

  public static function set_column_no(){
    return  [
              'class' => 'kartik\grid\SerialColumn',
              'header' => 'NO',
              'mergeHeader' => false,
              'headerOptions' => ['class' => 'bg-success b-r'],
              'contentOptions' => ['class' => 'text-right b-r'],
              'filterOptions' => ['class' => 'b-b b-grey b-r'],
              'width' => '36px',
            ];
  }

  public static function set_icon($iid, $ifolder, $iform){
    $cm_form = $ifolder.'/'.$iform.'/form';

    $val = '<div class="tooltip-action">
              <div class="trigger">
                '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
              </div>
              <div class="action-mask">
                <div class="action">
                  '.Html::a(
                    '<i class="fa fa-edit"></i>',
                    false,
                    [
                        'onclick' => "FormUpdate('".Yii::$app->urlManager->createAbsoluteUrl([$cm_form, 'id' => $iid])."')",
                        'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                        'style' => 'bjob-radius: 5px !important',
                    ]
                  ).'
                '.Html::a('<i class="pg-trash"></i>',
                                        ['delete', 'id' => $iid],
                                        [
                                          'data-method' => 'post',
                                          'data-confirm' => 'Are you sure to delete this item?',
                                          'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                        ]).'
                </div>
              </div>
            </div>';
    return $val;
  }

  public static function set_icon2($iid, $ifolder, $iform){
    $cm_form = $ifolder.'/'.$iform.'/form';

    $val = '<div class="tooltip-action">
              <div class="trigger">
                '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
              </div>
              <div class="action-mask">
                <div class="action">
                  '.Html::a(
                    '<i class="fa fa-edit"></i>',
                    false,
                    [
                        'onclick' => "FormUpdate('".Yii::$app->urlManager->createAbsoluteUrl([$cm_form, 'id' => $iid])."')",
                        'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                        'style' => 'bjob-radius: 5px !important',
                    ]
                  ).'
                  '.Html::a('<i class="pg-trash"></i>',
                                          false,
                                          [
                                              'onclick' => "FormDelete('".Yii::$app->urlManager->createUrl([Yii::$app->request->getPathInfo()."/delete", 'id' => $iid])."')",
                                              'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                          ]).'
                </div>
              </div>
            </div>';
    return $val;
  }

  public static function set_icon3($iid, $ifolder, $iform){
    $cm_form = $ifolder.'/'.$iform.'/form';

    $val = '<div class="tooltip-action">
              <div class="trigger">
                '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
              </div>
              <div class="action-mask">
                <div class="action">
                  '.Html::a(
                    '<i class="fa fa-edit"></i>',
                    false,
                    [
                        'onclick' => "FormUpdate('".Yii::$app->urlManager->createAbsoluteUrl([$cm_form, 'id' => $iid])."')",
                        'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                        'style' => 'bjob-radius: 5px !important',
                    ]
                  ).'
                </div>
              </div>
            </div>';
    return $val;
  }

  public static function widget_pjax(){
    $data = '
              <div class="modal fade stick-up " id="crud-modal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-lg ">
                  <div class="modal-content no-border">
                    <div class="modal-header bg-success text-white">
                    </div>
                    <div id="detail" class="modal-body padding-20">
                    </div>
                  </div>
                </div>
              </div>
            ';

    return $data;
  }

  public static function widget_pjax2(){
    $data = '
              <div class="modal" id="crud-modal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-xlg ">
                  <div class="modal-content no-border  modal-xlg ">
                      <div class="modal-header bg-success text-white">
                      </div>
                      <div id="detail" class="modal-body padding-20">
                      </div>
                  </div>
                </div>
              </div>
            ';

    return $data;
  }

  public static function grid_view($idataProvider, $isearchModel, $icolumn){
    $data = [
              'dataProvider' => $idataProvider,
              'filterModel' => $isearchModel,
              'columns' => $icolumn,
              'layout'=>'
                <div class="card card-default">
                  <div class="row ">
                    <div class="col-md-12">
                      {items}
                    </div>
                  </div>
                  <div class="row padding-10">
                    <div class="col-md-4">{summary}</div>
                    <div class="col-md-8">{pager}</div>
                  </div>
                </div>
                  ',
              'emptyText' => '
                      <div class="text-center" style="padding: 2em 0">
                        <i class="fa fa-exclamation-circle fa-5x text-warning"></i>
                        <br>
                        <br>
                        '.Yii::t('backend', 'You do not have any data within your Filters.').'
                        <br>
                        '.Yii::t('backend', 'To create a new data, click <b><i class="icon-plus-circle"></i> ADD NEW</b> on the right top of corner').'
                    </div>',



              'resizableColumns' => true,
              'bordered' => false,
              'striped' => false,
              'condensed' => false,
              'responsive' => false,
              'hover' => true,
              'persistResize' => false,
            ];

    return $data;
  }

  public static function grid_view2($idataProvider, $isearchModel, $icolumn){
    $data = [
              'dataProvider' => $idataProvider,
              'filterModel' => $isearchModel,
              'columns' => $icolumn,
              'layout'=>'
                  <div class="card card-default">
                      <div class="row ">
                      <div class="col-md-12">
                          {items}
                      </div>
                      </div>
                      <div class="row padding-10">
                      <div class="col-md-4">{summary}</div>
                      <div class="col-md-8">{pager}</div>
                      </div>
                  </div>
                  ',
              'resizableColumns' => true,
              'bordered' => false,
              'striped' => true,
              'condensed' => false,
              'responsive' => false,
              'hover' => true,
              'persistResize' => false,
            ];

    return $data;
  }

  public static function set_script($iform){
    $rform = '/'.'cm/'.$iform.'/form';
    $data = "
              var paramJs = (paramJs || {});
              paramJs.urlFormShowModal = '". Yii::$app->urlManager->createAbsoluteUrl([$rform]) ."';

              window.closeModal = function(){
                reload();
              };

            	function FormShowModal(link='') {
            		var link = (link || paramJs.urlFormShowModal);
            		$.ajax({
            			url: link,
            			data:  $('#crud-form').serialize(),
            			method: 'POST',
            			dataType: 'html',
            			success: function(data) {
            				$('#detail').html(data);
            			},
            		});
            	}

            	function FormCreate() {
            		FormShowModal();
            		$('#crud-modal').modal('show');
            	}
            	function FormUpdate(link) {
            		FormShowModal(link);
            		$('#crud-modal').modal('show');
            	}

            	function reload(){
            		$.pjax.defaults.timeout = false;
            		$.pjax.reload({
            			container: '#crud-pjax'
            		})
            		$('#crud-form').modal('hide');
            		$('#crud-form').data('modal', null);
                $('.modal-backdrop').modal('hide');
                $( '.modal-backdrop' ).removeClass( 'show' ).addClass( 'hide' );
            	}
            ";

    return $data;
  }

  public static function set_error($imodel)
  {
    return 'error : validation not valid : '.strip_tags(Html::errorSummary($imodel, ['encode' => false]));
  }

  public static function set_active_form()
  {
    return ActiveForm::begin([
      'validateOnChange' => true,
                              'id' => 'crud-form', //perhatiin
                              'enableClientValidation' => true,
                              'validateOnSubmit' => true,
                              'validateOnType' => true,
                              'fieldConfig' => [
                                  'template' => '{label}{input}',
                                  'options' => [
                                      'class' => 'form-group form-group-default',
                                  ],
                              ],
                              'errorSummaryCssClass'=> 'alert alert-danger'
                            ]);
  }

  public static function set_text_input($icol, $iform, $imodel, $ifield)
  {
    $data = "<div class='col-md-".$icol."'>"
              .$iform->field($imodel, $ifield)->textInput(['maxlength' => true]).
            "</div>";

    return $data;
  }

  public static function set_phone_input($icol, $iform, $imodel, $ifield)
  {
    $data = "<div class='col-md-".$icol."'>"
              .$iform->field($imodel, $ifield)->widget(MaskedInput::className(), ['mask' => '(+99) 999 999 999 9999']).
            "</div>";

    return $data;
  }

  public static function set_label_text_input($icol, $iform, $imodel, $ifield, $ilabel)
  {
    $data = "<div class='col-md-".$icol."'>"
              .$iform->field($imodel, $ifield)->label($ilabel)->textInput(['maxlength' => true]).
            "</div>";

    return $data;
  }

  public static function set_checkbox_input($icol, $iform, $imodel, $ifield)
  {
    $data = "<div class='col-md-".$icol."'>"
              .$iform->field($imodel, $ifield)->checkbox()->label('').
            "</div>";

    return $data;
  }

  public static function set_lookup_input($icol, $ivalue)
  {
    $data = "<div class='col-md-".$icol."'>"
              .$ivalue.
            "</div>";

    return $data;
  }

  public static function set_button_input()
  {
    $data = '<div class="row">
              <div class="col-md-12 text-right m-t-5">
                <hr class="m-b-5"/>'
                .Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']).
                '<button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
              </div>
            </div>';

    return $data;
  }

  public static function row_group_header()
  {
    return '<div class="form-group-attached">
              <div class="row">';
  }

  public static function row_group_footer()
  {
    return '  </div>
            </div>
            <br/>';
  }

  public static function row_foot_head()
  {
    return '</div>
            <div class="row">';
  }

  public static function row_empty12()
  {
    return '<div class="row">
                <div id="error-summary" class="col-md-12 ">

                </div>
            </div>';
  }

  public static function script_input($iurl, $iid, $iform)
  {
    $rform = 'cm/'.$iurl.'/save';
    $data = " paramJs.urlFormSave = '" . Yii::$app->urlManager->createAbsoluteUrl([$rform, 'id'=>$iid]) . "';
              $('#crud-form').on('beforeSubmit', function() {

                var " . $iform . " = new FormData($('#crud-form')[0]);
                $.ajax({
                  url: paramJs.urlFormSave,
                  type: 'POST',
                  data: " . $iform . ",
                  async: false,
                  cache: false,
                  contentType: false,
                  processData: false,

                  success: function (data) {
                    if(data != 1){
                      alert(data);
                    }else{
                      $('#crud-modal').modal('hide');
                      reload();
                    }
                  },
                  error: function(jqXHR, errMsg) {
                    alert(errMsg);
                  }
                });
                return false;
              });";

    return $data;
  }

  public static function script_input2($iform)
  {
    $data = "$('#crud-form').on('beforeSubmit', function() {
              var " . $iform . " = new FormData($('#crud-form')[0]);
              $.ajax({
                url: paramJs.urlFormSave,
                type: 'POST',
                data: " . $iform . ",
                async: false,
                cache: false,
                contentType: false,
                processData: false,

                success: function (data) {
                  if(data != 1){
                    $('#error-summary').html('<div class=".'""alert alert-danger""'.">' + data + '</div>');
                  }else{
                    $('#crud-modal').modal('hide');
                    reload();
                  }
                },
                error: function(jqXHR, errMsg) {
                  alert(errMsg);
                }
              });
              return false;
            });";
    return $data;
  }

  public static function scr_reload()
  {
    return  "function reload() {
              $.pjax.defaults.timeout = false;
              $.pjax.reload({
                container: '#crud-pjax'
              })
              $('#ChildModal').modal('hide');
              $('#ChildModal').data('modal', null);
            }";
  }

  public static function scr_close()
  {
    return  "window.closeModal = function() {
              reload();
              hideFullLoading();
            };";
  }

}
