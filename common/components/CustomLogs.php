<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use common\components\CommonHelper;
use common\models\Logs;

class CustomLogs extends \yii\base\Component{
    public function init() {
        $logs = new Logs();
        $logs->ip = Self::get_client_ip();
        $logs->username = empty(Yii::$app->user->identity->Username) ? "_notlogin" : Yii::$app->user->identity->Username;
        $logs->action = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        
        $logs->save(false);
        parent::init();
    }

    public static function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}