<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use yii\web\JsExpression;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use common\models\cl\Client;
use common\models\hr\Employee;
use common\models\cl\viewClientZone;


class JobHelper
{

  public static function set_title_txt($this_form, $title1, $title2 = null)
  {
    $this_form->params['breadcrumbs'][] = $title1;
    $this_form->params['breadcrumbs'][] = $title2;
  }

  public static function set_title_btn($this_form, $click_function)
  {
    $this_form->params['breadcrumbs_btn'] = Html::a(
      '<i class="pg-plus"></i> <span class="hidden-xs">CREATE NEW</span>',
      false,
      [
        'onclick' => $click_function,
        'class' => 'btn btn-warning text-white ',
      ]
    );
  }

  public static function set_active_form($iform_id, $imethod)
  {
    return  [
      'id' => $iform_id,
      'method' => $imethod,
      'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
          'class' => 'form-group form-group-default',
        ],
      ],
    ];
  }

  public static function set_search_box($iparam)
  {
    return '<div class="card m-t-10 m-b-5" style="background: #f2f2f2">
        	    <div class="card-body">
        	        <div class="form-group-attached">
        	            <div class="row">
        							    <div class="col-sm-3">'
      . $iparam .
      '</div>
          	            </div>
          	        </div>
          	    </div>
          	</div>';
  }

  public static function set_empty_text()
  {
    return '
      <div class="card panel-default no-margin">
        <div class="card-body no-padding text-center padding-20">
          <i class="fa fa-exclamation-circle fa-5x text-warning"></i>
          <br>
           ' . Yii::t('backend', 'You do not have any Pending job within your Filters.') . '
          <br>
          <br>
        </div>
      </div>
      <div class="card pacardnel-default no-margin">
        <div class="card-body padding-20" style="border-top: 0">
          <div class="row">
            <div class="col-md-6 text-left sm-text-center">
            Showing 0 of 0 items.
            </div>
            <div class="col-md-6 text-right sm-text-center">
            </div>
          </div>
        </div>
      </div>
    ';
  }

  public static function set_layout($iform_id)
  {
    return '
      <form id="' . $iform_id . '">
        {items}
      </form>
      <div class="card card-default ">
        <div class="panel-body p-t-15 p-b-5 p-l-20 p-r-20" style="border-top: 0">
          <div class="row">
            <div class="col-md-6 text-left sm-text-center">
              {summary}
            </div>
            <div class="col-md-6 text-right sm-text-center">
              {pager}
            </div>
          </div>
        </div>
      </div>
    ';
  }

  public static function set_line()
  {
    return '
      <div class="col-sm-12">
          <hr class="no-margin">
      </div>
    ';
  }

  public static function set_label($icol_sm, $itext_align, $ititle, $ibody, $itag)
  {
    return '
      <div class="col-sm-' . $icol_sm . ' text-' . $itext_align . ' p-t-5 p-b-5">
          ' . $ititle . '
          <br>
          <' . $itag . ' class="bold no-padding no-margin">' . $ibody . '</' . $itag . '>
      </div>
    ';
  }

  public static function set_status($istatus)
  {
    return '
      <h3 class="bold fs-24">
          <span class="badge badge-default fs-10 text-black">' . $istatus . '</span>
      </h3>
    ';
  }

  public static function set_allowance($imeal, $itaxi, $iope)
  {
    return '
      <div class="card card-default m-t-10 m-b-5">
          <div class="card-body">
              <p><b>ALLOWANCE</b></p>
              <table class="table table-striped panel panel-default text-center" >
                  <thead>
                      <tr>
                          <td class="btn-success text-white">MEAL PROVIDED</td>
                          <td class="btn-success text-white">TRANSPORT PROVIDED</td>
                          <td class="btn-success text-white">Out Of Office Allowance</td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td class="text-center">
                              ' . $imeal . '
                          </td>
                          <td class="text-center">
                              ' . $itaxi . '
                          </td>
                          <td class="text-center">
                              ' . $iope . '
                          </td>
                      </tr>
                  </tbody>
              </table>
          </div>
      </div>
    ';
  }

  public static function set_empty_text_budgeting()
  {
    return '
      <div class="text-center" style="padding: 2em 0">
        <i class="fa fa-exclamation-circle fa-5x text-warning"></i>
        <br> '
      . Yii::t('backend', 'THERE IS NO ONE IN THIS TEAM. <br/>START REGISTER YOUR TEAM FOR THIS JOB ') . '
      </div>
    ';
  }

  public static function set_column_no()
  {
    return [
      'class' => 'kartik\grid\SerialColumn',
      'headerOptions' => ['class' => 'bg-warning text-white b-r'],
      'contentOptions' => ['class' => 'text-right'],
      'width' => '36px',
      'header' => 'NO',
    ];
  }

  public static function set_column($iattribute, $icol_sm, $itext_align_head = '', $itext_align_content = '', $ivalue = null)
  {
    if (!is_null($ivalue)) {
      $data = [
        'attribute' => $iattribute,
        'headerOptions' => ['class' => 'bg-success col-sm-' . $icol_sm . ' ' . $itext_align_head],
        'contentOptions' => ['class' => 'kv-align-middle ' . $itext_align_content],
        'value' => $ivalue
      ];
    } else {
      $data = [
        'attribute' => $iattribute,
        'headerOptions' => ['class' => 'bg-success col-sm-' . $icol_sm . ' ' . $itext_align_head],
        'contentOptions' => ['class' => 'kv-align-middle ' . $itext_align_content],
      ];
    }

    return $data;
  }

  public static function set_label_footer($icol_md, $ititle, $ibody, $itag)
  {
    return '
      <div class="col-md-' . $icol_md . '">
                    ' . $ititle . '
                    <br>
                    <' . $itag . ' class="bold no-padding no-margin">
                    ' . $ibody . '
                    </' . $itag . '>
      </div>
    ';
  }

  public static function set_table_footer($icap, $iestimated, $irealize)
  {
    return '
      <tr>
        <td>' . $icap . '</td>
        <td>
          <span class="pull-right">
            ' . $iestimated . '
          </span>
        </td>
        <td>
          <span class="pull-right">
            ' . $irealize . '
          </span>
        </td>
      </tr>
    ';
  }

  public static function widget_pjax($iid)
  {
    return '
      <div class="modal fade stick-up " id="' . $iid . '" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
      	<div class="modal-dialog modal-lg ">
      		<div class="modal-content no-border">
      			<div class="modal-header bg-success text-white">
      			</div>
      			<div id="detail" class="modal-body padding-20">
      			</div>
      		</div>
      	</div>
      </div>
    ';
  }

  public static function begin_form($iid)
  {
    return [
      'id' => $iid,
      'enableClientValidation' => true,
      'validateOnSubmit' => true,
      'validateOnChange' => true,
      'validateOnType' => true,
      'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
          'class' => 'form-group form-group-default',
        ],
      ],
      'errorSummaryCssClass' => 'alert alert-danger'
    ];
  }

  public static function set_label_form_header($icol1, $itext1, $icol2, $itext2, $icol3, $itext3, $istatus = '')
  {
    $data1 = $itext1 . ' ' . $itext2 . ' ' . $itext3;

    if ($istatus != '') {
      $data2 = ' <span class="badge badge-warning fs-13 text-black">' . $istatus . '</span>';
    } else {
      $data2 = '';
    }

    return $data1 . $data2 . "<br/>";
  }

  public static function set_date_form($icol, $iform, $imodel, $ifield, $ichange_function)
  {
    return '<div class="col-md-' . $icol . '">
      ' .
      $iform->field(
        $imodel,
        $ifield,
        [
          'template' => '{label}{input}', 'options' => ['class' => 'form-group form-group-default input-group', 'onchange' => $ichange_function],
        ]
      )->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_INPUT,
        'value' => date('Y-m-d'),
        'pluginOptions' => [
          'autoclose' => true,
          'format' => 'yyyy-mm-dd',
        ],
      ])
      . '
    </div>';
  }

  public static function set_area_form($icol, $iform, $imodel, $ifield, $iclient_area, $ichange_function)
  {
    $data = explode(',', $iclient_area);
    $cl = array();
    foreach ($data as $d) {
      $cl[$d] = $d;
    }
    return '
      <div class="col-md-' . $icol . '">
        ' .  $iform->field($imodel, $ifield)->widget(Select2::classname(), [
      'data' => $cl,
      'options' => ['placeholder' => 'Select Zone ...', 'multiple' => true, 'onchange' => $ichange_function, 'required'],
      'pluginOptions' => [
        'tokenSeparators' => [','],
      ],
    ]) . '
      </div>';
  }

  public static function set_switch_form($icol, $iform, $imodel, $ifield, $ichange_function = '')
  {
    return '
      <div class="col-md-' . $icol . '">
      ' .  $iform->field(
      $imodel,
      $ifield,
      ['options' => ['class' => ' form-group form-group-default', 'onchange' => $ichange_function]]
    )->widget(
      SwitchInput::className(),
      ['pluginOptions' => [
        'size' => 'small',
        'onText' => 'Yes',
        'offText' => 'No',
      ]]
    ) . '
      </div>
    ';
  }

  public static function set_text_form($icol, $iform, $imodel, $ifield, $ichange_function)
  {
    return '
      <div class="col-md-' . $icol . '">
        ' . $iform->field($imodel, $ifield, ['options' => ['onchange' => $ichange_function]])->textInput() . '
      </div>
    ';
  }

  public static function set_number_form($icol, $iform, $imodel, $ifield, $ievent, $ifunction)
  {
    return '
    <div class="col-md-' . $icol . '">
      ' . $iform->field($imodel, $ifield, ['options' => [$ievent => $ifunction]])->widget(MaskedInput::className(), [
      'clientOptions' => [
        'alias' => 'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true,
      ],
    ]) . '
    </div>
    ';
  }

  public static function set_combo_form_read($icol, $iform, $imodel, $ifield, $ichange_function, $idata)
  {
    return '
      <div class="col-md-' . $icol . '">
        ' . $iform->field($imodel, $ifield, ['options' => ['onchange' => $ichange_function, 'class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
      Select2::classname(),
      [
        'data' =>  $idata,
        'options' => ['id' => 'Class', 'placeholder' => 'Select ...', 'class' => 'readonly'],
      ]
    ) . '
      </div>
    ';
  }

  public static function set_text_form_read($icol, $iform, $imodel, $ifield)
  {
    return '
      <div class="col-md-' . $icol . '">
        ' . $iform->field($imodel, $ifield, ['options' => ['class' => ' form-group form-group-default']])->textInput(['readonly' => true, 'class' => "form-control readonly"]) . '
      </div>
    ';
  }

  public static function set_combo_allowance($icol, $iform, $imodel, $ifield, $iid, $ievent, $ifunction, $ijs_event, $ijs_id)
  {
    return '
      <div class="col-md-' . $icol . '">
        ' . $iform->field(
      $imodel,
      $ifield,
      ['options' => ['class' => ' form-group form-group-default', $ievent => $ifunction]]
    )->widget(
      SwitchInput::className(),
      [
        'options' => [
          'id' => $iid,
          'size' => 'small',
          'onText' => 'Yes',
          'offText' => 'No',
          'onchange'=>'
            if($(this).is(":checked")){
              $("#' . $ijs_id . '").prop("disabled", false);
            }else{
              $("#' . $ijs_id . '").prop("disabled", true);
            }
          '
        ],
      ]
    ) . '
      </div>
    ';
  }

  public static function set_budget_register($ititle, $ievent, $ifunction)
  {
    return
      Html::a(
        '<i class="pg-plus"></i> <span class="hidden-xs">' . $ititle . '</span>',
        false,
        [
          $ievent => $ifunction,
          'class' => 'btn btn-warning text-white btn-rounded p-t-10 p-b-10',
          'style' => 'position: absolute; top: -1.1em; right: 1.8em; border-radius: 20px !important',
        ]
      );
  }

  public static function set_icon_register($iicon_class, $ievent, $ifunction, $ibutton_class)
  {
    return
      Html::a(
        '<i class="' . $iicon_class . '"></i>',
        false,
        [
          $ievent => $ifunction,
          'class' => 'btn ' . $ibutton_class . ' text-white padding-5 p-l-10 p-r-10',
          'style' => 'bjob-radius: 5px !important',
        ]
      );
  }

  public static function set_icon_ellipse($iparam)
  {
    return '
      <div class="tooltip-action">
        <div class="trigger">
          ' . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']) . '
        </div>
        <div class="action-mask">
          <div class="action">
            ' . $iparam[0] . '
            ' . $iparam[1] . '
          </div>
        </div>
      </div>
    ';
  }

  public static function set_pic_project($icol, $imodel, $iemployee_id, $iform, $ifield, $ijob_id)
  {
    $employee_id = '';
    if (!$imodel->isNewRecord || !empty($iemployee_id)) {
      $data = Employee::findOne($iemployee_id);
      $employee_id = empty($data->fullName) ? $iemployee_id : $data->Id . ' - ' . $data->fullName . ' - ' . $data->level->levelName;
    }

    return '
      <div class="col-md-' . $icol . '">
        ' . $iform->field($imodel, $ifield, ['options' => ['class' => ' form-group form-group-default form-group-default-select2', 'onchange' => 'request_save()']])
      ->widget(Select2::classname(), [
        'initValueText' => $employee_id,
        'options' => ['placeholder' => 'Search ' . $ifield . ' ... '],
        'pluginOptions' => [
          'tags' => false,
          'ajax' => [
            'url' => Url::to(['helper/srcemployejob', 'id' => $ijob_id]),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term , JobID:$("#JobID").val()}; }'),
          ],
        ],
      ]) . '
      </div>
    ';
  }

  public static function set_footer_form($icap, $iestimated, $irealize)
  {
    return '
      <tr>
        <td>' . $icap . '</td>
        <td>
          <div class="row">
            <div class="col-md-8">
              <span class="pull-right text-black c-f-14 c-p-r-15">
                ' . $iestimated . '
              </span>
            </div>
          </div>
        </td>
        <td>
          <span class="pull-right" id="TimeChargesAct">
            ' . $irealize . '
          </span>
        </td>
      </tr>
    ';
  }

  public static function set_footer_form_allowance($icap, $iis_true, $iform, $imodel, $ifield, $iid, $irealize)
  {
    $disabled = false;
    if ($iis_true == 0) {
      $disabled = true;
    }

    return '
      <tr>
        <td>' . $icap . '</td>
        <td>
          <div class="row">
            <div class="col-md-8">
              ' . $iform->field($imodel, $ifield, ['options' => ['onchange' => 'request_save()', 'id' => $iid]])->widget(MaskedInput::className(), [
      'options' => ['disabled' => $disabled],
      'clientOptions' => [
        'alias' => 'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true,

      ],
    ])->label(false) . '
            </div>
          </div>
        </td>
        <td>
          <span class="pull-right" id="' . $iid . 'Act">
            ' . $irealize . '
          </span>
        </td>
      </tr>
    ';
  }

  public static function set_footer_form_expense($icap, $iform, $imodel, $ifield, $iid, $irealize)
  {
    return '
      <tr>
        <td>
          ' . $icap . '
        </td>
        <td>
          <div class="row">
            <div class="col-md-8">
              ' . $iform->field($imodel, $ifield, ['options' => ['onchange' => 'request_save()', 'id' => $iid]])->widget(MaskedInput::className(), [
      'options' => ['readonly'],
      'clientOptions' => [
        'alias' => 'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true,
      ],
    ])->label(false) . '
            </div>
          </div>
        </td>
        <td>
          <span class="pull-right" id="' . $iid . 'Act">
            ' . $irealize . '
          </span>
        </td>
      </tr>
    ';
  }

  public static function set_footer_form_recovery($icap, $iestimated, $irealize)
  {
    return '
      <tr>
        <td>' . $icap . '</td>
        <td>
          <div class="row">
            <div class="col-md-8">
              <span class="pull-right text-black data-r c-f-14 c-p-r-15" id="Percentage">
                ' . $iestimated . '
              </div>
            </div>
          </td>
          <td>
            <span class="pull-right" id="PercentageAct">
              ' . $irealize . '
            </span>
          </td>
      </tr>
    ';
  }
}
