<?php

namespace console\controllers;
use common\components\TimeReportHelper;

use Yii;
use yii\console\Controller;

class TrController extends Controller
{
  public function actionTes(){
    $this->stdout("Hello?\n");
  }

  /*
  public function actionManagerdaily(){
    $sql = "CALL antTrReminderApprovalManagerDaily()";
    $lookups = Yii::$app->db->createCommand($sql)->queryAll();

    $email = [];
    if(!empty($lookups)){
        foreach($lookups as $r) {
            if(!empty($r['Email']) && !empty($r['Id'])){
                if(filter_var($r['Email'], FILTER_VALIDATE_EMAIL)) {
                    $sql = "CALL antTrReminderApprovalManagerDailyDetail('{$r['Id']}')";
                    $data = Yii::$app->db->createCommand($sql)->queryAll();
                    $subject = 'Crowe TimeReport - Daily Manager Pending Approval Summary';
                    $this->Email (
                        $r['Email'], 
                        $subject,
                        [
                            'header'=>$r,
                            'data'=>$data,
                        ],
                        'layouts/reminderdailypendingapproval'
                    );
                }
            }
        }
    }
  }

  public function actionSupervisordaily(){
    $sql = "CALL antTrReminderApprovalSupervisorDaily()";
    $lookups = Yii::$app->db->createCommand($sql)->queryAll();

    $email = [];
    if(!empty($lookups)){
        foreach($lookups as $r) {
            if(!empty($r['Email']) && !empty($r['Id'])){
                if(filter_var($r['Email'], FILTER_VALIDATE_EMAIL)) {
                    $sql = "CALL antTrReminderApprovalSupervisorDailyDetail('{$r['Id']}')";
                    $data = Yii::$app->db->createCommand($sql)->queryAll();
                    $subject = 'Crowe TimeReport - Daily Supervisor Pending Approval Summary';
                    $this->Email (
                        $r['Email'], 
                        $subject,
                        [
                            'header'=>$r,
                            'data'=>$data,
                        ],
                        'layouts/reminderdailypendingapproval'
                    );
                    break;
                }
            }
        }
    }
  }
  */

//============
  public function Email($sendto, $subject, $param, $layouts='layouts/newapprovals'){
    Yii::$app->cache->flush();

    $from = Yii::$app->params['appNotificationEmail'];
    $cc = Yii::$app->params['appNotificationEmailCC'];

    $to = $sendto;
    
    if (!empty($to)) {
        $html = ['html' => $layouts];

        $compose = Yii::$app->mailer->compose($html, $param);
        $compose->setFrom($from);
        $compose->setTo($to);
        if (!empty($cc)) {
            $compose->setCc($cc);
        }
        $compose->setSubject($subject);

        $compose->send();
    }
  }

  public function actionMonthly_supervisor()
  {
      return TimeReportHelper::emailMonthlySupervisor();
  }
  public function actionMonthly_manager()
  {
      return TimeReportHelper::emailMonthlyManager();
  }

}





















