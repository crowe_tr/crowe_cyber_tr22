<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace console\controllers;

use yii;
use yii\console\Controller;

class MailController extends Controller
{
    public function actionSend()
    {
        Yii::$app->mailqueue->process();
    }
}
