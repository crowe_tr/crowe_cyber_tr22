

import axios from 'axios';
import Compressor from 'compressorjs';


function antplad(n) {
  var file = n.files[0];
  var parent = n.parentElement;
  var input = parent.children[2];
  var img = parent.children[3];
  var loading = parent.children[5];
  loading.style.display = "block";

  if (!file) {
    loading.style.display = "none";
    return;
  }

  new Compressor(file, {
    quality: 0.8,
    width: 700,

    success(result) {
      const formData = new FormData();
      formData.append('file', result, result.name);
      axios.post('/fm/upload', formData)
        .then(function (response) {
          var urlCreator = window.URL || window.webkitURL;
          var imageUrl = urlCreator.createObjectURL(result);
          img.src = imageUrl;

          input.value = response.data;
          loading.style.display = "none";
        })
        .catch(function (error) {
          loading.style.display = "none";
          console.log(error);
        });

    },
    error(err) {
      console.log(err.message);
    },
  });
}


window.onload = function () {

  /* Start Ant Upload */
  document.querySelectorAll('.antfile-input').forEach(item => {
    item.addEventListener('change', event => {
      var n = event.target;
      antplad(n);
    })
  })

  document.querySelectorAll('.antfile-preview').forEach(item => {
    item.addEventListener('click', event => {
      var n = event.target;
      var parent = n.parentElement;
      var file = parent.children[1];
      file.click();
    })
  })

  document.querySelectorAll('.antfile-remove').forEach(item => {
    item.addEventListener('click', event => {
      var n = event.target;
      var parent = n.parentElement;
      var input = parent.children[2];
      var img = parent.children[3];

      img.removeAttribute('src');
      input.value = null;
    })
  })
  /* End Ant Upload */

}