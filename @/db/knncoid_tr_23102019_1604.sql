/*
 Navicat Premium Data Transfer

 Source Server         : crowe
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : knn.co.id:3306
 Source Schema         : knncoid_tr

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 23/10/2019 16:04:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for AuditTrail
-- ----------------------------
DROP TABLE IF EXISTS `AuditTrail`;
CREATE TABLE `AuditTrail` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Source` varchar(3) DEFAULT NULL,
  `TransType` varchar(3) DEFAULT NULL,
  `TransactionType` varchar(128) DEFAULT NULL,
  `SourceNo` varchar(60) DEFAULT NULL,
  `TransID` int(11) unsigned DEFAULT NULL,
  `AuditDescription` text,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` varchar(128) DEFAULT NULL,
  `DateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedBy` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of AuditTrail
-- ----------------------------
BEGIN;
INSERT INTO `AuditTrail` VALUES (51, 'TR', 'JOB', 'Job Transaction', '001', 35, '  Description Changed : test lagi dongg  => test', 0, '2019-10-20 11:13:11', NULL, '2019-10-20 11:13:11', NULL);
INSERT INTO `AuditTrail` VALUES (52, 'TR', 'JOB', 'Job Transaction', '001', 35, '  Description Changed : test lagi dongg  => test', 0, '2019-10-20 11:15:26', NULL, '2019-10-20 11:15:26', NULL);
INSERT INTO `AuditTrail` VALUES (53, 'TR', 'JOB', 'Job Transaction', '001', 35, '  Description Changed : test lagi dongg  => test', 1, '2019-10-20 11:16:34', NULL, '2019-10-20 11:16:34', NULL);
INSERT INTO `AuditTrail` VALUES (54, 'TR', 'JOB', 'Job Transaction', 'ABI-0560-34', 35, '', 2, '2019-10-22 01:36:30', 'system', '2019-10-22 01:36:30', 'system');
INSERT INTO `AuditTrail` VALUES (55, 'TR', 'JOB', 'Job Transaction', 'ABI-0560-34', 35, '  Description Changed : test lagi dongg  => test', 1, '2019-10-22 01:38:55', 'system', '2019-10-22 01:38:55', 'system');
COMMIT;

-- ----------------------------
-- Table structure for AuditTrailDetail
-- ----------------------------
DROP TABLE IF EXISTS `AuditTrailDetail`;
CREATE TABLE `AuditTrailDetail` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AuditTrailID` int(11) unsigned NOT NULL,
  `Source` varchar(3) DEFAULT NULL,
  `TransType` varchar(3) DEFAULT NULL,
  `TransactionType` varchar(60) DEFAULT NULL,
  `TransID` int(11) unsigned DEFAULT NULL,
  `TransSeq` tinyint(2) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `AuditDescription` text,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` varchar(128) DEFAULT NULL,
  `DateModified` datetime DEFAULT CURRENT_TIMESTAMP,
  `ModifiedBy` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of AuditTrailDetail
-- ----------------------------
BEGIN;
INSERT INTO `AuditTrailDetail` VALUES (22, 51, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 1, 2, '', '2019-10-20 11:13:11', NULL, '2019-10-20 11:13:11', NULL);
INSERT INTO `AuditTrailDetail` VALUES (23, 51, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 2, 2, '', '2019-10-20 11:13:11', NULL, '2019-10-20 11:13:11', NULL);
INSERT INTO `AuditTrailDetail` VALUES (24, 52, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 1, 2, '', '2019-10-20 11:15:26', NULL, '2019-10-20 11:15:26', NULL);
INSERT INTO `AuditTrailDetail` VALUES (25, 52, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 2, 3, '  <Row Index : 2, Employee : 281140027 - WISNU ADI NUGROHO, Total Work Hours : 100.0, Overtimes : 0.0, Status : Delete>', '2019-10-20 11:15:26', NULL, '2019-10-20 11:15:26', NULL);
INSERT INTO `AuditTrailDetail` VALUES (26, 52, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 3, 1, '  <Row Index : 3, Employee : 281140027 - WISNU ADI NUGROHO, Total Work Hours : 100.0, Overtimes : 0.0, Status : Insert>', '2019-10-20 11:15:26', NULL, '2019-10-20 11:15:26', NULL);
INSERT INTO `AuditTrailDetail` VALUES (27, 53, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 1, 2, '  <Row Index : 1, Employee : 281130001 - DIMAS PRASETYO TERTIANTO, Total Work Hours : 100.0, Overtimes : 0.0, Status : Change>\n    Reporting Changed : 30 => 35\n    Total Work Hours Changed : 100 => 105', '2019-10-20 11:16:34', NULL, '2019-10-20 11:16:34', NULL);
INSERT INTO `AuditTrailDetail` VALUES (28, 53, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 2, 3, '  <Row Index : 2, Employee : 281140027 - WISNU ADI NUGROHO, Total Work Hours : 100.0, Overtimes : 0.0, Status : Delete>', '2019-10-20 11:16:34', NULL, '2019-10-20 11:16:34', NULL);
INSERT INTO `AuditTrailDetail` VALUES (29, 53, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 3, 1, '  <Row Index : 3, Employee : 281140027 - WISNU ADI NUGROHO, Total Work Hours : 100.0, Overtimes : 0.0, Status : Insert>', '2019-10-20 11:16:34', NULL, '2019-10-20 11:16:34', NULL);
INSERT INTO `AuditTrailDetail` VALUES (30, 54, 'TR', 'JOB', 'Job Transaction Budgeting', 35, NULL, NULL, '', '2019-10-22 01:36:30', 'system', '2019-10-22 01:36:30', 'system');
INSERT INTO `AuditTrailDetail` VALUES (31, 54, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 1, 3, '  <Row Index : 1, Employee : 281130001 - DIMAS PRASETYO TERTIANTO, Total Work Hours : 100.0, Overtimes : 10.0, Status : Delete>', '2019-10-22 01:36:30', 'system', '2019-10-22 01:36:30', 'system');
INSERT INTO `AuditTrailDetail` VALUES (32, 54, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 2, 3, '  <Row Index : 2, Employee : 281140027 - WISNU ADI NUGROHO, Total Work Hours : 100.0, Overtimes : 5.0, Status : Delete>', '2019-10-22 01:36:30', 'system', '2019-10-22 01:36:30', 'system');
INSERT INTO `AuditTrailDetail` VALUES (33, 54, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 3, 3, '  <Row Index : 3, Employee : 28115T025 - JOHANES ADRIANUS SEBASTIAN, Total Work Hours : 40.0, Overtimes : 0.0, Status : Delete>', '2019-10-22 01:36:30', 'system', '2019-10-22 01:36:30', 'system');
INSERT INTO `AuditTrailDetail` VALUES (34, 55, 'TR', 'JOB', 'Job Transaction Budgeting', 35, NULL, NULL, '', '2019-10-22 01:38:55', 'system', '2019-10-22 01:38:55', 'system');
INSERT INTO `AuditTrailDetail` VALUES (35, 55, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 1, 3, '  <Row Index : 1, Employee : 281130001 - DIMAS PRASETYO TERTIANTO, Total Work Hours : 100.0, Overtimes : 10.0, Status : Delete>', '2019-10-22 01:38:55', 'system', '2019-10-22 01:38:55', 'system');
INSERT INTO `AuditTrailDetail` VALUES (36, 55, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 2, 3, '  <Row Index : 2, Employee : 281140027 - WISNU ADI NUGROHO, Total Work Hours : 100.0, Overtimes : 5.0, Status : Delete>', '2019-10-22 01:38:55', 'system', '2019-10-22 01:38:55', 'system');
INSERT INTO `AuditTrailDetail` VALUES (37, 55, 'TR', 'JOB', 'Job Transaction Budgeting', 35, 3, 3, '  <Row Index : 3, Employee : 28115T025 - JOHANES ADRIANUS SEBASTIAN, Total Work Hours : 40.0, Overtimes : 0.0, Status : Delete>', '2019-10-22 01:38:55', 'system', '2019-10-22 01:38:55', 'system');
COMMIT;

-- ----------------------------
-- Table structure for auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`) USING BTREE,
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_assignment
-- ----------------------------
BEGIN;
INSERT INTO `auth_assignment` VALUES ('/*', '0', NULL);
INSERT INTO `auth_assignment` VALUES ('Clients', '281AA88874', 1569750875);
INSERT INTO `auth_assignment` VALUES ('Job', '00.19.01.001', 1569750806);
INSERT INTO `auth_assignment` VALUES ('Job', '281030013', 1571570151);
INSERT INTO `auth_assignment` VALUES ('Job', '281030014', 1571157380);
INSERT INTO `auth_assignment` VALUES ('Job', '281030015', 1570959245);
INSERT INTO `auth_assignment` VALUES ('Job', '281060004', 1571159783);
INSERT INTO `auth_assignment` VALUES ('Job', '281070006', 1571158515);
INSERT INTO `auth_assignment` VALUES ('Job', '281110008', 1571160456);
INSERT INTO `auth_assignment` VALUES ('Job', '281110014', 1571157252);
INSERT INTO `auth_assignment` VALUES ('Job', '281110037', 1570725879);
INSERT INTO `auth_assignment` VALUES ('Job', '281110043', 1571160661);
INSERT INTO `auth_assignment` VALUES ('Job', '281130001', 1571353194);
INSERT INTO `auth_assignment` VALUES ('Job', '281130029', 1571391487);
INSERT INTO `auth_assignment` VALUES ('Job', '281140027', 1570728532);
INSERT INTO `auth_assignment` VALUES ('Job', '28115T025', 1571595251);
INSERT INTO `auth_assignment` VALUES ('Job', '28117T069', 1571159866);
INSERT INTO `auth_assignment` VALUES ('Job', '281AA88874', 1569750875);
INSERT INTO `auth_assignment` VALUES ('JobApprovals', '281030013', 1571570151);
INSERT INTO `auth_assignment` VALUES ('JobApprovals', '281030014', 1571157380);
INSERT INTO `auth_assignment` VALUES ('JobApprovals', '281030015', 1570959245);
INSERT INTO `auth_assignment` VALUES ('JobApprovals', '281060004', 1571159783);
INSERT INTO `auth_assignment` VALUES ('JobApprovals', '281070006', 1571158515);
INSERT INTO `auth_assignment` VALUES ('Setup', '00.19.01.001', 1569750806);
INSERT INTO `auth_assignment` VALUES ('Setup', '00.19.01.002', 1569382892);
INSERT INTO `auth_assignment` VALUES ('Setup', '281AA88874', 1569750875);
INSERT INTO `auth_assignment` VALUES ('Setup', 'aasd', 1569383783);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281030013', 1571570151);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281030014', 1571157380);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281030015', 1570959245);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281060004', 1571159783);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281070006', 1571158515);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281110008', 1571160456);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281110014', 1571157253);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281110037', 1570725879);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281110043', 1571160661);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281110050', 1571158795);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281130001', 1571353194);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281130029', 1571391487);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281130030', 1571160130);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281140027', 1570728533);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28114T035', 1571387638);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28115T025', 1571595251);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28116T005', 1570967881);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28116T013', 1571392660);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28116T021', 1571159948);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28116T027', 1571160730);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28117T054', 1571159689);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28117T069', 1571159866);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28117T075', 1570966893);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28118T002', 1571160208);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28118T007', 1571157788);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28118T023', 1571160808);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28119T003', 1571160343);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '28119T005', 1571160281);
INSERT INTO `auth_assignment` VALUES ('TimeReport', '281AA88874', 1569750876);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281030013', 1571570151);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281030015', 1570959246);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281060004', 1571159783);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281070006', 1571158516);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281110008', 1571160456);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281110014', 1571157253);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281110043', 1571160662);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281110050', 1571158795);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281130001', 1571353194);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '281130030', 1571160130);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '28116T021', 1571159948);
INSERT INTO `auth_assignment` VALUES ('TimeReport Approvals', '28117T069', 1571159866);
COMMIT;

-- ----------------------------
-- Table structure for auth_item
-- ----------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  KEY `rule_name` (`rule_name`) USING BTREE,
  KEY `idx-auth_item-type` (`type`) USING BTREE,
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item
-- ----------------------------
BEGIN;
INSERT INTO `auth_item` VALUES ('/*', 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_item` VALUES ('/cl/*', 2, NULL, NULL, NULL, 1569363759, 1569363759);
INSERT INTO `auth_item` VALUES ('/cm/*', 2, NULL, NULL, NULL, 1569363693, 1569363693);
INSERT INTO `auth_item` VALUES ('/hr/*', 2, NULL, NULL, NULL, 1569363676, 1569363676);
INSERT INTO `auth_item` VALUES ('/site/*', 2, NULL, NULL, NULL, 1569363891, 1569363891);
INSERT INTO `auth_item` VALUES ('/st/*', 2, NULL, NULL, NULL, 1569363705, 1569363705);
INSERT INTO `auth_item` VALUES ('/tr/job/*', 2, NULL, NULL, NULL, 1569363778, 1569363778);
INSERT INTO `auth_item` VALUES ('/tr/jobapproval/*', 2, NULL, NULL, NULL, 1570957709, 1570957709);
INSERT INTO `auth_item` VALUES ('/tr/timereport/*', 2, NULL, NULL, NULL, 1569363794, 1569363794);
INSERT INTO `auth_item` VALUES ('/tr/trapproval/*', 2, NULL, NULL, NULL, 1570957813, 1570957813);
INSERT INTO `auth_item` VALUES ('00site', 2, NULL, NULL, NULL, 1569364062, 1569364081);
INSERT INTO `auth_item` VALUES ('01setup', 2, NULL, NULL, NULL, 1569363823, 1569363823);
INSERT INTO `auth_item` VALUES ('02Client', 2, NULL, NULL, NULL, 1569364132, 1569364177);
INSERT INTO `auth_item` VALUES ('03Job', 2, NULL, NULL, NULL, 1569364199, 1569364199);
INSERT INTO `auth_item` VALUES ('04TimeReport', 2, NULL, NULL, NULL, 1569364215, 1569364215);
INSERT INTO `auth_item` VALUES ('51JobApprovals', 2, NULL, NULL, NULL, 1570957654, 1570958282);
INSERT INTO `auth_item` VALUES ('52TrApprovals', 2, NULL, NULL, NULL, 1570958315, 1570958315);
INSERT INTO `auth_item` VALUES ('Clients', 1, '<i class =\'fa fa-users\' data-feather=\'users\'></i> Clients', NULL, NULL, 1569364307, 1570957936);
INSERT INTO `auth_item` VALUES ('Job', 1, '<i class=\'fa fa-newspaper-o\'></i> Jobs', NULL, NULL, 1569364330, 1570957931);
INSERT INTO `auth_item` VALUES ('JobApprovals', 1, '<i class=\'fa fa-check-circle-o\'></i> Job Approvals', NULL, NULL, 1570957869, 1570958375);
INSERT INTO `auth_item` VALUES ('root', 2, '*', NULL, NULL, 1458794111, 1458794111);
INSERT INTO `auth_item` VALUES ('Setup', 1, '<i class=\'fa fa-database\'></i> Setup', NULL, NULL, 1569364293, 1570957953);
INSERT INTO `auth_item` VALUES ('TimeReport', 1, '<i class=\'fa fa-calendar\'></i> Time Report', NULL, NULL, 1569364349, 1570957876);
INSERT INTO `auth_item` VALUES ('TimeReport Approvals', 1, '<i class=\'fa fa-check-circle-o\'></i> TR Approvals', NULL, NULL, 1570958388, 1570958446);
COMMIT;

-- ----------------------------
-- Table structure for auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`) USING BTREE,
  KEY `child` (`child`) USING BTREE,
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item_child
-- ----------------------------
BEGIN;
INSERT INTO `auth_item_child` VALUES ('root', '/*');
INSERT INTO `auth_item_child` VALUES ('02Client', '/cl/*');
INSERT INTO `auth_item_child` VALUES ('01setup', '/cm/*');
INSERT INTO `auth_item_child` VALUES ('01setup', '/hr/*');
INSERT INTO `auth_item_child` VALUES ('00site', '/site/*');
INSERT INTO `auth_item_child` VALUES ('01setup', '/st/*');
INSERT INTO `auth_item_child` VALUES ('03Job', '/tr/job/*');
INSERT INTO `auth_item_child` VALUES ('51JobApprovals', '/tr/jobapproval/*');
INSERT INTO `auth_item_child` VALUES ('04TimeReport', '/tr/timereport/*');
INSERT INTO `auth_item_child` VALUES ('52TrApprovals', '/tr/trapproval/*');
INSERT INTO `auth_item_child` VALUES ('01setup', '00site');
INSERT INTO `auth_item_child` VALUES ('02Client', '00site');
INSERT INTO `auth_item_child` VALUES ('03Job', '00site');
INSERT INTO `auth_item_child` VALUES ('04TimeReport', '00site');
INSERT INTO `auth_item_child` VALUES ('51JobApprovals', '00site');
INSERT INTO `auth_item_child` VALUES ('52TrApprovals', '00site');
INSERT INTO `auth_item_child` VALUES ('Setup', '01setup');
INSERT INTO `auth_item_child` VALUES ('Clients', '02Client');
INSERT INTO `auth_item_child` VALUES ('Job', '03Job');
INSERT INTO `auth_item_child` VALUES ('TimeReport', '04TimeReport');
INSERT INTO `auth_item_child` VALUES ('JobApprovals', '51JobApprovals');
INSERT INTO `auth_item_child` VALUES ('TimeReport Approvals', '52TrApprovals');
COMMIT;

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for auto_number
-- ----------------------------
DROP TABLE IF EXISTS `auto_number`;
CREATE TABLE `auto_number` (
  `group` varchar(32) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `optimistic_lock` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auto_number
-- ----------------------------
BEGIN;
INSERT INTO `auto_number` VALUES ('10053794a404b90579fb2c6fac61aab0', 5, 4, 1570250648, NULL);
INSERT INTO `auto_number` VALUES ('167456a7f3bb4f2535a8b94cc05ac50d', 2, 1, 1568481469, NULL);
INSERT INTO `auto_number` VALUES ('bf913f55d5509ae3f5b146df0086d5fa', 44, 43, 1571393585, 'a:4:{s:5:\"class\";s:23:\"common\\models\\cl\\Client\";s:5:\"group\";N;s:9:\"attribute\";s:3:\"Seq\";s:5:\"value\";s:1:\"?\";}');
COMMIT;

-- ----------------------------
-- Table structure for calendar
-- ----------------------------
DROP TABLE IF EXISTS `calendar`;
CREATE TABLE `calendar` (
  `calendardate` date NOT NULL,
  PRIMARY KEY (`calendardate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of calendar
-- ----------------------------
BEGIN;
INSERT INTO `calendar` VALUES ('2019-01-01');
INSERT INTO `calendar` VALUES ('2019-01-02');
INSERT INTO `calendar` VALUES ('2019-01-03');
INSERT INTO `calendar` VALUES ('2019-01-04');
INSERT INTO `calendar` VALUES ('2019-01-05');
INSERT INTO `calendar` VALUES ('2019-01-06');
INSERT INTO `calendar` VALUES ('2019-01-07');
INSERT INTO `calendar` VALUES ('2019-01-08');
INSERT INTO `calendar` VALUES ('2019-01-09');
INSERT INTO `calendar` VALUES ('2019-01-10');
INSERT INTO `calendar` VALUES ('2019-01-11');
INSERT INTO `calendar` VALUES ('2019-01-12');
INSERT INTO `calendar` VALUES ('2019-01-13');
INSERT INTO `calendar` VALUES ('2019-01-14');
INSERT INTO `calendar` VALUES ('2019-01-15');
INSERT INTO `calendar` VALUES ('2019-01-16');
INSERT INTO `calendar` VALUES ('2019-01-17');
INSERT INTO `calendar` VALUES ('2019-01-18');
INSERT INTO `calendar` VALUES ('2019-01-19');
INSERT INTO `calendar` VALUES ('2019-01-20');
INSERT INTO `calendar` VALUES ('2019-01-21');
INSERT INTO `calendar` VALUES ('2019-01-22');
INSERT INTO `calendar` VALUES ('2019-01-23');
INSERT INTO `calendar` VALUES ('2019-01-24');
INSERT INTO `calendar` VALUES ('2019-01-25');
INSERT INTO `calendar` VALUES ('2019-01-26');
INSERT INTO `calendar` VALUES ('2019-01-27');
INSERT INTO `calendar` VALUES ('2019-01-28');
INSERT INTO `calendar` VALUES ('2019-01-29');
INSERT INTO `calendar` VALUES ('2019-01-30');
INSERT INTO `calendar` VALUES ('2019-01-31');
INSERT INTO `calendar` VALUES ('2019-02-01');
INSERT INTO `calendar` VALUES ('2019-02-02');
INSERT INTO `calendar` VALUES ('2019-02-03');
INSERT INTO `calendar` VALUES ('2019-02-04');
INSERT INTO `calendar` VALUES ('2019-02-05');
INSERT INTO `calendar` VALUES ('2019-02-06');
INSERT INTO `calendar` VALUES ('2019-02-07');
INSERT INTO `calendar` VALUES ('2019-02-08');
INSERT INTO `calendar` VALUES ('2019-02-09');
INSERT INTO `calendar` VALUES ('2019-02-10');
INSERT INTO `calendar` VALUES ('2019-02-11');
INSERT INTO `calendar` VALUES ('2019-02-12');
INSERT INTO `calendar` VALUES ('2019-02-13');
INSERT INTO `calendar` VALUES ('2019-02-14');
INSERT INTO `calendar` VALUES ('2019-02-15');
INSERT INTO `calendar` VALUES ('2019-02-16');
INSERT INTO `calendar` VALUES ('2019-02-17');
INSERT INTO `calendar` VALUES ('2019-02-18');
INSERT INTO `calendar` VALUES ('2019-02-19');
INSERT INTO `calendar` VALUES ('2019-02-20');
INSERT INTO `calendar` VALUES ('2019-02-21');
INSERT INTO `calendar` VALUES ('2019-02-22');
INSERT INTO `calendar` VALUES ('2019-02-23');
INSERT INTO `calendar` VALUES ('2019-02-24');
INSERT INTO `calendar` VALUES ('2019-02-25');
INSERT INTO `calendar` VALUES ('2019-02-26');
INSERT INTO `calendar` VALUES ('2019-02-27');
INSERT INTO `calendar` VALUES ('2019-02-28');
INSERT INTO `calendar` VALUES ('2019-03-01');
INSERT INTO `calendar` VALUES ('2019-03-02');
INSERT INTO `calendar` VALUES ('2019-03-03');
INSERT INTO `calendar` VALUES ('2019-03-04');
INSERT INTO `calendar` VALUES ('2019-03-05');
INSERT INTO `calendar` VALUES ('2019-03-06');
INSERT INTO `calendar` VALUES ('2019-03-07');
INSERT INTO `calendar` VALUES ('2019-03-08');
INSERT INTO `calendar` VALUES ('2019-03-09');
INSERT INTO `calendar` VALUES ('2019-03-10');
INSERT INTO `calendar` VALUES ('2019-03-11');
INSERT INTO `calendar` VALUES ('2019-03-12');
INSERT INTO `calendar` VALUES ('2019-03-13');
INSERT INTO `calendar` VALUES ('2019-03-14');
INSERT INTO `calendar` VALUES ('2019-03-15');
INSERT INTO `calendar` VALUES ('2019-03-16');
INSERT INTO `calendar` VALUES ('2019-03-17');
INSERT INTO `calendar` VALUES ('2019-03-18');
INSERT INTO `calendar` VALUES ('2019-03-19');
INSERT INTO `calendar` VALUES ('2019-03-20');
INSERT INTO `calendar` VALUES ('2019-03-21');
INSERT INTO `calendar` VALUES ('2019-03-22');
INSERT INTO `calendar` VALUES ('2019-03-23');
INSERT INTO `calendar` VALUES ('2019-03-24');
INSERT INTO `calendar` VALUES ('2019-03-25');
INSERT INTO `calendar` VALUES ('2019-03-26');
INSERT INTO `calendar` VALUES ('2019-03-27');
INSERT INTO `calendar` VALUES ('2019-03-28');
INSERT INTO `calendar` VALUES ('2019-03-29');
INSERT INTO `calendar` VALUES ('2019-03-30');
INSERT INTO `calendar` VALUES ('2019-03-31');
INSERT INTO `calendar` VALUES ('2019-04-01');
INSERT INTO `calendar` VALUES ('2019-04-02');
INSERT INTO `calendar` VALUES ('2019-04-03');
INSERT INTO `calendar` VALUES ('2019-04-04');
INSERT INTO `calendar` VALUES ('2019-04-05');
INSERT INTO `calendar` VALUES ('2019-04-06');
INSERT INTO `calendar` VALUES ('2019-04-07');
INSERT INTO `calendar` VALUES ('2019-04-08');
INSERT INTO `calendar` VALUES ('2019-04-09');
INSERT INTO `calendar` VALUES ('2019-04-10');
INSERT INTO `calendar` VALUES ('2019-04-11');
INSERT INTO `calendar` VALUES ('2019-04-12');
INSERT INTO `calendar` VALUES ('2019-04-13');
INSERT INTO `calendar` VALUES ('2019-04-14');
INSERT INTO `calendar` VALUES ('2019-04-15');
INSERT INTO `calendar` VALUES ('2019-04-16');
INSERT INTO `calendar` VALUES ('2019-04-17');
INSERT INTO `calendar` VALUES ('2019-04-18');
INSERT INTO `calendar` VALUES ('2019-04-19');
INSERT INTO `calendar` VALUES ('2019-04-20');
INSERT INTO `calendar` VALUES ('2019-04-21');
INSERT INTO `calendar` VALUES ('2019-04-22');
INSERT INTO `calendar` VALUES ('2019-04-23');
INSERT INTO `calendar` VALUES ('2019-04-24');
INSERT INTO `calendar` VALUES ('2019-04-25');
INSERT INTO `calendar` VALUES ('2019-04-26');
INSERT INTO `calendar` VALUES ('2019-04-27');
INSERT INTO `calendar` VALUES ('2019-04-28');
INSERT INTO `calendar` VALUES ('2019-04-29');
INSERT INTO `calendar` VALUES ('2019-04-30');
INSERT INTO `calendar` VALUES ('2019-05-01');
INSERT INTO `calendar` VALUES ('2019-05-02');
INSERT INTO `calendar` VALUES ('2019-05-03');
INSERT INTO `calendar` VALUES ('2019-05-04');
INSERT INTO `calendar` VALUES ('2019-05-05');
INSERT INTO `calendar` VALUES ('2019-05-06');
INSERT INTO `calendar` VALUES ('2019-05-07');
INSERT INTO `calendar` VALUES ('2019-05-08');
INSERT INTO `calendar` VALUES ('2019-05-09');
INSERT INTO `calendar` VALUES ('2019-05-10');
INSERT INTO `calendar` VALUES ('2019-05-11');
INSERT INTO `calendar` VALUES ('2019-05-12');
INSERT INTO `calendar` VALUES ('2019-05-13');
INSERT INTO `calendar` VALUES ('2019-05-14');
INSERT INTO `calendar` VALUES ('2019-05-15');
INSERT INTO `calendar` VALUES ('2019-05-16');
INSERT INTO `calendar` VALUES ('2019-05-17');
INSERT INTO `calendar` VALUES ('2019-05-18');
INSERT INTO `calendar` VALUES ('2019-05-19');
INSERT INTO `calendar` VALUES ('2019-05-20');
INSERT INTO `calendar` VALUES ('2019-05-21');
INSERT INTO `calendar` VALUES ('2019-05-22');
INSERT INTO `calendar` VALUES ('2019-05-23');
INSERT INTO `calendar` VALUES ('2019-05-24');
INSERT INTO `calendar` VALUES ('2019-05-25');
INSERT INTO `calendar` VALUES ('2019-05-26');
INSERT INTO `calendar` VALUES ('2019-05-27');
INSERT INTO `calendar` VALUES ('2019-05-28');
INSERT INTO `calendar` VALUES ('2019-05-29');
INSERT INTO `calendar` VALUES ('2019-05-30');
INSERT INTO `calendar` VALUES ('2019-05-31');
INSERT INTO `calendar` VALUES ('2019-06-01');
INSERT INTO `calendar` VALUES ('2019-06-02');
INSERT INTO `calendar` VALUES ('2019-06-03');
INSERT INTO `calendar` VALUES ('2019-06-04');
INSERT INTO `calendar` VALUES ('2019-06-05');
INSERT INTO `calendar` VALUES ('2019-06-06');
INSERT INTO `calendar` VALUES ('2019-06-07');
INSERT INTO `calendar` VALUES ('2019-06-08');
INSERT INTO `calendar` VALUES ('2019-06-09');
INSERT INTO `calendar` VALUES ('2019-06-10');
INSERT INTO `calendar` VALUES ('2019-06-11');
INSERT INTO `calendar` VALUES ('2019-06-12');
INSERT INTO `calendar` VALUES ('2019-06-13');
INSERT INTO `calendar` VALUES ('2019-06-14');
INSERT INTO `calendar` VALUES ('2019-06-15');
INSERT INTO `calendar` VALUES ('2019-06-16');
INSERT INTO `calendar` VALUES ('2019-06-17');
INSERT INTO `calendar` VALUES ('2019-06-18');
INSERT INTO `calendar` VALUES ('2019-06-19');
INSERT INTO `calendar` VALUES ('2019-06-20');
INSERT INTO `calendar` VALUES ('2019-06-21');
INSERT INTO `calendar` VALUES ('2019-06-22');
INSERT INTO `calendar` VALUES ('2019-06-23');
INSERT INTO `calendar` VALUES ('2019-06-24');
INSERT INTO `calendar` VALUES ('2019-06-25');
INSERT INTO `calendar` VALUES ('2019-06-26');
INSERT INTO `calendar` VALUES ('2019-06-27');
INSERT INTO `calendar` VALUES ('2019-06-28');
INSERT INTO `calendar` VALUES ('2019-06-29');
INSERT INTO `calendar` VALUES ('2019-06-30');
INSERT INTO `calendar` VALUES ('2019-07-01');
INSERT INTO `calendar` VALUES ('2019-07-02');
INSERT INTO `calendar` VALUES ('2019-07-03');
INSERT INTO `calendar` VALUES ('2019-07-04');
INSERT INTO `calendar` VALUES ('2019-07-05');
INSERT INTO `calendar` VALUES ('2019-07-06');
INSERT INTO `calendar` VALUES ('2019-07-07');
INSERT INTO `calendar` VALUES ('2019-07-08');
INSERT INTO `calendar` VALUES ('2019-07-09');
INSERT INTO `calendar` VALUES ('2019-07-10');
INSERT INTO `calendar` VALUES ('2019-07-11');
INSERT INTO `calendar` VALUES ('2019-07-12');
INSERT INTO `calendar` VALUES ('2019-07-13');
INSERT INTO `calendar` VALUES ('2019-07-14');
INSERT INTO `calendar` VALUES ('2019-07-15');
INSERT INTO `calendar` VALUES ('2019-07-16');
INSERT INTO `calendar` VALUES ('2019-07-17');
INSERT INTO `calendar` VALUES ('2019-07-18');
INSERT INTO `calendar` VALUES ('2019-07-19');
INSERT INTO `calendar` VALUES ('2019-07-20');
INSERT INTO `calendar` VALUES ('2019-07-21');
INSERT INTO `calendar` VALUES ('2019-07-22');
INSERT INTO `calendar` VALUES ('2019-07-23');
INSERT INTO `calendar` VALUES ('2019-07-24');
INSERT INTO `calendar` VALUES ('2019-07-25');
INSERT INTO `calendar` VALUES ('2019-07-26');
INSERT INTO `calendar` VALUES ('2019-07-27');
INSERT INTO `calendar` VALUES ('2019-07-28');
INSERT INTO `calendar` VALUES ('2019-07-29');
INSERT INTO `calendar` VALUES ('2019-07-30');
INSERT INTO `calendar` VALUES ('2019-07-31');
INSERT INTO `calendar` VALUES ('2019-08-01');
INSERT INTO `calendar` VALUES ('2019-08-02');
INSERT INTO `calendar` VALUES ('2019-08-03');
INSERT INTO `calendar` VALUES ('2019-08-04');
INSERT INTO `calendar` VALUES ('2019-08-05');
INSERT INTO `calendar` VALUES ('2019-08-06');
INSERT INTO `calendar` VALUES ('2019-08-07');
INSERT INTO `calendar` VALUES ('2019-08-08');
INSERT INTO `calendar` VALUES ('2019-08-09');
INSERT INTO `calendar` VALUES ('2019-08-10');
INSERT INTO `calendar` VALUES ('2019-08-11');
INSERT INTO `calendar` VALUES ('2019-08-12');
INSERT INTO `calendar` VALUES ('2019-08-13');
INSERT INTO `calendar` VALUES ('2019-08-14');
INSERT INTO `calendar` VALUES ('2019-08-15');
INSERT INTO `calendar` VALUES ('2019-08-16');
INSERT INTO `calendar` VALUES ('2019-08-17');
INSERT INTO `calendar` VALUES ('2019-08-18');
INSERT INTO `calendar` VALUES ('2019-08-19');
INSERT INTO `calendar` VALUES ('2019-08-20');
INSERT INTO `calendar` VALUES ('2019-08-21');
INSERT INTO `calendar` VALUES ('2019-08-22');
INSERT INTO `calendar` VALUES ('2019-08-23');
INSERT INTO `calendar` VALUES ('2019-08-24');
INSERT INTO `calendar` VALUES ('2019-08-25');
INSERT INTO `calendar` VALUES ('2019-08-26');
INSERT INTO `calendar` VALUES ('2019-08-27');
INSERT INTO `calendar` VALUES ('2019-08-28');
INSERT INTO `calendar` VALUES ('2019-08-29');
INSERT INTO `calendar` VALUES ('2019-08-30');
INSERT INTO `calendar` VALUES ('2019-08-31');
INSERT INTO `calendar` VALUES ('2019-09-01');
INSERT INTO `calendar` VALUES ('2019-09-02');
INSERT INTO `calendar` VALUES ('2019-09-03');
INSERT INTO `calendar` VALUES ('2019-09-04');
INSERT INTO `calendar` VALUES ('2019-09-05');
INSERT INTO `calendar` VALUES ('2019-09-06');
INSERT INTO `calendar` VALUES ('2019-09-07');
INSERT INTO `calendar` VALUES ('2019-09-08');
INSERT INTO `calendar` VALUES ('2019-09-09');
INSERT INTO `calendar` VALUES ('2019-09-10');
INSERT INTO `calendar` VALUES ('2019-09-11');
INSERT INTO `calendar` VALUES ('2019-09-12');
INSERT INTO `calendar` VALUES ('2019-09-13');
INSERT INTO `calendar` VALUES ('2019-09-14');
INSERT INTO `calendar` VALUES ('2019-09-15');
INSERT INTO `calendar` VALUES ('2019-09-16');
INSERT INTO `calendar` VALUES ('2019-09-17');
INSERT INTO `calendar` VALUES ('2019-09-18');
INSERT INTO `calendar` VALUES ('2019-09-19');
INSERT INTO `calendar` VALUES ('2019-09-20');
INSERT INTO `calendar` VALUES ('2019-09-21');
INSERT INTO `calendar` VALUES ('2019-09-22');
INSERT INTO `calendar` VALUES ('2019-09-23');
INSERT INTO `calendar` VALUES ('2019-09-24');
INSERT INTO `calendar` VALUES ('2019-09-25');
INSERT INTO `calendar` VALUES ('2019-09-26');
INSERT INTO `calendar` VALUES ('2019-09-27');
INSERT INTO `calendar` VALUES ('2019-09-28');
INSERT INTO `calendar` VALUES ('2019-09-29');
INSERT INTO `calendar` VALUES ('2019-09-30');
INSERT INTO `calendar` VALUES ('2019-10-01');
INSERT INTO `calendar` VALUES ('2019-10-02');
INSERT INTO `calendar` VALUES ('2019-10-03');
INSERT INTO `calendar` VALUES ('2019-10-04');
INSERT INTO `calendar` VALUES ('2019-10-05');
INSERT INTO `calendar` VALUES ('2019-10-06');
INSERT INTO `calendar` VALUES ('2019-10-07');
INSERT INTO `calendar` VALUES ('2019-10-08');
INSERT INTO `calendar` VALUES ('2019-10-09');
INSERT INTO `calendar` VALUES ('2019-10-10');
INSERT INTO `calendar` VALUES ('2019-10-11');
INSERT INTO `calendar` VALUES ('2019-10-12');
INSERT INTO `calendar` VALUES ('2019-10-13');
INSERT INTO `calendar` VALUES ('2019-10-14');
INSERT INTO `calendar` VALUES ('2019-10-15');
INSERT INTO `calendar` VALUES ('2019-10-16');
INSERT INTO `calendar` VALUES ('2019-10-17');
INSERT INTO `calendar` VALUES ('2019-10-18');
INSERT INTO `calendar` VALUES ('2019-10-19');
INSERT INTO `calendar` VALUES ('2019-10-20');
INSERT INTO `calendar` VALUES ('2019-10-21');
INSERT INTO `calendar` VALUES ('2019-10-22');
INSERT INTO `calendar` VALUES ('2019-10-23');
INSERT INTO `calendar` VALUES ('2019-10-24');
INSERT INTO `calendar` VALUES ('2019-10-25');
INSERT INTO `calendar` VALUES ('2019-10-26');
INSERT INTO `calendar` VALUES ('2019-10-27');
INSERT INTO `calendar` VALUES ('2019-10-28');
INSERT INTO `calendar` VALUES ('2019-10-29');
INSERT INTO `calendar` VALUES ('2019-10-30');
INSERT INTO `calendar` VALUES ('2019-10-31');
INSERT INTO `calendar` VALUES ('2019-11-01');
INSERT INTO `calendar` VALUES ('2019-11-02');
INSERT INTO `calendar` VALUES ('2019-11-03');
INSERT INTO `calendar` VALUES ('2019-11-04');
INSERT INTO `calendar` VALUES ('2019-11-05');
INSERT INTO `calendar` VALUES ('2019-11-06');
INSERT INTO `calendar` VALUES ('2019-11-07');
INSERT INTO `calendar` VALUES ('2019-11-08');
INSERT INTO `calendar` VALUES ('2019-11-09');
INSERT INTO `calendar` VALUES ('2019-11-10');
INSERT INTO `calendar` VALUES ('2019-11-11');
INSERT INTO `calendar` VALUES ('2019-11-12');
INSERT INTO `calendar` VALUES ('2019-11-13');
INSERT INTO `calendar` VALUES ('2019-11-14');
INSERT INTO `calendar` VALUES ('2019-11-15');
INSERT INTO `calendar` VALUES ('2019-11-16');
INSERT INTO `calendar` VALUES ('2019-11-17');
INSERT INTO `calendar` VALUES ('2019-11-18');
INSERT INTO `calendar` VALUES ('2019-11-19');
INSERT INTO `calendar` VALUES ('2019-11-20');
INSERT INTO `calendar` VALUES ('2019-11-21');
INSERT INTO `calendar` VALUES ('2019-11-22');
INSERT INTO `calendar` VALUES ('2019-11-23');
INSERT INTO `calendar` VALUES ('2019-11-24');
INSERT INTO `calendar` VALUES ('2019-11-25');
INSERT INTO `calendar` VALUES ('2019-11-26');
INSERT INTO `calendar` VALUES ('2019-11-27');
INSERT INTO `calendar` VALUES ('2019-11-28');
INSERT INTO `calendar` VALUES ('2019-11-29');
INSERT INTO `calendar` VALUES ('2019-11-30');
INSERT INTO `calendar` VALUES ('2019-12-01');
INSERT INTO `calendar` VALUES ('2019-12-02');
INSERT INTO `calendar` VALUES ('2019-12-03');
INSERT INTO `calendar` VALUES ('2019-12-04');
INSERT INTO `calendar` VALUES ('2019-12-05');
INSERT INTO `calendar` VALUES ('2019-12-06');
INSERT INTO `calendar` VALUES ('2019-12-07');
INSERT INTO `calendar` VALUES ('2019-12-08');
INSERT INTO `calendar` VALUES ('2019-12-09');
INSERT INTO `calendar` VALUES ('2019-12-10');
INSERT INTO `calendar` VALUES ('2019-12-11');
INSERT INTO `calendar` VALUES ('2019-12-12');
INSERT INTO `calendar` VALUES ('2019-12-13');
INSERT INTO `calendar` VALUES ('2019-12-14');
INSERT INTO `calendar` VALUES ('2019-12-15');
INSERT INTO `calendar` VALUES ('2019-12-16');
INSERT INTO `calendar` VALUES ('2019-12-17');
INSERT INTO `calendar` VALUES ('2019-12-18');
INSERT INTO `calendar` VALUES ('2019-12-19');
INSERT INTO `calendar` VALUES ('2019-12-20');
INSERT INTO `calendar` VALUES ('2019-12-21');
INSERT INTO `calendar` VALUES ('2019-12-22');
INSERT INTO `calendar` VALUES ('2019-12-23');
INSERT INTO `calendar` VALUES ('2019-12-24');
INSERT INTO `calendar` VALUES ('2019-12-25');
INSERT INTO `calendar` VALUES ('2019-12-26');
INSERT INTO `calendar` VALUES ('2019-12-27');
INSERT INTO `calendar` VALUES ('2019-12-28');
INSERT INTO `calendar` VALUES ('2019-12-29');
INSERT INTO `calendar` VALUES ('2019-12-30');
INSERT INTO `calendar` VALUES ('2019-12-31');
INSERT INTO `calendar` VALUES ('2020-01-01');
INSERT INTO `calendar` VALUES ('2020-01-02');
INSERT INTO `calendar` VALUES ('2020-01-03');
INSERT INTO `calendar` VALUES ('2020-01-04');
INSERT INTO `calendar` VALUES ('2020-01-05');
INSERT INTO `calendar` VALUES ('2020-01-06');
INSERT INTO `calendar` VALUES ('2020-01-07');
INSERT INTO `calendar` VALUES ('2020-01-08');
INSERT INTO `calendar` VALUES ('2020-01-09');
INSERT INTO `calendar` VALUES ('2020-01-10');
INSERT INTO `calendar` VALUES ('2020-01-11');
INSERT INTO `calendar` VALUES ('2020-01-12');
INSERT INTO `calendar` VALUES ('2020-01-13');
INSERT INTO `calendar` VALUES ('2020-01-14');
INSERT INTO `calendar` VALUES ('2020-01-15');
INSERT INTO `calendar` VALUES ('2020-01-16');
INSERT INTO `calendar` VALUES ('2020-01-17');
INSERT INTO `calendar` VALUES ('2020-01-18');
INSERT INTO `calendar` VALUES ('2020-01-19');
INSERT INTO `calendar` VALUES ('2020-01-20');
INSERT INTO `calendar` VALUES ('2020-01-21');
INSERT INTO `calendar` VALUES ('2020-01-22');
INSERT INTO `calendar` VALUES ('2020-01-23');
INSERT INTO `calendar` VALUES ('2020-01-24');
INSERT INTO `calendar` VALUES ('2020-01-25');
INSERT INTO `calendar` VALUES ('2020-01-26');
INSERT INTO `calendar` VALUES ('2020-01-27');
INSERT INTO `calendar` VALUES ('2020-01-28');
INSERT INTO `calendar` VALUES ('2020-01-29');
INSERT INTO `calendar` VALUES ('2020-01-30');
INSERT INTO `calendar` VALUES ('2020-01-31');
INSERT INTO `calendar` VALUES ('2020-02-01');
INSERT INTO `calendar` VALUES ('2020-02-02');
INSERT INTO `calendar` VALUES ('2020-02-03');
INSERT INTO `calendar` VALUES ('2020-02-04');
INSERT INTO `calendar` VALUES ('2020-02-05');
INSERT INTO `calendar` VALUES ('2020-02-06');
INSERT INTO `calendar` VALUES ('2020-02-07');
INSERT INTO `calendar` VALUES ('2020-02-08');
INSERT INTO `calendar` VALUES ('2020-02-09');
INSERT INTO `calendar` VALUES ('2020-02-10');
INSERT INTO `calendar` VALUES ('2020-02-11');
INSERT INTO `calendar` VALUES ('2020-02-12');
INSERT INTO `calendar` VALUES ('2020-02-13');
INSERT INTO `calendar` VALUES ('2020-02-14');
INSERT INTO `calendar` VALUES ('2020-02-15');
INSERT INTO `calendar` VALUES ('2020-02-16');
INSERT INTO `calendar` VALUES ('2020-02-17');
INSERT INTO `calendar` VALUES ('2020-02-18');
INSERT INTO `calendar` VALUES ('2020-02-19');
INSERT INTO `calendar` VALUES ('2020-02-20');
INSERT INTO `calendar` VALUES ('2020-02-21');
INSERT INTO `calendar` VALUES ('2020-02-22');
INSERT INTO `calendar` VALUES ('2020-02-23');
INSERT INTO `calendar` VALUES ('2020-02-24');
INSERT INTO `calendar` VALUES ('2020-02-25');
INSERT INTO `calendar` VALUES ('2020-02-26');
INSERT INTO `calendar` VALUES ('2020-02-27');
INSERT INTO `calendar` VALUES ('2020-02-28');
INSERT INTO `calendar` VALUES ('2020-02-29');
INSERT INTO `calendar` VALUES ('2020-03-01');
INSERT INTO `calendar` VALUES ('2020-03-02');
INSERT INTO `calendar` VALUES ('2020-03-03');
INSERT INTO `calendar` VALUES ('2020-03-04');
INSERT INTO `calendar` VALUES ('2020-03-05');
INSERT INTO `calendar` VALUES ('2020-03-06');
INSERT INTO `calendar` VALUES ('2020-03-07');
INSERT INTO `calendar` VALUES ('2020-03-08');
INSERT INTO `calendar` VALUES ('2020-03-09');
INSERT INTO `calendar` VALUES ('2020-03-10');
INSERT INTO `calendar` VALUES ('2020-03-11');
INSERT INTO `calendar` VALUES ('2020-03-12');
INSERT INTO `calendar` VALUES ('2020-03-13');
INSERT INTO `calendar` VALUES ('2020-03-14');
INSERT INTO `calendar` VALUES ('2020-03-15');
INSERT INTO `calendar` VALUES ('2020-03-16');
INSERT INTO `calendar` VALUES ('2020-03-17');
INSERT INTO `calendar` VALUES ('2020-03-18');
INSERT INTO `calendar` VALUES ('2020-03-19');
INSERT INTO `calendar` VALUES ('2020-03-20');
INSERT INTO `calendar` VALUES ('2020-03-21');
INSERT INTO `calendar` VALUES ('2020-03-22');
INSERT INTO `calendar` VALUES ('2020-03-23');
INSERT INTO `calendar` VALUES ('2020-03-24');
INSERT INTO `calendar` VALUES ('2020-03-25');
INSERT INTO `calendar` VALUES ('2020-03-26');
INSERT INTO `calendar` VALUES ('2020-03-27');
INSERT INTO `calendar` VALUES ('2020-03-28');
INSERT INTO `calendar` VALUES ('2020-03-29');
INSERT INTO `calendar` VALUES ('2020-03-30');
INSERT INTO `calendar` VALUES ('2020-03-31');
INSERT INTO `calendar` VALUES ('2020-04-01');
INSERT INTO `calendar` VALUES ('2020-04-02');
INSERT INTO `calendar` VALUES ('2020-04-03');
INSERT INTO `calendar` VALUES ('2020-04-04');
INSERT INTO `calendar` VALUES ('2020-04-05');
INSERT INTO `calendar` VALUES ('2020-04-06');
INSERT INTO `calendar` VALUES ('2020-04-07');
INSERT INTO `calendar` VALUES ('2020-04-08');
INSERT INTO `calendar` VALUES ('2020-04-09');
INSERT INTO `calendar` VALUES ('2020-04-10');
INSERT INTO `calendar` VALUES ('2020-04-11');
INSERT INTO `calendar` VALUES ('2020-04-12');
INSERT INTO `calendar` VALUES ('2020-04-13');
INSERT INTO `calendar` VALUES ('2020-04-14');
INSERT INTO `calendar` VALUES ('2020-04-15');
INSERT INTO `calendar` VALUES ('2020-04-16');
INSERT INTO `calendar` VALUES ('2020-04-17');
INSERT INTO `calendar` VALUES ('2020-04-18');
INSERT INTO `calendar` VALUES ('2020-04-19');
INSERT INTO `calendar` VALUES ('2020-04-20');
INSERT INTO `calendar` VALUES ('2020-04-21');
INSERT INTO `calendar` VALUES ('2020-04-22');
INSERT INTO `calendar` VALUES ('2020-04-23');
INSERT INTO `calendar` VALUES ('2020-04-24');
INSERT INTO `calendar` VALUES ('2020-04-25');
INSERT INTO `calendar` VALUES ('2020-04-26');
INSERT INTO `calendar` VALUES ('2020-04-27');
INSERT INTO `calendar` VALUES ('2020-04-28');
INSERT INTO `calendar` VALUES ('2020-04-29');
INSERT INTO `calendar` VALUES ('2020-04-30');
INSERT INTO `calendar` VALUES ('2020-05-01');
INSERT INTO `calendar` VALUES ('2020-05-02');
INSERT INTO `calendar` VALUES ('2020-05-03');
INSERT INTO `calendar` VALUES ('2020-05-04');
INSERT INTO `calendar` VALUES ('2020-05-05');
INSERT INTO `calendar` VALUES ('2020-05-06');
INSERT INTO `calendar` VALUES ('2020-05-07');
INSERT INTO `calendar` VALUES ('2020-05-08');
INSERT INTO `calendar` VALUES ('2020-05-09');
INSERT INTO `calendar` VALUES ('2020-05-10');
INSERT INTO `calendar` VALUES ('2020-05-11');
INSERT INTO `calendar` VALUES ('2020-05-12');
INSERT INTO `calendar` VALUES ('2020-05-13');
INSERT INTO `calendar` VALUES ('2020-05-14');
INSERT INTO `calendar` VALUES ('2020-05-15');
INSERT INTO `calendar` VALUES ('2020-05-16');
INSERT INTO `calendar` VALUES ('2020-05-17');
INSERT INTO `calendar` VALUES ('2020-05-18');
INSERT INTO `calendar` VALUES ('2020-05-19');
INSERT INTO `calendar` VALUES ('2020-05-20');
INSERT INTO `calendar` VALUES ('2020-05-21');
INSERT INTO `calendar` VALUES ('2020-05-22');
INSERT INTO `calendar` VALUES ('2020-05-23');
INSERT INTO `calendar` VALUES ('2020-05-24');
INSERT INTO `calendar` VALUES ('2020-05-25');
INSERT INTO `calendar` VALUES ('2020-05-26');
INSERT INTO `calendar` VALUES ('2020-05-27');
INSERT INTO `calendar` VALUES ('2020-05-28');
INSERT INTO `calendar` VALUES ('2020-05-29');
INSERT INTO `calendar` VALUES ('2020-05-30');
INSERT INTO `calendar` VALUES ('2020-05-31');
INSERT INTO `calendar` VALUES ('2020-06-01');
INSERT INTO `calendar` VALUES ('2020-06-02');
INSERT INTO `calendar` VALUES ('2020-06-03');
INSERT INTO `calendar` VALUES ('2020-06-04');
INSERT INTO `calendar` VALUES ('2020-06-05');
INSERT INTO `calendar` VALUES ('2020-06-06');
INSERT INTO `calendar` VALUES ('2020-06-07');
INSERT INTO `calendar` VALUES ('2020-06-08');
INSERT INTO `calendar` VALUES ('2020-06-09');
INSERT INTO `calendar` VALUES ('2020-06-10');
INSERT INTO `calendar` VALUES ('2020-06-11');
INSERT INTO `calendar` VALUES ('2020-06-12');
INSERT INTO `calendar` VALUES ('2020-06-13');
INSERT INTO `calendar` VALUES ('2020-06-14');
INSERT INTO `calendar` VALUES ('2020-06-15');
INSERT INTO `calendar` VALUES ('2020-06-16');
INSERT INTO `calendar` VALUES ('2020-06-17');
INSERT INTO `calendar` VALUES ('2020-06-18');
INSERT INTO `calendar` VALUES ('2020-06-19');
INSERT INTO `calendar` VALUES ('2020-06-20');
INSERT INTO `calendar` VALUES ('2020-06-21');
INSERT INTO `calendar` VALUES ('2020-06-22');
INSERT INTO `calendar` VALUES ('2020-06-23');
INSERT INTO `calendar` VALUES ('2020-06-24');
INSERT INTO `calendar` VALUES ('2020-06-25');
INSERT INTO `calendar` VALUES ('2020-06-26');
INSERT INTO `calendar` VALUES ('2020-06-27');
INSERT INTO `calendar` VALUES ('2020-06-28');
INSERT INTO `calendar` VALUES ('2020-06-29');
INSERT INTO `calendar` VALUES ('2020-06-30');
INSERT INTO `calendar` VALUES ('2020-07-01');
INSERT INTO `calendar` VALUES ('2020-07-02');
INSERT INTO `calendar` VALUES ('2020-07-03');
INSERT INTO `calendar` VALUES ('2020-07-04');
INSERT INTO `calendar` VALUES ('2020-07-05');
INSERT INTO `calendar` VALUES ('2020-07-06');
INSERT INTO `calendar` VALUES ('2020-07-07');
INSERT INTO `calendar` VALUES ('2020-07-08');
INSERT INTO `calendar` VALUES ('2020-07-09');
INSERT INTO `calendar` VALUES ('2020-07-10');
INSERT INTO `calendar` VALUES ('2020-07-11');
INSERT INTO `calendar` VALUES ('2020-07-12');
INSERT INTO `calendar` VALUES ('2020-07-13');
INSERT INTO `calendar` VALUES ('2020-07-14');
INSERT INTO `calendar` VALUES ('2020-07-15');
INSERT INTO `calendar` VALUES ('2020-07-16');
INSERT INTO `calendar` VALUES ('2020-07-17');
INSERT INTO `calendar` VALUES ('2020-07-18');
INSERT INTO `calendar` VALUES ('2020-07-19');
INSERT INTO `calendar` VALUES ('2020-07-20');
INSERT INTO `calendar` VALUES ('2020-07-21');
INSERT INTO `calendar` VALUES ('2020-07-22');
INSERT INTO `calendar` VALUES ('2020-07-23');
INSERT INTO `calendar` VALUES ('2020-07-24');
INSERT INTO `calendar` VALUES ('2020-07-25');
INSERT INTO `calendar` VALUES ('2020-07-26');
INSERT INTO `calendar` VALUES ('2020-07-27');
INSERT INTO `calendar` VALUES ('2020-07-28');
INSERT INTO `calendar` VALUES ('2020-07-29');
INSERT INTO `calendar` VALUES ('2020-07-30');
INSERT INTO `calendar` VALUES ('2020-07-31');
INSERT INTO `calendar` VALUES ('2020-08-01');
INSERT INTO `calendar` VALUES ('2020-08-02');
INSERT INTO `calendar` VALUES ('2020-08-03');
INSERT INTO `calendar` VALUES ('2020-08-04');
INSERT INTO `calendar` VALUES ('2020-08-05');
INSERT INTO `calendar` VALUES ('2020-08-06');
INSERT INTO `calendar` VALUES ('2020-08-07');
INSERT INTO `calendar` VALUES ('2020-08-08');
INSERT INTO `calendar` VALUES ('2020-08-09');
INSERT INTO `calendar` VALUES ('2020-08-10');
INSERT INTO `calendar` VALUES ('2020-08-11');
INSERT INTO `calendar` VALUES ('2020-08-12');
INSERT INTO `calendar` VALUES ('2020-08-13');
INSERT INTO `calendar` VALUES ('2020-08-14');
INSERT INTO `calendar` VALUES ('2020-08-15');
INSERT INTO `calendar` VALUES ('2020-08-16');
INSERT INTO `calendar` VALUES ('2020-08-17');
INSERT INTO `calendar` VALUES ('2020-08-18');
INSERT INTO `calendar` VALUES ('2020-08-19');
INSERT INTO `calendar` VALUES ('2020-08-20');
INSERT INTO `calendar` VALUES ('2020-08-21');
INSERT INTO `calendar` VALUES ('2020-08-22');
INSERT INTO `calendar` VALUES ('2020-08-23');
INSERT INTO `calendar` VALUES ('2020-08-24');
INSERT INTO `calendar` VALUES ('2020-08-25');
INSERT INTO `calendar` VALUES ('2020-08-26');
INSERT INTO `calendar` VALUES ('2020-08-27');
INSERT INTO `calendar` VALUES ('2020-08-28');
INSERT INTO `calendar` VALUES ('2020-08-29');
INSERT INTO `calendar` VALUES ('2020-08-30');
INSERT INTO `calendar` VALUES ('2020-08-31');
INSERT INTO `calendar` VALUES ('2020-09-01');
INSERT INTO `calendar` VALUES ('2020-09-02');
INSERT INTO `calendar` VALUES ('2020-09-03');
INSERT INTO `calendar` VALUES ('2020-09-04');
INSERT INTO `calendar` VALUES ('2020-09-05');
INSERT INTO `calendar` VALUES ('2020-09-06');
INSERT INTO `calendar` VALUES ('2020-09-07');
INSERT INTO `calendar` VALUES ('2020-09-08');
INSERT INTO `calendar` VALUES ('2020-09-09');
INSERT INTO `calendar` VALUES ('2020-09-10');
INSERT INTO `calendar` VALUES ('2020-09-11');
INSERT INTO `calendar` VALUES ('2020-09-12');
INSERT INTO `calendar` VALUES ('2020-09-13');
INSERT INTO `calendar` VALUES ('2020-09-14');
INSERT INTO `calendar` VALUES ('2020-09-15');
INSERT INTO `calendar` VALUES ('2020-09-16');
INSERT INTO `calendar` VALUES ('2020-09-17');
INSERT INTO `calendar` VALUES ('2020-09-18');
INSERT INTO `calendar` VALUES ('2020-09-19');
INSERT INTO `calendar` VALUES ('2020-09-20');
INSERT INTO `calendar` VALUES ('2020-09-21');
INSERT INTO `calendar` VALUES ('2020-09-22');
INSERT INTO `calendar` VALUES ('2020-09-23');
INSERT INTO `calendar` VALUES ('2020-09-24');
INSERT INTO `calendar` VALUES ('2020-09-25');
INSERT INTO `calendar` VALUES ('2020-09-26');
INSERT INTO `calendar` VALUES ('2020-09-27');
INSERT INTO `calendar` VALUES ('2020-09-28');
INSERT INTO `calendar` VALUES ('2020-09-29');
INSERT INTO `calendar` VALUES ('2020-09-30');
INSERT INTO `calendar` VALUES ('2020-10-01');
INSERT INTO `calendar` VALUES ('2020-10-02');
INSERT INTO `calendar` VALUES ('2020-10-03');
INSERT INTO `calendar` VALUES ('2020-10-04');
INSERT INTO `calendar` VALUES ('2020-10-05');
INSERT INTO `calendar` VALUES ('2020-10-06');
INSERT INTO `calendar` VALUES ('2020-10-07');
INSERT INTO `calendar` VALUES ('2020-10-08');
INSERT INTO `calendar` VALUES ('2020-10-09');
INSERT INTO `calendar` VALUES ('2020-10-10');
INSERT INTO `calendar` VALUES ('2020-10-11');
INSERT INTO `calendar` VALUES ('2020-10-12');
INSERT INTO `calendar` VALUES ('2020-10-13');
INSERT INTO `calendar` VALUES ('2020-10-14');
INSERT INTO `calendar` VALUES ('2020-10-15');
INSERT INTO `calendar` VALUES ('2020-10-16');
INSERT INTO `calendar` VALUES ('2020-10-17');
INSERT INTO `calendar` VALUES ('2020-10-18');
INSERT INTO `calendar` VALUES ('2020-10-19');
INSERT INTO `calendar` VALUES ('2020-10-20');
INSERT INTO `calendar` VALUES ('2020-10-21');
INSERT INTO `calendar` VALUES ('2020-10-22');
INSERT INTO `calendar` VALUES ('2020-10-23');
INSERT INTO `calendar` VALUES ('2020-10-24');
INSERT INTO `calendar` VALUES ('2020-10-25');
INSERT INTO `calendar` VALUES ('2020-10-26');
INSERT INTO `calendar` VALUES ('2020-10-27');
INSERT INTO `calendar` VALUES ('2020-10-28');
INSERT INTO `calendar` VALUES ('2020-10-29');
INSERT INTO `calendar` VALUES ('2020-10-30');
INSERT INTO `calendar` VALUES ('2020-10-31');
INSERT INTO `calendar` VALUES ('2020-11-01');
INSERT INTO `calendar` VALUES ('2020-11-02');
INSERT INTO `calendar` VALUES ('2020-11-03');
INSERT INTO `calendar` VALUES ('2020-11-04');
INSERT INTO `calendar` VALUES ('2020-11-05');
INSERT INTO `calendar` VALUES ('2020-11-06');
INSERT INTO `calendar` VALUES ('2020-11-07');
INSERT INTO `calendar` VALUES ('2020-11-08');
INSERT INTO `calendar` VALUES ('2020-11-09');
INSERT INTO `calendar` VALUES ('2020-11-10');
INSERT INTO `calendar` VALUES ('2020-11-11');
INSERT INTO `calendar` VALUES ('2020-11-12');
INSERT INTO `calendar` VALUES ('2020-11-13');
INSERT INTO `calendar` VALUES ('2020-11-14');
INSERT INTO `calendar` VALUES ('2020-11-15');
INSERT INTO `calendar` VALUES ('2020-11-16');
INSERT INTO `calendar` VALUES ('2020-11-17');
INSERT INTO `calendar` VALUES ('2020-11-18');
INSERT INTO `calendar` VALUES ('2020-11-19');
INSERT INTO `calendar` VALUES ('2020-11-20');
INSERT INTO `calendar` VALUES ('2020-11-21');
INSERT INTO `calendar` VALUES ('2020-11-22');
INSERT INTO `calendar` VALUES ('2020-11-23');
INSERT INTO `calendar` VALUES ('2020-11-24');
INSERT INTO `calendar` VALUES ('2020-11-25');
INSERT INTO `calendar` VALUES ('2020-11-26');
INSERT INTO `calendar` VALUES ('2020-11-27');
INSERT INTO `calendar` VALUES ('2020-11-28');
INSERT INTO `calendar` VALUES ('2020-11-29');
INSERT INTO `calendar` VALUES ('2020-11-30');
INSERT INTO `calendar` VALUES ('2020-12-01');
INSERT INTO `calendar` VALUES ('2020-12-02');
INSERT INTO `calendar` VALUES ('2020-12-03');
INSERT INTO `calendar` VALUES ('2020-12-04');
INSERT INTO `calendar` VALUES ('2020-12-05');
INSERT INTO `calendar` VALUES ('2020-12-06');
INSERT INTO `calendar` VALUES ('2020-12-07');
INSERT INTO `calendar` VALUES ('2020-12-08');
INSERT INTO `calendar` VALUES ('2020-12-09');
INSERT INTO `calendar` VALUES ('2020-12-10');
INSERT INTO `calendar` VALUES ('2020-12-11');
INSERT INTO `calendar` VALUES ('2020-12-12');
INSERT INTO `calendar` VALUES ('2020-12-13');
INSERT INTO `calendar` VALUES ('2020-12-14');
INSERT INTO `calendar` VALUES ('2020-12-15');
INSERT INTO `calendar` VALUES ('2020-12-16');
INSERT INTO `calendar` VALUES ('2020-12-17');
INSERT INTO `calendar` VALUES ('2020-12-18');
INSERT INTO `calendar` VALUES ('2020-12-19');
INSERT INTO `calendar` VALUES ('2020-12-20');
INSERT INTO `calendar` VALUES ('2020-12-21');
INSERT INTO `calendar` VALUES ('2020-12-22');
INSERT INTO `calendar` VALUES ('2020-12-23');
INSERT INTO `calendar` VALUES ('2020-12-24');
INSERT INTO `calendar` VALUES ('2020-12-25');
INSERT INTO `calendar` VALUES ('2020-12-26');
INSERT INTO `calendar` VALUES ('2020-12-27');
INSERT INTO `calendar` VALUES ('2020-12-28');
INSERT INTO `calendar` VALUES ('2020-12-29');
INSERT INTO `calendar` VALUES ('2020-12-30');
INSERT INTO `calendar` VALUES ('2020-12-31');
INSERT INTO `calendar` VALUES ('2021-01-01');
INSERT INTO `calendar` VALUES ('2021-01-02');
INSERT INTO `calendar` VALUES ('2021-01-03');
INSERT INTO `calendar` VALUES ('2021-01-04');
INSERT INTO `calendar` VALUES ('2021-01-05');
INSERT INTO `calendar` VALUES ('2021-01-06');
INSERT INTO `calendar` VALUES ('2021-01-07');
INSERT INTO `calendar` VALUES ('2021-01-08');
INSERT INTO `calendar` VALUES ('2021-01-09');
INSERT INTO `calendar` VALUES ('2021-01-10');
INSERT INTO `calendar` VALUES ('2021-01-11');
INSERT INTO `calendar` VALUES ('2021-01-12');
INSERT INTO `calendar` VALUES ('2021-01-13');
INSERT INTO `calendar` VALUES ('2021-01-14');
INSERT INTO `calendar` VALUES ('2021-01-15');
INSERT INTO `calendar` VALUES ('2021-01-16');
INSERT INTO `calendar` VALUES ('2021-01-17');
INSERT INTO `calendar` VALUES ('2021-01-18');
INSERT INTO `calendar` VALUES ('2021-01-19');
INSERT INTO `calendar` VALUES ('2021-01-20');
INSERT INTO `calendar` VALUES ('2021-01-21');
INSERT INTO `calendar` VALUES ('2021-01-22');
INSERT INTO `calendar` VALUES ('2021-01-23');
INSERT INTO `calendar` VALUES ('2021-01-24');
INSERT INTO `calendar` VALUES ('2021-01-25');
INSERT INTO `calendar` VALUES ('2021-01-26');
INSERT INTO `calendar` VALUES ('2021-01-27');
INSERT INTO `calendar` VALUES ('2021-01-28');
INSERT INTO `calendar` VALUES ('2021-01-29');
INSERT INTO `calendar` VALUES ('2021-01-30');
INSERT INTO `calendar` VALUES ('2021-01-31');
INSERT INTO `calendar` VALUES ('2021-02-01');
INSERT INTO `calendar` VALUES ('2021-02-02');
INSERT INTO `calendar` VALUES ('2021-02-03');
INSERT INTO `calendar` VALUES ('2021-02-04');
INSERT INTO `calendar` VALUES ('2021-02-05');
INSERT INTO `calendar` VALUES ('2021-02-06');
INSERT INTO `calendar` VALUES ('2021-02-07');
INSERT INTO `calendar` VALUES ('2021-02-08');
INSERT INTO `calendar` VALUES ('2021-02-09');
INSERT INTO `calendar` VALUES ('2021-02-10');
INSERT INTO `calendar` VALUES ('2021-02-11');
INSERT INTO `calendar` VALUES ('2021-02-12');
INSERT INTO `calendar` VALUES ('2021-02-13');
INSERT INTO `calendar` VALUES ('2021-02-14');
INSERT INTO `calendar` VALUES ('2021-02-15');
INSERT INTO `calendar` VALUES ('2021-02-16');
INSERT INTO `calendar` VALUES ('2021-02-17');
INSERT INTO `calendar` VALUES ('2021-02-18');
INSERT INTO `calendar` VALUES ('2021-02-19');
INSERT INTO `calendar` VALUES ('2021-02-20');
INSERT INTO `calendar` VALUES ('2021-02-21');
INSERT INTO `calendar` VALUES ('2021-02-22');
INSERT INTO `calendar` VALUES ('2021-02-23');
INSERT INTO `calendar` VALUES ('2021-02-24');
INSERT INTO `calendar` VALUES ('2021-02-25');
INSERT INTO `calendar` VALUES ('2021-02-26');
INSERT INTO `calendar` VALUES ('2021-02-27');
INSERT INTO `calendar` VALUES ('2021-02-28');
INSERT INTO `calendar` VALUES ('2021-03-01');
INSERT INTO `calendar` VALUES ('2021-03-02');
INSERT INTO `calendar` VALUES ('2021-03-03');
INSERT INTO `calendar` VALUES ('2021-03-04');
INSERT INTO `calendar` VALUES ('2021-03-05');
INSERT INTO `calendar` VALUES ('2021-03-06');
INSERT INTO `calendar` VALUES ('2021-03-07');
INSERT INTO `calendar` VALUES ('2021-03-08');
INSERT INTO `calendar` VALUES ('2021-03-09');
INSERT INTO `calendar` VALUES ('2021-03-10');
INSERT INTO `calendar` VALUES ('2021-03-11');
INSERT INTO `calendar` VALUES ('2021-03-12');
INSERT INTO `calendar` VALUES ('2021-03-13');
INSERT INTO `calendar` VALUES ('2021-03-14');
INSERT INTO `calendar` VALUES ('2021-03-15');
INSERT INTO `calendar` VALUES ('2021-03-16');
INSERT INTO `calendar` VALUES ('2021-03-17');
INSERT INTO `calendar` VALUES ('2021-03-18');
INSERT INTO `calendar` VALUES ('2021-03-19');
INSERT INTO `calendar` VALUES ('2021-03-20');
INSERT INTO `calendar` VALUES ('2021-03-21');
INSERT INTO `calendar` VALUES ('2021-03-22');
INSERT INTO `calendar` VALUES ('2021-03-23');
INSERT INTO `calendar` VALUES ('2021-03-24');
INSERT INTO `calendar` VALUES ('2021-03-25');
INSERT INTO `calendar` VALUES ('2021-03-26');
INSERT INTO `calendar` VALUES ('2021-03-27');
INSERT INTO `calendar` VALUES ('2021-03-28');
INSERT INTO `calendar` VALUES ('2021-03-29');
INSERT INTO `calendar` VALUES ('2021-03-30');
INSERT INTO `calendar` VALUES ('2021-03-31');
INSERT INTO `calendar` VALUES ('2021-04-01');
INSERT INTO `calendar` VALUES ('2021-04-02');
INSERT INTO `calendar` VALUES ('2021-04-03');
INSERT INTO `calendar` VALUES ('2021-04-04');
INSERT INTO `calendar` VALUES ('2021-04-05');
INSERT INTO `calendar` VALUES ('2021-04-06');
INSERT INTO `calendar` VALUES ('2021-04-07');
INSERT INTO `calendar` VALUES ('2021-04-08');
INSERT INTO `calendar` VALUES ('2021-04-09');
INSERT INTO `calendar` VALUES ('2021-04-10');
INSERT INTO `calendar` VALUES ('2021-04-11');
INSERT INTO `calendar` VALUES ('2021-04-12');
INSERT INTO `calendar` VALUES ('2021-04-13');
INSERT INTO `calendar` VALUES ('2021-04-14');
INSERT INTO `calendar` VALUES ('2021-04-15');
INSERT INTO `calendar` VALUES ('2021-04-16');
INSERT INTO `calendar` VALUES ('2021-04-17');
INSERT INTO `calendar` VALUES ('2021-04-18');
INSERT INTO `calendar` VALUES ('2021-04-19');
INSERT INTO `calendar` VALUES ('2021-04-20');
INSERT INTO `calendar` VALUES ('2021-04-21');
INSERT INTO `calendar` VALUES ('2021-04-22');
INSERT INTO `calendar` VALUES ('2021-04-23');
INSERT INTO `calendar` VALUES ('2021-04-24');
INSERT INTO `calendar` VALUES ('2021-04-25');
INSERT INTO `calendar` VALUES ('2021-04-26');
INSERT INTO `calendar` VALUES ('2021-04-27');
INSERT INTO `calendar` VALUES ('2021-04-28');
INSERT INTO `calendar` VALUES ('2021-04-29');
INSERT INTO `calendar` VALUES ('2021-04-30');
INSERT INTO `calendar` VALUES ('2021-05-01');
INSERT INTO `calendar` VALUES ('2021-05-02');
INSERT INTO `calendar` VALUES ('2021-05-03');
INSERT INTO `calendar` VALUES ('2021-05-04');
INSERT INTO `calendar` VALUES ('2021-05-05');
INSERT INTO `calendar` VALUES ('2021-05-06');
INSERT INTO `calendar` VALUES ('2021-05-07');
INSERT INTO `calendar` VALUES ('2021-05-08');
INSERT INTO `calendar` VALUES ('2021-05-09');
INSERT INTO `calendar` VALUES ('2021-05-10');
INSERT INTO `calendar` VALUES ('2021-05-11');
INSERT INTO `calendar` VALUES ('2021-05-12');
INSERT INTO `calendar` VALUES ('2021-05-13');
INSERT INTO `calendar` VALUES ('2021-05-14');
INSERT INTO `calendar` VALUES ('2021-05-15');
INSERT INTO `calendar` VALUES ('2021-05-16');
INSERT INTO `calendar` VALUES ('2021-05-17');
INSERT INTO `calendar` VALUES ('2021-05-18');
INSERT INTO `calendar` VALUES ('2021-05-19');
INSERT INTO `calendar` VALUES ('2021-05-20');
INSERT INTO `calendar` VALUES ('2021-05-21');
INSERT INTO `calendar` VALUES ('2021-05-22');
INSERT INTO `calendar` VALUES ('2021-05-23');
INSERT INTO `calendar` VALUES ('2021-05-24');
INSERT INTO `calendar` VALUES ('2021-05-25');
INSERT INTO `calendar` VALUES ('2021-05-26');
INSERT INTO `calendar` VALUES ('2021-05-27');
INSERT INTO `calendar` VALUES ('2021-05-28');
INSERT INTO `calendar` VALUES ('2021-05-29');
INSERT INTO `calendar` VALUES ('2021-05-30');
INSERT INTO `calendar` VALUES ('2021-05-31');
INSERT INTO `calendar` VALUES ('2021-06-01');
INSERT INTO `calendar` VALUES ('2021-06-02');
INSERT INTO `calendar` VALUES ('2021-06-03');
INSERT INTO `calendar` VALUES ('2021-06-04');
INSERT INTO `calendar` VALUES ('2021-06-05');
INSERT INTO `calendar` VALUES ('2021-06-06');
INSERT INTO `calendar` VALUES ('2021-06-07');
INSERT INTO `calendar` VALUES ('2021-06-08');
INSERT INTO `calendar` VALUES ('2021-06-09');
INSERT INTO `calendar` VALUES ('2021-06-10');
INSERT INTO `calendar` VALUES ('2021-06-11');
INSERT INTO `calendar` VALUES ('2021-06-12');
INSERT INTO `calendar` VALUES ('2021-06-13');
INSERT INTO `calendar` VALUES ('2021-06-14');
INSERT INTO `calendar` VALUES ('2021-06-15');
INSERT INTO `calendar` VALUES ('2021-06-16');
INSERT INTO `calendar` VALUES ('2021-06-17');
INSERT INTO `calendar` VALUES ('2021-06-18');
INSERT INTO `calendar` VALUES ('2021-06-19');
INSERT INTO `calendar` VALUES ('2021-06-20');
INSERT INTO `calendar` VALUES ('2021-06-21');
INSERT INTO `calendar` VALUES ('2021-06-22');
INSERT INTO `calendar` VALUES ('2021-06-23');
INSERT INTO `calendar` VALUES ('2021-06-24');
INSERT INTO `calendar` VALUES ('2021-06-25');
INSERT INTO `calendar` VALUES ('2021-06-26');
INSERT INTO `calendar` VALUES ('2021-06-27');
INSERT INTO `calendar` VALUES ('2021-06-28');
INSERT INTO `calendar` VALUES ('2021-06-29');
INSERT INTO `calendar` VALUES ('2021-06-30');
INSERT INTO `calendar` VALUES ('2021-07-01');
INSERT INTO `calendar` VALUES ('2021-07-02');
INSERT INTO `calendar` VALUES ('2021-07-03');
INSERT INTO `calendar` VALUES ('2021-07-04');
INSERT INTO `calendar` VALUES ('2021-07-05');
INSERT INTO `calendar` VALUES ('2021-07-06');
INSERT INTO `calendar` VALUES ('2021-07-07');
INSERT INTO `calendar` VALUES ('2021-07-08');
INSERT INTO `calendar` VALUES ('2021-07-09');
INSERT INTO `calendar` VALUES ('2021-07-10');
INSERT INTO `calendar` VALUES ('2021-07-11');
INSERT INTO `calendar` VALUES ('2021-07-12');
INSERT INTO `calendar` VALUES ('2021-07-13');
INSERT INTO `calendar` VALUES ('2021-07-14');
INSERT INTO `calendar` VALUES ('2021-07-15');
INSERT INTO `calendar` VALUES ('2021-07-16');
INSERT INTO `calendar` VALUES ('2021-07-17');
INSERT INTO `calendar` VALUES ('2021-07-18');
INSERT INTO `calendar` VALUES ('2021-07-19');
INSERT INTO `calendar` VALUES ('2021-07-20');
INSERT INTO `calendar` VALUES ('2021-07-21');
INSERT INTO `calendar` VALUES ('2021-07-22');
INSERT INTO `calendar` VALUES ('2021-07-23');
INSERT INTO `calendar` VALUES ('2021-07-24');
INSERT INTO `calendar` VALUES ('2021-07-25');
INSERT INTO `calendar` VALUES ('2021-07-26');
INSERT INTO `calendar` VALUES ('2021-07-27');
INSERT INTO `calendar` VALUES ('2021-07-28');
INSERT INTO `calendar` VALUES ('2021-07-29');
INSERT INTO `calendar` VALUES ('2021-07-30');
INSERT INTO `calendar` VALUES ('2021-07-31');
INSERT INTO `calendar` VALUES ('2021-08-01');
INSERT INTO `calendar` VALUES ('2021-08-02');
INSERT INTO `calendar` VALUES ('2021-08-03');
INSERT INTO `calendar` VALUES ('2021-08-04');
INSERT INTO `calendar` VALUES ('2021-08-05');
INSERT INTO `calendar` VALUES ('2021-08-06');
INSERT INTO `calendar` VALUES ('2021-08-07');
INSERT INTO `calendar` VALUES ('2021-08-08');
INSERT INTO `calendar` VALUES ('2021-08-09');
INSERT INTO `calendar` VALUES ('2021-08-10');
INSERT INTO `calendar` VALUES ('2021-08-11');
INSERT INTO `calendar` VALUES ('2021-08-12');
INSERT INTO `calendar` VALUES ('2021-08-13');
INSERT INTO `calendar` VALUES ('2021-08-14');
INSERT INTO `calendar` VALUES ('2021-08-15');
INSERT INTO `calendar` VALUES ('2021-08-16');
INSERT INTO `calendar` VALUES ('2021-08-17');
INSERT INTO `calendar` VALUES ('2021-08-18');
INSERT INTO `calendar` VALUES ('2021-08-19');
INSERT INTO `calendar` VALUES ('2021-08-20');
INSERT INTO `calendar` VALUES ('2021-08-21');
INSERT INTO `calendar` VALUES ('2021-08-22');
INSERT INTO `calendar` VALUES ('2021-08-23');
INSERT INTO `calendar` VALUES ('2021-08-24');
INSERT INTO `calendar` VALUES ('2021-08-25');
INSERT INTO `calendar` VALUES ('2021-08-26');
INSERT INTO `calendar` VALUES ('2021-08-27');
INSERT INTO `calendar` VALUES ('2021-08-28');
INSERT INTO `calendar` VALUES ('2021-08-29');
INSERT INTO `calendar` VALUES ('2021-08-30');
INSERT INTO `calendar` VALUES ('2021-08-31');
INSERT INTO `calendar` VALUES ('2021-09-01');
INSERT INTO `calendar` VALUES ('2021-09-02');
INSERT INTO `calendar` VALUES ('2021-09-03');
INSERT INTO `calendar` VALUES ('2021-09-04');
INSERT INTO `calendar` VALUES ('2021-09-05');
INSERT INTO `calendar` VALUES ('2021-09-06');
INSERT INTO `calendar` VALUES ('2021-09-07');
INSERT INTO `calendar` VALUES ('2021-09-08');
INSERT INTO `calendar` VALUES ('2021-09-09');
INSERT INTO `calendar` VALUES ('2021-09-10');
INSERT INTO `calendar` VALUES ('2021-09-11');
INSERT INTO `calendar` VALUES ('2021-09-12');
INSERT INTO `calendar` VALUES ('2021-09-13');
INSERT INTO `calendar` VALUES ('2021-09-14');
INSERT INTO `calendar` VALUES ('2021-09-15');
INSERT INTO `calendar` VALUES ('2021-09-16');
INSERT INTO `calendar` VALUES ('2021-09-17');
INSERT INTO `calendar` VALUES ('2021-09-18');
INSERT INTO `calendar` VALUES ('2021-09-19');
INSERT INTO `calendar` VALUES ('2021-09-20');
INSERT INTO `calendar` VALUES ('2021-09-21');
INSERT INTO `calendar` VALUES ('2021-09-22');
INSERT INTO `calendar` VALUES ('2021-09-23');
INSERT INTO `calendar` VALUES ('2021-09-24');
INSERT INTO `calendar` VALUES ('2021-09-25');
INSERT INTO `calendar` VALUES ('2021-09-26');
INSERT INTO `calendar` VALUES ('2021-09-27');
INSERT INTO `calendar` VALUES ('2021-09-28');
INSERT INTO `calendar` VALUES ('2021-09-29');
INSERT INTO `calendar` VALUES ('2021-09-30');
INSERT INTO `calendar` VALUES ('2021-10-01');
INSERT INTO `calendar` VALUES ('2021-10-02');
INSERT INTO `calendar` VALUES ('2021-10-03');
INSERT INTO `calendar` VALUES ('2021-10-04');
INSERT INTO `calendar` VALUES ('2021-10-05');
INSERT INTO `calendar` VALUES ('2021-10-06');
INSERT INTO `calendar` VALUES ('2021-10-07');
INSERT INTO `calendar` VALUES ('2021-10-08');
INSERT INTO `calendar` VALUES ('2021-10-09');
INSERT INTO `calendar` VALUES ('2021-10-10');
INSERT INTO `calendar` VALUES ('2021-10-11');
INSERT INTO `calendar` VALUES ('2021-10-12');
INSERT INTO `calendar` VALUES ('2021-10-13');
INSERT INTO `calendar` VALUES ('2021-10-14');
INSERT INTO `calendar` VALUES ('2021-10-15');
INSERT INTO `calendar` VALUES ('2021-10-16');
INSERT INTO `calendar` VALUES ('2021-10-17');
INSERT INTO `calendar` VALUES ('2021-10-18');
INSERT INTO `calendar` VALUES ('2021-10-19');
INSERT INTO `calendar` VALUES ('2021-10-20');
INSERT INTO `calendar` VALUES ('2021-10-21');
INSERT INTO `calendar` VALUES ('2021-10-22');
INSERT INTO `calendar` VALUES ('2021-10-23');
INSERT INTO `calendar` VALUES ('2021-10-24');
INSERT INTO `calendar` VALUES ('2021-10-25');
INSERT INTO `calendar` VALUES ('2021-10-26');
INSERT INTO `calendar` VALUES ('2021-10-27');
INSERT INTO `calendar` VALUES ('2021-10-28');
INSERT INTO `calendar` VALUES ('2021-10-29');
INSERT INTO `calendar` VALUES ('2021-10-30');
INSERT INTO `calendar` VALUES ('2021-10-31');
INSERT INTO `calendar` VALUES ('2021-11-01');
INSERT INTO `calendar` VALUES ('2021-11-02');
INSERT INTO `calendar` VALUES ('2021-11-03');
INSERT INTO `calendar` VALUES ('2021-11-04');
INSERT INTO `calendar` VALUES ('2021-11-05');
INSERT INTO `calendar` VALUES ('2021-11-06');
INSERT INTO `calendar` VALUES ('2021-11-07');
INSERT INTO `calendar` VALUES ('2021-11-08');
INSERT INTO `calendar` VALUES ('2021-11-09');
INSERT INTO `calendar` VALUES ('2021-11-10');
INSERT INTO `calendar` VALUES ('2021-11-11');
INSERT INTO `calendar` VALUES ('2021-11-12');
INSERT INTO `calendar` VALUES ('2021-11-13');
INSERT INTO `calendar` VALUES ('2021-11-14');
INSERT INTO `calendar` VALUES ('2021-11-15');
INSERT INTO `calendar` VALUES ('2021-11-16');
INSERT INTO `calendar` VALUES ('2021-11-17');
INSERT INTO `calendar` VALUES ('2021-11-18');
INSERT INTO `calendar` VALUES ('2021-11-19');
INSERT INTO `calendar` VALUES ('2021-11-20');
INSERT INTO `calendar` VALUES ('2021-11-21');
INSERT INTO `calendar` VALUES ('2021-11-22');
INSERT INTO `calendar` VALUES ('2021-11-23');
INSERT INTO `calendar` VALUES ('2021-11-24');
INSERT INTO `calendar` VALUES ('2021-11-25');
INSERT INTO `calendar` VALUES ('2021-11-26');
INSERT INTO `calendar` VALUES ('2021-11-27');
INSERT INTO `calendar` VALUES ('2021-11-28');
INSERT INTO `calendar` VALUES ('2021-11-29');
INSERT INTO `calendar` VALUES ('2021-11-30');
INSERT INTO `calendar` VALUES ('2021-12-01');
INSERT INTO `calendar` VALUES ('2021-12-02');
INSERT INTO `calendar` VALUES ('2021-12-03');
INSERT INTO `calendar` VALUES ('2021-12-04');
INSERT INTO `calendar` VALUES ('2021-12-05');
INSERT INTO `calendar` VALUES ('2021-12-06');
INSERT INTO `calendar` VALUES ('2021-12-07');
INSERT INTO `calendar` VALUES ('2021-12-08');
INSERT INTO `calendar` VALUES ('2021-12-09');
INSERT INTO `calendar` VALUES ('2021-12-10');
INSERT INTO `calendar` VALUES ('2021-12-11');
INSERT INTO `calendar` VALUES ('2021-12-12');
INSERT INTO `calendar` VALUES ('2021-12-13');
INSERT INTO `calendar` VALUES ('2021-12-14');
INSERT INTO `calendar` VALUES ('2021-12-15');
INSERT INTO `calendar` VALUES ('2021-12-16');
INSERT INTO `calendar` VALUES ('2021-12-17');
INSERT INTO `calendar` VALUES ('2021-12-18');
INSERT INTO `calendar` VALUES ('2021-12-19');
INSERT INTO `calendar` VALUES ('2021-12-20');
INSERT INTO `calendar` VALUES ('2021-12-21');
INSERT INTO `calendar` VALUES ('2021-12-22');
INSERT INTO `calendar` VALUES ('2021-12-23');
INSERT INTO `calendar` VALUES ('2021-12-24');
INSERT INTO `calendar` VALUES ('2021-12-25');
INSERT INTO `calendar` VALUES ('2021-12-26');
INSERT INTO `calendar` VALUES ('2021-12-27');
INSERT INTO `calendar` VALUES ('2021-12-28');
INSERT INTO `calendar` VALUES ('2021-12-29');
INSERT INTO `calendar` VALUES ('2021-12-30');
INSERT INTO `calendar` VALUES ('2021-12-31');
INSERT INTO `calendar` VALUES ('2022-01-01');
INSERT INTO `calendar` VALUES ('2022-01-02');
INSERT INTO `calendar` VALUES ('2022-01-03');
INSERT INTO `calendar` VALUES ('2022-01-04');
INSERT INTO `calendar` VALUES ('2022-01-05');
INSERT INTO `calendar` VALUES ('2022-01-06');
INSERT INTO `calendar` VALUES ('2022-01-07');
INSERT INTO `calendar` VALUES ('2022-01-08');
INSERT INTO `calendar` VALUES ('2022-01-09');
INSERT INTO `calendar` VALUES ('2022-01-10');
INSERT INTO `calendar` VALUES ('2022-01-11');
INSERT INTO `calendar` VALUES ('2022-01-12');
INSERT INTO `calendar` VALUES ('2022-01-13');
INSERT INTO `calendar` VALUES ('2022-01-14');
INSERT INTO `calendar` VALUES ('2022-01-15');
INSERT INTO `calendar` VALUES ('2022-01-16');
INSERT INTO `calendar` VALUES ('2022-01-17');
INSERT INTO `calendar` VALUES ('2022-01-18');
INSERT INTO `calendar` VALUES ('2022-01-19');
INSERT INTO `calendar` VALUES ('2022-01-20');
INSERT INTO `calendar` VALUES ('2022-01-21');
INSERT INTO `calendar` VALUES ('2022-01-22');
INSERT INTO `calendar` VALUES ('2022-01-23');
INSERT INTO `calendar` VALUES ('2022-01-24');
INSERT INTO `calendar` VALUES ('2022-01-25');
INSERT INTO `calendar` VALUES ('2022-01-26');
INSERT INTO `calendar` VALUES ('2022-01-27');
INSERT INTO `calendar` VALUES ('2022-01-28');
INSERT INTO `calendar` VALUES ('2022-01-29');
INSERT INTO `calendar` VALUES ('2022-01-30');
INSERT INTO `calendar` VALUES ('2022-01-31');
INSERT INTO `calendar` VALUES ('2022-02-01');
INSERT INTO `calendar` VALUES ('2022-02-02');
INSERT INTO `calendar` VALUES ('2022-02-03');
INSERT INTO `calendar` VALUES ('2022-02-04');
INSERT INTO `calendar` VALUES ('2022-02-05');
INSERT INTO `calendar` VALUES ('2022-02-06');
INSERT INTO `calendar` VALUES ('2022-02-07');
INSERT INTO `calendar` VALUES ('2022-02-08');
INSERT INTO `calendar` VALUES ('2022-02-09');
INSERT INTO `calendar` VALUES ('2022-02-10');
INSERT INTO `calendar` VALUES ('2022-02-11');
INSERT INTO `calendar` VALUES ('2022-02-12');
INSERT INTO `calendar` VALUES ('2022-02-13');
INSERT INTO `calendar` VALUES ('2022-02-14');
INSERT INTO `calendar` VALUES ('2022-02-15');
INSERT INTO `calendar` VALUES ('2022-02-16');
INSERT INTO `calendar` VALUES ('2022-02-17');
INSERT INTO `calendar` VALUES ('2022-02-18');
INSERT INTO `calendar` VALUES ('2022-02-19');
INSERT INTO `calendar` VALUES ('2022-02-20');
INSERT INTO `calendar` VALUES ('2022-02-21');
INSERT INTO `calendar` VALUES ('2022-02-22');
INSERT INTO `calendar` VALUES ('2022-02-23');
INSERT INTO `calendar` VALUES ('2022-02-24');
INSERT INTO `calendar` VALUES ('2022-02-25');
INSERT INTO `calendar` VALUES ('2022-02-26');
INSERT INTO `calendar` VALUES ('2022-02-27');
INSERT INTO `calendar` VALUES ('2022-02-28');
INSERT INTO `calendar` VALUES ('2022-03-01');
INSERT INTO `calendar` VALUES ('2022-03-02');
INSERT INTO `calendar` VALUES ('2022-03-03');
INSERT INTO `calendar` VALUES ('2022-03-04');
INSERT INTO `calendar` VALUES ('2022-03-05');
INSERT INTO `calendar` VALUES ('2022-03-06');
INSERT INTO `calendar` VALUES ('2022-03-07');
INSERT INTO `calendar` VALUES ('2022-03-08');
INSERT INTO `calendar` VALUES ('2022-03-09');
INSERT INTO `calendar` VALUES ('2022-03-10');
INSERT INTO `calendar` VALUES ('2022-03-11');
INSERT INTO `calendar` VALUES ('2022-03-12');
INSERT INTO `calendar` VALUES ('2022-03-13');
INSERT INTO `calendar` VALUES ('2022-03-14');
INSERT INTO `calendar` VALUES ('2022-03-15');
INSERT INTO `calendar` VALUES ('2022-03-16');
INSERT INTO `calendar` VALUES ('2022-03-17');
INSERT INTO `calendar` VALUES ('2022-03-18');
INSERT INTO `calendar` VALUES ('2022-03-19');
INSERT INTO `calendar` VALUES ('2022-03-20');
INSERT INTO `calendar` VALUES ('2022-03-21');
INSERT INTO `calendar` VALUES ('2022-03-22');
INSERT INTO `calendar` VALUES ('2022-03-23');
INSERT INTO `calendar` VALUES ('2022-03-24');
INSERT INTO `calendar` VALUES ('2022-03-25');
INSERT INTO `calendar` VALUES ('2022-03-26');
INSERT INTO `calendar` VALUES ('2022-03-27');
INSERT INTO `calendar` VALUES ('2022-03-28');
INSERT INTO `calendar` VALUES ('2022-03-29');
INSERT INTO `calendar` VALUES ('2022-03-30');
INSERT INTO `calendar` VALUES ('2022-03-31');
INSERT INTO `calendar` VALUES ('2022-04-01');
INSERT INTO `calendar` VALUES ('2022-04-02');
INSERT INTO `calendar` VALUES ('2022-04-03');
INSERT INTO `calendar` VALUES ('2022-04-04');
INSERT INTO `calendar` VALUES ('2022-04-05');
INSERT INTO `calendar` VALUES ('2022-04-06');
INSERT INTO `calendar` VALUES ('2022-04-07');
INSERT INTO `calendar` VALUES ('2022-04-08');
INSERT INTO `calendar` VALUES ('2022-04-09');
INSERT INTO `calendar` VALUES ('2022-04-10');
INSERT INTO `calendar` VALUES ('2022-04-11');
INSERT INTO `calendar` VALUES ('2022-04-12');
INSERT INTO `calendar` VALUES ('2022-04-13');
INSERT INTO `calendar` VALUES ('2022-04-14');
INSERT INTO `calendar` VALUES ('2022-04-15');
INSERT INTO `calendar` VALUES ('2022-04-16');
INSERT INTO `calendar` VALUES ('2022-04-17');
INSERT INTO `calendar` VALUES ('2022-04-18');
INSERT INTO `calendar` VALUES ('2022-04-19');
INSERT INTO `calendar` VALUES ('2022-04-20');
INSERT INTO `calendar` VALUES ('2022-04-21');
INSERT INTO `calendar` VALUES ('2022-04-22');
INSERT INTO `calendar` VALUES ('2022-04-23');
INSERT INTO `calendar` VALUES ('2022-04-24');
INSERT INTO `calendar` VALUES ('2022-04-25');
INSERT INTO `calendar` VALUES ('2022-04-26');
INSERT INTO `calendar` VALUES ('2022-04-27');
INSERT INTO `calendar` VALUES ('2022-04-28');
INSERT INTO `calendar` VALUES ('2022-04-29');
INSERT INTO `calendar` VALUES ('2022-04-30');
INSERT INTO `calendar` VALUES ('2022-05-01');
INSERT INTO `calendar` VALUES ('2022-05-02');
INSERT INTO `calendar` VALUES ('2022-05-03');
INSERT INTO `calendar` VALUES ('2022-05-04');
INSERT INTO `calendar` VALUES ('2022-05-05');
INSERT INTO `calendar` VALUES ('2022-05-06');
INSERT INTO `calendar` VALUES ('2022-05-07');
INSERT INTO `calendar` VALUES ('2022-05-08');
INSERT INTO `calendar` VALUES ('2022-05-09');
INSERT INTO `calendar` VALUES ('2022-05-10');
INSERT INTO `calendar` VALUES ('2022-05-11');
INSERT INTO `calendar` VALUES ('2022-05-12');
INSERT INTO `calendar` VALUES ('2022-05-13');
INSERT INTO `calendar` VALUES ('2022-05-14');
INSERT INTO `calendar` VALUES ('2022-05-15');
INSERT INTO `calendar` VALUES ('2022-05-16');
INSERT INTO `calendar` VALUES ('2022-05-17');
INSERT INTO `calendar` VALUES ('2022-05-18');
INSERT INTO `calendar` VALUES ('2022-05-19');
INSERT INTO `calendar` VALUES ('2022-05-20');
INSERT INTO `calendar` VALUES ('2022-05-21');
INSERT INTO `calendar` VALUES ('2022-05-22');
INSERT INTO `calendar` VALUES ('2022-05-23');
INSERT INTO `calendar` VALUES ('2022-05-24');
INSERT INTO `calendar` VALUES ('2022-05-25');
INSERT INTO `calendar` VALUES ('2022-05-26');
INSERT INTO `calendar` VALUES ('2022-05-27');
INSERT INTO `calendar` VALUES ('2022-05-28');
INSERT INTO `calendar` VALUES ('2022-05-29');
INSERT INTO `calendar` VALUES ('2022-05-30');
INSERT INTO `calendar` VALUES ('2022-05-31');
INSERT INTO `calendar` VALUES ('2022-06-01');
INSERT INTO `calendar` VALUES ('2022-06-02');
INSERT INTO `calendar` VALUES ('2022-06-03');
INSERT INTO `calendar` VALUES ('2022-06-04');
INSERT INTO `calendar` VALUES ('2022-06-05');
INSERT INTO `calendar` VALUES ('2022-06-06');
INSERT INTO `calendar` VALUES ('2022-06-07');
INSERT INTO `calendar` VALUES ('2022-06-08');
INSERT INTO `calendar` VALUES ('2022-06-09');
INSERT INTO `calendar` VALUES ('2022-06-10');
INSERT INTO `calendar` VALUES ('2022-06-11');
INSERT INTO `calendar` VALUES ('2022-06-12');
INSERT INTO `calendar` VALUES ('2022-06-13');
INSERT INTO `calendar` VALUES ('2022-06-14');
INSERT INTO `calendar` VALUES ('2022-06-15');
INSERT INTO `calendar` VALUES ('2022-06-16');
INSERT INTO `calendar` VALUES ('2022-06-17');
INSERT INTO `calendar` VALUES ('2022-06-18');
INSERT INTO `calendar` VALUES ('2022-06-19');
INSERT INTO `calendar` VALUES ('2022-06-20');
INSERT INTO `calendar` VALUES ('2022-06-21');
INSERT INTO `calendar` VALUES ('2022-06-22');
INSERT INTO `calendar` VALUES ('2022-06-23');
INSERT INTO `calendar` VALUES ('2022-06-24');
INSERT INTO `calendar` VALUES ('2022-06-25');
INSERT INTO `calendar` VALUES ('2022-06-26');
INSERT INTO `calendar` VALUES ('2022-06-27');
INSERT INTO `calendar` VALUES ('2022-06-28');
INSERT INTO `calendar` VALUES ('2022-06-29');
INSERT INTO `calendar` VALUES ('2022-06-30');
INSERT INTO `calendar` VALUES ('2022-07-01');
INSERT INTO `calendar` VALUES ('2022-07-02');
INSERT INTO `calendar` VALUES ('2022-07-03');
INSERT INTO `calendar` VALUES ('2022-07-04');
INSERT INTO `calendar` VALUES ('2022-07-05');
INSERT INTO `calendar` VALUES ('2022-07-06');
INSERT INTO `calendar` VALUES ('2022-07-07');
INSERT INTO `calendar` VALUES ('2022-07-08');
INSERT INTO `calendar` VALUES ('2022-07-09');
INSERT INTO `calendar` VALUES ('2022-07-10');
INSERT INTO `calendar` VALUES ('2022-07-11');
INSERT INTO `calendar` VALUES ('2022-07-12');
INSERT INTO `calendar` VALUES ('2022-07-13');
INSERT INTO `calendar` VALUES ('2022-07-14');
INSERT INTO `calendar` VALUES ('2022-07-15');
INSERT INTO `calendar` VALUES ('2022-07-16');
INSERT INTO `calendar` VALUES ('2022-07-17');
INSERT INTO `calendar` VALUES ('2022-07-18');
INSERT INTO `calendar` VALUES ('2022-07-19');
INSERT INTO `calendar` VALUES ('2022-07-20');
INSERT INTO `calendar` VALUES ('2022-07-21');
INSERT INTO `calendar` VALUES ('2022-07-22');
INSERT INTO `calendar` VALUES ('2022-07-23');
INSERT INTO `calendar` VALUES ('2022-07-24');
INSERT INTO `calendar` VALUES ('2022-07-25');
INSERT INTO `calendar` VALUES ('2022-07-26');
INSERT INTO `calendar` VALUES ('2022-07-27');
INSERT INTO `calendar` VALUES ('2022-07-28');
INSERT INTO `calendar` VALUES ('2022-07-29');
INSERT INTO `calendar` VALUES ('2022-07-30');
INSERT INTO `calendar` VALUES ('2022-07-31');
INSERT INTO `calendar` VALUES ('2022-08-01');
INSERT INTO `calendar` VALUES ('2022-08-02');
INSERT INTO `calendar` VALUES ('2022-08-03');
INSERT INTO `calendar` VALUES ('2022-08-04');
INSERT INTO `calendar` VALUES ('2022-08-05');
INSERT INTO `calendar` VALUES ('2022-08-06');
INSERT INTO `calendar` VALUES ('2022-08-07');
INSERT INTO `calendar` VALUES ('2022-08-08');
INSERT INTO `calendar` VALUES ('2022-08-09');
INSERT INTO `calendar` VALUES ('2022-08-10');
INSERT INTO `calendar` VALUES ('2022-08-11');
INSERT INTO `calendar` VALUES ('2022-08-12');
INSERT INTO `calendar` VALUES ('2022-08-13');
INSERT INTO `calendar` VALUES ('2022-08-14');
INSERT INTO `calendar` VALUES ('2022-08-15');
INSERT INTO `calendar` VALUES ('2022-08-16');
INSERT INTO `calendar` VALUES ('2022-08-17');
INSERT INTO `calendar` VALUES ('2022-08-18');
INSERT INTO `calendar` VALUES ('2022-08-19');
INSERT INTO `calendar` VALUES ('2022-08-20');
INSERT INTO `calendar` VALUES ('2022-08-21');
INSERT INTO `calendar` VALUES ('2022-08-22');
INSERT INTO `calendar` VALUES ('2022-08-23');
INSERT INTO `calendar` VALUES ('2022-08-24');
INSERT INTO `calendar` VALUES ('2022-08-25');
INSERT INTO `calendar` VALUES ('2022-08-26');
INSERT INTO `calendar` VALUES ('2022-08-27');
INSERT INTO `calendar` VALUES ('2022-08-28');
INSERT INTO `calendar` VALUES ('2022-08-29');
INSERT INTO `calendar` VALUES ('2022-08-30');
INSERT INTO `calendar` VALUES ('2022-08-31');
INSERT INTO `calendar` VALUES ('2022-09-01');
INSERT INTO `calendar` VALUES ('2022-09-02');
INSERT INTO `calendar` VALUES ('2022-09-03');
INSERT INTO `calendar` VALUES ('2022-09-04');
INSERT INTO `calendar` VALUES ('2022-09-05');
INSERT INTO `calendar` VALUES ('2022-09-06');
INSERT INTO `calendar` VALUES ('2022-09-07');
INSERT INTO `calendar` VALUES ('2022-09-08');
INSERT INTO `calendar` VALUES ('2022-09-09');
INSERT INTO `calendar` VALUES ('2022-09-10');
INSERT INTO `calendar` VALUES ('2022-09-11');
INSERT INTO `calendar` VALUES ('2022-09-12');
INSERT INTO `calendar` VALUES ('2022-09-13');
INSERT INTO `calendar` VALUES ('2022-09-14');
INSERT INTO `calendar` VALUES ('2022-09-15');
INSERT INTO `calendar` VALUES ('2022-09-16');
INSERT INTO `calendar` VALUES ('2022-09-17');
INSERT INTO `calendar` VALUES ('2022-09-18');
INSERT INTO `calendar` VALUES ('2022-09-19');
INSERT INTO `calendar` VALUES ('2022-09-20');
INSERT INTO `calendar` VALUES ('2022-09-21');
INSERT INTO `calendar` VALUES ('2022-09-22');
INSERT INTO `calendar` VALUES ('2022-09-23');
INSERT INTO `calendar` VALUES ('2022-09-24');
INSERT INTO `calendar` VALUES ('2022-09-25');
INSERT INTO `calendar` VALUES ('2022-09-26');
INSERT INTO `calendar` VALUES ('2022-09-27');
INSERT INTO `calendar` VALUES ('2022-09-28');
INSERT INTO `calendar` VALUES ('2022-09-29');
INSERT INTO `calendar` VALUES ('2022-09-30');
INSERT INTO `calendar` VALUES ('2022-10-01');
INSERT INTO `calendar` VALUES ('2022-10-02');
INSERT INTO `calendar` VALUES ('2022-10-03');
INSERT INTO `calendar` VALUES ('2022-10-04');
INSERT INTO `calendar` VALUES ('2022-10-05');
INSERT INTO `calendar` VALUES ('2022-10-06');
INSERT INTO `calendar` VALUES ('2022-10-07');
INSERT INTO `calendar` VALUES ('2022-10-08');
INSERT INTO `calendar` VALUES ('2022-10-09');
INSERT INTO `calendar` VALUES ('2022-10-10');
INSERT INTO `calendar` VALUES ('2022-10-11');
INSERT INTO `calendar` VALUES ('2022-10-12');
INSERT INTO `calendar` VALUES ('2022-10-13');
INSERT INTO `calendar` VALUES ('2022-10-14');
INSERT INTO `calendar` VALUES ('2022-10-15');
INSERT INTO `calendar` VALUES ('2022-10-16');
INSERT INTO `calendar` VALUES ('2022-10-17');
INSERT INTO `calendar` VALUES ('2022-10-18');
INSERT INTO `calendar` VALUES ('2022-10-19');
INSERT INTO `calendar` VALUES ('2022-10-20');
INSERT INTO `calendar` VALUES ('2022-10-21');
INSERT INTO `calendar` VALUES ('2022-10-22');
INSERT INTO `calendar` VALUES ('2022-10-23');
INSERT INTO `calendar` VALUES ('2022-10-24');
INSERT INTO `calendar` VALUES ('2022-10-25');
INSERT INTO `calendar` VALUES ('2022-10-26');
INSERT INTO `calendar` VALUES ('2022-10-27');
INSERT INTO `calendar` VALUES ('2022-10-28');
INSERT INTO `calendar` VALUES ('2022-10-29');
INSERT INTO `calendar` VALUES ('2022-10-30');
INSERT INTO `calendar` VALUES ('2022-10-31');
INSERT INTO `calendar` VALUES ('2022-11-01');
INSERT INTO `calendar` VALUES ('2022-11-02');
INSERT INTO `calendar` VALUES ('2022-11-03');
INSERT INTO `calendar` VALUES ('2022-11-04');
INSERT INTO `calendar` VALUES ('2022-11-05');
INSERT INTO `calendar` VALUES ('2022-11-06');
INSERT INTO `calendar` VALUES ('2022-11-07');
INSERT INTO `calendar` VALUES ('2022-11-08');
INSERT INTO `calendar` VALUES ('2022-11-09');
INSERT INTO `calendar` VALUES ('2022-11-10');
INSERT INTO `calendar` VALUES ('2022-11-11');
INSERT INTO `calendar` VALUES ('2022-11-12');
INSERT INTO `calendar` VALUES ('2022-11-13');
INSERT INTO `calendar` VALUES ('2022-11-14');
INSERT INTO `calendar` VALUES ('2022-11-15');
INSERT INTO `calendar` VALUES ('2022-11-16');
INSERT INTO `calendar` VALUES ('2022-11-17');
INSERT INTO `calendar` VALUES ('2022-11-18');
INSERT INTO `calendar` VALUES ('2022-11-19');
INSERT INTO `calendar` VALUES ('2022-11-20');
INSERT INTO `calendar` VALUES ('2022-11-21');
INSERT INTO `calendar` VALUES ('2022-11-22');
INSERT INTO `calendar` VALUES ('2022-11-23');
INSERT INTO `calendar` VALUES ('2022-11-24');
INSERT INTO `calendar` VALUES ('2022-11-25');
INSERT INTO `calendar` VALUES ('2022-11-26');
INSERT INTO `calendar` VALUES ('2022-11-27');
INSERT INTO `calendar` VALUES ('2022-11-28');
INSERT INTO `calendar` VALUES ('2022-11-29');
INSERT INTO `calendar` VALUES ('2022-11-30');
INSERT INTO `calendar` VALUES ('2022-12-01');
INSERT INTO `calendar` VALUES ('2022-12-02');
INSERT INTO `calendar` VALUES ('2022-12-03');
INSERT INTO `calendar` VALUES ('2022-12-04');
INSERT INTO `calendar` VALUES ('2022-12-05');
INSERT INTO `calendar` VALUES ('2022-12-06');
INSERT INTO `calendar` VALUES ('2022-12-07');
INSERT INTO `calendar` VALUES ('2022-12-08');
INSERT INTO `calendar` VALUES ('2022-12-09');
INSERT INTO `calendar` VALUES ('2022-12-10');
INSERT INTO `calendar` VALUES ('2022-12-11');
INSERT INTO `calendar` VALUES ('2022-12-12');
INSERT INTO `calendar` VALUES ('2022-12-13');
INSERT INTO `calendar` VALUES ('2022-12-14');
INSERT INTO `calendar` VALUES ('2022-12-15');
INSERT INTO `calendar` VALUES ('2022-12-16');
INSERT INTO `calendar` VALUES ('2022-12-17');
INSERT INTO `calendar` VALUES ('2022-12-18');
INSERT INTO `calendar` VALUES ('2022-12-19');
INSERT INTO `calendar` VALUES ('2022-12-20');
INSERT INTO `calendar` VALUES ('2022-12-21');
INSERT INTO `calendar` VALUES ('2022-12-22');
INSERT INTO `calendar` VALUES ('2022-12-23');
INSERT INTO `calendar` VALUES ('2022-12-24');
INSERT INTO `calendar` VALUES ('2022-12-25');
INSERT INTO `calendar` VALUES ('2022-12-26');
INSERT INTO `calendar` VALUES ('2022-12-27');
INSERT INTO `calendar` VALUES ('2022-12-28');
INSERT INTO `calendar` VALUES ('2022-12-29');
INSERT INTO `calendar` VALUES ('2022-12-30');
INSERT INTO `calendar` VALUES ('2022-12-31');
COMMIT;

-- ----------------------------
-- Table structure for clClient
-- ----------------------------
DROP TABLE IF EXISTS `clClient`;
CREATE TABLE `clClient` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Alias` varchar(3) NOT NULL,
  `Seq` int(4) unsigned zerofill NOT NULL,
  `Code` varchar(10) NOT NULL,
  `Entity` smallint(5) unsigned NOT NULL,
  `divID` smallint(5) unsigned NOT NULL,
  `Name` varchar(100) NOT NULL,
  `NPWP` varchar(50) DEFAULT NULL,
  `LatestJob` smallint(2) NOT NULL,
  `Industry` int(11) NOT NULL,
  `ClientAreas` varchar(255) NOT NULL,
  `Flag` int(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastUpdateBy` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` datetime DEFAULT NULL,
  `DeletedBy` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `DeletedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `fkEntityID_client` (`Entity`),
  KEY `fkDivID_client` (`divID`),
  KEY `fkIndustryID_client` (`Industry`),
  CONSTRAINT `fkDivID_client` FOREIGN KEY (`divID`) REFERENCES `cmDivision` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fkEntityID_client` FOREIGN KEY (`Entity`) REFERENCES `cmEntity` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fkIndustryID_client` FOREIGN KEY (`Industry`) REFERENCES `clClientIndustries` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of clClient
-- ----------------------------
BEGIN;
INSERT INTO `clClient` VALUES (65, 'GST', 1477, 'GST-1477', 3, 2, 'GST PERSADA INDONESIA', '82.647.869.5-048.000', 11, 18, 'Office,A,E', 1, NULL, '2019-10-10 23:41:38', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (66, 'ABI', 0560, 'ABI-0560', 3, 2, 'ASURANSI BUANA INDEPENDENT', '01.305.801.1-038.000', 41, 15, 'Office,A,E', 1, NULL, '2019-10-11 00:26:58', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (67, 'AJM', 0862, 'AJM-0862', 3, 2, 'ASURANSI JIWA MEGA INDONESIA', '03.211.169.2-014.000', 7, 15, 'Office,A', 1, NULL, '2019-10-13 20:00:51', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (69, 'ACS', 0472, 'ACS-0472', 3, 2, 'ASIA CELLULAR SATELLITE', '01.711.096.6-217.000', 12, 1, 'Office,A,E', 1, NULL, '2019-10-15 22:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (70, 'AMS', 1248, 'AMS-1248', 3, 2, 'ALPHA MAKMUR SENTOSA INDONESIA', '03.040.175.6-047.000', 0, 9, 'Office,A,E', 1, NULL, '2019-10-15 22:56:36', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (71, 'ANU', 1574, 'ANU-1574', 3, 2, 'ANUGERAH TANI MAKMUR', '31.756.386.4-216.000', 0, 14, 'Office,A,E', 1, NULL, '2019-10-15 22:59:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (72, 'AOI', 1144, 'AOI-1144', 3, 2, 'ASIA BIOMASS INDONESIA', '03.343.582.7-072.000', 3, 14, 'Office,A,E', 1, NULL, '2019-10-15 23:00:57', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (73, 'APM', 0445, 'APM-0445', 3, 2, 'ADHI PUTRA MULIA', '01.312.655.2-038.000', 14, 5, 'Office,A,E', 1, NULL, '2019-10-15 23:02:47', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (74, 'ASG', 1482, 'ASG-1482', 3, 2, 'ACOMMERCE SOLUSI GEMILANG', '82.294.422.9-022.000', 1, 18, 'Office,A,E', 1, NULL, '2019-10-15 23:04:00', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (75, 'ASI', 0186, 'ASI-0186', 3, 2, 'ADVANCE STABILINDO INDUSTRY', '01.660.084.3-431.000', 12, 5, 'Office,A,E', 1, NULL, '2019-10-15 23:06:16', NULL, NULL, NULL, NULL);
INSERT INTO `clClient` VALUES (76, 'CBA', 0871, 'CBA-0871', 3, 2, 'CAKAP BERKARYA ABADI', '8282374787', 5, 11, 'Office,A,B,C', 1, NULL, '2019-10-18 17:13:03', NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for clClientIndustries
-- ----------------------------
DROP TABLE IF EXISTS `clClientIndustries`;
CREATE TABLE `clClientIndustries` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Industries` varchar(128) DEFAULT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  `Flag` int(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastUpdateBy` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` datetime DEFAULT NULL,
  `DeletedBy` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `DeletedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of clClientIndustries
-- ----------------------------
BEGIN;
INSERT INTO `clClientIndustries` VALUES (1, 'TECHNOLOGY & TELECOMMUNICATIONS', 1, 1, 'system', '2019-08-24 11:31:24', 'system', '2019-08-24 11:57:42', NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (2, 'INDUSTRIAL METAL & MINING', 1, 1, 'system', '2019-08-24 11:45:26', 'system', '2019-08-24 11:57:42', NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (3, 'PHARMACEUTICALS', 1, 1, 'system', '2019-09-07 04:50:15', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (4, 'TRANSPORTATION', 1, 1, 'system', '2019-09-07 04:50:53', NULL, NULL, 'system', '2019-09-07 05:04:04');
INSERT INTO `clClientIndustries` VALUES (5, 'MANUFACTURING', 1, 1, 'system', '2019-09-07 04:51:21', NULL, NULL, 'system', '2019-09-07 05:04:01');
INSERT INTO `clClientIndustries` VALUES (6, 'GOVERNMENTS', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (7, 'MEDIA', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (8, 'PROFESSIONAL SERVICES', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (9, 'HEALTHCARE', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (10, 'REAL ESTATE & PROPERTY', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (11, 'CONSTRUCTION', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (12, 'DISTRIBUTION', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (13, 'CHEMICALS', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (14, 'FOOD & BEVERAGES', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (15, 'BANKING & FINANCIAL SERVICES', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (16, 'HOTEL, TOURISM & LEISURE', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (17, 'PLANTATION', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (18, 'TRADING', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (19, 'NOT FOR PROFIT', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (20, 'FOUNDATION', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (21, 'OIL AND GAS', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
INSERT INTO `clClientIndustries` VALUES (22, 'EDUCATIONAL', 1, 1, NULL, '2019-09-29 12:54:34', NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for cmDept
-- ----------------------------
DROP TABLE IF EXISTS `cmDept`;
CREATE TABLE `cmDept` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deptCode` varchar(6) NOT NULL,
  `deptName` varchar(128) NOT NULL,
  `divID` smallint(5) unsigned DEFAULT NULL,
  `Flag` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `deptCode_UNIQUE` (`deptCode`),
  UNIQUE KEY `divDeptName_UNIQUE` (`divID`,`deptName`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cmDept
-- ----------------------------
BEGIN;
INSERT INTO `cmDept` VALUES (1, 'SS-01', 'FINANCE & ACCOUNTING', 4, 1);
INSERT INTO `cmDept` VALUES (2, 'SS-02', 'GENERAL AFFAIR', 4, 1);
INSERT INTO `cmDept` VALUES (3, 'SS-03', 'HUMAN RESOURCES', 4, 1);
INSERT INTO `cmDept` VALUES (4, 'SS-04', 'IT', 4, 1);
INSERT INTO `cmDept` VALUES (5, 'SS-05', 'QUALITY CONTROL', 4, 1);
INSERT INTO `cmDept` VALUES (6, 'SS-06', 'SECRETARY', 4, 1);
INSERT INTO `cmDept` VALUES (7, 'SS-07', 'TRANSACTION SUPPORT', 4, 1);
INSERT INTO `cmDept` VALUES (8, 'SS-08', 'ACCOUNTING SERVICES', 4, 1);
COMMIT;

-- ----------------------------
-- Table structure for cmDivision
-- ----------------------------
DROP TABLE IF EXISTS `cmDivision`;
CREATE TABLE `cmDivision` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `divCode` varchar(4) NOT NULL,
  `divName` varchar(50) NOT NULL,
  `flag` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `divCode` (`divCode`),
  UNIQUE KEY `divName` (`divName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cmDivision
-- ----------------------------
BEGIN;
INSERT INTO `cmDivision` VALUES (1, '001', 'TAX', 1);
INSERT INTO `cmDivision` VALUES (2, '002', 'AUDIT', 1);
INSERT INTO `cmDivision` VALUES (3, '003', 'BUSINESS ADVISORY', 1);
INSERT INTO `cmDivision` VALUES (4, '004', 'SHARED SERVICES', 1);
COMMIT;

-- ----------------------------
-- Table structure for cmEntity
-- ----------------------------
DROP TABLE IF EXISTS `cmEntity`;
CREATE TABLE `cmEntity` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `entityCode` varchar(6) NOT NULL,
  `entityName` varchar(120) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entityId` (`entityCode`),
  UNIQUE KEY `name` (`entityName`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cmEntity
-- ----------------------------
BEGIN;
INSERT INTO `cmEntity` VALUES (1, 'CBA', 'CBA');
INSERT INTO `cmEntity` VALUES (2, 'CC', 'CBA CONSULTING');
INSERT INTO `cmEntity` VALUES (3, 'KNMTR', 'KNMTR');
COMMIT;

-- ----------------------------
-- Table structure for cmLevel
-- ----------------------------
DROP TABLE IF EXISTS `cmLevel`;
CREATE TABLE `cmLevel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `levelCode` varchar(3) NOT NULL,
  `levelName` varchar(45) NOT NULL,
  `notes` varchar(45) DEFAULT NULL,
  `AutoApproved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `position` (`levelName`) USING BTREE,
  UNIQUE KEY `levelCode_UNIQUE` (`levelCode`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cmLevel
-- ----------------------------
BEGIN;
INSERT INTO `cmLevel` VALUES (2, '001', 'PARTNER', '', 1);
INSERT INTO `cmLevel` VALUES (3, '003', 'DIRECTOR', '', 0);
INSERT INTO `cmLevel` VALUES (4, '002', 'ASSOCIATE DIRECTOR', '', 0);
INSERT INTO `cmLevel` VALUES (5, '004', 'SENIOR MANAGER', NULL, 1);
INSERT INTO `cmLevel` VALUES (6, '005', 'MANAGER', NULL, 1);
INSERT INTO `cmLevel` VALUES (7, '006', 'ASSOCIATE MANAGER', '', 0);
INSERT INTO `cmLevel` VALUES (8, '007', 'MANAGING PARTNER', NULL, 0);
INSERT INTO `cmLevel` VALUES (9, '008', 'SECRETARY', NULL, 0);
INSERT INTO `cmLevel` VALUES (10, '009', 'DEPUTY MANAGING PARTNER', NULL, 0);
INSERT INTO `cmLevel` VALUES (11, '010', 'CHAIRMAN', NULL, 0);
INSERT INTO `cmLevel` VALUES (12, '011', 'PRINCIPAL', NULL, 0);
INSERT INTO `cmLevel` VALUES (13, '012', 'SUPERVISOR', NULL, 0);
INSERT INTO `cmLevel` VALUES (14, '013', 'SUPERVISOR 1', NULL, 0);
INSERT INTO `cmLevel` VALUES (15, '014', 'SUPERVISOR 2', NULL, 0);
INSERT INTO `cmLevel` VALUES (16, '015', 'SUPERVISOR 3', NULL, 0);
INSERT INTO `cmLevel` VALUES (17, '016', 'SENIOR', '', 0);
INSERT INTO `cmLevel` VALUES (18, '017', 'SENIOR 1', '', 0);
INSERT INTO `cmLevel` VALUES (19, '018', 'SENIOR 2', NULL, 0);
INSERT INTO `cmLevel` VALUES (20, '019', 'SENIOR 3', NULL, 0);
INSERT INTO `cmLevel` VALUES (21, '020', 'JUNIOR', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for cmOptions
-- ----------------------------
DROP TABLE IF EXISTS `cmOptions`;
CREATE TABLE `cmOptions` (
  `optionsName` varchar(60) NOT NULL,
  `optionsValue` varchar(45) NOT NULL,
  PRIMARY KEY (`optionsName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cmOptions
-- ----------------------------
BEGIN;
INSERT INTO `cmOptions` VALUES ('LAST CLIENT NUMBER', '0871');
INSERT INTO `cmOptions` VALUES ('MAX OVERTIME HOLIDAY', '12');
INSERT INTO `cmOptions` VALUES ('MAX OVERTIME ONE MONTH', '40');
INSERT INTO `cmOptions` VALUES ('MAX OVERTIME WORKDAY', '6');
INSERT INTO `cmOptions` VALUES ('PERCENTAGE ADMINISTRATIVE CHARGE', '5');
INSERT INTO `cmOptions` VALUES ('WORK HOURS', '8');
COMMIT;

-- ----------------------------
-- Table structure for cmTaxi
-- ----------------------------
DROP TABLE IF EXISTS `cmTaxi`;
CREATE TABLE `cmTaxi` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `overtime` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cmTaxi
-- ----------------------------
BEGIN;
INSERT INTO `cmTaxi` VALUES (1, 'PULL IN', 0);
INSERT INTO `cmTaxi` VALUES (2, 'PULL OUT', 0);
INSERT INTO `cmTaxi` VALUES (3, 'MEETING', 0);
INSERT INTO `cmTaxi` VALUES (4, 'AIRPORT', 0);
INSERT INTO `cmTaxi` VALUES (5, 'OVERTIME', 1);
INSERT INTO `cmTaxi` VALUES (6, 'OTHER', 0);
COMMIT;

-- ----------------------------
-- Table structure for hrEmployee
-- ----------------------------
DROP TABLE IF EXISTS `hrEmployee`;
CREATE TABLE `hrEmployee` (
  `Username` varchar(32) DEFAULT NULL,
  `passwordHash` varchar(255) DEFAULT NULL,
  `AuthKey` varchar(32) DEFAULT NULL,
  `PasswordResetToken` int(255) DEFAULT NULL,
  `AccountActivationToken` varchar(255) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `flag` int(1) NOT NULL DEFAULT '1',
  `Id` varchar(32) NOT NULL,
  `fullName` varchar(128) NOT NULL,
  `initial` varchar(4) DEFAULT NULL,
  `gender` varchar(1) NOT NULL DEFAULT '',
  `placeOfBirth` varchar(32) DEFAULT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `joinDate` date DEFAULT NULL,
  `entityId` smallint(5) unsigned DEFAULT NULL,
  `levelID` int(11) unsigned DEFAULT NULL,
  `divisionID` smallint(5) unsigned DEFAULT NULL,
  `deptID` int(11) unsigned DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `certificate` text,
  `religion` varchar(1) DEFAULT NULL,
  `maritalStatus` varchar(1) DEFAULT NULL,
  `nationality` varchar(32) DEFAULT NULL,
  `resignDate` date DEFAULT NULL,
  `resignDescription` text,
  `address` text,
  `postalCode` varchar(10) DEFAULT NULL,
  `mobilePhone` varchar(32) DEFAULT NULL,
  `photo` varchar(128) DEFAULT NULL,
  `keepVacationBalance` int(1) DEFAULT NULL,
  `annualVacation` int(2) DEFAULT NULL,
  `CreatedBy` varchar(64) DEFAULT NULL,
  `CreatedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `LastUpdateBy` varchar(64) DEFAULT NULL,
  `LastUpdateDate` datetime DEFAULT NULL,
  `DeletedBy` varchar(64) DEFAULT NULL,
  `DeletedDate` datetime DEFAULT NULL,
  `parentID` varchar(32) DEFAULT NULL,
  `maxViewTimeReport` smallint(3) DEFAULT NULL,
  `SupervisorID` varchar(32) DEFAULT NULL,
  `ManagerID` varchar(32) DEFAULT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE KEY `userName_UNIQUE` (`Username`),
  KEY `FKEntityID_Employee_idx` (`entityId`),
  KEY `FKLevelID_Employee_idx` (`levelID`),
  KEY `FKDivisionID_Employee_idx` (`divisionID`),
  KEY `FKDeptID_Employee_idx` (`deptID`),
  KEY `parentID_FromID` (`parentID`),
  CONSTRAINT `FKDeptID_Employee` FOREIGN KEY (`deptID`) REFERENCES `cmDept` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FKDivisionID_Employee` FOREIGN KEY (`divisionID`) REFERENCES `cmDivision` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FKEntityID_Employee` FOREIGN KEY (`entityId`) REFERENCES `cmEntity` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FKLevelID_Employee` FOREIGN KEY (`levelID`) REFERENCES `cmLevel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hrEmployee
-- ----------------------------
BEGIN;
INSERT INTO `hrEmployee` VALUES ('system', '$2y$13$RolZYjD.Y5hxcMlaF3L9i.f/.3h/lxRl8BDNIEHhZ3tHR7U32SZYC', '-PMrBgZ6CxkFAR7Qk8Wy-2-jO4MZ3At8', NULL, NULL, 'dan.antariksa@gmail.com', 1, '0', 'Andre Antariksa', '1', 'm', 'Bogor', '2017-01-01', NULL, 2, 17, 4, 4, NULL, NULL, '1', 'S', '1', NULL, NULL, 'SYSTEM', NULL, '081314258332', 'male.png', NULL, NULL, 'system', '2017-02-21 06:56:02', NULL, NULL, NULL, NULL, '1', 2, NULL, NULL, 1);
INSERT INTO `hrEmployee` VALUES ('nununurdiyaman', '$2y$13$PmrIdW3WfTkqdwr4/YvepOCpeRCZloNUQUjbO3Y9QU77xWP.DlqCu', '7eX4aE-QrqzKsmhqIfFS5p9m9ASORQuh', NULL, NULL, '', 1, '281030013', 'NUNU NURDIYAMAN', 'NUN', 'm', '', '1970-01-01', '2003-09-01', 3, 2, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, 0, 12, NULL, '2019-10-13 19:56:33', NULL, NULL, NULL, NULL, '', 2, '', '', 0);
INSERT INTO `hrEmployee` VALUES ('ruchjatkosasih', '$2y$13$FDQVSwpwf/4tvbN5rEUXM.wZTXoP382GUcTAwRIGrrb0TVajL2ZaG', 'YonUKPNTAOanpr9SHNeMLoEq5FrJfyZe', NULL, NULL, '', 1, '281030014', 'RUCHJAT KOSASIH', 'RUC', 'm', '', '1970-01-01', '2003-01-09', 3, 2, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-15 23:36:15', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('hendrarno', '$2y$13$mJjd4.GrjKAnEZ8xKbpDkOeXGnAGAL.tZ.T80fAOdG4uUll9cAO1O', 'aTA1iSXteD8clmrYsqQSg6j6Lx8QA211', NULL, NULL, '', 1, '281030015', 'Y.F. HENDRARNO', 'YFH', 'm', '', NULL, '2003-09-01', 3, 2, 2, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', NULL, 0, 12, NULL, '2019-10-10 23:46:39', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('juanitabudijani', '$2y$13$OTgxtDxel0HBVAi6Q6FWYudr52aQgcdUNoaqBGyybzkJE.ZXBBzO.', 'REm2HAq8r8bwD_rjOPkWXJMNhTzzn36l', NULL, NULL, '', 1, '281060004', 'JUANITA BUDIJANI', '', 'f', '', '1970-01-01', '2006-01-08', 3, 2, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:16:22', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('muhamadmuhidin', '$2y$13$d2t74V6Ofk1IabG59LMj/O5erifHTMEaziw6TggO5idGkrOjVeGE6', '9RBGFovZoaTTYGRvHKRy4HDSKrcjERUk', NULL, NULL, '', 1, '281070006', 'MUHAMAD MUHIDIN', 'MUM', 'm', '', '1970-01-01', '1970-01-01', 3, 2, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-15 23:55:14', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('sumartono', '$2y$13$iczlQUb03.fNDF44A0JzX.XjrXMoCzHUU60KFhmljJv09NgnU4.dS', 'gIrJ5OBRVtQCPiOdVUG9guHhpDFVQTIn', NULL, NULL, '', 1, '281110008', 'SUMARTONO', '', 'm', '', '1970-01-01', '2011-01-03', 3, 5, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:27:35', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('dimasprayogo', '$2y$13$E1cBD6Th4nJHUVU3Tw1cZepqmFZX4WbH1/pSS9OE/nc9m26JJYVT.', 'NDRFf0ftZGaEdA1rhF0A2VnUDiFf7lhB', NULL, NULL, '', 1, '281110014', 'DIMAS PRAYOGO', '', 'm', '', '1970-01-01', '2008-10-11', 3, 7, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-15 23:34:07', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('bambangmuratno', '$2y$13$5DiDHCidCzUnlRvFp9A6aukWg5RNNzDPrzhw0qOlXm2GLEoi/tttm', 'Bs0xE1-1dpCVl4u_UjPItK0A3sQsdmpm', NULL, NULL, '', 1, '281110037', 'BAMBANG MURATNO', 'BMR', 'm', '', NULL, '2009-07-02', 3, 2, 2, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', NULL, 0, 12, NULL, '2019-10-10 23:44:39', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('rismawaty', '$2y$13$/vMKnLoirYOvSoumZzvajejVf0i6ovTAGi1lZcmijX7/38./4X5Nu', 'V4Hi5DGAk3zy8YxN3OiYT-Dht2qiN55T', NULL, NULL, '', 1, '281110043', 'RISMAWATY', '', 'f', '', '1970-01-01', '2010-01-12', 3, 13, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:31:00', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('helmiirfany', '$2y$13$EjKPra1MH6wloXGhLTsFIuQQo3SOZtXsMQ1.UYfgRMqTrmAoIi2PW', 'j934UT5E3OQPdCfftkCxXUX_GJabnv1B', NULL, NULL, '', 1, '281110050', 'HELMI INDRA IRFANY', '', 'm', '', '1970-01-01', '1970-01-01', 3, 13, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-15 23:59:54', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('dimasprasetyo', '$2y$13$YU7C73tU7pg8yynoBj0MluHMaWvcpq8nQHDetpU6ZOpAc80auGjv6', 'tDKIZiw0NQMznNgzfIA_KavaJjOrg9yW', NULL, NULL, '', 1, '281130001', 'DIMAS PRASETYO TERTIANTO', 'DPT', 'm', '', '1970-01-01', '2013-11-02', 3, 7, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, 1, 12, NULL, '2019-10-10 23:48:29', NULL, NULL, NULL, NULL, '281030015', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('kristinagultom', '$2y$13$kac73jFBmLG.mO9hBAfO/ek1O2yU.qymt8z2xvYedd.hweGjehWeu', 'nFWfuG-QcUhCgfJ2YT2pEFpczZ3Io_cb', NULL, NULL, '', 1, '281130029', 'KRISTINA GULTOM', '', 'f', '', '1970-01-01', '2013-02-04', 3, 17, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, 1, 12, NULL, '2019-10-11 00:31:19', NULL, NULL, NULL, NULL, '281030015', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('lanamatheswara', '$2y$13$l8FZQ9uWQqne.r/NxglUkeANg9UwAULQ92Yn33VzM3.j8OzmLOBOm', '1qjpoA7RSxKgS6gXsrB7tVzAUxusaw-n', NULL, NULL, '', 1, '281130030', 'LANA MATHESWARA SUCIPTO', '', 'm', '', '1970-01-01', '1970-01-01', 3, 17, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:22:09', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('wisnuadinugroho', '$2y$13$iyBM8tUxfc.7F3cZKsW4UuELRKQf1JJ93jnwEtoHDkWru/zF6AAFy', '1jVMwyvaZ7A6Z_A_HEREBCB1lyfhKbRW', NULL, NULL, '', 1, '281140027', 'WISNU ADI NUGROHO', 'WAN', 'm', '', NULL, '2014-12-01', 3, 7, 2, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', NULL, 1, 12, NULL, '2019-10-11 00:28:52', NULL, NULL, NULL, NULL, '281030015', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('hujantotejohutomo', '$2y$13$M.Uo47T2UAXapcgLCudzJ./5/Rv1GFJLY20VB/1KnsqfSSEqGlR2K', 'kZ874VevTORSXqtrERY5G47xLIA6H-Yb', NULL, NULL, '', 1, '28114T035', 'HUJANTO TEJO HUTOMO', '', 'm', '', '1970-01-01', '1970-01-01', 3, 17, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:28:54', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('johanesadrianus', '$2y$13$1Wn8ondQ//54e1EEBGmIbuutRbQq8pdajQ82Ju4okXjXs2JV.PXeO', 'aT_cKpXr4QKBPxMk9dFN1N1Rn9NyeDEP', NULL, NULL, '', 1, '28115T025', 'JOHANES ADRIANUS SEBASTIAN', '', 'm', '', '1970-01-01', '1970-01-01', 3, 17, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, 1, 12, NULL, '2019-10-10 23:50:49', '28115T025', '2019-10-18 00:26:46', NULL, NULL, '281030015', 10, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('angelinelaluur', '$2y$13$7CdNg32pmsD7eXF4l8vtaedWFaMaLtjX2wZz3NwxGnocXlCdLIlQO', 'gTbmRAju2V611PhdGO0P5f5mcLrBJ8Ld', NULL, NULL, '', 1, '28116T005', 'ANGELINE JULIA NARENGI LALUUR', '', 'f', '', NULL, '2016-01-11', 3, 17, 2, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', NULL, 1, 12, NULL, '2019-10-13 18:57:57', NULL, NULL, NULL, NULL, '281030015', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('gracemonica', '$2y$13$tXVmdIyzO2b0KEdZwaX5u.j2O9KS54ZpLgMjkQvAhbpQ9ZTbZtCTC', 'i92wpBr6wYQQjHsOAzHcf30_D7mwenwD', NULL, NULL, '', 1, '28116T013', 'GRACE MONICA', '', 'f', '', '1970-01-01', '1970-01-01', 3, 21, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, 1, 12, NULL, '2019-10-13 18:46:34', NULL, NULL, NULL, NULL, '281110014', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('sylviasuwandi', '$2y$13$gwQCNxgod6XxLWhCZX95fOSu0vVZdFq8tnXGtxlS.Kb93HPee494y', 'eD4L9TPHBgpW8-gdMzf_IyIdUpYWaN--', NULL, NULL, '', 1, '28116T021', 'SYLVIA SUWANDI', '', 'f', '', '1970-01-01', '2016-01-08', 3, 17, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:19:06', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('iqbalhalim', '$2y$13$7sCADHcaq4GZHEp.XWmoIuo1fcG/wcFziXk.oUAI1968ypF82fRgm', '1beR8OA2LEqOBK45YyRNMnnpGMyeVvKX', NULL, NULL, '', 1, '28116T027', 'IQBAL HALIM', '', 'm', '', '1970-01-01', '2016-08-11', 3, 21, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:32:08', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('rangga', '$2y$13$8dVgCk6AfTM8Z/CijUbxCePRmiTZo8B6CCababNsavq0lfZBoJO3K', 'hDw-iw1yuPQx6JG28K6-qZNo6kZVyHGl', NULL, NULL, '', 1, '28117T054', 'RANGGA', '', 'm', '', '1970-01-01', '2017-11-09', 3, 21, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:14:48', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('aaronbataque', '$2y$13$j9Qt0pmwcYIoVjnpb8wW/eGy0Gcqu2GScnbYaEwrqsfqklf6Hj7IO', 'C-HePu2fEVZcSWYObCjGfwokOs-EtIZ7', NULL, NULL, '', 1, '28117T069', 'AARON DOLOSA BATAQUE', '', 'm', '', '1970-01-01', '1970-01-01', 3, 7, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:17:45', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('febrinahutapea', '$2y$13$OYZR56F1a.rtiGluFwFh1OdJNMOlz8tqpksQXf.ocPrPe5eUV.Hdq', 'LCQy2O_ZTI_wOXKWuGStDbVfzMF-HH2q', NULL, NULL, '', 1, '28117T075', 'FEBRINA LASTIAR HUTAPEA', '', 'f', '', NULL, '2017-12-13', 3, 21, 2, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', NULL, 0, 12, NULL, '2019-10-13 18:41:28', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('herdianjatmiko', '$2y$13$LILErQOYPIXJaB/COmxquuEdv.lwLEnxWTsIhy7HTs/LRPLnG8mRy', 'rf2wo-9UvHqGCaaezcbFJnAgTRrjbgW7', NULL, NULL, '', 1, '28118T002', 'HERDIAN PROBO JATMIKO', '', 'm', '', '1970-01-01', '1970-01-01', 3, 21, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:23:27', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('johnsitanggang', '$2y$13$l.Cqs3wyfZsiPvrvtvt7behNzpC72/Mk..DkQGfqAzVGPl5wgdLiW', 'yOKEm6lKSXItFY9veTksPy_w7DVqJyPP', NULL, NULL, '', 1, '28118T007', 'JHON PERSON SITANGGANG', '', 'm', '', '1970-01-01', '2018-11-05', 3, 21, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-15 23:43:03', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('mithamapanmarpaung', '$2y$13$a7/NhQDojV7apO8q7JRZhuR6PXaOY//.tXtHv7CGaCdVhpoU.du.G', 'G0Hj9Ap_19InVRrpv7ILPCUKRvJgvu-Y', NULL, NULL, '', 1, '28118T023', 'MITHAMAPAN MARPAUNG', '', 'f', '', '1970-01-01', '2018-03-09', 3, 21, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:33:27', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('wayanirawan', '$2y$13$tC.ICGR5dOMCn/7jR39zvOTfMsE6VNwXCnY6xNKTOV2fFq2kOC4p.', 'XJiicOtKdxD31wg8_tXlaM8hpZ6fIl1s', NULL, NULL, '', 1, '28119T003', 'WAYAN BAYU INDRA IRAWAN', '', 'm', '', '1970-01-01', '2019-02-01', 3, 21, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:25:42', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
INSERT INTO `hrEmployee` VALUES ('kevinadam', '$2y$13$1F7iL8jMYiEWasyvct7anOFPtfsdjwVN3dX.0GlJACOB4fUfxTX1.', 'iU6AdB9Gs43Zi4TvYjZbk8i3SCrAAodk', NULL, NULL, '', 1, '28119T005', 'KEVIN CARMELO CINDY ADAM', '', 'm', '', '1970-01-01', '1970-01-01', 3, 21, 2, NULL, '', NULL, '', '', '', '1970-01-01', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-10-16 00:24:40', NULL, NULL, NULL, NULL, '', 2, NULL, NULL, 0);
COMMIT;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(256) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_menu_1_idx` (`parent`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=626 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO `menu` VALUES (1, 'Dashboard', NULL, '/site/index', 1, '{\"icon\": \"<i data-feather=\'shield\'></i>\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (2, 'Clients', NULL, '/cl/client', 3, '{\"icon\": \"<i data-feather=\'users\'></i>\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (3, 'Job', NULL, '/tr/job/all', 4, '{\"icon\": \"<i class=\'fa fa-newspaper-o\'></i>\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (4, 'Time Report', NULL, '/tr/timereport/all', 5, '{\"icon\": \"<i class=\'fa fa-calendar\'></i>\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (5, 'Approvals', NULL, '#', 6, '{\"icon\": \"<i class=\'fa fa-check-circle-o\'></i>\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (6, 'Setup', NULL, '#', 2, '{\"icon\": \"<i class=\'fa fa-database\'></i>\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (7, 'Report', NULL, '#', 7, '{\"icon\": \"<i class=\'fa fa-tasks\'></i>\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (8, 'Logout', NULL, '/site/logout', 8, '{\"icon\": \"<i class=\'fa fa-signout\'></i>\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (51, 'Jobs', 5, '/tr/job/pending', 1, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (52, 'Time Report', 5, '/tr/trapproval/all', 2, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (61, 'General', 6, '#', 1, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (62, 'Setup', 6, '#', 2, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (63, 'Employee', 6, '/hr/employee', 0, '{\"icon\": \"<i class=\'fa fa-users\'></i>\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (611, 'Entity', 61, '/cm/entity', 1, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (612, 'Division', 61, '/cm/division', 2, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (613, 'Department', 61, '/cm/dept', 3, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (614, 'Level', 61, '/cm/level', 4, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (621, 'Holiday', 62, '/st/holiday', 1, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (622, 'Billing Rate', 62, '/st/billingrate', 2, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (623, 'Task', 62, '/st/task', 3, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (624, 'Rules', 62, '/st/rules', 4, '{\"icon\": \"\", \"module\": \"tr\"}');
INSERT INTO `menu` VALUES (625, 'Options', 61, '/cm/options', 5, NULL);
COMMIT;

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rgUser
-- ----------------------------
DROP TABLE IF EXISTS `rgUser`;
CREATE TABLE `rgUser` (
  `Id` varchar(16) NOT NULL COMMENT 'Username',
  `CompanyId` varchar(9) DEFAULT NULL COMMENT 'Company',
  `BranchId` varchar(12) DEFAULT NULL COMMENT 'Branch',
  `DeptId` varchar(3) DEFAULT NULL,
  `EmployeeId` varchar(32) DEFAULT NULL,
  `Email` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PasswordHash` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `AuthKey` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `PasswordResetToken` int(255) DEFAULT NULL,
  `AccountActivationToken` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` int(1) DEFAULT NULL,
  `canChangeCompany` int(1) DEFAULT '0',
  `canChangeBranch` int(1) DEFAULT '0',
  `canChangeDept` int(1) DEFAULT '0',
  `LastUpdateBy` varchar(64) DEFAULT NULL,
  `LastUpdateDate` datetime DEFAULT NULL,
  `CreatedBy` varchar(64) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `Flag` int(1) DEFAULT '1',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rgUser
-- ----------------------------
BEGIN;
INSERT INTO `rgUser` VALUES ('sys1', '1', '11', '1', '00.19.01.001', 'antariksa.org@gmail.com', '$2y$13$21txgGx0Nh0U.OwGLC1CQua1r6WQL4.aRAuQR4zVT4HJlW033tpk6', 'MV8hng-vu9fBfRl-YaJQOVGEAdXEK6u2', NULL, NULL, 10, 0, 1, 1, 'sys', '2019-09-25 07:04:30', NULL, '2017-09-06 16:37:53', 1);
INSERT INTO `rgUser` VALUES ('system1', '1', '1', '1', '00.19.01.001', 'sysadmin@dutacenda.co.id', '$2y$13$MYbGI4BX0dtDEPC8xIaOkuS5rNAcr5dMQxuNZJDX.k5PuoyL6bzva', 'XtETA77drnHcED5vDPSMNtmdNDur4EXj', NULL, NULL, 10, 1, 1, 1, 'system', '2019-09-25 07:04:31', 'system', '2016-05-20 16:46:10', 1);
COMMIT;

-- ----------------------------
-- Table structure for session
-- ----------------------------
DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` char(40) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for stBillingRate
-- ----------------------------
DROP TABLE IF EXISTS `stBillingRate`;
CREATE TABLE `stBillingRate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `effectiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `efectiveDate` (`effectiveDate`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of stBillingRate
-- ----------------------------
BEGIN;
INSERT INTO `stBillingRate` VALUES (27, 'SK 002', '2019-01-01');
INSERT INTO `stBillingRate` VALUES (28, 'SK 003', '2019-10-01');
COMMIT;

-- ----------------------------
-- Table structure for stBillingRateDetail
-- ----------------------------
DROP TABLE IF EXISTS `stBillingRateDetail`;
CREATE TABLE `stBillingRateDetail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `billingRateID` int(10) unsigned NOT NULL,
  `LevelID` int(11) unsigned NOT NULL,
  `BillingRateValue` decimal(18,0) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `billingRatePositionID` (`billingRateID`,`LevelID`) USING BTREE,
  KEY `fkLevelID_BillingRateDetail_idx` (`LevelID`),
  CONSTRAINT `FKBillingRateID` FOREIGN KEY (`billingRateID`) REFERENCES `stBillingRate` (`id`),
  CONSTRAINT `fkLevelID_BillingRateDetail` FOREIGN KEY (`LevelID`) REFERENCES `cmLevel` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stBillingRateDetail
-- ----------------------------
BEGIN;
INSERT INTO `stBillingRateDetail` VALUES (167, 27, 2, 2500000);
INSERT INTO `stBillingRateDetail` VALUES (168, 27, 4, 0);
INSERT INTO `stBillingRateDetail` VALUES (169, 27, 3, 2100000);
INSERT INTO `stBillingRateDetail` VALUES (170, 27, 5, 1500000);
INSERT INTO `stBillingRateDetail` VALUES (171, 27, 6, 1100000);
INSERT INTO `stBillingRateDetail` VALUES (172, 27, 7, 900000);
INSERT INTO `stBillingRateDetail` VALUES (173, 27, 8, 0);
INSERT INTO `stBillingRateDetail` VALUES (174, 27, 9, 0);
INSERT INTO `stBillingRateDetail` VALUES (175, 27, 10, 0);
INSERT INTO `stBillingRateDetail` VALUES (176, 27, 11, 0);
INSERT INTO `stBillingRateDetail` VALUES (177, 27, 12, 0);
INSERT INTO `stBillingRateDetail` VALUES (178, 27, 13, 600000);
INSERT INTO `stBillingRateDetail` VALUES (179, 27, 14, 600000);
INSERT INTO `stBillingRateDetail` VALUES (180, 27, 15, 600000);
INSERT INTO `stBillingRateDetail` VALUES (181, 27, 16, 600000);
INSERT INTO `stBillingRateDetail` VALUES (182, 27, 17, 475000);
INSERT INTO `stBillingRateDetail` VALUES (183, 27, 18, 475000);
INSERT INTO `stBillingRateDetail` VALUES (184, 27, 19, 475000);
INSERT INTO `stBillingRateDetail` VALUES (185, 27, 20, 475000);
INSERT INTO `stBillingRateDetail` VALUES (186, 27, 21, 300000);
INSERT INTO `stBillingRateDetail` VALUES (190, 28, 2, 2600000);
INSERT INTO `stBillingRateDetail` VALUES (191, 28, 4, 0);
INSERT INTO `stBillingRateDetail` VALUES (192, 28, 3, 2200000);
INSERT INTO `stBillingRateDetail` VALUES (193, 28, 5, 1600000);
INSERT INTO `stBillingRateDetail` VALUES (194, 28, 6, 1200000);
INSERT INTO `stBillingRateDetail` VALUES (195, 28, 7, 1000000);
INSERT INTO `stBillingRateDetail` VALUES (196, 28, 8, 0);
INSERT INTO `stBillingRateDetail` VALUES (197, 28, 9, 0);
INSERT INTO `stBillingRateDetail` VALUES (198, 28, 10, 0);
INSERT INTO `stBillingRateDetail` VALUES (199, 28, 11, 0);
INSERT INTO `stBillingRateDetail` VALUES (200, 28, 12, 0);
INSERT INTO `stBillingRateDetail` VALUES (201, 28, 13, 700000);
INSERT INTO `stBillingRateDetail` VALUES (202, 28, 14, 700000);
INSERT INTO `stBillingRateDetail` VALUES (203, 28, 15, 700000);
INSERT INTO `stBillingRateDetail` VALUES (204, 28, 16, 700000);
INSERT INTO `stBillingRateDetail` VALUES (205, 28, 17, 575000);
INSERT INTO `stBillingRateDetail` VALUES (206, 28, 18, 575000);
INSERT INTO `stBillingRateDetail` VALUES (207, 28, 19, 575000);
INSERT INTO `stBillingRateDetail` VALUES (208, 28, 20, 575000);
INSERT INTO `stBillingRateDetail` VALUES (209, 28, 21, 400000);
COMMIT;

-- ----------------------------
-- Table structure for stHolidayTimeOff
-- ----------------------------
DROP TABLE IF EXISTS `stHolidayTimeOff`;
CREATE TABLE `stHolidayTimeOff` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `holidayDate` date NOT NULL,
  `typeID` smallint(5) unsigned NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `date` (`holidayDate`),
  KEY `FKHolidayTypeID` (`typeID`),
  CONSTRAINT `FKHolidayTypeID` FOREIGN KEY (`typeID`) REFERENCES `stHolidayType` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of stHolidayTimeOff
-- ----------------------------
BEGIN;
INSERT INTO `stHolidayTimeOff` VALUES (5, '2019-01-01', 1, 'NEW YEAR\'S DAY');
INSERT INTO `stHolidayTimeOff` VALUES (6, '2019-02-05', 1, 'CHINESE NEW YEAR');
INSERT INTO `stHolidayTimeOff` VALUES (7, '2019-03-07', 1, 'BALI HINDU NEW YEAR');
INSERT INTO `stHolidayTimeOff` VALUES (8, '2019-04-03', 1, 'ISRA MI\'RAJ');
INSERT INTO `stHolidayTimeOff` VALUES (9, '2019-04-19', 1, 'GOOD FRIDAY');
INSERT INTO `stHolidayTimeOff` VALUES (25, '2019-05-01', 1, 'LABOUR DAY');
INSERT INTO `stHolidayTimeOff` VALUES (26, '2019-05-19', 1, 'WAISAK DAY');
INSERT INTO `stHolidayTimeOff` VALUES (27, '2019-05-30', 1, 'ASCENSION DAY OF JESUS CHRIST');
INSERT INTO `stHolidayTimeOff` VALUES (28, '2019-06-01', 1, 'PANCASILA DAY');
INSERT INTO `stHolidayTimeOff` VALUES (29, '2019-06-03', 5, 'LEBARAN HOLIDAY');
INSERT INTO `stHolidayTimeOff` VALUES (30, '2019-06-04', 5, 'LEBARAN HOLIDAY');
INSERT INTO `stHolidayTimeOff` VALUES (31, '2019-06-05', 1, 'HARI RAYA IDUL FITRI');
INSERT INTO `stHolidayTimeOff` VALUES (32, '2019-06-06', 1, 'HARI RAYA IDUL FITRI');
INSERT INTO `stHolidayTimeOff` VALUES (33, '2019-06-07', 5, 'LEBARAN HOLIDAY');
INSERT INTO `stHolidayTimeOff` VALUES (34, '2019-08-11', 1, 'IDUL ADHA');
INSERT INTO `stHolidayTimeOff` VALUES (35, '2019-08-17', 1, 'INDEPENDENCE DAY');
INSERT INTO `stHolidayTimeOff` VALUES (36, '2019-09-01', 1, 'ISLAMIC NEW YEAR');
INSERT INTO `stHolidayTimeOff` VALUES (37, '2019-11-09', 1, 'PROPHET MUHAMMAD\'S BIRTHDAY');
INSERT INTO `stHolidayTimeOff` VALUES (39, '2019-12-25', 1, 'CHRISTMAS DAY');
INSERT INTO `stHolidayTimeOff` VALUES (40, '2019-12-24', 5, 'CHRISTMAS HOLIDAY');
COMMIT;

-- ----------------------------
-- Table structure for stHolidayType
-- ----------------------------
DROP TABLE IF EXISTS `stHolidayType`;
CREATE TABLE `stHolidayType` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `holidayTypeName` varchar(20) NOT NULL,
  `isHoliday` tinyint(1) NOT NULL DEFAULT '0',
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `holidayTypeName` (`holidayTypeName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stHolidayType
-- ----------------------------
BEGIN;
INSERT INTO `stHolidayType` VALUES (1, 'HOLIDAY', 1, 1);
INSERT INTO `stHolidayType` VALUES (3, 'TIME OFF', 1, 1);
INSERT INTO `stHolidayType` VALUES (5, 'CUTI BERSAMA', 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for stRuleItem
-- ----------------------------
DROP TABLE IF EXISTS `stRuleItem`;
CREATE TABLE `stRuleItem` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `termItemName` varchar(80) NOT NULL,
  `termItemDesc` text NOT NULL,
  `termZoneName` varchar(240) NOT NULL,
  `isClientZone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `areaItemName` (`termItemName`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stRuleItem
-- ----------------------------
BEGIN;
INSERT INTO `stRuleItem` VALUES (1, 'OVERTIME MEALS ALLOWANCE', '(Not applicable if the meals is provided by client and applicable only for Senior Level and below)', 'Working Day,Holiday', 0);
INSERT INTO `stRuleItem` VALUES (2, 'OVERTIME TRANSPORTATION', 'Taxi Transportation', 'Working Day,Holiday', 0);
INSERT INTO `stRuleItem` VALUES (3, 'OUT OF OFFICE ALLOWANCE', '(If use taxi for overtime transportation, the out of office allowance is 50%, from normal rate)- Applicable only for Supervisor level and below- Maximum charge OPE 2 times a day', 'Office,A,B,C,D,E', 1);
COMMIT;

-- ----------------------------
-- Table structure for stRuleTaskAE
-- ----------------------------
DROP TABLE IF EXISTS `stRuleTaskAE`;
CREATE TABLE `stRuleTaskAE` (
  `levelID` int(10) unsigned NOT NULL,
  `taskID` int(10) unsigned NOT NULL,
  `percentage` smallint(3) NOT NULL,
  UNIQUE KEY `levelTaskID_UNIQUE` (`levelID`,`taskID`),
  KEY `fkLevelID_TaskAE_idx` (`levelID`),
  KEY `fkTaskID_TaskAE_idx` (`taskID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stRuleTaskAE
-- ----------------------------
BEGIN;
INSERT INTO `stRuleTaskAE` VALUES (17, 63, 10);
INSERT INTO `stRuleTaskAE` VALUES (45, 63, 60);
INSERT INTO `stRuleTaskAE` VALUES (45, 64, 20);
INSERT INTO `stRuleTaskAE` VALUES (45, 65, 10);
INSERT INTO `stRuleTaskAE` VALUES (45, 66, 10);
INSERT INTO `stRuleTaskAE` VALUES (2, 63, 80);
INSERT INTO `stRuleTaskAE` VALUES (2, 64, 0);
INSERT INTO `stRuleTaskAE` VALUES (2, 65, 20);
INSERT INTO `stRuleTaskAE` VALUES (2, 66, 0);
INSERT INTO `stRuleTaskAE` VALUES (17, 64, 70);
INSERT INTO `stRuleTaskAE` VALUES (17, 65, 10);
INSERT INTO `stRuleTaskAE` VALUES (17, 66, 10);
INSERT INTO `stRuleTaskAE` VALUES (7, 63, 20);
INSERT INTO `stRuleTaskAE` VALUES (7, 64, 25);
INSERT INTO `stRuleTaskAE` VALUES (7, 65, 30);
INSERT INTO `stRuleTaskAE` VALUES (7, 66, 25);
INSERT INTO `stRuleTaskAE` VALUES (48, 63, 10);
INSERT INTO `stRuleTaskAE` VALUES (48, 64, 5);
INSERT INTO `stRuleTaskAE` VALUES (48, 65, 5);
INSERT INTO `stRuleTaskAE` VALUES (48, 66, 80);
COMMIT;

-- ----------------------------
-- Table structure for stRuleTermZone
-- ----------------------------
DROP TABLE IF EXISTS `stRuleTermZone`;
CREATE TABLE `stRuleTermZone` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `termZoneName` varchar(30) NOT NULL,
  `termZoneDescription` varchar(255) DEFAULT NULL,
  `isStayed` tinyint(1) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `areaTermName` (`termZoneName`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stRuleTermZone
-- ----------------------------
BEGIN;
INSERT INTO `stRuleTermZone` VALUES (1, 'Working Day', '.', 0, 0);
INSERT INTO `stRuleTermZone` VALUES (2, 'Holiday', NULL, 0, 0);
INSERT INTO `stRuleTermZone` VALUES (3, 'Office', NULL, 0, 1);
INSERT INTO `stRuleTermZone` VALUES (4, 'A', 'Zone A ( < 2 KM )', 0, 1);
INSERT INTO `stRuleTermZone` VALUES (5, 'B', 'Zone B ( > 2 KM - 10 KM )', 0, 1);
INSERT INTO `stRuleTermZone` VALUES (6, 'C', 'Zone C ( > 10 KM - 20 KM )', 0, 1);
INSERT INTO `stRuleTermZone` VALUES (7, 'D', 'Zone D ( > 20 KM - 30 KM )', 0, 1);
INSERT INTO `stRuleTermZone` VALUES (8, 'E', 'Zone E ( > 30 KM )', 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for stRules
-- ----------------------------
DROP TABLE IF EXISTS `stRules`;
CREATE TABLE `stRules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `effectiveDate` date NOT NULL,
  `description` varchar(240) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `effectivedate` (`effectiveDate`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stRules
-- ----------------------------
BEGIN;
INSERT INTO `stRules` VALUES (24, '2018-01-01', 'No. 001');
INSERT INTO `stRules` VALUES (25, '2019-01-01', '002');
COMMIT;

-- ----------------------------
-- Table structure for stRulesDetail
-- ----------------------------
DROP TABLE IF EXISTS `stRulesDetail`;
CREATE TABLE `stRulesDetail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rulesID` int(11) unsigned NOT NULL,
  `itemID` smallint(5) unsigned NOT NULL,
  `termID` smallint(5) unsigned NOT NULL,
  `minOvertime` decimal(4,1) DEFAULT NULL,
  `isClaim` tinyint(1) NOT NULL DEFAULT '0',
  `claimQty` smallint(6) NOT NULL,
  `useTaxi` tinyint(1) DEFAULT '0',
  `minClock` time DEFAULT NULL,
  `stayed` tinyint(1) NOT NULL DEFAULT '0',
  `allowanceAmount` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKRulesID_RulesDetail_idx` (`rulesID`),
  KEY `FKItemID_RulesDetail_idx` (`itemID`),
  KEY `FKTermID_RulesDetail_idx` (`termID`),
  CONSTRAINT `FKItemID_RulesDetail` FOREIGN KEY (`itemID`) REFERENCES `stRuleItem` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FKRulesID_RulesDetail` FOREIGN KEY (`rulesID`) REFERENCES `stRules` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FKTermID_RulesDetail` FOREIGN KEY (`termID`) REFERENCES `stRuleTermZone` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stRulesDetail
-- ----------------------------
BEGIN;
INSERT INTO `stRulesDetail` VALUES (90, 24, 1, 1, 3.0, 1, 1, 0, '12:00:00', 1, 50000.00);
INSERT INTO `stRulesDetail` VALUES (91, 24, 1, 2, 5.0, 1, 1, 0, '04:00:00', 1, 6000.00);
INSERT INTO `stRulesDetail` VALUES (92, 24, 2, 1, 3.5, 1, 1, 1, '03:00:00', 1, 3.50);
INSERT INTO `stRulesDetail` VALUES (93, 24, 2, 2, 8.0, 1, 1, 1, '08:00:00', 1, 8.00);
INSERT INTO `stRulesDetail` VALUES (94, 24, 3, 4, 1.0, 1, 2, 1, '01:00:00', 1, 10000000.00);
INSERT INTO `stRulesDetail` VALUES (95, 24, 3, 5, 2.0, 1, 2, 0, '02:00:00', 1, 90000.00);
INSERT INTO `stRulesDetail` VALUES (96, 24, 3, 6, 3.0, 1, 2, 1, '03:00:00', 1, 80000000.00);
INSERT INTO `stRulesDetail` VALUES (97, 24, 3, 7, 4.0, 1, 2, 0, '04:00:00', 1, 7000000.00);
INSERT INTO `stRulesDetail` VALUES (98, 24, 3, 8, 5.0, 1, 2, 1, '05:00:00', 1, 6000000.00);
INSERT INTO `stRulesDetail` VALUES (100, 25, 1, 1, 3.5, 1, 1, 0, '00:00:00', 1, 30000.00);
INSERT INTO `stRulesDetail` VALUES (101, 25, 1, 2, 4.0, 1, 1, 0, '00:00:00', 1, 30000.00);
INSERT INTO `stRulesDetail` VALUES (102, 25, 2, 1, 3.0, 1, 1, 1, '21:00:00', 1, 0.00);
INSERT INTO `stRulesDetail` VALUES (103, 25, 2, 2, 4.0, 1, 1, 1, '21:00:00', 1, 0.00);
INSERT INTO `stRulesDetail` VALUES (104, 25, 3, 3, 0.0, 1, 2, 0, '00:00:00', 1, 0.00);
INSERT INTO `stRulesDetail` VALUES (105, 25, 3, 4, 0.0, 1, 2, 0, '00:00:00', 1, 30000.00);
INSERT INTO `stRulesDetail` VALUES (106, 25, 3, 5, 0.0, 1, 2, 0, '00:00:00', 1, 40000.00);
INSERT INTO `stRulesDetail` VALUES (107, 25, 3, 6, 0.0, 1, 2, 0, '00:00:00', 1, 55000.00);
INSERT INTO `stRulesDetail` VALUES (108, 25, 3, 7, 0.0, 1, 2, 0, '00:00:00', 1, 70000.00);
INSERT INTO `stRulesDetail` VALUES (109, 25, 3, 8, 0.0, 1, 2, 0, '00:00:00', 1, 100000.00);
COMMIT;

-- ----------------------------
-- Table structure for stRulesLevel
-- ----------------------------
DROP TABLE IF EXISTS `stRulesLevel`;
CREATE TABLE `stRulesLevel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rulesID` int(10) unsigned NOT NULL,
  `itemID` smallint(5) unsigned NOT NULL,
  `levelDescription` varchar(1000) DEFAULT NULL,
  `qtyClaim` smallint(1) DEFAULT NULL,
  `qtyClaimStayed` smallint(1) DEFAULT NULL,
  `stayedAllowance` decimal(18,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKItemID_RulesLevel_idx` (`itemID`),
  KEY `FKRulesID_RulesLevel` (`rulesID`),
  CONSTRAINT `FKItemID_RulesLevel` FOREIGN KEY (`itemID`) REFERENCES `stRuleItem` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FKRulesID_RulesLevel` FOREIGN KEY (`rulesID`) REFERENCES `stRules` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stRulesLevel
-- ----------------------------
BEGIN;
INSERT INTO `stRulesLevel` VALUES (205, 24, 1, 'SENIOR,SENIOR 1,SENIOR 2,SENIOR 3,JUNIOR', 11, 6, 33);
INSERT INTO `stRulesLevel` VALUES (206, 24, 2, 'SUPERVISOR,SUPERVISOR 1,SUPERVISOR 2,SUPERVISOR 3,SENIOR,SENIOR 1,SENIOR 2,SENIOR 3,JUNIOR', 4, 22, 5);
INSERT INTO `stRulesLevel` VALUES (207, 24, 3, 'SUPERVISOR,SUPERVISOR 1,SUPERVISOR 2,SUPERVISOR 3,SENIOR,SENIOR 1,SENIOR 2,SENIOR 3,JUNIOR', 11, 7, 33);
INSERT INTO `stRulesLevel` VALUES (208, 25, 1, 'PARTNER,DIRECTOR,ASSOCIATE DIRECTOR,SENIOR MANAGER,MANAGER,ASSOCIATE MANAGER,MANAGING PARTNER,SECRETARY,DEPUTY MANAGING PARTNER,CHAIRMAN,PRINCIPAL,SUPERVISOR,SUPERVISOR 1,SUPERVISOR 2,SUPERVISOR 3,SENIOR,SENIOR 1,SENIOR 2,SENIOR 3,JUNIOR', 1, 1, 30000);
INSERT INTO `stRulesLevel` VALUES (209, 25, 2, 'PARTNER,DIRECTOR,ASSOCIATE DIRECTOR,SENIOR MANAGER,MANAGER,ASSOCIATE MANAGER,MANAGING PARTNER,SECRETARY,DEPUTY MANAGING PARTNER,CHAIRMAN,PRINCIPAL,SUPERVISOR,SUPERVISOR 1,SUPERVISOR 2,SUPERVISOR 3,SENIOR,SENIOR 1,SENIOR 2,SENIOR 3,JUNIOR', 1, 1, 0);
INSERT INTO `stRulesLevel` VALUES (210, 25, 3, 'PARTNER,DIRECTOR,ASSOCIATE DIRECTOR,SENIOR MANAGER,MANAGER,ASSOCIATE MANAGER,MANAGING PARTNER,SECRETARY,DEPUTY MANAGING PARTNER,CHAIRMAN,PRINCIPAL,SUPERVISOR,SUPERVISOR 1,SUPERVISOR 2,SUPERVISOR 3,SENIOR,SENIOR 1,SENIOR 2,SENIOR 3,JUNIOR', 2, 1, 50000);
COMMIT;

-- ----------------------------
-- Table structure for stTask
-- ----------------------------
DROP TABLE IF EXISTS `stTask`;
CREATE TABLE `stTask` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `seq` int(10) unsigned DEFAULT NULL,
  `taskName` varchar(60) DEFAULT NULL,
  `taskTypeID` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskTypeName` (`taskTypeID`,`taskName`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of stTask
-- ----------------------------
BEGIN;
INSERT INTO `stTask` VALUES (63, 1, 'PLANNING', 1);
INSERT INTO `stTask` VALUES (64, 2, 'FIELD WORK', 1);
INSERT INTO `stTask` VALUES (65, 3, 'REPORTING', 1);
INSERT INTO `stTask` VALUES (66, 4, 'WRAP UP', 1);
INSERT INTO `stTask` VALUES (67, 1, 'ACCOUNTING & FINANCE', 2);
INSERT INTO `stTask` VALUES (68, 2, 'INFORMATION TECHNOLOGY', 2);
INSERT INTO `stTask` VALUES (69, 3, 'HR DEPARTMENT', 2);
INSERT INTO `stTask` VALUES (70, 4, 'SECRETARIAL WORK', 2);
INSERT INTO `stTask` VALUES (71, 5, 'GENERAL AFFAIR', 2);
INSERT INTO `stTask` VALUES (72, 1, 'JOB DEVELOPMENT', 3);
INSERT INTO `stTask` VALUES (73, 2, 'PERSONAL PRACTICE DEVELOPMENT', 3);
INSERT INTO `stTask` VALUES (74, 1, 'OFFICE MEETING', 4);
INSERT INTO `stTask` VALUES (75, 2, 'TRAINING / SEMINAR / COMPANY OUTING', 4);
INSERT INTO `stTask` VALUES (76, 3, 'JOB PRACTICE ADMINISTRATION', 3);
INSERT INTO `stTask` VALUES (77, 3, 'PERSONAL ILLNESS APPROVED', 4);
INSERT INTO `stTask` VALUES (78, 4, 'STUDY LEAVE', 4);
INSERT INTO `stTask` VALUES (79, 5, 'UNPAID LEAVE', 4);
INSERT INTO `stTask` VALUES (80, 6, 'MATERNITY LEAVE', 4);
INSERT INTO `stTask` VALUES (81, 7, 'SICK LEAVE', 4);
INSERT INTO `stTask` VALUES (82, 8, 'PERSONAL HOURS APPROVED', 4);
INSERT INTO `stTask` VALUES (86, 9, 'NOT WORKING YET', 4);
INSERT INTO `stTask` VALUES (87, 10, 'ANNUAL LEAVE', 4);
INSERT INTO `stTask` VALUES (88, 11, 'CUTI BERSAMA', 4);
INSERT INTO `stTask` VALUES (89, 12, 'RESIGN', 4);
COMMIT;

-- ----------------------------
-- Table structure for stTaskType
-- ----------------------------
DROP TABLE IF EXISTS `stTaskType`;
CREATE TABLE `stTaskType` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `taskTypeName` varchar(30) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `isMeal` tinyint(1) DEFAULT '0',
  `isOPE` tinyint(1) DEFAULT '0',
  `isTaxi` tinyint(1) DEFAULT '0',
  `usedProject` tinyint(1) DEFAULT NULL,
  `usedOvertime` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskTypeName` (`taskTypeName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stTaskType
-- ----------------------------
BEGIN;
INSERT INTO `stTaskType` VALUES (1, 'PROJECTS', 1, 1, 1, 1, 1, 1);
INSERT INTO `stTaskType` VALUES (2, 'BACK OFFICE', 1, 1, 0, 1, 0, 1);
INSERT INTO `stTaskType` VALUES (3, 'PREPARATION', 1, 0, 0, 1, 0, 0);
INSERT INTO `stTaskType` VALUES (4, 'LEAVE', 1, 0, 0, 1, 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for trJob
-- ----------------------------
DROP TABLE IF EXISTS `trJob`;
CREATE TABLE `trJob` (
  `JobID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `JobCode` varchar(16) DEFAULT NULL,
  `Description` varchar(255) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `JobArea` varchar(255) DEFAULT NULL,
  `JobCreated` date DEFAULT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `Partner` varchar(32) DEFAULT NULL,
  `Manager` varchar(32) DEFAULT NULL,
  `Supervisor` varchar(32) DEFAULT NULL,
  `IncludeOPE` tinyint(1) NOT NULL DEFAULT '0',
  `IsMeal` tinyint(1) DEFAULT '0',
  `IsOutOfOffice` tinyint(1) DEFAULT '0',
  `IsTaxi` tinyint(1) DEFAULT '0',
  `MealAllowance` decimal(18,0) DEFAULT '0',
  `OutOfOfficeAllowance` decimal(18,0) DEFAULT NULL,
  `TaxiAllowance` decimal(18,0) DEFAULT '0',
  `AdministrativeCharge` decimal(18,0) DEFAULT NULL,
  `OtherExpenseAllowance` decimal(18,0) DEFAULT '0',
  `Fee` decimal(18,0) DEFAULT '0',
  `TimeCharges` decimal(18,0) DEFAULT NULL,
  `Percentage` varchar(32) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL COMMENT '0:Draff; 1:Submit; 2:Approve; 3:Reject; 4:Close; 5:Finish',
  `Flag` tinyint(1) DEFAULT NULL COMMENT '0:Nothing; 1:Draff Revise; 2:Submit Revise; 3:Approve Revise; 4:Reject Revise',
  `CreatedBy` varchar(32) DEFAULT NULL,
  `CreatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UpdateBy` varchar(32) DEFAULT NULL,
  `UpdateAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`JobID`) USING BTREE,
  KEY `fk_ClientID_Job` (`ClientID`),
  CONSTRAINT `fk_ClientID_Job` FOREIGN KEY (`ClientID`) REFERENCES `clClient` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trJob
-- ----------------------------
BEGIN;
INSERT INTO `trJob` VALUES (13, 'GST-1477-03', 'GENERAL AUDIT DECEMBER 31, 2018', 65, 'Office,A,E', NULL, '2019-01-01', '2019-12-31', '281030015', '281130001', '281130001', 0, 1, 1, 1, 390000, 4550000, 10000000, 1250000, 1000000, 25000000, 124150000, '20.14 %', 1, 1, NULL, '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (16, 'ABI-0560-02', 'test', 66, NULL, NULL, '2019-10-02', '2019-10-31', NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 10000000, 0, '0 %', 2, 1, NULL, '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (17, 'ABI-0560-03', 'test2', 66, 'Office,A,E', NULL, '2019-10-01', '2019-10-31', '281130029', '281130029', '281130029', 1, 1, 1, 1, 0, 0, 0, 0, 0, 100000000, 128750000, '77.67 %', 1, 1, NULL, '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (18, 'GST-1477-05', 'teser3', 65, 'Office,A,E', NULL, '2019-10-01', '2019-10-31', '281130001', '281130029', '28115T025', 1, 1, 1, 0, 1000000, NULL, 0, NULL, 3000000, 100000000, 139000000, NULL, 3, 1, NULL, '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (19, 'GST-1477-06', 'test', 65, '1,2', NULL, '2019-10-03', '2019-10-31', '281130001', '281130001', '281130001', 1, 0, 0, 0, 0, 0, 0, NULL, 0, 20000000, 90000000, '22.22 %', 4, 1, '0', '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (20, 'ABI-0560-04', 'perocbaan lagi', 66, NULL, NULL, '2019-10-01', '2019-10-31', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, 200000000, 157500000, '126.98 %', 4, 1, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (21, 'ABI-0560-05', 'fd', 66, NULL, NULL, '2019-10-01', '2019-10-31', '281130001', '281130001', '281130001', 0, 1, 1, 1, 1000000, NULL, 100000, NULL, 1000000, 100000000, 205750000, '48.60 %', 5, 1, '0', '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (22, 'ABI-0560-06', 'test', 66, NULL, NULL, '2019-10-09', '2019-10-31', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, 100000000, 0, '0 %', 5, 1, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (23, 'ABI-0560-07', 'test 6', 66, 'B', NULL, '2019-10-01', '2019-10-09', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, 100000000, 0, '0 %', 5, 1, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (24, 'ABI-0560-08', 'test lagi', 66, 'Office,A,E', NULL, '2019-10-01', '2019-10-30', '281130001', '281130001', '281130029', 1, 0, 1, 1, 0, NULL, 10000000, NULL, 50000000, 100000000, 151750000, NULL, 2, 1, '0', '2019-10-23 01:07:18', '281030013', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (25, 'APM-0445-12', 'GENERAL AUDIT', 73, 'A', NULL, '2019-10-01', '2019-10-31', '281140027', '281130001', '281140027', 1, 1, 1, 1, 1000000, NULL, 20000000, NULL, 50000000, 100000000, 187250000, NULL, 2, 1, '0', '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (26, 'ABI-0560-09', 'GENEERAL AUDIT', 66, 'Office,E', NULL, '2019-10-24', '2019-10-31', '281030015', '281130001', '281130001', 1, 1, 1, 1, 240000000, 10000000, 10000000, NULL, 50000000, 80000000, 356100000, '12.01 %', 2, 1, '0', '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (27, 'ABI-0560-10', 'general audit', 66, 'Office,A,E', NULL, '2019-10-01', '2019-10-31', '281130001', '281130001', '281130001', 1, 1, 1, 1, 90000, 1690000, 1012300, 500000, 10000000, 10000000, 100000000, '8.83 %', 2, 0, '0', '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (28, 'ABI-0560-11', 'GENERAL AUDIT', 66, 'A', NULL, '2019-10-01', '2019-10-31', '281130001', '281130029', '281130029', 1, 1, 1, 1, 90000, 510000, 10000000, 500000, 1000000, 10000000, 31500000, '22.94 %', 2, 1, '0', '2019-10-23 01:27:21', '0', '2019-10-23 01:27:21');
INSERT INTO `trJob` VALUES (29, 'GST-1477-07', 'kians test', 65, 'A,E', NULL, '2019-10-01', '2019-10-31', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, 20000000, 0, '0 %', 0, 0, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (30, 'ABI-0560-12', 'KIANS TEST 2', 66, 'A,D', NULL, '2019-10-18', '2019-10-16', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, 50000000, 0, '0 %', 0, 0, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (31, 'ABI-0560-13', 'kians test 5', 66, 'Office,A,B,C,D,E', NULL, '2019-10-01', '2019-10-15', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, 1000000000, 0, '0 %', 0, 0, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (32, 'ACS-0472-09', 'Tes Job', 69, 'Office,A,E', NULL, '2019-10-01', '2019-10-31', '281130029', '281130029', '281130029', 0, 0, 0, 0, 0, 0, 0, NULL, 50000000, 100000000, 5750000, '1,739.13 %', 2, 1, '0', '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (33, 'CBA-0871-01', 'CAKAP BERKARYA JOB', 76, 'A,C', NULL, '2019-09-01', '2019-10-31', '281070006', '281130029', '28115T025', 1, 0, 0, 0, 0, 0, 0, NULL, 800000000, 800000000, 326000000, '71.05 %', 2, 1, '0', '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (34, 'ABI-0560-14', 'GTS TES JOB', 66, 'Office,A,E', NULL, '2019-10-01', '2019-10-31', '281130001', '281130001', '281130001', 1, 1, 1, 1, 0, 1170000, 0, 5000000, 0, 100000000, 60000000, '151.13 %', 0, 0, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (35, 'ABI-0560-34', 'test lagi dongg ', 66, 'Office,A,E', NULL, '2019-10-01', '2019-10-31', '281130001', '281140027', '281140027', 1, 1, 1, 1, 150000, 4030000, 100000000, 25000000, 100000000, 500000000, 223000000, '110.58 %', 2, 3, '0', '2019-10-23 01:07:18', '0', '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (36, 'ABI-0560-36', 'test 2', 66, NULL, NULL, '2019-10-01', '2019-10-31', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1500000000, 0, '0 %', 3, 0, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJob` VALUES (37, 'ACS-0472-11', 'test lagi cuy', 69, NULL, NULL, '2019-10-01', '2019-10-31', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, 200000000, 0, '0 %', 1, 0, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
COMMIT;

-- ----------------------------
-- Table structure for trJobBudgeting
-- ----------------------------
DROP TABLE IF EXISTS `trJobBudgeting`;
CREATE TABLE `trJobBudgeting` (
  `JobID` int(11) NOT NULL,
  `Seq` tinyint(2) DEFAULT NULL,
  `EmployeeID` varchar(32) NOT NULL,
  `Planning` decimal(6,1) DEFAULT '0.0',
  `PlanningUsed` decimal(6,1) DEFAULT '0.0',
  `FieldWork` decimal(6,1) DEFAULT '0.0',
  `FieldWorkUsed` decimal(6,1) DEFAULT '0.0',
  `Reporting` decimal(6,1) DEFAULT '0.0',
  `ReportingUsed` decimal(6,1) DEFAULT '0.0',
  `WrapUp` decimal(6,1) DEFAULT '0.0',
  `WrapUpUsed` decimal(6,1) DEFAULT '0.0',
  `OverTime` decimal(6,1) DEFAULT '0.0',
  `OverTimeUsed` decimal(6,1) DEFAULT '0.0',
  `Total` decimal(6,1) DEFAULT NULL,
  `CreatedBy` varchar(16) DEFAULT NULL,
  `CreatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UpdateBy` varchar(16) DEFAULT NULL,
  `UpdateAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`JobID`,`EmployeeID`) USING BTREE,
  UNIQUE KEY `Unique_JobIDSeq` (`JobID`,`Seq`),
  KEY `JobId` (`JobID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trJobBudgeting
-- ----------------------------
BEGIN;
INSERT INTO `trJobBudgeting` VALUES (13, NULL, '281030015', 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, NULL, '2019-10-10 23:59:22', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (13, NULL, '281110037', 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, NULL, '2019-10-10 23:58:42', NULL, '2019-10-12 22:53:33');
INSERT INTO `trJobBudgeting` VALUES (13, NULL, '281130001', 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, NULL, '2019-10-11 00:04:52', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (13, NULL, '281130029', 10.0, 0.0, 70.0, 0.0, 10.0, 0.0, 10.0, 0.0, 10.0, 0.0, 100.0, '0', '2019-10-16 23:10:15', NULL, '2019-10-17 00:15:10');
INSERT INTO `trJobBudgeting` VALUES (13, NULL, '281140027', 2.0, 0.0, 2.5, 0.0, 3.0, 0.0, 2.5, 0.0, 0.0, 0.0, 10.0, NULL, '2019-10-12 21:13:06', NULL, '2019-10-12 22:53:33');
INSERT INTO `trJobBudgeting` VALUES (13, NULL, '28115T025', 13.0, 0.0, 91.0, 0.0, 13.0, 0.0, 13.0, 0.0, 33.0, 0.0, 130.0, NULL, '2019-10-11 00:05:36', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (14, NULL, '281130029', 12.0, 0.0, 84.0, 0.0, 12.0, 0.0, 12.0, 0.0, 20.0, 0.0, 120.0, NULL, '2019-10-12 20:19:58', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (14, NULL, '28115T025', 12.0, 0.0, 84.0, 0.0, 12.0, 0.0, 12.0, 0.0, 20.0, 0.0, 120.0, NULL, '2019-10-12 20:16:14', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (15, NULL, '281110037', 80.0, 0.0, 0.0, 0.0, 20.0, 0.0, 0.0, 0.0, 0.0, 0.0, 100.0, NULL, '2019-10-12 21:21:44', NULL, '2019-10-12 22:53:33');
INSERT INTO `trJobBudgeting` VALUES (17, NULL, '281130001', 20.0, 0.0, 25.0, 0.0, 30.0, 0.0, 25.0, 0.0, 0.0, 0.0, 100.0, '0', '2019-10-22 20:20:58', NULL, '2019-10-22 20:20:58');
INSERT INTO `trJobBudgeting` VALUES (17, NULL, '281130029', 3.0, 0.0, 21.0, 0.0, 3.0, 0.0, 3.0, 0.0, 0.0, 0.0, 30.0, '0', '2019-10-22 20:21:10', NULL, '2019-10-22 20:21:10');
INSERT INTO `trJobBudgeting` VALUES (17, NULL, '28115T025', 2.0, 0.0, 14.0, 0.0, 2.0, 0.0, 2.0, 0.0, 0.0, 0.0, 20.0, '0', '2019-10-22 20:22:19', NULL, '2019-10-22 20:22:19');
INSERT INTO `trJobBudgeting` VALUES (18, NULL, '281130001', 3.0, 0.0, 21.0, 0.0, 3.0, 0.0, 3.0, 0.0, 0.0, 0.0, 30.0, '0', '2019-10-12 23:56:00', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (18, NULL, '281130029', 5.0, 0.0, 35.0, 0.0, 5.0, 0.0, 5.0, 0.0, 0.0, 0.0, 50.0, NULL, '2019-10-12 23:45:02', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (18, NULL, '281140027', 8.0, 0.0, 10.0, 0.0, 12.0, 0.0, 10.0, 0.0, 0.0, 0.0, 40.0, '0', '2019-10-12 23:46:00', NULL, '2019-10-12 23:59:52');
INSERT INTO `trJobBudgeting` VALUES (18, NULL, '28115T025', 7.0, 0.0, 49.0, 0.0, 7.0, 0.0, 7.0, 0.0, 20.0, 0.0, 70.0, NULL, '2019-10-12 23:46:42', NULL, '2019-10-12 23:57:12');
INSERT INTO `trJobBudgeting` VALUES (19, NULL, '281130001', 18.0, 0.0, 22.5, 0.0, 27.0, 0.0, 22.5, 0.0, 0.0, 0.0, 90.0, '0', '2019-10-13 00:07:26', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (20, NULL, '281130001', 20.0, 0.0, 25.0, 0.0, 30.0, 0.0, 25.0, 0.0, 0.0, 0.0, 100.0, '0', '2019-10-16 11:19:47', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (20, NULL, '281130029', 10.0, 0.0, 70.0, 0.0, 10.0, 0.0, 10.0, 0.0, 0.0, 0.0, 100.0, '0', '2019-10-16 11:34:52', NULL, '2019-10-16 11:35:34');
INSERT INTO `trJobBudgeting` VALUES (21, NULL, '281130001', 20.0, 0.0, 25.0, 0.0, 30.0, 0.0, 25.0, 0.0, 0.0, 0.0, 100.0, '0', '2019-10-16 11:47:10', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (21, NULL, '281130029', 1.0, 0.0, 7.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 10.0, '0', '2019-10-16 16:50:12', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (21, NULL, '281140027', 20.0, 0.0, 25.0, 0.0, 30.0, 0.0, 25.0, 0.0, 0.0, 0.0, 100.0, '0', '2019-10-16 16:52:30', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (24, NULL, '281130001', 20.0, 0.0, 25.0, 0.0, 30.0, 0.0, 25.0, 0.0, 0.0, 0.0, 100.0, '0', '2019-10-16 22:07:30', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (24, NULL, '281130029', 5.0, 0.0, 35.0, 0.0, 5.0, 0.0, 5.0, 0.0, 0.0, 0.0, 50.0, '0', '2019-10-16 22:08:21', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (24, NULL, '28116T005', 4.0, 0.0, 28.0, 0.0, 4.0, 0.0, 4.0, 0.0, 0.0, 0.0, 40.0, '0', '2019-10-16 22:09:14', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (25, NULL, '281130001', 24.0, 0.0, 30.0, 0.0, 36.0, 0.0, 30.0, 0.0, 20.0, 0.0, 120.0, '0', '2019-10-17 23:51:15', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (25, NULL, '281140027', 10.0, 0.0, 12.5, 0.0, 15.0, 0.0, 12.5, 0.0, 10.0, 0.0, 50.0, '0', '2019-10-17 23:52:31', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (25, NULL, '28116T005', 3.0, 0.0, 21.0, 0.0, 3.0, 0.0, 3.0, 0.0, 0.0, 0.0, 30.0, '0', '2019-10-17 23:53:20', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (26, NULL, '281030015', 0.8, 0.0, 0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, '0', '2019-10-18 00:00:34', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (26, NULL, '281130001', 10.0, 0.0, 12.5, 0.0, 15.0, 0.0, 12.5, 0.0, 10.0, 0.0, 50.0, '0', '2019-10-17 23:59:57', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (26, NULL, '281130029', 10.0, 0.0, 70.0, 0.0, 10.0, 0.0, 10.0, 0.0, 0.0, 0.0, 100.0, '0', '2019-10-20 17:44:11', NULL, '2019-10-20 17:44:11');
INSERT INTO `trJobBudgeting` VALUES (26, NULL, '281140027', 20.0, 0.0, 140.0, 0.0, 20.0, 0.0, 20.0, 0.0, 0.0, 0.0, 200.0, '0', '2019-10-20 17:26:57', NULL, '2019-10-20 17:26:57');
INSERT INTO `trJobBudgeting` VALUES (26, NULL, '28115T025', 5.0, 0.0, 35.0, 0.0, 5.0, 0.0, 5.0, 0.0, 5.0, 0.0, 50.0, '0', '2019-10-18 00:26:24', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (26, NULL, '28116T005', 3.0, 0.0, 21.0, 0.0, 3.0, 0.0, 3.0, 0.0, 10.0, 0.0, 30.0, '0', '2019-10-18 00:25:37', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (27, NULL, '281130001', 20.0, 0.0, 25.0, 0.0, 30.0, 0.0, 25.0, 0.0, 10.0, 0.0, 100.0, '0', '2019-10-18 02:08:59', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (28, NULL, '281130001', 4.0, 0.0, 5.0, 0.0, 6.0, 0.0, 5.0, 0.0, 10.0, 0.0, 20.0, '0', '2019-10-23 01:06:29', NULL, '2019-10-23 01:06:29');
INSERT INTO `trJobBudgeting` VALUES (28, NULL, '281130029', 4.0, 0.0, 5.0, 0.0, 6.0, 0.0, 5.0, 0.0, 0.0, 0.0, 20.0, '0', '2019-10-23 01:07:18', NULL, '2019-10-23 01:07:18');
INSERT INTO `trJobBudgeting` VALUES (32, NULL, '281130029', 1.0, 0.0, 7.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 10.0, '0', '2019-10-18 16:03:27', NULL, NULL);
INSERT INTO `trJobBudgeting` VALUES (33, 1, '281070006', 80.0, 0.0, 0.0, 0.0, 20.0, 0.0, 0.0, 0.0, 0.0, 0.0, 100.0, '0', '2019-10-19 17:10:37', NULL, '2019-10-19 17:10:37');
INSERT INTO `trJobBudgeting` VALUES (33, 2, '281130029', 8.0, 0.0, 56.0, 0.0, 8.0, 0.0, 8.0, 0.0, 20.0, 0.0, 80.0, '0', '2019-10-19 17:10:38', NULL, '2019-10-19 17:10:38');
INSERT INTO `trJobBudgeting` VALUES (33, 3, '28115T025', 8.0, 0.0, 56.0, 0.0, 8.0, 0.0, 8.0, 0.0, 10.0, 0.0, 80.0, '0', '2019-10-19 17:10:39', NULL, '2019-10-19 17:10:39');
INSERT INTO `trJobBudgeting` VALUES (34, 1, '281130001', 2.0, 0.0, 2.5, 0.0, 3.0, 0.0, 2.5, 0.0, 0.0, 0.0, 10.0, '0', '2019-10-19 17:10:35', NULL, '2019-10-19 17:10:35');
INSERT INTO `trJobBudgeting` VALUES (34, 2, '281140027', 10.0, 0.0, 12.5, 0.0, 15.0, 0.0, 12.5, 0.0, 0.0, 0.0, 50.0, '0', '2019-10-22 21:45:47', NULL, '2019-10-22 21:45:47');
INSERT INTO `trJobBudgeting` VALUES (35, 1, '281130001', 20.0, 0.0, 25.0, 0.0, 30.0, 0.0, 25.0, 0.0, 10.0, 0.0, 100.0, '0', '2019-10-20 16:39:44', NULL, '2019-10-20 16:39:44');
INSERT INTO `trJobBudgeting` VALUES (35, 2, '281140027', 20.0, 0.0, 25.0, 0.0, 30.0, 0.0, 25.0, 0.0, 5.0, 0.0, 100.0, '0', '2019-10-20 16:39:48', NULL, '2019-10-20 16:39:48');
INSERT INTO `trJobBudgeting` VALUES (35, 3, '28115T025', 10.0, 0.0, 10.0, 0.0, 10.0, 0.0, 10.0, 0.0, 0.0, 0.0, 40.0, NULL, '2019-10-20 12:24:55', NULL, '2019-10-20 12:24:55');
COMMIT;

-- ----------------------------
-- Table structure for trJobBudgetingTemp
-- ----------------------------
DROP TABLE IF EXISTS `trJobBudgetingTemp`;
CREATE TABLE `trJobBudgetingTemp` (
  `JobID` int(11) NOT NULL,
  `Seq` tinyint(2) DEFAULT NULL,
  `EmployeeID` varchar(32) NOT NULL,
  `Planning` decimal(6,1) DEFAULT '0.0',
  `FieldWork` decimal(6,1) DEFAULT '0.0',
  `Reporting` decimal(6,1) DEFAULT '0.0',
  `WrapUp` decimal(6,1) DEFAULT '0.0',
  `OverTime` decimal(6,1) DEFAULT '0.0',
  `Total` decimal(6,1) DEFAULT NULL,
  `CreatedBy` varchar(16) DEFAULT NULL,
  `CreatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UpdateBy` varchar(16) DEFAULT NULL,
  `UpdateAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`JobID`,`EmployeeID`) USING BTREE,
  UNIQUE KEY `Unique_JobIDSeq` (`JobID`,`Seq`),
  KEY `JobId` (`JobID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trJobBudgetingTemp
-- ----------------------------
BEGIN;
INSERT INTO `trJobBudgetingTemp` VALUES (16, NULL, '281130001', 20.0, 25.0, 30.0, 25.0, 0.0, 100.0, '0', '2019-10-22 21:38:55', NULL, '2019-10-22 21:38:55');
INSERT INTO `trJobBudgetingTemp` VALUES (24, NULL, '281130001', 20.0, 25.0, 30.0, 25.0, 0.0, 100.0, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobBudgetingTemp` VALUES (24, NULL, '281130029', 5.0, 35.0, 5.0, 5.0, 0.0, 50.0, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobBudgetingTemp` VALUES (24, NULL, '28116T005', 4.0, 28.0, 4.0, 4.0, 0.0, 40.0, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobBudgetingTemp` VALUES (25, NULL, '281130001', 24.0, 30.0, 36.0, 30.0, 20.0, 120.0, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobBudgetingTemp` VALUES (25, NULL, '281140027', 10.0, 12.5, 15.0, 12.5, 10.0, 50.0, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobBudgetingTemp` VALUES (25, NULL, '28116T005', 3.0, 21.0, 3.0, 3.0, 0.0, 30.0, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobBudgetingTemp` VALUES (26, NULL, '281030015', 0.8, 0.0, 0.2, 0.0, 0.0, 1.0, 'system', '2019-10-22 09:21:33', 'system', '2019-10-22 09:21:33');
INSERT INTO `trJobBudgetingTemp` VALUES (26, NULL, '281130001', 10.0, 12.5, 15.0, 12.5, 10.0, 50.0, 'system', '2019-10-22 09:21:33', 'system', '2019-10-22 09:21:33');
INSERT INTO `trJobBudgetingTemp` VALUES (26, NULL, '281130029', 10.0, 70.0, 10.0, 10.0, 0.0, 100.0, 'system', '2019-10-22 09:21:33', 'system', '2019-10-22 09:21:33');
INSERT INTO `trJobBudgetingTemp` VALUES (26, NULL, '281140027', 20.0, 140.0, 20.0, 20.0, 0.0, 200.0, 'system', '2019-10-22 09:21:33', 'system', '2019-10-22 09:21:33');
INSERT INTO `trJobBudgetingTemp` VALUES (26, NULL, '28115T025', 5.0, 35.0, 5.0, 5.0, 5.0, 50.0, 'system', '2019-10-22 09:21:33', 'system', '2019-10-22 09:21:33');
INSERT INTO `trJobBudgetingTemp` VALUES (26, NULL, '28116T005', 3.0, 21.0, 3.0, 3.0, 10.0, 30.0, 'system', '2019-10-22 09:21:33', 'system', '2019-10-22 09:21:33');
INSERT INTO `trJobBudgetingTemp` VALUES (28, NULL, '281130001', 4.0, 5.0, 6.0, 5.0, 10.0, 20.0, 'system', '2019-10-23 01:27:21', 'system', '2019-10-23 01:27:21');
INSERT INTO `trJobBudgetingTemp` VALUES (28, NULL, '281130029', 4.0, 5.0, 6.0, 5.0, 0.0, 20.0, 'system', '2019-10-23 01:27:21', 'system', '2019-10-23 01:27:21');
INSERT INTO `trJobBudgetingTemp` VALUES (32, NULL, '281130029', 1.0, 7.0, 1.0, 1.0, 0.0, 10.0, 'system', '2019-10-22 19:31:10', 'system', '2019-10-22 19:31:10');
INSERT INTO `trJobBudgetingTemp` VALUES (33, NULL, '281070006', 80.0, 0.0, 20.0, 0.0, 0.0, 100.0, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobBudgetingTemp` VALUES (33, NULL, '281130029', 8.0, 56.0, 8.0, 8.0, 20.0, 80.0, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobBudgetingTemp` VALUES (33, NULL, '28115T025', 8.0, 56.0, 8.0, 8.0, 10.0, 80.0, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
COMMIT;

-- ----------------------------
-- Table structure for trJobComment
-- ----------------------------
DROP TABLE IF EXISTS `trJobComment`;
CREATE TABLE `trJobComment` (
  `CommentId` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `Comments` text,
  `CreatedBy` varchar(16) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CommentId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for trJobTemp
-- ----------------------------
DROP TABLE IF EXISTS `trJobTemp`;
CREATE TABLE `trJobTemp` (
  `JobID` int(11) NOT NULL,
  `JobCode` varchar(16) DEFAULT NULL,
  `Description` varchar(255) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `JobArea` varchar(255) DEFAULT NULL,
  `JobCreated` date DEFAULT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `Partner` varchar(16) DEFAULT NULL,
  `Manager` varchar(16) DEFAULT NULL,
  `Supervisor` varchar(16) DEFAULT NULL,
  `IncludeOPE` tinyint(1) NOT NULL DEFAULT '0',
  `IsMeal` int(1) DEFAULT '0',
  `IsOutOfOffice` int(1) DEFAULT '0',
  `IsTaxi` int(1) DEFAULT '0',
  `MealAllowance` decimal(18,0) DEFAULT '0',
  `OutOfOfficeAllowance` decimal(18,0) DEFAULT NULL,
  `TaxiAllowance` decimal(18,0) DEFAULT '0',
  `AdministrativeCharge` decimal(18,0) DEFAULT NULL,
  `OtherExpenseAllowance` decimal(18,0) DEFAULT '0',
  `Fee` decimal(18,0) DEFAULT '0',
  `TimeCharges` decimal(18,0) DEFAULT NULL,
  `Percentage` varchar(32) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL COMMENT '0:Draff; 1:Submit; 2:Reject; 3:Approve; 4:Finish; 5:Close',
  `Flag` tinyint(1) DEFAULT NULL,
  `CreatedBy` varchar(32) DEFAULT NULL,
  `CreatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UpdateBy` varchar(32) DEFAULT NULL,
  `UpdateAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`JobID`) USING BTREE,
  KEY `fk_ClientID_Job` (`ClientID`),
  CONSTRAINT `trJobTemp_ibfk_1` FOREIGN KEY (`ClientId`) REFERENCES `clClient` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trJobTemp
-- ----------------------------
BEGIN;
INSERT INTO `trJobTemp` VALUES (16, 'ABI-0560-02', 'test', 66, 'Office,A,E', NULL, '2019-10-02', '2019-10-31', '281130001', '281130001', '281130001', 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 10000000, 0, '0 %', 2, 1, NULL, '2019-10-22 21:56:26', NULL, '2019-10-22 21:56:26');
INSERT INTO `trJobTemp` VALUES (24, 'ABI-0560-08', 'test lagi', 66, 'Office,A,E', NULL, '2019-10-01', '2019-10-30', '281130001', '281130001', '281130029', 1, 0, 1, 1, 0, NULL, 10000000, NULL, 50000000, 100000000, 151750000, NULL, 2, 1, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobTemp` VALUES (25, 'APM-0445-12', 'GENERAL AUDIT', 73, 'A', NULL, '2019-10-01', '2019-10-31', '281140027', '281130001', '281140027', 1, 1, 1, 1, 1000000, NULL, 20000000, NULL, 50000000, 100000000, 187250000, NULL, 2, 1, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
INSERT INTO `trJobTemp` VALUES (26, 'ABI-0560-09', 'GENEERAL AUDIT', 66, 'Office,E', NULL, '2019-10-24', '2019-10-31', '281030015', '281130001', '281130001', 1, 1, 1, 1, 240000000, 10000000, 10000000, NULL, 50000000, 80000000, 356100000, '12.01 %', 2, 1, 'system', '2019-10-22 09:21:33', 'system', '2019-10-22 09:21:33');
INSERT INTO `trJobTemp` VALUES (28, 'ABI-0560-11', 'GENERAL AUDIT', 66, 'A', NULL, '2019-10-01', '2019-10-31', '281130001', '281130029', '281130029', 1, 0, 1, 0, 0, 0, 0, 500000, 1000000, 10000000, 31500000, '22.94 %', 2, 1, 'system', '2019-10-23 01:28:34', 'system', '2019-10-23 01:28:34');
INSERT INTO `trJobTemp` VALUES (32, 'ACS-0472-09', 'Tes Job', 69, 'Office,A,E', NULL, '2019-10-01', '2019-10-31', '281130029', '281130029', '281130029', 0, 0, 0, 0, 0, 0, 0, NULL, 50000000, 100000000, 5750000, '1,739.13 %', 2, 1, 'system', '2019-10-22 19:31:10', 'system', '2019-10-22 19:31:10');
INSERT INTO `trJobTemp` VALUES (33, 'CBA-0871-01', 'CAKAP BERKARYA JOB', 76, 'A,C', NULL, '2019-09-01', '2019-10-31', '281070006', '281130029', '28115T025', 1, 0, 0, 0, 0, 0, 0, NULL, 800000000, 800000000, 326000000, '71.05 %', 2, 1, 'system', '2019-10-23 00:44:31', 'system', '2019-10-23 00:44:31');
COMMIT;

-- ----------------------------
-- Table structure for trTaxi
-- ----------------------------
DROP TABLE IF EXISTS `trTaxi`;
CREATE TABLE `trTaxi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TimeReportDetailID` int(11) NOT NULL,
  `cmTaxiID` smallint(5) NOT NULL,
  `Start` time NOT NULL,
  `Finish` time NOT NULL,
  `VoucherNo` varchar(30) NOT NULL,
  `Destination` varchar(100) NOT NULL,
  `Amount` decimal(18,0) NOT NULL,
  `Description` text,
  `Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trTaxi
-- ----------------------------
BEGIN;
INSERT INTO `trTaxi` VALUES (1, 50, 1, '07:04:00', '07:04:00', '123456789', 'BOGOR STATION', 200000, '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for trTimeReport
-- ----------------------------
DROP TABLE IF EXISTS `trTimeReport`;
CREATE TABLE `trTimeReport` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `EmployeeId` varchar(32) CHARACTER SET latin1 NOT NULL,
  `Date` date NOT NULL,
  `isStayed` tinyint(1) NOT NULL,
  `maxOverTime` varchar(45) DEFAULT NULL,
  `maxMeal` int(1) DEFAULT NULL COMMENT 'Isinya Claim Quantiy di Rule',
  `maxOPE` int(1) DEFAULT NULL COMMENT 'Isinya Claim Quantiy di Rule',
  `Status` int(3) NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE KEY `EmployeeID_Date_Unique` (`EmployeeId`,`Date`),
  CONSTRAINT `fkEmployeeID_TimeReport` FOREIGN KEY (`EmployeeId`) REFERENCES `hrEmployee` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trTimeReport
-- ----------------------------
BEGIN;
INSERT INTO `trTimeReport` VALUES (9, '0', '2019-10-10', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (10, '0', '2019-10-11', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (11, '28115T025', '2019-10-11', 0, NULL, NULL, NULL, 2);
INSERT INTO `trTimeReport` VALUES (12, '28115T025', '2019-10-10', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (13, '0', '2019-10-13', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (14, '0', '2019-10-12', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (19, '28116T005', '2019-10-13', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (20, '28116T005', '2019-10-12', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (21, '28116T005', '2019-10-11', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (22, '28115T025', '2019-10-14', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (23, '0', '2019-10-15', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (24, '0', '2019-10-16', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (25, '28115T025', '2019-10-16', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (26, '281110014', '2019-10-16', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (27, '281130001', '2019-10-17', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (28, '0', '2019-10-18', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (29, '28115T025', '2019-10-18', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (30, '281130029', '2019-10-17', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (31, '281130029', '2019-10-18', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (32, '281130029', '2019-10-16', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (33, '0', '2019-10-19', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (34, '0', '2019-10-20', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (35, '28115T025', '2019-10-20', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (36, '281030015', '2019-10-21', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (37, '28115T025', '2019-10-22', 0, NULL, NULL, NULL, 0);
INSERT INTO `trTimeReport` VALUES (38, '28115T025', '2019-10-23', 0, NULL, NULL, NULL, 0);
COMMIT;

-- ----------------------------
-- Table structure for trTimeReportDetail
-- ----------------------------
DROP TABLE IF EXISTS `trTimeReportDetail`;
CREATE TABLE `trTimeReportDetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID untuk disambungkan ke Detail Taxi',
  `TimeReportID` int(11) NOT NULL,
  `TaskTypeID` smallint(5) unsigned NOT NULL,
  `JobId` int(11) DEFAULT NULL,
  `TaskID` int(11) NOT NULL,
  `WorkHour` int(1) DEFAULT NULL,
  `Overtime` int(1) DEFAULT NULL,
  `Meal` int(1) NOT NULL DEFAULT '0',
  `Transportation` int(1) DEFAULT NULL,
  `OutOfOffice` int(1) NOT NULL DEFAULT '0',
  `Zone` varchar(255) DEFAULT NULL,
  `Status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `JobId` (`JobId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trTimeReportDetail
-- ----------------------------
BEGIN;
INSERT INTO `trTimeReportDetail` VALUES (63, 35, 1, 13, 63, 6, 4, 0, NULL, 0, NULL, NULL);
INSERT INTO `trTimeReportDetail` VALUES (64, 35, 1, 26, 64, 2, 0, 0, NULL, 0, NULL, NULL);
INSERT INTO `trTimeReportDetail` VALUES (65, 38, 1, 13, 64, 8, 4, 0, NULL, 0, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for trTimeReportMeals
-- ----------------------------
DROP TABLE IF EXISTS `trTimeReportMeals`;
CREATE TABLE `trTimeReportMeals` (
  `TimeReportID` int(11) unsigned NOT NULL,
  `Seq` tinyint(2) unsigned NOT NULL,
  `TaskTypeID` smallint(5) unsigned NOT NULL,
  `JobId` int(11) unsigned DEFAULT NULL,
  `TaskID` int(11) unsigned NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `Unique_Meals1` (`TimeReportID`,`Seq`),
  KEY `fkJobId_Meals` (`JobId`) USING BTREE,
  KEY `fkTaskTypeID_Meals` (`TaskTypeID`),
  KEY `fkTaskID_Meals` (`TaskID`),
  KEY `fkTimeReportID_Meals` (`TimeReportID`) USING BTREE,
  CONSTRAINT `fkJobID_Meals` FOREIGN KEY (`JobId`) REFERENCES `trJob` (`JobID`) ON UPDATE CASCADE,
  CONSTRAINT `fkTaskID_Meals` FOREIGN KEY (`TaskID`) REFERENCES `stTask` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fkTaskTypeID_Meals` FOREIGN KEY (`TaskTypeID`) REFERENCES `stTaskType` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fkTimeReportID_Meals` FOREIGN KEY (`TimeReportID`) REFERENCES `trTimeReport` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trTimeReportMeals
-- ----------------------------
BEGIN;
INSERT INTO `trTimeReportMeals` VALUES (35, 2, 1, 13, 63, 0);
INSERT INTO `trTimeReportMeals` VALUES (38, 2, 1, 13, 64, 0);
COMMIT;

-- ----------------------------
-- Table structure for trTimeReportNote
-- ----------------------------
DROP TABLE IF EXISTS `trTimeReportNote`;
CREATE TABLE `trTimeReportNote` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TimeReportID` int(11) DEFAULT NULL,
  `ParentID` int(11) DEFAULT NULL,
  `Type` smallint(1) DEFAULT NULL COMMENT '1:Details; 2:Meals; 3:OutOfOffice; 4:Taxi',
  `EmployeeId` varchar(32) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `Notes` text,
  `CreatedBy` varchar(16) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `EmployeeId` (`EmployeeId`,`Date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trTimeReportNote
-- ----------------------------
BEGIN;
INSERT INTO `trTimeReportNote` VALUES (7, 35, 2, 3, '28115T025', '2019-10-23', '<p>OUT OF OFFICE</p>', NULL, '2019-10-23 08:10:03');
INSERT INTO `trTimeReportNote` VALUES (8, 35, 2, 2, '28115T025', '2019-10-23', '<p>MEALS</p>', NULL, '2019-10-23 08:10:38');
INSERT INTO `trTimeReportNote` VALUES (9, 35, 2, 2, '28115T025', '2019-10-23', '<p>TES MEALS</p><p><br></p>', NULL, '2019-10-23 08:11:07');
INSERT INTO `trTimeReportNote` VALUES (10, 35, 64, 1, '28115T025', '2019-10-23', '<p>WORKHOURS 2<br></p>', NULL, '2019-10-23 08:11:28');
COMMIT;

-- ----------------------------
-- Table structure for trTimeReportOutOfOffice
-- ----------------------------
DROP TABLE IF EXISTS `trTimeReportOutOfOffice`;
CREATE TABLE `trTimeReportOutOfOffice` (
  `TimeReportID` int(11) unsigned NOT NULL,
  `Seq` tinyint(2) unsigned NOT NULL,
  `TaskTypeID` smallint(5) unsigned NOT NULL,
  `JobId` int(11) unsigned DEFAULT NULL,
  `TaskID` int(11) unsigned NOT NULL,
  `ZoneID` varchar(255) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `Unique_OutOfOffice` (`TimeReportID`,`Seq`),
  KEY `fkJobId_OutOfOffice` (`JobId`) USING BTREE,
  KEY `fkTaskTypeID_OutOfOffice` (`TaskTypeID`),
  KEY `fkTaskID_OutOfOffice` (`TaskID`),
  KEY `fkTimeReportID_OutOfOffice` (`TimeReportID`) USING BTREE,
  CONSTRAINT `trJobID_OutOfOffice` FOREIGN KEY (`JobId`) REFERENCES `trJob` (`JobID`) ON UPDATE CASCADE,
  CONSTRAINT `trTaskID_OutOfOffice` FOREIGN KEY (`TaskID`) REFERENCES `stTask` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `trTaskTypeID_OutOfOffice` FOREIGN KEY (`TaskTypeID`) REFERENCES `stTaskType` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `trTimeReportID_OutOfOffice` FOREIGN KEY (`TimeReportID`) REFERENCES `trTimeReport` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trTimeReportOutOfOffice
-- ----------------------------
BEGIN;
INSERT INTO `trTimeReportOutOfOffice` VALUES (35, 2, 1, 13, 63, 'E', 0);
INSERT INTO `trTimeReportOutOfOffice` VALUES (38, 2, 1, 13, 64, 'A', 0);
COMMIT;

-- ----------------------------
-- Table structure for trTimeReportTaxi
-- ----------------------------
DROP TABLE IF EXISTS `trTimeReportTaxi`;
CREATE TABLE `trTimeReportTaxi` (
  `TimeReportID` int(11) unsigned NOT NULL,
  `Seq` tinyint(2) unsigned NOT NULL,
  `cmTaxiID` smallint(5) unsigned NOT NULL,
  `TaskTypeID` smallint(5) unsigned DEFAULT NULL,
  `JobId` int(11) unsigned DEFAULT NULL,
  `TaskID` int(11) unsigned DEFAULT NULL,
  `UsedProject` tinyint(1) DEFAULT NULL,
  `TaskJobID` int(11) DEFAULT NULL,
  `Start` time NOT NULL,
  `Finish` time NOT NULL,
  `VoucherNo` varchar(30) NOT NULL,
  `Destination` varchar(100) NOT NULL,
  `Amount` decimal(18,0) NOT NULL,
  `Description` text,
  `Status` tinyint(1) DEFAULT NULL,
  UNIQUE KEY `Unique_Taxi` (`TimeReportID`,`Seq`),
  KEY `fkJobID_Taxi` (`JobId`),
  KEY `fkTaskID_Taxi` (`TaskID`),
  KEY `fkTaskTypeID_Taxi` (`TaskTypeID`),
  KEY `fkTimeReportID_Taxi` (`TimeReportID`),
  KEY `fkCmTaxiID_Taxi` (`cmTaxiID`),
  CONSTRAINT `fkCmTaxiID_Taxi` FOREIGN KEY (`cmTaxiID`) REFERENCES `cmTaxi` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fkJobID_Taxi` FOREIGN KEY (`JobId`) REFERENCES `trJob` (`JobID`) ON UPDATE CASCADE,
  CONSTRAINT `fkTaskID_Taxi` FOREIGN KEY (`TaskID`) REFERENCES `stTask` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fkTaskTypeID_Taxi` FOREIGN KEY (`TaskTypeID`) REFERENCES `stTaskType` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fkTimeReportID_Taxi` FOREIGN KEY (`TimeReportID`) REFERENCES `trTimeReport` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- View structure for vAutoNumber
-- ----------------------------
DROP VIEW IF EXISTS `vAutoNumber`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `vAutoNumber` AS select `auto_number`.`group` AS `group`,`auto_number`.`number` AS `number`,`auto_number`.`optimistic_lock` AS `optimistic_lock`,`auto_number`.`update_time` AS `update_time`,replace(substring_index(substring_index(`auto_number`.`data`,';',2),':',-(1)),'"','') AS `class`,replace(substring_index(substring_index(`auto_number`.`data`,';',4),':',-(1)),'"','') AS `groupby`,replace(substring_index(substring_index(`auto_number`.`data`,';',6),':',-(1)),'"','') AS `attribute`,replace(substring_index(substring_index(`auto_number`.`data`,';',8),':',-(1)),'"','') AS `value` from `auto_number`;

-- ----------------------------
-- View structure for vempGroup
-- ----------------------------
DROP VIEW IF EXISTS `vempGroup`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `vempGroup` AS select `h`.`Id` AS `id`,`h`.`fullName` AS `fullName`,`h`.`entityId` AS `entityID`,`h`.`divisionID` AS `divisionID`,`l`.`levelName` AS `levelName` from (`hrEmployee` `h` left join `cmLevel` `l` on((`l`.`id` = `h`.`levelID`))) where `h`.`Id` in (select coalesce(`i`.`parentID`,'') from `hrEmployee` `i`);

-- ----------------------------
-- View structure for viewClientZone
-- ----------------------------
DROP VIEW IF EXISTS `viewClientZone`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `viewClientZone` AS select `tz`.`termZoneName` AS `termZoneName` from `stRuleTermZone` `tz` where ((find_in_set(`tz`.`termZoneName`,(select `i`.`termZoneName` from `stRuleItem` `i` where (`i`.`isClientZone` = 1))) <> 0) and (`tz`.`isStayed` = 0)) order by `tz`.`id`;

-- ----------------------------
-- Procedure structure for ATES2
-- ----------------------------
DROP PROCEDURE IF EXISTS `ATES2`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`ATES2`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for ATEST
-- ----------------------------
DROP PROCEDURE IF EXISTS `ATEST`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`ATEST`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getApprovalManager
-- ----------------------------
DROP FUNCTION IF EXISTS `getApprovalManager`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getApprovalManager`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getApprovalSupervisor
-- ----------------------------
DROP FUNCTION IF EXISTS `getApprovalSupervisor`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getApprovalSupervisor`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getAuditChange
-- ----------------------------
DROP FUNCTION IF EXISTS `getAuditChange`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getAuditChange`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getAuditDescription
-- ----------------------------
DROP FUNCTION IF EXISTS `getAuditDescription`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getAuditDescription`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getAuditDescriptionAll
-- ----------------------------
DROP PROCEDURE IF EXISTS `getAuditDescriptionAll`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getAuditDescriptionAll`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getAuditDescriptionDetail
-- ----------------------------
DROP FUNCTION IF EXISTS `getAuditDescriptionDetail`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getAuditDescriptionDetail`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getAuditTransactionType
-- ----------------------------
DROP FUNCTION IF EXISTS `getAuditTransactionType`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getAuditTransactionType`()

;;
delimiter ;

-- ----------------------------
-- Function structure for GetAuditTrJob
-- ----------------------------
DROP FUNCTION IF EXISTS `GetAuditTrJob`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`GetAuditTrJob`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getAuditTrJobBudgeting
-- ----------------------------
DROP FUNCTION IF EXISTS `getAuditTrJobBudgeting`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getAuditTrJobBudgeting`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getBillingRateLevel
-- ----------------------------
DROP PROCEDURE IF EXISTS `getBillingRateLevel`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getBillingRateLevel`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getCalendarType
-- ----------------------------
DROP FUNCTION IF EXISTS `getCalendarType`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getCalendarType`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getClientZone
-- ----------------------------
DROP PROCEDURE IF EXISTS `getClientZone`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getClientZone`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getClientZone2
-- ----------------------------
DROP PROCEDURE IF EXISTS `getClientZone2`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getClientZone2`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getCmMaxOvertime
-- ----------------------------
DROP FUNCTION IF EXISTS `getCmMaxOvertime`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getCmMaxOvertime`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getCmWorkHours
-- ----------------------------
DROP FUNCTION IF EXISTS `getCmWorkHours`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getCmWorkHours`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getEffectiveBillingID
-- ----------------------------
DROP FUNCTION IF EXISTS `getEffectiveBillingID`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getEffectiveBillingID`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getEmpCalendar
-- ----------------------------
DROP PROCEDURE IF EXISTS `getEmpCalendar`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getEmpCalendar`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getEmpCalendarCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `getEmpCalendarCount`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getEmpCalendarCount`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getEmpLevelName
-- ----------------------------
DROP FUNCTION IF EXISTS `getEmpLevelName`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getEmpLevelName`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getEmployeeIDApproval
-- ----------------------------
DROP FUNCTION IF EXISTS `getEmployeeIDApproval`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getEmployeeIDApproval`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getEmployeeIDApproval2
-- ----------------------------
DROP FUNCTION IF EXISTS `getEmployeeIDApproval2`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getEmployeeIDApproval2`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getEmpUserName
-- ----------------------------
DROP FUNCTION IF EXISTS `getEmpUserName`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getEmpUserName`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getJobCountHeader
-- ----------------------------
DROP FUNCTION IF EXISTS `getJobCountHeader`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getJobCountHeader`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getJobEstAdmCharge
-- ----------------------------
DROP FUNCTION IF EXISTS `getJobEstAdmCharge`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getJobEstAdmCharge`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getJobEstMealAllow
-- ----------------------------
DROP FUNCTION IF EXISTS `getJobEstMealAllow`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getJobEstMealAllow`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getJobEstOutOfOfficeAllow
-- ----------------------------
DROP FUNCTION IF EXISTS `getJobEstOutOfOfficeAllow`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getJobEstOutOfOfficeAllow`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getJobEstRulesMeal
-- ----------------------------
DROP PROCEDURE IF EXISTS `getJobEstRulesMeal`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getJobEstRulesMeal`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getJobEstRulesOutOfOffice
-- ----------------------------
DROP PROCEDURE IF EXISTS `getJobEstRulesOutOfOffice`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getJobEstRulesOutOfOffice`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getJobEstTimeCharges
-- ----------------------------
DROP FUNCTION IF EXISTS `getJobEstTimeCharges`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getJobEstTimeCharges`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getJobNo
-- ----------------------------
DROP PROCEDURE IF EXISTS `getJobNo`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getJobNo`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getJobTimeUsed
-- ----------------------------
DROP FUNCTION IF EXISTS `getJobTimeUsed`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getJobTimeUsed`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getJobZone
-- ----------------------------
DROP PROCEDURE IF EXISTS `getJobZone`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getJobZone`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getListRuleTimeReport
-- ----------------------------
DROP PROCEDURE IF EXISTS `getListRuleTimeReport`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getListRuleTimeReport`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getOvertimes
-- ----------------------------
DROP FUNCTION IF EXISTS `getOvertimes`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getOvertimes`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getRuleLevel
-- ----------------------------
DROP PROCEDURE IF EXISTS `getRuleLevel`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getRuleLevel`()
  NO SQL 

;;
delimiter ;

-- ----------------------------
-- Function structure for getRulesIDCurrent
-- ----------------------------
DROP FUNCTION IF EXISTS `getRulesIDCurrent`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getRulesIDCurrent`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getRuleTaskAE
-- ----------------------------
DROP PROCEDURE IF EXISTS `getRuleTaskAE`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getRuleTaskAE`()
  NO SQL 

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getRuleTermZone
-- ----------------------------
DROP PROCEDURE IF EXISTS `getRuleTermZone`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getRuleTermZone`()
  NO SQL 

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getRuleTermZoneName
-- ----------------------------
DROP PROCEDURE IF EXISTS `getRuleTermZoneName`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getRuleTermZoneName`()
  NO SQL 

;;
delimiter ;

-- ----------------------------
-- Function structure for getStHolidaySign
-- ----------------------------
DROP FUNCTION IF EXISTS `getStHolidaySign`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getStHolidaySign`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTimeReportAllocation
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTimeReportAllocation`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTimeReportAllocation`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrJob
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrJob`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrJob`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrMealList
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrMealList`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrMealList`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrMealRule
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrMealRule`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrMealRule`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrMealRule2
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrMealRule2`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrMealRule2`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrOptions
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrOptions`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrOptions`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrOutOfOfficeList
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrOutOfOfficeList`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrOutOfOfficeList`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrOutOfOfficeRule
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrOutOfOfficeRule`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrOutOfOfficeRule`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrOutOfOfficeRule2
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrOutOfOfficeRule2`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrOutOfOfficeRule2`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getTrSpvApprove
-- ----------------------------
DROP FUNCTION IF EXISTS `getTrSpvApprove`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getTrSpvApprove`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrSpvApproveDateSummary
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrSpvApproveDateSummary`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrSpvApproveDateSummary`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrSpvApproveMonthSummary
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrSpvApproveMonthSummary`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrSpvApproveMonthSummary`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getTrSupervisorApprovalList
-- ----------------------------
DROP FUNCTION IF EXISTS `getTrSupervisorApprovalList`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getTrSupervisorApprovalList`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrTaxiClassList
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrTaxiClassList`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrTaxiClassList`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrTaxiList
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrTaxiList`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrTaxiList`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getTrTaxiRule
-- ----------------------------
DROP PROCEDURE IF EXISTS `getTrTaxiRule`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getTrTaxiRule`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for getValueTaskAE
-- ----------------------------
DROP PROCEDURE IF EXISTS `getValueTaskAE`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`getValueTaskAE`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getWeekOfMonth
-- ----------------------------
DROP FUNCTION IF EXISTS `getWeekOfMonth`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getWeekOfMonth`()

;;
delimiter ;

-- ----------------------------
-- Function structure for getWorkHours
-- ----------------------------
DROP FUNCTION IF EXISTS `getWorkHours`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`getWorkHours`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for JobEmpTimeDiffDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `JobEmpTimeDiffDetail`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`JobEmpTimeDiffDetail`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for JobEmpTimeDiffSummary
-- ----------------------------
DROP PROCEDURE IF EXISTS `JobEmpTimeDiffSummary`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`JobEmpTimeDiffSummary`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for JobEstimated
-- ----------------------------
DROP PROCEDURE IF EXISTS `JobEstimated`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`JobEstimated`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for SetJobCountHeaderList
-- ----------------------------
DROP PROCEDURE IF EXISTS `SetJobCountHeaderList`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`SetJobCountHeaderList`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for SetJobList
-- ----------------------------
DROP PROCEDURE IF EXISTS `SetJobList`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`SetJobList`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for SetTrJobTempFromJob
-- ----------------------------
DROP PROCEDURE IF EXISTS `SetTrJobTempFromJob`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`SetTrJobTempFromJob`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for setTrManagerApprove
-- ----------------------------
DROP PROCEDURE IF EXISTS `setTrManagerApprove`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`setTrManagerApprove`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for setTrSupervisorApprove
-- ----------------------------
DROP PROCEDURE IF EXISTS `setTrSupervisorApprove`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`setTrSupervisorApprove`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for setTrSupervisorApproveAll
-- ----------------------------
DROP PROCEDURE IF EXISTS `setTrSupervisorApproveAll`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`setTrSupervisorApproveAll`()

;;
delimiter ;

-- ----------------------------
-- Function structure for Split_String
-- ----------------------------
DROP FUNCTION IF EXISTS `Split_String`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`Split_String`()

;;
delimiter ;

-- ----------------------------
-- Function structure for SupervisorApprovalStatus
-- ----------------------------
DROP FUNCTION IF EXISTS `SupervisorApprovalStatus`;
delimiter ;;
CREATE FUNCTION `knncoid_tr`.`SupervisorApprovalStatus`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for SupervisorApprovalTier1
-- ----------------------------
DROP PROCEDURE IF EXISTS `SupervisorApprovalTier1`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`SupervisorApprovalTier1`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for SupervisorApprovalTier2
-- ----------------------------
DROP PROCEDURE IF EXISTS `SupervisorApprovalTier2`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`SupervisorApprovalTier2`()

;;
delimiter ;

-- ----------------------------
-- Procedure structure for UNUSED_getTimeReportAllocationDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `UNUSED_getTimeReportAllocationDetail`;
delimiter ;;
CREATE PROCEDURE `knncoid_tr`.`UNUSED_getTimeReportAllocationDetail`()

;;
delimiter ;

-- ----------------------------
-- Triggers structure for table AuditTrail
-- ----------------------------
DROP TRIGGER IF EXISTS `AuditTrail_Before_Insert`;
delimiter ;;
CREATE TRIGGER `AuditTrail_Before_Insert` BEFORE INSERT ON `AuditTrail` FOR EACH ROW BEGIN
	SET New.TransactionType = getAuditTransactionType( New.Source, New.TransType );
  SET New.AuditDescription = getAuditDescription( New.Source, New.TransType, New.TransID );
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table AuditTrail
-- ----------------------------
DROP TRIGGER IF EXISTS `AuditTrail_After_Insert`;
delimiter ;;
CREATE TRIGGER `AuditTrail_After_Insert` AFTER INSERT ON `AuditTrail` FOR EACH ROW BEGIN
	DECLARE vTransID INT(11);
	DECLARE vTransSeq TINYINT(2);
	DECLARE vStatus TINYINT(1);
  DECLARE done INT DEFAULT 0;
	DECLARE vJobBudgeting CURSOR FOR
		SELECT DISTINCT r.JobID, r.Seq
		, CASE
				WHEN ( IFNULL( jb.Seq,0 ) = 0 ) AND ( IFNULL( jbt.Seq,0 ) <> 0 ) THEN 1  -- Insert Data
				WHEN ( IFNULL( jb.Seq,0 ) <> 0 ) AND ( IFNULL( jbt.Seq,0 ) <> 0 ) THEN 2  -- Update Data
				WHEN ( IFNULL( jb.Seq,0 ) <> 0 ) AND ( IFNULL( jbt.Seq,0 ) = 0 ) THEN 3  -- Delete Data
			END Status
		FROM
		(
			SELECT JobID, Seq
			FROM trJobBudgeting 
			WHERE JobId=New.TransID

			UNION ALL

			SELECT JobID, Seq
			FROM trJobBudgetingTemp 
			WHERE JobId=New.TransID
		) r
		LEFT OUTER JOIN trJobBudgeting jb on jb.JobID=r.JobID AND jb.Seq=r.Seq
		LEFT OUTER JOIN trJobBudgetingTemp jbt on jbt.JobID=r.JobID AND jbt.Seq=r.Seq
		ORDER BY r.JobID, r.Seq;	
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;		
	
-- 	SET vStatus = 1;
	IF New.Source = 'TR' THEN
	  IF New.TransType = 'JOB' THEN
			OPEN vJobBudgeting;
				REPEAT
					FETCH vJobBudgeting INTO vTransID, vTransSeq, vStatus;
					IF NOT done THEN
						INSERT INTO AuditTrailDetail( AuditTrailID, Source, TransType, TransactionType, 
							TransID, TransSeq, Status, CreatedBy, ModifiedBy )
						VALUES( New.ID, New.Source, New.TransType, CONCAT( New.TransactionType, ' Budgeting' ),
							New.TransID, vTransSeq, vStatus, New.CreatedBy, New.ModifiedBy );
					END IF;
				UNTIL done END REPEAT;
			CLOSE vJobBudgeting;
		END IF;
	END IF;
	
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table AuditTrail
-- ----------------------------
DROP TRIGGER IF EXISTS `AuditTrail_After_Update`;
delimiter ;;
CREATE TRIGGER `AuditTrail_After_Update` AFTER UPDATE ON `AuditTrail` FOR EACH ROW BEGIN
	IF New.Source = 'TR' THEN
	  IF New.TransType = 'JOB' THEN
			IF New.Status = 1 THEN
				UPDATE trJob j SET j.Flag = 3
				WHERE j.JobID = New.TransID;
				
				DELETE FROM trJobBudgetingTemp
				WHERE JobID = New.TransID;
				DELETE FROM trJobTemp
				WHERE JobID = New.TransID;
			ELSEIF New.Status = 2 THEN
				UPDATE trJob j SET j.Flag = 4
				WHERE j.JobID = New.TransID;				
			END IF;
		END IF;
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table AuditTrailDetail
-- ----------------------------
DROP TRIGGER IF EXISTS `AuditTrailDetail_Before_Insert`;
delimiter ;;
CREATE TRIGGER `AuditTrailDetail_Before_Insert` BEFORE INSERT ON `AuditTrailDetail` FOR EACH ROW BEGIN
  SET New.AuditDescription = getAuditDescriptionDetail( New.Source, New.TransType, New.TransID, New.TransSeq, New.Status);
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table clClient
-- ----------------------------
DROP TRIGGER IF EXISTS `clClient_BEFORE_INSERT`;
delimiter ;;
CREATE TRIGGER `clClient_BEFORE_INSERT` BEFORE INSERT ON `clClient` FOR EACH ROW BEGIN
  DECLARE vclientNumber INTEGER;
  
  SELECT cast( c.optionsValue as SIGNED ) + 1
  INTO vclientNumber
  FROM cmOptions c
  WHERE c.optionsName = 'Last Client Number';
  
--  SET vclientNumber = 61;
  SET New.Seq = vclientNumber;
  SET New.Code = concat( New.Alias, '-', New.Seq );
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table clClient
-- ----------------------------
DROP TRIGGER IF EXISTS `clClient_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `clClient_AFTER_INSERT` AFTER INSERT ON `clClient` FOR EACH ROW BEGIN
  UPDATE cmOptions c 
  SET c.optionsValue = Cast( New.Seq as char(45) )
  WHERE c.optionsName = 'Last Client Number';
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table clClient
-- ----------------------------
DROP TRIGGER IF EXISTS `clClient_BEFORE_UPDATE`;
delimiter ;;
CREATE TRIGGER `clClient_BEFORE_UPDATE` BEFORE UPDATE ON `clClient` FOR EACH ROW BEGIN
  SET New.Code = concat( New.Alias, '-', New.Seq );
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table clClient
-- ----------------------------
DROP TRIGGER IF EXISTS `clClient_AFTER_UPDATE`;
delimiter ;;
CREATE TRIGGER `clClient_AFTER_UPDATE` AFTER UPDATE ON `clClient` FOR EACH ROW BEGIN
  SET @Code = concat( New.Alias, '-', New.Seq );
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table rgUser
-- ----------------------------
DROP TRIGGER IF EXISTS `CmMsRgUser_BeforeInsert`;
delimiter ;;
CREATE TRIGGER `CmMsRgUser_BeforeInsert` BEFORE INSERT ON `rgUser` FOR EACH ROW BEGIN
SET new.CreatedDate = NOW();
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table rgUser
-- ----------------------------
DROP TRIGGER IF EXISTS `CmMsRgUser_BeforeUpdate`;
delimiter ;;
CREATE TRIGGER `CmMsRgUser_BeforeUpdate` BEFORE UPDATE ON `rgUser` FOR EACH ROW BEGIN
SET new.LastUpdateDate = NOW();
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJob
-- ----------------------------
DROP TRIGGER IF EXISTS `trJob_Before_Insert`;
delimiter ;;
CREATE TRIGGER `trJob_Before_Insert` BEFORE INSERT ON `trJob` FOR EACH ROW BEGIN
	DECLARE vExpenses DECIMAL(18,0);
	
	SET New.TimeCharges = IFNULL( getJobEstTimeCharges( New.JobID, IFNULL( New.StartDate, '1900-01-01' ), 0 ), 0 );
	IF IFNULL( New.IsMeal,0 ) = 0 THEN
	  SET New.MealAllowance = 0;
	END IF;
	
	IF IFNULL( New.IsOutOfOffice,0 ) = 0 THEN
	  SET New.OutOfOfficeAllowance = 0;
	END IF;
	
	IF IFNULL( New.IsTaxi,0 ) = 0 THEN
	  SET New.TaxiAllowance = 0;
	END IF;

	IF IFNULL( New.IncludeOPE,0 ) = 0 THEN
	  SET vExpenses = New.TimeCharges;
	ELSEIF IFNULL( New.IncludeOPE,0 ) = 1 THEN
	  SET vExpenses = New.TimeCharges + New.MealAllowance + New.OutOfOfficeAllowance + New.TaxiAllowance +
										IFNULL( New.AdministrativeCharge,0 ) + IFNULL( New.OtherExpenseAllowance, 0 );	
	END IF;
	
	IF vExpenses = 0 THEN
		SET New.Percentage = '0 %'; 
	ELSE
		SET New.Percentage = CONCAT( FORMAT( ( New.Fee / vExpenses ) * 100, 2 ), ' %' ); 
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJob
-- ----------------------------
DROP TRIGGER IF EXISTS `trJob_AFTER_INSERT`;
delimiter ;;
CREATE TRIGGER `trJob_AFTER_INSERT` AFTER INSERT ON `trJob` FOR EACH ROW BEGIN
  update knncoid_tr.clClient a
  set a.LatestJob = a.LatestJob + 1
  where a.ID = New.ClientID;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJob
-- ----------------------------
DROP TRIGGER IF EXISTS `trJob_Before_Update`;
delimiter ;;
CREATE TRIGGER `trJob_Before_Update` BEFORE UPDATE ON `trJob` FOR EACH ROW BEGIN
	DECLARE vExpenses DECIMAL(18,0);
	
	SET New.TimeCharges = IFNULL( getJobEstTimeCharges( New.JobID, IFNULL( New.StartDate, '1900-01-01' ), 0 ), 0 );
	IF IFNULL( New.IsMeal,0 ) = 0 THEN
	  SET New.MealAllowance = 0;
	END IF;
	
	IF IFNULL( New.IsOutOfOffice,0 ) = 0 THEN
	  SET New.OutOfOfficeAllowance = 0;
	END IF;
	
	IF IFNULL( New.IsTaxi,0 ) = 0 THEN
	  SET New.TaxiAllowance = 0;
	END IF;

	IF IFNULL( New.IncludeOPE,0 ) = 0 THEN
	  SET vExpenses = New.TimeCharges;
	ELSEIF IFNULL( New.IncludeOPE,0 ) = 1 THEN
	  SET vExpenses = New.TimeCharges + New.MealAllowance + New.OutOfOfficeAllowance + New.TaxiAllowance +
										IFNULL( New.AdministrativeCharge,0 ) + IFNULL( New.OtherExpenseAllowance, 0 );	
	END IF;
	
	IF vExpenses = 0 THEN
		SET New.Percentage = '0 %'; 
	ELSE
		SET New.Percentage = CONCAT( FORMAT( ( New.Fee / vExpenses ) * 100, 2 ), ' %' ); 
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJob
-- ----------------------------
DROP TRIGGER IF EXISTS `tblJob_After_Update`;
delimiter ;;
CREATE TRIGGER `tblJob_After_Update` AFTER UPDATE ON `trJob` FOR EACH ROW BEGIN
	DECLARE vAuditID INT;

  IF ( New.Status = 2 ) AND ( New.Flag = 1 ) THEN
	  CALL SetTrJobTempFromJob( New.JobID, getEmpUserName( IFNULL( New.CreatedBy,'' ) ) );
	ELSEIF ( New.Status = 2 ) AND ( New.Flag = 2 ) THEN
	  INSERT INTO AuditTrail( Source, TransType, SourceNo, TransID, CreatedBy, ModifiedBy )
		VALUES ( 'TR', 'JOB', New.JobCode, New.JobID, 
						 getEmpUserName( IFNULL( New.CreatedBy,'' ) ), 
						 getEmpUserName( IFNULL( New.UpdateBy,'' ) ) );
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJobBudgeting
-- ----------------------------
DROP TRIGGER IF EXISTS `trJobBudgeting_Before_Insert`;
delimiter ;;
CREATE TRIGGER `trJobBudgeting_Before_Insert` BEFORE INSERT ON `trJobBudgeting` FOR EACH ROW BEGIN
	DECLARE vSeq TINYINT( 2 );
  DECLARE TotalHours DECIMAL( 18,1 );
	
	SELECT jb.Seq
	INTO vSeq
	FROM trJobBudgeting jb
	WHERE jb.JobID = New.JobID
	  AND jb.Seq IS NOT NULL
	ORDER BY jb.Seq DESC
	LIMIT 1;

	SET New.Seq = vSeq + 1;
	
	SET TotalHours = IFNULL( New.Planning, 0 ) + IFNULL( New.FieldWork,0 ) +
	                 IFNULL( New.Reporting, 0 ) + IFNULL( New.WrapUp, 0 ); 
	
  IF TotalHours <> New.Total THEN
		SIGNAL SQLSTATE '30001'
		  SET MESSAGE_TEXT = 'Cannot add or update row: Total and Detail Difference';
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJobBudgeting
-- ----------------------------
DROP TRIGGER IF EXISTS `trJobBudgeting_After_Insert`;
delimiter ;;
CREATE TRIGGER `trJobBudgeting_After_Insert` AFTER INSERT ON `trJobBudgeting` FOR EACH ROW BEGIN
  DECLARE vStartDate date;
	
	SELECT IFNULL( j.StartDate, '1900-01-01' )
	INTO vStartDate
	FROM trJob j
	WHERE j.JobID = New.JobID;
	
  UPDATE trJob j
	SET j.TimeCharges = IFNULL( getJobEstTimeCharges( New.JobID, vStartDate, 0 ), 0 );
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJobBudgeting
-- ----------------------------
DROP TRIGGER IF EXISTS `trJobBudgeting_Before_Update`;
delimiter ;;
CREATE TRIGGER `trJobBudgeting_Before_Update` BEFORE UPDATE ON `trJobBudgeting` FOR EACH ROW BEGIN
  DECLARE TotalHours DECIMAL( 18,1 );
	
	SET TotalHours = IFNULL( New.Planning, 0 ) + IFNULL( New.FieldWork,0 ) +
	                 IFNULL( New.Reporting, 0 ) + IFNULL( New.WrapUp, 0 ); 
	
  IF TotalHours <> New.Total THEN
		SIGNAL SQLSTATE '30001'
		  SET MESSAGE_TEXT = 'Cannot add or update row: Total and Detail Difference';
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJobBudgeting
-- ----------------------------
DROP TRIGGER IF EXISTS `trJobBudgeting_After_Update`;
delimiter ;;
CREATE TRIGGER `trJobBudgeting_After_Update` AFTER UPDATE ON `trJobBudgeting` FOR EACH ROW BEGIN
  DECLARE vStartDate date;
	
	SELECT IFNULL( j.StartDate, '1900-01-01' )
	INTO vStartDate
	FROM trJob j
	WHERE j.JobID = New.JobID;
	
  UPDATE trJob j
	SET j.TimeCharges = IFNULL( getJobEstTimeCharges( New.JobID, vStartDate, 0 ), 0 );
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJobBudgeting
-- ----------------------------
DROP TRIGGER IF EXISTS `trJobBudgeting_After_Delete`;
delimiter ;;
CREATE TRIGGER `trJobBudgeting_After_Delete` AFTER DELETE ON `trJobBudgeting` FOR EACH ROW BEGIN
  DECLARE vStartDate date;
	
	SELECT IFNULL( j.StartDate, '1900-01-01' )
	INTO vStartDate
	FROM trJob j
	WHERE j.JobID = Old.JobID;
	
  UPDATE trJob j
	SET j.TimeCharges = IFNULL( getJobEstTimeCharges( Old.JobID, vStartDate, 0 ), 0 );
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJobBudgetingTemp
-- ----------------------------
DROP TRIGGER IF EXISTS `trJobBudgetingTemp_Before_Insert`;
delimiter ;;
CREATE TRIGGER `trJobBudgetingTemp_Before_Insert` BEFORE INSERT ON `trJobBudgetingTemp` FOR EACH ROW BEGIN
	DECLARE vSeq TINYINT( 2 );
  DECLARE TotalHours DECIMAL( 18,1 );
	
	SELECT jb.Seq
	INTO vSeq
	FROM trJobBudgetingTemp jb
	WHERE jb.JobID = New.JobID
	  AND jb.Seq IS NOT NULL
	ORDER BY jb.Seq DESC
	LIMIT 1;

	SET New.Seq = vSeq + 1;
	
	SET TotalHours = IFNULL( New.Planning, 0 ) + IFNULL( New.FieldWork,0 ) +
	                 IFNULL( New.Reporting, 0 ) + IFNULL( New.WrapUp, 0 ); 
	
  IF TotalHours <> New.Total THEN
		SIGNAL SQLSTATE '30001'
		  SET MESSAGE_TEXT = 'Cannot add or update row: Total and Detail Difference';
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJobBudgetingTemp
-- ----------------------------
DROP TRIGGER IF EXISTS `trJobBudgetingTemp_Before_Update`;
delimiter ;;
CREATE TRIGGER `trJobBudgetingTemp_Before_Update` BEFORE UPDATE ON `trJobBudgetingTemp` FOR EACH ROW BEGIN
  DECLARE TotalHours DECIMAL( 18,1 );
	
	SET TotalHours = IFNULL( New.Planning, 0 ) + IFNULL( New.FieldWork,0 ) +
	                 IFNULL( New.Reporting, 0 ) + IFNULL( New.WrapUp, 0 ); 
	
  IF TotalHours <> New.Total THEN
		SIGNAL SQLSTATE '30001'
		  SET MESSAGE_TEXT = 'Cannot add or update row: Total and Detail Difference';
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trJobTemp
-- ----------------------------
DROP TRIGGER IF EXISTS `trJob_AFTER_INSERT_copy1`;
delimiter ;;
CREATE TRIGGER `trJob_AFTER_INSERT_copy1` AFTER INSERT ON `trJobTemp` FOR EACH ROW BEGIN
  update knncoid_tr.clClient a
  set a.LatestJob = a.LatestJob + 1
  where a.ID = New.ClientID;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trTimeReportMeals
-- ----------------------------
DROP TRIGGER IF EXISTS `trTimeReportMeals_Before_Insert`;
delimiter ;;
CREATE TRIGGER `trTimeReportMeals_Before_Insert` BEFORE INSERT ON `trTimeReportMeals` FOR EACH ROW BEGIN

	DECLARE vSeq TINYINT( 2 );
	
	IF EXISTS ( SELECT i.Seq
							FROM trTimeReportMeals i
							WHERE i.TimeReportID = New.TimeReportID
								AND i.Seq IS NOT NULL ) THEN
		SELECT i.Seq
		INTO vSeq
		FROM trTimeReportMeals i
		WHERE i.TimeReportID = New.TimeReportID
			AND i.Seq IS NOT NULL
		ORDER BY jb.Seq DESC
		LIMIT 1;
	ELSE
		SET vSeq = 1;
	END IF;

	SET New.Seq = vSeq + 1;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trTimeReportOutOfOffice
-- ----------------------------
DROP TRIGGER IF EXISTS `trTimeReportOutOfOffice_before_insert`;
delimiter ;;
CREATE TRIGGER `trTimeReportOutOfOffice_before_insert` BEFORE INSERT ON `trTimeReportOutOfOffice` FOR EACH ROW BEGIN
	DECLARE vSeq TINYINT( 2 );
	
	IF EXISTS( SELECT i.Seq
						 FROM trTimeReportOutOfOffice i
						 WHERE i.TimeReportID = New.TimeReportID
							 AND i.Seq IS NOT NULL ) THEN
		SELECT i.Seq
		INTO vSeq
		FROM trTimeReportOutOfOffice i
		WHERE i.TimeReportID = New.TimeReportID
			AND i.Seq IS NOT NULL
		ORDER BY jb.Seq DESC
		LIMIT 1;
	ELSE
		SET vSeq = 1;
	END IF;

	SET New.Seq = vSeq + 1;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table trTimeReportTaxi
-- ----------------------------
DROP TRIGGER IF EXISTS `trTimeReportTaxi_before_insert`;
delimiter ;;
CREATE TRIGGER `trTimeReportTaxi_before_insert` BEFORE INSERT ON `trTimeReportTaxi` FOR EACH ROW BEGIN

	DECLARE vSeq TINYINT( 2 );

	IF EXISTS ( SELECT i.Seq
							FROM trTimeReportTaxi i
							WHERE i.TimeReportID = New.TimeReportID
								AND i.Seq IS NOT NULL ) THEN
		SELECT i.Seq
		INTO vSeq
		FROM trTimeReportTaxi i
		WHERE i.TimeReportID = New.TimeReportID
			AND i.Seq IS NOT NULL
		ORDER BY jb.Seq DESC
		LIMIT 1;
	ELSE
	  SET vSeq = 1;
	END IF;

	SET New.Seq = vSeq + 1;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
